﻿using Amanah.Posthub.OrderService.Application.Features.Accounts.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class AccountController : BaseApiController
    {
        [HttpGet("activateAccount/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new ActivateAccountCommand { BusinessCustomerId = id }));
        }
    }
}
