﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.Areas.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Areas.Queries;
using Amanah.Posthub.OrderService.Application.Features.Blocks.Queries;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class AreaController : BaseApiController
    {
        [HttpPost("GetAreasForDispatch")]
        public async Task<IActionResult> GetAreasForDispatch(QueryListReq queryList)
        {
            return Ok(await Mediator.Send(new GetAreasForDispatchQuery()
            {
                PageNumber = queryList.PageNumber,
                PageSize = queryList.PageSize
            }));
        }

        [HttpGet("GetBlocksByAreaIdForDispatch/{areaId}")]
        public async Task<IActionResult> GetBlocksByAreaIdForDispatch(string areaId)
        {
            return Ok(await Mediator.Send(new GetBlocksByAreaIdForDispatchQuery { AreaId = areaId }));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Areas.CreateArea)]
        [HttpPost("CreateArea")]
        public async Task<IActionResult> Create(CreateAreaDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Areas.DeleteArea)]
        [HttpPost("DeleteArea")]
        public async Task<IActionResult> Delete(DeleteAreaDTO command)
        {
            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Areas.UpdateArea)]
        [HttpPut("UpdateArea")]
        public async Task<IActionResult> Update(UpdateAreaDTO command)
        {
            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Areas.ListAreas)]
        [HttpGet("GetAllActiveAreas")]
        public async Task<IActionResult> GetAllActiveAreas()
        {
            return Ok(await Mediator.Send(new GetAllActiveAreaQuery()));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Areas.ListAreas)]
        [HttpPost("GetAllAreasPaging")]
        public async Task<IActionResult> GetAllAreas(GetAllAreasQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpGet("GetAllBlocksByAreaId/{areaId}")]
        public async Task<IActionResult> GetAllBlocksByAreaId(int areaId)
        {
            return Ok(await Mediator.Send(new GetAllBlocksByAreaIdQuery { AreaId = areaId }));
        }

    }


}
