﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.API.Controllers;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries;
using Amanah.Posthub.OrderService.Application.Shared.ValidationsHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.BusinessOrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    [FeatureGate(FeatureManagementFlags.Concierge)]
    public class BusinessOrderController : BaseApiController
    {
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IValidationsUploadFiles _validationsUploadFiles;

        public BusinessOrderController(IUploadFormFileService uploadFormFileService, IValidationsUploadFiles validationsUploadFiles)
        {
            _uploadFormFileService = uploadFormFileService;
            _validationsUploadFiles = validationsUploadFiles; 
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.CreateBusinessOrders)]
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromForm] CreateBusinessOrderCommand command)
        {
            if (command.OrderFile != null && command.OrderFile.Count > 0)
            {
                _validationsUploadFiles.ValidateAllFilesSize(command.OrderFile, 10);

                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesAsync(command.OrderFile, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImageURL = ProofOfDeliveryResult.ReturnData.ToList();
            }

            try
            {
                var res = await Mediator.Send(command);  
            //return Redirect(res.PaymentURL);

                 return Ok(res);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }

        [HttpPost("CreateInternationalOrderForMobile")]
        public async Task<IActionResult> CreateInternationalOrderForMobile([FromForm] CreateBusinessOrderForMobileInputDTO command)
        {
            if (command.OrderFiles != null && command.OrderFiles.Count > 0)
            {
                _validationsUploadFiles.ValidateAllFilesSize(command.OrderFiles, 10);

                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesAsync(command.OrderFiles, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImagesURLs = ProofOfDeliveryResult.ReturnData.ToList();
            }

            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        [HttpPost("CreateInternationalOrderForMobileFromBody")]
        public async Task<IActionResult> CreateInternationalOrderForMobileFromBody([FromBody] CreateBusinessOrderForMobileInputDTO command)
        {
            if (command.OrderFilesBase64 != null && command.OrderFilesBase64.Count > 0)
            {
                  _validationsUploadFiles.ValidateAllFilesSize(command.OrderFilesBase64, 10);

                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesFromListOfBase64Async(command.OrderFilesBase64, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImagesURLs = ProofOfDeliveryResult.ReturnData.ToList();
            }

            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpPost("CreateBusinessOrderDraft")]
        public async Task<IActionResult> CreateBusinessOrderDraft([FromForm] CreateBusinessOrderDraftCommand command)
        {
            if (command.OrderFile != null)
            {
                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFileAsync(command.OrderFile, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && !string.IsNullOrEmpty(ProofOfDeliveryResult.ReturnData))
                {
                    command.ImageURL = (ProofOfDeliveryResult.ReturnData);
                }
            }
            return Ok(await Mediator.Send(command));
        }


        [HttpGet("GetAll")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_ViewOrders)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetBusinessOrderListQuery()));
        }


        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetBusinessOrderByIdQuery { Id = id }));
        }

        [HttpGet("GetInternalOrderById/{id}")]
        public async Task<IActionResult> GetInternalOrderByIdQuery(int id)
        {
            return Ok(await Mediator.Send(new InternalOrderByIdQuery { Id = id }));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_DeleteOrder)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteBusinessOrderByIdCommand { Id = id }));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_UpdateOrder)]
        [HttpPost("UpdateTrackingNumber")]
        public async Task<IActionResult> UpdateTrackingNumber(TrackingNumberDto command)
        {
            if (command.OrderId < 0)
                return BadRequest();

            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_UpdateOrder)]
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromForm] UpdateBusinessOrderCommand command)
        {
            if (command.Id < 0)
            {
                return BadRequest();
            }
            if (command.OrderFile != null)
            {
                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFileAsync(command.OrderFile, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && !string.IsNullOrEmpty(ProofOfDeliveryResult.ReturnData))
                {
                    command.ImageURL = ProofOfDeliveryResult.ReturnData;
                }
            }
            return Ok(await Mediator.Send(command));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.InternalPosts.InternalPost_EditInternalPost)]

        [HttpPost("UpdateInternalOrder")]
        public async Task<IActionResult> UpdateInternalOrder(UpdateInternalOrderCommand command)
        {
            return Ok(await Mediator.Send(command));
        }


        [HttpGet("GetOrderByCustomerId/{customerId}")]
        public async Task<IActionResult> GetOrderByCustomerId(int customerId)
        {
            return Ok(await Mediator.Send(new GetBusinessOrderByCustomerIdQuery { CustomerId = customerId }));
        }

        [HttpGet("GetAllDraftBusinessOrder")]
        public async Task<IActionResult> GetAllDraftBusinessOrder(int customerId)
        {
            return Ok(await Mediator.Send(new GetAllDraftBusinessOrderQuery()));
        }
        [HttpGet("GetBusinessOrderCount")]
        public async Task<IActionResult> GetBusinessOrderCount()
        {
            return Ok(await Mediator.Send(new GetBusinessOrderCountQuery()));
        }

        [HttpGet("GetDailyOrders")]
        public async Task<IActionResult> GetDailyOrders()
        {
            return Ok(await Mediator.Send(new GetDailyOrdersQuery()));
        }


        [HttpGet("GetClosedOrders")]
        public async Task<IActionResult> GetClosedOrders()
        {
            return Ok(await Mediator.Send(new GetClosedOrdersQuery()));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.InternalPosts.CreateInternalPost)]
        [HttpPost("CreateInternal")]
        public async Task<IActionResult> CreateInternal(CreateInternalOrderCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
       [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.InternalPosts.InternalPost_ReadInternalPost)]

        [HttpPost("GetInternalList")]
        public async Task<IActionResult> GetInternalList(GetInternalListOrderQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("GetReceiveOrderPaginatedList")]
        public async Task<IActionResult> GetReceiveOrderPaginatedList(GetReceiveOrderPaginatedListQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("GetStatementPaginatedList")]
        public async Task<IActionResult> GetStatementPaginatedList(GetStatementPaginatedListQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("GetStatementFilteredPaginatedList")]
        public async Task<IActionResult> GetStatementFilteredPaginatedList(GetStatementFilteredPaginatedListQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("ReceiveOrder")]
        public async Task<IActionResult> ReceiveOrder(ReceiveOrderCommand command)
        {
            var res = await Mediator.Send(command);
            return Ok(res);
        }

        [HttpPost("ChangeReceiveOrder")]
        public async Task<IActionResult> ChangeReceiveOrder(ChangeReceiveOrderInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("PrintPostBagStatement")]
        public async Task<IActionResult> PrintPostBagStatement(PrintPostBagStatementQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("CreateStatement")]
        public async Task<IActionResult> CreateStatement(CreateStatementInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("UpdateStatement")]
        public async Task<IActionResult> UpdateStatement([FromForm] UpdateStatementInputDTO command)
        {
            if (command.WorkOrderFile != null && command.WorkOrderFile.Length > 0)
            {
                var uploadSignatureResult = await _uploadFormFileService.UploadFileAsync(command.WorkOrderFile, "UploadFile/WorkOrderFile");
                if (!string.IsNullOrEmpty(uploadSignatureResult.ReturnData))
                {
                    command.WorkOrderImageURL = uploadSignatureResult.ReturnData;
                }
            }
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("PayAgain")]
        public async Task<IActionResult> PayAgainAsync([FromBody] PayBusinessOrderAgainCommand command)
        {
            if (command.Id < 0)
            {
                return BadRequest();
            }           
            return Ok(await Mediator.Send(command));
        }


    }
}
