﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ContractController : BaseApiController
    {


        [HttpGet("GetAll")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Contracts.ReadContract)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetContractListQuery()));
        }
        
        [HttpGet("GetContractStatusById/{contractId}")]
        public async Task<IActionResult> GetContractStatusById(int contractId)
        {
            return Ok(await Mediator.Send(new GetContractStatusByContractIdQuery { ContractStatusId = contractId }));
        }

        [HttpGet("GetContractById/{id}")]
        public async Task<IActionResult> GetContractById(int id)
        {
            return Ok(await Mediator.Send(new GetContractByIdQuery { Id = id }));

        }

        [HttpPost("Update")]

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Contracts.UpdateContract)]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateContractInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Contracts.AddContract)]
        [HttpPost("Create")]
        public async Task<IActionResult> CreateAsync([FromBody] CreateContractInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("Delete")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Contracts.DeleteContract)]
        public async Task<IActionResult> DeleteAsync([FromBody] DeleteContractInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetContractTypes")]
        public async Task<IActionResult> GetContractTypes()
        {
            return Ok(await Mediator.Send(new ContractTypesLookupQuery()));
        }

        [HttpGet("GetContractStatuss")]
        public async Task<IActionResult> ContractStatuss()
        {
            return Ok(await Mediator.Send(new ContractStatussLookupQuery()));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Contracts.ReadContract)]

        [HttpPost("GetAllPagginated")]
        public async Task<IActionResult> GetAllPagginatedAsync([FromBody] GetContractPagginatedListQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("GetContractsByBusinessCustomerId/{businessCustomerId}")]
        public async Task<IActionResult> GetContractsByBusinessCustomerId(int businessCustomerId)
        {
            return Ok(await Mediator.Send(new GetContractsByBusinessCustomerIdQuery() { BusinessCustomerId = businessCustomerId }));
        }
        [HttpPost("GetUnitPriceByBusinessCustomerIdAndContractCode")]
        public async Task<IActionResult> GetUnitPriceByBusinessCustomerIdAndContractCode([FromBody] GetUnitPriceByBusinessCustomerIdAndCodeQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpPost("ChangeContractStatus")]
        public async Task<IActionResult> ChangeContractStatus([FromBody] ChangeContractStatusInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }





        [Route("[action]")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Contracts.ExportContract)]
        public async Task<ActionResult> ExportToExcel([FromQuery] ExportContractListQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "Contracts.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }

        [Route("PrintContract/{id}")]
        [HttpGet]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<ActionResult> PrintContract(int id)
        {
            var data = await Mediator.Send(new GetContractAsPDF() { ContractId = id });
            var file = File(data, "application/pdf", "contract.pdf");
            return file;
        }
       
        

        [Route("SendContractToCustomer/{id}")]
        [HttpGet]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<ActionResult> SendContractToCustomer(int id)
        {
            var data = await Mediator.Send(new GetContractAsPDF() { ContractId = id, SendContractToCustomerEmail = true });


            return File(data, "application/pdf", "contract.pdf");
        }



    }
}
