﻿using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Queries;
using Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class CourierController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Create(CourierCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveCourier")]
        public async Task<IActionResult> ActiveInActiveCourier(ActivateCourierCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetCourierListQuery()));
        }
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveCourierListQuery()));
        }
        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetCourierByIdQuery { Id = id }));
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteCourierCommand { Id = id }));
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateCourierCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }


        [HttpPost("GetAllPagginated")]
        public async Task<IActionResult> GetAllPagginatedAsync([FromBody] GetContractPagginatedListQuery query)
        {
            return Ok(await Mediator.Send(query));
        }


    }
}
