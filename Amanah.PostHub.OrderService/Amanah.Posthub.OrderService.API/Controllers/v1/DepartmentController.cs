﻿using Amanah.Posthub.OrderService.Application.Features.Department.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.Department.Queries;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Amanah.Posthub.BLL.Authorization;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class DepartmentController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Departments.CreateDepartment)]

        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreateDepartmentInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Departments.ListDepartment)]

        [HttpPost("GetDepartmentPagginatedList")]
        public async Task<IActionResult> GetDepartmentPagginatedList(GetDepartmentPagginatedListQuery query)
        {
            return Ok(await Mediator.Send(query));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Departments.DeleteDepartment)]

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteDepartmentInputDTO { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Departments.UpdateDepartment)]

        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdateDepartmentInputDTO command)
        {
            if (command == null)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
