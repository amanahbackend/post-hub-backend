﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.DriverRoles.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.DriverRoles.Queries;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DriverRolesController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.DriverRoles.CreateDriverRoles)]

        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreateDriverRoleInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.DriverRoles.ListDriverRoles)]

        [HttpPost("GetDriverRolesPaginatedList")]
        public async Task<IActionResult> GetDriverRolesPaginatedList(GetDriverRolesPaginatedListQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("GetAllDriverRoles")]
        public async Task<IActionResult> GetAllDriverRoles( )
        {
            return Ok(await Mediator.Send(new GetAllDriverRolesQuery()));
        }
        
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.DriverRoles.DeleteDriverRoles)]

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteDriverRoleInputDTO { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.DriverRoles.UpdateDriverRoles)]

        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdateDriverRolesInputDTO command)
        {
            if (command == null)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
