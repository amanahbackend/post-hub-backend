﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.Express;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.Express;
using Amanah.Posthub.OrderService.Application.Shared.ValidationsHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.OrderService.API.Controllers.v1.Express
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    [FeatureGate(FeatureManagementFlags.Express)]
    public class BusinessOrderExpressController : BaseApiController
    {
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IValidationsUploadFiles _validationsUploadFiles;

        public BusinessOrderExpressController(IUploadFormFileService uploadFormFileService, IValidationsUploadFiles validationsUploadFiles)
        {
            _uploadFormFileService = uploadFormFileService;
            _validationsUploadFiles = validationsUploadFiles;
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.CreateBusinessOrders)]
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromForm] CreateBusinessOrderForExpressCommand command)
        {
            if (command.OrderFiles != null)
            {
                _validationsUploadFiles.ValidateAllFilesSize(command.OrderFiles, 10);

                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesAsync(command.OrderFiles, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImagesURLs = ProofOfDeliveryResult.ReturnData.ToList();
            }
            return Ok(await Mediator.Send(command));
        }


        [HttpPost("CreateBusinessOrderDraft")]
        public async Task<IActionResult> CreateBusinessOrderDraft([FromForm] CreateBusinessOrderDraftForExpressCommand command)
        {
            if (command.OrderFiles != null)
            {
                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesAsync(command.OrderFiles, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImagesURLs = (ProofOfDeliveryResult.ReturnData);

            }
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetAll")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_ViewOrders)]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetBusinessOrderListQuery()));
        }


        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetBusinessOrderByIdForExpressQuery { Id = id }));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_DeleteOrder)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteBusinessOrderByIdCommand { Id = id }));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_UpdateOrder)]
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromForm] UpdateBusinessOrderForExpressCommand command)
        {
            if (command.Id < 0)
            {
                return BadRequest();
            }
            if (command.OrderFile != null)
            {
                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFileAsync(command.OrderFile, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && !string.IsNullOrEmpty(ProofOfDeliveryResult.ReturnData))
                {
                    command.ImageURL = ProofOfDeliveryResult.ReturnData;
                }
            }
            return Ok(await Mediator.Send(command));
        }


        [HttpGet("GetOrderByCustomerId/{customerId}")]
        public async Task<IActionResult> GetOrderByCustomerId(int customerId)
        {
            return Ok(await Mediator.Send(new GetBusinessOrderByCustomerIdQuery { CustomerId = customerId }));
        }

        [HttpGet("GetAllDraftBusinessOrder")]
        public async Task<IActionResult> GetAllDraftBusinessOrder(int customerId)
        {
            return Ok(await Mediator.Send(new GetAllDraftBusinessOrderQuery()));
        }
        [HttpGet("GetBusinessOrderCount")]
        public async Task<IActionResult> GetBusinessOrderCount()
        {
            return Ok(await Mediator.Send(new GetBusinessOrderCountQuery()));
        }

        [HttpGet("GetDailyOrders")]
        public async Task<IActionResult> GetDailyOrders()
        {
            return Ok(await Mediator.Send(new GetDailyOrdersQuery()));
        }


        [HttpGet("GetClosedOrders")]
        public async Task<IActionResult> GetClosedOrders()
        {
            return Ok(await Mediator.Send(new GetClosedOrdersQuery()));
        }

        [HttpPost("PayAgain")]
        public async Task<IActionResult> PayAgainAsync([FromBody] PayBusinessOrderAgainCommand command)
        {
            if (command.Id < 0)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }


    }
}
