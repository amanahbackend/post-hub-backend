﻿using Amanah.Posthub.OrderService.Application.Features.Address.Queries;
using Amanah.Posthub.OrderService.Application.Shared.ValidationsHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Microsoft.FeatureManagement.Mvc;
using static Utilities.Utilites.Localization.Keys;
using Amanah.Posthub.OrderService.Application.Features.MailItemStatusLogs.Queries.Express;
using Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries.Express;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries.Express;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.Express;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.Express;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.Express;

namespace Amanah.Posthub.OrderService.API.Controllers.v1.Express
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [FeatureGate(FeatureManagementFlags.Express)]
    public class MobileExpressController : BaseApiController
    {
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IValidationsUploadFiles _validationsUploadFiles;
        public MobileExpressController(IUploadFormFileService uploadFormFileService, IValidationsUploadFiles validationsUploadFiles)
        {
            _uploadFormFileService = uploadFormFileService;
            _validationsUploadFiles = validationsUploadFiles;
        }

        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Mobile.MailItemTracking)]
        [HttpGet("GeMailItemStatusLogByItemCode/{Code}")]
        public async Task<IActionResult> GeMailItemStatusLogByItemCode(string Code)
        {
            return Ok(await Mediator.Send(new GetMailItemStatusLogsByItemCodeForMobileExpressQuery() { Code = Code }));
        }

        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Mobile.OrderTracking)]
        [HttpGet("GeOrderStatusLogsByOrderCode/{Code}")]
        public async Task<IActionResult> GeOrderStatusLogsByOrderCode(string Code)
        {
            return Ok(await Mediator.Send(new GetOrderStatusLogsByOrderCodeForMobileExpressQuery() { Code = Code }));
        }

        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Mobile.OrdersList)]
        [HttpPost("GetAllOrdersPagginated")]
        public async Task<IActionResult> GetAllOrdersPagginatedAsync([FromBody] GetAllOrdersPagginatedExpressQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("GetMailItemById/{id}")]
        public async Task<IActionResult> GetMailItemById(int id)
        {
            return Ok(await Mediator.Send(new GetMailItemByIdExpressQuery { Id = id }));
        }

        [HttpGet("GetOrderById/{id}")]
        public async Task<IActionResult> GetOrderById(int id)
        {
            return Ok(await Mediator.Send(new GetBusinessOrderByIdForMobileExpressQuery { Id = id }));
        }

        [HttpPost("GetMailItemsByOrderId")]
        public async Task<IActionResult> GetMailItemsByOrderId(GetMailItemListByOrderIdForMobileExpressQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [AllowAnonymous]
        [HttpGet("GetAddressData")]
        public async Task<IActionResult> GetAddressData()
        {
            return Ok(await Mediator.Send(new GetAddressDataQuery()));
        }

        [HttpPost("CreateOrder")]
        public async Task<IActionResult> CreateOrder([FromForm] CreateBusinessOrderForMobileExpressCommand command)
        {
            if (command.OrderFiles != null && command.OrderFiles.Count > 0)
            {
                _validationsUploadFiles.ValidateAllFilesSize(command.OrderFiles, 10);

                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesAsync(command.OrderFiles, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImagesURLs = ProofOfDeliveryResult.ReturnData.ToList();
            }

            return Ok(await Mediator.Send(command));
        }

        [HttpPost("CreateOrderFromBody")]
        public async Task<IActionResult> CreateOrderFromBody([FromBody] CreateBusinessOrderForMobileExpressCommand command)
        {
            if (command.OrderFilesBase64 != null && command.OrderFilesBase64.Count > 0)
            {
                _validationsUploadFiles.ValidateAllFilesSize(command.OrderFilesBase64, 10);

                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesFromListOfBase64Async(command.OrderFilesBase64, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImagesURLs = ProofOfDeliveryResult.ReturnData.ToList();
            }

            return Ok(await Mediator.Send(command));
        }

    }
}
