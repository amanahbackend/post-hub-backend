﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.Orders.Commands;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries.Express;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System.IO;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.OrderService.API.Controllers.v1.Express
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    [ApiController]
    [FeatureGate(FeatureManagementFlags.Express)]
    public class OrderExpressController : BaseApiController
    {

        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreateOrderCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetOrderListQuery()));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_ViewOrders)]
        [HttpPost("GetOrderForDispatch")]
        public async Task<IActionResult> GetOrderForDispatch(QueryListReq queryListReq)
        {
            return Ok(await Mediator.Send(new GetOrderForDispatchQuery()
            {
                PageNumber = queryListReq.PageNumber,
                PageSize = queryListReq.PageSize
            }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_ViewOrders)]
        [HttpPost("GetAllPagginated")]
        public async Task<IActionResult> GetAllPagginatedAsync([FromBody] GetAllOrdersPagginatedExpressQuery query) 
        {
            return Ok(await Mediator.Send(query));
        }



        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetOrderByIdQuery { Id = id }));
        }



        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_DeleteOrder)]

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteOrderByIdCommand { Id = id }));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_UpdateOrder)]
        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateOrderCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_ExportOrders)]
        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcel([FromQuery] GetAllOrdersForExportToExcelQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "Orders.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }
    }
}
