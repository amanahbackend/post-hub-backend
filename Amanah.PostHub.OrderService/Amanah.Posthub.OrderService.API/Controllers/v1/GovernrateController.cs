﻿using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Queries;
using Amanah.Posthub.OrderService.Application.Features.Lookups.Governrates.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class GovernrateController : BaseApiController
    {
        private readonly ILocalizer _localizer;

        public GovernrateController(ILocalizer localizer)
        {
            _localizer = localizer;
        }
        [HttpPost("Create")]
        public async Task<IActionResult> Create(GovernrateCommand command)
        {
          
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveGovernrate")]
        public async Task<IActionResult> ActiveInActiveGovernrate(ActivateGovernrateCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetGovernrateListQuery()));
        }
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveGovernrateListQuery()));
        }

        [HttpGet("GetAllActiveByCountryId/{countryId}")]
        public async Task<IActionResult> GetAllActiveByCountryId(int countryId)
        {
            return Ok(await Mediator.Send(new GetActiveGovernrateListByCountryIdQuery {CountryId= countryId }));
        }

        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetGovernrateByIdQuery { Id = id }));
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteGovernrateCommand { Id = id }));
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateGovernrateCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

    }
}
