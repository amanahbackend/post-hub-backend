﻿
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.Invoices.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.Invoices.Queries;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class InvoiceController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Invoices.DeleteInvoices)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteInvoiceInputDTO { Id = id }));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Invoices.EditInvoices)]
        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateInvoiceInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Invoices.GenerateInvoice)]
        [HttpPost("Create")]
        public async Task<IActionResult> CreateAsync([FromBody] GenerateInvoiceInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Invoices.ListInvoices)]
        [HttpPost("GetAllPagginated")]
        public async Task<IActionResult> GetAllPagginatedAsync([FromBody] GetAllInvoicesPaginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Invoices.ListInvoices)]
        [HttpPost("GetSearchedInvoicesPagginated")]
        public async Task<IActionResult> GetSearchedInvoicesPagginated([FromBody] GetSearchedInvoicesPaginatedQuery filter)
        {
            return Ok(await Mediator.Send(filter));
        }


        [HttpGet("GetById/{Id}")]
        public async Task<IActionResult> GetByIdAsync(int Id)
        {
            return Ok(await Mediator.Send(new InvoiceDetailsQuery() { Id = Id }));
        }


        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Invoices.ExportToExcelInvoices)]
        [Route("ExportToExcel")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcel([FromQuery] ExportPLDataQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "PriceLists.xlsx";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }
        [HttpGet("GetInvoicesCount")]
        public async Task<IActionResult> GetInvoicesCount()
        {
            return Ok(await Mediator.Send(new GetInvoicesCountQuery()));
        }

    }
}
