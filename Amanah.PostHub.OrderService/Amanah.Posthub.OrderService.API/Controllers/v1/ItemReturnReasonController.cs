﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    public class ItemReturnReasonController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.ItemReturnReasons.CreateItemReturnReason)]
        [HttpPost("Create")]
        public async Task<IActionResult> Create(ItemReturnReasonCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveItemReturnReason")]
        public async Task<IActionResult> ActiveInActiveItemReturnReason(ActivateItemReturnReasonCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetItemReturnReasonListQuery()));
        }
        // Ramzy: (Policy) not working with mobile, need to be isolated into new API for mobile
        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.ItemReturnReasons.ListItemReturnReasons)]
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveItemReturnReasonListQuery()));
        }
        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetItemReturnReasonByIdQuery { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.ItemReturnReasons.DeleteItemReturnReason)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteItemReturnReasonCommand { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.ItemReturnReasons.UpdateItemReturnReason)]
        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateItemReturnReasonCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
