﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    public class LanguageController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Languages.CreateLanguage)]
        [HttpPost("Create")]
        public async Task<IActionResult> Create(LanguageCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveLanguage")]
        public async Task<IActionResult> ActiveInActiveLanguage(ActivateLanguageCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetLanguageListQuery()));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Languages.ListLanguages)]
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveLanguageListQuery()));
        }
        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetLanguageByIdQuery { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Languages.DeleteLanguage)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteLanguageCommand { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Languages.UpdateLanguage)]
        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateLanguageCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
