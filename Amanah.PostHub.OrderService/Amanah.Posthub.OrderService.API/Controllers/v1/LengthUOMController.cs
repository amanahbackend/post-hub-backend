﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    public class LengthUOMController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.LengthUOMs.CreateLengthUOM)]
        [HttpPost("Create")]
        public async Task<IActionResult> Create(LengthUOMCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveLengthUOM")]
        public async Task<IActionResult> ActiveInActiveLengthUOM(ActivateLengthUOMCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetLengthUOMListQuery()));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.LengthUOMs.ListLengthUOMs)]
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveLengthUOMListQuery()));
        }
        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetLengthUOMByIdQuery { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.LengthUOMs.DeleteLengthUOM)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteLengthUOMCommand { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.LengthUOMs.UpdateLengthUOM)]
        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateLengthUOMCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

    }
}
