﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Commands;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.Express;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System.Threading.Tasks;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class MailItemController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreateMailItemCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetMailItemListQuery()));
        }

        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetMailItemByIdQuery { Id = id }));
        }
        [HttpGet("GetMailItemsByOrderIdForDispatch/{id}")]
        public async Task<IActionResult> GetMailItemsByOrderIdForDispatch(int id)
        {
            return Ok(await Mediator.Send(new GetMailItemsByOrderIdForDispatchQuery { OrderId = id }));
        }
        [HttpPost("GetMailItemsByBlockForDispatchQuery")]
        public async Task<IActionResult> GetMailItemsByBlockForDispatchQuery(GetMailItemsByBlockForDispatchQuery query)
        {
            return Ok(await Mediator.Send(new GetMailItemsByBlockForDispatchQuery { BlockId = query.BlockId, AreaId = query.AreaId }));
        }


        [HttpPost("GetDrivesrForMailItemDispatch")]
        public async Task<IActionResult> GetDrivesrForMailItemDispatchAsync(GetDrivesrForMailItemDispatchQuery body)
        {
            return Ok(await Mediator.Send(body));
        }

        [HttpPost("GetMailItemsByAssignedDriverForDispatch")]
        public async Task<IActionResult> GetMailItemsByBlockForDispatchQuery(GetMailItemsByAssignedDriverForDispatchQuery query)
        {
            return Ok(await Mediator.Send(query));
        }


        

        [HttpPost("GetMailItemsForPintForm2")]
        [AllowAnonymous]
        public async Task<IActionResult> GetMailItemsByBlockForDispatchQuery(GetMailItemForrm2Query query)
        {
            return Ok(await Mediator.Send(query));
        }


        [HttpGet]
        [Route("GetByOrderId/{orderId}")]
        public async Task<IActionResult> GetByOrderId(int orderId)
        {
            return Ok(await Mediator.Send(new GetMailItemListByOrderIdQuery { Id = orderId }));
        }

        [HttpPost]
        [Route("GetMailitemsBySearch")]
        public async Task<IActionResult> GetMailitemsBySearch(GetMailItemListByOrderIdQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteMailItemByIdCommand { Id = id }));
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateMailItemCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
        #region Reports
        [HttpPost("GetDeliveredMailItemsPagginated")]
        public async Task<IActionResult> GetDeliveredMailItemsPagginated([FromBody] GetDeliveredMailItemsPaginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

       // [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WorkOrders.ReturnedMailitem_ViewReturnedMailitem)]
        [HttpPost("GetReturnedMailItemsPagginated")]
        public async Task<IActionResult> GetReturnedMailItemsPagginated([FromBody] GetReturnedMailItemsPaginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WorkOrders.ReturnedMailitem_EditReturnedMailitem)]
        [HttpPost("GetReturnedMailItemsForEditPagginated")]
        public async Task<IActionResult> GetReturnedMailItemsForEditPagginated([FromBody] GetReturnedMailItemsForEditPaginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }




        [HttpGet("GetDeliveredMailItemById/{Id}")]
        public async Task<IActionResult> GetDeliveredMailItemById(int Id)
        {
            return Ok(await Mediator.Send(new DeliveredMailItemsDetailsQuery() { Id = Id }));
        }
        #endregion


        [FeatureGate(FeatureManagementFlags.Concierge)]
        [HttpPost("GeMailItemByCodeForMobile")]
        public async Task<IActionResult> GeMailItemByIdForMobileAsync(GeMailItemByCodeForMobileQuery query)
        {
            var res= await Mediator.Send(new GeMailItemByCodeForMobileQuery { Code = query.Code, DriverId = query.DriverId });
            //if(! string.IsNullOrEmpty( res.Error))
            //{
            //    return BadRequest(_localizer[res.Error]);

            //}
            return Ok(res);
        }

        [FeatureGate(FeatureManagementFlags.Express)]
        [HttpPost("GeMailItemByCodeForMobileExpress")]
        public async Task<IActionResult> GeMailItemByIdForMobileForExpressAsync(GeMailItemByCodeForMobileForExpressQuery query)
        {
            var res = await Mediator.Send(new GeMailItemByCodeForMobileForExpressQuery { Code = query.Code, DriverId = query.DriverId });
            //if(! string.IsNullOrEmpty( res.Error))
            //{
            //    return BadRequest(_localizer[res.Error]);

            //}
            return Ok(res);
        }



        [HttpGet("GetMailItemCountByItemStatus")]
        public async Task<IActionResult> GetMailItemCountByItemStatus()
        {
            return Ok(await Mediator.Send(new GetMailItemCountByItemStatusQuery()));
        }

        [HttpGet("IsExistMailItemCode/{code}")]
        public async Task<IActionResult> IsExistMailItemCodeAsync(string  code)
        {
            return Ok(await Mediator.Send(new IsExistMailItemByCodeQuery { Code = code}));
        }

        [HttpPost("GeReceivedMailitemsPrint")]
        public async Task<IActionResult> GeReceivedMailitemsPrint(ReceivedMailitemsPrintQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
    }
} 
