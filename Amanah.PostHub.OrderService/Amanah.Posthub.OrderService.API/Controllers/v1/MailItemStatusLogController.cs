﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class MailItemStatusLogController : BaseApiController
    {

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Reports.MailItemTracking)]
        [HttpGet("GeMailItemStatusLogByItemCode/{Code}")]
        public async Task<IActionResult> GeMailItemStatusLogByItemCode(string Code)
        {
            return Ok(await Mediator.Send(new GetMailItemStatusLogsByItemCodeQuery() { Code = Code }));
        }
    }
}
