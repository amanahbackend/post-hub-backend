﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    public class MailItemTypeController : BaseApiController
    {
        private readonly ILocalizer _localizer;

        public MailItemTypeController(ILocalizer localizer)
        {
            _localizer = localizer;
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.ItemTypes.CreateItemType)]
        [HttpPost("Create")]
        public async Task<IActionResult> Create(MailItemTypeCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveMailItemType")]
        public async Task<IActionResult> ActiveInActiveMailItemType(ActivateMailItemTypeCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetMailItemTypeListQuery()));
        }
        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.ItemTypes.ListItemTypes)]
        [AllowAnonymous]
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveMailItemTypeListQuery()));
        }
        
        
        
        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetMailItemTypeByIdQuery { Id = id }));
        }
        
        
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.ItemTypes.DeleteItemType)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteMailItemTypeByIdCommand { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.ItemTypes.UpdateItemType)]
        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateMailItemTypeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

    }
}
