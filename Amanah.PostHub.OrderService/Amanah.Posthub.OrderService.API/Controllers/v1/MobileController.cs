﻿using Amanah.Posthub.OrderService.Application.Features.Address.Queries;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Queries;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries;
using Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries;
using Amanah.Posthub.OrderService.Application.Shared.ValidationsHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [FeatureGate(FeatureManagementFlags.Concierge)]
    public class MobileController : BaseApiController
    {
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IValidationsUploadFiles _validationsUploadFiles;
        public MobileController(IUploadFormFileService uploadFormFileService, IValidationsUploadFiles validationsUploadFiles)
        {
            _uploadFormFileService = uploadFormFileService;
            _validationsUploadFiles = validationsUploadFiles;
        }

        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Mobile.MailItemTracking)]
        [HttpGet("GeMailItemStatusLogByItemCode/{Code}")]
        public async Task<IActionResult> GeMailItemStatusLogByItemCode(string Code)
        {
            return Ok(await Mediator.Send(new GetMailItemStatusLogsByItemCodeForMobileQuery() { Code = Code }));
        }

        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Mobile.OrderTracking)]
        [HttpGet("GeOrderStatusLogsByOrderCode/{Code}")]
        public async Task<IActionResult> GeOrderStatusLogsByOrderCode(string Code)
        {
            return Ok(await Mediator.Send(new GetOrderStatusLogsByOrderCodeForMobileQuery() { Code = Code }));
        }

        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Mobile.OrdersList)]
        [HttpPost("GetAllOrdersPagginated")]
        public async Task<IActionResult> GetAllOrdersPagginatedAsync([FromBody] GetAllOrdersPagginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("GetMailItemById/{id}")]
        public async Task<IActionResult> GetMailItemById(int id)
        {
            return Ok(await Mediator.Send(new GetMailItemByIdQuery { Id = id }));
        }

        [HttpGet("GetOrderById/{id}")]
        public async Task<IActionResult> GetOrderById(int id)
        {
            return Ok(await Mediator.Send(new GetBusinessOrderByIdForMobileQuery { Id = id }));
        }

        [HttpPost("GetMailItemsByOrderId")]
        public async Task<IActionResult> GetMailItemsByOrderId(GetMailItemListByOrderIdForMobileQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [AllowAnonymous]
        [HttpGet("GetAddressData")]
        public async Task<IActionResult> GetAddressData()
        {
            return Ok(await Mediator.Send(new GetAddressDataQuery ()));
        }

        [HttpPost("CreateOrder")]
        public async Task<IActionResult> CreateOrder([FromForm] CreateBusinessOrderForMobileCommand command)
        {
            if (command.OrderFiles != null && command.OrderFiles.Count > 0)
            {
                _validationsUploadFiles.ValidateAllFilesSize(command.OrderFiles, 10);

                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesAsync(command.OrderFiles, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImagesURLs = ProofOfDeliveryResult.ReturnData.ToList();
            }
          
            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            } 
        }

        [HttpPost("CreateOrderFromBody")]
        public async Task<IActionResult> CreateOrderFromBody([FromBody] CreateBusinessOrderForMobileCommand command)
        {
            if (command.OrderFilesBase64 != null && command.OrderFilesBase64.Count > 0)
            {
                _validationsUploadFiles.ValidateAllFilesSize(command.OrderFilesBase64, 10);

                var ProofOfDeliveryResult = await _uploadFormFileService.UploadFilesFromListOfBase64Async(command.OrderFilesBase64, "UploadFile/OrderFiles");
                if (ProofOfDeliveryResult.IsSucceeded && ProofOfDeliveryResult.ReturnData.Count > 0)
                    command.ImagesURLs = ProofOfDeliveryResult.ReturnData.ToList();
            }

            try
            {
                return Ok(await Mediator.Send(command));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet("GetOrderTransactions/{orderId}")]
        public async Task<IActionResult> GetOrderTransactions(int orderId)
        {
            return Ok(await Mediator.Send(new GetOrderTransactionsQuery { OrderId = orderId}));
        }

        //[HttpPost("CreateOrderConcierge")]
        //public async Task<IActionResult> CreateOrderConcierge(CreateBusinessOrderConciergeForMobileCommand command)
        //{
        //    return Ok(await Mediator.Send(command));
        //}

        [HttpPost("Repay")]
        public async Task<IActionResult> RepayAsync(PayBusinessOrderAgainCommand command)
        {
            if (command.Id < 0)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetTermsAndConditionsByNameForMobile/{name}")]
        public async Task<IActionResult> GetTermsAndConditionsByNameForMobile(string name)
        {
            return Ok(await Mediator.Send(new GetTermsAndConditionsByNameQuery { Name = name }));
        }

    }
}
