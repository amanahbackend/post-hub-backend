﻿using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class OrderRecurencyController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Create(OrderRecurencyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveOrderRecurency")]
        public async Task<IActionResult> ActiveInActiveOrderRecurency(ActivateOrderRecurencyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetOrderRecurencyList()));
        }
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveOrderRecurencyList()));
        }
        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetOrderRecurencyByIdQuery { Id = id }));
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteOrderRecurencyCommand { Id = id }));
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateOrderRecurencyCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
