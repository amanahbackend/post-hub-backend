﻿using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class OrderStatusController : BaseApiController
    {
        [HttpPost("Create")]
        public async Task<IActionResult> Create(OrderStatusCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveOrderStatus")]
        public async Task<IActionResult> ActiveInActiveOrderStatus(ActivateOrderStatusCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetOrderStatusListQuery()));
        }
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveOrderStatusQuery()));
        }
        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetOrderStatusByIdQuery { Id = id }));
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteOrderStatusCommand { Id = id }));
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateOrderStatusCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
