﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Orders.Orders_ViewOrders)]
    public class OrderStatusLogController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.OrdersTracking.OrderTracking)]
        [HttpGet("GeOrderStatusLogsByOrderCode/{Code}")]
        public async Task<IActionResult> GeOrderStatusLogsByOrderCode(string Code)
        {
            return Ok(await Mediator.Send(new GetOrderStatusLogsByOrderCodeQuery() { Code = Code }));
        }
    }
}
