﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Amanah.Posthub.BLL.Authorization;

using Utilites.UploadFile;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries;
using System.IO;
using Utilities.Utilites.GenericListToExcel;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class PackingPackageController : BaseApiController
    {

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PackingPackage.CreatePackingPackage)]

        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreatePackingPackageInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PackingPackage.ListPackingPackage)]

        [HttpPost("GetPackingPackagePaginatedList")]
        public async Task<IActionResult> GetPackingPackagePaginatedList(GetPackingPackagePaginatedListQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpGet("GetPackingPackageById/{id}")]
        public async Task<IActionResult> GetPackingPackageById(int id)
        {
            return Ok(await Mediator.Send(new GetPackingPackageByIdQuery {Id=id }));
        }

        [HttpGet("GetPackagesByCountryId/{CountryId}")]
        public async Task<IActionResult> GetPackagesByCountryId(int CountryId)
        {
            return Ok(await Mediator.Send(new GetPackagesByCountryIdQuery { CountryId = CountryId }));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PackingPackage.DeletePackingPackage)]

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeletePackingPackageInputDTO { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PackingPackage.UpdatePackingPackage)]

        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdatePackingPackageInputDTO command)
        {
            if (command == null)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetPackingPackageByCountryIdForMobile/{countryId}")]
        public async Task<IActionResult> GetPackingPackageByCountryIdForMobile(int countryId)
        {
            return Ok(await Mediator.Send(new GetPackingPackageByCountryIdForMobileQuery { CountryId= countryId }));
        }

      
        [HttpGet("GetCountriesHavePackages")]
        public async Task<ActionResult> GetCountriesHavePackagesAsync()
        {            
            return Ok(await Mediator.Send(new GetCountriesHavePackagesQuery { }));
        }

        [HttpGet("GetCountriesHavePackagesForMobile")]
        public async Task<ActionResult> GetCountriesHavePackagesForMobileAsync()
        {
            return Ok(await Mediator.Send(new GetCountriesHavePackagesForMobileQuery { }));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PackingPackage.ExportToExcelPackingPackage)]
        [HttpGet("ExportToExcel")]
        public async Task<ActionResult> ExportToExcel([FromQuery] GetAllPackingPackagesForExportQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "PackingPackage.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }


    }
}
