﻿using Amanah.PaymentIntegrations.DTOs;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Amanah.PaymentIntegrations.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.FeatureManagement.Mvc;
using System.Threading.Tasks;
using Utilites.UploadFile;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class PaymentsController : BaseApiController
    {
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IPaymentService _paymentService;
        private readonly IConfiguration _configuration;
        public PaymentsController(IUploadFormFileService uploadFormFileService,
            IPaymentService paymentService, IConfiguration configuration
            )
        {
             _paymentService = paymentService;
            _uploadFormFileService = uploadFormFileService;
            _configuration = configuration;
        }
        [HttpGet("Test")]
        public async Task<IActionResult> GetTest()
        {
            var response = await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
            {
                ReferenceId = "160",
                TotalPrice = 50
            });
            return Ok(response);
        }

        [HttpPost("PostPayment")]
        public async Task<IActionResult> PostPaymentAsync([FromBody] RequestPaymentRequestDTO model)
        {
            var response = await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
            {
                ReferenceId = model.ReferenceId,
                TotalPrice = model.TotalPrice
            });
            return Ok(response);
        }

        [HttpGet("GetByRferenceId/{referenceId}")]
        public async Task<IActionResult> GetByRferenceId(int referenceId)
        {
            var result = await _paymentService.GetPaymentTransactionAsync(referenceId.ToString());
            return Ok(result);
        }
        [HttpGet("Success")]
        public async Task<IActionResult> Success(string payment_type, string PaymentID, string Result, string OrderID, string TranID, string TrackID, string PostDate, string Ref, string Auth)
        {
           await _paymentService.EditPaymentTransactionResponseAsync(new UpPaymentsTransactionResponseDTO
            {
                IsSuccess = true,
                PaymentType = payment_type,
                PaymentId = PaymentID,
                PaymentResult = Result,
                OrderId = OrderID,
                TransactionId = TranID,
                TrackId = TrackID,
                PostDate = PostDate,
                TransactionReferenceId = Ref,
                Auth = Auth,
            });
            var basePaymentCallbackURL = _configuration.GetSection("UpPaymentsConfiguration")["PaymentFailOrSuccessBaseCallbackURL"];
            return Redirect(basePaymentCallbackURL + "/#/create-business-order/payment-transaction-result");
        }
        [HttpGet("Fail")]
        public async Task<IActionResult> Fail(string payment_type, string PaymentID, string Result, string OrderID, string TranID, string TrackID, string PostDate, string Ref, string Auth)
        {
            await _paymentService.EditPaymentTransactionResponseAsync(new UpPaymentsTransactionResponseDTO
            {
                IsSuccess = false,
                PaymentType = payment_type,
                PaymentId = PaymentID,
                PaymentResult = Result,
                OrderId = OrderID,
                TransactionId = TranID,
                TrackId = TrackID,
                PostDate = PostDate,
                TransactionReferenceId = Ref,
                Auth = Auth,
            });
            var basePaymentCallbackURL = _configuration.GetSection("UpPaymentsConfiguration")["PaymentFailOrSuccessBaseCallbackURL"];
            return Redirect(basePaymentCallbackURL + "/#/create-business-order/payment-transaction-result");
        }
        [HttpGet("Notify")]
        public async Task<IActionResult> Notify(string payment_type, string PaymentID, string Result, string OrderID, string TranID, string TrackID, string PostDate, string Ref, string Auth)
        {
            await _paymentService.EditPaymentTransactionResponseAsync(new UpPaymentsTransactionResponseDTO
            {
                IsSuccess = false,
                PaymentType = payment_type,
                PaymentId = PaymentID,
                PaymentResult = Result,
                OrderId = OrderID,
                TransactionId = TranID,
                TrackId = TrackID,
                PostDate = PostDate,
                TransactionReferenceId = Ref,
                Auth = Auth,
            });
            var basePaymentCallbackURL = _configuration.GetSection("UpPaymentsConfiguration")["PaymentFailOrSuccessBaseCallbackURL"];
            return Redirect(basePaymentCallbackURL + "/#/create-business-order/payment-transaction-result");
        }

    }
}
