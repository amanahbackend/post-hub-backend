﻿
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    public class PriceListController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceList.DeletePriceList)]

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeletePLInputDTO { Id = id }));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceList.UpdatePriceList)]

        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdatePLInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceList.AddPriceList)]

        [HttpPost("Create")]
        public async Task<IActionResult> CreateAsync([FromBody] CreatePLInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceList.ReadPriceList)]

        [HttpPost("GetAllPagginated")]
        public async Task<IActionResult> GetAllPagginatedAsync([FromBody] GetAllPLPagginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceList.ReadPriceList)]

        [HttpPost("GetFilteredPLPagginated")]
        public async Task<IActionResult> GetFilteredPLPagginated([FromBody] GetFilteredPLPagginatedQuery filter)
        {
            return Ok(await Mediator.Send(filter));
        }

        //[AllowAnonymous]
        [HttpGet("GetLocalDeliveryPL")]
        public async Task<IActionResult> GetLocalDeliveryPLAsync()
        {
            return Ok(await Mediator.Send(new GetLocalDeliveryPLQuery()));
        }

        [HttpGet("GetById/{Id}")]
        public async Task<IActionResult> GetByIdAsync(int Id)
        {
            return Ok(await Mediator.Send(new PLDetailsQuery() { Id = Id }));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceList.ExportPriceList)]
        [Route("ExportToExcel")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcel([FromQuery] ExportPLDataQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "PriceLists.xlsx";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }

    }
}
