﻿using Amanah.Posthub.OrderService.Application.Features.Lookups.PriceListStatus.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class PriceListStatusController : BaseApiController
    {
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetPLStatusListQuery()));
        }

    }
}
