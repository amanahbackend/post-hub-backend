﻿
using Amanah.Posthub.OrderService.Application.Features.Lookups.PriceQuotationStatus.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    public class PriceQuotationStatusController : BaseApiController
    {
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetPQStatusListQuery()));
        }

    }
}
