﻿
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]

    [ApiVersion("1.0")]
    public class RFQController : BaseApiController
    {
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceQoutation.DeletePriceQoutation)]

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteRFQInputDTO { Id = id }));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceQoutation.UpdatePriceQoutation)]
        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateRFQDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceQoutation.AddPriceQoutation)]
        [HttpPost("Create")]
        public async Task<IActionResult> CreateAsync([FromBody] CreateRFQInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.PriceQoutation.ReadPriceQoutation)]

        [HttpPost("GetAllPagginated")]
        public async Task<IActionResult> GetAllPagginatedAsync([FromBody] GetAllRFQPagginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }


        [HttpGet("GetById/{Id}")]
        public async Task<IActionResult> GetByIdAsync(int Id)
        {
            return Ok(await Mediator.Send(new RFQDetailsQuery() { Id = Id }));
        }

        [HttpGet("GetAllRFQCuntries")]
        public async Task<IActionResult> GetAllPQsCuntriesAsync()
        {
            return Ok(await Mediator.Send(new GetAllRFQCuntriesQuery()));
        }


        [HttpGet("GetInternationalPostItemsByPQId/{Id}")]
        public async Task<IActionResult> GetInternationalPostItemsByPQId(int Id)
        {
            return Ok(await Mediator.Send(new InternationalRFQDetailsQuery() { Id = Id }));
        }


        [HttpGet("GetRFQItemsByRFQId/{id}")]
        public async Task<IActionResult> GetRFQItemsByRFQId(int id)
        {
            return Ok(await Mediator.Send(new GetRFQItemsByRFQIdQuery() { RFQId = id }));
        }

        [HttpGet("GetRFQList")]
        public async Task<IActionResult> GetRFQListAsync()
        {
            return Ok(await Mediator.Send(new GetRFQListQuery()));
        }
        [HttpGet("GetRFQItemsByBusinessCustomerId/{businessCustomerId}")]
        public async Task<IActionResult> GetRFQItemsByBusinessCustomerIdAsync(int businessCustomerId)
        {
            return Ok(await Mediator.Send(new GetRFQItemsByBusinessCustomerIdQuery() { BusinessCustomerId = businessCustomerId }));
        }


        [AllowAnonymous]
        [Route("PrintPQ/{id}")]
        [HttpGet]
        //[Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<ActionResult> PrintContract(int id)
        {
            var data = await Mediator.Send(new GetPQAsPDF() { Id = id });
            return File(data, "application/pdf", "PQ.pdf");
        }

        #region GeneralOffer
        [HttpPost("CreatePQWithGeneralOffer")]
        public async Task<IActionResult> CreatePQWithGeneralOfferAsync([FromBody] CreateRFQWithGeneralOfferInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }
        
            [HttpPut("UpdatePQWithGeneralOffer")]
        public async Task<ActionResult> UpdatePQWithGeneralOfferAsync([FromBody] UpdateRFQWithGeneralOfferDTO command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetGeneralOffersWithSectors")]
        public async Task<IActionResult> GetGeneralOffersWithSectors()
        {
            return Ok(await Mediator.Send(new GetGenealOffersWithSectorsQuery() {  }));
        }
        [HttpGet("GetRFQItemsOfGeneralOfferByServiceSectorId/{serviceSectorId}")]
        public async Task<IActionResult> GetRFQItemsOfGeneralOfferByServiceSectorIdAsync(int serviceSectorId)
        {
            return Ok(await Mediator.Send(new GetRFQItemsOfGeneralOfferByServiceSectorIdQuery() { ServiceSectorId = serviceSectorId }));
        }
        #endregion

    }
}
