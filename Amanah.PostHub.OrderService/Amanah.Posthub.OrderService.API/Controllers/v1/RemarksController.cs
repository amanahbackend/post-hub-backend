﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.Remarks.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Remarks.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    public class RemarksController : BaseApiController
    {
        private readonly ILocalizer _localizer;


        public RemarksController(ILocalizer localizer)
        {
            _localizer = localizer;
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Remarks.CreateRemark)]
        [HttpPost("Create")]
        public async Task<IActionResult> Create(CreateRemarkInputDTO command)
        {
           
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Remarks.ReadRemark)]

        [HttpPost("GetAll")]
        public async Task<IActionResult> GetAll(GetRemarkPaginatedListQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Remarks.DeleteRemark)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteRemarkInputDTO { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Remarks.UpdateRemark)]
        [HttpPut("Update")]
        public async Task<IActionResult> Update(UpdateRemarkInputDTO command)
        {
            if (command.Id < 0)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetActiveRemarkList")]
        public async Task<IActionResult> GetActiveRemarkList()
        {
            return Ok(await Mediator.Send(new GetActiveRemarkListQuery()));
        }
        
    }
}
