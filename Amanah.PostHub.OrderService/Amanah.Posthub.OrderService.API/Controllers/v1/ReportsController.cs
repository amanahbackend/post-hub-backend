﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries;
using Amanah.Posthub.SharedKernel.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class ReportsController : BaseApiController
    {


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Reports.LocalMessageDistributionReport)]
        [HttpPost("GetLocalMessageDistributionReport")]
        public async Task<IActionResult> GetLocalMessageDistributionReportAsync(GetLocalMessageDistributionReportQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Reports.LocalMessageDistributionReport)]
        [HttpPost("GetLocalMessageDistributionsReport")]
        public async Task<IActionResult> GetLocalMessageDistributionsReportAsync(GetLocalMessageDistributionsReportQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Reports.LocalMessageDistributionReport)]
        [Route("ExportLocalMessageDistributionReportToExcel")]
        [HttpGet]
        public async Task<ActionResult> ExportLocalMessageDistributionReportToExcel([FromQuery] GetAllLocalMessageDistributionReportForExcelQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "LocalMessageDistributionReport.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Reports.BusinessCustomerAndBranchesReport)]
        [HttpPost("GetBusinessCustomerAndItsBranches")]
        public async Task<IActionResult> GetBusinessCustomerAndItsBranches(BusinessCustomerAndItsBranchesQuery command)
        {
            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Reports.BusinessCustomerAndBranchesReport)]
        [Route("BusinessCustomerAndItsBranchesForExport")]
        [HttpGet]
        public async Task<ActionResult> BusinessCustomerAndItsBranchesForExport([FromQuery] BusinessCustomerAndItsBranchesForExportQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "BusinessCustomerAndItsBranches.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }



        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Reports.BusinessCustomerDistributionReport)]
        [HttpPost("GetBusinessCustomerDistributionReport")]
        public async Task<IActionResult> GetBusinessCustomerDistributionReport(GetBusinessCustomerDistributionReportQuery command)
        {
            return Ok(await Mediator.Send(command));
        }



        [HttpPost("GetBusinessCustomerDistributionCountReport")]
        public async Task<IActionResult> GetBusinessCustomerDistributionCountReport(GetBusinessCustomerDistributionCountReport command)
        {
            return Ok(await Mediator.Send(command));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Reports.BusinessCustomerDistributionReport)]
        [Route("GetBusinessCustomerDistributionForExportReport")]
        [HttpGet]
        public async Task<ActionResult> GetBusinessCustomerDistributionForExportReport([FromQuery] GetBusinessCustomerDistributionForExportReportQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "BusinessCustomerDistribution.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }

        [AllowAnonymous]
        [HttpPost("GetBusinessCustomerDeliveredItemsReportToEachBranch")]
        public async Task<IActionResult> GetBusinessCustomerDeliveredItemsReportToEachBranchAsync(GetBusinessCustomerDeliveredItemsReportToEachBranch command)
        {
            return Ok(await Mediator.Send(command));
        }

        [AllowAnonymous]
        [HttpPost("GetBusinessCustomerDeliveredItemsPagedReportToEachBranch")]
        public async Task<IActionResult> GetBusinessCustomerDeliveredItemsPagedReportToEachBranchAsync(GetBusinessCustomerDeliveredItemsPagedReportToEachBranch command)
        {
            return Ok(await Mediator.Send(command));
        }

        [AllowAnonymous]
        [Route("testPermisions")]
        [HttpGet]
        public async Task<ActionResult> testPermisions([FromQuery] GetBusinessCustomerDistributionForExportReportQuery query)
        {
            var permissions = BsinessCustomerPermissionPovider.GetDafaultPermissions();
            return Ok(permissions);
        }

        [AllowAnonymous]
        [HttpPost("GetTotalItemsPerBranches")]
        public async Task<IActionResult> GetTotalItemsPerBranches(BusinessCustomerBranchesWithTotalItemsQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        [AllowAnonymous]
        [HttpPost("GetDistributedItemsPerBranches")]
        public async Task<IActionResult> GetDistributedItemsPerBranches(DistriputedItemsForEachBranchWithTotalItemsQuery command)
        {
            return Ok(await Mediator.Send(command));
        }

        

    }
}
