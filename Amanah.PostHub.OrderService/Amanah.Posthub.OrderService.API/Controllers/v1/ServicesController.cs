﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.Services.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Services.Qureies;
using Utilites.UploadFile;
using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using MediatR;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ServicesController : BaseApiController
    {
        private readonly IUploadFormFileService _uploadFormFileService;

        public ServicesController(IUploadFormFileService uploadFormFileService)
        {
            _uploadFormFileService = uploadFormFileService;
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Services.CreateService)]

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromForm] CreateServiceInputDTO command)
        {
            if (command.PhotoUrlFile != null && command.PhotoUrlFile.Length > 0)
            {
                var uploadSignatureResult = await _uploadFormFileService.UploadFileAsync(command.PhotoUrlFile, "UploadFile/ServiceFile");
                if (!string.IsNullOrEmpty(uploadSignatureResult.ReturnData))
                {
                    command.PhotoUrl = uploadSignatureResult.ReturnData;
                }
            }

            return Ok(await Mediator.Send(command));
        }

       [Authorize(AuthenticationSchemes = "Bearer")]
        //, Policy = TenantPermissions.Services.ListService) removed by request of Adam

        [HttpGet("GetAllServices")]
        public async Task<IActionResult> GetAllServices()
        {
            return Ok(await Mediator.Send(new GetAllServicesQuery()));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Services.ListService)]

        [HttpPost("GetServicePagginatedList")]
        public async Task<IActionResult> GetServicePagginatedList(GetServicesPaginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Services.DeleteService)]

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteServiceInputDTO { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Services.UpdateService)]

        [HttpPut("Update")]
        public async Task<IActionResult> Update([FromForm] UpdateServcieInputDTO command)
        {
            if (command == null)
            {
                return BadRequest();
            }

            if (command.PhotoUrlFile != null && command.PhotoUrlFile.Length > 0)
            {
                var uploadSignatureResult = await _uploadFormFileService.UploadFileAsync(command.PhotoUrlFile, "UploadFile/ServiceFile");
                if (!string.IsNullOrEmpty(uploadSignatureResult.ReturnData))
                {
                    command.PhotoUrl = uploadSignatureResult.ReturnData;
                }
            }
            return Ok(await Mediator.Send(command));
        }

        [HttpGet("GetAllServicesForMobile")]
        public async Task<IActionResult> GetAllServicesForMobile()
        {
            return Ok(await Mediator.Send(new GetAllServicesForMobileQuery()));
        }
    }

}
