﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TermsAndConditionsController : BaseApiController
    {

        [HttpGet("GetTermsAndConditionsById/{id}")]
        public async Task<IActionResult> GetTermsAndConditionsById(int id)
        {
            return Ok(await Mediator.Send(new GetTermsAndConditionsByIdQuery { Id = id }));
        }

        [HttpGet("GetTermsAndConditionsByName/{name}")]
        public async Task<IActionResult> GetTermsAndConditionsByName(string name)
        {
            return Ok(await Mediator.Send(new GetTermsAndConditionsByNameQuery { Name = name }));
        }

        [HttpGet("GetFirstTermsAndConditions")]
        public async Task<IActionResult> GetFirstTermsAndConditions()
        {
            return Ok(await Mediator.Send(new GetFirstTermsAndConditionsQuery()));
        }

        [HttpPost("GetTermsAndConditionsPaginated")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.TermsAndConditions.ListTermsAndConditions)]
        public async Task<IActionResult> GetTermsAndConditionsPaginated(GetTermsAndConditionsPaginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost("Create")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.TermsAndConditions.CreateTermsAndConditions)]
        public async Task<IActionResult> Create(CreateTermsAndConditionsCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
         
        [HttpPut("Update")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.TermsAndConditions.UpdateTermsAndConditions)]
        public async Task<IActionResult> Update(UpdateTermsAndConditionsCommand command)
        {
            if (command == null)
                return BadRequest();
            
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("Delete/{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.TermsAndConditions.DeleteTermsAndConditions)]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteTermsAndConditionsCommand { Id = id }));
        }
        
    }
}
