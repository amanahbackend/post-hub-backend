﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.WeightUOM.Commands;
using Amanah.Posthub.OrderService.Application.Features.Lookups.WeightUOM.Queries;
using Amanah.Posthub.SharedKernel.LoggingService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiVersion("1.0")]
    public class WeightUOMController : BaseApiController
    {
        private readonly ILoggerManager _logger;
        public WeightUOMController()
        {
            _logger = LoggerManager.GetLoggerInstance();
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WeightUOMs.CreateWeightUOM)]
        [HttpPost("Create")]
        public async Task<IActionResult> Create(WeightUOMCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("ActiveInActiveWeightUOM")]
        public async Task<IActionResult> ActiveInActiveWeightUOM(ActivateWeightUOMCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpGet("GetAll")]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await Mediator.Send(new GetWeightUOMListQuery()));
        }
        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WeightUOMs.ListWeightUOMs)]
        [AllowAnonymous]
        [HttpGet("GetAllActive")]
        public async Task<IActionResult> GetAllActive()
        {
            return Ok(await Mediator.Send(new GetActiveWeightUOMListQuery()));
        }
        
        
        
        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            return Ok(await Mediator.Send(new GetWeightUOMByIdQuery { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WeightUOMs.DeleteWeightUOM)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteWeightUOMByIdCommand { Id = id }));
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WeightUOMs.UpdateWeightUOM)]
        [HttpPut("[action]")]
        public async Task<IActionResult> Update(int id, UpdateWeightUOMCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }
    }
}
