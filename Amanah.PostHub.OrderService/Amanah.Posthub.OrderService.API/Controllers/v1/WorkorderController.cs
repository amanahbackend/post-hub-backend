﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.OrderService.API.UploadFile;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries;
using Amanah.Posthub.OrderService.Application.Shared.ValidationsHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Utilites.GenericListToExcel;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.OrderService.API.Controllers.v1
{
    [ApiVersion("1.0")]
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [FeatureGate(FeatureManagementFlags.Concierge)]
    public class WorkorderController : BaseApiController
    {

        private readonly IUploadBase64Service _uploadBase64Service;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IHostingEnvironment _env;
        private readonly IValidationsUploadFiles _validationsUploadFiles;
        public WorkorderController(IHostingEnvironment env,
             IUploadBase64Service uploadBase64Service,
             IUploadFormFileService uploadFormFileService,
             IValidationsUploadFiles validationsUploadFiles)
        {
            _uploadBase64Service = uploadBase64Service;
            _env = env;
            _uploadFormFileService = uploadFormFileService;
            _validationsUploadFiles = validationsUploadFiles;
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WorkOrders.WorkOrders_DeleteOrder)]
        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteWorkOrderInputDTO { Id = id }));
        }



        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WorkOrders.WorkOrders_UpdateOrder)]
        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateWorkOrderDTO command)
        {
            return Ok(await Mediator.Send(command));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WorkOrders.WorkOrders_CreateWorkOrder)]
        [HttpPost("Create")]
        public async Task<IActionResult> CreateAsync([FromBody] CreateWorkOrderInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }



        [HttpPost("GetAllPagginated")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.WorkOrders.WorkOrders_ViewOrders)]
        public async Task<IActionResult> GetAllPagginatedAsync([FromBody] GetAllWorkOrdersPagginatedQuery query)
        {
            return Ok(await Mediator.Send(query));
        }


        [HttpPost("WorkorderForDispatch")]
        public async Task<IActionResult> WorkorderForDispatch(QueryListReq queryListReq)
        {
            return Ok(await Mediator.Send(new WorkorderForDispatchQuery()
            {
                PageNumber = queryListReq.PageNumber,
                PageSize = queryListReq.PageSize
            }));
        }
        [HttpGet("GetById/{Id}")]
        public async Task<IActionResult> GetByIdAsync(int Id)
        {
            return Ok(await Mediator.Send(new WorkOrderDetailsQuery() { Id = Id }));
        }



        [Route("[action]")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcel([FromQuery] GetAllWorkOrderForExportQuery query)
        {
            var data = await Mediator.Send(query);
            var path = "ExportedTemplates";
            var fileName = "WorkOrders.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }

        [HttpPost("AssignDriverToWorkOrder")]
        public async Task<IActionResult> AssignDriverToWorkOrderAsync(AssignWorkOrderToDriverInputDTO query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost("AutoAssignWorkOrders")]
        public async Task<IActionResult> AssignDriverToWorkOrderAutoAssignWorkOrdersAsync(AutoAssignWorkOrderToDriverInputDTO query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost("DispatchDriverToWorkOrder")]
        public async Task<IActionResult> DispatchDriverToWorkOrderAsync(DispatchWorkOrderToDriverInputDTO query)
        {
            return Ok(await Mediator.Send(query));
        }

        #region MobileApis

        [HttpPost("GetAllActiveWorkOrdersByDriverIdForMobile")]
        public async Task<IActionResult> GetAllWorkOrdersByDriverIdForMobileAsync(GetAllActiveWorkOrdersByDriverIdForMobileQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpGet("GetWorkOrderBydForMobile/{id}")]
        public async Task<IActionResult> GetWorkOrderBydForMobileAsync(int id)
        {
            return Ok(await Mediator.Send(new GetWorkOrderBydForMobileQuery() { Id = id }));
        }
        [HttpPost("GeAllMailItemByWorkOrderIdForMobile")]
        public async Task<IActionResult> GeMailItemByWorkOrderIdForMobileAsync(GeMailItemByWorkOrderIdForMobileQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpPost("GetAllMailItemByWorkOrderIdForMobile")]
        public async Task<IActionResult> GeAllMailItemByWorkOrderIdForMobileAsync(GetAllMailItemsByWorkOrderIdForMobileQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpPut("UpdateMailItemForMobile")]
        public async Task<ActionResult> UpdateMailItemAsync([FromForm] UpdateMailItemInputDto command)
        {
            command.ProofOfDeliveryImageURLs = new System.Collections.Generic.List<string>();

            if (command.PhotosFiles != null && command.PhotosFiles.Count > 0)
                foreach (var file in command.PhotosFiles.Where(ProofOfDeliveryFile => ProofOfDeliveryFile.Length > 0))
                {
                    var ProofOfDeliveryResult = await _uploadFormFileService.UploadFileAsync(file, "MailItemImages/ProveOfDelivery");
                    if (ProofOfDeliveryResult.IsSucceeded && !string.IsNullOrEmpty(ProofOfDeliveryResult.ReturnData))
                    {
                        command.ProofOfDeliveryImageURLs.Add(ProofOfDeliveryResult.ReturnData);
                    }
                }

            if (command.SignatureFile != null && command.SignatureFile.Length > 0)
            {
                var uploadSignatureResult = await _uploadFormFileService.UploadFileAsync(command.SignatureFile, "MailItemImages/Signature");
                if (!string.IsNullOrEmpty(uploadSignatureResult.ReturnData))
                {
                    command.SignatureImageURL = uploadSignatureResult.ReturnData;
                }
            }

            return Ok(await Mediator.Send(command));


        }


        [HttpGet("GeMailItemByIdForMobile/{id}")]
        public async Task<IActionResult> GeMailItemByIdForMobileAsync(int id)
        {
            return Ok(await Mediator.Send(new GeMailItemByIdForMobileQuery() { Id = id }));
        }
        [HttpGet("GetAllMailItemStatusForMobile")]
        public async Task<IActionResult> GetAllMailItemStatusForMobileAsync()
        {
            return Ok(await Mediator.Send(new GetAllMailItemStatusForMobileQuery()));
        }
        [HttpPost("GetAllAssignedWorkOrdersByDriverIdForMobile")]
        public async Task<IActionResult> GetAllAssignedWorkOrdersByDriverIdForMobileAsync(GetAllAssignedWorkOrdersByDriverIdForMobileQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpPost("GetAllClosedWorkOrdersByDriverIdForMobile")]
        public async Task<IActionResult> GetAllClosedWorkOrdersByDriverIdForMobileAsync(GetAllClosedWorkOrdersByDriverIdForMobileQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost("ReturnMailItemForMobil")]
        public async Task<IActionResult> ReturnMailItemForMobileAsync(ReturnMailItemDTO command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("PickupMailItemForMobil")]
        public async Task<IActionResult> PickupMailItemForMobil(PickupMailItemDTO command) 
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("PickupMailItemInternationalOrdersForMobile")]
        public async Task<IActionResult> PickupMailItemInternationalOrdersForMobile([FromForm]PickupMailItemForMobileDTO command)
        {
            if (command.PickupFiles != null && command.PickupFiles.Count > 0)
            {
                _validationsUploadFiles.ValidateAllFilesSize(command.PickupFiles, 10);

                var uploadPickupFilesResult = await _uploadFormFileService.UploadFilesAsync(command.PickupFiles, "MailItemImages/ProveOfDelivery");
                if (uploadPickupFilesResult.IsSucceeded && uploadPickupFilesResult.ReturnData.Count > 0)
                    command.PickupImgsUrls = uploadPickupFilesResult.ReturnData.ToList();
            }
            if (command.SignatureFile != null && command.SignatureFile.Length > 0)
            {
                _validationsUploadFiles.ValidateFileSize(command.SignatureFile);

                var uploadSignatureResult = await _uploadFormFileService.UploadFileAsync(command.SignatureFile, "MailItemImages/Signature");
                if (uploadSignatureResult.IsSucceeded && !string.IsNullOrEmpty(uploadSignatureResult.ReturnData))
                    command.SignatureImgUrl = uploadSignatureResult.ReturnData;
            }

            return Ok(await Mediator.Send(command)); 
        }

        [Route("UploadPhotoForMobile")]
        [HttpPost]
        public async Task<IActionResult> UploadPhotoAsync([FromForm] DriverPhotoDTO driverPhotoDTO)
        {


            var mailItem = await Mediator.Send(new GeMailItemByIdForMobileQuery() { Id = driverPhotoDTO.Id });
            if (driverPhotoDTO.photo != null)//Image Base64
            {
                var path = "DriverImages";
                var processResult = await _uploadFormFileService.UploadFileAsync(driverPhotoDTO.photo, path);
                if (processResult.IsSucceeded)
                {
                    mailItem.ProofOfDeliveryImage.Path = processResult.ReturnData;
                }
                else
                {
                    return BadRequest(processResult.Exception);
                }
            }
            var res = await Mediator.Send(new UpdateMailItemInputDto()
            {
                Id = mailItem.Id,
                // DeliveryBefore= mailItem.DeliveryBefore,
                DropOffDate = mailItem.DeliveryBefore,
                // ItemReturnReasonId= mailItem.ItemReturnReasonId,
                // OtherItemReturnReason= mailItem.OtherItemReturnReason,
                StatusId = mailItem.ItemstatusId,
                MailItemBarCode = mailItem.MailItemBarCode,
                Notes = mailItem.Notes,
                //ProofOfDeliveryImagePath= mailItem.ProofOfDeliveryImage.Path

            });

            return Ok(res);
        }


        [Route("SignaturePhotoForMobile")]
        [HttpPost]
        public async Task<IActionResult> SignaturePhotoAsync([FromForm] DriverPhotoDTO driverPhotoDTO)
        {


            var mailItem = await Mediator.Send(new GeMailItemByIdForMobileQuery() { Id = driverPhotoDTO.Id });
            if (driverPhotoDTO.photo != null)//Image Base64
            {
                var path = "DriverImages";
                var processResult = await _uploadFormFileService.UploadFileAsync(driverPhotoDTO.photo, path);
                if (processResult.IsSucceeded)
                {
                    mailItem.SignatureImage.Path = processResult.ReturnData;
                }
                else
                {
                    return BadRequest(processResult.Exception);
                }
            }
            var res = await Mediator.Send(new UpdateMailItemInputDto()
            {
                Id = mailItem.Id,
                // DeliveryBefore = mailItem.DeliveryBefore,
                DropOffDate = mailItem.DeliveryBefore,
                // ItemReturnReasonId = mailItem.ItemReturnReasonId,
                // OtherItemReturnReason = mailItem.OtherItemReturnReason,
                StatusId = mailItem.ItemstatusId,
                MailItemBarCode = mailItem.MailItemBarCode,
                Notes = mailItem.Notes,
                //  SignatureImageURL = mailItem.SignatureImage.Path

            });

            return Ok(res);
        }
        //[HttpPost("UpdateWorkOrderStatusForMobile")]
        //public async Task<IActionResult> UpdateWorkOrderStatusAsync(UpdateWorkOrderStatusInputDTO command)
        //{
        //    return Ok(await Mediator.Send(command));
        //}

        #endregion

        //[HttpPost("ChangeWorkOrderStatus")]
        //public async Task<IActionResult> ChangeWorkOrderStatusAsync(ChangeWorkOrderStatusDTO query)
        //{
        //    return Ok(await Mediator.Send(query));
        //}
        [HttpGet("GetAllWorkOrderStatuss")]
        public async Task<IActionResult> GetAllWorkOrderStatussAsync()
        {
            return Ok(await Mediator.Send(new GetAllWorkOrderStatussQuery()));
        }

        [HttpPost("WoFromOrders")]
        public async Task<IActionResult> WorkorderFromOrdersAsync(WorkorderFromOrdersDto command)
        {
            return Ok(await Mediator.Send(command));
        }
        [HttpPost("WoFromAreaBlock")]
        public async Task<IActionResult> WorkorderFromAreaBlockAsync(WorkorderFromAreaBlockDto command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("WoByOrderIdAndDriverId")]
        public async Task<IActionResult> CreateWorkorderByOrderIdAndDriverIdAsync(CreateWorkorderByOrderIdAndDriverIdCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("WoFromDrivers")]
        public async Task<IActionResult> WorkorderFromDriversAsync(CreateWorkorderFromDriversCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("ChangeWorkOrderStatus")]
        public async Task<IActionResult> ChangeWorkOrderStatusAsync(ChangeWorkOrderStatusCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPost("GetAssignedInternalWorkordersForMobile")]
        public async Task<IActionResult> GetAssignedInternalWorkordersForMobile(AssignedInternalWorkordersForMobileQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        [HttpPost("GetActiveInternalWorkordersForMobile")]
        public async Task<IActionResult> GetActiveInternalWorkordersForMobile(ActiveInternalWorkordersForMobileQuery query)
        {
            return Ok(await Mediator.Send(query));
        }

        [HttpPost("ReassignWorkOrderToDriverForMobile")]
        public async Task<IActionResult> GetActiveInternalWorkordersForMobile(ReassignWorkOrderToDriverInputDTO command)
        {
            return Ok(await Mediator.Send(command));
        }
        
    }
}
