using Amanah.PaymentIntegrations.Configurations;
using Amanah.PaymentIntegrations.Infrastructure.Configurations;
using Amanah.PaymentIntegrations.Infrastructure.EntityFramework.DBContext;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.Configurations;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.Services;
using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.API.Authorization;
using Amanah.Posthub.BASE;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.UserManagement.Permissions;
using Amanah.Posthub.Infrastructure;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Infrastructure.ExternalServices.Settings;
using Amanah.Posthub.OrderService.API.Localization;
using Amanah.Posthub.OrderService.API.UploadFile;
using Amanah.Posthub.OrderService.Application;
using Amanah.Posthub.OrderService.Application.AutomapperProfiles;
using Amanah.Posthub.OrderService.Application.Shared;
using Amanah.Posthub.Settings;
using Amanah.Posthub.SharedKernel.Common.Reporting;
using Amanah.Posthub.SharedKernel.Infrastructure.Authentication;
using AutoMapper;
using ElmahCore.Mvc;
using Hangfire;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
//

using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;
//added
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Amanah.Posthub.BASE;
using Utilities.Utilites.Exceptions;
using System.Net;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Amanah.Posthub.OrderService.API.Localization;
using System.IO;
using Microsoft.Extensions.FileProviders;

//using Microsoft.Extensions.FileProviders;
//using Microsoft.Extensions.FileProviders.Physical;

using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using System.IO;
using System.Net;
using System.Reflection;
using Utilities.Utilites.Exceptions;
using Wkhtmltopdf.NetCore;
using Microsoft.FeatureManagement;
using static Utilities.Utilites.Localization.Keys;
using Microsoft.FeatureManagement.FeatureFilters;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder.Extensions;
using Amanah.Posthub.OrderService.Application.Shared.ValidationsHelper;
using Amanah.Posthub.SharedKernel.Exceptions;
using NLog;
using Amanah.Posthub.SharedKernel.LoggingService;
using Amanah.Posthub.OrderService.Application.Shared.OrderPayment;

namespace Amanah.Posthub.OrderService.API
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;
        public IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                    name: "AllowOrigin",
                    builder =>
                    {
                        builder.AllowAnyOrigin()
                                .AllowAnyMethod()
                                .AllowAnyHeader();
                    });
            });
            services.AddControllers()
                .AddNewtonsoftJson()
                .AddDataAnnotationsLocalization(options => options.DataAnnotationLocalizerProvider =
                    (type, factory) => factory.Create(typeof(Translations)));
            ;
            services.AddScoped(typeof(IUploadBase64Service), typeof(UploadBase64Service));
            services.AddScoped(typeof(IExecuteStoredProc), typeof(ExecuteStoredProc));
            services.AddSingleton(typeof(IEmailSenderservice), typeof(EmailSenderService));
            services.Configure<EmailSettings>(_configuration.GetSection("EmailSettings"));
            //services.AddScoped(provider => new MapperConfiguration(cfg =>
            //{
            //    cfg.AddProfile(new OrderMapperProfile(provider.GetService<PaymentDatabaseContext>()));
            //}).CreateMapper());
            services.AddSingleton(typeof(EmailSettings), typeof(EmailSettings));

            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            ConfigureAuthService(services);

            services.AddTransient<ILocalizer, Localizer>();
            services.AddScoped<IRDLCrenderService, RDLCrenderService>();

            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
           // services.AddSingleton<ILoggerManager, LoggerManager>();


            #region Swagger
            services.AddSwaggerGen(c =>
            {
                //c.IncludeXmlComments(string.Format(@"{0}\Amanah.Posthub.OrderService.API.xml", System.AppDomain.CurrentDomain.BaseDirectory));
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Amanah.Posthub.OrderService.API", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Enter 'Bearer' [space] and then your valid token in the text input below.\r\n\r\nExample: \"Bearer jknkjhjkhljlhkjhNiIsInR5cCI6IkpXVCJ9\"",
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        }, new string[] {}
                    }
                });
            });
            #endregion
            #region Api Versioning
            // Add API Versioning to the Project
            services.AddApiVersioning(config =>
            {
                // Specify the default API Version as 1.0
                config.DefaultApiVersion = new ApiVersion(1, 0);
                // If the client hasn't specified the API version in the request, use the default API version number 
                config.AssumeDefaultVersionWhenUnspecified = true;
                // Advertise the API versions supported for the particular endpoint
                config.ReportApiVersions = true;
            });
            #endregion
           
            
            services.ConfigurePayment(_configuration, "DefaultConnection");
            //services.AddMvc(c => c.Conventions.Add(new ApiExplorerIgnores()));
            services.AddScoped(typeof(IUploadFormFileService), typeof(UploadFormFileService));
            services.AddScoped(typeof(IValidationsUploadFiles), typeof(ValidationsUploadFiles));
            services.AddScoped(typeof(IOrderPayment), typeof(OrderPayment));


            services.Configure<AppSettings>(_configuration);
            services.AddHttpContextAccessor();
            services.AddCurrentUser();
            services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();
            services.AddAuthorization(options =>
            {
                var allPermissions = TenantPermissions.FlatPermissions
                    .Concat(PlatformPermissions.FlatPermissions)
                    .Concat(AgentPermissions.FlatPermissions)
                    .ToArray();
                foreach (var permission in allPermissions)
                {
                    options.AddPolicy(permission, builder =>
                    {
                        builder.AddRequirements(new PermissionRequirement(permission));
                    });
                }
            });

            // services.AddElmah();

            services.AddApplication(_configuration);

            services.AddPersistence(_configuration, _environment);

            services.AddFeatureManagement();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRequestLocalization(options =>
            {
                var arabicCulture = new CultureInfo("ar");
                arabicCulture.DateTimeFormat.Calendar = new GregorianCalendar();
                var englishCulture = new CultureInfo("en");
                var supportedCultures = new[] { englishCulture, arabicCulture };
                options.DefaultRequestCulture = new RequestCulture(supportedCultures.First());
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
            app.UseCors("AllowOrigin");

            if (_environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(errorAppBuilder =>
                {
                    errorAppBuilder.Run(async context =>
                    {
                        var exception = context.Features.Get<IExceptionHandlerFeature>()?.Error;
                        var localizer = context.RequestServices.GetRequiredService<ILocalizer>();
                        var logger = context.RequestServices.GetRequiredService<ILogger<ExceptionHandlerMiddleware>>();
                        var (content, status) = exception switch
                        {
                            NotFoundException notFoundException => (notFoundException.Message, HttpStatusCode.NotFound),
                            DomainException domainException => (domainException.Message, HttpStatusCode.BadRequest),
                            UnauthorizedAccessException unauthorizedAccessException =>
                                (Keys.Validation.UnAuthorizedAccess, HttpStatusCode.Unauthorized),
                            _ => (Keys.Validation.GeneralError, HttpStatusCode.InternalServerError),
                        };
                        logger.LogCritical(exception, content);
                        if (!context.Response.HasStarted)
                        {
                            context.Response.StatusCode = (int)status;
                            var result =
                                new
                                {
                                    error = localizer[content]
                                };
                            // TODO: move to an extension method WriteJsonAsync
                            context.Response.ContentType = "application/json; charset=UTF-8";
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                            await context.Response.Body.FlushAsync();
                        }
                    });
                });
            }

            // to close all endpoints 
            //app.UseForFeature(FeatureManagementFlags.Concierge, appBuilder =>
            //{
            //    //appBuilder.UseMiddleware<FeatureFlagsMiddleware>();
            //    appBuilder.Use(
            //        async (ctx, next) =>
            //        {
            //            await next();
            //            await ctx.Response.WriteAsync("<!--Enable feautureA -->");
            //        });
            //});

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("v1/swagger.json", "Amanah.Posthub.OrderService.API v1"));

            
            app.UseHangfireDashboard();
            app.UseHttpsRedirection();

            app.UseRouting();
            var paths = new[]
            {
                "UploadFile/OrderFiles",
                "MailItemImages/Signature",
                "DriverImages",
                "MailItemImages/ProveOfDelivery",
                "ExportedTemplates",
                "UploadFile/ServiceFile",
                "UploadFile/WorkOrderFile",
            };

            foreach (var path in paths)
            {
                var fullPath = Path.Combine(Directory.GetCurrentDirectory(), path);
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(fullPath),
                    RequestPath = $"/{path}"
                });
            }

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseMiddleware<ErrorHandlerMiddleware>();

            // app.UseElmah();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            var identityUrl = _configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication("Bearer")
             .AddJwtBearer("Bearer", options =>
             {
                 options.Authority = identityUrl;
                 options.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateAudience = false
                 };
             });
        }

    }
}


