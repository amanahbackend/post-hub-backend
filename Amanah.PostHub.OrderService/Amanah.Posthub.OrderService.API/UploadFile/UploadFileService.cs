﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Amanah.Posthub.OrderService.API.UploadFile
{
    public interface IUploadBase64Service
    {
        Task<ProcessResult<string>> UploadFileAsync(string file, string path);
        Task<ProcessResult<List<string>>> UploadFilesAsync(List<string> files, string path);
    }
    public class UploadBase64Service : IUploadBase64Service
    {

        public async Task<ProcessResult<string>> UploadFileAsync(string file, string path)
        {
            ProcessResult<string> result = new ProcessResult<string>
            {
                MethodName = MethodBase.GetCurrentMethod().Name
            };
            if (file == null) return result;
            try
            {
                //var extension = Path.GetExtension("image/jpeg");
                //string fileName = Guid.NewGuid().ToString() + extension;
                //Generate unique filename
                //var fileName = string.Format(@"{0}", Guid.NewGuid());

                // string filepath = path + "/" + fileName + ".jpeg";

                var bytess = Convert.FromBase64String(file);
                using (var imageFile = new FileStream(path, FileMode.Create))
                {
                    imageFile.Write(bytess, 0, bytess.Length);
                    imageFile.Flush();
                }
                result.IsSucceeded = true;
                result.ReturnData = path;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSucceeded = false;
            }
            return result;
        }

        public Task<ProcessResult<List<string>>> UploadFilesAsync(List<string> files, string path)
        {
            throw new NotImplementedException();
        }


    }
}
