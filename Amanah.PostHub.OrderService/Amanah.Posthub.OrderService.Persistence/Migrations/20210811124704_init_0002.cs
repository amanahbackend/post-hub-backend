﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.OrderService.Persistence.Migrations
{
    public partial class init_0002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AddressTags",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "منزل" },
                    { 2, true, false, "عمل" },
                    { 3, true, false, "مؤقت" },
                    { 4, true, false, "جديد" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AddressTags",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AddressTags",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "AddressTags",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "AddressTags",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
