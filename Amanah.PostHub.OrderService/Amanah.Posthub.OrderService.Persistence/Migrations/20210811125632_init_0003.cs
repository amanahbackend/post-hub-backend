﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.OrderService.Persistence.Migrations
{
    public partial class init_0003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "WeightUOMs",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "Name" },
                values: new object[] { 1, "KG", "A unit of mass equal to one thousand grams.", true, false, "Kilogram" });

            migrationBuilder.InsertData(
                table: "WeightUOMs",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "Name" },
                values: new object[] { 2, "G", "", true, false, "Gram" });

            migrationBuilder.InsertData(
                table: "WeightUOMs",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "Name" },
                values: new object[] { 3, "lb", "is a unit of mass used in British imperial and United States customary systems of measurement", true, false, "Pound" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
