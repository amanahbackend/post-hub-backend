﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.OrderService.Persistence.Migrations
{
    public partial class init_0004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "CollectionMethods",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "Cash" },
                    { 2, true, false, "Prepaid Stock" },
                    { 3, true, false, "Account" }
                });

            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "SPOk" },
                    { 2, true, false, "contracts" },
                    { 3, true, false, "invoices" },
                    { 4, true, false, "branch" },
                    { 5, true, false, "pickup" }
                });

            migrationBuilder.InsertData(
                table: "ContractStatus",
                columns: new[] { "Id", "Description", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 8, "", true, false, "Closed" },
                    { 7, "", true, false, "InRenewal" },
                    { 5, "", true, false, "Blocked" },
                    { 6, "", true, false, "Canceled" },
                    { 3, "", true, false, "Inactive" },
                    { 2, "", true, false, "Active" },
                    { 1, "", true, false, "WaitingApproval" },
                    { 4, "", true, false, "OnHold" }
                });

            migrationBuilder.InsertData(
                table: "CreditStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "إئتمان" },
                    { 2, true, false, "نقدي" },
                    { 3, true, false, "إيداع" },
                    { 4, true, false, "محظور" }
                });

            migrationBuilder.InsertData(
                table: "Currency",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "Name", "Symbol" },
                values: new object[,]
                {
                    { 1, "USD", true, false, "", null },
                    { 2, "GBP", true, false, "", null },
                    { 3, "KWD", true, false, "", null },
                    { 4, "SAR", true, false, "", null },
                    { 5, "EUR", true, false, "", null }
                });

            migrationBuilder.InsertData(
                table: "DriverDutyStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 5, true, false, "OnBreak" },
                    { 4, true, false, "Going-off-duty" },
                    { 3, true, false, "OffDuty" },
                    { 2, true, false, "Away" },
                    { 1, true, false, "OnDuty" }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "بنوك" },
                    { 2, true, false, "محاكم" },
                    { 3, true, false, "مجمعات" },
                    { 4, true, false, "نوادي" },
                    { 5, true, false, "محامون" },
                    { 6, true, false, " اتحادات" },
                    { 7, true, false, "نقابات" },
                    { 8, true, false, "كيانات غير ربحيه" },
                    { 9, true, false, "جمعيات" },
                    { 10, true, false, "دعاية" },
                    { 11, true, false, "مطبوعات" }
                });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "Name" },
                values: new object[] { 2, "en", true, false, "English" });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "Name" },
                values: new object[] { 1, "ar", true, false, "arabic" });

            migrationBuilder.InsertData(
                table: "LengthUOMs",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, "Cm", true, false, "centemeter" },
                    { 2, "Inch", true, false, "inches" },
                    { 3, "Feet", true, false, "feet" }
                });

            migrationBuilder.InsertData(
                table: "MailItemStatuss",
                columns: new[] { "Id", "Description", "IsActive", "IsDeleted", "Name", "Rank" },
                values: new object[,]
                {
                    { 19, null, true, false, "  العنصر موقوف", 0 },
                    { 18, null, true, false, "  تم تسليم العنصر", 0 },
                    { 17, null, true, false, " جاري تسليم العنصر", 0 },
                    { 16, null, true, false, " العنصر تحت التسليم", 0 },
                    { 15, null, true, false, "  تم توزيع العنصر", 0 },
                    { 14, null, true, false, "  العنصر تحت التوزيع", 0 },
                    { 13, null, true, false, "  تم عنونة العنصر", 0 },
                    { 12, null, true, false, "  تم فرز العنصر", 0 },
                    { 11, null, true, false, " العنصر تحت الفرز", 0 },
                    { 20, null, true, false, "  العنصر مرتجع", 0 },
                    { 9, null, true, false, "  تم التقاط العنصر", 0 },
                    { 8, null, true, false, "  تم فرز الطلب", 0 },
                    { 7, null, true, false, "  طلب تحت الفرز ", 0 },
                    { 6, null, true, false, " تم رفع بيانات العناصر", 0 },
                    { 5, null, true, false, "لرفع بيانات العناصر", 0 },
                    { 4, null, true, false, "تم التقاط الطلب", 0 },
                    { 3, null, true, false, "تم التوزيع لسائق", 0 },
                    { 2, null, true, false, "تم توزي ع الطلب", 0 },
                    { 1, null, true, false, "طلب جديد", 0 },
                    { 10, null, true, false, "  تم رفع العنصر", 0 }
                });

            migrationBuilder.InsertData(
                table: "MailItemTypes",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 6, true, false, "Commodity" },
                    { 4, true, false, "Post Card" },
                    { 5, true, false, "Parcel" },
                    { 2, true, false, "Letter" },
                    { 1, true, false, "Root" },
                    { 3, true, false, "Flat" }
                });

            migrationBuilder.InsertData(
                table: "OrderRecurencies",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, "", true, false, "Daily" },
                    { 2, "", true, false, "Weekly" },
                    { 3, "", true, false, "BiWeekly" },
                    { 4, "", true, false, "Monthly" },
                    { 5, "", true, false, "AtDayofMonth" },
                    { 6, "", true, false, "AtDayofWeek" }
                });

            migrationBuilder.InsertData(
                table: "OrderStatus",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "Name", "Rank" },
                values: new object[,]
                {
                    { 10, "", "", true, false, "On-Hold", 0 },
                    { 15, "", "", true, false, "Canceled", 0 },
                    { 14, "", "", true, false, "ClosedWithReturns", 0 },
                    { 13, "", "", true, false, "Delivered", 0 },
                    { 12, "", "", true, false, "Driver at Delivery", 0 },
                    { 11, "", "", true, false, "Dispatched", 0 }
                });

            migrationBuilder.InsertData(
                table: "OrderStatus",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "Name", "Rank" },
                values: new object[,]
                {
                    { 9, "", "", true, false, "Sorted", 0 },
                    { 16, "", "", true, false, "PassedToCorrier", 0 },
                    { 7, "", "", true, false, "Uploaded", 0 },
                    { 6, "", "", true, false, "In-upload", 0 },
                    { 5, "", "", true, false, "Received", 0 },
                    { 4, "", "", true, false, "Picked-up", 0 },
                    { 3, "", "", true, false, "Driver at Pickup", 0 },
                    { 2, "", "", true, false, "Assigned", 0 },
                    { 1, "", "", true, false, "Registered", 0 },
                    { 8, "", "", true, false, "In-Sorting", 0 }
                });

            migrationBuilder.InsertData(
                table: "OrderTypes",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 2, true, false, "عادي" },
                    { 1, true, false, "تجاري" }
                });

            migrationBuilder.InsertData(
                table: "PaymentMethods",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "نقدي" },
                    { 2, true, false, "فيزا" },
                    { 3, true, false, "إيداع" },
                    { 4, true, false, "KNET" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 8, "", "", true, false, "سائق" },
                    { 7, "", "", true, false, "محاسب" },
                    { 6, "", "", true, false, "إداري" },
                    { 5, "", "", true, false, "موزع" },
                    { 9, "", "", true, false, "مندوب" },
                    { 3, "", "", true, false, "خدمات عملاء" },
                    { 2, "", "", true, false, "مشغل النظام" },
                    { 1, "", "", true, false, "مدير النظام" },
                    { 4, "", "", true, false, "فراز" }
                });

            migrationBuilder.InsertData(
                table: "ShipServiceTypes",
                columns: new[] { "Id", "Description", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, null, true, false, " IP" },
                    { 2, null, true, false, " IE" },
                    { 3, null, true, false, "  IPF" },
                    { 4, null, true, false, " IEE" }
                });

            migrationBuilder.InsertData(
                table: "UserAccessTypes",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "MobileAcess" },
                    { 2, true, false, "WebAcess" }
                });

            migrationBuilder.InsertData(
                table: "UserAccountStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name", "Rank" },
                values: new object[,]
                {
                    { 6, true, false, " Canceled", 0 },
                    { 5, true, false, "Blocked", 0 },
                    { 4, true, false, "OnHold", 0 },
                    { 3, true, false, "Inactive", 0 },
                    { 1, true, false, "WaitingApproval", 0 },
                    { 2, true, false, "Active", 0 }
                });

            migrationBuilder.InsertData(
                table: "WorkOrderStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name", "Rank" },
                values: new object[,]
                {
                    { 9, true, false, "ملغي", 0 },
                    { 1, true, false, "جديد", 0 },
                    { 2, true, false, "استكمال عتاصر", 0 },
                    { 3, true, false, "موقوف", 0 },
                    { 4, true, false, "للتوزيع", 0 }
                });

            migrationBuilder.InsertData(
                table: "WorkOrderStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name", "Rank" },
                values: new object[,]
                {
                    { 5, true, false, "تم التوزيع", 0 },
                    { 6, true, false, " مرفوض", 0 },
                    { 7, true, false, "في قائمة السائق", 0 },
                    { 8, true, false, "مفتوح للعمل", 0 },
                    { 10, true, false, " مغلق", 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CollectionMethods",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CollectionMethods",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CollectionMethods",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ContractStatus",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ContractStatus",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ContractStatus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ContractStatus",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ContractStatus",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "ContractStatus",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "ContractStatus",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "ContractStatus",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CreditStatus",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CreditStatus",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CreditStatus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CreditStatus",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "DriverDutyStatus",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "DriverDutyStatus",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "DriverDutyStatus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "DriverDutyStatus",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "DriverDutyStatus",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Industries",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "LengthUOMs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "LengthUOMs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "LengthUOMs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "MailItemStatuss",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "OrderRecurencies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "OrderRecurencies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "OrderRecurencies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "OrderRecurencies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "OrderRecurencies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "OrderRecurencies",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "OrderTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "OrderTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PaymentMethods",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PaymentMethods",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PaymentMethods",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PaymentMethods",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "ShipServiceTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ShipServiceTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ShipServiceTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ShipServiceTypes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "UserAccessTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "UserAccessTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "UserAccountStatus",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "UserAccountStatus",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "UserAccountStatus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "UserAccountStatus",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "UserAccountStatus",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "UserAccountStatus",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "WorkOrderStatus",
                keyColumn: "Id",
                keyValue: 10);
        }
    }
}
