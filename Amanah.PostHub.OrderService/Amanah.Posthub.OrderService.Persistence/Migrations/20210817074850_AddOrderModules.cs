﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.OrderService.Persistence.Migrations
{
    public partial class AddOrderModules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Branch_Company_CompanyId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkingHours_Branch_BranchId",
                table: "WorkingHours");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkingHours_Company_CompanyId",
                table: "WorkingHours");

            migrationBuilder.DeleteData(
                table: "CollectionMethods",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CollectionMethods",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CollectionMethods",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "ContactFunctions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DropColumn(
                name: "Offset",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "TimeZone",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "CollectionMethods");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "ContactFunctions",
                newName: "UpdatedBy_Id");

            migrationBuilder.RenameColumn(
                name: "TwitterURL",
                table: "Company",
                newName: "Mobile");

            migrationBuilder.RenameColumn(
                name: "TradeNameAR",
                table: "Company",
                newName: "MissionSlogan");

            migrationBuilder.RenameColumn(
                name: "NameAR",
                table: "Company",
                newName: "FaxNo");

            migrationBuilder.RenameColumn(
                name: "FacebookURl",
                table: "Company",
                newName: "CompanyAddress_Street");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "CollectionMethods",
                newName: "UpdatedBy_Id");

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "WorkorderType",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "WorkOrderStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "To",
                table: "WorkingHours",
                type: "time",
                nullable: true,
                oldClrType: typeof(TimeSpan),
                oldType: "time");

            migrationBuilder.AlterColumn<int>(
                name: "BranchId",
                table: "WorkingHours",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SecondFrom",
                table: "WorkingHours",
                type: "time",
                nullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SecondTo",
                table: "WorkingHours",
                type: "time",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "WeightUOMs",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "UserAccountStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "UserAccessTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "States",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "ShipServiceTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "ServiceTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Roles",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "PaymentMethods",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "OrderTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "OrderStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PickupAddress_PACINumber",
                table: "Orders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "OrderRecurencies",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "MailItemTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "MailItemStatuss",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "DropOffAddress_PACINumber",
                table: "MailItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PickUpAddress_PACINumber",
                table: "MailItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "LengthUOMs",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Languages",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Industries",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "FileType",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "DriverDutyStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "DropOffAddress_PACINumber",
                table: "DirectOrders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Currency",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "CreditStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "CourierZones",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Couriers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "ContractStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Address_PACINumber",
                table: "Contacts",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "ContactFunctions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "ContactFunctions",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "ContactFunctions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "ContactFunctions",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Name_ar",
                table: "ContactFunctions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name_en",
                table: "ContactFunctions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "ContactFunctions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "ContactFunctions",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BussinesDiscription",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Area",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Block",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Building",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Flat",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Floor",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Governorate",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Latitude",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Longitude",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_PACINumber",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MainBranchId",
                table: "Company",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "CollectionMethods",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "CollectionMethods",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "CollectionMethods",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "CollectionMethods",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Name_ar",
                table: "CollectionMethods",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name_en",
                table: "CollectionMethods",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "CollectionMethods",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "CollectionMethods",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<int>(
                name: "RestaurantId",
                table: "Branch",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "GeoFenceId",
                table: "Branch",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Branch",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Mobile",
                table: "Branch",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "AddressTags",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "CompanySocialLinks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    SiteName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SiteLink = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanySocialLinks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanySocialLinks_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CourierShipmentServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourierShipmentServices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ItemReturnReasons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemReturnReasons", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "CourierShipmentServices",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, true, false, false, "IP" },
                    { 2, true, false, false, "IE" },
                    { 3, true, false, false, "IPF" },
                    { 4, true, false, false, "IEE" }
                });

            migrationBuilder.UpdateData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Code", "Name" },
                values: new object[] { "ar-eg", "العربيه" });

            migrationBuilder.UpdateData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: 2,
                column: "Code",
                value: "en-us");

            migrationBuilder.CreateIndex(
                name: "IX_Company_MainBranchId",
                table: "Company",
                column: "MainBranchId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanySocialLinks_CompanyId",
                table: "CompanySocialLinks",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_Company_CompanyId",
                table: "Branch",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch",
                column: "GeoFenceId",
                principalTable: "GeoFence",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch",
                column: "RestaurantId",
                principalTable: "Restaurant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Branch_MainBranchId",
                table: "Company",
                column: "MainBranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkingHours_Branch_BranchId",
                table: "WorkingHours",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkingHours_Company_CompanyId",
                table: "WorkingHours",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Branch_Company_CompanyId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Branch_MainBranchId",
                table: "Company");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkingHours_Branch_BranchId",
                table: "WorkingHours");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkingHours_Company_CompanyId",
                table: "WorkingHours");

            migrationBuilder.DropTable(
                name: "CompanySocialLinks");

            migrationBuilder.DropTable(
                name: "CourierShipmentServices");

            migrationBuilder.DropTable(
                name: "ItemReturnReasons");

            migrationBuilder.DropIndex(
                name: "IX_Company_MainBranchId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "WorkorderType");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "WorkOrderStatus");

            migrationBuilder.DropColumn(
                name: "SecondFrom",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "SecondTo",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "UserAccountStatus");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "UserAccessTypes");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "States");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "ShipServiceTypes");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "ServiceTypes");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Roles");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "PaymentMethods");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "OrderTypes");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "OrderStatus");

            migrationBuilder.DropColumn(
                name: "PickupAddress_PACINumber",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "OrderRecurencies");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "MailItemStatuss");

            migrationBuilder.DropColumn(
                name: "DropOffAddress_PACINumber",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "PickUpAddress_PACINumber",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "LengthUOMs");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Languages");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Industries");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "FileType");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "DriverDutyStatus");

            migrationBuilder.DropColumn(
                name: "DropOffAddress_PACINumber",
                table: "DirectOrders");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Currency");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "CreditStatus");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "CourierZones");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Couriers");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "ContractStatus");

            migrationBuilder.DropColumn(
                name: "Address_PACINumber",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "Name_ar",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "Name_en",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "BussinesDiscription",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Area",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Block",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Building",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Flat",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Floor",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Governorate",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Latitude",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Longitude",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_PACINumber",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "MainBranchId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "Name_ar",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "Name_en",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "Mobile",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "AddressTags");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy_Id",
                table: "ContactFunctions",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Mobile",
                table: "Company",
                newName: "TwitterURL");

            migrationBuilder.RenameColumn(
                name: "MissionSlogan",
                table: "Company",
                newName: "TradeNameAR");

            migrationBuilder.RenameColumn(
                name: "FaxNo",
                table: "Company",
                newName: "NameAR");

            migrationBuilder.RenameColumn(
                name: "CompanyAddress_Street",
                table: "Company",
                newName: "FacebookURl");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy_Id",
                table: "CollectionMethods",
                newName: "Name");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "To",
                table: "WorkingHours",
                type: "time",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0),
                oldClrType: typeof(TimeSpan),
                oldType: "time",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BranchId",
                table: "WorkingHours",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "Offset",
                table: "WorkingHours",
                type: "time",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "TimeZone",
                table: "WorkingHours",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ContactFunctions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "CollectionMethods",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<int>(
                name: "RestaurantId",
                table: "Branch",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GeoFenceId",
                table: "Branch",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "CollectionMethods",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "Cash" },
                    { 2, true, false, "Prepaid Stock" },
                    { 3, true, false, "Account" }
                });

            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Id", "IsActive", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 1, true, false, "SPOk" },
                    { 2, true, false, "contracts" },
                    { 3, true, false, "invoices" },
                    { 4, true, false, "branch" },
                    { 5, true, false, "pickup" }
                });

            migrationBuilder.UpdateData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Code", "Name" },
                values: new object[] { "ar", "arabic" });

            migrationBuilder.UpdateData(
                table: "Languages",
                keyColumn: "Id",
                keyValue: 2,
                column: "Code",
                value: "en");

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_Company_CompanyId",
                table: "Branch",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch",
                column: "GeoFenceId",
                principalTable: "GeoFence",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch",
                column: "RestaurantId",
                principalTable: "Restaurant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkingHours_Branch_BranchId",
                table: "WorkingHours",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkingHours_Company_CompanyId",
                table: "WorkingHours",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
