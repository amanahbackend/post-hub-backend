﻿using Amanah.Posthub.BASE.Authentication;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Authorization
{
    public class PermissionAuthorizationHandler : AuthorizationHandler<PermissionRequirement>
    {
        private readonly ICurrentUser _currentUser;

        public PermissionAuthorizationHandler(
            ICurrentUser currentUser)
        {

            _currentUser = currentUser;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            PermissionRequirement requirement)
        {
            if (context.User == null)
            {
                context.Fail();
                return Task.CompletedTask;
            }
            var requirementPermissions = requirement.Permission
                .Split(',')
                .Select(permission => permission.Trim())
                .ToArray();
            var hasPermission = _currentUser.UserPermissions
                .Intersect(requirementPermissions)
                .Any();
            if (hasPermission)
            {
                context.Succeed(requirement);
            }
            else
            {
                context.Fail();
            }
            return Task.CompletedTask;
        }


    }
}
