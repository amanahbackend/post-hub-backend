﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Amanah.Posthub.BASE.Authentication
{
    public interface ICurrentUser
    {
        //TODO: make sure all those data are stored in the token
        bool? IsAuthenticated { get; }

        string Id { get; }

        int? DriverId { get; }
        int? BusinessCustomerId { get; }

        string UserName { get; }

        //string Email { get; }
        /// <summary>
        ///  Admin = 0,Agent = 2,Manager = 3,
        /// </summary>
        public RoleType? UserType { get; }
        public string Scope { get; }

        public IReadOnlyCollection<string> UserPermissions { get; }

        string TenantId { get; }
    }

    public interface ICurrentUserDetailsStore
    {
        void Clear();

        void Deserialize(string details);

        string Serialize();
    }


    public static class Extensions
    {
        public const string SuperAdminId = "AV59A46B-72BF-4849-82D0-43851B574590";
        public static bool HasPermission(this ICurrentUser currentUser, string permission)
        {
            return currentUser.UserPermissions.Contains(permission);
        }

        public static bool IsDriver(this ICurrentUser currentUser) => currentUser.DriverId.HasValue;
        public static bool IsManager(this ICurrentUser currentUser) => (currentUser.Id != currentUser.TenantId &&
                !string.IsNullOrEmpty(currentUser.TenantId) && currentUser.TenantId == SuperAdminId);
            //||
            //(currentUser.Id != SuperAdminId &&
            //    !string.IsNullOrEmpty(currentUser.TenantId));

        public static bool IsBusinessCustomer(this ICurrentUser currentUser) => (
             currentUser.BusinessCustomerId != null && currentUser.BusinessCustomerId > 0)
            || (currentUser.Id != SuperAdminId &&
                !string.IsNullOrEmpty(currentUser.TenantId));


    }

    public static class CustomClaimTypes
    {
        public const string UserId = ClaimTypes.NameIdentifier;//"sub";
        public const string TenantId = "Tenant_Id";
        public const string UserType = "UserType";
        public const string Permission = "permission";
        public const string Scope = "scope";
    }
}