﻿using System.ComponentModel;
using System.Linq;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Authorization
{
    public static class AgentPermissions
    {
        public static (string Name, string[] Permissions)[] FeaturePermissions { get; } =
            PermissionsReflectionHelper.GetFeaturePermissions(typeof(AgentPermissions));

        public static string[] FlatPermissions =>
            FeaturePermissions.SelectMany(feature => feature.Permissions).ToArray();

        [Description(Keys.PermissionFeatures.Profile)]
        public static class Profile
        {
            public const string UpdateProfile = "AgentPermissions.Profile.UpdateProfile";
            public const string UpdateProfilePicture = "AgentPermissions.Profile.UpdateProfilePicture";
        }

    }
}
