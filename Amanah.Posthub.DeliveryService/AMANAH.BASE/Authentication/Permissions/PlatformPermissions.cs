﻿using Amanah.Posthub.BLL.Authorization;
using System.ComponentModel;
using System.Linq;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.UserManagement.Permissions
{
    public static class PlatformPermissions
    {
        public static (string Name, string[] Permissions)[] FeaturePermissions { get; } =
           PermissionsReflectionHelper.GetFeaturePermissions(typeof(PlatformPermissions));

        public static string[] FlatPermissions =>
            FeaturePermissions.SelectMany(feature => feature.Permissions).ToArray();


        [Description(Keys.PermissionFeatures.Agent)]
        public static class Agent
        {
            public const string ReadRequestAgent = TenantPermissions.Agent.ReadRequestAgent;
            public const string EditRequestAgent = TenantPermissions.Agent.EditRequestAgent;
            public const string ApproveRequestAgent = TenantPermissions.Agent.ApproveRequestAgent;
            public const string RejectRequestAgent = TenantPermissions.Agent.RejectRequestAgent;
            public const string UpdateAgentTermsAndConditions = "ManagerPermissions.Agent.UpdateAgentTermsAndConditions";
            public const string ReadAgent = TenantPermissions.Agent.ReadAgent;
            public const string UpdateAgent = TenantPermissions.Agent.UpdateAgent;
            public const string DeleteAgent = TenantPermissions.Agent.DeleteAgent;
            public const string DeleteAllAgent = TenantPermissions.Agent.DeleteAllAgent;
            public const string UpdateAllAgent = TenantPermissions.Agent.UpdateAllAgent;
            public const string ChangeAgentPassword = TenantPermissions.Agent.ChangeAgentPassword;
            public const string ViewDriversLoginRequests = TenantPermissions.Agent.ViewDriversLoginRequests;
        }

        [Description(Keys.PermissionFeatures.Tenants)]
        public static class Tenants
        {
            public const string ReadDefaultPermissionTenants = "PlatformManagerPermissions.Tenants.ReadDefaultPermissionTenants";
            public const string ReadRequestTenant = "PlatformManagerPermissions.Tenants.ReadRequestTenant";
            public const string ApproveRequestTenant = "PlatformManagerPermissions.Tenants.ApproveRequestTenant";
            public const string RejectRequestTenant = "PlatformManagerPermissions.Tenants.RejectRequestTenant";
            public const string EditRequestTenant = "PlatformManagerPermissions.Tenants.EditRequestTenant";

        }

        [Description(Keys.PermissionFeatures.Managers)]
        public static class Managers
        {
            public const string ReadDefaultPermissionManagers = "PlatformManagerPermissions.Managers.ReadDefaultPermissionManagers";
            public const string AddManager = TenantPermissions.Managers.AddManager;
            public const string ReadAllManagers = TenantPermissions.Managers.ReadAllManagers;
            public const string UpdateAllManager = TenantPermissions.Managers.UpdateAllManager;
        }

        [Description(Keys.PermissionFeatures.Roles)]
        public static class Roles
        {
            public const string AddRole = TenantPermissions.Roles.AddRole;
            public const string ReadAllRoles = TenantPermissions.Roles.ReadAllRoles;
            public const string UpdateAllRoles = TenantPermissions.Roles.UpdateAllRoles;
            public const string UpdateRole = TenantPermissions.Roles.UpdateRole;
            public const string DeleteRole = TenantPermissions.Roles.DeleteRole;
        }

        [Description(Keys.PermissionFeatures.Teams)]
        public static class Team
        {
            public const string CreateTeam = TenantPermissions.Team.CreateTeam;
            public const string DeleteTeam = TenantPermissions.Team.DeleteTeam;
            public const string UpdateTeam = TenantPermissions.Team.UpdateTeam;
            public const string ReadTeam = TenantPermissions.Team.ReadTeam;
            public const string DeleteAllTeam = TenantPermissions.Team.DeleteAllTeam;
            public const string UpdateAllTeam = TenantPermissions.Team.UpdateAllTeam;
            public const string ReadMyTeam = TenantPermissions.Team.ReadMyTeam;
        }

        [Description(Keys.PermissionFeatures.Settings)]
        public static class Settings
        {
            public const string UpdateAutoAllocation = TenantPermissions.Settings.UpdateAutoAllocation;
            public const string ReadNotification = TenantPermissions.Settings.ReadNotification;
            public const string UpdateNotification = TenantPermissions.Settings.UpdateNotification;
        }

    }
}
