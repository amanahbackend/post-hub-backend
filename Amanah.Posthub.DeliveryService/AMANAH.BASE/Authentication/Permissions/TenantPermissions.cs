﻿using System.ComponentModel;
using System.Linq;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Authorization
{
    public static class TenantPermissions
    {
        public static (string Name, string[] Permissions)[] FeaturePermissions { get; } =
            PermissionsReflectionHelper.GetFeaturePermissions(typeof(TenantPermissions));

        public static string[] FlatPermissions =>
            FeaturePermissions.SelectMany(feature => feature.Permissions).ToArray();



        [Description(Keys.PermissionFeatures.Orders)]
        public static class Orders
        {
            public const string CreateBusinessOrders = "ManagerPermissions.BusinessOrders.CreateBusinessOrders";
            public const string Orders_ViewOrders = "ManagerPermissions.Orders.Orders_ViewOrders";
            public const string Orders_UpdateOrder = "ManagerPermissions.Orders.Orders_UpdateOrder";
            public const string Orders_DeleteOrder = "ManagerPermissions.Orders.Orders_DeleteOrder";
            public const string Orders_ExportOrders = "ManagerPermissions.Orders.Orders_ExportOrders";
            public const string Orders_PrintOrders = "ManagerPermissions.Orders.Orders_PrintOrders";
            public const string Orders_DispatchOrdersPage = "ManagerPermissions.Orders.Orders_DispatchOrdersPage";
        }


        [Description(Keys.PermissionFeatures.InternalPosts)]
        public static class InternalPosts
        {
            public const string CreateInternalPost = "ManagerPermissions.InternalPosts.CreateInternalPost";
            public const string InternalPost_ViewInternalPost = "ManagerPermissions.InternalPosts.InternalPost_ViewInternalPost";
            public const string InternalPost_DeleteInternalPost = "ManagerPermissions.InternalPosts.InternalPost_DeleteInternalPost";
            public const string InternalPost_EditInternalPost = "ManagerPermissions.InternalPosts.InternalPost_EditInternalPost";
            public const string InternalPost_ReadInternalPost = "ManagerPermissions.InternalPosts.InternalPost_ReadInternalPost";
            public const string InternalPost_PrintInternalPost = "ManagerPermissions.InternalPosts.InternalPost_PrintInternalPost";
          
        }

        [Description(Keys.PermissionFeatures.WorkOrders)]
        public static class WorkOrders
        {
            public const string WorkOrders_CreateWorkOrder = "ManagerPermissions.WorkOrders.WorkOrders_CreateOrder";
            public const string WorkOrders_ViewOrders = "ManagerPermissions.WorkOrders.WorkOrders_ViewOrders";
            public const string WorkOrders_UpdateOrder = "ManagerPermissions.WorkOrders.WorkOrders_UpdateOrder";
            public const string WorkOrders_AssignToDriver = "ManagerPermissions.WorkOrders.WorkOrders_AssignToDriver";
            public const string WorkOrders_AddToWorOrder = "ManagerPermissions.WorkOrders.WorkOrders_AddToWorOrder";
            public const string WorkOrders_IssueWorkOrder = "ManagerPermissions.WorkOrders.WorkOrders_IssueWorkOrder";
            public const string WorkOrders_DeleteOrder = "ManagerPermissions.WorkOrders.WorkOrders_DeleteOrder";
            public const string WorkOrders_ChangeStatus = "ManagerPermissions.WorkOrders.WorkOrders_ChangeStatus";
            public const string WorkOrders_ExportOrders = "ManagerPermissions.WorkOrders.WorkOrders_ExportOrders";
            public const string WorkOrders_PrintOrders = "ManagerPermissions.WorkOrders.WorkOrders_PrintOrders";
            public const string ReturnedMailitem_ViewReturnedMailitem = "ManagerPermissions.WorkOrders.ReturnedMailitem_ViewReturnedMailitem";
            public const string ReturnedMailitem_EditReturnedMailitem = "ManagerPermissions.WorkOrders.ReturnedMailitem_EditReturnedMailitem";

            //public const string WorkOrders_ViewWorkorderInformation = "ManagerPermissions.WorkOrders.WorkOrders_ViewWorkorderInformation";
            //public const string WorkOrders_ViewNonBusinessCustomerCodes = "ManagerPermissions.WorkOrders.WorkOrders_ViewNonBusinessCustomerCodes";
            //public const string WorkOrders_ViewCustomerIndustryList = "ManagerPermissions.WorkOrders.WorkOrders_ViewCustomerIndustryList";
            //public const string WorkOrders_ViewBusinessCustomerCodes = "ManagerPermissions.WorkOrders.WorkOrders_ViewBusinessCustomerCodes";
            //public const string WorkOrders_ViewBusinessCustomerNames = "ManagerPermissions.WorkOrders.WorkOrders_ViewBusinessCustomerNames";
            //public const string WorkOrders_ViewBusinessCustomerIndustry = "ManagerPermissions.WorkOrders.WorkOrders_ViewBusinessCustomerIndustry";

            //public const string WorkOrders_CreatePersonalOrder = "ManagerPermissions.WorkOrders.WorkOrders_CreatePersonalOrder";

        }



        [Description(Keys.PermissionFeatures.Contracts)]
        public static class Contracts
        {

            public const string AddContract = "ManagerPermissions.BusinessCustomer.AddContract";
            public const string ReadContract = "ManagerPermissions.BusinessCustomer.ReadContract";
            public const string UpdateContract = "ManagerPermissions.BusinessCustomer.UpdateContract";
            public const string DeleteContract = "ManagerPermissions.BusinessCustomer.DeleteContract";
            public const string ExportContract = "ManagerPermissions.BusinessCustomer.ExportContract";

        }


        [Description(Keys.PermissionFeatures.BusinessCustomer)]
        public static class BusinessCustomer
        {
            public const string AddBusinessCustomer = "ManagerPermissions.BusinessCustomer.AddBusinessCustomer";
            public const string ReadBusinessCustomer = "ManagerPermissions.BusinessCustomer.ReadBusinessCustomer";
            public const string UpdateBusinessCustomer = "ManagerPermissions.BusinessCustomer.UpdateBusinessCustomer";
            public const string DeleteBusinessCustomer = "ManagerPermissions.BusinessCustomer.DeleteBusinessCustomer";
            public const string ExportBusinessCustomer = "ManagerPermissions.BusinessCustomer.ExportBusinessCustomer";
            public const string PrintBusinessCustomer = "ManagerPermissions.BusinessCustomer.PrintBusinessCustomer";
            //public const string ViewAccountRFQ = "ManagerPermissions.BusinessCustomer.ViewAccountRFQ";
            //public const string ViewAccountInvoices = "ManagerPermissions.BusinessCustomer.ViewAccountInvoices";
            //public const string ViewAccountContracts = "ManagerPermissions.BusinessCustomer.ViewAccountContracts";
            //public const string ViewAccountReports = "ManagerPermissions.BusinessCustomer.ViewAccountReports";
            //public const string ViewAccountAddressBook = "ManagerPermissions.BusinessCustomer.ViewAccountAddressBook";
            //public const string ViewAccountOrders = "ManagerPermissions.BusinessCustomer.ViewAccountOrders";
        }



        [Description(Keys.PermissionFeatures.Reports)]
        public static class Reports
        {
            //public const string ViewReport = "ManagerPermissions.Reports.ViewReport";
            //public const string ExportReport = "ManagerPermissions.Reports.ExportReport";
            public const string MailItemTracking = "ManagerPermissions.Reports.MailItemTracking";
            public const string LocalMessageDistributionReport = "ManagerPermissions.Reports.LocalMessageDistributionReport";
            public const string BusinessCustomerAndBranchesReport = "ManagerPermissions.Reports.BusinessCustomerAndBranchesReport";
            public const string BusinessCustomerDistributionReport = "ManagerPermissions.Reports.BusinessCustomerDistributionReport";
            public const string DeliveredMailitemsReport = "ManagerPermissions.Reports.DeliveredMailitemsReport";
            public const string ReturnedMailitemReport = "ManagerPermissions.Reports.ReturnedMailitemReport";
            public const string BranchesAndTotlaItemsReport = "ManagerPermissions.Reports.BranchesAndTotlaItemsReport";

        }


        [Description(Keys.PermissionFeatures.Task)]
        public static class Task
        {
            public const string CreateTask = "ManagerPermissions.Task.CreateTask";
            public const string UpdateTask = "ManagerPermissions.Task.UpdateTask";
            public const string DeleteTask = "ManagerPermissions.Task.DeleteTask";
            public const string ChangeTaskStatus = "ManagerPermissions.Task.ChangeTaskStatus";
            public const string ReadUnassignedTask = "ManagerPermissions.Task.ReadUnassignedTask";
            public const string ExportTask = "ManagerPermissions.Task.ExportTask";
        }

        [Description(Keys.PermissionFeatures.Agent)]
        public static class Agent
        {
            public const string ReadRequestAgent = "ManagerPermissions.Agent.ReadRequestAgent";
            public const string ApproveRequestAgent = "ManagerPermissions.Agent.ApproveRequestAgent";
            public const string EditRequestAgent = "ManagerPermissions.Agent.EditRequestAgent";
            public const string RejectRequestAgent = "ManagerPermissions.Agent.RejectRequestAgent";
            public const string UpdateRequestAgent = "ManagerPermissions.Agent.UpdateRequestAgent";

            public const string ReadAgent = "ManagerPermissions.Agent.ReadAgent";
            public const string CreateAgent = "ManagerPermissions.Agent.CreateAgent";
            public const string UpdateAgent = "ManagerPermissions.Agent.UpdateAgent";
            public const string DeleteAgent = "ManagerPermissions.Agent.DeleteAgent";
            public const string DeleteAllAgent = "ManagerPermissions.Agent.DeleteAllAgent";
            public const string UpdateAllAgent = "ManagerPermissions.Agent.UpdateAllAgent";
            //public const string ViewUnverifiedAgent = "ManagerPermissions.Agent.ViewUnverifiedAgent";
            public const string ChangeAgentPassword = "ManagerPermissions.Agent.ChangeAgentPassword";
            public const string ViewDriversLoginRequests = "ManagerPermissions.Agent.ViewDriversLoginRequests";
            public const string ExportAgent = "ManagerPermissions.Agent.ExportAgent";
            public const string ImportAgent = "ManagerPermissions.Agent.ImportAgent";
        }

        [Description(Keys.PermissionFeatures.PlatformAgent)]
        public static class PlatformAgent
        {
            public const string ReadPlatformAgent = "ManagerPermissions.Agent.ReadPlatformAgent";
        }

        [Description(Keys.PermissionFeatures.Customer)]
        public static class Customer
        {
            public const string CreateCustomer = "ManagerPermissions.Customer.CreateCustomer";
            public const string DeleteCustomer = "ManagerPermissions.Customer.DeleteCustomer";
            public const string UpdateCustomer = "ManagerPermissions.Customer.UpdateCustomer";
            public const string ReadCustomer = "ManagerPermissions.Customer.ReadCustomer";
            public const string ExportCustomer = "ManagerPermissions.Customer.ExportCustomer";
            public const string ImportCustomer = "ManagerPermissions.Customer.ImportCustomer";
        }

        [Description(Keys.PermissionFeatures.Teams)]
        public static class Team
        {
            public const string CreateTeam = "ManagerPermissions.Team.CreateTeam";
            public const string DeleteTeam = "ManagerPermissions.Team.DeleteTeam";
            public const string UpdateTeam = "ManagerPermissions.Team.UpdateTeam";
            public const string ReadTeam = "ManagerPermissions.Team.ReadTeam";
            public const string DeleteAllTeam = "ManagerPermissions.Team.DeleteAllTeam";
            public const string UpdateAllTeam = "ManagerPermissions.Team.UpdateAllTeam";
            public const string ReadMyTeam = "ManagerPermissions.Team.ReadMyTeam";
        }

        [Description(Keys.PermissionFeatures.Settings)]
        public static class Settings
        {
            //public const string ReadAdvancePreference = "ManagerPermissions.Settings.ReadAdvancePreference";
            //public const string UpdateAdvancedPreference = "ManagerPermissions.Settings.UpdateAdvancedPreference";
            //public const string ReadAutoAllocation = "ManagerPermissions.Settings.ReadAutoAllocation";
            public const string UpdateAutoAllocation = "ManagerPermissions.Settings.UpdateAutoAllocation";
            public const string ReadNotification = "ManagerPermissions.Settings.ReadNotification";
            public const string UpdateNotification = "ManagerPermissions.Settings.UpdateNotification";
        }

        [Description(Keys.PermissionFeatures.Managers)]
        public static class Managers
        {
            public const string AddManager = "ManagerPermissions.Settings.AddManager";
            public const string ReadAllManagers = "ManagerPermissions.Settings.ReadAllManagers";
            public const string UpdateAllManager = "ManagerPermissions.Settings.UpdateAllManager";
            public const string DeleteAllManager = "ManagerPermissions.Settings.DeleteAllManager";
            public const string ExportManagers = "ManagerPermissions.Settings.ExportManager";
            public const string PrintManagers = "ManagerPermissions.Settings.ExportManager";

            //public const string ReadTeamManager = "ManagerPermissions.Settings.ReadTeamManager";
            ////public const string UpdateTeamManager = "ManagerPermissions.Settings.UpdateTeamManager";
            //public const string ChangeManagerPassword = "ManagerPermissions.Settings.ChangeManagerPassword";
        }



        //[Description(Keys.PermissionFeatures.Restaurants)]
        //public static class Restaurants
        //{
        //    public const string AddRestaurant = "ManagerPermissions.Settings.AddRestaurant";
        //    public const string ReadRestaurant = "ManagerPermissions.Settings.ReadRestaurant";
        //    public const string UpdateRestaurant = "ManagerPermissions.Settings.UpdateRestaurant";
        //    public const string DeleteRestaurant = "ManagerPermissions.Settings.DeleteRestaurant";
        //    public const string BlockRestaurant = "ManagerPermissions.Settings.BlockRestaurant";
        //    public const string UnBlockRestaurant = "ManagerPermissions.Settings.UnBlockRestaurant";
        //}

        [Description(Keys.PermissionFeatures.Branches)]
        public static class Branches
        {
            public const string AddBranch = "ManagerPermissions.Settings.AddBranch";
            public const string ReadBranch = "ManagerPermissions.Settings.ReadBranch";
            public const string UpdateBranch = "ManagerPermissions.Settings.UpdateBranch";
            public const string DeleteBranch = "ManagerPermissions.Settings.DeleteBranch";
            public const string BlockBranch = "ManagerPermissions.Settings.BlockBranch";
            public const string UnBlockBranch = "ManagerPermissions.Settings.UnBlockBranch";
        }

        [Description(Keys.PermissionFeatures.DispatchingManager)]
        public static class DispatchingManager
        {
            public const string AddManagerDispatching = "ManagerPermissions.Settings.AddManagerDispatching";
            public const string ReadManagerDispatching = "ManagerPermissions.Settings.ReadManagerDispatching";
            public const string UpdateManagerDispatching = "ManagerPermissions.Settings.UpdateManagerDispatching";
            public const string DeleteManagerDispatching = "ManagerPermissions.Settings.DeleteManagerDispatching";
        }





        [Description(Keys.PermissionFeatures.Roles)]
        public static class Roles
        {
            public const string AddRole = "ManagerPermissions.Roles.AddRole";
            public const string ReadAllRoles = "ManagerPermissions.Roles.ReadAllRoles";
            public const string UpdateAllRoles = "ManagerPermissions.Roles.UpdateAllRoles";
            public const string UpdateRole = "ManagerPermissions.Roles.UpdateRole";
            public const string DeleteRole = "ManagerPermissions.Roles.DeleteRole";
        }


        [Description(Keys.PermissionFeatures.CompanyProfile)]
        public static class CompanyProfile
        {
            public const string UpdateLogo = "ManagerPermissions.CompanyProfile.UpdateLogo";
            public const string ViewWorkingHours = "ManagerPermissions.CompanyProfile.ViewWorkingHours";
            public const string UpdateWorkingHours = "ManagerPermissions.CompanyProfile.UpdateWorkingHours";
            public const string UpdateCompanyProfile = "ManagerPermissions.CompanyProfile.UpdateCompanyProfile";
            public const string Viewbranches = "ManagerPermissions.CompanyProfile.Viewbranches";
            public const string AddCompanyBranches = "ManagerPermissions.CompanyProfile.AddCompanyBranches";
            public const string UpdateCompanyBranches = "ManagerPermissions.CompanyProfile.UpdateCompanyBranches";
            public const string DeleteCompanyBranches = "ManagerPermissions.CompanyProfile.DeleteCompanyBranches";
            public const string ExportBranches = "ManagerPermissions.CompanyProfile.ExportBranches";
            public const string PrintBranches = "ManagerPermissions.CompanyProfile.PrintBranches";
        }


        [Description(Keys.PermissionFeatures.Profile)]
        public static class Profile
        {
            public const string ViewProfile = "ManagerPermissions.Profile.ViewProfile";
            public const string EditProfile = "ManagerPermissions.Profile.EditProfile";
        }


        //[Description(Keys.PermissionFeatures.BusinessOrders)]
        //public static class BusinessOrders
        //{          //public const string CreateBusinessOrders = "ManagerPermissions.BusinessOrders.CreateBusinessOrders";
        ////    public const string BusinessOrdersDownloadTemplate = "ManagerPermissions.BusinessOrders.BusinessOrdersDownloadTemplate";
        ////    public const string BusinessOrdersImportItems = "ManagerPermissions.BusinessOrders.BusinessOrdersImportItems";
        ////    public const string LoadPreviousOrders = "ManagerPermissions.BusinessOrders.LoadPreviousOrders";
        ////    public const string BusinessOrdersAssignDrivers = "ManagerPermissions.BusinessOrders.BusinessOrdersAssignDrivers";
        ////    public const string BusinessOrdersViewDrivers = "ManagerPermissions.BusinessOrders.BusinessOrdersViewDrivers";
        ////    public const string BusinessOrdersViewItems = "ManagerPermissions.BusinessOrders.BusinessOrdersViewItems";
        ////    public const string BusinessOrdersViewItemTypes = "ManagerPermissions.BusinessOrders.BusinessOrdersViewItemTypes";
        ////    public const string BusinessOrdersUpdateItem = "ManagerPermissions.BusinessOrders.BusinessOrdersUpdateItem";
        ////    public const string BusinessOrdersCreateItem = "ManagerPermissions.BusinessOrders.BusinessOrdersCreateItem";
        ////    public const string BusinessOrdersDeleteItem = "ManagerPermissions.BusinessOrders.BusinessOrdersDeleteItem";
        ////    public const string BusinessOrdersDeleteBulkItems = "ManagerPermissions.BusinessOrders.BusinessOrdersDeleteBulkItems";
        ////    public const string BusinessOrdersExportStaff = "ManagerPermissions.BusinessOrders.BusinessOrdersExportStaff";
        ////    public const string BusinessOrdersChangeStatus = "ManagerPermissions.BusinessOrders.BusinessOrdersChangeStatus";
        ////    public const string BusinessOrdersPreviewEnvelopLabels = "ManagerPermissions.BusinessOrders.BusinessOrdersPreviewEnvelopLabels";
        ////    public const string BusinessOrdersPrintEnvelopLabels = "ManagerPermissions.BusinessOrders.BusinessOrdersPrintEnvelopLabels";
        ////    public const string BusinessOrdersPrintReceiptForms = "ManagerPermissions.BusinessOrders.BusinessOrdersPrintReceiptForms";
        ////    public const string BusinessOrdersPreviewReceiptForms = "ManagerPermissions.BusinessOrders.BusinessOrdersPreviewReceiptForms";
        ////    public const string BusinessOrdersScanBarCode = "ManagerPermissions.BusinessOrders.BusinessOrdersScanBarCode";
        //}


        [Description(Keys.PermissionFeatures.PersonalAccount)]
        public static class PersonalAccount
        {
            public const string PersonalAccount_ViewPersonalAccounts = "ManagerPermissions.PersonalAccount.PersonalAccount_ViewPersonalAccounts";
            public const string PersonalAccount_UpdatePersonalAccount = "ManagerPermissions.PersonalAccount.PersonalAccount_UpdatePersonalAccount";
            public const string PersonalAccount_CreatePersonalAccount = "ManagerPermissions.PersonalAccount.PersonalAccount_CreatePersonalAccount";
            public const string PersonalAccount_DeletePersonalAccount = "ManagerPermissions.PersonalAccount.PersonalAccount_DeletePersonalAccount";
            public const string PersonalAccount_ExportPersonalAccounts = "ManagerPermissions.PersonalAccount.PersonalAccount_ExportPersonalAccounts";
            public const string PersonalAccount_PrintPersonalAccounts = "ManagerPermissions.PersonalAccount.PersonalAccount_PrintPersonalAccounts";
            public const string PersonalAccount_ViewAccountReports = "ManagerPermissions.PersonalAccount.PersonalAccount_ViewAccountReports";
            public const string PersonalAccount_ViewAccountAddressBook = "ManagerPermissions.PersonalAccount.PersonalAccount_ViewAccountAddressBook";
            public const string PersonalAccount_ViewAccountOrders = "ManagerPermissions.PersonalAccount.PersonalAccount_ViewAccountOrders";
        }


        [Description(Keys.PermissionFeatures.PriceQoutation)]
        public static class PriceQoutation
        {
            public const string AddPriceQoutation = "ManagerPermissions.PriceQoutation.AddPriceQoutation";
            public const string ReadPriceQoutation = "ManagerPermissions.PriceQoutation.ReadPriceQoutation";
            public const string UpdatePriceQoutation = "ManagerPermissions.PriceQoutation.UpdatePriceQoutation";
            public const string DeletePriceQoutation = "ManagerPermissions.PriceQoutation.DeletePriceQoutation";
            public const string ViewPriceQoutation = "ManagerPermissions.PriceQoutation.ViewPriceQoutation";
            public const string PrintPriceQoutation = "ManagerPermissions.PriceQoutation.PrintPriceQoutation";
        }

        [Description(Keys.PermissionFeatures.PriceList)]
        public static class PriceList
        {
            public const string AddPriceList = "ManagerPermissions.PriceList.AddPriceList";
            public const string ReadPriceList = "ManagerPermissions.PriceList.ReadPriceList";
            public const string UpdatePriceList = "ManagerPermissions.PriceList.UpdatePriceList";
            public const string DeletePriceList = "ManagerPermissions.PriceList.DeletePriceList";
            public const string ViewPriceList = "ManagerPermissions.PriceList.viewPriceList";
            public const string ExportPriceList = "ManagerPermissions.PriceList.ExportPriceList";
        }



        [Description(Keys.PermissionFeatures.AccountLogs)]
        public static class AccountLogs
        {
            public const string ViewAccountLogs = "ManagerPermissions.AccountLogs.ViewAccountLogs";
            public const string ExportAccountLogs = "ManagerPermissions.AccountLogs.ExportAccountLogs";
        }


        [Description(Keys.PermissionFeatures.Areas)]
        public static class Areas
        {
            public const string CreateArea = "ManagerPermissions.Area.CreateArea";
            public const string UpdateArea = "ManagerPermissions.Area.UpdateArea";
            public const string DeleteArea = "ManagerPermissions.Area.DeleteArea";
            public const string ListAreas = "ManagerPermissions.Area.ListAreas";
        }

        [Description(Keys.PermissionFeatures.Countries)]
        public static class Countries
        {
            public const string CreateCountry = "ManagerPermissions.Country.CreateCountry";
            public const string UpdateCountry = "ManagerPermissions.Country.UpdateCountry";
            public const string DeleteCountry = "ManagerPermissions.Country.DeleteCountry";
            public const string ListCountrys = "ManagerPermissions.Country.ListCountries";
        }

        [Description(Keys.PermissionFeatures.Governorates)]
        public static class Governorates
        {
            public const string CreateGovernorate = "ManagerPermissions.Governorate.CreateGovernorate";
            public const string UpdateGovernorate = "ManagerPermissions.Governorate.UpdateGovernorate";
            public const string DeleteGovernorate = "ManagerPermissions.Governorate.DeleteGovernorate";
            public const string ListGovernorates = "ManagerPermissions.Governorate.ListGovernorates";
        }
        [Description(Keys.PermissionFeatures.Languages)]
        public static class Languages
        {
            public const string CreateLanguage = "ManagerPermissions.Language.CreateLanguage";
            public const string UpdateLanguage = "ManagerPermissions.Language.UpdateLanguage";
            public const string DeleteLanguage = "ManagerPermissions.Language.DeleteLanguage";
            public const string ListLanguages = "ManagerPermissions.Language.ListLanguages";
        }
        [Description(Keys.PermissionFeatures.Departments)]
        public static class Departments
        {
            public const string CreateDepartment = "ManagerPermissions.Department.CreateDepartment";
            public const string UpdateDepartment = "ManagerPermissions.Department.UpdateDepartment";
            public const string DeleteDepartment = "ManagerPermissions.Department.DeleteDepartment";
            public const string ListDepartment = "ManagerPermissions.Department.ListDepartment";
        }
        [Description(Keys.PermissionFeatures.DriverRoles)]
        public static class DriverRoles
        {
            public const string CreateDriverRoles = "ManagerPermissions.DriverRoles.CreateDriverRoles";
            public const string UpdateDriverRoles = "ManagerPermissions.DriverRoles.UpdateDriverRoles";
            public const string DeleteDriverRoles = "ManagerPermissions.DriverRoles.DeleteDriverRoles";
            public const string ListDriverRoles = "ManagerPermissions.DriverRoles.ListDriverRoles";
        }

        [Description(Keys.PermissionFeatures.PackingPackages)]
        public static class PackingPackage
        {
            public const string CreatePackingPackage = "ManagerPermissions.PackingPackage.CreatePackingPackage";
            public const string UpdatePackingPackage = "ManagerPermissions.PackingPackage.UpdatePackingPackage";
            public const string DeletePackingPackage = "ManagerPermissions.PackingPackage.DeletePackingPackage";
            public const string ListPackingPackage = "ManagerPermissions.PackingPackage.ListPackingPackage";
            public const string ExportToExcelPackingPackage = "ManagerPermissions.PackingPackage.ExportToExcelPackingPackage";
        }

        [Description(Keys.PermissionFeatures.Services)]
        public static class Services
        {
            public const string CreateService = "ManagerPermissions.Services.CreateService";
            public const string UpdateService = "ManagerPermissions.Services.UpdateService";
            public const string DeleteService = "ManagerPermissions.Services.DeleteService";
            public const string ListService   = "ManagerPermissions.Services.ListService";
        }

        
        [Description(Keys.PermissionFeatures.LengthUOMs)]
        public static class LengthUOMs
        {
            public const string CreateLengthUOM = "ManagerPermissions.LengthUOM.CreateLengthUOM";
            public const string UpdateLengthUOM = "ManagerPermissions.LengthUOM.UpdateLengthUOM";
            public const string DeleteLengthUOM = "ManagerPermissions.LengthUOM.DeleteLengthUOM";
            public const string ListLengthUOMs = "ManagerPermissions.LengthUOM.ListLengthUOMs";
        }
        [Description(Keys.PermissionFeatures.WeightUOMs)]
        public static class WeightUOMs
        {
            public const string CreateWeightUOM = "ManagerPermissions.WeightUOM.CreateWeightUOM";
            public const string UpdateWeightUOM = "ManagerPermissions.WeightUOM.UpdateWeightUOM";
            public const string DeleteWeightUOM = "ManagerPermissions.WeightUOM.DeleteWeightUOM";
            public const string ListWeightUOMs = "ManagerPermissions.WeightUOM.ListWeightUOMs";
        }
        [Description(Keys.PermissionFeatures.ItemTypes)]
        public static class ItemTypes
        {
            public const string CreateItemType = "ManagerPermissions.ItemType.CreateItemType";
            public const string UpdateItemType = "ManagerPermissions.ItemType.UpdateItemType";
            public const string DeleteItemType = "ManagerPermissions.ItemType.DeleteItemType";
            public const string ListItemTypes = "ManagerPermissions.ItemType.ListItemTypes";
        }
        [Description(Keys.PermissionFeatures.Remarks)]
        public static class Remarks
        {
            public const string CreateRemark = "ManagerPermissions.Remark.CreateRemark";
            public const string UpdateRemark = "ManagerPermissions.Remark.UpdateRemark";
            public const string DeleteRemark = "ManagerPermissions.Remark.DeleteRemark";
            public const string ReadRemark = "ManagerPermissions.Remark.ReadRemark";
        }
        [Description(Keys.PermissionFeatures.ItemReturnReasons)]
        public static class ItemReturnReasons
        {
            public const string CreateItemReturnReason = "ManagerPermissions.ItemReturnReason.CreateItemReturnReason";
            public const string UpdateItemReturnReason = "ManagerPermissions.ItemReturnReason.UpdateItemReturnReason";
            public const string DeleteItemReturnReason = "ManagerPermissions.ItemReturnReason.DeleteItemReturnReason";
            public const string ListItemReturnReasons = "ManagerPermissions.ItemReturnReason.ListItemReturnReasons";
        }

        [Description(Keys.PermissionFeatures.Geofences)]
        public static class Geofences
        {
            public const string AddGeofence = "ManagerPermissions.Settings.AddGeofence";
            public const string ReadGeofence = "ManagerPermissions.Settings.ReadGeofence";
            public const string UpdateGeofence = "ManagerPermissions.Settings.UpdateGeofence";
            public const string DeleteGeofence = "ManagerPermissions.Settings.DeleteGeofence";
            public const string ExportGeofence = "ManagerPermissions.Settings.ExportGeoFence";
        }

        //[Description(Keys.PermissionFeatures.Staffs)]
        //public static class Staff
        //{
        //    public const string AddStaff = "ManagerPermissions.Staff.AddStaff";
        //    public const string ReadStaff = "ManagerPermissions.Staff.ReadStaff";
        //    public const string UpdateStaff = "ManagerPermissions.Staff.UpdateStaff";
        //    public const string DeleteStaff = "ManagerPermissions.Staff.DeleteStaff";
        //    public const string ExportStaff = "ManagerPermissions.Staff.ExportStaff";
        //    public const string PrintStaff = "ManagerPermissions.Staff.PrintStaff";
        //}

        [Description(Keys.PermissionFeatures.Drivers)]
        public static class Drivers
        {
            public const string CreateDriver = "ManagerPermissions.Driver.CreateDriver";
            public const string UpdateDriver = "ManagerPermissions.Driver.UpdateDriver";
            public const string DeleteDriver = "ManagerPermissions.Driver.DeleteDriver";
            public const string ListDrivers = "ManagerPermissions.Driver.ListDrivers";
            public const string ExportDriversToExcel = "ManagerPermissions.Driver.ExportDriversToExcel";
        }

        [Description(Keys.PermissionFeatures.OrdersTracking)]
        public static class OrdersTracking
        {
            public const string OrderTracking = "ManagerPermissions.OrderTracking.OrderTracking";
        }
        [Description(Keys.PermissionFeatures.Invoices)]
        public static class Invoices
        {
            public const string GenerateInvoice = "ManagerPermissions.Invoice.GenerateInvoice";
            public const string ListInvoices = "ManagerPermissions.Invoice.ListInvoices";
            public const string ViewInvoice = "ManagerPermissions.Invoice.ViewInvoice";
            public const string EditInvoices = "ManagerPermissions.Invoice.EditInvoices";
            public const string PrintInvoice = "ManagerPermissions.Invoice.PrintInvoice";
            public const string DeleteInvoices = "ManagerPermissions.Invoice.DeleteInvoices";
        }


        [Description(Keys.PermissionFeatures.BusinessTypes)]
        public static class BusinessTypes
        {
            public const string CreateBusinessType = "ManagerPermissions.BusinessType.CreateBusinessType";
            public const string UpdateBusinessType = "ManagerPermissions.BusinessType.UpdateBusinessType";
            public const string DeleteBusinessType = "ManagerPermissions.BusinessType.DeleteBusinessType";
            public const string ListBusinessType = "ManagerPermissions.BusinessType.ListBusinessType";
        }

        [Description(Keys.PermissionFeatures.TermsAndConditions)]
        public static class TermsAndConditions
        {
            public const string CreateTermsAndConditions = "ManagerPermissions.TermsAndConditions.CreateTermsAndConditions";
            public const string UpdateTermsAndConditions = "ManagerPermissions.TermsAndConditions.UpdateTermsAndConditions";
            public const string DeleteTermsAndConditions = "ManagerPermissions.TermsAndConditions.DeleteTermsAndConditions";
            public const string ListTermsAndConditions   = "ManagerPermissions.TermsAndConditions.ListTermsAndConditions";
        }

    }
}
