﻿using Amanah.Posthub.BLL.Authorization;
using System.Collections.Generic;
using System.Linq;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.SharedKernel.Authentication
{
    public static class BsinessCustomerPermissionPovider
    {


        public static List<string> GetDafaultPermissions()
        {

         List<string> permissions = new List<string>
            {
              "ManagerPermissions.BusinessOrders.CreateBusinessOrders",
              "ManagerPermissions.Orders.Orders_ViewOrders",
              "ManagerPermissions.Orders.Orders_UpdateOrder",
              "ManagerPermissions.Orders.Orders_DeleteOrder",
              "ManagerPermissions.Orders.Orders_ExportOrders",
              "ManagerPermissions.Orders.Orders_PrintOrders",
              "ManagerPermissions.Order.OrderTracking",
              "ManagerPermissions.Reports.MailItemTracking",
              "ManagerPermissions.OrderTracking.OrderTracking",
              "ManagerPermissions.Reports.LocalMessageDistributionReport",
               "ManagerPermissions.Reports.BusinessCustomerAndBranchesReport",
               "ManagerPermissions.Reports.BusinessCustomerDistributionReport",
               "ManagerPermissions.Reports.DeliveredMailitemsReport",
               "ManagerPermissions.Reports.ReturnedMailitemReport",
               "ManagerPermissions.Reports.BranchesAndTotlaItemsReport"

           };
            return permissions
                .Distinct()
                .ToList();
        }

    }
}
