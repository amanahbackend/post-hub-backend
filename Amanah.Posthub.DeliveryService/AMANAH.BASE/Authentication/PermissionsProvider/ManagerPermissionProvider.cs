﻿using System.Collections.Generic;
using System.Linq;

namespace Amanah.Posthub.SharedKernel.Authentication.PermissionsProvider
{
    public static class ManagerPermissionProvider
    {
        public static List<string> GetDafaultPermissions()
        {

            List<string> permissions = new List<string>
            {

               "ManagerPermissions.InternalPosts.CreateInternalPost",
               "ManagerPermissions.InternalPosts.InternalPost_ViewInternalPost",
               "ManagerPermissions.InternalPosts.InternalPost_DeleteInternalPost",
               "ManagerPermissions.InternalPosts.InternalPost_EditInternalPost",
               "ManagerPermissions.InternalPosts.InternalPost_ReadInternalPost",
               "ManagerPermissions.InternalPosts.InternalPost_PrintInternalPost",

            };
            return permissions
                .Distinct()
                .ToList();
        }
    }
}
