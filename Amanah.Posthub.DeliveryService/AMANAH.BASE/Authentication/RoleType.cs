﻿namespace Amanah.Posthub.BASE.Authentication
{
    public enum RoleType
    {
        Manager = 1,
        Agent = 2,
        BusinessCustomer = 3
    }
}