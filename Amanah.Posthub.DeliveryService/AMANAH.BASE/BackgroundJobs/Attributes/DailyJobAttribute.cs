﻿using System;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DailyJobAttribute : PeriodicJobAttribute
    {
        private const int OneDay = 1;

        public int EveryDays { get; }

        public int AtHour { get; }

        public int AtMinute { get; }

        public DailyJobAttribute(
            int everyDays = OneDay,
            int atHour = DefaultHour,
            int atMinute = DefaultMinute)
        {
            EveryDays = ValidateEveryAmount(everyDays);
            AtHour = ValidateHour(atHour);
            AtMinute = ValidateMinute(atMinute);
        }
    }
}