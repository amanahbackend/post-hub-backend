﻿using System;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class HourlyJobAttribute : PeriodicJobAttribute
    {
        private const int OneHour = 1;

        public int EveryHours { get; }

        public int AtMinute { get; }

        public HourlyJobAttribute(int everyHours = OneHour, int atMinute = DefaultMinute)
        {
            EveryHours = ValidateEveryAmount(everyHours);
            AtMinute = ValidateMinute(atMinute);
        }
    }

}