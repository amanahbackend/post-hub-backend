﻿using System;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class MinutelyJobAttribute : PeriodicJobAttribute
    {
        private const int OneMinute = 1;

        public int EveryMinutes { get; }

        public MinutelyJobAttribute(int everyMinutes = OneMinute)
        {
            EveryMinutes = ValidateEveryAmount(everyMinutes);
        }
    }
}