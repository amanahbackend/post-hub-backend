﻿using System;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class MonthlyJobAttribute : PeriodicJobAttribute
    {
        private const int OneMonth = 1;

        public int EveryMonths { get; }

        public int AtDayOfMonth { get; }

        public int AtHour { get; }

        public int AtMinute { get; }

        public MonthlyJobAttribute(
            int everyMonths = OneMonth,
            int atDayOfMonth = DefaultDayOfMonth,
            int atHour = DefaultHour,
            int atMinute = DefaultMinute)
        {
            EveryMonths = ValidateEveryAmount(everyMonths);
            AtDayOfMonth = ValidateDayOfMonth(atDayOfMonth);
            AtHour = ValidateHour(atHour);
            AtMinute = ValidateMinute(atMinute);
        }
    }
}