﻿using Amanah.Posthub.SharedKernel.Utilites.Helpers;
using System;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes
{
    public abstract class PeriodicJobAttribute : Attribute
    {
        public const int DefaultMonth = 1;
        public const int DefaultDayOfMonth = 1;
        public const DayOfWeek DefaultDayOfWeek = DayOfWeek.Monday;
        public const int DefaultHour = 0;
        public const int DefaultMinute = 0;

        protected int ValidateEveryAmount(int everyAmount)
        {
            if (everyAmount < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(everyAmount));
            }
            return everyAmount;
        }

        protected int ValidateMinute(int minute)
        {
            return Check.Between(minute, 0, 59, nameof(minute));
        }

        protected int ValidateHour(int hour)
        {
            return Check.Between(hour, 0, 23, nameof(hour));
        }

        protected int ValidateDayOfMonth(int day)
        {
            return Check.Between(day, 1, 31, nameof(day));
        }

        protected DayOfWeek ValidateDayOfWeek(DayOfWeek dayOfWeek)
        {
            if (!Enum.IsDefined(typeof(DayOfWeek), dayOfWeek))
            {
                throw new ArgumentException("Invalid DayOfWeek.");
            }
            return dayOfWeek;
        }

        protected int ValidateMonth(int month)
        {
            return Check.Between(month, 1, 12, nameof(month));
        }
    }
}