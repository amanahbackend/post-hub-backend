﻿using System;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class WeeklyJobAttribute : PeriodicJobAttribute
    {
        public DayOfWeek AtDayOfWeek { get; }

        public int AtHour { get; }

        public int AtMinute { get; }

        public WeeklyJobAttribute(
            DayOfWeek atDayOfWeek = DefaultDayOfWeek,
            int atHour = DefaultHour,
            int atMinute = DefaultMinute)
        {
            AtDayOfWeek = ValidateDayOfWeek(atDayOfWeek);
            AtHour = ValidateHour(atHour);
            AtMinute = ValidateMinute(atMinute);
        }
    }
}