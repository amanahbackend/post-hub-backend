﻿using System;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class YearlyJobAttribute : PeriodicJobAttribute
    {
        public int AtMonth { get; }

        public int AtDayOfMonth { get; }

        public int AtHour { get; }

        public int AtMinute { get; }

        public YearlyJobAttribute(
            int atMonth = DefaultMonth,
            int atDayOfMonth = DefaultDayOfMonth,
            int atHour = DefaultHour,
            int atMinute = DefaultMinute)
        {
            AtMonth = ValidateMonth(atMonth);
            AtDayOfMonth = ValidateDayOfMonth(atDayOfMonth);
            AtHour = ValidateHour(atHour);
            AtMinute = ValidateMinute(atMinute);
        }
    }
}