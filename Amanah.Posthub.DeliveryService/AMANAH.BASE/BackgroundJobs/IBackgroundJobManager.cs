﻿using System;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs
{
    public interface IBackgroundJobManager
    {
        /// <summary>
        /// Enqueue a new job model, and invoke its handler that implement<code>IBackgroundJob&#60;TJobModel&#62;</code>
        /// </summary>
        /// <typeparam name="TJobModel">Type of job model</typeparam>
        /// <param name="jobModel">Job model</param>
        /// <param name="delay">Time to delay start of the enqueued job, by default Zero</param>

        void Enqueue<TJobModel>(TJobModel jobModel, TimeSpan delay = default);

        /// <summary>
        /// Schedule a new job model at a specific DateTime, and invoke its handler that implement<code>IBackgroundJob&#60;TJobModel&#62;</code>
        /// </summary>
        /// <typeparam name="TJobModel">Type of job model</typeparam>
        /// <param name="jobModel">Job model</param>
        /// <param name="enqueeAtUtc">UTC DateTime at which the job should run</param>
        void Schedule<TJobModel>(TJobModel jobModel, DateTime enqueeAtUtc);
    }
}
