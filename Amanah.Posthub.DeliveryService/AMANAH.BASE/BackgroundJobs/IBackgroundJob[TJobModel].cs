﻿using System.Threading.Tasks;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs
{
    public interface IBackgroundJob<TJobModel>
    {
        Task ExecuteAsync(TJobModel jobModel);
    }
}
