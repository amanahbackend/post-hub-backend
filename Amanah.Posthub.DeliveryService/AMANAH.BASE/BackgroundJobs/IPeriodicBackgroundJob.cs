﻿using System.Threading.Tasks;

namespace Amanah.Posthub.SharedKernel.BackgroundJobs
{
    /// <summary>
    /// Represents a periodic job that will run every specific period of time,
    /// user must customize this behavior use Job Attributes like (MinutelyJob, HourlyJob, DailyJob,..)
    /// </summary>
    public interface IPeriodicBackgroundJob
    {
        Task ExecuteAsync();
    }
}
