﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Utilites.UploadFile
{
    public interface IUploadFormFileService
    {
        Task<ProcessResult<string>> UploadFileAsync(IFormFile file, string path);
        Task<ProcessResult<List<string>>> UploadFilesAsync(List<IFormFile> files, string path);

        Task<ProcessResult<string>> UploadMailItemBarcodeImgAsync(Image MailItemBarcodeImg, string path);


        Task<ProcessResult<string>> UploadFileFromBase64Async(string base64, string path);
        Task<ProcessResult<List<string>>> UploadFilesFromListOfBase64Async(List<string> listOfBase64, string path);
    }
}
