﻿using System.Collections.Generic;

namespace Amanah.Posthub.SharedKernel.Common.Reporting
{
    public interface IRDLCrenderService
    {
        byte[] RenderRdlc(string reportPath, ExportReportType exportReportType,
            Dictionary<string, string> parameters = null, Dictionary<string, object> dataSources = null);

    }
}
