﻿using AspNetCore.Reporting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.SharedKernel.Common.Reporting
{
    public class RDLCrenderService : IRDLCrenderService
    {
        private readonly ILogger<RDLCrenderService> _logger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        public RDLCrenderService(
            ILogger<RDLCrenderService> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Render a rdlc (SQL Server Reporting Services)
        /// </summary>
        /// <param name="reportPath">Absolute file path</param>
        /// <param name="exportReportType">Export file extension</param>
        /// <param name="parameters">Report parameters</param>
        /// <param name="dataSources">Report datasources</param>
        /// <returns>Array file contents</returns>
        public byte[] RenderRdlc(string reportPath, ExportReportType exportReportType, Dictionary<string, string> parameters = null,
            Dictionary<string, object> dataSources = null)
        {
            try
            {

                var report = new LocalReport(reportPath);

                if (dataSources != default)
                {
                    foreach (var dataSource in dataSources)
                    {
                        report.AddDataSource(dataSource.Key, dataSource.Value);                        
                    }
                }

                var result = report.Execute((RenderType)exportReportType, 1, parameters);

                return result.MainStream;
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw;
            }
        }
    }

}
