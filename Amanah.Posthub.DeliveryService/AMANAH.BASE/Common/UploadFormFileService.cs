﻿// Decompiled with JetBrains decompiler
// Type: Utilites.UploadFile.UploadFileManager
// Assembly: Utilities, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7635F4C6-B6EE-485B-9347-798CDAAB016C
// Assembly location: D:\EnmaaBKp\Enmaa\Contract\Utilities.dll

using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Utilites.UploadFile
{
    public class UploadFormFileService : IUploadFormFileService
    {
        public async Task<ProcessResult<string>> UploadFileAsync(IFormFile file, string path)
        {
            ProcessResult<string> result = new ProcessResult<string>
            {
                MethodName = MethodBase.GetCurrentMethod().Name
            };
            if (file == null) return result;
            try
            {
                FileHelper.CreateIfMissing(path);
                var extension = Path.GetExtension(file.FileName);
                string fileName = Guid.NewGuid().ToString() + extension; //Create a new Name 
                string filePath = Path.Combine(path, fileName);                                                   //for the file due to security reasons.
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                result.IsSucceeded = true;
                result.ReturnData = fileName;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSucceeded = false;
            }
            return result;
        }
        public async Task<ProcessResult<List<string>>> UploadFilesAsync(List<IFormFile> files, string path)
        {
            ProcessResult<List<string>> output = new ProcessResult<List<string>>();
            output.ReturnData = new List<string>();
            foreach (var file in files)
            {
                ProcessResult<string> input = await this.UploadFileAsync(file, path);
                if (input.IsSucceeded)
                {
                    output.ReturnData.Add(input.ReturnData);
                    output = ProcessResultMapping.Map<string, List<string>>(input, output);
                }
                else
                {
                    output = ProcessResultMapping.Map<string, List<string>>(input, output);
                    break;
                }
            }
            return output;
        }

        public async Task<ProcessResult<string>> UploadMailItemBarcodeImgAsync(Image MailItemBarcodeImg, string path)
        {
            ProcessResult<string> result = new ProcessResult<string>
            {
                MethodName = MethodBase.GetCurrentMethod().Name
            };
            if (MailItemBarcodeImg == null) return result;
            try
            {
                FileHelper.CreateIfMissing(path);
                //var extension = Path.GetExtension(MailItemBarcodeImg);
                string fileName = Guid.NewGuid().ToString() + ".jpeg"; //Create a new Name 
                string filePath = Path.Combine(path, fileName);                                                   //for the file due to security reasons.
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    MailItemBarcodeImg.Save(stream, ImageFormat.Jpeg );
                }
                result.IsSucceeded = true;
                result.ReturnData = filePath.Replace("\\","/");
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSucceeded = false;
            }
            return result;
        }


        public async Task<ProcessResult<string>> UploadFileFromBase64Async(string base64, string path)
        {
            ProcessResult<string> result = new ProcessResult<string>
            {
                MethodName = MethodBase.GetCurrentMethod().Name
            };
            if (string.IsNullOrEmpty(base64)) return result;
            try
            {
                string fileName = $"{Guid.NewGuid()}.jpg"; 
                string filePath = Path.Combine(path, fileName);
                await File.WriteAllBytesAsync(filePath, Convert.FromBase64String(base64));
                result.IsSucceeded = true;
                result.ReturnData = fileName;
            }
            catch (Exception ex)
            {
                result.Exception = ex;
                result.IsSucceeded = false;
            }
            return result;
        }
        public async Task<ProcessResult<List<string>>> UploadFilesFromListOfBase64Async(List<string> listOfBase64, string path)
        {
            ProcessResult<List<string>> output = new ProcessResult<List<string>>();
            output.ReturnData = new List<string>();
            foreach (var base64 in listOfBase64)
            {
                ProcessResult<string> input = await this.UploadFileFromBase64Async(base64, path);
                if (input.IsSucceeded)
                {
                    output.ReturnData.Add(input.ReturnData);
                    output = ProcessResultMapping.Map<string, List<string>>(input, output);
                }
                else
                {
                    output = ProcessResultMapping.Map<string, List<string>>(input, output);
                    break;
                }
            }
            return output;
        }
    }
}
