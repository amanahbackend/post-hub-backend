﻿namespace Amanah.Posthub.BASE.Domain.Entities
{
    public class BaseEntity<TKey> : BaseGlobalEntity<TKey>, IBaseEntity<TKey>
    {
        public string Tenant_Id { get;  set; }
    }

    public class BaseEntity : BaseEntity<int>
    {
    }
}
