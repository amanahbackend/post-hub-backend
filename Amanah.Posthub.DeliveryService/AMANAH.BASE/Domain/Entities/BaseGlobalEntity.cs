﻿using System;

namespace Amanah.Posthub.BASE.Domain.Entities
{
    public class BaseGlobalEntity<TKey> : IBaseGlobalEntity<TKey>
    {
        public virtual string CreatedBy_Id { get; private set; }
        public virtual string UpdatedBy_Id { get; private set; }
        public virtual string DeletedBy_Id { get; private set; }
        public virtual bool IsDeleted { get; set; }
        public virtual DateTime CreatedDate { get; private set; }
        public virtual DateTime UpdatedDate { get; private set; }
        public virtual DateTime DeletedDate { get; protected set; }
        public virtual TKey Id { get; set; }

        public void SetIsDeleted(bool isDeleted)
        {
            IsDeleted = isDeleted;
            if (isDeleted)
            {
                DeletedDate = DateTime.UtcNow;
            }
            else
            {
                DeletedDate = DateTime.MinValue;
            }
        }
        public BaseGlobalEntity()
        {
            CreatedDate = DateTime.UtcNow;
        }

    }

    public class BaseGlobalEntity : BaseGlobalEntity<int>
    {

    }
}
