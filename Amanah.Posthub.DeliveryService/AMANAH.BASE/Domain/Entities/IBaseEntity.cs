﻿namespace Amanah.Posthub.BASE.Domain.Entities
{
    public interface IBaseEntity<TKey> : IBaseGlobalEntity<TKey>, IHaveTenant
    {

    }

    public interface IBaseEntity : IBaseEntity<int>
    {

    }
}
