﻿using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.BASE.Domain.Entities
{
    public interface IBaseGlobalEntity<TKey> : IEntity<TKey>, IHaveAuditing
    {

    }

    public interface IBaseGlobalEntity : IBaseGlobalEntity<int>
    {

    }
}
