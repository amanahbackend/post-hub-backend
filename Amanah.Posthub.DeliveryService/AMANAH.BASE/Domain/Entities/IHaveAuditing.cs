﻿using System;

namespace Amanah.Posthub.BASE.Domain.Entities
{
    public interface IHaveAuditing
    {
        string CreatedBy_Id { get; }

        string UpdatedBy_Id { get; }

        string DeletedBy_Id { get; }

        bool IsDeleted { get; }

        DateTime CreatedDate { get; }

        DateTime UpdatedDate { get; }

        DateTime DeletedDate { get; }

        void SetIsDeleted(bool isDeleted);
    }
}