﻿namespace Amanah.Posthub.BASE.Domain.Entities
{
    public interface IHaveTenant
    {
        string Tenant_Id { get; }

    }
}
