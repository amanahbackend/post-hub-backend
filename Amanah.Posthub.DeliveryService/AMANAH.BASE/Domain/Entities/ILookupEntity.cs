﻿namespace Amanah.Posthub.SharedKernel.Domain.Entities
{
    public interface ILookupEntity
    {
        public int Id { set; get; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }

    }
}
