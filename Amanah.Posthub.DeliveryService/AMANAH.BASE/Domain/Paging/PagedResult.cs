﻿using System.Collections.Generic;
using System.Linq;

namespace Utilities.Utilites.Paging
{
    public class PagedResult<T>
    {
        public virtual IReadOnlyCollection<T> Result { set; get; }

        public virtual int TotalCount { set; get; }

    }
    public class EnumerablePagedResult<T>
    {
        public IQueryable<T> ResultAsQuery { set; get; }
        public virtual IEnumerable<T> Result { set; get; }
        public virtual int TotalCount { set; get; }

    }

    public class QueryablePagedResult<T>
    {
        public virtual int TotalCount { set; get; }

        public virtual IQueryable<T> Result { set; get; }

    }
}
