﻿namespace Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel
{
    public class PaginatedItemsViewModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchBy { get; set; }
        public int Id { get; set; }

    }
}
