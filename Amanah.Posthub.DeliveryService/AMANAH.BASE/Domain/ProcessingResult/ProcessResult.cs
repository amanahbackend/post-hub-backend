﻿using System;
using System.Net;

namespace Utilites.ProcessingResult
{
    public class ProcessResult<T>
    {
        public string Message;
        public string MethodName;

        public ProcessResult(string methodName)
        {
            this.MethodName = methodName;
        }

        public ProcessResult<T> Success(T returnData, string message = null)
        {
            this.ReturnData = returnData;
            this.Message = message;
            this.IsSucceeded = true;
            return this;
        }
        public ProcessResult<T> Fail(string message = null)
        {
            this.Message = message;
            this.IsSucceeded = false;
            return this;
        }
        public ProcessResult<newT> Fail<newT>()
        {
            var failed = new ProcessResult<newT>();
            failed.Fail(this.Message);
            return failed;
        }

        public ProcessResult()
        {
        }

        public bool IsSucceeded { get; set; }

        public HttpStatusCode Status { get; set; }

        public Exception Exception { get; set; }

        public T ReturnData { get; set; }
    }
}
