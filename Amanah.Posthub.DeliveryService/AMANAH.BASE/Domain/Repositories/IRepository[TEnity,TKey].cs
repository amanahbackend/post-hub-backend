﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.SharedKernel.Domain.Repositories
{
    #region Sorting
    public enum SortDirection
    {
        Ascending = 1,
        Descending
    }

    public class SortingField
    {
        public string Field { get; set; }

        public SortDirection Direction { get; set; }

        public SortingField()
        {
        }

        public SortingField(string field, SortDirection direction)
        {
            Field = field;
            Direction = direction;
        }
    }

    public class SortingField<TEntity>
    {
        public Expression<Func<TEntity, object>> Field { get; }

        public SortDirection Direction { get; }

        public SortingField(Expression<Func<TEntity, object>> field, SortDirection sortDirection)
        {
            Field = field;
            Direction = sortDirection;
        }
    }

    #endregion

    #region Paging
    public class PagingOptions
    {
        public const int DefaultPageSize = 20;
        private const int MinimumPageNumber = 1;
        private const int MinimumPageSize = 1;
        private const int MaximumPgeSize = 1000;
        private int _pageNumber = MinimumPageNumber;
        private int _pageSize = DefaultPageSize;

        public int PageNumber
        {
            get => _pageNumber;
            set
            {
                if (value < MinimumPageNumber)
                {
                    throw new ArgumentException($"{nameof(PageNumber)} should be above one.");
                }
                _pageNumber = value;
            }
        }

        public int PageSize
        {
            get => _pageSize;
            set
            {
                _pageSize = Math.Min(Math.Max(value, MinimumPageSize), MaximumPgeSize);
            }
        }

        public PagingOptions(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
        }

        public PagingOptions()
        {
        }
    }
    public class _PagedResult<TEntity>
    {
        public int TotalCount { get; }

        public int TotalPages { get; }

        public int CurrentPage { get; }

        public int PageSize { get; }

        public IReadOnlyCollection<TEntity> Items { get; }

        public static _PagedResult<TEntity> Empty =>
            new _PagedResult<TEntity>(0, 0, 1, PagingOptions.DefaultPageSize, Array.Empty<TEntity>());

        public _PagedResult(
            int totalCount,
            int totalPages,
            int currentPage,
            int pageSize,
            IReadOnlyCollection<TEntity> items)
        {
            TotalCount = totalCount;
            TotalPages = totalPages;
            CurrentPage = currentPage;
            PageSize = pageSize;
            Items = items;
        }
    }
    public static class PagingExtensions
    {
        public static _PagedResult<TTarget> ReplaceItems<TSource, TTarget>(
            this _PagedResult<TSource> source,
            IReadOnlyCollection<TTarget> items)
        {
            return new _PagedResult<TTarget>(
                source.TotalCount,
                source.TotalPages,
                source.CurrentPage,
                source.PageSize,
                items);
        }
    }
    #endregion

    #region Entity
    public interface IEntity<TKey>
    {
        public TKey Id { get; }
    }

    public interface IEntity : IEntity<int>
    {
    }

    //public interface IAggregateRoot
    //{
    //}
    #endregion

    #region Repository
    public interface IRepository<TEntity, TKey> where TEntity : class, IEntity<TKey>//, IAggregateRoot
    {
        Task<TEntity> GetByIdAsync(TKey id);

        Task<IReadOnlyCollection<TEntity>> GetAllAsync(
            [NotNull] Expression<Func<TEntity, bool>> predicate,
            params SortingField<TEntity>[] sortingFields);


        Task<TEntity> GetFirstOrDefaultAsync(
         [NotNull] Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include);

        Task<TEntity> GetFirstOrDefaultAsync(
         [NotNull] Expression<Func<TEntity, bool>> predicate, bool IgnoreQueryFilters = false);

        Task<TEntity> GetFirstOrDefaultAsync(
            [NotNull] Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetFirstOrDefaultAsync(
            [NotNull] Expression<Func<TEntity, bool>> predicate,
            params SortingField<TEntity>[] sortingFields);


        Task<_PagedResult<TEntity>> GetAllPagedResult(
            [NotNull] Expression<Func<TEntity, bool>> predicate,
            [NotNull] PagingOptions pagingOptions,
            params SortingField<TEntity>[] sortingFields);


        Task<IReadOnlyCollection<TEntity>> ToIReadOnlyCollectionAsync(IQueryable<TEntity> query);
        IQueryable<TEntity> GetAllIQueryable([NotNull] Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include);
        IQueryable<TEntity> GetAllIQueryable([NotNull] Expression<Func<TEntity, bool>> predicate);
        IQueryable<TEntity> GetAllIQueryable();


        Task<TEntity> GetSingleOrDefaultAsync([NotNull] Expression<Func<TEntity, bool>> predicate);

        Task<int> CountAsync([NotNull] Expression<Func<TEntity, bool>> predicate);

        Task<bool> AnyAsync([NotNull] Expression<Func<TEntity, bool>> predicate);

        void Add([NotNull] TEntity entity);

        void AddRange([NotNull] IEnumerable<TEntity> entities);

        void Update([NotNull] TEntity entity, bool updateTrackedEntity = true);
        void UpdateRange([NotNull] IEnumerable<TEntity> entity);

        void Delete([NotNull] TEntity entity);

        //void Delete(TKey id);

        //void DeleteRange(IEnumerable<TKey> ids);

        void DeleteRange([NotNull] IEnumerable<TEntity> entities);
    }

    public interface IRepository<TEntity> : IRepository<TEntity, int>
        where TEntity : class, IEntity<int>//, IAggregateRoot
    {
    }
    #endregion

    #region UnitOfWork
    public interface IUnitOfWork
    {
        /// <summary>
        /// Enable or disable Tenant filtration on the current unit of work
        /// </summary>
        bool IsTenantFilterEnabled { get; set; }

        /// <summary>
        /// save all pening changes in current unit of work
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Run a sequence of changes to database wrapped in a single transaction and automatically commit on success or rollback on failure
        /// </summary>
        /// <param name="sequence">multiple steps that each of them change model state</param>
        /// <param name="isolationLevel">Isolation level of transaction</param>
        /// <returns>returns true if transaction was commited or false if rolledback</returns>
        Task<bool> RunTransaction(Func<Task> sequence, IsolationLevel isolationLevel = IsolationLevel.Serializable);
    }
    #endregion
}