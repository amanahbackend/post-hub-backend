﻿using Amanah.Posthub.SharedKernel.LoggingService;
using Amanah.Posthub.SharedKernel.Utilites.Helpers;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;

namespace Amanah.Posthub.SharedKernel.Exceptions
{
    public class ErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILoggerManager _logger;

        public ErrorHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
            _logger = LoggerManager.GetLoggerInstance();
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";
                response.StatusCode = (int)HttpStatusCode.BadRequest;

                var controllerActionDescriptor = context.GetEndpoint().Metadata.GetMetadata<ControllerActionDescriptor>();
                var controllerName = controllerActionDescriptor.ControllerName;
                var actionName = controllerActionDescriptor.ActionName;

                switch (error)
                {
                    case DomainException:
                        // custom application error
                        await response.WriteAsync($"{error.Message}");
                        break;
                    default:
                        // unhandled error
                        _logger.LogError($"In Controller Name: {controllerName}, Action Name: {actionName},\n\tStatus Code: { (int)HttpStatusCode.BadRequest }, Error Message: {error.Message}");
                        await response.WriteAsync("Generic error occurred, Please try again later");
                        break;
                }

            }
        }
    }
}
