﻿using System;
using Utilities.Utilites.Exceptions;

namespace Amanah.Posthub.BASE
{
    public class NotFoundException : DomainException
    {
        public NotFoundException(string message)
            : base(message)
        {
        }

        public NotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
