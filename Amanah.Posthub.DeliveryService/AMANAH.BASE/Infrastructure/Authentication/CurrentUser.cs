﻿using Amanah.Posthub.BASE.Authentication;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;

namespace Amanah.Posthub.SharedKernel.Infrastructure.Authentication
{
    internal class CurrentUser : ICurrentUser, ICurrentUserDetailsStore
    {
        // Note: we must specify if the current user is anonymous(not authenticated but httpcontext is not null) of system (httpcontext is null)
        // This is just revamping of existing logic
        //private readonly JsonSerializerSettings _jsonSerializerSettings =
        //    new JsonSerializerSettings
        //    {
        //        TypeNameHandling = TypeNameHandling.All,
        //        TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
        //    };
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly AsyncLocal<ICurrentUser> _httpcontextEnqueuedJobUserDetails = new AsyncLocal<ICurrentUser>();

        private ICurrentUser CurrentUserDetails => _httpContextAccessor.HttpContext != null
            ? new HttpContextUserDetails(_httpContextAccessor.HttpContext)
            : _httpcontextEnqueuedJobUserDetails.Value ?? EmptyUserDetails.Instance;

        public bool? IsAuthenticated => CurrentUserDetails.IsAuthenticated;

        public string Id => CurrentUserDetails.Id;


        public string TenantId => CurrentUserDetails.TenantId;

        public int? DriverId => CurrentUserDetails.DriverId;
        public int? BusinessCustomerId => CurrentUserDetails.BusinessCustomerId;
        public string UserName => CurrentUserDetails.UserName;
        public RoleType? UserType => CurrentUserDetails.UserType;
        public string Scope => CurrentUserDetails.Scope;

        public IReadOnlyCollection<string> UserPermissions => CurrentUserDetails.UserPermissions;

        public CurrentUser(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        void ICurrentUserDetailsStore.Clear()
        {
            _httpcontextEnqueuedJobUserDetails.Value = null;
        }

        void ICurrentUserDetailsStore.Deserialize(string details)
        {
            if (string.IsNullOrWhiteSpace(details))
            {
                return;
            }
            _httpcontextEnqueuedJobUserDetails.Value =
                JsonConvert.DeserializeObject<HttpContextEnqueuedJobUserDetails>(details);
        }

        string ICurrentUserDetailsStore.Serialize()
        {
            if (CurrentUserDetails == EmptyUserDetails.Instance)
            {
                return null;
            }
            var result = JsonConvert.SerializeObject(
                CurrentUserDetails,
                Formatting.None);
            return result;
        }

        private class HttpContextUserDetails : ICurrentUser
        {
            public const string AnonymousUserName = "anonymous";
            private readonly HttpContext _httpContext;

            public bool? IsAuthenticated => _httpContext.User.Identity.IsAuthenticated;

            public string Id => GetClaimValue(CustomClaimTypes.UserId);

            public int? DriverId
            {
                get
                {
                    if (int.TryParse(GetClaimValue("DriverId"), out var driverId))
                    {
                        return driverId;
                    }
                    return null;
                }
            }

            public int? BusinessCustomerId
            {
                get
                {
                    if (int.TryParse(GetClaimValue("BusinessCustomerId"), out var businessCustomerId))
                    {
                        return businessCustomerId;
                    }
                    return null;
                }
            }
            public string UserName => _httpContext.User.Identity.Name ?? AnonymousUserName;

            public string TenantId => GetClaimValue(CustomClaimTypes.TenantId);

            public RoleType? UserType
            {
                get
                {
                    if (Enum.TryParse<RoleType>(GetClaimValue(CustomClaimTypes.UserType), out var userType))
                    {
                        return userType;
                    }
                    return null;
                }
            }

            public string Scope
            {
                get
                {
                    return GetClaimValue(CustomClaimTypes.Scope);
                }
            }

            public IReadOnlyCollection<string> UserPermissions =>
                GetClaims(CustomClaimTypes.Permission).Select(claim => claim.Value).ToArray();

            public HttpContextUserDetails(HttpContext httpContext)
            {
                _httpContext = httpContext ?? throw new ArgumentNullException(nameof(httpContext));
            }

            private string GetClaimValue(string claimType)
            {
                return GetClaims(claimType).FirstOrDefault()?.Value;
            }

            private IEnumerable<Claim> GetClaims(string claimType)
            {
                return _httpContext.User
                   .Claims
                   .Where(x => x.Type == claimType);
            }
        }

        private class HttpContextEnqueuedJobUserDetails : ICurrentUser
        {
            public bool? IsAuthenticated { get; set; }

            public string Id { get; set; }

            public string TenantId { get; set; }

            public int? DriverId { get; set; }
            public int? BusinessCustomerId { set; get; }
            public string UserName { get; set; }

            public RoleType? UserType { get; set; }

            public string Scope { get; set; }

            public IReadOnlyCollection<string> UserPermissions { get; set; }
        }

        private class EmptyUserDetails : ICurrentUser
        {
            private static ICurrentUser _instance = new EmptyUserDetails();

            public static ICurrentUser Instance => _instance;

            public bool? IsAuthenticated { get; }

            public string Id { get; }

            public string TenantId { get; }

            public int? DriverId { get; }
            public int? BusinessCustomerId { get; }
            public string UserName { get; }

            public RoleType? UserType { get; }

            public string Scope { get; }

            public IReadOnlyCollection<string> UserPermissions { get; }

            private EmptyUserDetails()
            {
            }
        }
    }
}
