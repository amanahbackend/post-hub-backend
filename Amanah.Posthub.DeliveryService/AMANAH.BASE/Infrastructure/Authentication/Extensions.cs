﻿using Amanah.Posthub.BASE.Authentication;
using Microsoft.Extensions.DependencyInjection;

namespace Amanah.Posthub.SharedKernel.Infrastructure.Authentication
{
    public static class Extensions
    {
        public static IServiceCollection AddCurrentUser(this IServiceCollection services)
        {
            services.AddSingleton<CurrentUser>();
            services.AddSingleton<ICurrentUser>(ServiceProvider => ServiceProvider.GetRequiredService<CurrentUser>());
            services.AddSingleton<ICurrentUserDetailsStore>(ServiceProvider => ServiceProvider.GetRequiredService<CurrentUser>());
            return services;
        }
    }
}
