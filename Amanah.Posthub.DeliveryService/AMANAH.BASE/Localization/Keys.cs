﻿namespace Utilities.Utilites.Localization
{
    public static class Keys
    {
        public static class Messages
        {
            public const string TaskRequestIsFailed = "Messages.TaskRequestIsFailed";//"You have reached at branch {0}."
            public const string DriverReachedBranchName = "Messages.DriverReachedBranchName";//"You have reached at branch {0}."
            public const string AllNotificationsMarkedAsRead = "Messages.AllNotificationsMarkedAsRead";//"All Read"
            public const string AllNotificationsCleared = "Messages.AllNotificationsCleared";//"All Clear"
            public const string PasswordChangedSuccessfully = "Messages.PasswordChangedSuccessfully";//"Password Changed successfully"
            public const string LanguageUpdateSuccessfully = "Messages.LanguageUpdateSuccessfully";//"Languaged Updated successfully"
            public const string AccountDeletedSuccessfully = "Messages.AccountDeletedSuccessfully";//"Deleted Successfully"
            public const string DefaultBranchUpdatedsuccessfully = "Messages.DefaultBranchUpdatedsuccessfully";// "Default Branch  Updated successfully"
            public const string PACIUnavailableOrNotFound = "Messages.PACIUnavailableOrNotFound";// "Default Branch  Updated successfully"
            public const string BlockExistonGeoFence = "Messages.BlockExistonGeoFence";// "Default Branch  Updated successfully"
            public const string CountryExist = "Messages.CountryExist";// "Default Branch  Updated successfully"
            public const string NoCodeFound = "Messages.NoCodeFound";
            public const string ContractExist = "Messages.ContractExistWithContractDateAndStartEndDate";
            public const string LengthExist = "Messages.LengthExist"; 
            public const string WeightExist = "Messages.WeightExist"; 
            public const string GovernrateExist = "Messages.GovernrateExist"; 
            public const string ItemReturnReasonExist = "Messages.ItemReturnReasonExist"; 
            public const string MailItemTypeExist = "Messages.MailItemTypeExist"; 
            public const string DepartmentExist = "Messages.DepartmentExist"; 
            public const string AreaExist = "Messages.AreaExist";
            public const string BranchExist = "Messages.BranchExist";
            public const string DriverRoleExist = "Messages.DriverRoleExist";
            public const string PackingPackageExist = "Messages.PackingPackageExist";
            public const string ServiceExist = "Messages.ServiceExist";
            public const string RFQWithSamepqDateAndValidatetoDateExist = "Messages.RFQWithSamepqDateAndValidatetoDateExist";
            public const string RemarkExist = "Messages.RemarkExist";
            public const string WorkorderRecieved = "Messages.WorkorderRecievedOrClosed";
            public const string WorkorderDoNotHaveItems = "Messages.WorkorderDoNotHaveItems";
            public const string TermsAndConditionsExist = "Messages.TermsAndConditionsExist";
            public const string MailitemRequired = "Messages.MailitemRequired";
            public const string OnlyOneMailitemForInternationalOrder = "Messages.OnlyOneMailitemForInternationalOrder";

            public const string MaxFileSize = "Messages.MaxFileSize"; //"Max size of file is 1 MB"
            public const string MaxCountOfFiles = "Messages.MaxCountOfFiles"; //$"Max count of attactments is {MaxNumberOfFiles} files"

        }

        public static class Validation
        {
            public const string UnAuthorizedAccess = "Validation.UnauthorizedAccess";//"UnAuthorized Access"
            public const string NewPasswordIsSameAsOld = "Validation.NewPasswordIsSameAsOld";//"New Password should not be similar as Current password"
            public const string RetypePasswordMismatch = "Validation.RetypePasswordMismatch";//"New Password deos not match ReType Password"
            public const string ShortPassowrd = "Validation.ShortPassowrd";//"Password Must be at Least 8 Characters"
            public const string PasswordHasNoUppercaseLetter = "Validation.PasswordHasNoUppercaseLetter";//"Password Must Contain at least one Uppercase Letter"
            public const string PasswordHasNoLowercaseLetter = "Validation.PasswordHasNoLowercaseLetter";//"Password Must Contain at least one lowercase Letter"
            public const string PasswordHasNoDigit = "Validation.PasswordHasNoDigit";//"Password Must Contain at least one digit"
            public const string PasswordHasNoSpecialCharacter = "Validation.PasswordHasNoSpecialCharacter";//"Password Must Contain at least one Special Character !@#$%^&*"
            public const string NameMustBeBetweenMinAndMaxCharacters = "Validation.NameMustBeBetweenMinAndMaxCharacters";//"Name must be between {0} and {1} characters"
            public const string AccountIsNotActiveOrBlocked = "Validation.AccountIsNotActiveOrBlocked";//"Your account is not active or blocked by manager, please contact your manager"
            public const string DriverRegistrationIsPending = "Validation.RegistrationIsPending";
            public const string DriverRegistrationIsRejected = "Validation.RegistrationIsRejected";
            public const string SendResetEmailError = "Validation.SendResetEmailError";//"Can't send Reset Email"
            public const string UserHasNoRole = "Validation.UserHasNoRole";//"User dosn't have any Role !"
            public const string InvalidToken = "Validation.InvalidToken";//"Invalid Token"
            public const string InvalidUserNameOrPassword = "Validation.InvalidUserNameOrPassword";//"Invalid username or password"
            public const string UserIsDeleted = "Validation.UserIsDeleted";//"User has been deleted"
            public const string UserIsLocked = "Validation.UserIsLocked";//"User has been deleted"
            public const string PhoneNumberIsRequired = "Validation.PhoneNumberIsRequired";//"PhoneNumber is required"
            public const string NameIsRequired = "Validation.NameIsRequired";//"Name is required"
            public const string EmailIsRequired = "Validation.EmailIsRequired";//"Email is required"
            public const string AddressIsRequired = "Validation.AddressIsRequired";//"Address is required"
            public const string TaskStatusIsRequired = "Validation.TaskStatusIsRequired";//"Must enter at least one status"
            public const string TaskNotFound = "Validation.TaskNotFound";//"Task not found."
            public const string TeamIdIsRequired = "Validation.TeamIdIsRequired";//"TeamId is required"
            public const string TransportIdIsRequired = "Validation.TransportIdIsRequired";//"TransportId is required"
            public const string UserNameIsRequired = "Validation.UserNameIsRequired";//"UserName is required"
            public const string CreateMangerTeamIsRequired = "Validation.CreateMangerTeamIsRequired";//"Must enter at least one team"
            public const string GeneralError = "Validation.GeneralError";//"Unknown error occured."
            public const string EmptyRestaurantIds = "Validation.EmptyRestaurantIds";//"not allow to pass empty resturantIds"
            public const string EmptyGeoFenceIds = "Validation.EmptyGeoFenceIds";//"not allow to pass empty geoFenceIds"
            public const string EmailAlreadyExists = "Validation.EmailAlreadyExists";//"The email is already exist"
            public const string PhoneAlreadyExists = "Validation.PhoneAlreadyExists";//"The phone is already exist"
            public const string RoleAlreadyExists = "Validation.RoleAlreadyExists";//"Role name already exist."
            public const string RoleNotFound = "Validation.RoleNotFound";//"Role Not Exist"
            public const string CanNotDeleteRoleAssignedToManagers = "Validation.CanNotDeleteRoleAssignedToManagers";//"This role cannot deleted becase it assigned for managers"
            public const string CustomerAlreadyExists = "Validation.CustomerAlreadyExists";//"The customer is already exist"
            public const string CanNotDeleteCustomer = "Validation.CanNotDeleteCustomer";//"Can't Delete this Customer"
            public const string CanNotDeleteCustomers = "Validation.CanNotDeleteCustomers";//"Can't Delete those Customers"
            public const string CanNotDeleteDriver = "Validation.CanNotDeleteDriver";//"Can't Delete this driver"
            public const string CanNotDeleteDrivers = "Validation.CanNotDeleteDrivers";//"Can't Delete those drivers"
            public const string NoDriverWithThisId = "Validation.NoDriverWithThisId";//"No Driver with this id"
            public const string NoAvailableDrivers = "Validation.NoAvailableDrivers";//"No available Drivers "
            public const string DriverNotFound = "Validation.DriverNotFound";//"Driver  not found"
            public const string TeamNotFound = "Validation.TeamNotFound";//"Team not found"
            public const string DriverIsDeleted = "Validation.DriverIsDeleted";//"This user has been deleted"
            public const string DriverUserNameNotFound = "Validation.DriverUserNameNotFound";//"This username not registerd on the system"
            public const string DriverAccountBlocked = "Validation.DriverAccountBlocked";//"Your account is not active or blocked by manager, please contact your manager"
            public const string DriverLoginRequestIsPending = "Validation.DriverLoginRequestIsPending";//"this username already has pending request"
            public const string DriverLoginRequestIsAlreadyApproved = "Validation.DriverLoginRequestIsAlreadyApproved";//"You have sent already a request before and got approved"
            public const string CanNotDeleteDriverLoginRequest = "Validation.CanNotDeleteDriverLoginRequest";//"Can't Delete this driver request"
            public const string DriverAlreadyLoggedIn = "Validation.DriverAlreadyLoggedIn";//"This username already logged in"
            public const string CurrentUserIsNotDriver = "Validation.CurrentUserIsNotDriver";//"Logged user not A driver "
            public const string UserIsNotRegisteredAsDriver = "Validation.UserIsNotRegisteredAsDriver";//"This user is not registerd as Driver"
            public const string CanNotDeleteDriverTaskNotification = "Validation.CanNotDeleteDriverTaskNotification";//Can't Delete this driverTaskNotification";
            public const string CanNotCheckInTasksnNotCompleted = "Validation.CanNotCheckInTasksnNotCompleted";//"Sorry, you have tasks not completed yet, please finish them first "
            public const string CanNotCheckCurrentLocationIsNotAvailable = "Validation.CanNotCheckCurrentLocationIsNotAvailable";//"Sorry, you are not reached a branch yet"
            public const string CanNotCheckInNoGeofencesFound = "Validation.CanNotCheckInNoGeofencesFound";//"Sorry,you don't have any Geofences"
            public const string CanNotCheckInOutOfRangeOfAnyBranch = "Validation.CanNotCheckInOutOfRangeOfAnyBranch";//"Sorry, you are not reached any Geofences yet"
            public const string CanNotCheckInOutOfRangeOfAnyGeofence = "Validation.CanNotCheckInOutOfRangeOfAnyGeofence";//"Sorry, you are not reached a branch yet"
            public const string CanNotStartTaskNotInLocation = "Validation.CanNotStartTaskNotInLocation";//"You are not reached the location yet to start the task,please reach first"
            public const string CouldNotChangeDriverPassword = "Validation.CouldNotChangeDriverPassword";//"Could not Change your password, type it correctly"
            public const string CouldNotUpdateDefaultBranch = "Validation.CouldNotUpdateDefaultBranch";//"Could not update default branch
            public const string CanNotDelteAgentType = "Validation.CanNotDelteAgentType";//"Can't Delete this agentType"
            public const string CanNotDeleteTask = "Validation.CanNotDeleteTask";//"This task can't be deleted ."
            public const string CanNotUploadTaskImage = "Validation.CanNotUploadTaskImage";//"Can't Upload Task Image "
            public const string CanNotDeleteManager = "Validation.CanNotDeleteManager";//"Can't Delete this Manager"
            public const string CanNotDeleteCountry = "Validation.CanNotDeleteCountry";//"Can't Delete this country"
            public const string CanNotDeleteDispatchingManager = "Validation.CanNotDeleteDispatchingManager";//"Can't Delete this dispatching manager"
            public const string CanNotDeleteDispatchingManagers = "Validation.CanNotDeleteDispatchingManagers";//"Can't Delete those  dispatching managers"
            public const string TaskDeletedOrNotExisting = "Validation.TaskDeletedOrNotExisting";//"Task was deleted or not exist"
            public const string YouReachedPickupLocation = "Validation.YouReachedPickupLocation";//"You have reached at location"
            public const string YouDidNotReachPickupLocation = "Validation.YouDidNotReachPickupLocation";//"Sorry, You didn't reach pickup location yet"
            public const string CanNotAddTaskToCompletedMainTask = "Validation.CanNotAddTaskToCompletedMainTask";//"This Order is completed. So you cannot create task"
            public const string NoMainTaskWithThisId = "Validation.NoMainTaskWithThisId";//"There is No Main Task with This Id"
            public const string CanNotDeleteTeamItHasDrivers = "Validation.CanNotDeleteTeamItHasDrivers";//"Can't Delete this team because it's has a drivers."
            public const string CanNotDeleteTeamIHasManager = "Validation.CanNotDeleteTeamIHasManager";//"Can't Delete this team because it's has a Manager."
            public const string CanNotDeleteTeam = "Validation.CanNotDeleteTeam";//"Can't Delete this team"
            public const string UserNameAlreadyExists = "Validation.UserNameAlreadyExists";//"This User Name : {0} already exist "
            public const string CanNotDeleteSetting = "Validation.CanNotDeleteSetting";//"Can't Delete this setting"
            public const string CanNotDeleteAdmin = "Validation.CanNotDeleteAdmin";// "We Can't Delete This Admin He is the only admin on the system, you must have 1  admin on the system. "
            public const string UserNotFound = "Validation.UserNotFound";//"User not found"
            public const string InvalidloginAttempts = "Validation.InvalidloginAttempts";//"User not found"

            public const string CanNotUpdateLanguage = "Validation.CanNotUpdateLanguage";//"Could not Update Language"
            public const string CanNotUpdateProfileData = "Validation.CanNotUpdateProfileData";//"Could not update Profile Data"
            public const string InCorrectPassword = "Validation.InCorrectPassword";//"Incorrect Password"
            public const string CanNotDeleteAccount = "Validation.CanNotDeleteAccount";//"Could not Delete Account"
            public const string CanNotSetReason = "Validation.CanNotSetReason";//"Could Not Set Reason "
            public const string CanNotCreateAdminProfile = "Validation.CanNotCreateAdminProfile";//"could not Create Admin Profile"
            public const string CanNotDeleteLocationAccuracy = "Validation.CanNotDeleteLocationAccuracy";//"Can't Delete this locationAccuracy"
            public const string CanNotDeleteMainTaskType = "Validation.CanNotDeleteMainTaskType";//"Can't Delete this MainTaskType"
            public const string CanNotDeleteTaskType = "Validation.CanNotDeleteTaskType";//"Can't Delete this TaskType"
            public const string CanNotDeleteMainTaskStatus = "Validation.CanNotDeleteMainTaskStatus";//"Can't Delete this MainTaskStatus"
            public const string CanNotDeleteTaskStatus = "Validation.CanNotDeleteTaskStatus";//"Can't Delete this TaskStatus"
            public const string CanNotDeleteTaskRoute = "Validation.CanNotDeleteTaskRoute";//"Can't Delete this TaskRoute"
            public const string CanNotDeleteTransportType = "Validation.CanNotDeleteTransportType";//"Can't Delete this transportType"
            public const string TaskActionAlreadyDoneBefore = "Validation.TaskActionAlreadyDoneBefore";

            public const string MissingAttachmentInCreateDriver = "Validation.MissingAttachmentInCreateDriver";//"Missing Attachment In Create Driver"
            public const string InvalidAttachmentExtention = "Validation.InvalidAttachmentExtention";//"Invalid Attachment Extention"
            public const string MissingDriverRegistrationId = "Validation.MissingDriverRegistrationId";
            public const string MissingTenantCode = "Validation.MissingTenantCode";
            public const string NotMatchedTenantId = "Validation.NotMatchedTenantId";
            public const string RegisteredBefore = "Validation.RegisteredBefore";
            public const string TenantNotExist = "Validation.TenantNotExist";

            public const string CannotStartTaskWhileCluppingTime = "Validation.CannotStartTaskWhileCluppingTime";

            public const string TenantEmailIsAlreadyExist = "Validation.TenantEmailIsAlreadyExist";
            public const string TenantPhoneIsAlreadyExist = "Validation.TenantPhoneIsAlreadyExist";
            public const string TenantCIDIsAlreadyExist = "Validation.TenantCIDIsAlreadyExist";
            public const string FailSendEmail = "Validation.FailSendEmail";
            public const string SocialMediaNotNull = "Validation.SocialMediaNotNull";
            public const string AttachmentNotNull = "Validation.AttachmentNotNull";

            public const string AutoAllocationMethodIsMissing = "Validation.AutoAllocationMethodIsMissing";
            public const string MissingDriverPassword = "Validation.MissingDriverPassword";
            public const string PlatformSettingMissing = "Validation.PlatformSettingMissing";
            public const string NotAssignedToYourAccount = "Validation.NotAssignedToYourAccount";
        }

        public static class General
        {
            public const string On = "General.On";// "on";
            public const string Off = "General.Off";// "off";
            public const string HasBeenDeleted = "General.HasBeenDeleted";//"has been deleted";
        }

        public static class Notifications
        {
            public const string YourAccountBlocked = "Notifications.YourAccountBlocked";//"You are blocked from admin ."
            public const string AdminPutYouOnDuty = "Notifications.AdminPutYouOnDuty";//"You have been put on duty by the manager"
            public const string AdminPutYouOffDuty = "Notifications.AdminPutYouOffDuty";//"You have been put off duty by the manager"
            public const string AdminEndedYourSession = "Notifications.AdminEndedYourSession";//"The admin logged you out ."
            public const string DriverHasBeenLoggedOut = "Notifications.DriverHasBeenLoggedOut";//"Driver has been logged Out"
            public const string DriverNameHasBeenLoggedOut = "Notifications.DriverNameHasBeenLoggedOut";//"{0} has been logged Out."
            public const string DriverHasLoggedIn = "Notifications.DriverHasLoggedIn";//"Driver has been logged in"
            public const string DriverNameHasLoggeIn = "Notifications.DriverNameHasLoggeIn";//"{0} has logged in."
            public const string AdminApprovedLoginRequest = "Notifications.AdminApprovedLoginRequest";//"The admin approved your login request . true"
            public const string AdminRejectedLoginRequest = "Notifications.AdminRejectedLoginRequest";//"The admin rejected your login request . false"
            public const string DriverSentLoginRequest = "Notifications.DriverSentLoginRequest";//"New Login Request"
            public const string DriverNameSentLoginRequest = "Notifications.DriverNameSentLoginRequest";//"{0} has sent a login request."
            public const string DriverLoginRequestCanceled = "Notifications.DriverLoginRequestCanceled";//"Login Request Canceled"
            public const string DriverNameLoginRequestCanceled = "Notifications.DriverNameLoginRequestCanceled";//"{0} Login Request Canceled"
            public const string DriverSetDuty = "Notifications.DriverSetDuty";//"Driver has been set duty"
            public const string DriverNameSetDutyVaue = "Notifications.DriverNameSetDutyVaue";//"{0} Set {1} duty "
            public const string DriverAcceptedTask = "Notifications.DriverAcceptedTask";//"Task Accepted"
            public const string DriverNameAcceptedTaskNo = "Notifications.DriverNameAcceptedTaskNo";//"{0} has accepted task no. {1}"
            public const string DriverDeclinedTask = "Notifications.DriverDeclinedTask";//"Task Declined"
            public const string DriverNameDeclinedTaskNo = "Notifications.DriverNameDeclinedTaskNo";//"{0} has declined task no. {1}"
            public const string DriverFailedTask = "Notifications.DriverFailedTask";//"Task Failed"
            public const string DriverNameFailedTaskNo = "Notifications.DriverNameFailedTaskNo";//"{0} has failed task no. {1}"
            public const string DriverCompletedTask = "Notifications.DriverCompletedTask";//"Task Successfull"
            public const string DriverNameCompletedTask = "Notifications.DriverNameCompletedTask";//$"{0} has successfully completed task no. {1}"
            public const string DriverCanceledTask = "Notifications.DriverCanceledTask";//"Cancel Task"
            public const string DriverNameCanceledTaskNo = "Notifications.DriverNameCanceledTaskNo";//"{0} has cancelled task no. {1}"
            public const string DriverStartedTask = "Notifications.DriverStartedTask";//"Task started"
            public const string DriverNameStartedTaskNo = "Notifications.DriverNameStartedTaskNo";//"{0}  has started task no. {1}"
            public const string TaskCreateAndAutoAllocationStartedTitle = "Notifications.TaskCreateAndAutoAllocationStartedTitle";//"Task Created and The Auto Allocation Started"
            public const string TaskCreateAndAutoAllocationStartedBody = "Notifications.TaskCreateAndAutoAllocationStartedBody";//"The task has been created and the auto allocation process has been started."
            public const string TaskAutoAllocationSucceded = "Notifications.TaskAutoAllocationSucceded";//"Auto Allocation Assigned  Sucessfully"
            public const string TaskAutoAllocationSuccededDetailsTaskId = "Notifications.TaskAutoAllocationSuccededDetailsTaskId";//"Auto allocation process has been Assigned. For main task with Id {0}"
            public const string TaskAutoAllocationFailed = "Notifications.TaskAutoAllocationFailed";//"Auto Allocation Failed"
            public const string TaskAutoAllocationFailedDetailsTaskId = "Notifications.TaskAutoAllocationFailedDetailsTaskId";//"Auto allocation process has been failed, please to assign it mananualy. For main task with Id {0}"
            public const string TaskNoHasBeenDeletedFromYou = "Notifications.TaskNoHasBeenDeletedFromYou";//"Task {0} has been deleted from you"
            public const string TaskNoHasBeenUpdated = "Notifications.TaskNoHasBeenUpdated";//"Task Id {0} has been updated"
            public const string NewTasksAssignedToYou = "Notifications.NewTasksAssignedToYou";//"New task (s) has been assigned to you"
            public const string YourAreReadyToStartTasks = "Notifications.YourAreReadyToStartTasks";//You are ready to start working on your tasks
            public const string TaskPendingForYourAcceptance = "Notifications.TaskPendingForYourAcceptance";//"You have been recived a Task waiting for your acceptance ..  "
            public const string TaskNoActionByUserName = "Notifications.TaskNoActionByUserName";//"Task ID :{0} {1} by {2}"

            public const string NewWorkOrderAssignedToYou = "Notifications.NewWorkOrderAssignedToYou";//"New task (s) has been assigned to you"

        }

        public static class PermissionFeatures
        {
            public const string Task = "Permissions.Features.Task";// "Order Permissions"
            public const string Agent = "Permissions.Features.Agent";// "Driver Permissions"
            public const string PlatformAgent = "Permissions.Features.PlatformAgent";// "Driver Permissions"
            public const string Customer = "Permissions.Features.Customer";// "Customer Permissions"
            public const string Teams = "Permissions.Features.Team";// "Team Permissions"
            public const string Settings = "Permissions.Features.Settings";//"Settings Permissions"
            public const string Managers = "Permissions.Features.Managers";//"Managers Permissions"
            public const string Geofences = "Permissions.Features.Geofences";//"Geofences Permissions"
            public const string Restaurants = "Permissions.Features.Restaurants";//"Restaurant Permissions"
            public const string Branches = "Permissions.Features.Branches";//"Branch Permissions"
            public const string DispatchingManager = "Permissions.Features.DispatchingManagers";//"Manager dispatching  Permissions"
            public const string Reports = "Permissions.Features.Reports";//"Managers Permissions"
            public const string Profile = "Permissions.Features.Profile";//"Geofences Permissions"
            public const string AccountLogs = "Permissions.Features.AccountLogs";//"AccountLogs Permissions"

            public const string Roles = "Permissions.Features.Roles";
            public const string CompanyProfile = "Permissions.Features.CompanyProfile";
            public const string Tenants = "Permissions.Features.Tenants";
            public const string BusinessCustomer = "Permissions.Features.BusinessCustomer";//"Branch Permissions"
            public const string BusinessOrders = "Permissions.Features.BusinessOrders";//"Geofences Permissions"
            public const string Orders = "Permissions.Features.Orders";//"Geofences Permissions"
            public const string InternalPosts = "Permissions.Features.InternalPosts";
            public const string WorkOrders = "Permissions.Features.WorkOrders";//"Geofences Permissions"
            public const string PersonalAccount = "Permissions.Features.PersonalAccount";//"Geofences Permissions"
            public const string Contracts = "Permissions.Features.Contracts";//"Geofences Permissions"
            public const string PriceList = "Permissions.Features.PriceList";//"Geofences Permissions"
            public const string PriceQoutation = "Permissions.Features.PriceQoutation";//"Geofences Permissions"

            public const string Areas = "Permissions.Features.Areas";// "Areas Permissions"
            public const string Countries = "Permissions.Features.Countries";// "Countries Permissions"
            public const string Governorates = "Permissions.Features.Governorates";// "Governorates Permissions"
            public const string Languages = "Permissions.Features.Languages";// "Languages Permissions"
            public const string Departments = "Permissions.Features.Departments";
            public const string DriverRoles = "Permissions.Features.DriverRoles";
            public const string Services = "Permissions.Features.Services";
            public const string PackingPackages = "Permissions.Features.PackingPackages";
            public const string LengthUOMs = "Permissions.Features.LengthUOMs";// "LengthUOMs Permissions"
            public const string WeightUOMs = "Permissions.Features.WeightUOMs";// "WeightUOMs Permissions"
            public const string ItemTypes = "Permissions.Features.ItemTypes";// "ItemTypes Permissions"
            public const string ItemReturnReasons = "Permissions.Features.ItemReturnReasons";// "ItemReturnReasons Permissions"
            public const string Staffs = "Permissions.Features.Staff";// "Staff Permissions"
            public const string Drivers = "Permissions.Features.Drivers";// "Driver Permissions"
            public const string OrdersTracking = "Permissions.Features.OrdersTracking";// "OrdersTracking Permissions"
            public const string Invoices = "Permissions.Features.Invoices";// "Invoices Permissions"
            public const string Remarks = "Permissions.Features.Remarks";

            public const string BusinessTypes = "Permissions.Features.BusinessTypes";//"BusinessTypes Permissions"
            public const string TermsAndConditions = "Permissions.Features.TermsAndConditions";//"TermsAndConditions Permissions"


        }
        // permission values are used as a key

        public static class FeatureManagementFlags
        {
            public const string Concierge = "Concierge";
            public const string Express = "Express";
            public const string KFH = "KFH";
        }
    }
}
