﻿using NLog;
namespace Amanah.Posthub.SharedKernel.LoggingService
{
  
    public class LoggerManager : ILoggerManager
    {


        private static ILogger logger = LogManager.GetCurrentClassLogger();
        public void LogDebug(string message) => logger.Debug(message);
        public void LogError(string message) => logger.Error(message);
        public void LogInfo(string message) => logger.Info(message);
        public void LogWarn(string message) => logger.Warn(message);

        #region Singlton pattern to get only one instanse of loggerManager
        static LoggerManager instance;
        // Lock synchronization object
        private static object locker = new object();

        private LoggerManager() { }

        public static ILoggerManager GetLoggerInstance()
        {
            // Support multithreaded applications through 'Double checked locking' pattern which (once the instance exists) avoids locking each time the method is invoked
            if (instance == null)
            {
                lock (locker)
                {
                    if (instance == null)
                        instance = new LoggerManager();
                }
            }
            return instance;
        }
        #endregion
    }





}
