﻿using System.Threading.Tasks;

namespace Amanah.Posthub.BASE.Services
{
    public interface IRealTimeNotifier
    {
        public Task SendAsync(string toUserId, string notificationName, string message, object payload);
    }
}
