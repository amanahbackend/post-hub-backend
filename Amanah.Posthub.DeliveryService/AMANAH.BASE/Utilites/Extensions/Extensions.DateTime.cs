﻿using System;

namespace Utilities.Extensions
{
    public static partial class Extensions
    {
        public static DateTime BeginingOfDay(this DateTime dateTime)
        {
            return dateTime.Date;
        }

        public static DateTime EndOfDay(this DateTime dateTime)
        {
            return dateTime.Date.AddDays(1).AddTicks(-1);
        }

        public static bool IsFuture(this DateTime dateTime)
        {
            return dateTime > DateTime.UtcNow;//may need utcNow
        }

        public static bool IsPast(this DateTime dateTime)
        {
            return dateTime < DateTime.UtcNow;//may need utcNow
        }

        public static bool IsToday(this DateTime dateTime)
        {
            return dateTime.Date == DateTime.UtcNow.Date;//may need utcNow
        }

        public static DateTime EndOfDayOrNowIfToday(this DateTime dateTime)
        {
            var endOfDay = dateTime.EndOfDay();
            if (dateTime.IsFuture())
            {
                return dateTime;
            }
            else if (dateTime.IsPast() && dateTime.IsToday())
            {
                return DateTime.UtcNow;
            }
            else
            {
                return dateTime.EndOfDay();
            }
        }

        public static TimeSpan FixFraction(this TimeSpan time)
        {
            return TimeSpan.FromMinutes(Math.Round(time.TotalMinutes, MidpointRounding.AwayFromZero));
            //if(time.Seconds == 59 && time.Milliseconds == 999)
            //{
            //    Math.
            //    return time
            //}
        }
    }
}
