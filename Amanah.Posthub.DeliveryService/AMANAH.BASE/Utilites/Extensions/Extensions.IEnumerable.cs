﻿using System.Collections.Generic;
using System.Linq;

namespace Utilities.Extensions
{
    public static partial class Extensions
    {
        public static IEnumerable<T> SelectMany<T>(this IEnumerable<IEnumerable<T>> source)
        {
            return source.SelectMany(_ => _);
        }

        public static IEnumerable<T> GetDuplicates<T>(this IEnumerable<T> source)
        {
            return source.GroupBy(item => item)
                .Where(group => group.Count() > 1)
                .SelectMany()
                .Distinct();
        }

        public static string Join(this IEnumerable<string> source, string separator)
        {
            return string.Join(separator, source);
        }

        public static bool IsSingleItem<T>(this IEnumerable<T> items)
        {
            return items?.Count() == 1;
        }

        public static IEnumerable<T> WhereIsOddIndex<T>(this IEnumerable<T> items)
        {
            return items.Where((_, index) => index.IsOdd());
        }

        public static IEnumerable<T> WhereIsEvenIndex<T>(this IEnumerable<T> items)
        {
            return items.Where((_, index) => index.IsEven());
        }
    }
}
