﻿namespace Utilities.Extensions
{
    public static partial class Extensions
    {
        public static bool IsEven(this int number) => number % 2 == 0;

        public static bool IsOdd(this int number) => number % 2 != 0;
    }
}
