﻿using System.Globalization;

namespace Utilities.Extensions
{
    public static partial class Extensions
    {
        public static bool IsArabic(this CultureInfo cultureInfo)
        {
            return cultureInfo.Name == "ar";
        }

        public static bool IsEnglish(this CultureInfo cultureInfo)
        {
            return cultureInfo.Name == "en";
        }
    }
}
