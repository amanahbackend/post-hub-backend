﻿using System;
using System.Linq;

namespace Utilities.Extensions
{
    public static partial class Extensions
    {
        public static bool IsConcrete(this Type type)
        {
            return type.IsClass && !type.IsAbstract && !type.ContainsGenericParameters;
        }

        public static bool IsOfType<TType>(this Type type)
        {
            return typeof(TType).IsAssignableFrom(type);
        }

        public static bool IsOfGenericType(this Type type, Type genericType)
        {
            return
                (type.IsGenericType &&
                 type.GetGenericTypeDefinition() == genericType) ||
                type.GetInterfaces()
                    .Any(@interface => @interface.IsOfGenericType(genericType)) ||
                type.BaseType?.IsOfGenericType(genericType) == true;
        }
    }
}
