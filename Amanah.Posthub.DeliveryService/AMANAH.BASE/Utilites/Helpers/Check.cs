﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Amanah.Posthub.SharedKernel.Utilites.Helpers
{
    [DebuggerStepThrough]
    public static class Check
    {
        public static T NotNull<T>(T value, [NotNull] string parameterName)
        {
            //#pragma warning disable IDE0041 // Use 'is null' check
            if (ReferenceEquals(value, null))
            //#pragma warning restore IDE0041 // Use 'is null' check
            {
                NotEmpty(parameterName, nameof(parameterName));
                throw new ArgumentNullException(parameterName);
            }
            return value;
        }

        public static IReadOnlyCollection<T> NotEmpty<T>(
            IReadOnlyCollection<T> value, [NotNull] string parameterName)
        {
            NotNull(value, parameterName);
            if (value.Count == 0)
            {
                NotEmpty(parameterName, nameof(parameterName));
                throw new ArgumentException(CollectionArgumentIsEmpty(parameterName));
            }
            return value;
        }

        public static T NotNegative<T>(T value, string parameterName) where T : struct, IComparable
        {
            if (value.CompareTo(default(T)) < 0)
            {
                throw new ArgumentException("Value can not be negative", parameterName);
            }
            return value;
        }

        public static IEnumerable<T> NotEmpty<T>(
            IEnumerable<T> value, [NotNull] string parameterName)
        {
            NotNull(value, parameterName);
            if (!value.Any())
            {
                NotEmpty(parameterName, nameof(parameterName));
                throw new ArgumentException(CollectionArgumentIsEmpty(parameterName));
            }
            return value;
        }

        public static string NotEmpty(string value, [NotNull] string parameterName)
        {
            var exception = default(Exception);
            if (value is null)
            {
                exception = new ArgumentNullException(parameterName);
            }
            else if (value.Trim().Length == 0)
            {
                exception = new ArgumentException(ArgumentIsEmpty(parameterName));
            }
            if (exception != null)
            {
                NotEmpty(parameterName, nameof(parameterName));
                throw exception;
            }
            return value;
        }

        public static string NullButNotEmpty(string value, [NotNull] string parameterName)
        {
            if (!(value is null) && value.Length == 0)
            {
                NotEmpty(parameterName, nameof(parameterName));
                throw new ArgumentException(ArgumentIsEmpty(parameterName));
            }
            return value;
        }

        public static IReadOnlyList<T> HasNoNulls<T>(
            IReadOnlyList<T> value, [NotNull] string parameterName)
            where T : class
        {
            NotNull(value, parameterName);
            if (value.Any(e => e == null))
            {
                NotEmpty(parameterName, nameof(parameterName));
                throw new ArgumentException(parameterName);
            }
            return value;
        }

        public static T Between<T>(
            T value,
            T rangeBegin,
            T rangeEnd,
            [NotNull] string parameterName)
        where T : struct, IComparable
        {
            var smallerThanBegin = value.CompareTo(rangeBegin) < 0;
            var biggerThanEnd = value.CompareTo(rangeEnd) > 0;
            if (smallerThanBegin || biggerThanEnd)
            {
                throw new ArgumentOutOfRangeException(parameterName);
            }
            return value;
        }

        public static T ShouldBe<T>(
            this T value,
            T expected,
            [NotNull] string parameterName,
            string message = null)
            where T : IComparable
        {
            if (value.CompareTo(expected) != 0)
            {
                throw new ArgumentException(message ?? "parameter value is not as expected.", parameterName);
            }
            return value;
        }

        private static string ArgumentIsEmpty(string argumentName)
        {
            return $"The string argument '{argumentName}' cannot be empty.";
        }

        private static string CollectionArgumentIsEmpty(string argumentName)
        {
            return $"The collection argument '{argumentName}' must contain at least one element.";
        }
    }
}
