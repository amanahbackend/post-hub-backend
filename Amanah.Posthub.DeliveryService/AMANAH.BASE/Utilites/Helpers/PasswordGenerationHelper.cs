﻿using System;

namespace Utilities.Utilites
{
    public class PasswordGenerationHelper
    {

        public static string Generate(int length, int nonAlphaNumericChars)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            string allowedNonAlphaNum = "!@#$%^&*()_-+=[{]};:<>|./?";
            Random rd = new Random();

            if (nonAlphaNumericChars > length || length <= 0 || nonAlphaNumericChars < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            char[] pass = new char[length];
            int[] pos = new int[length];
            int i = 0;
            while (i < length - 1)
            {
                bool flag = false;
                int temp = rd.Next(0, length);
                int j;
                for (j = 0; j < length; j++)
                {
                    if (temp == pos[j])
                    {
                        flag = true;
                        j = length;
                    }
                }
                if (!flag)
                {
                    pos[i] = temp;
                    i++;
                }
            }

            for (i = 0; i < length - nonAlphaNumericChars; i++)
            {
                pass[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            for (i = length - nonAlphaNumericChars; i < length; i++)
            {
                pass[i] = allowedNonAlphaNum[rd.Next(0, allowedNonAlphaNum.Length)];
            }
            char[] sorted = new char[length];
            for (i = 0; i < length; i++)
            {
                sorted[i] = pass[pos[i]];
            }
            return new string(sorted);
        }
    }
}
