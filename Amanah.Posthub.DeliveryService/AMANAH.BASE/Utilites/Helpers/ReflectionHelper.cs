﻿using System;
using System.Reflection;

namespace Utilites
{
    public static class ReflectionHelper
    {
        public static bool IsDefaultValue(object value)
        {
            return value == null || value.Equals(GetDefaultValue(value.GetType()));
        }

        public static object GetDefaultValue(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        public static MethodInfo GetStaticMethod<T>(string methodName)
        {
            return GetStaticMethod(typeof(T), methodName);
        }

        public static MethodInfo GetStaticMethod(Type type, string methodName)
        {
            return type.GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
        }
    }
}
