﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Amanah.Posthub.SharedKernel.Utilites.Helpers
{
    public static class TimeZoneUtility
    {
        public static TimeZoneInfo GetCurrent(string city)
        {
            ReadOnlyCollection<TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones();
            var currentTimeZone = zones.Where(zone => zone.StandardName.ToLower().Contains(city.ToLower().Trim())
                || zone.DisplayName.ToLower().Contains(city.ToLower().Trim())
                || zone.Id.ToLower().Trim() == city.ToLower().Trim())
                .FirstOrDefault();
            return currentTimeZone;
        }
    }
}
