﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Amanah.Posthub.SharedKernel.Utilites.Helpers
{
    public static class TypeFinder
    {
        private static readonly object _lockObject = new object();
        private static WeakReference<IReadOnlyCollection<Type>> _cachedTypes;

        public static IReadOnlyCollection<Type> FindTypes(Func<Type, bool> filter)
        {
            return FindTypes()
                .Where(filter)
                .ToList();
        }

        public static IReadOnlyCollection<Type> FindTypes()
        {
            lock (_lockObject)
            {
                if (_cachedTypes == null || !_cachedTypes.TryGetTarget(out var types))
                {
                    types = GetAllTypes();
                    _cachedTypes = new WeakReference<IReadOnlyCollection<Type>>(types);
                }
                return types;
            }
        }

        private static IReadOnlyCollection<Type> GetAllTypes()
        {
            return AssemblyFinder.FindApplicationAssemblies()
                .SelectMany(assembly => assembly.GetTypes())
                .ToArray();
        }

        internal static class AssemblyFinder
        {
            private static readonly Lazy<Assembly[]> _cachedAssemblies =
                new Lazy<Assembly[]>(InnerFindApplicationAssemblies);

            public static string ApplicationNamespace => $"{nameof(Amanah)}";

            public static Assembly[] FindApplicationAssemblies()
            {
                return _cachedAssemblies.Value;
            }

            private static Assembly[] InnerFindApplicationAssemblies()
            {
                return FindAssemblies(assemblyPath => Path.GetFileName(assemblyPath).StartsWith(ApplicationNamespace))
                    .ToArray();
            }

            private static IEnumerable<Assembly> FindAssemblies(Func<string, bool> filter)
            {
                var applicationPath = AppDomain.CurrentDomain.BaseDirectory;
                return FindAssemblies(applicationPath, filter);
            }

            private static IEnumerable<Assembly> FindAssemblies(string assemblyPath, Func<string, bool> filter)
            {
                var assemblyNames = Directory
                    .EnumerateFiles(assemblyPath, "*.dll", SearchOption.AllDirectories)
                    .Where(filter)
                    .ToArray();
                foreach (string assemblyName in assemblyNames)
                {
                    var nameWithoutExtension = Path.GetFileNameWithoutExtension(assemblyName);
                    Assembly assembly = null;
                    try
                    {
                        assembly = AppDomain.CurrentDomain.Load(nameWithoutExtension);
                    }
                    catch
                    {
                        try
                        {
                            assembly = Assembly.LoadFrom(assemblyName);
                        }
                        catch
                        {
                            LogFailure(assemblyName);
                        }
                    }
                    if (assembly != null)
                    {
                        yield return assembly;
                    }
                }
            }

            private static void LogFailure(string assemblyName)
            {
                Debugger.Log(0, null, $"ERROR: Failed to load assembly {assemblyName}");
            }
        }
    }
}
