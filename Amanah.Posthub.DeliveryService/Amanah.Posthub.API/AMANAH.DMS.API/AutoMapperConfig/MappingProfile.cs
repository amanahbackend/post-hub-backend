﻿using Amanah.Posthub.BLL;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.BLL.Notifications.DTOs;
using Amanah.Posthub.BLL.TasksFolder.ViewModels.MainTasksDTO;
using Amanah.Posthub.BLL.TenantRegistrations.Services.DTO;
using Amanah.Posthub.BLL.UserManagement.ViewModels;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Account;
using Amanah.Posthub.BLL.ViewModels.Admin;
using Amanah.Posthub.BLL.ViewModels.Driver;
using Amanah.Posthub.BLL.ViewModels.Mobile;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using Amanah.Posthub.DeliverService.BLL.Addresses.ViewModels;
using Amanah.Posthub.DeliverService.BLL.Customers.ViewModels;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Admins.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.Service.Domain.Notifications.Entities;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.Service.Domain.TermsAndConditions.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Amanah.Posthub.ViewModel;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using MapsUtilities.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.API.AutoMapperConfig
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<TaskDriverRequests, TaskDriverRequestViewModel>(MemberList.None);

            CreateMap<TaskDriverRequestViewModel, TaskDriverRequests>(MemberList.None)
                .ForMember(dest => dest.ResponseStatus, opt => opt.MapFrom(src => (int)src.ResponseStatus));

            CreateMap<ApplicationUser, UserViewModel>(MemberList.None)
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + ' ' + src.MiddleName + ' ' + src.LastName));
            CreateMap<IdentityResult, UserManagerResult>(MemberList.None);

            CreateMap<UserDevice, UserDeviceViewModel>(MemberList.None);
            CreateMap<UserDeviceViewModel, UserDevice>(MemberList.None);


            CreateMap<CreateDriverRateViewModel, DriverRateViewModel>(MemberList.None);

            CreateMap<DriverRate, DriverRateViewModel>(MemberList.None);
            CreateMap<DriverRateViewModel, DriverRate>(MemberList.None);

            CreateMap<ApplicationRoleViewModel, ApplicationRole>(MemberList.None)
              .ForMember(dest => dest.NormalizedName, opt => opt.Ignore())
              .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore());
            CreateMap<ApplicationRole, ApplicationRoleViewModel>(MemberList.None);

            CreateMap<ApplicationUserViewModel, ApplicationUser>(MemberList.None)
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());


            CreateMap<ApplicationUser, ApplicationUserViewModel>(MemberList.None)
            .ForMember(dest => dest.Password, opt => opt.Ignore());


            CreateMap<PagedResult<ApplicationUser>, PagedResult<ApplicationUserViewModel>>(MemberList.None);
            CreateMap<EditApplicationUserViewModel, ApplicationUser>(MemberList.None)
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());

            CreateMap<ApplicationUser, EditApplicationUserViewModel>(MemberList.None);
            CreateMap<EditUserProfileViewModel, ApplicationUser>(MemberList.None);

            //-------------  Settings Profiles ---------------------------------//

            CreateMap<Setting, SettingViewModel>(MemberList.None);
            CreateMap<SettingViewModel, Setting>(MemberList.None);

            CreateMap<LocationAccuracy, LocationAccuracyViewModel>(MemberList.None);
            CreateMap<LocationAccuracyViewModel, LocationAccuracy>(MemberList.None);
            CreateMap<Team, TeamViewModel>(MemberList.None)
                 .ForMember(dest => dest.LocationAccuracyName, opt => opt.MapFrom(src => src.LocationAccuracy.Name));
            CreateMap<TeamViewModel, Team>(MemberList.None)
                 .ForMember(dest => dest.Drivers, opt => opt.Ignore());

            CreateMap<AgentType, AgentTypeViewModel>(MemberList.None);
            CreateMap<AgentTypeViewModel, AgentType>(MemberList.None);


            CreateMap<TransportType, TransportTypeViewModel>(MemberList.None);
            CreateMap<TransportTypeViewModel, TransportType>(MemberList.None);

            CreateMap<CreateUpdateGovernarateViewModel, Governrate>(MemberList.None);

            CreateMap<Governrate, GovernarateListViewModel>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
              .ForMember(dest => dest.IsDeleted, opt => opt.MapFrom(src => src.IsDeleted))
              .ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
              .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.Name))
              .ForMember(dest => dest.NameEN, opt => opt.MapFrom(src => src.NameEN))
              .ForMember(dest => dest.NameAR, opt => opt.MapFrom(src => src.NameAR));

            CreateMap<CountryResultDto, Country>(MemberList.None);
            CreateMap<Country, CountryResultDto>(MemberList.None)
                               .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                               .ForMember(dest => dest.CourierZoneId, opt => opt.MapFrom(src => src.CourierZoneId))
                               .ForMember(dest => dest.CourierZoneName, opt => opt.MapFrom(src => src.CourierZone.Name))
                               .ForMember(dest => dest.TopLevel, opt => opt.MapFrom(src => src.TopLevel))
                               .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
                               .ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
                               .ForMember(dest => dest.Flag, opt => opt.MapFrom(src => src.Flag))
                               .ForMember(dest => dest.FlagUrl, opt => opt.MapFrom((src) => "CountryFlags/" + src.Flag));


            CreateMap<Customer, CustomerResultDto>(MemberList.None);
            CreateMap<Customer, CustomerResultDTO>(MemberList.None);
            CreateMap<CustomerResultDto, Customer>(MemberList.None)
                .ForMember(dest => dest.Country, opt => opt.Ignore());
            CreateMap<ImportCustomerResultDto, CustomerResultDto>(MemberList.None)
              .ForMember(dest => dest.Country, opt => opt.Ignore());

            CreateMap<Customer, CustomerNamesDto>(MemberList.None)
             .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name)
             );

            CreateMap<Customer, CustomerExportToExcelViewModel>(MemberList.None)
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
              .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.Tags))
              .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
              .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
              .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
              .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country.Name));

            //-------------  Driver Profiles ---------------------------------//
            CreateMap<DriverViewModel, Driver>(MemberList.None);
            CreateMap<Driver, DriverViewModel>(MemberList.None)
              .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
              .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName))
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.User.LastName) ? "" : src.User.LastName))
              .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl.Replace("DriverImages", "")))
              .ForMember(dest => dest.AgentTypeName, opt => opt.MapFrom(src => src.AgentType.Name))
              .ForMember(dest => dest.AgentStatusName, opt => opt.MapFrom(src => src.AgentStatus != null ? src.AgentStatusId == 6 ? "Available" : src.AgentStatus.Name : string.Empty))
              .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
              .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name))
              .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.Name))
              .ForMember(dest => dest.CountryCode, opt => opt.MapFrom(src => src.Country.Code))
              .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name))
              .ForMember(dest => dest.DeviceType, opt => opt.MapFrom(src => src.DeviceType))
              .ForMember(dest => dest.Version, opt => opt.MapFrom(src => src.Version))
              .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.Latitude))
              .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.Longitude))
              .ForMember(dest => dest.LocationAccuracyName, opt => opt.MapFrom(src => src.Team.LocationAccuracy.Name))
              .ForMember(dest => dest.LocationAccuracyDuration, opt => opt.MapFrom(src => src.Team.LocationAccuracy.Duration))
              .ForMember(dest => dest.BranchId, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.BranchId));




            CreateMap<Driver, DriverNearestAvailableDTO>(MemberList.None)
             .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.Latitude))
             .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.Longitude))
             ;







            CreateMap<Driver, DriverExportToExcelViewModel>(MemberList.None)
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
              .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FirstName + ' ' + src.User.LastName))
              .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
              .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.Tags))
              .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name))
              .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName))
              .ForMember(dest => dest.Version, opt => opt.MapFrom(src => src.Version))
              .ForMember(dest => dest.DeviceType, opt => opt.MapFrom(src => src.DeviceType));


            CreateMap<BaseDriverFreeLancerRegistrationDTO, CreateDriverRegistrationInputDTO>(MemberList.None);
            CreateMap<CreateDriverTenantRegistrationDTO, CreateDriverRegistrationInputDTO>(MemberList.None);

            CreateMap<TenantRegistration, ApplicationUser>(MemberList.None)
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => HandleFirstLastName(src.OwnerName, NameType.First)))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => HandleFirstLastName(src.OwnerName, NameType.Last)));

            CreateMap<ApproveTenantRegistrationDTO, ApproveSettings>(MemberList.None).ReverseMap();

            CreateMap<ApproveDriverRegistrationRequestInputDTO, CreateDriverViewModel>(MemberList.None);
            CreateMap<ApproveTenantRegistrationDTO, ApprovedTenantDTO>(MemberList.None);

            CreateMap<Driver, DriverWithTasksViewModel>(MemberList.None)
                .IncludeBase<Driver, DriverViewModel>()
                .ForMember(dest => dest.AssignedMainTasks, opt => opt.Ignore());

            CreateMap<AddressResultDto, PACI>(MemberList.None);
            CreateMap<AddressResultDto, Address>(MemberList.None);
            CreateMap<Address, AddressResultDto>(MemberList.None);



            CreateMap<ImportDriverViewModel, ApplicationUserViewModel>(MemberList.None);
            CreateMap<ImportDriverViewModel, DriverViewModel>(MemberList.None);

            CreateMap<CreateDriverViewModel, ApplicationUserViewModel>(MemberList.None);
            CreateMap<CreateDriverViewModel, Driver>(MemberList.None);
            CreateMap<EditDriverViewModel, ApplicationUserViewModel>(MemberList.None)
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId));

            CreateMap<EditDriverViewModel, DriverViewModel>(MemberList.None);
            CreateMap<Driver, DriverForAssignViewModel>(MemberList.None)
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FirstName + " " + src.User.LastName))
              .ForMember(dest => dest.AgentStatusName, opt => opt.MapFrom(src => src.AgentStatus.Name));


            CreateMap<Driver, DriverTeamViewModel>(MemberList.None)
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FirstName + " " + src.User.LastName));
            CreateMap<TeamManagerViewModel, TeamManager>(MemberList.None);
            CreateMap<TeamManager, TeamManagerViewModel>(MemberList.None)
              .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name));
            CreateMap<ManagerViewModel, Manager>(MemberList.None);
            CreateMap<Manager, ManagerViewModel>(MemberList.None)
              .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
              .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName))
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.Id))
              .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.User.CountryId))
              .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.User.Country));

            CreateMap<Driver, DriverTransportionTypeViewModel>(MemberList.None)
              .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name));

            CreateMap<ManagerViewModel, ApplicationUserViewModel>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId));
            CreateMap<CreateManagerViewModel, ApplicationUserViewModel>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.Ignore());
            CreateMap<CreateManagerViewModel, ManagerViewModel>(MemberList.None);


            CreateMap<TaskRoute, TaskRouteDtoInput>(MemberList.None);
            CreateMap<TaskRouteDtoInput, TaskRoute>(MemberList.None);

            CreateMap<TaskRoute, TaskRouteDTO>(MemberList.None);
            CreateMap<TaskRouteDTO, TaskRoute>(MemberList.None);


            CreateMap<TaskGallary, BLL.ViewModels.TaskGallaryResultDTO>(MemberList.None);
            CreateMap<TaskGallary, BLL.TasksFolder.ViewModels.MainTasksDTO.TaskGallaryResultDTO>(MemberList.None);


            CreateMap<Area, AreaViewModel>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.NameEN))
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => CultureInfo.CurrentCulture.Name == "en" ? src.NameEN : src.NameAR))
              .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src =>src.Governrate !=null? src.Governrate.CountryId :null))
              .ForMember(dest => dest.GovernorateId, opt => opt.MapFrom(src => src.FK_Governrate_Id != null ? src.FK_Governrate_Id : null));


            CreateMap<AreaViewModel, Area>(MemberList.None);

            CreateMap<GovernarateViewModel, Governrate>(MemberList.None);
            CreateMap<Governrate, GovernarateViewModel>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
              .ForMember(dest => dest.IsDeleted, opt => opt.MapFrom(src => src.IsDeleted))
              .ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
              .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.Name))
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => CultureInfo.CurrentCulture.Name == "en" ? src.NameEN : src.NameAR));

            CreateMap<Governrate, GovernarateWithNameENKeyResponseDTO>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.NameEN))
              .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
              .ForMember(dest => dest.IsDeleted, opt => opt.MapFrom(src => src.IsDeleted))
              .ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
              .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.Name))
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => CultureInfo.CurrentCulture.Name == "en" ? src.NameEN : src.NameAR));



            CreateMap<Address, PACI>(MemberList.None)
                .ForMember(dest => dest.GovernorateNameEnglish, opt => opt.MapFrom(src => src.Governorate))
                .ForMember(dest => dest.AreaNameEnglish, opt => opt.MapFrom(src => src.Area))
                .ForMember(dest => dest.BlockNameEnglish, opt => opt.MapFrom(src => src.Block))
                .ForMember(dest => dest.StreetNameEnglish, opt => opt.MapFrom(src => src.Street))
                .ForMember(dest => dest.BuildingNumber, opt => opt.MapFrom(src => src.Building))
                .ForMember(dest => dest.FloorNumber, opt => opt.MapFrom(src => src.Floor))
                .ForMember(dest => dest.AppartementNumber, opt => opt.MapFrom(src => src.Flat))
                .ReverseMap();


            CreateMap<MainTaskStatus, LkpResultDto>(MemberList.None);
            CreateMap<LkpResultDto, MainTaskStatus>(MemberList.None);
            CreateMap<MainTaskType, LkpResultDto>(MemberList.None);
            CreateMap<LkpResultDto, MainTaskType>(MemberList.None);
            CreateMap<TaskType, LkpResultDto>(MemberList.None);
            CreateMap<LkpResultDto, TaskType>(MemberList.None);
            CreateMap<TaskStatus, TaskStatusResultDto>(MemberList.None);
            CreateMap<TaskStatusResultDto, TaskStatus>(MemberList.None);
            CreateMap<MainTask, MainTaskViewModel>(MemberList.None)
                .ForMember(dest => dest.MainTaskStatusName, opt => opt.MapFrom(src => src.MainTaskStatus != null ? src.MainTaskStatus.Name : null))
                .ForMember(dest => dest.MainTaskTypeName, opt => opt.MapFrom(src => src.MainTaskType != null ? src.MainTaskType.Name : null))
                .ForMember(dest => dest.Settings, opt => new TaskSettingsViewModel());

            CreateMap<MainTask, MainTaskResultDTO>(MemberList.None)
                        .ForMember(dest => dest.MainTaskStatusName, opt => opt.MapFrom(src => src.MainTaskStatus != null ? src.MainTaskStatus.Name : null))
                        .ForMember(dest => dest.MainTaskTypeName, opt => opt.MapFrom(src => src.MainTaskType != null ? src.MainTaskType.Name : null))
                        .ForMember(dest => dest.Settings, opt => new TaskSettingsViewModel())
                        .ForMember(dest => dest.RemainingTimeInSeconds, opt => opt.MapFrom(t => t.ExpirationDate.HasValue ? (int)Math.Round((t.ExpirationDate.Value - DateTime.UtcNow).TotalSeconds) : (int?)null));

            CreateMap<MainTask, MainTaskMobileMiniResultDTO>(MemberList.None)
    .ForMember(dest => dest.MainTaskTypeName, opt => opt.MapFrom(src => src.MainTaskType.Name))
                ;
            CreateMap<MainTaskMobileResultWithTasksDTO, MainTaskMobileMiniResultDTO>(MemberList.None);
            CreateMap<MainTaskMobileMiniResultDTO, MainTaskMobileResultWithTasksDTO>(MemberList.None)
    .ForMember(dest => dest.Tasks, opt => opt.MapFrom(src => new List<TasksResultDTO>()))
                ;


            CreateMap<MainTask, HistoryMainTaskMobileResultDTO>(MemberList.None)
            .ForMember(dest => dest.MainTaskTypeName, opt => opt.MapFrom(src => src.MainTaskType.Name))
            .ForMember(dest => dest.NoOfTasks, opt => opt.MapFrom(src => src.Tasks.Count()))
            .ForMember(dest => dest.NoOfCompletedTasks, opt => opt.MapFrom(src => src.Tasks.Count(t => t.TaskStatusId == (int)Amanah.Posthub.BLL.Enums.TaskStatusEnum.Successful)))
    ;
            CreateMap<HistoryMainTaskMobileResultWithTasksDTO, HistoryMainTaskMobileResultDTO>(MemberList.None);
            CreateMap<HistoryMainTaskMobileResultDTO, HistoryMainTaskMobileResultWithTasksDTO>(MemberList.None)
                .ForMember(dest => dest.Tasks, opt => opt.MapFrom(src => new List<TaskHistoryDetailsResultDTO>()))
                ;



            CreateMap<TasksResultWithMainTaskDTO, TasksResultDTO>(MemberList.None);
            CreateMap<TasksResultDTO, TasksResultWithMainTaskDTO>(MemberList.None)
                .ForMember(dest => dest.MainTask, opt => opt.Ignore())
                ;


            CreateMap<TaskHistoryDetailsResultWithMainTaskDTO, TaskHistoryDetailsResultDTO>(MemberList.None);
            CreateMap<TaskHistoryDetailsResultDTO, TaskHistoryDetailsResultWithMainTaskDTO>(MemberList.None)
                .ForMember(dest => dest.MainTask, opt => opt.Ignore())
                ;




            CreateMap<MainTaskResultDTO, MainTask>(MemberList.None)
                .ForMember(dest => dest.IsCompleted, opt => opt.Ignore())
                .ForMember(dest => dest.IsDelayed, opt => opt.Ignore());

            CreateMap<MainTaskViewModel, MainTask>(MemberList.None)
                .ForMember(dest => dest.IsCompleted, opt => opt.Ignore())
                .ForMember(dest => dest.IsDelayed, opt => opt.Ignore());
            CreateMap<SubTask, TasksViewModel>(MemberList.None)
                .ForMember(dest => dest.DelayTime, opt => opt.MapFrom(src => src.DelayTime))
                .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Name : src.Address))
                .ForMember(dest => dest.IsTaskReached, opt => opt.MapFrom(src => src.IsTaskReached ?? false))
                .ForMember(dest => dest.TotalWaitingTime, opt => opt.MapFrom(src => HandleTotalWaitingTime(src)))
                .ForMember(dest => dest.TotalEstimationTime, opt => opt.MapFrom(src => HandleTotalEstimationTime(src.EstimatedTime)))
                .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
                .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
                .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.Driver.User.FirstName + " " + src.Driver.User.LastName))
                .ForMember(dest => dest.DriverPhoneNumber, opt => opt.MapFrom(src => src.Driver.User.PhoneNumber))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Driver.Team.Name))
                .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
                .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => "TaskSignatures/" + src.SignatureURL))
                .ForMember(dest => dest.DriverRates, opt => opt.MapFrom(src => src.DriverRates))
                .ForMember(dest => dest.FormImage, opt => opt.Ignore());

            CreateMap<SubTask, TasksMobileResultDTO>(MemberList.None)
              .ForMember(dest => dest.DelayTime, opt => opt.MapFrom(src => src.DelayTime))
              .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Name : src.Address))
              .ForMember(dest => dest.IsTaskReached, opt => opt.MapFrom(src => src.IsTaskReached ?? false))
              .ForMember(dest => dest.TotalWaitingTime, opt => opt.MapFrom(src => HandleTotalWaitingTime(src)))
              .ForMember(dest => dest.TotalEstimationTime, opt => opt.MapFrom(src => HandleTotalEstimationTime(src.EstimatedTime)))
              .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
              .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
              .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.Driver.User.FirstName + " " + src.Driver.User.LastName))
              .ForMember(dest => dest.DriverPhoneNumber, opt => opt.MapFrom(src => src.Driver.User.PhoneNumber))
              .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
              .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
              .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Driver.Team.Name))
              .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
              .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => "TaskSignatures/" + src.SignatureURL))
              .ForMember(dest => dest.DriverRates, opt => opt.MapFrom(src => src.DriverRates))
              .ForMember(dest => dest.FormImage, opt => opt.Ignore());



            CreateMap<SubTask, TasksResultDTO>(MemberList.None)
           .ForMember(dest => dest.DelayTime, opt => opt.MapFrom(src => src.DelayTime))
           .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Name : src.Address))
           .ForMember(dest => dest.IsTaskReached, opt => opt.MapFrom(src => src.IsTaskReached ?? false))
           .ForMember(dest => dest.TotalWaitingTime, opt => opt.MapFrom(src => HandleTotalWaitingTime(src)))
           .ForMember(dest => dest.TotalEstimationTime, opt => opt.MapFrom(src => HandleTotalEstimationTime(src.EstimatedTime)))
           .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
           .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
           .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.Driver.User.FirstName + " " + src.Driver.User.LastName))
           .ForMember(dest => dest.DriverPhoneNumber, opt => opt.MapFrom(src => src.Driver.User.PhoneNumber))
           .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
           .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
           .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Driver.Team.Name))
           .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
           .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => "TaskSignatures/" + src.SignatureURL))
           .ForMember(dest => dest.FormImage, opt => opt.Ignore());

            CreateMap<SubTask, TasksResultWithMainTaskDTO>(MemberList.None)
       .ForMember(dest => dest.DelayTime, opt => opt.MapFrom(src => src.DelayTime))
       .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Name : src.Address))
       .ForMember(dest => dest.IsTaskReached, opt => opt.MapFrom(src => src.IsTaskReached ?? false))
       .ForMember(dest => dest.TotalWaitingTime, opt => opt.MapFrom(src => HandleTotalWaitingTime(src)))
       .ForMember(dest => dest.TotalEstimationTime, opt => opt.MapFrom(src => HandleTotalEstimationTime(src.EstimatedTime)))
       .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
       .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
       .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.Driver.User.FirstName + " " + src.Driver.User.LastName))
       .ForMember(dest => dest.DriverPhoneNumber, opt => opt.MapFrom(src => src.Driver.User.PhoneNumber))
       .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
       .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
       .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Driver.Team.Name))
       .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
       .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => "TaskSignatures/" + src.SignatureURL))
       .ForMember(dest => dest.FormImage, opt => opt.Ignore());



            CreateMap<SubTask, TaskHistoryDetailsViewModel>(MemberList.None)
                .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
                .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                .ForMember(dest => dest.CustomerAddress, opt => opt.MapFrom(src => src.Customer.Address))
                .ForMember(dest => dest.TaskDate, opt => opt.MapFrom(src => src.PickupDate ?? src.DeliveryDate))
                .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
                .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.SignatureURL) ? null : "TaskSignatures/" + src.SignatureURL))
                .ForMember(dest => dest.FormImage, opt => opt.Ignore());




            CreateMap<SubTask, TaskHistoryDetailsResultDTO>(MemberList.None)
            .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
            .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
            .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
            .ForMember(dest => dest.CustomerAddress, opt => opt.MapFrom(src => src.Customer.Address))
            .ForMember(dest => dest.TaskDate, opt => opt.MapFrom(src => src.PickupDate ?? src.DeliveryDate))
            .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
            .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.SignatureURL) ? null : "TaskSignatures/" + src.SignatureURL))
            .ForMember(dest => dest.FormImage, opt => opt.Ignore());



            CreateMap<SubTask, TaskHistoryDetailsResultWithMainTaskDTO>(MemberList.None)
            .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
            .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
            .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
            .ForMember(dest => dest.CustomerAddress, opt => opt.MapFrom(src => src.Customer.Address))
            .ForMember(dest => dest.TaskDate, opt => opt.MapFrom(src => src.PickupDate ?? src.DeliveryDate))
            .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
            .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.SignatureURL) ? null : "TaskSignatures/" + src.SignatureURL))
            .ForMember(dest => dest.FormImage, opt => opt.Ignore());



            CreateMap<SubTask, ExportTasksWithoutProgressViewModel>(MemberList.None)
                .ForMember(dest => dest.Order_Type, opt => opt.MapFrom(src => src.TaskType.Name))
                .ForMember(dest => dest.Order_Status, opt => opt.MapFrom(src => src.TaskStatus.Name))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Customer.Name))
                .ForMember(dest => dest.Order_DateTime, opt => opt.MapFrom(src => src.PickupDate ?? src.DeliveryDate))
                .ForMember(dest => dest.Driver_Name, opt => opt.MapFrom(src => src.Driver.User.FirstName))
                .ForMember(dest => dest.Reference_Image, opt => opt.MapFrom(src => src.Image))
                .ForMember(dest => dest.Order_Description, opt => opt.MapFrom(src => src.Description));


            CreateMap<TasksViewModel, SubTask>(MemberList.None)
                .ForMember(dest => dest.DelayTime, opt => opt.Ignore())
                .ForMember(dest => dest.Customer, opt => opt.Ignore())
                .ForMember(dest => dest.TaskHistories, opt => opt.Ignore())
                .ForMember(dest => dest.StartDate, opt => opt.Ignore())
                .ForMember(dest => dest.SuccessfulDate, opt => opt.Ignore())
                .ForMember(dest => dest.TotalTime, opt => opt.Ignore())
                .ForMember(dest => dest.TaskGallaries, opt => opt.Ignore());

            CreateMap<TasksMobileResultDTO, SubTask>(MemberList.None)
           .ForMember(dest => dest.DelayTime, opt => opt.Ignore())
           .ForMember(dest => dest.Customer, opt => opt.Ignore())
           .ForMember(dest => dest.TaskHistories, opt => opt.Ignore())
           .ForMember(dest => dest.StartDate, opt => opt.Ignore())
           .ForMember(dest => dest.SuccessfulDate, opt => opt.Ignore())
           .ForMember(dest => dest.TotalTime, opt => opt.Ignore())
           .ForMember(dest => dest.TaskGallaries, opt => opt.Ignore());



            //.ForMember(dest => dest.DriverRates, opt => opt.Ignore());
            CreateMap<TaskHistory, TaskHistoryViewModel>(MemberList.None)
                .ForMember(dest => dest.TaskTypeId, opt => opt.MapFrom(src => src.Task != null ? src.Task.TaskTypeId : 0))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Task != null ? src.Task.Address : string.Empty))
                .ForMember(dest => dest.FromStatusName, opt => opt.MapFrom(src => src.FromStatus.Name))
                .ForMember(dest => dest.ToStatusName, opt => opt.MapFrom(src => src.ToStatus.Name))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy_Id));

            CreateMap<TaskHistory, MapPoint>(MemberList.None);
            CreateMap<TaskRoute, MapPoint>(MemberList.None);

            CreateMap<TermsAndConditions, TermsAndConditionsViewModel>(MemberList.None);
            CreateMap<TermsAndConditionsViewModel, TermsAndConditions>(MemberList.None);


            CreateMap<TaskHistoryViewModel, TaskHistory>(MemberList.None);
            CreateMap<DriverTaskNotification, DriverTaskNotificationViewModel>(MemberList.None);
            CreateMap<DriverTaskNotificationViewModel, DriverTaskNotification>(MemberList.None)
                .ForMember(dest => dest.Task, opt => opt.Ignore());
            CreateMap<DriverTaskNotification, TaskNotificationViewModel>(MemberList.None)
                .ForMember(dest => dest.ActionStatus, opt => opt.MapFrom(src => src.ActionStatus))
                .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.Task.TaskType.Name))
                .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.Task.TaskStatus.Name))
                .ForMember(dest => dest.TaskTypeId, opt => opt.MapFrom(src => src.Task.TaskTypeId))
                .ForMember(dest => dest.TaskStatusId, opt => opt.MapFrom(src => src.Task.TaskStatusId))
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Task.Customer.Name))
                .ForMember(dest => dest.CustomerAddress, opt => opt.MapFrom(src => src.Task.Customer.Address))
                .ForMember(dest => dest.TaskDate, opt => opt.MapFrom(src => src.Task.PickupDate ?? src.Task.DeliveryDate));



            CreateMap<DriverTaskNotification, DriverTaskNotificationResultDTO>(MemberList.None)
              .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.Task.TaskType.Name))
              .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.Task.TaskStatus.Name))
              .ForMember(dest => dest.TaskTypeId, opt => opt.MapFrom(src => src.Task.TaskTypeId))
              .ForMember(dest => dest.TaskStatusId, opt => opt.MapFrom(src => src.Task.TaskStatusId))
              .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Task.Customer.Name))
              .ForMember(dest => dest.CustomerAddress, opt => opt.MapFrom(src => src.Task.Customer.Address))
              .ForMember(dest => dest.TaskDate, opt => opt.MapFrom(src => src.Task.PickupDate ?? src.Task.DeliveryDate));

            CreateMap<GeoFenceResultDTO, GeoFence>(MemberList.None)
                .ConstructUsing(geofenceVM => new GeoFence(geofenceVM.Tenant_Id))
                .ForMember(m => m.GeoFenceLocations, src => src.Ignore());

            CreateMap<GeoFence, GeoFenceLookupViewModel>(MemberList.None);

            CreateMap<Team, LkpResultDto>(MemberList.None);
            CreateMap<GeoFence, GeoFenceResultDTO>(MemberList.None)
                .ForMember(m => m.Locations, src => src.MapFrom(t => t.GeoFenceLocations != null || t.GeoFenceLocations.Count > 0 ? t.GeoFenceLocations.Select(t => new GeoFenceLocationResultDTO { Latitude = t.Latitude, Longitude = t.Longitude }).ToList() : new List<GeoFenceLocationResultDTO>()));

            CreateMap<CreateGeoFenceViewModel, GeoFenceResultDTO>(MemberList.None);

            CreateMap<Branch, BranchViewModel>(MemberList.None)
                .ForMember(m => m.Restaurant, src => src.MapFrom(t => t.Restaurant));
            CreateMap<BranchViewModel, Branch>(MemberList.None)
                .ConstructUsing(branchVM => new Branch(branchVM.Tenant_Id))
                .ForMember(m => m.Restaurant, src => src.Ignore());
            CreateMap<CreateBranchViewModel, BranchViewModel>(MemberList.None);

            CreateMap<Restaurant, RestaurantViewModel>(MemberList.None);
            CreateMap<RestaurantViewModel, Restaurant>(MemberList.None)
                .ConstructUsing(restaurantVM => new Restaurant(restaurantVM.Tenant_Id));
            CreateMap<CreateRestaurantViewModel, RestaurantViewModel>(MemberList.None);


            CreateMap<GeoFence, LkpResultDto>(MemberList.None);
            CreateMap<Restaurant, LkpResultDto>(MemberList.None);
            CreateMap<Branch, LkpResultDto>(MemberList.None);


            CreateMap<DriverDeliveryGeoFenceViewModel, DriverDeliveryGeoFence>(MemberList.None);
            CreateMap<DriverDeliveryGeoFence, DriverDeliveryGeoFenceViewModel>(MemberList.None)
                .ForMember(m => m.GeoFenceName, src => src.MapFrom(t => t.GeoFence.Name));

            CreateMap<DriverPickUpGeoFenceViewModel, DriverPickUpGeoFence>(MemberList.None);
            CreateMap<DriverPickUpGeoFence, DriverPickUpGeoFenceViewModel>(MemberList.None)
                .ForMember(m => m.GeoFenceName, src => src.MapFrom(t => t.GeoFence.Name));


            CreateMap<DriverLoginRequest, DriverLoginRequestViewModel>(MemberList.None)
                .ForMember(m => m.DriverName, src => src.MapFrom(t => t.Driver.User.UserName))
                .ForMember(m => m.TeamId, src => src.MapFrom(t => t.Driver.TeamId))
                .ForMember(m => m.TeamName, src => src.MapFrom(t => t.Driver.Team == null ? "" : t.Driver.Team.Name))
                .ForMember(m => m.AgentTypeId, src => src.MapFrom(t => t.Driver.AgentTypeId))
                .ForMember(m => m.AgentTypeName, src => src.MapFrom(t => t.Driver.AgentType == null ? "" : t.Driver.AgentType.Name))
                .ReverseMap();
            CreateMap<CreateDriverLoginRequestViewModel, DriverLoginRequestViewModel>(MemberList.None);
            CreateMap<CreateDriverLoginRequestViewModel, DriverLoginRequest>(MemberList.None);


            CreateMap<AccountLogViewModel, AccountLogs>(MemberList.None);
            CreateMap<AccountLogs, AccountLogViewModel>(MemberList.None);


            CreateMap<AdminFullProfile, AdminProfileViewModel>();
            CreateMap<AdminFullProfile, UserInfoViewModel>();
            CreateMap<AdminViewModel, Admin>(MemberList.None)
                 .ForMember(dest => dest.Branch, opt => opt.Ignore());
            CreateMap<Admin, AdminViewModel>(MemberList.None);


            CreateMap<Notification, NotificationViewModel>(MemberList.None);
            CreateMap<NotificationViewModel, Notification>(MemberList.None);

            CreateMap<Notification, NotificationResultDTO>(MemberList.None);
            CreateMap<NotificationResultDTO, Notification>(MemberList.None);

            CreateMap<Notification, NotificationInputDTO>(MemberList.None);
            CreateMap<NotificationInputDTO, Notification>(MemberList.None);




            CreateMap<DriverloginTracking, DriverloginTrackingViewModel>(MemberList.None)
                .ForMember(m => m.DriverName, src => src.MapFrom(t => t.Driver.User.FirstName + " " + t.Driver.User.LastName))
                .ForMember(m => m.LogoutDate, src => src.MapFrom(t => t.LogoutDate.HasValue && t.LogoutDate.Value.Date < new DateTime(2000, 01, 01).Date ? null : t.LogoutDate));
            CreateMap<DriverloginTrackingViewModel, DriverloginTracking>(MemberList.None);



        }

        private static TimeViewModel HandleTotalWaitingTime(SubTask src)
        {
            TimeViewModel waitingTime = null;
            if (src.IsTaskReached.HasValue && src.ReachedTime.HasValue && src.StartDate.HasValue)
            {
                TimeSpan differenceInTime = (src.StartDate.Value - src.ReachedTime.Value);
                waitingTime = new TimeViewModel
                {
                    Time = differenceInTime,
                    Day = differenceInTime.Days,
                    Hour = differenceInTime.Hours,
                    Minute = differenceInTime.Minutes,
                    Second = differenceInTime.Seconds
                };
            }
            return waitingTime;
        }

        private static TimeViewModel HandleTotalEstimationTime(double? estimationTime)
        {
            TimeViewModel EstimationTime = null;
            if (estimationTime.HasValue)
            {
                TimeSpan EstimationTimeSpan = TimeSpan.FromSeconds((double)estimationTime);
                EstimationTime = new TimeViewModel
                {
                    Time = EstimationTimeSpan,
                    Day = EstimationTimeSpan.Days,
                    Hour = EstimationTimeSpan.Hours,
                    Minute = EstimationTimeSpan.Minutes,
                    Second = EstimationTimeSpan.Seconds
                };
            }
            return EstimationTime;
        }

        private static string HandleFirstLastName(string fullName, NameType nameType)
        {
            var names = fullName.Split(' ');
            switch (nameType)
            {
                case NameType.First: return names[0];
                case NameType.Last:
                    {
                        if (names.Length > 1)
                        {
                            return names[^1];
                        }
                        break;
                    }
                case NameType.Middle:
                    {
                        if (names.Length > 2)
                        {
                            return names[^2];
                        }
                        break;
                    }
            }

            return string.Empty;
        }


    }
}

