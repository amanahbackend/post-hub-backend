﻿using Amanah.Posthub.BLL.ActionLogs.Queries;
using Amanah.Posthub.BLL.ActionLogs.Queries.DTOs;
using Amanah.Posthub.SharedKernel.Utilites.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class AccountLogsController : Controller
    {
        private readonly IAccountLogQuery _accountLogQuery;

        public AccountLogsController(IAccountLogQuery accountLogQuery)
        {
            _accountLogQuery = accountLogQuery;
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            return Ok(await _accountLogQuery.GetAllAsync());
        }

        [HttpPost("GetAllByPagination")]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] AccountLogFilterDTO filter)
        {
            return Ok(await _accountLogQuery.GetByPaginationAsync(filter));
        }

        [HttpGet("ExportToExcel")]
        public async Task<ActionResult> ExportToExcelAsync([FromQuery] AccountLogFilterDTO filter)
        {
            var data = await _accountLogQuery.GetAccountLogsForExportAsync(filter);
            var city = filter.TimeZone.Split('/').Length > 1
               ? filter.TimeZone.Split('/')[1]
               : filter.TimeZone;
            TimeZoneInfo currentTimeZone = TimeZoneUtility.GetCurrent(city);
            if (currentTimeZone != null)
            {
                data.ForEach(accountLog =>
                accountLog.CreatedDate = TimeZoneInfo.ConvertTimeFromUtc(accountLog.CreatedDate, currentTimeZone));
            }

            var fileName = "AccountLogs.xlsx";
            string filePath = Path.Combine("ImportTemplates", fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

    }
}