﻿
using Amanah.Posthub.BLL.Addresses.Services;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Addresses.Queries;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AddressController : Controller
    {
        private IAddressService _addressService { set; get; }
        private IPACIService _paciService { set; get; }

        private readonly IAreaManager _areaManager;
        //  private readonly IGovernarateManager _governarateManager;
        private readonly IGovernorateQuery _governorateQuery;
        private readonly ILocalizer _localizer;
        private readonly IBlockQuery _blockQuery;
        protected readonly ApplicationDbContext _context;

        public AddressController(
            IAddressService addressService,
            IPACIService paciService,
            IAreaManager areaManager,
            ILocalizer localizer,
            IGovernorateQuery governorateQuery,
            IBlockQuery blockQuery,
            ApplicationDbContext context)
        {
            _addressService = addressService;
            _paciService = paciService;
            _areaManager = areaManager;
            _blockQuery = blockQuery;
            _context = context;
            //   _governarateManager = governarateManager;
            _localizer = localizer;
            _governorateQuery = governorateQuery;
        }

        // GET: Area
        [Route("search-by-components")]
        [HttpGet]
        public async Task<ActionResult> SearchByComponentsAsync([FromQuery] Address addressViewModel)
        {
            var paciaddress = await _addressService.SearchForAddressAsync(addressViewModel);
            if (paciaddress == null)
            {
                return BadRequest(_localizer);
            }
            return Ok(paciaddress);
        }


        [HttpGet]
        [Route("get-blocks-from-paci")]
        public async Task<IActionResult> GetBlocksAsync(string areaId)
        {
            var result = await _blockQuery.GetAllAreaBlocksAsync(areaId);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result.OrderBy(b => decimal.Parse(b.Id)));
        }

        [HttpGet]
        [Route("get-streets-from-paci")]
        public async Task<IActionResult> GetStreetsAsync(string areaId, string blockName)
        {
            var result = await _paciService.GetStreetsAsync(areaId, blockName);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }


        // GET: Area
        [Route("paci")]
        [HttpGet]
        public async Task<ActionResult> SearchByPACIAsync([FromQuery] int paciNumber)
        {
            var paciaddress = await _addressService.SearchForAddressByPACIAsync(paciNumber);
            if (paciaddress == null)
            {
                return BadRequest(_localizer[Keys.Messages.PACIUnavailableOrNotFound]);
            }
            return Ok(paciaddress);
        }


        // GET: Area
        [Route("GetAllAreas")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All Areas 
            var AllAreas = await _areaManager.GetAllAsync<AreaViewModel>(true);
            return Ok(AllAreas);
        }

        // GET: Area
        [Route("GetAllKwuitAreas")]
        [HttpGet]
        public async Task<ActionResult> GetAllKwuitAreasAsync()
        {
            ///Get All Kwuit Areas 
            var allKwuitAreas = await _areaManager.GetAllKwuitAreasAsync(); 
            return Ok(allKwuitAreas);
        }


        [HttpGet("governorates/{governorateId}/areas")]
        public async Task<ActionResult> GetAreasByGovernorateIdAsync(string governorateId)
        {
            var result = await _areaManager.GetAllByGovernorateIdAsync(governorateId);
            return Ok(result);
        }

        [HttpGet("governorates")]
        public async Task<ActionResult> GetGovernoratesAsync()
        {
            var result = await _governorateQuery.GetGovernarateListForAddressComponentAsync();
            return Ok(result);
        }
        [HttpGet("governoratesByCountryId/{id}")]
        public async Task<ActionResult> GetGovernoratesByCountryIdAsync(int id)
        {
            var result = await _governorateQuery.GetGovernarateListForAddressComponentAsync(id);
            return Ok(result);
        }



        [AllowAnonymous]
        [HttpGet]
        [Route("import-blocks-from-paci")]
        public async Task<IActionResult> ImportBlocksAsync()
        {

            var AllAreas =await _context.Area.AsQueryable().Select(x=>new {
                Id =x.Id ,
                NameEN = x.NameEN,
                NameAR = x.NameAR,
            }).ToListAsync();
            foreach (var area in AllAreas)
            {
                var blocks  = await _paciService.GetBlocksAsync(area.NameEN);

                foreach(var bock in blocks)
                {
                   var isExist =  await _context.Blocks.Where(x => x.AreaId == area.Id && x.BlockNo == bock.Name).AnyAsync();

                    if (!isExist)
                    {
                        _context.Blocks.Add(new Block()
                        {
                            AreaId = area.Id,
                            BlockNo = bock.Name,
                            IsActive = true,
                            IsSystem = true,
                        });

                        await _context.SaveChangesAsync();
                    }
                }
            }

            return Ok(AllAreas);
        }

    }
}