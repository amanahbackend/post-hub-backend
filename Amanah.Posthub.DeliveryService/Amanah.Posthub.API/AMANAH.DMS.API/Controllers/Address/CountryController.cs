﻿using Amanah.Posthub.BLL.Addresses.Services;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.BLL.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : Controller
    {
        private ICountryManager _countryManager { set; get; }
        public CountryController(ICountryManager countryManager)
        {
            _countryManager = countryManager;
        }


        [AllowAnonymous]
        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            var res = await _countryManager.GetAllCountriesesAsync();
            return Ok(res);
        }
        [HttpGet("GetAllActive")]
        public async Task<ActionResult> GetAllActiveAsync()
        {
            var governarateList = await _countryManager.GetAllActiveCountriesAsync();
            return Ok(governarateList);
        }
        [HttpPost("ActiveInActive")]
        public async Task<ActionResult> ActiveInActiveAsync(CountryResultDto countryResultDto)
        {
            var res = await _countryManager.ActiveInActiveAsync(countryResultDto);
            return Ok(res);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Countries.ListCountrys)]
        [HttpGet("GetAllByPagination")]
        public async Task<ActionResult> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var res = await _countryManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(res);
        }
        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Countries.DeleteCountry)]
        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var res = await _countryManager.Get(id);
            return Ok(res);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Countries.CreateCountry)]
        [HttpPost("Create")]
        public async Task<ActionResult> AddAsync(CountryResultDto countryResultDto)
        {
            var res = await _countryManager.CreateAsync(countryResultDto);
            return Ok(res);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Countries.UpdateCountry)]
        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync(CountryResultDto countryResultDto)
        {
            var res = await _countryManager.AsyncUpdate(countryResultDto);
            return Ok(res);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Countries.DeleteCountry)]
        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _countryManager.DeleteAsync(id);
            return Ok(true);
        }

      
    }
}