﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Branches.Queries;
using Amanah.Posthub.BLL.Branches.Queries.DTOs;
using Amanah.Posthub.BLL.Branches.Services;
using Amanah.Posthub.BLL.Branches.Services.DTOs;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Amanah.Posthub.DeliverService.BLL.Branches.Services.DTOs.Express;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;
using Utilities.Utilites.Localization;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.DeliverService.API.Controllers.Branches.Express
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [FeatureGate(FeatureManagementFlags.Express)]
    public class BranchExpressController : Controller
    {
        private readonly IBranchQuery _branchQuery;
        private readonly IBranchService _branchService;
        private readonly IUserCommonQuery _userCommonQuery;
        private readonly ICurrentUser _currentUser;
        private readonly ILocalizer _localizer;

        public BranchExpressController(
            IBranchQuery branchQuery,
            IBranchService branchService,
            IUserCommonQuery userCommonQuery,
            ICurrentUser currentUser,
            ILocalizer localizer)
        {
            _branchQuery = branchQuery;
            _branchService = branchService;
            _userCommonQuery = userCommonQuery;
            _currentUser = currentUser;
            _localizer = localizer;
        }


        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            List<int> managerBranchIds = null;
            if (_currentUser.IsManager())
            {
                managerBranchIds = await _userCommonQuery.GetManagerBranchesAsync();
                if (managerBranchIds == null || !managerBranchIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _branchQuery.GetAllAsync(managerBranchIds));
        }

        [HttpPost("GetAllByPagination")]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] BranchFilterDTO filter)
        {
            List<int> managerBranchIds = null;
            if (_currentUser.IsManager())
            {
                managerBranchIds = await _userCommonQuery.GetManagerBranchesAsync();
                if (managerBranchIds == null || !managerBranchIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _branchQuery.GetAllByPaginationAsync(filter, managerBranchIds));
        }

        [HttpPost("GetAllByResturantPagination")]
        public async Task<ActionResult> GetAllByResturantPaginationAsync([FromBody] BranchFilterDTO filter)
        {
            filter.RestaurantId = filter.Id;
            List<int> managerBranchIds = null;
            if (_currentUser.IsManager())
            {
                managerBranchIds = await _userCommonQuery.GetManagerBranchesAsync();
                if (managerBranchIds == null || !managerBranchIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _branchQuery.GetAllByPaginationAsync(filter, managerBranchIds));
        }


        [HttpPost("GetAllByCompanyPagination")]
        public async Task<ActionResult> GetAllByCompanyBranchesPaginationAsync([FromBody] BranchFilterDTO filter)
        {
            filter.CompanyId = filter.Id;
            List<int> managerBranchIds = null;
            if (_currentUser.IsManager())
            {
                managerBranchIds = await _userCommonQuery.GetManagerBranchesAsync();
                if (managerBranchIds == null || !managerBranchIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _branchQuery.GetAllByPaginationAsync(filter, managerBranchIds));
        }



        [HttpGet("GetBranchesBy")]
        public async Task<ActionResult> GetBranchesByAsync(int resturantId)
        {
            List<int> managerBranchIds = null;
            if (_currentUser.IsManager())
            {
                managerBranchIds = await _userCommonQuery.GetManagerBranchesAsync();
                if (managerBranchIds == null || !managerBranchIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _branchQuery.GetRestaurantBranchesAsync(resturantId, managerBranchIds));
        }

        [HttpGet("GetBranchesByresturantIdsAndgeoFenceIds")]
        public async Task<ActionResult> GetBranchesByResturantAndGeoFenceAsync(string resturantIds, string geoFenceIds)
        {
            if (string.IsNullOrEmpty(resturantIds))
            {
                return BadRequest(_localizer[Keys.Validation.EmptyRestaurantIds]);
            }
            if (string.IsNullOrEmpty(geoFenceIds))
            {
                return BadRequest(_localizer[Keys.Validation.EmptyGeoFenceIds]);
            }

            int[] resturantIdsArray = Array.ConvertAll(resturantIds.Split(','), restaurant => int.Parse(restaurant));
            int[] geoFenceIdsArray = Array.ConvertAll(geoFenceIds.Split(','), geofence => int.Parse(geofence));

            List<int> managerBranchIds = null;
            if (_currentUser.IsManager())
            {
                managerBranchIds = await _userCommonQuery.GetManagerBranchesAsync();
                if (managerBranchIds == null || !managerBranchIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _branchQuery.GetBranchesByAsync(resturantIdsArray, geoFenceIdsArray, managerBranchIds));
        }

        [HttpGet("GetByName/{name}")]
        public async Task<ActionResult> GetByNameAsync(string name)
        {
            List<int> managerBranchIds = null;
            if (_currentUser.IsManager())
            {
                managerBranchIds = await _userCommonQuery.GetManagerBranchesAsync();
                if (managerBranchIds == null || !managerBranchIds.Any())
                {
                    return Ok();
                }
            }
            return Ok(await _branchQuery.GetBranchesByNameAsync(name, managerBranchIds));
        }

        [HttpGet("Get")]
        public async Task<ActionResult> GetAsync(int id)
        {
            return Ok(await _branchQuery.GetAsync(id));
        }

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync([FromRoute] int id)
        {
            return Ok(await _branchQuery.GetAsync(id));
        }


        [HttpPost("Create")]

        public async Task<ActionResult> CreateAsync([FromBody] CreateBranchForExpressInputDTO branch)
        {
            return Ok(await _branchService.CreateExpressAsync(branch));
        }

        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateBranchForExpressInputDTO branch)
        {
            await _branchService.UpdateExpressAsync(branch);
            return Ok(true);
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            await _branchService.DeleteAsync(id);
            return Ok(true);
        }

        [HttpGet("SetActivate")]
        public async Task<ActionResult> SetActivateAsync(
            int id,
            bool isActive,
            string Reason)
        {
            return Ok(await _branchService.ActivateDeactivateAsync(id, isActive, Reason));
        }




        [Route("ExportToExcel")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcelAsync([FromQuery] int companyId)
        {
            var data = await _branchQuery.ExportCompanyBranchesAsync(companyId);

            var path = "ImportTemplates";
            var fileName = "CompanyBranches.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }


    }
}

