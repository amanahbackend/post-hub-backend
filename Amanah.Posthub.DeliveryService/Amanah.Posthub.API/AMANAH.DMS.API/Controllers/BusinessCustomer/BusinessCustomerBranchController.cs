﻿using Amanah.Posthub.BLL.BusinessCustomerBranchs.Queries;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomersBranchss.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.Courtesy
{
    [ApiController]
    [Route("api/[controller]")]
    //           [Authorize(AuthenticationSchemes = "Bearer")]
    public class BusinessCustomerBranchController : ControllerBase
    {
        private readonly IBusinessCustomerBranchService _BusinessCustomerBranchyService;
        private readonly IBusinessCustomerBranchQuery _businessCustomerBranchQuery;

        public BusinessCustomerBranchController(
            IBusinessCustomerBranchService BusinessCustomerBranchyService,
            IBusinessCustomerBranchQuery businessCustomerBranchQuery)
        {
            _BusinessCustomerBranchyService = BusinessCustomerBranchyService;
            _businessCustomerBranchQuery = businessCustomerBranchQuery;
        }



        [HttpPost("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateBusinessCustomerBranchInputDTO bcustomerInput)
        {
            return Ok(await _BusinessCustomerBranchyService.CreateAsync(bcustomerInput));
        }





        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            return Ok(await _businessCustomerBranchQuery.GetAllAsync());
        }



        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            var AllCustomers = await _businessCustomerBranchQuery.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(AllCustomers);
        }


        [Route("GetAllByBusinssCustomerPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByBusinssCustomerPaginationAsync([FromQuery] int businssCustomerId, [FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            var AllCustomerBranchess = await _businessCustomerBranchQuery.GetAllByBusinssCustomerPaginationAsync(pagingparametermodel);
            return Ok(AllCustomerBranchess);
        }

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync([FromRoute] int id)
        {
            return Ok(await _businessCustomerBranchQuery.GetByIdAsync(id));
        }


        [HttpGet("GetAllByBusinessCustomer")]
        public async Task<ActionResult> GetAllAsync(int businessCustomerId)
        {
            return Ok(await _businessCustomerBranchQuery.GetAllByBussinessCutomerAsync(businessCustomerId));
        }

        [HttpGet("GetAllByBusinessCustomerByUserId")]
        public async Task<ActionResult> GetAllAsync(string businessCustomerUserId)
        {
            return Ok(await _businessCustomerBranchQuery.GetAllByBussinessCutomerUserIdAsync(businessCustomerUserId));
        }

        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateBusinessCustomerBranchInputDTO branch)
        {
            await _BusinessCustomerBranchyService.UpdateAsync(branch);
            return Ok(true);
        }


        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            await _BusinessCustomerBranchyService.DeleteAsync(id);
            return Ok(true);
        }

    }
}
