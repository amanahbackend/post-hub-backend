﻿using Amanah.Posthub.BLL.BusinessContactAccounts.Queries;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomersContactss.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.Courtesy
{
    [ApiController]
    [Route("api/[controller]")]
    //          [Authorize(AuthenticationSchemes = "Bearer")]
    public class BusinessCustomerContactController : ControllerBase
    {
        private readonly IBusinessCustomerContactService _BusinessCustomerContactyService;
        private readonly IBusinessContactAccountQuery _businessContactAccountQuery;

        public BusinessCustomerContactController(
            IBusinessCustomerContactService BusinessCustomerContactyService,
            IBusinessContactAccountQuery businessContactAccountQuery)
        {
            _BusinessCustomerContactyService = BusinessCustomerContactyService;
            _businessContactAccountQuery = businessContactAccountQuery;
        }


        [AllowAnonymous]

        [HttpPost("Create")]
        public async Task<ActionResult> CreateAsync([FromForm] CreateBusinessCustomerContactInputDTO bcustomerInput)
        {
            return Ok(await _BusinessCustomerContactyService.CreateAsync(bcustomerInput));
        }

        [AllowAnonymous]
        [HttpPost("AccountRegistration")]
        public async Task<ActionResult> AccountRegistrationAsync([FromForm] AccountRegistrationDto accountInfo)
        {
            return Ok(await _BusinessCustomerContactyService.AccountRegistrationAsync(accountInfo));
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            return Ok(await _businessContactAccountQuery.GetAllAsync());
        }

        [HttpGet("GetAllByBusinessCustomer")]
        public async Task<ActionResult> GetAllAsync(int businessCustomerId)
        {
            return Ok(await _businessContactAccountQuery.GetAllByBussinessCutomerAsync(businessCustomerId));
        }




        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            var AllCustomers = await _businessContactAccountQuery.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(AllCustomers);
        }

        [Route("GetAllByBusinssCustomerPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByBusinssCustomerPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            var AllCustomers = await _businessContactAccountQuery.GetAllByBusinssCustomerPaginationAsync(pagingparametermodel);
            return Ok(AllCustomers);
        }


        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync([FromRoute] int id)
        {
            return Ok(await _businessContactAccountQuery.GetByIdAsync(id));
        }


        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromForm] UpdateBusinessCustomerContactInputDTO model)
        {
            await _BusinessCustomerContactyService.UpdateAsync(model);
            return Ok(true);
        }



        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            await _BusinessCustomerContactyService.DeleteAsync(id);
            return Ok(true);

        }


    }
}
