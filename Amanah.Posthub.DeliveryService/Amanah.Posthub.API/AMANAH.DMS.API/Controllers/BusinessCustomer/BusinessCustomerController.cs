﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.BusinessCustomers.Queries;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.IO;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;
using Amanah.Posthub.BASE.Authentication;

namespace Amanah.Posthub.DeliverService.API.Controllers.Courtesy
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]

    public class BusinessCustomerController : ControllerBase
    {
        private readonly IBusinessCustomerService _businessCustomeryService;
        private readonly IBusinessCustomerQuery _businessCustomeryQuery;
        private readonly ICurrentUser _currentUser;

        public BusinessCustomerController(
            IBusinessCustomerService businessCustomeryService,
            IBusinessCustomerQuery businessCustomeryQuery,
            ICurrentUser currentUser)
        {
            _businessCustomeryService = businessCustomeryService;
            _businessCustomeryQuery = businessCustomeryQuery;
            _currentUser = currentUser; 
        }



        [AllowAnonymous]
        [HttpPost("Create")]
        public async Task<ActionResult> CreateAsync([FromForm] CreateBusinessCustomerInputDTO bcustomerInput)
        {
            return Ok(await _businessCustomeryService.CreateAsync(bcustomerInput));
        }

        [AllowAnonymous]
        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromForm] UpdateBusinessCustomerInputDTO branch)
        {
            await _businessCustomeryService.UpdateAsync(branch);
            return Ok(true);
        }

        [HttpDelete("Delete/{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.BusinessCustomer.DeleteBusinessCustomer)]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            await _businessCustomeryService.DeleteAsync(id);
            return Ok(true);
        }


        [HttpGet("GetAll")]
       // [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.BusinessCustomer.ReadBusinessCustomer)]

        public async Task<ActionResult> GetAllAsync()
        {
            if(_currentUser.IsBusinessCustomer())
            {
                return Ok(_currentUser.BusinessCustomerId);
            }
           
            return Ok(await _businessCustomeryQuery.GetAllAsync());
        }


        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("GetAllByBussinesTypeId/{id}")]
        public async Task<ActionResult> GetAllByBussinesTypeIdAsync(int id)
        {
            return Ok(await _businessCustomeryQuery.GetAllByBussinesTypeAsync(id));
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.BusinessCustomer.ReadBusinessCustomer)]
        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] GeBusinessCustomertPagginatedInputDTO pagingparametermodel)
        {
            var AllCustomers = await _businessCustomeryQuery.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(AllCustomers);
        }


        [AllowAnonymous]

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync([FromRoute] int id)
        {
            return Ok(await _businessCustomeryQuery.GetByIdAsync(id));
        }



        [Route("[action]")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.BusinessCustomer.ExportBusinessCustomer)]
        public async Task<ActionResult> ExportToExcel([FromBody] GeBusinessCustomertPagginatedInputDTO pagingparametermodel)
        {
            var data = await _businessCustomeryQuery.GetExportBCustomersAsync(pagingparametermodel);
            var path = "ImportTemplates";
            var fileName = "customers.xlsx";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, "application/force-download", fileName);
        }

        [HttpGet("GetBussinessCutomerCount")]
        public async Task<ActionResult> GetBussinessCutomerCountAsync([FromRoute] int id)
        {
            return Ok(await _businessCustomeryQuery.GetBussinessCutomerCountAsync());
        }


        [HttpGet("GetBussinessCustomerId")]
        public async Task<ActionResult> GetBussinessCustomerIdAsync()
        {
            return Ok(await _businessCustomeryQuery.GetBussinessCustomerIdAsync());
        }

    }
}
