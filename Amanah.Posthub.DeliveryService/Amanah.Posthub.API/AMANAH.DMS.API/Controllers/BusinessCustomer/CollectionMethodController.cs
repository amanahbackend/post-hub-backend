﻿using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.CollectionMethod
{
    [ApiController]
    [Route("api/[controller]")]
    //          [Authorize(AuthenticationSchemes = "Bearer")]
    public class CollectionMethodController : ControllerBase
    {
        private readonly ICollectionMethodQuery _CollectionMethodQuery;

        public CollectionMethodController(
            ICollectionMethodQuery CollectionMethodQuery)
        {
            _CollectionMethodQuery = CollectionMethodQuery;
        }

        // GET: Customers/Details/5
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var CollectionMethod = await _CollectionMethodQuery.GetAllAsync();
            return Ok(CollectionMethod);
        }



    }
}
