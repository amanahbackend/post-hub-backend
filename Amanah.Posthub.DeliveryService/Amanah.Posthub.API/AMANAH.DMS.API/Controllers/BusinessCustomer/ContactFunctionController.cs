﻿using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.ContactFunction
{
    [ApiController]
    [Route("api/[controller]")]
    //          [Authorize(AuthenticationSchemes = "Bearer")]
    public class ContactFunctionController : ControllerBase
    {
        private readonly IContactFunctionQuery _ContactFunctionQuery;

        public ContactFunctionController(
            IContactFunctionQuery ContactFunctionQuery)
        {
            _ContactFunctionQuery = ContactFunctionQuery;
        }

        // GET: Customers/Details/5
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var ContactFunction = await _ContactFunctionQuery.GetAllAsync();
            return Ok(ContactFunction);
        }



    }
}
