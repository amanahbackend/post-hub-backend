﻿using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.Courtesy
{
    [ApiController]
    [Route("api/[controller]")]
    //          [Authorize(AuthenticationSchemes = "Bearer")]
    public class CourtesyController : ControllerBase
    {
        private readonly ICourtesyQuery _CourtesyQuery;

        public CourtesyController(
            ICourtesyQuery CourtesyQuery)
        {
            _CourtesyQuery = CourtesyQuery;
        }

        // GET: Customers/Details/5
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var Courtesy = await _CourtesyQuery.GetAllAsync();
            return Ok(Courtesy);
        }



    }
}
