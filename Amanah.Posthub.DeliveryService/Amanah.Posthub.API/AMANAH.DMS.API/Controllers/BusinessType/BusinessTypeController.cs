﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.DeliverService.BLL.BusinessTypes.Queries;
using Amanah.Posthub.DeliverService.BLL.BusinessTypes.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.BusinessTypes.Services;
using Amanah.Posthub.DeliverService.BLL.BusinessTypes.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.BusinessType
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class BusinessTypeController : ControllerBase
    {
        private readonly IBusinessTypeService _businessTypeService;
        private readonly IBusinessTypeQuery _businessTypeQuery;

        public BusinessTypeController(IBusinessTypeService businessTypeService, IBusinessTypeQuery businessTypeQuery)
        {
            _businessTypeService = businessTypeService;
            _businessTypeQuery = businessTypeQuery;
        }

        // GET: Customers/Details/5
        [AllowAnonymous]
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var BusinessType = await _businessTypeQuery.GetAllAsync();
            return Ok(BusinessType);
        }

        [Route("GetAllByBusinessCustomerId/{businessCustomerId}")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> GetAllByBusinessCustomerIdAsync(int businessCustomerId)
        {
            var res = await _businessTypeQuery.GetByBusinessCustomerIdAsync(businessCustomerId);
            return Ok(res);
        }


        [HttpPost("GetAllByPagination")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.BusinessTypes.ListBusinessType)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] BusinessTypeFilterDTO filter)
        {
            return Ok(await _businessTypeQuery.GetByPaginationAsync(filter));
        }

        [HttpPost("Create")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.BusinessTypes.CreateBusinessType)]
        public async Task<ActionResult> CreateAsync(CreateBusinessTypeInputDTO businessTypeInput)
        {
            return Ok(await _businessTypeService.CreateAsync(businessTypeInput));
        }

        [HttpPut("Update")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.BusinessTypes.UpdateBusinessType)]
        public async Task<ActionResult> UpdateAsync(UpdateBusinessTypeInputDTO businessTypeInput)
        {
            return Ok(await _businessTypeService.UpdateAsync(businessTypeInput));
        }

        [HttpDelete("Delete/{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.BusinessTypes.DeleteBusinessType)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            return Ok(await _businessTypeService.DeleteAsync(id));
        }
    }
}
