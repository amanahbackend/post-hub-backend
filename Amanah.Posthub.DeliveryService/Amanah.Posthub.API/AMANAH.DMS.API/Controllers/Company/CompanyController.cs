﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.CompanyProfile.Queries;
using Amanah.Posthub.BLL.CompanyProfile.Services;
using Amanah.Posthub.DeliverService.BLL.CompanyProfile.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.Company
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyQuery _companyQuery;
        private readonly ICompanyService _companyService;

        public CompanyController(
            ICompanyQuery companyQuery,
            ICompanyService companyService)
        {
            _companyQuery = companyQuery;
            _companyService = companyService;
        }

        // GET: Customers/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var Company = await _companyQuery.GetByIdAsync(id);
            return Ok(Company);
        }


        // GET: Customers/Details/5
        [Route("getCurrentCompanyProfile")]
        [HttpGet]
        public async Task<ActionResult> GetCurrentCompanyProfileAsync()
        {
            var Company = await _companyQuery.GetCurrentCompanyProfileAsync();
            return Ok(Company);
        }

        [Route("GetCurrentCompanyProfileForReports")]
        [HttpGet]
        public async Task<ActionResult> GetCurrentCompanyProfileForReportsAsync()
        {
            var Company = await _companyQuery.GetCurrentCompanyProfileForReportsAsync();
            return Ok(Company);
        }

        [HttpPut]
        [Authorize(TenantPermissions.CompanyProfile.UpdateCompanyProfile)]
        public async Task<ActionResult> UpdateAsync([FromForm] UpdateCompanyProfileDTO companyDTO)
        {
            await _companyService.UpdateAsync(companyDTO);
            return Ok();
        }


    }
}
