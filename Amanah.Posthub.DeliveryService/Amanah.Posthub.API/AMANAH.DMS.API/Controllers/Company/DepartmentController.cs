﻿using Amanah.Posthub.BLL.DepartmentProfile.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.Department
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DepartmentController : ControllerBase
    {
        private readonly IDepartmentQuery _DepartmentQuery;

        public DepartmentController(
            IDepartmentQuery DepartmentQuery)
        {
            _DepartmentQuery = DepartmentQuery;
        }

        // GET: Customers/Details/5
        [AllowAnonymous]
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var Department = await _DepartmentQuery.GetAllAsync();
            return Ok(Department);
        }



    }
}
