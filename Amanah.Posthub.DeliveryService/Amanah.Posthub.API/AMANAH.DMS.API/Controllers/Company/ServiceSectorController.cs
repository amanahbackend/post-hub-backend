﻿using Amanah.Posthub.BLL.ServiceSectorProfile.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.ServiceSector
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ServiceSectorController : ControllerBase
    {
        private readonly IServiceSectorQuery _ServiceSectorQuery;

        public ServiceSectorController(
            IServiceSectorQuery ServiceSectorQuery)
        {
            _ServiceSectorQuery = ServiceSectorQuery;
        }

        // GET: Customers/Details/5
        [AllowAnonymous]
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var ServiceSector = await _ServiceSectorQuery.GetAllAsync();
            return Ok(ServiceSector);
        }



    }
}
