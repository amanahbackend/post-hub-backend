﻿using Amanah.Posthub.DeliverService.BLL.Company.Queries;
using Amanah.Posthub.DeliverService.BLL.Company.Services;
using Amanah.Posthub.DeliverService.BLL.Company.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.WorkingHours
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class WorkingHoursController : ControllerBase
    {
        private readonly IWorkingHoursQuery _WorkingHoursQuery;
        private readonly IWorkingHoursService _WorkingHoursService;

        public WorkingHoursController(
            IWorkingHoursQuery WorkingHoursQuery,
            IWorkingHoursService WorkingHoursService)
        {
            _WorkingHoursQuery = WorkingHoursQuery;
            _WorkingHoursService = WorkingHoursService;
        }

        // GET: Customers/Details/5
        [Route("GetCompanyWorkingHours/{id}")]
        [HttpGet]
        public async Task<ActionResult> GetCompanyWorkingHoursAsync(int CompanyId)
        {
            var WorkingHours = await _WorkingHoursQuery.GetCompanyWorkingHoursAsync(CompanyId);
            return Ok(WorkingHours);
        }




        [HttpPut]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateWorkinghoursDTO WorkingHoursDTO)
        {
            await _WorkingHoursService.UpdateAsync(WorkingHoursDTO);
            return Ok();
        }


    }
}
