﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DownloadController : Controller
    {
        [Route("DownloadImportTemplate")]
        public IActionResult DownloadImportTemplate(string name)
        {
            var path = "ImportTemplates";
            string fileName = "Import" + name + "Template.xlsx";
            string filePath = Path.Combine(path, fileName);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }
    }
}