﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Drivers.Queries;
using Amanah.Posthub.BLL.Drivers.Queries.DTOs;
using Amanah.Posthub.BLL.Drivers.Services;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DriverLoginRequestController : Controller
    {
        private readonly IDriverLoginRequestQuery _driverLoginRequestQuery;
        private readonly IDriverService _driverService;
        private readonly IUserCommonQuery _userCommonQuery;
        private readonly ICurrentUser _currentUser;

        public DriverLoginRequestController(
            IDriverLoginRequestQuery driverLoginRequestQuery,
            IDriverService driverService,
            IUserCommonQuery userCommonQuery,
            ICurrentUser currentUser)
        {
            _driverLoginRequestQuery = driverLoginRequestQuery;
            _driverService = driverService;
            _userCommonQuery = userCommonQuery;
            _currentUser = currentUser;
        }

        [HttpGet("GetAllByPagination")]
        [Authorize(TenantPermissions.Agent.ViewDriversLoginRequests)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] DriverLoginRequestFilterDTO filter)
        {
            if (_currentUser.IsManager() && _currentUser.HasPermission(TenantPermissions.Team.ReadMyTeam))
            {
                var managerTeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                filter.DriverIds = await _userCommonQuery.GetManagerDriversAsync(managerTeamIds);
                if (filter.DriverIds == null || !filter.DriverIds.Any())
                {
                    return Ok();
                }
            }
            return Ok(await _driverLoginRequestQuery.GetAllByPaginationAsync(filter));
        }

        [HttpGet("ChangeStatus")]
        public async Task<IActionResult> ChangeStatusAsync(int id,
            DriverLoginRequestStatus status,
            string reason)
        {
            return Ok(await _driverService.ApproveOrRejectLoginRequestAsync(id, status, reason));
        }

    }
}
