﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.DriverRegistrations.Queries;
using Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs;
using Amanah.Posthub.BLL.DriverRegistrations.Services;
using Amanah.Posthub.BLL.DriverRegistrations.ViewModels;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DriverRegistrationRequestController : Controller
    {
        private readonly IDriverRegistrationService _driverRegistrationRequestService;
        private readonly IApplicationUserManager _appUserManager;
        private readonly ILocalizer _localizer;
        private readonly IDriverRegistrationRequestsQuery _driverRegistrationRequestsQuery;


        public DriverRegistrationRequestController(
            IDriverRegistrationService driverRegistrationRequestService,
            IApplicationUserManager appUserManager,
            ILocalizer localizer,
            IDriverRegistrationRequestsQuery driverRegistrationRequestsQuery)
        {
            _driverRegistrationRequestService = driverRegistrationRequestService;
            _appUserManager = appUserManager;
            _driverRegistrationRequestsQuery = driverRegistrationRequestsQuery;
            _localizer = localizer;
        }

        [HttpGet("GetAllByPagination")]
        [Authorize(Policy = TenantPermissions.Agent.ReadRequestAgent)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] DriverRegistrationSearchInputDTO pagingparametermodel)
        {
            var result = await _driverRegistrationRequestsQuery.GetAllDriverByPaginationAsync(pagingparametermodel);
            return Ok(result);
        }

        [HttpGet("GetById")]
        [Authorize(Policy = TenantPermissions.Agent.ReadRequestAgent)]
        public async Task<ActionResult> GetByIdAsync(int id)
        {
            var result = await _driverRegistrationRequestsQuery.GetByIdAsync(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpPut("update")]
        [Authorize(Policy = TenantPermissions.Agent.EditRequestAgent)]
        public async Task<ActionResult> UpdateAsync([FromForm] UpdateDriverRegistrationDetailsInputDTO model)
        {
            await _driverRegistrationRequestService.UpdateAsync(model);
            return Ok();
        }

        [HttpPost("Approve")]
        [Authorize(Policy = TenantPermissions.Agent.ApproveRequestAgent)]
        public async Task<IActionResult> ApproveAsync([FromForm] ApproveDriverRegistrationRequestInputDTO approveDriverVM)
        {
            var existUser = await _appUserManager.ValidateUserByEmailAsync(approveDriverVM.Email);
            if (existUser)
            {
                return BadRequest(_localizer[Keys.Validation.EmailAlreadyExists]);
            }
            existUser = await _appUserManager.ValidateUserByUserNameAsync(approveDriverVM.UserName);
            if (existUser)
            {
                return BadRequest(_localizer.Format(Keys.Validation.UserNameAlreadyExists, approveDriverVM.UserName));
            }

            if (!approveDriverVM.DriverRegistrationId.HasValue)
            {
                return BadRequest(_localizer[Keys.Validation.MissingDriverRegistrationId]);
            }
            var driverRegistration = await _driverRegistrationRequestsQuery.GetAsync(approveDriverVM.DriverRegistrationId.Value);
            if (string.IsNullOrEmpty(driverRegistration.PasswordHash))
            {
                return BadRequest(_localizer[Keys.Validation.MissingDriverPassword]);
            }
            await _driverRegistrationRequestService.ApproveAsync(approveDriverVM);

            return Ok();
        }


        [HttpPost("Reject")]
        [Authorize(Policy = TenantPermissions.Agent.RejectRequestAgent)]
        public async Task<IActionResult> RejectAsync(RejectDriverRegistrationInputDTO model)
        {
            await _driverRegistrationRequestService.RejectAsync(model.DriverId, model.Reason);

            return Ok();
        }

        [HttpGet("GetHistoricalAuditors")]
        [Authorize(Policy = TenantPermissions.Agent.ReadRequestAgent)]
        public async Task<ActionResult> GetHistoricalAuditorsAsync([FromQuery] PaginatedItemsViewModel paginatedItems)
        {
            var result = await _driverRegistrationRequestsQuery.GetHistoricalAuditorsAsync(paginatedItems);
            return Ok(result);
        }

    }
}
