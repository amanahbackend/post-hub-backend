﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.DriverWorkingHour.Queries;
using Amanah.Posthub.BLL.DriverWorkingHour.Queries.DTOs;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;

namespace Amanah.Posthub.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DriverWorkingHourTrackingController : Controller
    {
        private readonly IDriverWorkingHourTrackingQuery _driverWorkingHourTrackingQuery;
        private readonly IUserCommonQuery _userCommonQuery;
        private readonly ICurrentUser currentUser;

        public DriverWorkingHourTrackingController(IDriverWorkingHourTrackingQuery driverWorkingHourTrackingQuery,
            IUserCommonQuery userCommonQuery,
            ICurrentUser currentUser)
        {
            _driverWorkingHourTrackingQuery = driverWorkingHourTrackingQuery;
            this._userCommonQuery = userCommonQuery;
            this.currentUser = currentUser;
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync(
            [FromBody] DriverWorkingHourTrackingFilterDTO filter)
        {
            if (currentUser.IsManager())
            {
                filter.TeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (filter.TeamIds == null || !filter.TeamIds.Any())
                {
                    return Ok();
                }
            }
            return Ok(await _driverWorkingHourTrackingQuery.GetAllByPaginationAsync(filter));
        }

        [Route("ExportToExcel")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcelAsync(
            [FromQuery] DriverWorkingHourTrackingFilterDTO filter)
        {
            if (currentUser.IsManager())
            {
                filter.TeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (filter.TeamIds == null || !filter.TeamIds.Any())
                {
                    return Ok();
                }
            }
            var (totalCount, data) = await _driverWorkingHourTrackingQuery.GetAllAsync(filter);
            var excelRows = data.Select(model =>
                new
                {
                    DriverName = model.DriverName,
                    DayOfMonth = model.DayOfMonth.ToShortDateString(),
                    TotalHours = model.TotalHours
                })
                .ToList();
            var path = "ImportTemplates";
            var fileName = "DriverWorkingHoursTracking.CSV";
            string filePath = Path.Combine(path, fileName);
            var excelFile = excelRows.ToCSV(filePath, totalCountText: "Drivers Working Hours");
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }
    }
}