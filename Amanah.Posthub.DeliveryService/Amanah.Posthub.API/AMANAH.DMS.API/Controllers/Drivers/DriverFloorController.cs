﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using Amanah.Posthub.DeliverService.BLL.DriverFloor.Queries;

namespace Amanah.Posthub.DeliverService.API.Controllers.Drivers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DriverFloorController : Controller
    {
        private readonly IDriverFloorQuery _driverFloorQuery;

        public DriverFloorController(IDriverFloorQuery driverFloorQuery)
        {
            _driverFloorQuery = driverFloorQuery;
        }
        [Route("GetDriverFloors/{branchId}")]
        [HttpGet]
        public async Task<IActionResult> GetDriverFloors(int branchId)
        {
            var res = await _driverFloorQuery.GetDriverFloors(branchId);
            return Ok(res);
        }
    }
}
