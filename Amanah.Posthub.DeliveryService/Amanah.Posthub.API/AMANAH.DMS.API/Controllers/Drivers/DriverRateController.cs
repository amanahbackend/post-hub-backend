﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class DriverRateController : Controller
    {
        public readonly IMapper _mapper;
        public readonly IDriverRateManager _DriverRateManager;


        public DriverRateController(
            IMapper mapper,
            IDriverRateManager DriverRateManager)
        {
            _mapper = mapper;
            _DriverRateManager = DriverRateManager;
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var data = await _DriverRateManager.GetAllAsync<DriverRateViewModel>();
            return Ok(data);
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel pagingparametermodel)
        {
            var DriverRatees = await _DriverRateManager.GetAllByPaginationAsync(pagingparametermodel);
            return Ok(DriverRatees);
        }

        [Route("Get")]
        [HttpGet]
        public async Task<ActionResult> GetAsync(int id)
        {
            var data = await _DriverRateManager.Get<DriverRateViewModel>(id);
            return Ok(data);
        }

        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync([FromRoute] int id)
        {
            var driver = await _DriverRateManager.Get(id);
            return Ok(driver);
        }


        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateDriverRateViewModel input)
        {
            var mappedInput = _mapper.Map<DriverRateViewModel>(input);
            var validator = new DriverRateViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);


            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                return Ok(await _DriverRateManager.AddAsync(mappedInput));
            }
        }

        [HttpPut]
        [Route("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] CreateDriverRateViewModel input)
        {
            var mappedInput = _mapper.Map<DriverRateViewModel>(input);
            var validator = new DriverRateViewModelValidation();
            var validationResult = await validator.ValidateAsync(mappedInput);
            if (validationResult.IsValid == false)
            {
                return BadRequest(validationResult.Errors);
            }
            else
            {
                return Ok(await _DriverRateManager.UpdateAsync(mappedInput));
            }
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            return Ok(await _DriverRateManager.DeleteAsync(id));
        }
    }
}