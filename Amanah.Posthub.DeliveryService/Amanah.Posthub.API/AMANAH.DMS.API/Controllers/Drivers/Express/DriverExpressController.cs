﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Drivers.Queries;
using Amanah.Posthub.BLL.Drivers.Queries.DTOs;
using Amanah.Posthub.BLL.Drivers.Services;
using Amanah.Posthub.BLL.Drivers.Services.DTOs;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Amanah.Posthub.DeliverService.BLL.Drivers.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.Drivers.Services.DTOs.Express;
using Amanah.Posthub.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.GenericListToExcel;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.API.Controllers.Drivers.Express
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [FeatureGate(FeatureManagementFlags.Express)]
    public class DriverExpressController : Controller
    {
        private readonly IImportExcelManager _importExcelManager;
        private readonly ICurrentUser _currentUser;
        private readonly IUserCommonQuery _userCommonQuery;
        private readonly IDriverQuery _driverQuery;
        private readonly IDriverloginTrackingQuery _driverloginTrackingQuery;
        private readonly IDriverService _driverService;

        public DriverExpressController(
            IImportExcelManager importExcelManager,
            ICurrentUser currentUser,
            IUserCommonQuery userCommonQuery,
            IDriverQuery driverQuery,
            IDriverloginTrackingQuery driverloginTrackingQuery,
            IDriverService driverService)
        {
            _importExcelManager = importExcelManager;

            _currentUser = currentUser;
            _userCommonQuery = userCommonQuery;
            _driverQuery = driverQuery;
            _driverloginTrackingQuery = driverloginTrackingQuery;
            _driverService = driverService;
        }

        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            List<int> managerTeamIds = null;
            if (_currentUser.IsManager())
            {
                managerTeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _driverQuery.GetAllExpressAsync(managerTeamIds));
        }

        [Route("GetAllByServiceSector/{serviceSectorId}")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync(int serviceSectorId)
        {
            List<int> managerTeamIds = null;
            if (_currentUser.IsManager())
            {
                managerTeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _driverQuery.GetAllByServiceSectorExpressAsync(serviceSectorId, managerTeamIds));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Drivers.ListDrivers)]
        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] DriverFilterDTO filter)
        {
           
            List<int> managerTeamIds = null;
            if (_currentUser.IsManager())
            {
                managerTeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _driverQuery.GetAllDriverByPaginationExpressAsync(filter, managerTeamIds));
        }

        [Route("GetPlatformByPagination")]
        [HttpPost]
        [Authorize(TenantPermissions.PlatformAgent.ReadPlatformAgent)]
        public async Task<ActionResult> GetPlatformByPaginationAsync([FromBody] DriverFilterDTO filter)
        {
            return Ok(await _driverQuery.GetPlatformDriverByPaginationAsync(filter));
        }

        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            return Ok(await _driverQuery.GetExpressAsync(id));
        }

        [Route("GetAllDriversForAssignment")]
        [HttpGet]
        public async Task<ActionResult> GetAssignDriversAsync()
        {
            return Ok(await _driverQuery.GetAssignDriversAsync());
        }

        [Route("GetTags")]
        [HttpGet]
        public async Task<ActionResult> GetTagsAsync()
        {
            return Ok(await _driverQuery.GetTagsAsync());
        }

        [HttpPost]
        [Route("GetDriverLoginTracking")]
        public async Task<ActionResult> GetDriverLoginTrackingAsync(DriverLoginTrackingFilterDTO driverLoginTrackingFilterDTO)
        {
            return Ok(await _driverloginTrackingQuery.GetByPaginationAsync(driverLoginTrackingFilterDTO));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Drivers.CreateDriver)]
        [HttpPost, DisableRequestSizeLimit]
        [Route("Create")]
        [Authorize(TenantPermissions.Agent.CreateAgent)]
        public async Task<ActionResult> CreateAsync([FromForm] CreateDriverForExpressInputDTO createDriverDTo)
        {
            if (createDriverDTo.DriverDeliveryGeoFences != null)
            {
                var geofenceIds = createDriverDTo.DriverDeliveryGeoFences.Select(x => x.GeoFenceId).ToList();
                if (await _driverQuery.AnyActiveDriverHasGeofencesAsync(geofenceIds))
                {
                    throw new DomainException("There Is an Active User have The same geofence");
                }
            }
            var result = await _driverService.CreateExpressAsync(createDriverDTo);
            if (result.IsSucceeded)
            {
                return Ok(result.IsSucceeded);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Drivers.UpdateDriver)]
        [HttpPost("Update"), DisableRequestSizeLimit]
        [Authorize(TenantPermissions.Agent.UpdateAgent)]
        public async Task<ActionResult> UpdateAsync([FromForm] UpdateDriverForExpressInputDTO editDriverDTo)
        {
            if (editDriverDTo.DriverDeliveryGeoFences != null)
            {
                var geofenceIds = editDriverDTo.DriverDeliveryGeoFences.Select(x => x.GeoFenceId).ToList();
                if (await _driverQuery.AnyActiveDriverHasGeofencesAsync(geofenceIds, editDriverDTo.Id) && editDriverDTo.AccountStatusId == 1) //Account Status 1 = active 
                {
                    throw new DomainException("There Is an Active User have The same geofence");
                }
            }
            var result = await _driverService.EditExpressAsync(editDriverDTo);
            if (result.IsSucceeded)
            {
                return Ok(result.IsSucceeded);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Drivers.DeleteDriver)]
        [HttpPost("BullkDelete")]
        [Authorize(TenantPermissions.Agent.DeleteAllAgent)]
        public async Task<ActionResult> BullkDeleteAsync(List<DeleteDriverInputDTO> deleteDriverInputDTOs)
        {
            await _driverService.DeleteAsync(deleteDriverInputDTOs);
            return Ok();
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Drivers.DeleteDriver)]
        [HttpDelete("Delete/{id}")]
        [Authorize(TenantPermissions.Agent.DeleteAgent)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _driverService.DeleteAsync(id);
            return Ok();
        }

        [Route("ChangeDriversType")]
        [HttpPost]
        public async Task<IActionResult> ChangeDriversTypeAsync([FromBody] UpdateDriversAgentTypeInputDTO editDriversAgentTypeInputDTO)
        {
            await _driverService.EditAsync(editDriversAgentTypeInputDTO);
            return Ok();
        }

        [Route("BlockDrivers")]
        [HttpPut]
        public async Task<IActionResult> BlockDriversAsync([FromBody] BlockOrUnBlockDriversDTO blockOrUnBlockDriversDTO)
        {
            await _driverService.BlockAsync(blockOrUnBlockDriversDTO);
            return Ok();
        }

        [Route("UnBlockDrivers")]
        [HttpPut]
        public async Task<IActionResult> UnBlockDriversAsync([FromBody] BlockOrUnBlockDriversDTO blockOrUnBlockDriversDTO)
        {
            await _driverService.UnBlockAsync(blockOrUnBlockDriversDTO);
            return Ok();
        }

        [Route("BlockDriver")]
        [HttpPut]
        public async Task<IActionResult> BlockDriverAsync(int id, string reason)
        {
            var blockOrUnBlockDriversDTO = new BlockOrUnBlockDriversDTO
            {
                Reason = reason,
                DriverIds = new List<int> { id }
            };
            await _driverService.BlockAsync(blockOrUnBlockDriversDTO);
            return Ok();
        }

        [Route("UnBlockDriver")]
        [HttpPut]
        public async Task<IActionResult> UnBlockDriverAsync(int id, string reason)
        {
            var blockOrUnBlockDriversDTO = new BlockOrUnBlockDriversDTO
            {
                Reason = reason,
                DriverIds = new List<int> { id }
            };
            await _driverService.UnBlockAsync(blockOrUnBlockDriversDTO);

            return Ok();
        }

        [Route("SetOnDuty")]
        [HttpPost]
        public async Task<IActionResult> SetOnDutyAsync([FromBody] SetDriverDutyInputDTO driverDutyInputDTO)
        {
            await _driverService.SetDutyAsync(driverDutyInputDTO);
            return Ok(true);
        }

        [Route("ForceLogout/{driverId}")]
        [HttpGet]
        public async Task<IActionResult> ForceLogoutAsync(int driverId)
        {
            await _driverService.ForceLogoutDriverAsync(driverId);
            return Ok(true);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Drivers.ExportDriversToExcel)]
        [Route("ExportToExcel")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcelAsync([FromQuery] DriverFilterDTO filter)
        {
            var data = await _driverQuery.ExportExcelAsync(filter);

            var path = "ImportTemplates";
            var fileName = "Drivers.xlsx";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

        [Route("ExportDriverLoginTrackingToExcel")]
        [HttpPost]
        public async Task<ActionResult> ExportDriverLoginTrackingToExcelAsync(DriverLoginTrackingFilterDTO filter)
        {
            var data = await _driverloginTrackingQuery.ExportToExcelAsync(filter);

            var path = "ImportTemplates";
            var fileName = "Driverslogoutactions.xlsx";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }




        //todo : refactor after handle task manager 

        //[HttpPost("FilterDriver")]
        //public async Task<ActionResult> TestFilterAvailbleDriversAsync([FromBody] FilterDriverViewModel filterDriverViewModel)
        //{
        //    var driverToDelete = await _tasksManager.GetAvailableDrivers(filterDriverViewModel.TeamIds, filterDriverViewModel.Tags, filterDriverViewModel.GeoFenceIds);
        //    if (driverToDelete != null)
        //    {
        //        return Ok(driverToDelete);
        //    }
        //    else
        //    {
        //        return BadRequest(_localizer[Keys.Validation.NoAvailableDrivers]);
        //    }
        //}


        [HttpPost, DisableRequestSizeLimit]
        [Route("AddFromExcelSheet")]
        public async Task<IActionResult> AddFromExcelSheetAsync([FromForm] IFormFile file)
        {
            var uploadpath = "DriverSheet";
            var result = await _importExcelManager.AddFromExcelSheetAsync(uploadpath, file);
            var FaildRows = result.ReturnData.Where(x => x.Error != null || x.Error == "").ToList();
            if (FaildRows.Any())
            {
                string fileName = "Faild" + file.FileName + ".csv";
                var faildPath = "FaildDriverSheet";
                string filePath = Path.Combine(faildPath, fileName);
                var resultPath = FaildRows.ToCSV<ImportDriverViewModel>(filePath);
                byte[] fileBytes = System.IO.File.ReadAllBytes(resultPath);
                return File(fileBytes, "application/force-download", fileName);
            }
            return Ok();
        }

        [HttpGet("GetDriversCount")]
        public async Task<IActionResult> GetDriverCountAsync()
        {
            var res = await _driverQuery.GetDriverCountAsync();
            return Ok(res);
        }
        //[HttpPost("ChangePassword")]
        //public async Task<IActionResult> ChangePasswordAsync(ChangeDriverPasswordDTO changeDriverPassword)
        //{
        //    var res = await _driverService.ChangePasswordAsync(changeDriverPassword.UserId, changeDriverPassword.NewPassword, changeDriverPassword.CurrentPassword);
        //    return Ok(res);
        //}

        [Route("HasAnyActiveWorkorders/{driverId}")]
        [HttpGet]
        public async Task<ActionResult> HasAnyActiveWorkordersAsync(int driverId)
        {
            return Ok(await _driverQuery.HasAnyActiveWorkordersAsync(driverId));
        }
        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePasswordAsync(ChangeUserPasswordDTO changeDriverPassword)
        {
            var res = await _driverService.ChangeUserPasswordAsync(changeDriverPassword.UserId, changeDriverPassword.NewPassword);
            return Ok(res);
        }

    }
}

