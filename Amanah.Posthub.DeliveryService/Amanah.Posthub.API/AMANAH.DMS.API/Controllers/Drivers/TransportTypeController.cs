﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransportTypeController : Controller
    {
        private readonly ITransportTypeManager _transportTypeManager;

        public TransportTypeController(ITransportTypeManager transportTypeManager)
        {
            _transportTypeManager = transportTypeManager;
        }

        // GET: TransportType
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All AllTransportTypes 
            var AllTransportTypes = await _transportTypeManager.GetAllAsync<TransportTypeViewModel>(true);
            return Ok(AllTransportTypes);
        }

        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All Teams By Pagination 
            var AllTeams = await _transportTypeManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }
    }
}