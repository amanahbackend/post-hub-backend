﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.GeoFences.Queries;
using Amanah.Posthub.BLL.GeoFences.Services;
using Amanah.Posthub.BLL.GeoFences.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class GeoFenceController : Controller
    {
        private readonly IGeofenceQuery _geofenceQuery;
        private readonly IGeofenceService _geofenceService;
        private readonly ICurrentUser _currentUser;

        public GeoFenceController(
            IGeofenceQuery geofenceQuery,
            IGeofenceService geofenceService,
            ICurrentUser currentUser)
        {
            _geofenceQuery = geofenceQuery;
            _geofenceService = geofenceService;
            _currentUser = currentUser;
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            List<int> managerGeofenceIds = null;
            if (_currentUser.IsManager())
            {
                managerGeofenceIds = await _geofenceQuery.GetManagerDispatchingGeofenceAsync();
                if (managerGeofenceIds == null || !managerGeofenceIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _geofenceQuery.GetAllAsync(managerGeofenceIds, false));
        }

        [HttpGet("GetAllLookup")]
        public async Task<ActionResult> GetAllLookupAsync()
        {
            List<int> managerGeofenceIds = null;
            if (_currentUser.IsManager())
            {
                managerGeofenceIds = await _geofenceQuery.GetManagerDispatchingGeofenceAsync();
                if (managerGeofenceIds == null || !managerGeofenceIds.Any())
                {
                    return Ok();
                }
            }

            return Ok(await _geofenceQuery.GetAllAsync(managerGeofenceIds, true));
        }

        [HttpGet("Get")]
        public async Task<ActionResult> GetAsync(int id)
        {
            return Ok(await _geofenceQuery.GetAsync(id));
        }

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync([FromRoute] int id)
        {
            return Ok(await _geofenceQuery.GetAsync(id));
        }

        [AllowAnonymous]
        [HttpGet("platform-zones")]
        public async Task<ActionResult> GetAllPlaformZonesAsync()
        {
            return Ok(await _geofenceQuery.GetAllPlatformGeofencesAsync());
        }

        [HttpPost("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateGeofenceInputDTO geofenceInput)
        {
            await _geofenceService.CreateAsync(geofenceInput);
            return Ok();
        }

        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateGeofenceInputDTO geofenceInput)
        {
            var result = await _geofenceService.UpdateAsync(geofenceInput);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            var result = await _geofenceService.DeleteAsync(id);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }


        [HttpGet("ExportToExcel")]
        public async Task<ActionResult> ExportToExcelAsync()
        {
            var data = await _geofenceQuery.ExportExcelAsync();

            var path = "ImportTemplates";
            var fileName = "Geofencs.xlsx";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

    }
}