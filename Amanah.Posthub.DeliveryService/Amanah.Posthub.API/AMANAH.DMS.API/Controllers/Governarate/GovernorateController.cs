﻿using Amanah.Posthub.BLL.Addresses.Services;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.DeliverService.BLL.Addresses.Queries;
using Amanah.Posthub.DeliverService.BLL.Addresses.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.Governarate
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    public class GovernorateController : Controller
    {
        private IGovernorateQuery _governarateQuery { set; get; }
        private IGovernarateService _GovernarateService { set; get; }

        public GovernorateController(IGovernorateQuery governarateQuery
            , IGovernarateService GovernarateService)
        {
            _governarateQuery = governarateQuery;
            _GovernarateService = GovernarateService;
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            var res = await _governarateQuery.GetGovernarateListAsync();
            return Ok(res);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Governorates.ListGovernorates)]
        [HttpGet("GetAllActive")]
        public async Task<ActionResult> GetAllActiveAsync()
        {
            var governarateList = await _governarateQuery.GetAllActiveGovernarateAsync();
            return Ok(governarateList);
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Governorates.ListGovernorates)]
        [HttpGet("GetAllActiveByCountryId/{CountryId}")]
        public async Task<ActionResult> GetAllActiveByCountryIdAsync(int CountryId)
        {
            var governarateList = await _governarateQuery.GetAllActiveGovernarateByCountryIdAsync(CountryId);
            return Ok(governarateList);
        }


        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var res = await _governarateQuery.Get(id);
            return Ok(res);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Governorates.CreateGovernorate)]
        [HttpPost("Create")]
        public async Task<ActionResult> AddAsync(CreateUpdateGovernarateViewModel countryResultDto)
        {
            var res = await _GovernarateService.CreateAsync(countryResultDto);
            return Ok(res);
        }
        [HttpPost("ActiveInActive")]
        public async Task<ActionResult> ActiveInActiveAsync(CreateUpdateGovernarateViewModel countryResultDto)
        {
            var res = await _GovernarateService.ActiveInActiveAsync(countryResultDto);
            return Ok(res);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Governorates.UpdateGovernorate)]
        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync(CreateUpdateGovernarateViewModel countryResultDto)
        {
            var res = await _GovernarateService.UpdateAsync(countryResultDto);
            return Ok(res);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Governorates.DeleteGovernorate)]
        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _GovernarateService.DeleteAsync(id);
            return Ok(true);
        }
    }
}
