﻿using Amanah.Posthub.BLL.Managers.Queries;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.AccountStatus
{
    [ApiController]
    [Route("api/[controller]")]
    //          [Authorize(AuthenticationSchemes = "Bearer")]
    public class AccountStatusController : ControllerBase
    {
        private readonly IAccountStatusQuery _AccountStatusQuery;

        public AccountStatusController(
            IAccountStatusQuery AccountStatusQuery)
        {
            _AccountStatusQuery = AccountStatusQuery;
        }

        // GET: Customers/Details/5
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var AccountStatus = await _AccountStatusQuery.GetAllAsync();
            return Ok(AccountStatus);
        }



    }
}
