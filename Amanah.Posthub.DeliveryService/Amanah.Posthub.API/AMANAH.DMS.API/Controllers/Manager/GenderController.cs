﻿using Amanah.Posthub.BLL.Managers.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.API.Controllers.Gender
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class GenderController : ControllerBase
    {
        private readonly IGenderQuery _GenderQuery;

        public GenderController(
            IGenderQuery GenderQuery)
        {
            _GenderQuery = GenderQuery;
        }

        // GET: Customers/Details/5
        [AllowAnonymous]
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var Gender = await _GenderQuery.GetAllAsync();
            return Ok(Gender);
        }



    }
}
