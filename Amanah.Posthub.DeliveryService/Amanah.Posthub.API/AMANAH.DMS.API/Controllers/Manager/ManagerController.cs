﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Managers.Queries;
using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.BLL.Managers.Services;
using Amanah.Posthub.BLL.Managers.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.Managers.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.Drivers.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ManagerController : Controller
    {
        private readonly IManagerQuery _managerQuery;
        private readonly IManagerService _managerService;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;

        public ManagerController(
            IManagerQuery managerQuery,
            IManagerService managerService,
             ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _managerQuery = managerQuery;
            _managerService = managerService;
            _localizer = localizer;
            _currentUser = currentUser;
        }

        [AllowAnonymous]
        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            return Ok(await _managerQuery.GetAllAsync());
        }


        [HttpPost("GetAllByPagination")]
        //[Authorize(TenantPermissions.Managers.ReadTeamManager)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] ManagerFilterDTO filter)
        {
            if (_currentUser.IsManager() && !_currentUser.HasPermission(TenantPermissions.Managers.ReadAllManagers))
            {
                filter.OnlyCreatedByCurrentUser = true;
            }

            return Ok(await _managerQuery.GetByPaginationAsync(filter));
        }


        [HttpPost("GetPaginationBy")]
        //[Authorize(TenantPermissions.Managers.ReadTeamManager)]
        public async Task<ActionResult> GetPaginationByAsync([FromBody] ManagerFilterDTO filter)
        {
            if (_currentUser.IsManager() && !_currentUser.HasPermission(TenantPermissions.Managers.ReadAllManagers))
            {
                filter.OnlyCreatedByCurrentUser = true;
            }

            return Ok(await _managerQuery.GetByPaginationAsync(filter));
        }

        [HttpPost("GetPaginationByAccountStatus")]
        //[Authorize(TenantPermissions.Managers.ReadTeamManager)]
        public async Task<ActionResult> GetPaginationByAccountStatusAsync(ManagerByAccountStatusFilterDTO filter)
        {
            //if (_currentUser.IsManager() && !_currentUser.HasPermission(TenantPermissions.Managers.ReadAllManagers))
            //{
            //    filter.OnlyCreatedByCurrentUser = true;
            //}

            return Ok(await _managerQuery.GetPaginationByAccountStatusAsync(filter));
        }

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            return Ok(await _managerQuery.GetAsync(id));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Managers.AddManager)]


        [HttpPost("Create")]
        public async Task<ActionResult> CreateAsync([FromForm] CreateManagerInputDTO managerInput)
        {
            return Ok(await _managerService.CreateAsync(managerInput));
        }

        [HttpGet("ApproveStaff/{id}")]
        public async Task<ActionResult> ApproveStaffAsync(int id)
        {
            return Ok(await _managerService.ApproveStaffAsync(id));
        }
        
        [HttpGet("GetStaffInfoForPostRoom/{id}")]
        public async Task<ActionResult> GetStaffInfoForPostRoomAsync(string id)
        {
            return Ok(await _managerService.GetStaffInfoForPostRoomAsync(id));
        }

        [AllowAnonymous]
        [HttpPost("RegisterStaff")]
        public async Task<ActionResult> RegisterStaffAsync(RegisterManagerInputDTO managerInput)
        {
            return Ok(await _managerService.RegisterStaffAsync(managerInput));
        }

        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Managers.UpdateAllManager)]
        [HttpPost("Update")]
        public async Task<ActionResult> UpdateAsync([FromForm] UpdateManagerInputDTO managerInput)
        {
            return Ok(await _managerService.UpdateAsync(managerInput));
        }
        //[Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Managers.DeleteAllManager)]

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var result = await _managerService.DeleteAsync(id);
            if (result)
            {
                return Ok(true);
            }
            return BadRequest(_localizer[Keys.Validation.CanNotDeleteManager]);
        }


        [Authorize(AuthenticationSchemes = "Bearer", Policy = TenantPermissions.Managers.ExportManagers)]

        [Route("ExportToExcel")]
        [HttpGet]
        public async Task<ActionResult> ExportToExcelAsync([FromQuery] ManagerFilterDTO filter)
        {
            var data = await _managerQuery.ExportManagersAsync(filter);

            var path = "ImportTemplates";
            var fileName = "Staff.csv";
            string filePath = Path.Combine(path, fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePasswordAsync(ChangeUserPasswordDTO changeDriverPassword)
        {
            var res = await _managerService.ChangeUserPasswordAsync(changeDriverPassword.UserId, changeDriverPassword.NewPassword);
            return Ok(res);
        }

    }
}