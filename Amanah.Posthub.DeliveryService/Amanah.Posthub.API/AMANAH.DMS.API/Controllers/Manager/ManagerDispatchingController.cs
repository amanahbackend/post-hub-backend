﻿using Amanah.Posthub.BLL.Managers.Queries;
using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.BLL.Managers.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ManagerDispatchingController : Controller
    {
        private readonly IManagerDispatchingService _managerDispatchingService;
        private readonly IManagerDispatchingQuery _managerDispatchingQuery;

        public ManagerDispatchingController(
            IManagerDispatchingService managerDispatchingService,
            IManagerDispatchingQuery managerDispatchingQuery)
        {
            _managerDispatchingService = managerDispatchingService;
            _managerDispatchingQuery = managerDispatchingQuery;
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            return Ok(await _managerDispatchingQuery.GetAllAsync());
        }

        [HttpPost("GetAllByPagination")]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] PaginatedItemsViewModel filter)
        {
            return Ok(await _managerDispatchingQuery.GetAllByPaginationAsync(filter));
        }

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            return Ok(await _managerDispatchingQuery.GetAsync(id));
        }

        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateManagerDispatchingInputDTO managerDispatchingVM)
        {
            return Ok(await _managerDispatchingService.AddAsync(managerDispatchingVM));
        }

        [HttpPut]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateManagerDispatchingInputDTO managerDispatchingVM)
        {
            return Ok(await _managerDispatchingService.UpdateAsync(managerDispatchingVM));
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _managerDispatchingService.DeleteAsync(id);
            return Ok(id);
        }
    }
}