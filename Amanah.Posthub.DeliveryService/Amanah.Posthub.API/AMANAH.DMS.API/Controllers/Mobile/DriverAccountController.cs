﻿using Amanah.Posthub.BLL.DriverRegistrations.Queries;
using Amanah.Posthub.BLL.DriverRegistrations.Services;
using Amanah.Posthub.BLL.DriverRegistrations.ViewModels;
using Amanah.Posthub.BLL.Drivers.Services;
using Amanah.Posthub.BLL.Drivers.Services.DTOs;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class DriverAccountController : BaseController
    {
        private readonly IDriverManager _driverManager;
        private readonly IApplicationUserManager _userManager;
        private readonly ILocalizer _localizer;
        private readonly IDriverRegistrationService _driverRegistrationRequestService;
        private readonly IAdminManager _adminManager;
        private readonly IMapper _mapper;
        private readonly IDriverService _driverService;
        private readonly IDriverRegistrationRequestsQuery _driverRegistrationRequestQuery;

        public DriverAccountController(
            IApplicationUserManager userManager,
            IDriverManager driverManager,
            ILocalizer localizer,
            IDriverRegistrationService driverRegistrationRequestService,
            IAdminManager adminManager,
            IMapper mapper,
            IDriverRegistrationRequestsQuery driverRegistrationRequestQuery,
            IDriverService driverService)
        {
            _userManager = userManager;
            _driverManager = driverManager;
            _driverRegistrationRequestQuery = driverRegistrationRequestQuery;
            _localizer = localizer;
            _driverRegistrationRequestService = driverRegistrationRequestService;
            _adminManager = adminManager;
            _mapper = mapper;
            _driverService = driverService;
        }

        [HttpGet("registration-duplicate-validation")]
        public async Task<IActionResult> GetDuplicateValidationAsync(
            [FromQuery] GetDriverRegistrationDuplicationInputDTO model)
        {
            var result = await _driverRegistrationRequestQuery.GetDuplicateValidationAsync(model);
            return Ok(result);
        }

        [HttpPost("RegisterForFreelancer")]
        public async Task<IActionResult> RegisterForFreelancerAsync([FromForm] BaseDriverFreeLancerRegistrationDTO driverFreelancerRegistrationViewModel)
        {
            CreateDriverRegistrationInputDTO driverRegistrationViewModel = _mapper.Map<CreateDriverRegistrationInputDTO>(driverFreelancerRegistrationViewModel);
            await _driverRegistrationRequestService.AddAsync(driverRegistrationViewModel);
            return Ok();
        }

        [HttpPost("RegisterForTenant")]
        public async Task<IActionResult> RegisterForTenantAsync([FromForm] CreateDriverTenantRegistrationDTO driverTenantRegistrationViewModel)
        {
            if (string.IsNullOrEmpty(driverTenantRegistrationViewModel.CustomerCode))
            {
                return BadRequest(_localizer[Keys.Validation.MissingTenantCode]);
            }
            var tenantId = await _adminManager.GetTenantByCustomerCodeAsync(driverTenantRegistrationViewModel.CustomerCode);
            if (string.IsNullOrEmpty(tenantId))
            {
                return BadRequest(_localizer[Keys.Validation.NotMatchedTenantId]);
            }

            CreateDriverRegistrationInputDTO driverRegistrationViewModel = _mapper.Map<CreateDriverRegistrationInputDTO>(driverTenantRegistrationViewModel);
            driverRegistrationViewModel.Tenant_Id = tenantId;
            await _driverRegistrationRequestService.AddAsync(driverRegistrationViewModel);
            return Ok();
        }


        [AllowAnonymous]
        [Route("LoginRequest")]
        [HttpPost]
        public async Task<IActionResult> LoginRequestAsync([FromBody] LoginRequestInputDTO loginRequest)
        {
            var result = await _driverService.LoginRequestAsync(loginRequest);
            return Ok(new { succeeded = result.Key, error = result.Value });
        }

        [HttpPost("Login")]
        public async Task<IActionResult> LoginAsync([FromBody] DriverLoginInputDTO driverLoginInput)
        {
            var result = await _driverService.LoginAsync(driverLoginInput);
            if (!string.IsNullOrEmpty(result.ProfilePictureURL))
            {
                result.ProfilePictureURL = $"{Request.Scheme}://{Request.Host}{Request.PathBase}/{result.ProfilePictureURL}";
            }
            return Ok(result);
        }

        [HttpGet("LogoutByUserId")]
        [AllowAnonymous]
        public async Task<IActionResult> LogoutByUserIdAsync(string userId, double Latitude, double Longitude)
        {
            var result = await _driverService.LogoutAsync(userId, Latitude, Longitude);
            return Ok(new { IsSuccess = result });
        }

        [AllowAnonymous]
        [Route("CancelLoginRequest")]
        [HttpPost]
        public async Task<IActionResult> CancelLoginRequestAsync([FromBody] LoginRequestInputDTO loginRequest)
        {
            var result = await _driverService.CancelLoginRequestAsync(loginRequest);
            if (result.Key)
            {
                return Ok(true);
            }
            else
            {
                return BadRequest(result.Value);
            }
        }



        [Route("ForgotPassword/{userNameOrEmail}")]
        public async Task<IActionResult> ForgotPasswordAsync(string userNameOrEmail)
        {
            return Ok(await _userManager.ForgotPassword(userNameOrEmail));
        }

        [HttpGet("GetProfile/{userId}")]
        public async Task<IActionResult> GetProfileAsync(string userId)
        {
            var res = await _driverManager.GetByUserIDAsync(userId);
            var str = $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
            if (!string.IsNullOrEmpty(res.ImageUrl))
            {
                res.ImageUrl = $"{str}/DriverImages/{res.ImageUrl}";
            }
            return Ok(res);
        }
    }
}