﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Drivers.Queries;
using Amanah.Posthub.BLL.Drivers.Services;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.BaseResult;
using Amanah.Posthub.BLL.ViewModels.Driver;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class DriverController : BaseController
    {
        private readonly IDriverManager _driverManager;
        private readonly ITasksManager _tasksManager;
        private readonly IApplicationUserManager _appUserManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IDriverWorkingHourTrackingManager _driverWorkingHourTrackingManager;
        private readonly IBackGroundNotificationSercvice _notificationService;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IDriverQuery _driverQuery;
        private readonly IDriverService _driverService;

        public DriverController(
            IDriverManager driverManager,
            ITasksManager tasksManager,
            IApplicationUserManager appUserManager,
            ILocalizer localizer,
            IDriverWorkingHourTrackingManager driverWorkingHourTrackingManager,
            ICurrentUser currentUser,
            IBackGroundNotificationSercvice notificationService,
            IUploadFormFileService uploadFormFileService,
            IDriverQuery driverQuery,
            IDriverService driverService)
        {
            _driverManager = driverManager;
            _tasksManager = tasksManager;
            _appUserManager = appUserManager;
            _localizer = localizer;
            _driverWorkingHourTrackingManager = driverWorkingHourTrackingManager;
            _currentUser = currentUser;
            _notificationService = notificationService;
            _uploadFormFileService = uploadFormFileService;
            _driverQuery = driverQuery;
            _driverService = driverService;
        }


        [Route("DriverTransportionType")]
        [HttpGet]
        public async Task<IActionResult> GetDriverTransportionTypeAsync()
        {
            var driverTransportTypeDTO = await _driverQuery.GetDriverTransportionTypeAsync(_currentUser.Id);
            if (driverTransportTypeDTO == null)
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            return Ok(driverTransportTypeDTO);
        }


        [Route("SetOnDuty")]
        [HttpPost]
        public async Task<IActionResult> SetOnDutyAsync([FromBody] ChangeDriverDutyViewModel model)
        {
            int statusId = model.IsAvailable ? (int)AgentStatusesEnum.Available : (int)AgentStatusesEnum.Unavailable;
            var result = false;
            var driver = await _driverManager.GetByUserIDAsync(model.DriverId);
            if (model.IsAvailable)
            {
                await _tasksManager.MakeDriverAvailable(driver.Id);
                result = true;
            }
            else
            {
                statusId = (int)AgentStatusesEnum.Unavailable;
                result = await _driverManager.ChangeDriverStatusByUserIdAsync(model.DriverId, statusId);
            }

            var dutyValue = model.IsAvailable
                  ? _localizer[Keys.General.On]
                  : _localizer[Keys.General.Off];
            var title = _localizer[Keys.Notifications.DriverSetDuty];
            var body = _localizer.Format(Keys.Notifications.DriverNameSetDutyVaue, driver.FullName, dutyValue);
            var driverToSend = await _driverManager.GetByUserIDAsync(model.DriverId);

            _notificationService.SendToUserManagers(
               driver.UserId, "SetDuty", title, body, driverToSend);

            await _driverWorkingHourTrackingManager.AddDutyAsync(driver.Id, model.IsAvailable, false);

            return Ok(result);
        }


        [Route("UpdateDriverVichaleType")]
        [HttpPost]
        public async Task<IActionResult> UpdateVichaleTypeAsync([FromBody] ChangeDriverTransportTypeViewModel model)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            var driver = await _driverManager.GetByUserIDAsync(_currentUser.Id);
            driver.TransportTypeId = model.TransportTypeId;
            driver.TransportDescription = model.TransportDescription;
            driver.Color = model.Color;
            driver.LicensePlate = model.LicensePlate;

            var res = await _driverManager.UpdateAsync(driver);

            return Ok(res);
        }

        [Route("ChangeDriversType")]
        [HttpPut]
        public async Task<IActionResult> ChangeDriversTypeAsync([FromBody] ChangeDriversTypeViewModel model)
        {
            await _driverManager.ChangeDriversTypeAsync(model);
            return Ok();

        }

        [Route("CheckIn")]
        [HttpPut]
        public async Task<IActionResult> CheckInAsync(double Latitude, double Longitude)
        {
            var result = await _driverService.CheckInBranchAsync(Latitude, Longitude);
            if (result.Key == true)
            {
                return Success(_localizer.Format(Keys.Messages.DriverReachedBranchName, result.Value));
            }
            else
            {
                return Fail(_localizer[result.Value]);
            }
        }


        [Route("DriverChangePhoto")]
        [HttpPost]
        [Authorize(AgentPermissions.Profile.UpdateProfilePicture)]
        public async Task<IActionResult> ChangePhotoAsync([FromForm] DriverPhotoViewModel driverPhotoViewModel)
        {

            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            var driver = await _driverManager.GetByUserIDAsync(_currentUser.Id);
            if (driverPhotoViewModel.photo != null)//Image Base64
            {
                var path = "DriverImages";
                var processResult = await _uploadFormFileService.UploadFileAsync(driverPhotoViewModel.photo, path);
                if (processResult.IsSucceeded)
                {
                    driver.ImageUrl = processResult.ReturnData;
                }
                else
                {
                    return BadRequest(processResult.Exception);
                }
            }
            var res = await _driverManager.UpdateAsync(driver);

            return Ok(res);
        }

        [Route("DriverUpdateProfile")]
        [HttpPost]
        [Authorize(AgentPermissions.Profile.UpdateProfile)]
        public async Task<IActionResult> UpdateProfileAsync([FromBody] EditDriverProfileViewModel driverProfileViewModel)
        {
            // Response to Return With Success or Fail
            var response = new EntityResult<string>();
            // Get User
            var oldUser = await _appUserManager.GetByUserIdAsync(_currentUser.Id);

            if (!string.IsNullOrWhiteSpace(driverProfileViewModel.CurrentPassword) ||
                !string.IsNullOrWhiteSpace(driverProfileViewModel.NewPassword) ||
                !string.IsNullOrWhiteSpace(driverProfileViewModel.ConfirmPassword))
            {

                //change Password 
                bool result = await _appUserManager.ChangePasswordAsync(
                    oldUser, driverProfileViewModel.CurrentPassword, driverProfileViewModel.NewPassword);

                if (!result)
                {
                    response.Error = _localizer[Keys.Validation.CouldNotChangeDriverPassword];
                    response.Succeeded = false;
                    return Ok(response);
                }
            }

            //oldUser.PhoneNumber = driverProfileViewModel.phone;
            //var res = await _appUserManager.UpdateAsync(oldUser);

            //if (!res.Succeeded)
            //{
            //    response.Error = res.Errors.FirstOrDefault()?.Description;
            //    response.Succeeded = false;
            //    return Ok(response);
            //}

            return Ok(response);
        }

        [Route("ChangeDriverPassword")]
        [HttpPut]
        [Authorize(AgentPermissions.Profile.UpdateProfile)]
        public async Task<IActionResult> ChangeDriverPasswordAsync([FromBody] DriverChangePasswordViewModel changePasswordViewModel)
        {
            // Response to Return With Success or Fail
            var response = new EntityResult<string>();
            // Get User
            var oldUser = await _appUserManager.GetByUserIdAsync(_currentUser.Id);

            if (!string.IsNullOrWhiteSpace(changePasswordViewModel.CurrentPassword) ||
                !string.IsNullOrWhiteSpace(changePasswordViewModel.NewPassword) ||
                !string.IsNullOrWhiteSpace(changePasswordViewModel.ConfirmPassword))
            {

                //change Password 
                bool result = await _appUserManager.ChangePasswordAsync(
                    oldUser, changePasswordViewModel.CurrentPassword, changePasswordViewModel.NewPassword);

                if (!result)
                {
                    response.Error = _localizer[Keys.Validation.CouldNotChangeDriverPassword];
                    response.Succeeded = false;
                    return Ok(response);
                }
            }
            
            return Ok(response);
        }
    }
}