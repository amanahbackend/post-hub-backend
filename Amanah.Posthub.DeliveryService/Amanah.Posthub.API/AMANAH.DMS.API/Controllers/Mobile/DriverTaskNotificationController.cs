﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Drivers.Queries;
using Amanah.Posthub.BLL.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    public class DriverTaskNotificationController : BaseController
    {
        private readonly IDriverTaskNotificationQuery _driverTaskNotificationQuery;
        private readonly IDriverTaskNotificationService _driverTaskNotificationService;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;

        public DriverTaskNotificationController(
            IDriverTaskNotificationService driverTaskNotificationService,
            IDriverTaskNotificationQuery driverTaskNotificationQuery,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _driverTaskNotificationService = driverTaskNotificationService;
            _driverTaskNotificationQuery = driverTaskNotificationQuery;
            _localizer = localizer;
            _currentUser = currentUser;
        }

        [Route("GetByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetByPaginationAsync([FromBody] PaginatedTasksDriverMobileInputDTO pagingparametermodel)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            pagingparametermodel.DriverId = _currentUser.DriverId.Value;
            var AllDriverTaskNotification = await _driverTaskNotificationQuery.GetByPaginationAsync(pagingparametermodel);
            return Ok(AllDriverTaskNotification);
        }

        [Route("MarkAsRead")]
        [HttpGet]
        public async Task<ActionResult> MarkAsReadAsync()
        {
            if (!_currentUser.IsDriver())
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            var isSuccess = await _driverTaskNotificationService.MarkAsReadAsync(_currentUser.DriverId.Value);
            return Ok(_localizer[isSuccess ? Keys.Messages.AllNotificationsMarkedAsRead : Keys.Messages.TaskRequestIsFailed]);
        }

        [Route("GetUnReadCount")]
        [HttpGet]
        public ActionResult GetUnReadCount()
        {
            if (!_currentUser.IsDriver())
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            var count = _driverTaskNotificationQuery.GetUnReadCount(_currentUser.DriverId.Value);
            return Ok(count);
        }

        [Route("Clear")]
        [HttpGet]
        public async Task<ActionResult> ClearAsync()
        {
            if (!_currentUser.IsDriver())
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            var isSuccess = await _driverTaskNotificationService.ClearAsync(_currentUser.DriverId.Value);
            return Ok(_localizer[isSuccess ? Keys.Messages.AllNotificationsCleared : Keys.Messages.TaskRequestIsFailed]);
        }
    }
}