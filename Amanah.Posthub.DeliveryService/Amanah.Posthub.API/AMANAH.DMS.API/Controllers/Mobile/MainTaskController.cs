﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Drivers.Queries;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.TasksFolder.Queries;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class MainTaskController : BaseController
    {
        private readonly IMobileMainTaskService _taskService;
        private readonly IMainTaskQuery _taskQuery;
        private readonly IDriverQuery _driverQuery;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;

        private readonly ISettingsManager _settingManager;

        public MainTaskController(
            IMobileMainTaskService taskService,
            IMainTaskQuery taskQuery,
            IDriverQuery driverQuery,
            ISettingsManager settingManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _taskService = taskService;
            _taskQuery = taskQuery;
            _driverQuery = driverQuery;
            _settingManager = settingManager;
            _localizer = localizer;
            _currentUser = currentUser;
        }

        [Route("GetMyTasks")]
        [HttpPost]
        public async Task<ActionResult> GetMyTasksAsync([FromBody] PaginatedTasksDriverMobileInputDTO paginatedTasksDriverViewModel)
        {
            var driver = await _driverQuery.GetDriverByUserIdAsync(_currentUser.Id);
            if (driver == null)
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            paginatedTasksDriverViewModel.DriverId = driver.Id;
            int autoAllocationType = await _settingManager.GetAutoAllocationTypeidAsync();

            var result = await _taskQuery.GetDriverTasksAsync(paginatedTasksDriverViewModel);
            return Ok(new MainTaskPagedResultContainer<MainTaskMobileResultWithTasksDTO>
            {
                DriverAgentStatusId = driver.AgentStatusId ?? (int)AgentStatusesEnum.Unavailable,
                DriverIsSetReached = driver.ReachedTime.HasValue,
                AutoAllocationType = autoAllocationType,
                Result = result != null ? result.Result : new List<MainTaskMobileResultWithTasksDTO>(),
                TotalCount = result != null ? result.TotalCount : 0,
                IsInClubbingTime = driver.IsInClubbingTime,
                ClubbingTimeExpiration = driver.ClubbingTimeExpiration
            });
        }

        [Route("GetNewTasks")]
        [HttpGet]
        public async Task<ActionResult> GetNewTasksAsync()
        {
            var driver = await _driverQuery.GetDriverByUserIdAsync(_currentUser.Id);
            if (driver != null)
            {
                return Ok(await _taskQuery.GetDriverNewTasks(driver.Id).ToListAsync());
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
        }

        [Route("Accept")]
        [HttpGet]
        public async Task<ActionResult> AcceptAsync(int id, double? longitude = null, double? latitude = null)
        {
            var taskTenantId = await _taskService.DriverAcceptOrDeclineOperationAsync(id, true, longitude, latitude);
            var driver = await _driverQuery.GetByUserIdAsync(_currentUser.Id);
            return Ok();
        }

        [Route("Decline")]
        [HttpGet]
        public async Task<ActionResult> DeclineAsync(int id, double? longitude = null, double? latitude = null)
        {
            var taskTenantId = await _taskService.DriverAcceptOrDeclineOperationAsync(id, false, longitude, latitude);
            var driver = await _driverQuery.GetByUserIdAsync(_currentUser.Id);
            return Ok();
        }


        [Route("GetTaskHistoryDetails")]
        [HttpPost]
        public async Task<ActionResult> GetTaskHistoryDetailsAsync([FromBody] PaginatedTasksDriverMobileInputDTO paginatedTasksDriverViewModel)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            paginatedTasksDriverViewModel.DriverId = _currentUser.DriverId.Value;
            return Ok(await _taskQuery.GetTaskHistoryDetailsAsync(paginatedTasksDriverViewModel));
        }
    }
}