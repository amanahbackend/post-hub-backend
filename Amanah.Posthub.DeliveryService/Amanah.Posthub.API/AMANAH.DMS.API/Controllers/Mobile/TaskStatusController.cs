﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TaskStatusController : BaseController
    {
        private readonly ITaskStatusManager taskStatusManager;
        public TaskStatusController(ITaskStatusManager taskStatusManager)
        {
            this.taskStatusManager = taskStatusManager;
        }

        // GET: TaskStatus
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All TaskStatuss 
            var allTaskStatuss = await taskStatusManager.GetAllAsync<TaskStatusResultDto>(true);
            return Ok(allTaskStatuss);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All TaskStatuss By Pagination 
            var allTaskStatues = await taskStatusManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(allTaskStatues);
        }

    }
}