﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Drivers.Queries;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Tasks.Services.DTOs;
using Amanah.Posthub.BLL.TasksFolder.Queries;
using Amanah.Posthub.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers.Mobile
{
    [Route("mobile/api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TasksController : BaseController
    {
        private readonly ISubTaskQuery _subTaskQuery;
        private readonly IMobileSubTaskService _subTaskService;
        private readonly IDriverQuery _driverQuery;
        private readonly ICurrentUser _currentUser;
        private readonly ILocalizer _localizer;
        private readonly IBackGroundNotificationSercvice _notificationService;

        public TasksController(
            ICurrentUser currentUser,
            ILocalizer localizer,
            IBackGroundNotificationSercvice notificationService,
            ISubTaskQuery subTaskQuery,
            IDriverQuery driverQuery,
            IMobileSubTaskService subTaskService)
        {
            _currentUser = currentUser;
            _localizer = localizer;
            _notificationService = notificationService;

            _subTaskQuery = subTaskQuery;
            _subTaskService = subTaskService;
            _driverQuery = driverQuery;
        }

        [HttpGet("SetReached")]
        public async Task<IActionResult> SetReachedAsync(int taskId, double longitude, double latitude)
        {
            var result = await _subTaskService.SetReachedAsync(taskId, longitude, latitude);
            if (result.IsSucceeded)
            {
                return Success(result.ReturnData);
            }
            return Fail(result.Message);
        }

        [HttpGet("Start")]
        public async Task<IActionResult> StartAsync(int id, double longitude, double latitude)
        {
            var driver = await _driverQuery.GetByUserIdAsync(_currentUser.Id);
            if (driver.IsInClubbingTime)
            {
                return Fail(_localizer[Keys.Validation.CannotStartTaskWhileCluppingTime]);
            }

            var result = await _subTaskService.StartAsync(id, longitude, latitude);
            if (!result.Key)
            {
                return Fail(result.Value);
            }

            _notificationService.SendToUserManagers(result.Value,
               driver.UserId,
               "TaskStart",
                _localizer[Keys.Notifications.DriverStartedTask],
               _localizer.Format(Keys.Notifications.DriverNameStartedTaskNo, driver.FullName, id),
               new { id, driver });

            return Success();
        }

        [HttpPost("Successful")]
        public async Task<ActionResult> SuccessfulAsync([FromForm] SuccessOrFailTaskInputDTO successTask)
        {
            var taskTenantId = await _subTaskService.MarkSubTaskAsSussessOrFailAsync(successTask, (int)TaskStatusEnum.Successful);

            var driver = await _driverQuery.GetByUserIdAsync(_currentUser.Id);
            _notificationService.SendToUserManagers(
               taskTenantId,
               driver.UserId,
               "TaskSuccessful",
               _localizer[Keys.Notifications.DriverCompletedTask],
               _localizer.Format(Keys.Notifications.DriverNameAcceptedTaskNo, driver.FullName, successTask.MainTaskId),
               new { successTask.TaskId, driver });

            return Ok();
        }

        [HttpPost("FaildWithSignature")]
        public async Task<ActionResult> FaildWithSignatureAsync([FromForm] SuccessOrFailTaskInputDTO failTask)
        {
            var taskTenantId = await _subTaskService.MarkSubTaskAsSussessOrFailAsync(failTask, (int)TaskStatusEnum.Failed);

            var driver = await _driverQuery.GetByUserIdAsync(_currentUser.Id);
            _notificationService.SendToUserManagers(taskTenantId,
               driver.UserId,
               "TaskFailed",
               _localizer[Keys.Notifications.DriverFailedTask],
               _localizer.Format(Keys.Notifications.DriverNameFailedTaskNo, driver.FullName, failTask.MainTaskId),
               new { failTask.TaskId, driver, failTask.Reason });

            return Ok();
        }


        [HttpGet("Cancel")]
        public async Task<ActionResult> CancelAsync(int id, string reason, double longitude, double latitude)
        {
            var taskTenantId = await _subTaskService.MarkSubTaskAsCancelOrDeclineAsync(id, TaskStatusEnum.Cancelled, longitude, latitude, reason);
            var driver = await _driverQuery.GetByUserIdAsync(_currentUser.Id);
            var title = _localizer[Keys.Notifications.DriverCanceledTask];
            var body = _localizer.Format(Keys.Notifications.DriverNameCanceledTaskNo, driver.FullName, id);
            _notificationService.SendToUserManagers(taskTenantId, driver.UserId, "TaskCancel", title, body, new { id, reason });

            return Ok();
        }


        [HttpGet("Decline")]
        public async Task<ActionResult> DeclineAsync(int id, double longitude, double latitude)
        {
            var taskTenantId = await _subTaskService.MarkSubTaskAsCancelOrDeclineAsync(id, TaskStatusEnum.Declined, longitude, latitude, null);

            var driver = await _driverQuery.GetByUserIdAsync(_currentUser.Id);
            _notificationService.SendToUserManagers(taskTenantId,
               driver.UserId,
               "TaskDecline",
               _localizer[Keys.Notifications.DriverDeclinedTask],
                _localizer.Format(Keys.Notifications.DriverNameDeclinedTaskNo, driver.FullName, id),
               id);

            return Ok();
        }

        [HttpPost("GetDriverFinishedTasks")]
        public async Task<ActionResult> GetDriverFinishedTasksAsync(PaginatedTasksDriverMobileInputDTO paginatedTasksDriverViewModel)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            paginatedTasksDriverViewModel.DriverId = _currentUser.DriverId.Value;
            var result = await _subTaskQuery.GetDriverFinishedTasksAsync(paginatedTasksDriverViewModel);
            return Ok(result);
        }

        [HttpPost("GetTaskCalender")]
        public async Task<ActionResult> GetTaskCalenderAsync([FromBody] CalenderMobileInputDTO calenderViewModel)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            calenderViewModel.Driver_id = _currentUser.DriverId.Value;
            var tasks = await _subTaskQuery.GetTaskCalenderAsync(calenderViewModel);
            return Ok(tasks);
        }

        [HttpPost("GetTravelSummery")]
        public async Task<ActionResult> GetTravelSummeryAsync(DateTime date)
        {
            if (!_currentUser.IsDriver())
            {
                return BadRequest(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }
            var result = await _subTaskQuery.TravelSummeryAsync(date, _currentUser.DriverId.Value);
            return Ok(result);
        }

        [HttpGet("GetTaskStatusCountAsync")]
        public async Task<ActionResult> GetTaskStatusCountAsync(DateTime taskDate)
        {
            var taskStatus = await _subTaskQuery.GetTaskStatusCountAsync(taskDate);
            return Ok(new { IsSuccessful = true, data = taskStatus });
        }

    }
}