﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Notifications.Queries;
using Amanah.Posthub.BLL.Notifications.Services;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.DeliverService.BLL.Notifications.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class NotificationController : Controller
    {
        public readonly INotificationQuery _notificationQuery;
        public readonly INotificationService _notificationService;
        private readonly ICurrentUser _currentUser;

        public NotificationController(
            ICurrentUser currentUser,
            INotificationQuery notificationQuery,
            INotificationService notificationService)
        {
            _currentUser = currentUser;
            _notificationQuery = notificationQuery;
            _notificationService = notificationService;
        }

        [HttpPost("GetAllByPagination")]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] NotificatonFilterInputDTO filter)
        {
            filter.UserID = _currentUser.Id;
            return Ok(await _notificationQuery.GetAllByPaginationAsyncForUserAsync(filter));
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            return Ok(await _notificationQuery.GetAllAsync());
        }

        [HttpGet("Get")]
        public async Task<ActionResult> GetAsync(int id)
        {
            return Ok(await _notificationQuery.GetAsync(id));
        }

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync([FromRoute] int id)
        {
            return Ok(await _notificationQuery.GetAsync(id));
        }

        [HttpGet("MarkAsRead")]
        public async Task<ActionResult> MarkAsReadAsync()
        {
            return Ok(await _notificationService.MarkAsReadAsync(_currentUser.Id));
        }

        [HttpGet("UnReadCount")]
        public async Task<ActionResult> UnReadCountAsync()
        {
            return Ok(await _notificationQuery.GetUnReadCountAsync(_currentUser.Id));
        }

        [HttpPost("MarkThemAsRead")]
        public async Task<ActionResult> MarkThemAsReadAsync(List<NotificationViewModel> notificationlist)
        {
            return Ok(await _notificationService.MarkAsReadAsync(notificationlist.Select(x => x.Id).ToList()));
        }
    }
}