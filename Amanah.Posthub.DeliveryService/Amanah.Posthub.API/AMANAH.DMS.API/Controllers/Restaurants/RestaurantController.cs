﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Restaurants.Queries;
using Amanah.Posthub.BLL.Restaurants.Queries.DTOs;
using Amanah.Posthub.BLL.Restaurants.Services;
using Amanah.Posthub.BLL.Restaurants.Services.DTOs;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class RestaurantController : Controller
    {
        private readonly IUserCommonQuery _userCommonQuery;
        private readonly IRestaurantQuery _restaurantQuery;
        private readonly IRestaurantService _restaurantService;
        private readonly ICurrentUser _currentUser;


        public RestaurantController(
             IUserCommonQuery userCommonQuery,
             IRestaurantQuery restaurantQuery,
             IRestaurantService restaurantService,
             ICurrentUser currentUser)
        {
            _userCommonQuery = userCommonQuery;
            _restaurantQuery = restaurantQuery;
            _restaurantService = restaurantService;
            _currentUser = currentUser;
        }


        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            List<int> managerResturantIds = null;
            if (_currentUser.IsManager())
            {
                managerResturantIds = await _userCommonQuery.GetManagerRestaurantsAsync();
                if (managerResturantIds == null || !managerResturantIds.Any())
                {
                    return Ok();
                }
            }
            return Ok(await _restaurantQuery.GetAllAsync(managerResturantIds));
        }

        [HttpPost("GetAllByPagination")]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] RestaurantFilterDTO filter)
        {
            List<int> managerResturantIds = null;
            if (_currentUser.IsManager())
            {
                managerResturantIds = await _userCommonQuery.GetManagerRestaurantsAsync();
                if (managerResturantIds == null || !managerResturantIds.Any())
                {
                    return Ok();
                }
            }
            return Ok(await _restaurantQuery.GetAllByPaginationAsync(filter, managerResturantIds));
        }

        [HttpGet("Get")]
        public async Task<ActionResult> GetAsync(int id)
        {
            return Ok(await _restaurantQuery.GetAsync(id));
        }

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync([FromRoute] int id)
        {
            return Ok(await _restaurantQuery.GetAsync(id));
        }


        [HttpPost("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] CreateRestaurantInputDTO createRestaurant)
        {
            await _restaurantService.CreateAsync(createRestaurant);

            return Ok();
        }

        [HttpPut("Update")]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateRestaurantInputDTO updateRestaurant)
        {
            await _restaurantService.UpdateAsync(updateRestaurant);

            return Ok();
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync([FromRoute] int id)
        {
            await _restaurantService.DeleteAsync(id);
            return Ok();
        }

        [HttpGet("SetActivate")]
        public async Task<ActionResult> SetActivateAsync(int id, bool isActive, string Reason)
        {
            await _restaurantService.ActivateOrDeActivateAsync(id, isActive, Reason);
            return Ok();
        }
    }
}
