﻿using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.TenantRegistrations.ViewModels;
using Amanah.Posthub.BLL.UserManagement.Permissions;
using Amanah.Posthub.BLL.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers.Roles
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly ILocalizer _localizer;

        public RolesController(
            ILocalizer localizer)
        {
            _localizer = localizer;
        }

        [HttpGet("GetDefaultTenantPermissions")]
        [Authorize(PlatformPermissions.Tenants.ReadDefaultPermissionTenants)]
        public Task<List<FeaturePermissionViewModel>> GetDefaultTenantPermissionsAsync()
        {
            var result = TenantPermissions.FeaturePermissions
                .Select(feature =>
                    new FeaturePermissionViewModel
                    {
                        Name = _localizer[feature.Name],
                        Permissions = feature.Permissions
                            .Select(permission =>
                                new PermissionViewModel
                                {
                                    Value = permission,
                                    Name = _localizer[permission]
                                })
                            .ToList()
                    })
                .ToList();
            return Task.FromResult(result);
        }

        [HttpGet("GetDefaultPlatformPermissions")]
        [Authorize(PlatformPermissions.Managers.ReadDefaultPermissionManagers)]
        public Task<List<FeaturePermissionViewModel>> GetDefaultPlatformPermissionsAsync()
        {
            var result = PlatformPermissions.FeaturePermissions
                .Select(feature =>
                    new FeaturePermissionViewModel
                    {
                        Name = _localizer[feature.Name],
                        Permissions = feature.Permissions
                            .Select(permission =>
                                new PermissionViewModel
                                {
                                    Value = permission,
                                    Name = _localizer[permission]
                                })
                            .ToList()
                    })
                .ToList();
            return Task.FromResult(result);
        }

        [HttpGet("GetDefaultModule")]
        public ActionResult GetDefaultModule()
        {
            var defaultModules = Enum.GetValues(typeof(DefaultTenantModule))
                      .Cast<DefaultTenantModule>()
                      .Select(d =>
                      new
                      {
                          Id = (int)d,
                          Name = _localizer[d.ToString()]
                      })
                      .ToList();

            return Ok(defaultModules);
        }

    }
}
