﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class SettingsController : Controller
    {
        private readonly ILocalizer _localizer;
        private readonly ISettingsManager _settingManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly ICurrentUser _currentUser;
        private readonly IBackGroundNotificationSercvice _notificationService;


        public SettingsController(
            ISettingsManager settingManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer,
            ICurrentUser currentUser,
            IBackGroundNotificationSercvice notificationService)


        {
            _settingManager = settingManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
            _currentUser = currentUser;
            _notificationService = notificationService;
        }

        [HttpGet("GetAutoAllocationMethod")]
        public ActionResult GetAutoAllocationMethod()
        {
            var defaultModules = new List<object>
            {
                new
                      {
                          Id = SettingKeyConstant.NearestAvailableMethod,
                          Name = _localizer[SettingKeyConstant.NearestAvailableMethod]
                      },
                  new
                      {
                          Id = SettingKeyConstant.OneByOneMethod,
                          Name = _localizer[SettingKeyConstant.OneByOneMethod]
                      },
            };

            return Ok(defaultModules);
        }

        // GET: Settings

        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All Settings 
            var AllSettings = await _settingManager.GetAllAsync<SettingViewModel>();
            return Ok(AllSettings);
        }
        // GET: Settings

        [Route("GetSettingByGroupId")]
        [HttpGet]
        public async Task<ActionResult> GetSettingByGroupIdAsync(string GroupId)
        {
            ///Get All Settings 
            var AllSettings = await _settingManager.GetSettingByGroupIdAsync(GroupId);
            return Ok(AllSettings);
        }


        [Route("GetByKey/{key}")]
        [HttpGet]
        public async Task<ActionResult> GetByKey(string key)
        {
            var setting = await _settingManager.GetSettingByKeyAsync(key);
            return Ok(setting);
        }



        // GET: Settings/Details/5
        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var setting = await _settingManager.GetAsync(id);
            return Ok(setting);

        }


        // POST: Settings/Create
        [HttpPost]
        [Route("Create")]
        public async Task<ActionResult> CreateAsync([FromBody] SettingViewModel settingVM)
        {
            var created_setting = await _settingManager.AddAsync(settingVM);
            if (created_setting.SettingKey == "IsEnableAutoAllocation")
            {
                string isOnOff = created_setting.Value == "true" ? "On" : "Off";
                AccountLogs accountLog = new AccountLogs()
                {
                    TableName = "Setting",
                    ActivityType = "Auto-allocation",
                    Description = "Auto-allocation has been switched ( " + isOnOff + " )  by " + User.Identity.Name,
                    Record_Id = settingVM.Id

                };
                await _accountLogManager.Create(accountLog);
                if (created_setting.SettingKey == "SelectedAutoAllocationMethodName")
                {
                    int autoAllocationType = await GetAutoAllocationTypeCodeAsync(created_setting);
                    _notificationService.BroadcastToTenant(_currentUser.Id, "AutoAllocationSettingsChanged", "AutoAllocationSettingsChanged", "AutoAllocation Changed", autoAllocationType);

                }
            }

            return Ok(created_setting);
        }



        // GET: Settings/Edit/5


        // POST: Settings/Edit/5
        [HttpPut]
        public async Task<ActionResult> Update([FromBody] SettingViewModel settingVM)
        {
            bool result = await InternalUpdateAsync(settingVM);

            return Ok(result);
        }

        // GET: Settings/Delete/5
        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var settingToDelete = await _settingManager.GetAsync(id);
            var result = await _settingManager.SoftDeleteAsync(settingToDelete);
            // TODO: Add insert logic here

            if (result)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteSetting]);
            }
        }


        // POST: Settings/Create
        [HttpPost]
        [Route("UpdateRange")]
        public async Task<ActionResult> UpdateRange([FromBody] List<SettingViewModel> settingVMList)
        {
            var result = true;
            foreach (var settingVM in settingVMList)
            {
                result = result && await InternalUpdateAsync(settingVM);
            }

            return Ok(result);
        }




        private async Task<int> GetAutoAllocationTypeCodeAsync(SettingViewModel autoallocationSetting)
        {
            var enableAutoallocationSetting = await _settingManager.GetSettingByKeyAsync("IsEnableAutoAllocation");
            int autoAllocationType = enableAutoallocationSetting == null
                ? (int)AutoAllocationTypeEnum.Manual
                : enableAutoallocationSetting.Value.ToLower() == "false"
                    ? (int)AutoAllocationTypeEnum.Manual
                    : autoallocationSetting == null
                        ? (int)AutoAllocationTypeEnum.Manual
                        : autoallocationSetting.Value == "FirstInFirstOutMethod"
                            ? (int)AutoAllocationTypeEnum.Fifo
                            : autoallocationSetting.Value == "NearestAvailableMethod"
                                ? (int)AutoAllocationTypeEnum.Nearest
                                : autoallocationSetting.Value == "OneByOneMethod"
                                    ? (int)AutoAllocationTypeEnum.OneByOne
                                    : (int)AutoAllocationTypeEnum.Manual;

            return autoAllocationType;
        }

        //TODO: this need to moved to service, and no need to to all those trips this should be done in single db trip
        private async Task<bool> InternalUpdateAsync(SettingViewModel settingVM)
        {
            var setting = await _settingManager.GetSettingByKeyAsync(settingVM.SettingKey);
            bool result;
            if (setting == null)
            {
                setting = await _settingManager.AddAsync(settingVM);
                result = setting != null;
            }
            else
            {
                settingVM.Id = setting.Id;
                result = await _settingManager.UpdateAsync(settingVM);
            }
            if (result)
            {
                if (settingVM.SettingKey == "IsEnableAutoAllocation")
                {
                    string isOnOff = settingVM.Value == "true" ? "On" : "Off";
                    AccountLogs accountLog = new AccountLogs()
                    {
                        TableName = "Setting",
                        ActivityType = "Auto-allocation",
                        Description = "Auto-allocation has been switched ( " + isOnOff + " )  ",
                        Record_Id = settingVM.Id

                    };
                    await _accountLogManager.Create(accountLog);

                }
                if (settingVM.SettingKey == "SelectedAutoAllocationMethodName")
                {
                    int autoAllocationType = await GetAutoAllocationTypeCodeAsync(settingVM);
                    _notificationService.BroadcastToTenant(_currentUser.TenantId, "AutoAllocationSettingsChanged", "AutoAllocationSettingsChanged", "AutoAllocation Settings Changed", autoAllocationType);
                }
            }

            return result;
        }
    }
}
