﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Tasks.WebQueries;
using Amanah.Posthub.BLL.Tasks.WebQueries.DTOs;
using Amanah.Posthub.BLL.Tasks.WebServices;
using Amanah.Posthub.BLL.Tasks.WebServices.DTOs;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class MainTaskController : Controller
    {
        private readonly ISettingsManager _settingManager;

        private readonly IUserCommonQuery _userCommonQuery;
        private readonly ICurrentUser _currentUser;
        private readonly IMainTaskQuery _mainTaskQuery;
        private readonly IMainTaskService _mainTaskService;

        public MainTaskController(
            ISettingsManager settingManager,

            IUserCommonQuery userCommonQuery,
            ICurrentUser currentUser,
            IMainTaskQuery mainTaskQuery,
            IMainTaskService mainTaskService)
        {
            _settingManager = settingManager;
            _userCommonQuery = userCommonQuery;
            _currentUser = currentUser;
            _mainTaskQuery = mainTaskQuery;
            _mainTaskService = mainTaskService;
        }


        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            var mainTasks = await _mainTaskQuery.GetAllAsync();
            return Ok(mainTasks);
        }

        [Route("GetAllByPagination")]
        [HttpPost]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] MainTaskFilterDTO mainTaskFilterDTO)
        {
            var mainTasks = await _mainTaskQuery.GetAllByPaginationAsync(mainTaskFilterDTO);
            return Ok(mainTasks);
        }

        [Route("Details/{id}")]
        [HttpGet]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            var mainTask = await _mainTaskQuery.GetAsync(id);
            return Ok(mainTask);
        }

        [HttpPost]
        [Route("TaskHasGeofences")]
        public async Task<ActionResult> TaskHasGeofencesAsync([FromBody] MainTaskInputDTO mainTask)
        {
            var hasGeofences = await _mainTaskQuery.TaskHasGeofencesAsync(mainTask);
            return Ok(new { hasGeofences });
        }

        [Route("GetUnassigned")]
        [HttpPost]
        [Authorize(TenantPermissions.Task.ReadUnassignedTask)]
        public async Task<ActionResult> GetUnassignedAsync([FromBody] TaskFilterDTO pagingparametermodel)
        {
            pagingparametermodel = await HandleManagerFilterationAsync(pagingparametermodel);
            if (pagingparametermodel == null)
            {
                return Ok();
            }

            pagingparametermodel.TaskStatusIds = new List<int>
            {
                (int)TaskStatusEnum.Unassigned
            };
            var unAssignedMainTasks = await _mainTaskQuery.GetMainTasksByStatusAsync(pagingparametermodel, false);
            if (unAssignedMainTasks != null)
            {
                var enableAutoallocationSetting = await _settingManager.GetSettingByKeyAsync("IsEnableAutoAllocation");
                int autoAllocationType = await _settingManager.GetAutoAllocationTypeidAsync();
                foreach (var MainTask in unAssignedMainTasks.Result)
                {
                    MainTask.IsEnableAutoAllocation = enableAutoallocationSetting.Value.ToLower() != "false";
                    MainTask.AutoAllocationType = autoAllocationType;
                }
            }

            return Ok(unAssignedMainTasks);
        }

        [Route("GetAssigned")]
        [HttpPost]
        public async Task<ActionResult> GetAssignedAsync([FromBody] TaskFilterDTO pagingparametermodel)
        {
            pagingparametermodel = await HandleManagerFilterationAsync(pagingparametermodel);
            if (pagingparametermodel == null)
            {
                return Ok();
            }

            pagingparametermodel.TaskStatusIds = new List<int>
            {
              (int)TaskStatusEnum.Assigned,
              (int)TaskStatusEnum.Accepted,
              (int)TaskStatusEnum.Cancelled,
              (int)TaskStatusEnum.Declined,
              (int)TaskStatusEnum.Failed,
              (int)TaskStatusEnum.Inprogress,
              (int)TaskStatusEnum.Started,
              (int)TaskStatusEnum.Successful
            };
            var assignedTasks = await _mainTaskQuery.GetMainTasksByStatusAsync(pagingparametermodel, false);

            return Ok(assignedTasks);
        }

        [Route("GetCompleted")]
        [HttpPost]
        public async Task<ActionResult> GetCompletedAsync([FromBody] TaskFilterDTO pagingparametermodel)
        {
            pagingparametermodel = await HandleManagerFilterationAsync(pagingparametermodel);
            if (pagingparametermodel == null)
            {
                return Ok();
            }

            var completedMainTasks = await _mainTaskQuery.GetMainTasksByStatusAsync(pagingparametermodel, true);

            return Ok(completedMainTasks);
        }

        [Route("GetConnectedTasks")]
        [HttpPost]
        public async Task<ActionResult> GetConnectedTasksAsync([FromBody] TaskFilterDTO pagingparametermodel)
        {
            pagingparametermodel = await HandleManagerFilterationAsync(pagingparametermodel);
            if (pagingparametermodel == null)
            {
                return Ok();
            }

            return Ok(await _mainTaskQuery.GetMainTasksByStatusAsync(pagingparametermodel, null));
        }

        [Route("GetDriverTasks")]
        [HttpPost]
        public async Task<ActionResult> GetDriverTasksAsync([FromBody] TaskDriverFilterDTO pagingparametermodel)
        {
            var result = await _mainTaskQuery.GetDriverUncompletedTasksAsync(pagingparametermodel);
            return Ok(result);
        }

        private async Task<TaskFilterDTO> HandleManagerFilterationAsync(TaskFilterDTO taskFilter)
        {
            if (_currentUser.IsManager())
            {
                taskFilter.TeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (taskFilter.TeamIds == null || !taskFilter.TeamIds.Any())
                {
                    return null;
                }
                if (taskFilter.BranchesIds == null || !taskFilter.BranchesIds.Any())
                {
                    taskFilter.BranchesIds = await _userCommonQuery.GetManagerBranchesAsync();
                    taskFilter.GetCustomerTasks = true;
                }
            }

            return taskFilter;
        }

        [HttpPost("Create"), DisableRequestSizeLimit]
        [Authorize(TenantPermissions.Task.CreateTask)]
        public async Task<ActionResult> CreateAsync([FromForm] CreatedMainTaskInputDTO createMainTask)
        {
            var createdMainTaskId = await _mainTaskService.CreateAsync(createMainTask);
            if (createdMainTaskId.HasValue)
            {
                return StatusCode((int)HttpStatusCode.Created, createdMainTaskId);
            }
            else
            {
                //ToDo Refactor to return error message
                return BadRequest();
            }
        }

        [Route("ReassignMainTask")]
        [HttpPost]
        public async Task<ActionResult> ReassignMainTaskAsync([FromBody] ReassignDriverInputDTO reassignDriverMainTaskVM)
        {
            await _mainTaskService.ReassignDriverAsync(reassignDriverMainTaskVM);
            return Ok();
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _mainTaskService.DeleteAsync(id);
            return Ok(true);
        }


        [Route("TryAutoAssignmentAgain/{id}")]
        [HttpGet]
        public async Task<ActionResult> TryAutoAssignmentAgainAsync(int id)
        {
            //  await _mainTaskManager.TryAutoAssignmentAgain(id);
            await _mainTaskService.TryAutoAssignmentAgainAsync(id);
            return Ok();
        }

    }
}