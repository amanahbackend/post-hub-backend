﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class MainTaskStatusController : Controller
    {
        private readonly IMainTaskStatusManager _mainTaskStatusManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly ILocalizer _localizer;

        public MainTaskStatusController(
            IMainTaskStatusManager MainTaskStatusManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer)
        {
            _mainTaskStatusManager = MainTaskStatusManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
        }

        // GET: MainTaskStatus
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All MainTaskStatuss 
            var AllMainTaskStatuss = await _mainTaskStatusManager.GetAllAsync<LkpResultDto>(true);
            return Ok(AllMainTaskStatuss);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All MainTaskStatuss By Pagination 
            var AllTeams = await _mainTaskStatusManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }



    }
}