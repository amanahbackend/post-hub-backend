﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class MainTaskTypeController : Controller
    {
        private readonly IMainTaskTypeManager _mainTaskTypeManager;
        private readonly IAccountLogManager _accountLogManager;
        private readonly ILocalizer _localizer;

        public MainTaskTypeController(
            IMainTaskTypeManager mainTaskTypeManager,
            IAccountLogManager accountLogManager,
            ILocalizer localizer)
        {
            _mainTaskTypeManager = mainTaskTypeManager;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
        }

        // GET: MainTaskType
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All MainTaskTypes 
            var AllMainTaskTypes = await _mainTaskTypeManager.GetAllAsync<LkpResultDto>(true);
            return Ok(AllMainTaskTypes);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All MainTaskTypes By Pagination 
            var AllTeams = await _mainTaskTypeManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }

    }
}