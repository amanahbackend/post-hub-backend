﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TaskStatusController : Controller
    {
        private readonly ILocalizer _localizer;
        private ITaskStatusManager _taskStatusManager;

        public TaskStatusController(ITaskStatusManager TaskStatusManager, ILocalizer localizer)
        {
            _taskStatusManager = TaskStatusManager;
            _localizer = localizer;
        }

        // GET: TaskStatus
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All TaskStatuss 
            var AllTaskStatuss = await _taskStatusManager.GetAllAsync<TaskStatusResultDto>(true);
            return Ok(AllTaskStatuss);
        }


        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All TaskStatuss By Pagination 
            var AllTeams = await _taskStatusManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }


    }
}