﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TaskTypeController : Controller
    {
        private readonly ITaskTypeManager _taskTypeManager;

        public TaskTypeController(ITaskTypeManager taskTypeManager)
        {
            _taskTypeManager = taskTypeManager;
        }

        // GET: TaskType
        [Route("GetAll")]
        [HttpGet]
        public async Task<ActionResult> GetAllAsync()
        {
            ///Get All TaskTypes 
            var AllTaskTypes = await _taskTypeManager.GetAllAsync<LkpResultDto>(true);
            return Ok(AllTaskTypes);
        }

        [Route("GetAllByPagination")]
        [HttpGet]
        public async Task<ActionResult> GetAllByPaginationAsync([FromQuery] PaginatedItemsViewModel pagingparametermodel)
        {
            ///Get All TaskTypes By Pagination 
            var AllTeams = await _taskTypeManager.GetAllByPaginationAsync(true, pagingparametermodel);
            return Ok(AllTeams);
        }


    }
}