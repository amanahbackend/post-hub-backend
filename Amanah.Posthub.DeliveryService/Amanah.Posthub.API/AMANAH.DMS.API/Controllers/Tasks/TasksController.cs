﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Tasks.WebQueries;
using Amanah.Posthub.BLL.Tasks.WebQueries.DTOs;
using Amanah.Posthub.BLL.Tasks.WebServices;
using Amanah.Posthub.BLL.Tasks.WebServices.DTOs;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.GenericListToExcel;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TasksController : Controller
    {
        private readonly IUserCommonQuery _userCommonQuery;
        private readonly ICurrentUser _currentUser;
        private readonly ITaskQuery _taskQuery;
        private readonly ITaskService _taskService;

        public TasksController(
            IUserCommonQuery userCommonQuery,
            ICurrentUser currentUser,
            ITaskQuery taskQuery,
            ITaskService taskService)
        {
            _userCommonQuery = userCommonQuery;
            _currentUser = currentUser;
            _taskQuery = taskQuery;
            _taskService = taskService;
        }

        [HttpGet("GetAll")]
        public async Task<ActionResult> GetAllAsync()
        {
            return Ok(await _taskQuery.GetAllAsync());
        }

        [HttpPost("GetAllByPagination")]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] TaskFilterDTO taskFilter)
        {
            return Ok(await _taskQuery.GetByPaginationAsync(taskFilter));
        }

        [HttpPost("GetByPagination")]
        public async Task<ActionResult> GetByPaginationAsync([FromBody] TaskFilterDTO taskFilter)
        {
            if (_currentUser.IsManager())
            {
                taskFilter.TeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (taskFilter.TeamIds == null || !taskFilter.TeamIds.Any())
                {
                    return Ok();
                }
                if (taskFilter.BranchesIds == null || !taskFilter.BranchesIds.Any())
                {
                    taskFilter.BranchesIds = await _userCommonQuery.GetManagerBranchesAsync();
                    taskFilter.GetCustomerTasks = true;
                }
            }
            return Ok(await _taskQuery.GetByPaginationAsync(taskFilter));
        }

        [HttpGet("Details/{id}")]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            return Ok(await _taskQuery.GetAsync(id));
        }

        [HttpPost("GetDriverTimeLine")]
        public async Task<ActionResult> GetDriverTimeLineAsync([FromBody] TaskDriverFilterDTO taskDriverFilter)
        {
            return Ok(await _taskQuery.GetDriverTimeLineAsync(taskDriverFilter));
        }

        [HttpPost("GetTasksReport")]
        public async Task<ActionResult> GetTasksReportAsync([FromBody] TaskReportFilterDTO taskReportFilter)
        {
            if (_currentUser.IsManager())
            {
                if (taskReportFilter.BranchIds == null || !taskReportFilter.BranchIds.Any())
                {
                    taskReportFilter.BranchIds = await _userCommonQuery.GetManagerBranchesAsync();
                    taskReportFilter.GetCustomerTasks = true;
                }
            }
            return Ok(await _taskQuery.GetTasksReportAsync(taskReportFilter));
        }

        [HttpGet("GetTaskStatusCountAsync")]
        public async Task<ActionResult> GetTaskStatusCountAsync(DateTime taskDate)
        {
            return Ok(new
            {
                IsSuccessful = true,
                data = await _taskQuery.GetTaskStatusCountAsync(taskDate)
            });
        }


        [HttpPost("Create"), DisableRequestSizeLimit]
        [Authorize(TenantPermissions.Task.CreateTask)]
        public async Task<ActionResult> CreateAsync([FromForm] CreateSubTaskInputDTO createSubTaskInput)
        {
            var subTaskId = await _taskService.CreateAsync(createSubTaskInput);
            if (subTaskId.HasValue)
            {
                return Ok(subTaskId.Value);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("Update"), DisableRequestSizeLimit]
        [Authorize(TenantPermissions.Task.UpdateTask)]
        public async Task<ActionResult> UpdateAsync([FromForm] UpdateSubTaskInputDTO updateSubTaskInput)
        {
            var subTaskId = await _taskService.UpdateAsync(updateSubTaskInput);
            if (subTaskId.HasValue)
            {
                return Ok(subTaskId.Value);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete("Delete/{id}")]
        [Authorize(TenantPermissions.Task.DeleteTask)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _taskService.DeleteAsync(id);
            return Ok(true);
        }

        [HttpPut("ChangeStatus")]
        [Authorize(TenantPermissions.Task.ChangeTaskStatus)]
        public async Task<ActionResult> ChangeStatusAsync([FromBody] ChangeTaskStatusInputDTO taskStatusInputDTO)
        {
            await _taskService.ChangeStatusAsync(taskStatusInputDTO);
            return Ok();
        }

        [HttpPost("ReasignDriverTasks")]
        public async Task<ActionResult> ReasignDriverTasksAsync([FromBody] ReassignDriverToSubTaskInputDTO reassignDriverToSubTask)
        {
            await _taskService.ReassignDriverAsync(reassignDriverToSubTask);
            return Ok();
        }

        [HttpPost("ExportWithoutProgressToExcel")]
        public async Task<ActionResult> ExportWithoutProgressToExcelAsync([FromBody] TaskReportFilterDTO filter)
        {
            var data = await _taskQuery.ExportTasksWithoutProgressToExcelAsync(filter);
            if (data == null || !data.Any())
            {
                return Ok();
            }

            var fileName = "TaskReport.xlsx";
            string filePath = Path.Combine("ImportTemplates", fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

        [HttpPost("ExportWithProgressToExcel")]
        public async Task<ActionResult> ExportWithProgressToExcelAsync([FromBody] TaskReportFilterDTO filter)
        {
            var data = await _taskQuery.ExportTasksWithProgressToExcelAsync(filter);
            if (data == null || !data.Any())
            {
                return Ok();
            }

            var fileName = "TaskReportwithprogress.xlsx";
            string filePath = Path.Combine("ImportTemplates", fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

        [HttpGet("ExportTaskToExcel")]
        public async Task<ActionResult> ExportTaskToExcelAsync(int id, string timeZone)
        {
            var data = await _taskQuery.ExportTaskToExcelAsync(id, timeZone);

            var fileName = "Tasks.xlsx";
            string filePath = Path.Combine("ImportTemplates", fileName);
            var fileexcel = ListToExcelHelper.WriteObjectsToExcel(data, filePath);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
            return File(fileBytes, "application/force-download", fileName);
        }

    }
}