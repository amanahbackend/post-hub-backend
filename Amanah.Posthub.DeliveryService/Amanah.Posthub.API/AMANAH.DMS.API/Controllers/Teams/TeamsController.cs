﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Teams.Queries;
using Amanah.Posthub.BLL.Teams.Queries.DTOs;
using Amanah.Posthub.BLL.Teams.Services;
using Amanah.Posthub.BLL.Teams.Services.DTOs;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TeamsController : Controller
    {
        private readonly IUserCommonQuery _userCommonQuery;
        private readonly ITeamQuery _teamQuery;
        private readonly ITeamService _teamService;
        private readonly ICurrentUser _currentUser;

        public TeamsController(
            IUserCommonQuery userCommonQuery,
            ITeamQuery teamQuery,
            ITeamService teamService,
            ICurrentUser currentUser)
        {
            _userCommonQuery = userCommonQuery;
            _teamQuery = teamQuery;
            _teamService = teamService;
            _currentUser = currentUser;
        }


        [HttpGet("GetAll")]
        // [Authorize(TenantPermissions.Team.ReadTeam)]
        public async Task<ActionResult> GetAllAsync()
        {
            List<int> managerTeamIds = null;
            if (_currentUser.IsManager())
            {
                managerTeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
            }
            return Ok(await _teamQuery.GetAllAsync(managerTeamIds));
        }

        [HttpGet("GetByKeyword")]
        public async Task<ActionResult> GetByKeywordAsync(string searchKey)
        {
            return Ok(await _teamQuery.GetByKeywordAsync(searchKey));
        }

        [HttpPost("GetAllByPagination")]
        [Authorize(TenantPermissions.Team.ReadMyTeam)]
        public async Task<ActionResult> GetAllByPaginationAsync([FromBody] TeamFilterDTO filter)
        {
            List<int> managerTeamIds = null;
            if (_currentUser.IsManager() && !_currentUser.HasPermission(TenantPermissions.Team.ReadTeam))
            {
                managerTeamIds = await _userCommonQuery.GetManagerTeamsAsync();
                if (managerTeamIds == null || !managerTeamIds.Any())
                {
                    return Ok();
                }
            }
            return Ok(await _teamQuery.GetAllByPaginationAsync(filter, managerTeamIds));
        }

        [HttpGet("Details/{id}")]
        [Authorize(TenantPermissions.Team.ReadTeam)]
        public async Task<ActionResult> DetailsAsync(int id)
        {
            return Ok(await _teamQuery.GetAsync(id));
        }


        [HttpPost("Create")]
        [Authorize(TenantPermissions.Team.CreateTeam)]
        public async Task<ActionResult> CreateAsync([FromBody] CreateTeamInputDTO team)
        {
            return Ok(await _teamService.CreateAsync(team));
        }


        [HttpPut]
        [Authorize(TenantPermissions.Team.UpdateTeam)]
        public async Task<ActionResult> UpdateAsync([FromBody] UpdateTeamInputDTO team)
        {
            return Ok(await _teamService.UpdateAsync(team));
        }


        [HttpDelete("Delete/{id}")]
        [Authorize(TenantPermissions.Team.DeleteTeam)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            var (IsSuccess, Error) = await _teamService.DeleteAsync(id);
            if (IsSuccess)
            {
                return Ok(true);
            }
            return BadRequest(Error);
        }

    }
}