﻿using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers.TenantRegistrations
{
    [Route("api/[controller]")]
    [ApiController]
    public class TanentRegistrationRequestLookupsController : ControllerBase
    {
        private readonly ILocalizer _localizer;

        public TanentRegistrationRequestLookupsController(ILocalizer localizer)
        {
            _localizer = localizer;
        }

        [HttpGet("GetAllBusinessType")]
        public ActionResult GetAllBusinessType()
        {
            var businessTypes = Enum.GetValues(typeof(BusinessType))
                 .Cast<BusinessType>()
                 .Select(d =>
                 new
                 {
                     Id = (int)d,
                     Name = _localizer[d.ToString()]
                 })
                 .ToList();
            return Ok(businessTypes);
        }
    }
}
