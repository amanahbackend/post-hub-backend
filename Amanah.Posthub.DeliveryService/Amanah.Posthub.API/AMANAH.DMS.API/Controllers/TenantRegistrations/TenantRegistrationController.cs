﻿using Amanah.Posthub.BLL.TenantRegistrations.Services;
using Amanah.Posthub.BLL.TenantRegistrations.Services.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TenantRegistrationController : Controller
    {
        private readonly ITenantRegistrationRequestService _tenantRegistrationRequestService;

        public TenantRegistrationController(ITenantRegistrationRequestService tenantRegistrationRequestService)
        {
            _tenantRegistrationRequestService = tenantRegistrationRequestService;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> RegisterAsync([FromBody] CreateTenantRegistrationDTO createTenantRegistrationDTO)
        {
            await _tenantRegistrationRequestService.AddAsync(createTenantRegistrationDTO);
            return Ok();
        }

    }
}
