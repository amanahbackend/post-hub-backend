﻿using Amanah.Posthub.BLL.TenantRegistrations;
using Amanah.Posthub.BLL.TenantRegistrations.Queries;
using Amanah.Posthub.BLL.TenantRegistrations.Queries.DTOs;
using Amanah.Posthub.BLL.TenantRegistrations.Services;
using Amanah.Posthub.BLL.TenantRegistrations.Services.DTO;
using Amanah.Posthub.BLL.UserManagement.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Threading.Tasks;
using ApproveTenantRegistrationDTO = Amanah.Posthub.BLL.TenantRegistrations.Services.DTO.ApproveTenantRegistrationDTO;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TenantRegistrationRequestController : Controller
    {
        private readonly ITenantRegistrationRequestService _tenantRegistrationRequestService;
        private readonly ITenantRegistrationRequestQuery _tenantRegistrationRequestQuery;

        public TenantRegistrationRequestController(
           ITenantRegistrationRequestQuery tenantRegistrationRequestQuery,
           ITenantRegistrationRequestService tenantRegistrationRequestService)
        {
            _tenantRegistrationRequestService = tenantRegistrationRequestService;
            _tenantRegistrationRequestQuery = tenantRegistrationRequestQuery;
        }

        [HttpGet("GetAllByPagination")]
        [Authorize(Policy = PlatformPermissions.Tenants.ReadRequestTenant)]
        public async Task<ActionResult> GetAllByPaginationAsync(
            [FromQuery] TenantRegistrationPagginatedFilterInputDTO pagingparametermodel)
        {
            return Ok(await _tenantRegistrationRequestQuery.GetAllByPaginationAsync(pagingparametermodel));
        }

        [HttpGet("GetById")]
        [Authorize(Policy = PlatformPermissions.Tenants.ReadRequestTenant)]
        public async Task<ActionResult> GetByIdAsync(int id)
        {
            var result = await _tenantRegistrationRequestQuery.GetByIdAsync(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [HttpGet("GetHistoricalAuditors")]
        [Authorize(Policy = PlatformPermissions.Tenants.ReadRequestTenant)]
        public async Task<ActionResult> GetHistoricalAuditorsAsync([FromQuery] PaginatedItemsViewModel paginatedItems)
        {
            return Ok(await _tenantRegistrationRequestQuery.GetHistoricalAuditorsAsync(paginatedItems));
        }

        [HttpPost("Update")]
        [Authorize(Policy = PlatformPermissions.Tenants.EditRequestTenant)]
        public async Task<IActionResult> UpdateAsync(
            [FromForm] UpdateTenantRegistrationDetailsDTO tenantRegistrationDTO)
        {
            await _tenantRegistrationRequestService.UpdateAsync(tenantRegistrationDTO);
            return Ok();
        }

        [HttpPost("Approve")]
        [Authorize(Policy = PlatformPermissions.Tenants.ApproveRequestTenant)]
        public async Task<IActionResult> ApproveAsync([FromBody] ApproveTenantRegistrationDTO approveViewModel)
        {
            await _tenantRegistrationRequestService.ApproveAsync(approveViewModel);

            return Ok();
        }

        [HttpPost("Reject")]
        [Authorize(Policy = PlatformPermissions.Tenants.RejectRequestTenant)]
        public async Task<IActionResult> RejectAsync(RejectTenantRegistrationDTO rejectTenant)
        {
            await _tenantRegistrationRequestService.RejectAsync(rejectTenant.TenantId, rejectTenant.Reason);

            return Ok();
        }
    }
}
