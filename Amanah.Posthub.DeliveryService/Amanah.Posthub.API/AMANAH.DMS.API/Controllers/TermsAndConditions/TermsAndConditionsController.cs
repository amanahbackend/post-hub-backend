﻿using Amanah.Posthub.BLL;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.UserManagement.Permissions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class TermsAndConditionsController : Controller
    {
        private readonly ITermsAndConditionsManager _termsAndConditionsManager;

        public TermsAndConditionsController(ITermsAndConditionsManager termsAndConditionsManager)
        {
            _termsAndConditionsManager = termsAndConditionsManager;
        }

        [HttpPut("UpdateDriverTermsAndConditions")]
        [Authorize(Policy = PlatformPermissions.Agent.UpdateAgentTermsAndConditions)]
        public async Task<ActionResult> UpdateDriverTermsAndConditionsAsync([FromBody] TermsAndConditionsViewModel model)
        {
            var result = await _termsAndConditionsManager.UpdateDriverTermsAndConditionsAsync(model);
            return Ok(result);
        }

        [HttpGet("GetDriverTermsAndConditions")]
        [Authorize(Policy = PlatformPermissions.Agent.UpdateAgentTermsAndConditions)]
        public async Task<ActionResult> GetDriverTermsAndConditionsAsync()
        {
            var driverTerms = await _termsAndConditionsManager.GetDriverTermsAndConditionsAsync();
            return Ok(driverTerms);
        }

        [AllowAnonymous]
        [HttpGet("DowenloadDriverTermsAndConditions")]
        public async Task<ActionResult> DowenloadDriverTermsAndConditionsAsync()
        {
            string fileName = "Driver Terms And Conditons.pdf";
            var driverTermsPDFBytes = await _termsAndConditionsManager.DowenloadDriverTermsAndConditionsAsPDFAsync();
            return File(driverTermsPDFBytes, "application/pdf", fileName);
        }

        [AllowAnonymous]
        [HttpGet("GetDriverTermsAndConditionsContent")]
        public async Task<ActionResult> GetDriverTermsAndConditionsContentAsync()
        {
            var driverTerms = await _termsAndConditionsManager.GetDriverTermsAndConditionsContentAsync();
            return Ok(driverTerms);
        }

    }
}
