﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        protected readonly IApplicationUserManager _userManager;
        protected readonly ISettingsManager _settingsManager;

        public AccountController(IApplicationUserManager userManager,
            ISettingsManager settingsManager)
        {
            _userManager = userManager;
            _settingsManager = settingsManager;
        }

        [HttpGet("ResendResetPasswordEmail")]
        public async Task<IActionResult> ResendResetPasswordEmailAsync(string email)
        {
            await _userManager.SendResetPasswordEmailAsync(email);
            return Ok();
        }

        [HttpPost("Login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginViewModel model)
        {
            var res = await _userManager.LoginAsync(model);
            if (res.Errors.Any())
                return BadRequest(res.Errors.FirstOrDefault()?.Description);

            return Ok(res);
        }

        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPasswordAsync([FromBody] ForgotPasswordRequest model)
        {
            var res = await _userManager.ForgotPassword(model.Email);
            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }

        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordRequest model)
        {
            var res = await _userManager.ResetPasswordAsync(model.Email, model.Token, model.NewPassword);
            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }

        [HttpPost("CheckResetToken")]
        public async Task<IActionResult> CheckResetTokenAsync([FromBody] CheckResetTokenRequest model)
        {
            var res = await _userManager.CheckResetToken(model.Email, model.Token);

            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }
    }
}