﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Amanah.Posthub.BLL.UserManagement.Services;
using Amanah.Posthub.BLL.UserManagement.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/AgentAccessControl")]

    public class AgentAccessControlController : Controller
    {
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IAccessControlQuery _agentAccessControlQuery;
        private readonly IAccessControlService _agentAccessControlService;


        public AgentAccessControlController(
            ILocalizer localizer,
            ICurrentUser currentUser,
            IAccessControlQuery agentAccessControlQuery,
            IAccessControlService agentAccessControlService)
        {
            _localizer = localizer;
            _currentUser = currentUser;
            _agentAccessControlQuery = agentAccessControlQuery;
            _agentAccessControlService = agentAccessControlService;
        }

        [HttpGet("Get")]
        [Authorize(TenantPermissions.Roles.ReadAllRoles)]
        public async Task<IActionResult> GetAsync(string roleName)
        {
            return Ok(await _agentAccessControlQuery.GetAsync(roleName));
        }

        [HttpGet("GetAll")]
        [Authorize(TenantPermissions.Roles.ReadAllRoles)]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _agentAccessControlQuery.GetAllAsync(RoleType.Agent));
        }

        [HttpGet("GetAllPermissions")]
        [Authorize(TenantPermissions.Roles.ReadAllRoles)]
        public async Task<IActionResult> GetAllPermissionsAsync()
        {
            return Ok(await _agentAccessControlQuery.GetAllDriverPermissionsAsync());
        }

        [HttpPost("Create")]
        [Authorize(TenantPermissions.Roles.AddRole)]
        public async Task<IActionResult> PostAsync([FromBody] CreateAccessControlInputDTO model)
        {
            ValidateAgent();
            if (await _agentAccessControlQuery.IsRoleExistAsync(model.RoleName.Trim()))
            {
                return BadRequest(_localizer[Keys.Validation.RoleAlreadyExists]);
            }
            else
            {
                model.Type = RoleType.Agent;
                await _agentAccessControlService.CreateAsync(model, RoleType.Agent);

                return Ok();
            }
        }

        [HttpPut("Update")]
        [Authorize(TenantPermissions.Roles.UpdateRole)]
        public async Task<IActionResult> PutAsync([FromBody] UpdateAccessControlInputDTO model)
        {
            ValidateAgent();
            model.Type = RoleType.Agent;
            await _agentAccessControlService.UpdateAsync(model);

            return Ok();
        }


        [HttpDelete("Delete/{roleName}")]
        [Authorize(TenantPermissions.Roles.DeleteRole)]
        public async Task<IActionResult> DeleteAsync([FromRoute] string roleName)
        {
            await _agentAccessControlService.DeleteAsync(roleName);

            return Ok();
        }

        private bool ValidateAgent()
        {
            if (_currentUser.UserPermissions.Contains(TenantPermissions.Agent.CreateAgent) ||
                _currentUser.UserPermissions.Contains(TenantPermissions.Agent.UpdateAgent))
            {
                return true;
            }
            throw new UnauthorizedAccessException(Keys.Validation.UnAuthorizedAccess);
        }
    }
}