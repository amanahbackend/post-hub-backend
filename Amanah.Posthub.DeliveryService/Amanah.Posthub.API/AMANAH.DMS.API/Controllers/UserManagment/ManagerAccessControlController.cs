﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Amanah.Posthub.BLL.UserManagement.Services;
using Amanah.Posthub.BLL.UserManagement.Services.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/ManagerAccessControl")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ManagerAccessControlController : Controller
    {
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IAccessControlQuery _accessControlQuery;
        private readonly IAccessControlService _accessControlService;

        public ManagerAccessControlController(
            ILocalizer localizer,
            ICurrentUser currentUser,
            IAccessControlQuery accessControlQuery,
            IAccessControlService accessControlService)
        {
            _localizer = localizer;
            _currentUser = currentUser;
            _accessControlQuery = accessControlQuery;
            _accessControlService = accessControlService;
        }

        [HttpGet("Get")]
        [Authorize(TenantPermissions.Roles.ReadAllRoles)]
        public async Task<IActionResult> GetAsync(string roleName)
        {
            return Ok(await _accessControlQuery.GetAsync(roleName));
        }

        [AllowAnonymous]
        [HttpGet("GetAll")]
        //[Authorize(TenantPermissions.Roles.ReadAllRoles)]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _accessControlQuery.GetAllAsync(RoleType.Manager));
        }

        [HttpGet("GetAllPermissions")]
        [Authorize(TenantPermissions.Roles.ReadAllRoles)]
        public async Task<IActionResult> GetAllPermissionsAsync()
        {
            return Ok(await _accessControlQuery.GetAllManagerPermissionsAsync());
        }


        [HttpPost("Create")]
        [Authorize(TenantPermissions.Roles.AddRole)]
        public async Task<IActionResult> PostAsync([FromBody] CreateAccessControlInputDTO model)
        {
            ValidateManager();
            if (await _accessControlQuery.IsRoleExistAsync(model.RoleName.Trim()))
            {
                return BadRequest(_localizer[Keys.Validation.RoleAlreadyExists]);
            }
            else
            {
                model.Type = RoleType.Manager;
                await _accessControlService.CreateAsync(model, RoleType.Manager);

                return Ok(true);
            }
        }

        [HttpPut("Update")]
        [Authorize(TenantPermissions.Roles.UpdateRole)]
        public async Task<IActionResult> PutAsync([FromBody] UpdateAccessControlInputDTO model)
        {
            ValidateManager();
            model.Type = RoleType.Manager;
            await _accessControlService.UpdateAsync(model);
            return Ok();
        }

        [HttpDelete("Delete/{roleName}")]
        [Authorize(TenantPermissions.Roles.DeleteRole)]
        public async Task<IActionResult> DeleteAsync([FromRoute] string roleName)
        {
            await _accessControlService.DeleteAsync(roleName);
            return Ok();
        }

        private bool ValidateManager()
        {
            if (_currentUser.UserPermissions.Contains(TenantPermissions.Managers.AddManager) ||
                _currentUser.UserPermissions.Contains(TenantPermissions.Managers.UpdateAllManager))
            {
                return true;
            }
            throw new UnauthorizedAccessException(Keys.Validation.UnAuthorizedAccess);
        }
    }
}