﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Amanah.Posthub.BLL.UserManagement.Services;
using Amanah.Posthub.BLL.UserManagement.Services.DTOs;
using Amanah.Posthub.BLL.ViewModel;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Admin;
using Amanah.Posthub.BLL.ViewModels.BaseResult;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.Settings;
using Amanah.Posthub.ViewModel;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/User")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class UserController : Controller
    {
        private readonly IUserQuery _userQuery;
        private readonly IUserService _userService;

        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IApplicationUserManager _appUserManager;
        private readonly AppSettings _appSettings;
        private readonly IUserDeviceManager _userDeviceManager;
        private readonly IAdminManager _adminManager;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;

        public UserController(
            IUserQuery userQuery,
            IUserService userService,

            IMapper mapper,
            UserManager<ApplicationUser> userManager,
            IApplicationUserManager appuserManager,
            IOptions<AppSettings> appSettings,
            IUserDeviceManager userDeviceManager,
            IAdminManager adminManager,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            _userQuery = userQuery;
            _userService = userService;
            _mapper = mapper;
            _userManager = userManager;
            _appUserManager = appuserManager;
            _appSettings = appSettings.Value;
            _userDeviceManager = userDeviceManager;
            _localizer = localizer;
            _adminManager = adminManager;
            _currentUser = currentUser;
        }

        #region DefaultCrudOperation
        #region GetApi
        [Route("Get")]
        [HttpGet]
        public async Task<IActionResult> Get(ApplicationUserViewModel model)
        {
            ApplicationUserViewModel result = new ApplicationUserViewModel();
            ApplicationUser entityResult = new ApplicationUser();
            entityResult = _mapper.Map<ApplicationUserViewModel, ApplicationUser>(result);
            entityResult = await _appUserManager.GetAsync(entityResult);
            result = _mapper.Map<ApplicationUser, ApplicationUserViewModel>(entityResult);
            return Ok(result);
        }

        [Route("IsAllowToDeleteAdmin")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> IsAllowToDeleteAdmin()
        {
            if (await _appUserManager.IsAllowToDeleteAdmin())
            {
                return Ok(true);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteAdmin]);
            }
        }

        [Route("GetAll")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetAll()
        {
            var entityResult = await _appUserManager.GetAll();
            var result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetByUserIds")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public List<ApplicationUserViewModel> GetByUserIds([FromBody] List<string> userIds)
        {
            var entityResult = _appUserManager.GetByUserIds(userIds);
            var result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }

        [Route("GetTechnicansExceptUserIds")]
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<List<ApplicationUserViewModel>> GetTechnicansExceptUserIds([FromBody] List<string> userIds)
        {
            var entityResult = await _appUserManager.GetByRole(_appSettings.TechnicanRole, userIds);
            var result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }
        [Route("GetAllExceptUserIds")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public List<ApplicationUserViewModel> GetByRoleExcept([FromBody] List<string> userIds)
        {
            List<ApplicationUser> entityResult = _appUserManager.GetAllExcept(userIds);
            List<ApplicationUserViewModel> result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return result;
        }
        [Route("GetTechnicans")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetTechnicans()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await _appUserManager.GetByRole(_appSettings.TechnicanRole);
            result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetDispatchers")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetDispatchers()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await _appUserManager.GetByRole(_appSettings.DispatcherRole);
            result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }

        [Route("GetDispatcherSupervisor")]
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> GetDispatcherSupervisor()
        {
            List<ApplicationUserViewModel> result = new List<ApplicationUserViewModel>();
            List<ApplicationUser> entityResult = new List<ApplicationUser>();
            entityResult = await _appUserManager.GetByRole(_appSettings.SupervisorDispatcherRole);
            result = _mapper.Map<List<ApplicationUser>, List<ApplicationUserViewModel>>(entityResult);
            return Ok(result);
        }
        #endregion

        [HttpGet]
        [Route("UserRoles/{username}")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<ApplicationUserViewModel> UserRoles([FromRoute] string username)
        {
            ApplicationUserViewModel result = null;
            //if (Request != null && authHeader == null)
            //{
            //    //authHeader = Helper.GetValueFromRequestHeader(Request);
            //}
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrWhiteSpace(username))
            {
                var user = await _appUserManager.GetByEmailAsync(username);
                if (user != null)
                {
                    user.RoleNames = (await _appUserManager.GetRolesAsync(username)).ToList();
                    result = _mapper.Map<ApplicationUser, ApplicationUserViewModel>(user);
                    return result;
                }

            }
            return result;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Update")]
        [Authorize(AuthenticationSchemes = "Bearer")]

        public async Task<IActionResult> Update([FromBody] EditApplicationUserViewModel model)
        {
            if (await _appUserManager.IsUserNameExistAsync(model.UserName, model.Id))
            {
                return BadRequest(_localizer.Format(Keys.Validation.UserNameAlreadyExists, model.UserName));
            }
            bool result = false;
            if (ModelState.IsValid)
            {
                var entityResult = _mapper.Map<EditApplicationUserViewModel, ApplicationUser>(model);
                result = await _appUserManager.UpdateUserAsync(entityResult);
            }
            return Ok(result);
        }

        #region Update User Password

        //private async Task<bool> UpdateUserPassword(string userId, string newPassword, string modifiedByUserId)
        //{
        //    ApplicationUser applicationUser = await _appUserManager.GetByUserIdAsync(userId);

        //    if (applicationUser == null)
        //    {
        //        return false;
        //    }

        //    applicationUser.PasswordHash = _appUserManager.HashPassword(applicationUser, newPassword);
        //    applicationUser.UpdatedBy_Id = modifiedByUserId;
        //    applicationUser.UpdatedDate = DateTime.UtcNow;

        //    IdentityResult identityResult = await _appUserManager.UpdateAsync(applicationUser);

        //    return identityResult.Succeeded;
        //}

        #endregion Update User Password

        #region DeleteApi

        [Route("Delete/{username}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute] string username)
        {
            var entity = await _appUserManager.GetByEmailAsync(username);

            entity.UserName = entity.UserName + "_" + "Deleted_" + DateTime.UtcNow.ToString("yy_MM_dd_mm_ss");
            var result = await _appUserManager.SoftDeleteAsync(entity);
            return Ok(result);
        }

        [Route("ToggleDelete/{username}")]
        [HttpDelete]
        public async Task<IActionResult> Lock([FromRoute] string username)
        {
            var entity = await _appUserManager.GetByEmailAsync(username);
            entity.IsLocked = !entity.IsLocked;
            var identity = await _appUserManager.UpdateAsync(entity);
            return Ok(identity.Succeeded);
        }
        #endregion
        #endregion

        [HttpGet]
        [Route("UsernameExists/{username}")]
        [Authorize(AuthenticationSchemes = "Bearer")]


        public async Task<IActionResult> UsernameExists([FromRoute] string username)
        {
            bool result = await _appUserManager.IsUserNameExistAsync(username);
            return Ok(result);
        }

        [HttpGet]
        [Route("EmailExists/{email}")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> EmailExists([FromRoute] string email)
        {
            bool result = await _appUserManager.IsEmailExistAsync(email);
            return Ok(result);
        }

        [HttpPost]
        [Route("AssignUserToRole")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> AssignUserToRole([FromBody] AssignRoleViewModel assignRoleViewModel)
        {

            bool result = await _appUserManager.AddUserToRoleAsync(assignRoleViewModel.UserName, assignRoleViewModel.RoleName);
            return Ok(result);
        }


        [HttpDelete, Route("DeleteDevice")]
        public IActionResult DeleteDevice([FromQuery] string token)
        {
            var result = _userDeviceManager.DeleteDevice(token);
            return Ok(result);
        }


        [HttpPost]
        [Route("DeleteAccount")]
        public async Task<IActionResult> DeleteAccount([FromBody] AccountToDeleteViewModel accountToDeleteViewModel)
        {
            var user = await _appUserManager.GetByUserIdAsync(_currentUser.Id);
            if (user == null)
            {
                return BadRequest(_localizer[Keys.Validation.UserNotFound]);
            }
            // Check Password 
            var passResult = _userManager.PasswordHasher.VerifyHashedPassword(user, user.PasswordHash, accountToDeleteViewModel.password);
            if (passResult != PasswordVerificationResult.Success)
            {
                return BadRequest(_localizer[Keys.Validation.InCorrectPassword]);
            }
            // Update User
            var result = await _appUserManager.SoftDeleteAsync(user);
            if (!result)
            {
                return BadRequest(_localizer[Keys.Validation.CanNotDeleteAccount]);
            }
            var done = await _adminManager.SetDeactivationReason(user.Id, accountToDeleteViewModel.deactivationReason);
            if (!done)
            {
                return BadRequest(_localizer[Keys.Validation.CanNotSetReason]);
            }
            var res = new EntityResult<string>
            {
                result = _localizer[Keys.Messages.AccountDeletedSuccessfully]
            };
            return Ok(res);
        }




        #region --- Refactor User Profile ---

        [HttpGet("GetUserAdminInfo")]
        public async Task<IActionResult> GetUserProfileAsync()
        {
            var result = await _userQuery.GetUserProfileAsync();
            if (result != null)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(_localizer[Keys.Validation.UserNotFound]);
            }
        }

        [HttpGet("GetDefautBranch")]
        public async Task<IActionResult> GetUserDefautBranchAsync()
        {
            return Ok(await _userQuery.GetUserDefaultBranchAsync());
        }

        [HttpPost("UpdateProfile")]
        public async Task<IActionResult> UpdateMyProfileAsync([FromForm] UpdateProfileInputDTO updateProfileInput)
        {
            await _userService.UpdateMyProfileAsync(updateProfileInput);
            return Ok(new EntityResult<UpdateProfileInputDTO>
            {
                result = updateProfileInput
            });
        }

        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangeMyPasswordAsync([FromBody] ChangePasswordInputDTO changePasswordInput)
        {
            var result = await _userService.ChangeMyPasswordAsync(changePasswordInput);
            if (result)
            {
                return Ok(new EntityResult<string>
                {
                    result = _localizer[Keys.Messages.PasswordChangedSuccessfully]
                });
            }

            return BadRequest(_localizer[Keys.Validation.InCorrectPassword]);
        }

        [HttpPost("UpdateDefaultBranch")]
        public async Task<IActionResult> UpdateDefaultBranchAsync([FromBody] UpdateDefaultBranchInputDTO updateDefaultBranchInput)
        {
            var result = await _userService.UpdateTenantDefaultBranchAsync(updateDefaultBranchInput.DefaultBranchId);
            if (result)
            {
                return Ok(new EntityResult<string>
                {
                    result = _localizer[Keys.Messages.DefaultBranchUpdatedsuccessfully]
                });
            }
            return BadRequest(_localizer[Keys.Validation.CouldNotUpdateDefaultBranch]);
        }

        [HttpPost("UpdateLanguage")]
        public async Task<IActionResult> UpdateLanguageAsync([FromBody] UpdateLanguageInputDTO updateLanguage)
        {
            var result = await _userService.UpdateTenantLanguageAsync(updateLanguage.DashBoardLanguage);
            if (result)
            {
                return Ok(new EntityResult<string>
                {
                    result = _localizer[Keys.Messages.LanguageUpdateSuccessfully]
                });
            }
            return BadRequest(_localizer[Keys.Validation.CanNotUpdateLanguage]);
        }

        #endregion


    }
}