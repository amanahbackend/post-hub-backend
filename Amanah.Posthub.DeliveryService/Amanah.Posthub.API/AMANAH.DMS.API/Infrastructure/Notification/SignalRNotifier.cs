﻿using Amanah.Posthub.API.SignalRHups;
using Amanah.Posthub.BASE.Services;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.Infrastructure.Notification
{
    public class SignalRNotifier : IRealTimeNotifier
    {
        private readonly IHubContext<NotificationHub> _notificationhubContext;

        public SignalRNotifier(IHubContext<NotificationHub> notificationhubContext)
        {
            _notificationhubContext = notificationhubContext;
        }

        public async Task SendAsync(string toUserId, string notificationName, string message, object payload)
        {
            await _notificationhubContext.Clients.Group(toUserId).SendAsync(notificationName, payload);
        }
    }

}
