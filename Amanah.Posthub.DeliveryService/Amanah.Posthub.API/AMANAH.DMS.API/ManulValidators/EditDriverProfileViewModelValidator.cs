﻿using Amanah.Posthub.BLL.ViewModels.Driver;
using Amanah.Posthub.VALIDATOR.Extensions;
using FluentValidation;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.API.ManualValidators
{
    public class EditDriverProfileViewModelValidator : AbstractValidator<EditDriverProfileViewModel>
    {
        public EditDriverProfileViewModelValidator(ILocalizer localizer)
        {
            RuleFor(x => x.CurrentPassword).Password(localizer);
            RuleFor(x => x.NewPassword).Password(localizer);
            RuleFor(x => x.NewPassword).NotEqual(x => x.CurrentPassword)
              .WithMessage(localizer[Keys.Validation.NewPasswordIsSameAsOld]);
            RuleFor(x => x.ConfirmPassword).Equal(x => x.NewPassword)
                .When(x => !string.IsNullOrWhiteSpace(x.NewPassword))
                .WithMessage(localizer[Keys.Validation.RetypePasswordMismatch]);
        }
    }
}
