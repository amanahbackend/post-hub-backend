﻿using System.Threading.Tasks;

namespace Amanah.Posthub.API.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
