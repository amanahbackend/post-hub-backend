﻿using System.Threading.Tasks;

namespace Amanah.Posthub.API.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
