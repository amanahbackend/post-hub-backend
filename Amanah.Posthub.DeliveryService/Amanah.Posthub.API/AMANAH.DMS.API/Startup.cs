﻿using Amanah.Posthub.API.Authorization;
using Amanah.Posthub.API.Infrastructure.Ioc;
using Amanah.Posthub.API.Infrastructure.Notification;
using Amanah.Posthub.API.Services;
using Amanah.Posthub.API.SignalRHups;
using Amanah.Posthub.BASE;
using Amanah.Posthub.BASE.Services;
using Amanah.Posthub.BLL.Addresses;
using Amanah.Posthub.BLL.Addresses.Services;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Drivers.Services;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Managers;
using Amanah.Posthub.BLL.Managers.TaskAssignment;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Notifications.Services;
using Amanah.Posthub.BLL.TasksFolder.Queries;
using Amanah.Posthub.BLL.UserManagement.Permissions;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.API;
using Amanah.Posthub.DeliverService.API.Localization;
using Amanah.Posthub.DeliverService.BLL.DriverFloor.Queries;
using Amanah.Posthub.Domain.Drivers.DomainServices;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.GeoFences.Repositories;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Domain.Notifications.Repositories;
using Amanah.Posthub.Domain.SubTasks.Repositories;
using Amanah.Posthub.Domain.TaskHistorys.Repositories;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Infrastructure.EntityFramework.Drivers.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.GeoFences.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.MainTasks.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.SubTasks.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.TaskDriverRequestss.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.TaskHistorys.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.Users.Repositories;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Infrastructure.ExternalServices.Settings;
using Amanah.Posthub.Infrastructure.Hangfire;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.Settings;
using Amanah.Posthub.SharedKernel.Exceptions;
using Amanah.Posthub.SharedKernel.Infrastructure.Authentication;
using AutoMapper;
using FluentValidation.AspNetCore;
using IdentityServer4.Configuration;
using MapsUtilities.MapsManager;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.FileProviders.Physical;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.FeatureManagement;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NLog;
using NSwag;
using NSwag.Generation.Processors.Security;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;
using Wkhtmltopdf.NetCore;

namespace Amanah.Posthub.API
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                    "CorsPolicy",
                    builder => builder
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        .SetIsOriginAllowed(host => true));
            });

            // Add framework services.
            services.AddEntityFramework(_configuration, _environment);

            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddErrorDescriber<LocalizedIdentityErrorDescriber>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+ئءؤرلاىةوزظطكمنتالبيسشضصثقفغعهخحجدذألألإإلآآ";
            });
            //services.AddDataProtection()
            //    .PersistKeysToFileSystem(new DirectoryInfo(_configuration["AppSettings:keydir"]))
            //    .SetApplicationName(_configuration["AppSettings:keyname"]);
            services.Configure<DataProtectionTokenProviderOptions>(option =>
                option.TokenLifespan = TimeSpan.FromDays(1));

            ConfigureAuthService(services);

            services.Configure<AppSettings>(_configuration);
            services.Configure<EmailSettings>(_configuration.GetSection("EmailSettings"));

            services.AddBackgroundJobs(_configuration);

            services.AddMvc()
                .AddNewtonsoftJson(options =>
                    options.SerializerSettings.Converters.Add(new StringEnumConverter()))
                .AddFluentValidation(configuration =>
                {
                    configuration.RegisterValidatorsFromAssemblyContaining<ManagerManager>();
                    configuration.RegisterValidatorsFromAssemblyContaining<Startup>();
                })
                .AddDataAnnotationsLocalization(options => options.DataAnnotationLocalizerProvider =
                    (type, factory) => factory.Create(typeof(Translations)));


            //var migrationsAssembly = "Amanah.Posthub.Infrastructure";
            string IdentityServerDBConnectionString = _configuration["IdentityServerDBConnectionString"];
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            var builder = services.AddIdentityServer(options =>
            {
                options.Events.RaiseErrorEvents = true;
                options.Events.RaiseInformationEvents = true;
                options.Events.RaiseFailureEvents = true;
                options.Events.RaiseSuccessEvents = true;

                options.UserInteraction = new UserInteractionOptions
                {
                    LogoutUrl = "/Account/Logout",
                    LoginUrl = "/Account/Login",
                    LoginReturnUrlParameter = "returnUrl"
                };
            })
                .AddAspNetIdentity<ApplicationUser>()
                .AddDeveloperSigningCredential()
                // this adds the config data from DB (clients, resources, CORS)
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = db =>
                        db.UseSqlServer(IdentityServerDBConnectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                // this adds the operational data from DB (codes, tokens, consents)
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = db =>
                        db.UseSqlServer(IdentityServerDBConnectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    // options.TokenCleanupInterval = 15; // interval in seconds. 15 seconds useful for debugging
                });


            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            services.AddAutoMapper(typeof(Startup));
            services.AddScoped(typeof(IMapsManager), typeof(MapsManager));
            services.AddScoped(typeof(ITaskAutoAssignmentFactory), typeof(TaskAutoAssignmentFactory));

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<ILoginService<ApplicationUser>, EFLoginService>();
            services.AddTransient<IRedirectService, RedirectService>();
            services.AddSingleton(typeof(IEmailSenderservice), typeof(EmailSenderService));
            services.AddSingleton(typeof(EmailSettings), typeof(EmailSettings));
            services.AddScoped(typeof(IGeoFenceManager), typeof(GeoFenceManager));
            services.AddScoped(typeof(IFirebasePushNotificationService), typeof(FirebasePushNotificationService));
            services.AddScoped(typeof(IAddressProvider), typeof(CachedPACIProvider));
            services.AddScoped(typeof(IAddressProvider), typeof(PACIAddressProvider));
            services.AddScoped(typeof(IAddressProvider), typeof(GoogleAddressProvider));
            services.AddScoped(typeof(IAddressService), typeof(AddressService));
            services.AddScoped(typeof(INotificationSenderService), typeof(NotificationSenderService));
            services.AddScoped(typeof(IBackGroundNotificationSercvice), typeof(BackGroundNotificationSercvice));
            services.AddScoped(typeof(IBackGroundNotificationJob), typeof(BackGroundNotificationJob));


            services.AddScoped(typeof(IPACIService), typeof(PACIService));
            services.AddScoped(typeof(IAreaManager), typeof(AreaManager));
            services.AddScoped(typeof(IDriverWorkingHourTrackingManager), typeof(DriverWorkingHourTrackingManager));
            services.Configure<PACISettings>(_configuration.GetSection("Address").GetSection("PACIAddressProvider").GetSection("PACIServiceSettings"));
            services.AddScoped(typeof(IUploadFormFileService), typeof(UploadFormFileService));

            services.AddScoped(typeof(IRealTimeNotifier), typeof(SignalRNotifier));
            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));
            services.AddScoped(typeof(IDriverFloorQuery), typeof(DriverFloorQuery));

            services.AddSwaggerGen(config =>
            {
                config.DescribeAllEnumsAsStrings();
                //config.OperationFilter<SupportMultipleEnumValues>();
            });
            services.AddSwaggerDocument(config =>
               {
                   config.PostProcess = document =>
                   {
                       document.Info.Version = "v1";
                       document.Info.Title = "Posthub API";
                       document.Info.Description = "";
                       document.Info.TermsOfService = "None";
                   };

                   config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                   config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                       new OpenApiSecurityScheme()
                       {
                           Type = OpenApiSecuritySchemeType.ApiKey,
                           Name = "Authorization",
                           In = OpenApiSecurityApiKeyLocation.Header,
                           Description = "Copy this into the value field: Bearer {token}"
                       }
                   );
               });

            services.AddSignalR();
            services.AddHttpContextAccessor();
            services.AddCurrentUser();
            services.AddScoped<IAuthorizationHandler, PermissionAuthorizationHandler>();
            services.AddAuthorization(options =>
            {
                var allPermissions = TenantPermissions.FlatPermissions
                    .Concat(PlatformPermissions.FlatPermissions)
                    .Concat(AgentPermissions.FlatPermissions)
                    .ToArray();
                foreach (var permission in allPermissions)
                {
                    options.AddPolicy(permission, builder =>
                    {
                        builder.RequireAuthenticatedUser();
                        builder.RequireScope("profile");
                        builder.AddRequirements(new PermissionRequirement(permission));
                    });
                }
            });
            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });


            services.AddTransient<ILocalizer, Localizer>();
            services.AddWkhtmltopdf();

            services.AddFeatureManagement();

            RegisterConventions(services);
           // RegisterPollyServices(services);

            IocHelper.RegisterConventinos(services);
        }

        public void Configure(IApplicationBuilder app, IMapper mapper)
        {
            mapper.ConfigurationProvider.AssertConfigurationIsValid();
            app.UseRequestLocalization(options =>
            {
                var arabicCulture = new CultureInfo("ar");
                arabicCulture.DateTimeFormat.Calendar = new GregorianCalendar();
                var englishCulture = new CultureInfo("en");
                var supportedCultures = new[] { englishCulture, arabicCulture };
                options.DefaultRequestCulture = new RequestCulture(supportedCultures.First());
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
            app.UseCors("CorsPolicy");

            if (_environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(errorAppBuilder =>
                {
                    errorAppBuilder.Run(async context =>
                    {
                        var exception = context.Features.Get<IExceptionHandlerFeature>()?.Error;
                        var localizer = context.RequestServices.GetRequiredService<ILocalizer>();
                        var logger = context.RequestServices.GetRequiredService<ILogger<ExceptionHandlerMiddleware>>();
                        var (content, status) = exception switch
                        {
                            NotFoundException notFoundException => (notFoundException.Message, HttpStatusCode.NotFound),
                            DomainException domainException => (domainException.Message, HttpStatusCode.BadRequest),
                            UnauthorizedAccessException unauthorizedAccessException =>
                                (Keys.Validation.UnAuthorizedAccess, HttpStatusCode.Unauthorized),
                            _ => (Keys.Validation.GeneralError, HttpStatusCode.InternalServerError),
                        };
                        logger.LogCritical(exception, content);
                        if (!context.Response.HasStarted)
                        {
                            context.Response.StatusCode = (int)status;
                            var result =
                                new
                                {
                                    error = localizer[content]
                                };
                            // TODO: move to an extension method WriteJsonAsync
                            context.Response.ContentType = "application/json; charset=UTF-8";
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                            await context.Response.Body.FlushAsync();
                        }
                    });
                });
            }

            app.UseOpenApi();
            app.UseSwaggerUi3();
            app.UseRouting();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "front-end"))
            });

            var paths = new[]
            {
                "CountryFlags",
                "DriverImages",
                "TaskSignatures",
                "TasksGallary",
                "TasksImages",
                "DriverSheet",
                "FaildDriverSheet",
                "CustomerSheet",
                "FaildCustomerSheet",
                "ImportTemplates",
                "StaffImages",
                "CompanyImages",
                "BusinessCustomerLogos",
                "BusinessContactProfile"

            };

            //var fileProviders = paths.Select(path => new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), path)))
            //    .ToArray();
            foreach (var path in paths)
            {
                var fullPath = Path.Combine(Directory.GetCurrentDirectory(), path);
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(fullPath),
                    RequestPath = $"/{path}"
                });
            }
            app.UseBackgroundJobs();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<NotificationHub>("/notificationHub");
                //endpoints.MapHub<DriverHub>("/driverHub");


            });
            app.Use(async (context, next) =>
               {
                   var path = context.Request.Path.Value;
                   if (path.StartsWith("/api") || Path.HasExtension(path))
                   {
                       await next();
                       return;
                   }
                   var indexFilePath = Path.Combine(Directory.GetCurrentDirectory(), "front-end", "index.html");
                   var indexFileInfo = new FileInfo(indexFilePath);
                   if (indexFileInfo.Exists)
                   {
                       context.Response.ContentType = "text/html";
                       await context.Response.SendFileAsync(new PhysicalFileInfo(indexFileInfo));
                   }
               });
        }

        private void ConfigureAuthService(IServiceCollection services)
        {

            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var identityUrl = _configuration.GetValue<string>("IdentityUrl");

            services.AddAuthentication("Bearer")
             .AddJwtBearer("Bearer", options =>
             {
                 options.Authority = identityUrl;

                 options.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateAudience = false
                 };
             });
        }

        private void RegisterConventions(IServiceCollection services)
        {
            services.AddScoped<IDriverRepository, DriverRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();

            services.AddScoped<IUserDeviceRepository, UserDeviceRepository>();
            services.AddScoped<IDriverLoginTrackingRepository, DriverLoginTrackingRepository>();
            services.AddScoped<IGeofenceRepository, GeofenceRepository>();
            services.AddScoped<IMainTaskRepository, MainTaskRepository>();
            services.AddScoped<ISubTaskRepository, SubTaskRepository>();
            services.AddScoped<ITaskDriverRequestRepository, TaskDriverRequestRepository>();
            services.AddScoped<ITaskHistoryRepository, TaskHistoryRepository>();
            services.AddScoped<ITaskGallaryRepository, TaskGallaryRepository>();
            services.AddScoped<ISettingRepository, SettingRepository>();


            services.AddScoped<IDriverDomainService, DriverDomainService>();
            services.AddScoped<IUserDomainService, UserDomainService>();
            services.AddScoped<IRoleDomainService, RoleDomainService>();

            services.AddScoped<IDriverService, DriverService>();
            services.AddScoped<IMobileSubTaskService, MobileSubTaskService>();
            services.AddScoped<IMobileMainTaskService, MobileMainTaskService>();
            services.AddScoped<INearestAvailableMethods, NearestAvailableMethods>();
            services.AddScoped<TaskAutoAssignment, NearestAvailableTaskAssignment>();
        }


        private void RegisterPollyServices(IServiceCollection services)
        {
            services.AddHttpClient<IDriverRepository, DriverRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IRoleRepository, RoleRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IUserRepository, UserRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<INotificationRepository, NotificationRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IUserDeviceRepository, UserDeviceRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IDriverLoginTrackingRepository, DriverLoginTrackingRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IGeofenceRepository, GeofenceRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IMainTaskRepository, MainTaskRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<ISubTaskRepository, SubTaskRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<ITaskDriverRequestRepository, TaskDriverRequestRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<ITaskHistoryRepository, TaskHistoryRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<ITaskGallaryRepository, TaskGallaryRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<ISettingRepository, SettingRepository>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IDriverDomainService, DriverDomainService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IUserDomainService, UserDomainService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IRoleDomainService, RoleDomainService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IDriverService, DriverService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IMobileSubTaskService, MobileSubTaskService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IMobileMainTaskService, MobileMainTaskService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<INearestAvailableMethods, NearestAvailableMethods>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<TaskAutoAssignment, NearestAvailableTaskAssignment>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IEmailSender, AuthMessageSender>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<ISmsSender, AuthMessageSender>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<ILoginService<ApplicationUser>, EFLoginService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IRedirectService, RedirectService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IEmailSenderservice,EmailSenderService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<EmailSettings,EmailSettings>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IGeoFenceManager,GeoFenceManager > ()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IFirebasePushNotificationService,FirebasePushNotificationService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IAddressProvider,CachedPACIProvider>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IAddressProvider,PACIAddressProvider>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IAddressProvider,GoogleAddressProvider>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IAddressService,AddressService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<INotificationSenderService,NotificationSenderService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IBackGroundNotificationSercvice,BackGroundNotificationSercvice>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IBackGroundNotificationJob,BackGroundNotificationJob>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IPACIService,PACIService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IAreaManager,AreaManager>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IDriverWorkingHourTrackingManager,DriverWorkingHourTrackingManager>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IUploadFormFileService,UploadFormFileService>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IRealTimeNotifier,SignalRNotifier>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IUploadFileManager,UploadFileManager>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IDriverFloorQuery,DriverFloorQuery>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<IMapsManager,MapsManager>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

            services.AddHttpClient<ITaskAutoAssignmentFactory,TaskAutoAssignmentFactory>()
                .SetHandlerLifetime(TimeSpan.FromMinutes(5)).AddPolicyHandler(GetRetryPolicy());

        }

        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .OrResult(msg => msg.StatusCode == HttpStatusCode.BadRequest)
                .WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
        }

    }
}