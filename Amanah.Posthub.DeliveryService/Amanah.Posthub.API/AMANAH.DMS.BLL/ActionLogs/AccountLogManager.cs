﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class AccountLogManager : BaseManager<AccountLogViewModel, AccountLogs>, IAccountLogManager
    {

        public AccountLogManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<AccountLogs> repository)
            : base(context, repository, mapper)
        {
        }




        public override Task<List<AccountLogViewModel>> GetAllAsync<AccountLogViewModel>()
        {

            return repository.GetAll()
                .ProjectTo<AccountLogViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }



        public Task<bool> Create(AccountLogs accountLog)
        {
#warning cleanup- this is an async method calling sync and return value has no meaning
            repository.AddAsync(accountLog);
            return Task.FromResult(true);
        }



        #region Extra
        public async Task<bool> DeleteAsync(int id)
        {
            var accountLog = (context as ApplicationDbContext).AccountLogs.SingleOrDefault(t => t.Id == id);

            if (accountLog == null)
            {
                return false;
            }
            var data = await base.SoftDeleteAsync(mapper.Map<AccountLogViewModel>(accountLog));
            return data;
        }




        #endregion
    }
}
