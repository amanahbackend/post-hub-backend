﻿using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class AccountLogViewModel
    {
        public int Id { get; set; }
        public string ActivityType { get; set; }
        public string Description { get; set; }
        public string TableName { get; set; }
        public int Record_Id { get; set; }
        public string CreatedBy_Id { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
