﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IAccountLogManager : IBaseManager<AccountLogViewModel, AccountLogs>
    {
        Task<bool> Create(AccountLogs input);
        Task<bool> DeleteAsync(int id);

    }
}
