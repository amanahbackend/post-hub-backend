﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IMapRequestsLogManager : IBaseManager<AccountLogViewModel, MapRequestLog>
    {
    }
}
