﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using AutoMapper;

namespace Amanah.Posthub.BLL.Managers
{
    public class MapRequestsLogManager : BaseManager<AccountLogViewModel, MapRequestLog>, IMapRequestsLogManager
    {
        public MapRequestsLogManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<MapRequestLog> repository)
            : base(context, repository, mapper)
        {
        }
    }
}
