﻿using Amanah.Posthub.BLL.ActionLogs.Queries.DTOs;
using Amanah.Posthub.Context;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.ActionLogs.Queries
{
    public interface IAccountLogQuery
    {
        Task<List<AccountLogResponseDTO>> GetAllAsync();
        Task<PagedResult<AccountLogResponseDTO>> GetByPaginationAsync(AccountLogFilterDTO filter);
        Task<List<AccountLogResponseDTO>> GetAccountLogsForExportAsync(AccountLogFilterDTO filter);
    }
    internal class AccountLogQuery : IAccountLogQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public AccountLogQuery(
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<List<AccountLogResponseDTO>> GetAllAsync()
        {
            return await context.AccountLogs
                .ProjectTo<AccountLogResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<PagedResult<AccountLogResponseDTO>> GetByPaginationAsync(AccountLogFilterDTO filter)
        {
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;

            var query = context.AccountLogs
                .Where(accountLog => (accountLog.CreatedDate.Date <= filter.EndDate.Value.Date)
                    && (accountLog.CreatedDate.Date >= filter.StartDate.Value.Date)
                    && (filter.SearchBy == "All" || accountLog.TableName == filter.SearchBy)
                    && (accountLog.CreatedBy_Id == filter.UserID || filter.UserID == null))
                .OrderByDescending(accountLog => accountLog.Id)
                .ProjectTo<AccountLogResponseDTO>(mapper.ConfigurationProvider);

            return new PagedResult<AccountLogResponseDTO>
            {
                TotalCount = await query.CountAsync(),
                Result = await query
                    .Skip((filter.PageNumber - 1) * filter.PageSize)
                    .Take(filter.PageSize)
                    .ToListAsync()
            };
        }

        public async Task<List<AccountLogResponseDTO>> GetAccountLogsForExportAsync(AccountLogFilterDTO filter)
        {
            return await context.AccountLogs
                .Where(accountLog => (accountLog.CreatedDate.Date <= filter.EndDate.Value.Date)
                    && (accountLog.CreatedDate.Date >= filter.StartDate.Value.Date)
                    && (filter.SearchBy == "All" || accountLog.TableName == filter.SearchBy)
                    && (accountLog.CreatedBy_Id == filter.UserID || filter.UserID == null))
                .OrderByDescending(accountLog => accountLog.Id)
                .ProjectTo<AccountLogResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }


    }
}
