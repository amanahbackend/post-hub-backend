﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;

namespace Amanah.Posthub.BLL.ActionLogs.Queries.DTOs
{
    public class AccountLogFilterDTO : PaginatedItemsViewModel
    {
        public string UserID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TimeZone { get; set; }
    }
}
