﻿using System;

namespace Amanah.Posthub.BLL.ActionLogs.Queries.DTOs
{
    public class AccountLogResponseDTO
    {
        public int Id { get; set; }
        public string ActivityType { get; set; }
        public string Description { get; set; }
        public string TableName { get; set; }
        public int Record_Id { get; set; }
        public string CreatedBy_Id { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
