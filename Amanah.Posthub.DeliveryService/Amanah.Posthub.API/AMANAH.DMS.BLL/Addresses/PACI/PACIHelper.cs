﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Utilites.PACI
{
    public class PACIHelper
    {
        public static async Task<PACIModel> GetPACIModelAsync(int? PACI, string proxyUrl,
                        string paciServiceUrl, string paciNumberFieldName)
        {
            try
            {
                if (PACI != null)
                {
                    var url = ConstructQueryGetURL(proxyUrl, paciServiceUrl,
                        new Dictionary<string, object>
                        {
                            { paciNumberFieldName, PACI }
                        });
                    string responseFromServer = await UrlHelper.GetServerResponseAsync(url);

                    PACIModel oRootObject = new PACIModel();
                    oRootObject = JsonConvert.DeserializeObject<PACIModel>(responseFromServer);
                    return oRootObject;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<PACIInfo> GetLocationByPACIAsync(int? PACI, string proxyUrl,
                        string paciServiceUrl, string paciNumberFieldName,
                        string blockServiceUrl, string blockNameFieldNameBlockService, string nhoodNameFieldName,
                        string streetServiceUrl, string blockNameFieldNameStreetService, string streetNameFieldName)
        {
            try
            {
                if (PACI != null)
                {
                    PACIModel oRootObject = await GetPACIModelAsync(PACI, proxyUrl, paciServiceUrl, paciNumberFieldName);
                    var featureAttributes = oRootObject.features[0].attributes;

                    var block = await GetBlockAsync(featureAttributes.neighborhoodenglish,
                                           featureAttributes.blockenglish, proxyUrl,
                                           blockServiceUrl, blockNameFieldNameBlockService, nhoodNameFieldName);

                    var street = await GetStreetAsync(featureAttributes.streetenglish,
                                            featureAttributes.blockenglish, proxyUrl,
                                            streetServiceUrl, streetNameFieldName, blockNameFieldNameStreetService);

                    PACIInfo info = new PACIInfo()
                    {
                        Governorate = new DropPACI()
                        {
                            Id = featureAttributes.governorateid,
                            NameEnglish = featureAttributes.governorateenglish,
                            NameArabic = featureAttributes.governoratearabic
                        },
                        Area = new DropPACI()
                        {
                            Id = featureAttributes.neighborhoodid,
                            NameEnglish = featureAttributes.neighborhoodenglish,
                            NameArabic = featureAttributes.neighborhoodarabic
                        },
                        Block = block,
                        Street = street,
                        Longtiude = (decimal)featureAttributes.lon,
                        Latitude = (decimal)featureAttributes.lat

                    };
                    return info;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }

        }

        public static async Task<StreetPACIResultModel> GetStreetPACItModelAsync(string streetName, string blockName,
            string proxyUrl, string serviceUrl, string streetFieldName, string blockNameFieldName)
        {
            try
            {
                if (streetName != null && blockName != null)
                {
                    var URL = ConstructQueryGetURL(proxyUrl, serviceUrl,
                        new Dictionary<string, object>
                        {
                            {blockNameFieldName, blockName },
                            {streetFieldName, streetName}
                        });
                    string responseFromServer = await UrlHelper.GetServerResponseAsync(URL);
                    return JsonConvert.DeserializeObject<StreetPACIResultModel>(responseFromServer);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;

            }
        }

        public static async Task<DropPACI> GetStreetAsync(string streetName, string blockName, string proxyUrl,
                        string serviceUrl, string streetFieldName, string blockNameFieldName)
        {
            try
            {
                if (streetName != null && blockName != null)
                {
                    var oStreet = await GetStreetPACItModelAsync(streetName, blockName, proxyUrl,
                                                        serviceUrl, streetFieldName, blockNameFieldName);

                    return new DropPACI()
                    {
                        Id = oStreet.features[0].attributes.StreetNumber.ToString(),
                        NameEnglish = oStreet.features[0].attributes.StreetEnglish,
                        NameArabic = oStreet.features[0].attributes.StreetArabic

                    };
                }
                else
                {
                    return null;
                }

            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<DropPACI> GetBlockAsync(string nhoodName, string blockName, string proxyUrl,
            string serviceUrl, string blockNameFieldName, string nhoodNameFieldName)
        {
            //nhoodName = nhoodName.Replace("\"", "'");
            //blockName = blockName.Replace("\"", "'");
            try
            {
                if (nhoodName != null && blockName != null)
                {
                    var URL = ConstructQueryGetURL(proxyUrl, serviceUrl,
                        new Dictionary<string, object>
                        {
                            {blockNameFieldName, blockName},
                            {nhoodNameFieldName, nhoodName}
                        });

                    //var url2 = string.Format("http://37.34.230.240:81/KuwaitFinderProxy/proxy.ashx?https://kuwaitportal.paci.gov.kw/arcgisportal/rest/services/AddressSearch/MapServer/1/query?where=BlockEnglish="+"'{0}'"+"&NeighborhoodEnglish="+"'{1}'"+"&returnGeometry=false&outFields=*&f=pjson",blockName,nhoodName);
                    string responseFromServer = await UrlHelper.GetServerResponseAsync(URL);
                    //string responseFromServer2 = UrlUtilities.GetServerResponse(url2);
                    var oBlock = JsonConvert.DeserializeObject<BlockPACIResultModel>(responseFromServer);

                    return new DropPACI()
                    {
                        Id = oBlock.features[0].attributes.BlockID,
                        NameEnglish = oBlock.features[0].attributes.BlockEnglish,
                        NameArabic = oBlock.features[0].attributes.BlockArabic
                    };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<List<DropPACI>> GetAllGovernoratesAsync(string proxyUrl, string serviceUrl)
        {
            try
            {
                var URL = ConstructQueryGetURL(proxyUrl, serviceUrl,
                    new Dictionary<string, object>
                    {
                        {"1",1}
                    });
                string responseFromServer = await UrlHelper.GetServerResponseAsync(URL);
                var oGov = JsonConvert.DeserializeObject<GovPACIResultModel>(responseFromServer);
                List<DropPACI> outGovs = new List<DropPACI>();
                for (int i = 0; i < oGov.features.Count; i++)
                {
                    DropPACI temp = new DropPACI()
                    {
                        Id = oGov.features[i].attributes.Gov_no.ToString(),
                        NameEnglish = oGov.features[i].attributes.GovernorateEnglish,
                        NameArabic = oGov.features[i].attributes.GovernorateArabic,
                    };
                    outGovs.Add(temp);
                }
                return outGovs;
            }
            catch (Exception)
            {
                return null;
            }



        }

        public static async Task<List<DropPACI>> GetAreasAsync(int? govNo, string govNoFieldName, string proxyUrl, string serviceUrl)
        {
            try
            {
                if (govNo != null)
                {
                    var url = ConstructQueryGetURL(proxyUrl, serviceUrl,
                         new Dictionary<string, object>
                         {
                            {govNoFieldName, govNo }
                         });

                    string responseFromServer = await UrlHelper.GetServerResponseAsync(url);
                    var oAreas = JsonConvert.DeserializeObject<AreaPACIResultModel>(responseFromServer);
                    List<DropPACI> outAreas = new List<DropPACI>();
                    for (int i = 0; i < oAreas.features.Count; i++)
                    {
                        DropPACI temp = new DropPACI()
                        {
                            Id = oAreas.features[i].attributes.Nhood_No.ToString(),
                            NameEnglish = oAreas.features[i].attributes.NeighborhoodEnglish,
                            NameArabic = oAreas.features[i].attributes.NeighborhoodArabic
                        };
                        outAreas.Add(temp);
                    }
                    return outAreas;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public static async Task<List<DropPACI>> GetBlocksAsync(int? nhoodNo, string nhoodNoFieldName, string proxyUrl, string serviceUrl)
        {
            try
            {
                if (nhoodNo != null)
                {
                    var url = ConstructQueryGetURL(proxyUrl, serviceUrl,
                             new Dictionary<string, object>
                             {
                                {nhoodNoFieldName, nhoodNo}
                             });
                    string responseFromServer = await UrlHelper.GetServerResponseAsync(url);
                    var oBlocks = JsonConvert.DeserializeObject<BlockPACIResultModel>(responseFromServer);

                    List<DropPACI> outBlocks = new List<DropPACI>();
                    for (int i = 0; i < oBlocks.features.Count; i++)
                    {
                        DropPACI temp = new DropPACI()
                        {
                            Id = oBlocks.features[i].attributes.BlockID,
                            NameEnglish = oBlocks.features[i].attributes.BlockEnglish,
                            NameArabic = oBlocks.features[i].attributes.BlockArabic
                        };
                        outBlocks.Add(temp);
                    }
                    return outBlocks;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<List<DropPACI>> GetStreetsAsync(int? govId, string govIdFieldName,
                                int? areaId, string areaIdFieldName,
                                string blockName, string blockNameFieldName,
                                string proxyUrl, string serviceUrl)
        {
            try
            {
                if (govId != null && areaId != null && blockName != null && blockName != "")
                {
                    var url = ConstructQueryGetURL(proxyUrl, serviceUrl,
                             new Dictionary<string, object>()
                             {
                                {govIdFieldName, govId},
                                {areaIdFieldName, areaId},
                                {blockNameFieldName, blockName}
                             });
                    string responseFromServer = await UrlHelper.GetServerResponseAsync(url);
                    var oStreets = JsonConvert.DeserializeObject<StreetPACIResultModel>(responseFromServer);
                    List<DropPACI> outAStreets = new List<DropPACI>();
                    for (int i = 0; i < oStreets.features.Count; i++)
                    {
                        DropPACI temp = new DropPACI()
                        {
                            Id = oStreets.features[i].attributes.StreetNumber.ToString(),
                            NameEnglish = oStreets.features[i].attributes.StreetEnglish,
                            NameArabic = oStreets.features[i].attributes.StreetArabic
                        };
                        outAStreets.Add(temp);
                    }
                    return outAStreets;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }

        }

        private static string ConstructQueryGetURL(string proxyUrl, string serviceUrl, Dictionary<string, object> parametersValuesDict)

        {
            string whereClause = ConstructWhereClause(parametersValuesDict);
            var query = $"query?{whereClause}&returnGeometry=false&outFields=*&f=pjson";
            var URL = $"{proxyUrl}?{serviceUrl}/{query}";
            return URL;
        }

        private static string ConstructWhereClause(Dictionary<string, object> parametersValuesDict)
        {
            string result = "where=";
            foreach (var key in parametersValuesDict.Keys)
            {
                object value = parametersValuesDict[key];
                if (value is int intValue)
                {
                    if (parametersValuesDict.Keys.First().Equals(key))
                    {
                        result += $"{key}={intValue}";
                    }
                    else
                    {
                        result += $" and {key}={intValue}";
                    }
                }
                else
                {
                    if (parametersValuesDict.Keys.First().Equals(key))
                    {
                        result += $"{key}='{value}'";
                    }
                    else
                    {
                        result += $" and {key}='{value}'";
                    }
                }
            }
            return result;
        }


        public class DropPACI
        {
            public string Id { get; set; }
            public string NameEnglish { get; set; }
            public string NameArabic { get; set; }

        }

        public class PACIInfo
        {
            public DropPACI Governorate { get; set; }
            public DropPACI Area { get; set; }
            public DropPACI Block { get; set; }
            public DropPACI Street { get; set; }
            public decimal Longtiude { get; set; }
            public decimal Latitude { get; set; }
        }
    }
}
