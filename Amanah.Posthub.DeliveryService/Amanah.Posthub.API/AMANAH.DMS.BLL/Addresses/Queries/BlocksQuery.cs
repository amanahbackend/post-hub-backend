﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Context;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Addresses.Queries
{
    public interface IBlockQuery
    {
        Task<List<BlockViewModel>> GetAllBlockAsync();
        Task<List<BlockViewModel>> GetAllAreaBlocksAsync(string areaName);

        Task<BlockViewModel> Get(int id);
    }

    public class BlockQuery : IBlockQuery
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public BlockQuery(
             ApplicationDbContext context,
             IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<BlockViewModel> Get(int id)
        {
            var res = await _context.Blocks
                   .IgnoreQueryFilters()
                   .Where(x => x.Id == id && x.IsDeleted != true)
                   .ProjectTo<BlockViewModel>(_mapper.ConfigurationProvider).IgnoreQueryFilters()
                   .FirstOrDefaultAsync();
            return res;
        }
        public async Task<List<BlockViewModel>> GetAllBlockAsync()
        {
            var Blocks = await _context.Blocks.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                .ProjectTo<BlockViewModel>(_mapper.ConfigurationProvider)
                .IgnoreQueryFilters().ToListAsync();
            return Blocks;
        }

        public async Task<List<BlockViewModel>> GetAllAreaBlocksAsync(string areaName)
        {
            var Blocks = await _context.Blocks.IgnoreQueryFilters()
            .Where(c => c.IsDeleted != true && c.Area.NameEN == areaName)
            .ProjectTo<BlockViewModel>(_mapper.ConfigurationProvider)
            .IgnoreQueryFilters()
            .ToListAsync();
            return Blocks;
        }
    }
}
