﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Addresses.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Addresses.Queries
{
    public interface IGovernorateQuery
    {
        Task<List<GovernarateViewModel>> GetAllGovernarateAsync();
        Task<List<GovernarateListViewModel>> GetGovernarateListAsync();
        Task<List<GovernarateViewModel>> GetAllActiveGovernarateAsync();
        Task<List<GovernarateViewModel>> GetAllActiveGovernarateByCountryIdAsync(int CountryId);
        Task<GovernarateViewModel> Get(int id);
        Task<GovernarateViewModel> GetGovernarateAsync(int id);
        Task<string> GetGovernarateNameAsync(int id);

        Task<List<GovernarateWithNameENKeyResponseDTO>> GetGovernarateListForAddressComponentAsync(int id = 0);
    }

    public class GovernorateQuery : IGovernorateQuery
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GovernorateQuery(
             ApplicationDbContext context,
             IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<List<GovernarateViewModel>> GetAllActiveGovernarateAsync()
        {
            var governarates = await _context.Governrates.IgnoreQueryFilters().Where(c => c.IsActive == true && c.IsDeleted != true)
                .ProjectTo<GovernarateViewModel>(_mapper.ConfigurationProvider)
                .IgnoreQueryFilters().ToListAsync();
            return governarates;
        }

        public async Task<List<GovernarateViewModel>> GetAllActiveGovernarateByCountryIdAsync(int CountryId)
        {
            var governarates = await _context.Governrates.IgnoreQueryFilters().Where(c => c.IsActive == true && c.IsDeleted != true && (CountryId > 0 && c.CountryId == CountryId))
                .ProjectTo<GovernarateViewModel>(_mapper.ConfigurationProvider)
                .IgnoreQueryFilters()
                .ToListAsync();
            return governarates;
        }
        public async Task<GovernarateViewModel> Get(int id)
        {
            var res = await _context.Governrates.IgnoreQueryFilters()
                   .Where(x => x.Id == id && x.IsDeleted != true)
                   .ProjectTo<GovernarateViewModel>(_mapper.ConfigurationProvider).IgnoreQueryFilters()
                   .FirstOrDefaultAsync();
            return res;
        }
        public async Task<List<GovernarateViewModel>> GetAllGovernarateAsync()
        {
            var governarates = await _context.Governrates.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                .ProjectTo<GovernarateViewModel>(_mapper.ConfigurationProvider)
                .IgnoreQueryFilters().ToListAsync();
            return governarates;
        }

        public async Task<GovernarateViewModel> GetGovernarateAsync(int id)
        {
            var governarate = await _context.Governrates.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                .ProjectTo<GovernarateViewModel>(_mapper.ConfigurationProvider).IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.Id == id);
            return governarate;
        }

        public async Task<string> GetGovernarateNameAsync(int id)
        {
            var governarate = await _context.Governrates.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                .ProjectTo<GovernarateViewModel>(_mapper.ConfigurationProvider).IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.Id == id);
            return governarate.Name;
        }

        public async Task<List<GovernarateListViewModel>> GetGovernarateListAsync()
        {
            var governarates = await _context.Governrates.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
               .ProjectTo<GovernarateListViewModel>(_mapper.ConfigurationProvider)
               .IgnoreQueryFilters().ToListAsync();
            return governarates;
        }

        public async Task<List<GovernarateWithNameENKeyResponseDTO>> GetGovernarateListForAddressComponentAsync(int id = 0)
        {
            var governarates = await _context
                .Governrates.IgnoreQueryFilters().Where(c => c.IsDeleted != true && (id == 0 || c.CountryId == id))
               .ProjectTo<GovernarateWithNameENKeyResponseDTO>(_mapper.ConfigurationProvider)
               .IgnoreQueryFilters().ToListAsync();
            return governarates;
        }

    }
}
