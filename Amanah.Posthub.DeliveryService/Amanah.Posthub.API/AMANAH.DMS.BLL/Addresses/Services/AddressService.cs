﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public class AddressService : IAddressService
    {
        private readonly IReadOnlyCollection<IAddressProvider> _addressProviders;

        public AddressService(IEnumerable<IAddressProvider> addressProviders)
        {
            _addressProviders = addressProviders.OrderBy(x => x.Priority).ToArray();
        }

        public async Task<AddressResultViewModel> SearchForAddressAsync(Address address)
        {

            foreach (var addressProvider in _addressProviders)
            {
                var AddressResut = await addressProvider.SearchForAddressAsync(address);
                if (AddressResut != null)
                {
                    return AddressResut;
                }
            }
            return null;
        }

        public async Task<AddressResultViewModel> SearchForAddressByPACIAsync(int paciNumber)
        {
            foreach (var addressProvider in _addressProviders)
            {
                var AddressResut = await addressProvider.SearchForAddressByPACIAsync(paciNumber);
                if (AddressResut != null)
                {
                    return AddressResut;
                }
            }
            return null;
        }
    }
}
