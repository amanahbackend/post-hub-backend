﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public class AreaManager : BaseManager<AreaViewModel, Area>, IAreaManager
    {
        public AreaManager(ApplicationDbContext context, IMapper mapper, IRepositry<Area> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<IReadOnlyCollection<AreaViewModel>> GetAllByGovernorateIdAsync(string governorateId)
        {
            var areasQuery =
                from area in context.Area.IgnoreQueryFilters()
                join governorate in context.Governrates.IgnoreQueryFilters() on area.FK_Governrate_Id equals governorate.Id
                where governorate.NameEN == governorateId
                select area;
            var result = await areasQuery
                .ProjectTo<AreaViewModel>(mapper.ConfigurationProvider)
                .ToArrayAsync();
            return result;
        }

        public async Task<IReadOnlyCollection<AreaViewModel>> GetAllKwuitAreasAsync()
        {
          var areas = await context.Area
                .IgnoreQueryFilters()
                .Where(x => !x.IsDeleted && x.Governrate.CountryId == 121) //KwuitId = 121
                .ProjectTo<AreaViewModel>(mapper.ConfigurationProvider)
                .ToArrayAsync();
            return areas;
        }

    }
}
