﻿using Amanah.Posthub.BLL.Addresses.Services;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class CachedPACIProvider : IAddressProvider
    {
        private readonly ApplicationDbContext _context;
        private readonly IPACIService _paciService;
        private readonly IMapper _mapper;

        public int Priority { get; set; }

        public CachedPACIProvider(
            ApplicationDbContext context,
            IConfiguration configuration,
            IPACIService paciService,
            IMapper mapper)
        {
            _context = context;
            _paciService = paciService;
            _mapper = mapper;
            Priority = configuration.GetValue<int>("Address:CachedPACIProvider:Priority");
        }

        public async Task<AddressResultViewModel> SearchForAddressAsync(Address address)
        {
            var paciQuery = _context.PACI.AsQueryable().IgnoreQueryFilters().AsNoTracking();
            if (!string.IsNullOrWhiteSpace(address.Governorate))
            {
                paciQuery = paciQuery.Where(paci => paci.GovernorateNameEnglish.ToLower().Trim() == address.Governorate.ToLower().Trim());
            }
            if (!string.IsNullOrWhiteSpace(address.Area))
            {
                paciQuery = paciQuery.Where(paci => paci.AreaNameEnglish.ToLower().Trim() == address.Area.ToLower().Trim());
            }
            if (!string.IsNullOrWhiteSpace(address.Block))
            {
                paciQuery = paciQuery.Where(paci => paci.BlockNameEnglish.ToLower().Trim() == address.Block.ToLower().Trim());
            }
            if (!string.IsNullOrWhiteSpace(address.Building))
            {
                paciQuery = paciQuery.Where(paci => paci.BuildingNumber.ToLower().Trim() == address.Building.ToLower().Trim());
            }
            var PACIResult = await paciQuery.FirstOrDefaultAsync();
            if (PACIResult != null)
            {
                var result = new AddressResultViewModel
                {
                    Latitude = PACIResult.Latitude,
                    Longtiude = PACIResult.Longtiude,
                    Location = _mapper.Map<Address>(PACIResult),
                };
                result.Address = await _paciService.GetAddressLocalizedSummaryAsync(result.Location);
                return result;
            }
            return null;
        }

        public async Task<AddressResultViewModel> SearchForAddressByPACIAsync(int paciNumber)
        {
            var PACIResult = await _context.PACI.IgnoreQueryFilters()
                .FirstOrDefaultAsync(paci => paci.PACINumber.ToLower().Trim() == paciNumber.ToString());
            if (PACIResult != null)
            {
                var result = new AddressResultViewModel
                {
                    Latitude = PACIResult.Latitude,
                    Longtiude = PACIResult.Longtiude,
                    Location = _mapper.Map<Address>(PACIResult),
                };
                result.Address = await _paciService.GetAddressLocalizedSummaryAsync(result.Location);
                return result;
            }
            return null;
        }
    }
}
