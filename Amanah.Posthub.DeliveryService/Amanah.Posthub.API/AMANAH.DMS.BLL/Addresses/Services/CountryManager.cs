﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    class CountryManager : BaseManager<CountryResultDto, Country>, ICountryManager
    {
        private readonly IRepository<Country, int> _countryRepository;
        private readonly IUnitOfWork unityOfWork;
        private readonly IMapper mapperCountry;
        private readonly ApplicationDbContext _contextApp;
        private readonly ILocalizer _localizer;

        public CountryManager(
             ApplicationDbContext context,
             ApplicationDbContext contextApp,
             IMapper mapper,
             IRepositry<Country> repository,
             IRepository<Country, int> countryRepository,
             IUnitOfWork unityOfWork,
             IMapper mapperCountry,
             ILocalizer localizer
             )
             : base(context, repository, mapper)
        {
            _contextApp = contextApp;
            _countryRepository = countryRepository;
            this.mapperCountry = mapperCountry;
            this.unityOfWork = unityOfWork;
            this._localizer = localizer;
        }

        public async Task<CountryResultDto> Get(int id)
        {
            return await context.Country
                .IgnoreQueryFilters()
                .Where(x => x.Id == id)
                .ProjectTo<CountryResultDto>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
        public async Task<List<CountryResultDto>> GetAllActiveCountriesAsync()
        {
            var CountryList = await _contextApp.Country
                .IgnoreQueryFilters()
                .Where(c => c.IsDeleted != true 
                && c.IsActive == true
                && (c.Governrates != null && c.Governrates.Count > 0))
               .IgnoreQueryFilters()
               .ProjectTo<CountryResultDto>(mapper.ConfigurationProvider)
               .ToListAsync();
            return CountryList;
        }

        public async Task<bool> ActiveInActiveAsync(CountryResultDto entity)
        {
            var res = await context.Country
                  .IgnoreQueryFilters()
                  .Where(x => x.Id == entity.Id)
                  .ProjectTo<CountryResultDto>(mapper.ConfigurationProvider)
                  .FirstOrDefaultAsync();
            return true;
        }

        public async Task<CountryResultDto> CreateAsync(CountryResultDto entity)
        {
            Country country = null;
            await unityOfWork.RunTransaction(async () =>
            {
                var countryEF = context.Country.IgnoreQueryFilters().Where(c => c.Name.ToLower() == entity.Name.ToLower()).FirstOrDefault();
                country = mapper.Map<Country>(entity);
                if (countryEF == null)
                {
                    _countryRepository.Add(country);
                    await unityOfWork.SaveChangesAsync();
                }
                else
                {
                    throw new DomainException(_localizer[Keys.Messages.CountryExist]);
                }


            });

            return entity;
        }

        public async Task<CountryResultDto> AsyncUpdate(CountryResultDto entity)
        {
            await unityOfWork.RunTransaction(async () =>
            {
                bool isCountryExist = context.Country.IgnoreQueryFilters().Where(c => (c.Name.ToLower() == entity.Name.ToLower() || c.Code.ToLower() == entity.Code.ToLower())
                                                       && c.Id != entity.Id).Count() > 0;
                if (!isCountryExist)
                {
                    var existingCountry = mapper.Map<Country>(entity);
                    _countryRepository.Update(existingCountry);
                    await unityOfWork.SaveChangesAsync();
                }
                else
                {
                    throw new DomainException(_localizer[Keys.Messages.CountryExist]);
                }

            });
            return new CountryResultDto
            {
                Id = entity.Id,
                IsSystem = entity.IsSystem,
                IsActive = entity.IsActive,
                Name = entity.Name,
                TopLevel = entity.TopLevel,
                CourierZoneId = entity.CourierZoneId,
            };
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var existingCountry = await _countryRepository.GetAllIQueryable(c => c.Id == id)
               .FirstOrDefaultAsync();
            if (existingCountry != null)
            {
                await unityOfWork.RunTransaction(async () =>
                {
                    existingCountry.SetIsDeleted(true);
                    _countryRepository.Update(existingCountry);
                    await unityOfWork.SaveChangesAsync();
                });

                return true;
            }
            return false;
        }

        public async Task<List<CountryResultDto>> GetAllCountriesesAsync()
        {
            var CountryList = await _contextApp.Country
                .IgnoreQueryFilters()
                .Where(c => c.IsDeleted != true)
                .IgnoreQueryFilters()
                .OrderBy(c => c.Id)
                .ProjectTo<CountryResultDto>(mapper.ConfigurationProvider)
                .ToListAsync();

            return CountryList;
        }
    }
}
