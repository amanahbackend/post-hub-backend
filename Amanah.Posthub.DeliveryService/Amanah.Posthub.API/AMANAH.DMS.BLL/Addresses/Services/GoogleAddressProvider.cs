﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public class GoogleAddressProvider : IAddressProvider
    {
        public int Priority { get; set; }

        public GoogleAddressProvider(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<PACI> repository,
            IConfiguration configuration)
        {
            Priority = configuration.GetValue<int>("Address:GoogleAddressProvider:Priority");
        }

        public Task<AddressResultViewModel> SearchForAddressAsync(Address address)
        {
            return Task.FromResult<AddressResultViewModel>(null);
        }

        public Task<AddressResultViewModel> SearchForAddressByPACIAsync(int paciNumber)
        {
            return Task.FromResult<AddressResultViewModel>(null);
        }
    }
}
