﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Addresses.ViewModels;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public interface IGovernarateService
    {
        Task<CreateUpdateGovernarateViewModel> CreateAsync(CreateUpdateGovernarateViewModel entity);
        Task<CreateUpdateGovernarateViewModel> ActiveInActiveAsync(CreateUpdateGovernarateViewModel entity);
        Task<CreateUpdateGovernarateViewModel> UpdateAsync(CreateUpdateGovernarateViewModel entity);
        Task<bool> DeleteAsync(int id);
    }

    internal class GovernarateService : IGovernarateService
    {
        private readonly IRepository<Governrate, int> _governrateRepository;

        private readonly IMapper mapper;
        private readonly IUnitOfWork unityOfWork;
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        public GovernarateService(
            IRepository<Governrate, int> governrateRepository, IMapper mapper,
            IUnitOfWork unityOfWork, ApplicationDbContext context, ILocalizer localizer)
        {
            this._governrateRepository = governrateRepository;

            this.mapper = mapper;
            this.unityOfWork = unityOfWork;
            _context = context;
            _localizer = localizer;
        }

        public async Task<CreateUpdateGovernarateViewModel> CreateAsync(CreateUpdateGovernarateViewModel entity)
        {

            // avoid duplicate
            var isGovernrateExist = _context.Governrates.IgnoreQueryFilters()
                    .Where(a => !a.IsDeleted && (a.NameEN == entity.NameEN || a.NameAR == entity.NameAR))
                    .OrderByDescending(a => a.Id).ToList();

            if (isGovernrateExist.Count > 0)
                throw new DomainException(_localizer[Keys.Messages.GovernrateExist]);

            Governrate governrate = null;
            await unityOfWork.RunTransaction(async () =>
            {
                governrate = mapper.Map<Governrate>(entity);
                _governrateRepository.Add(governrate);
                await unityOfWork.SaveChangesAsync();

            });

            return entity;
        }

        public async Task<CreateUpdateGovernarateViewModel> UpdateAsync(CreateUpdateGovernarateViewModel entity)
        {
            await unityOfWork.RunTransaction(async () =>
            {
                var existingGov = await _governrateRepository.GetAllIQueryable(mng => mng.Id == entity.Id)
                   .FirstOrDefaultAsync();
                existingGov = mapper.Map<Governrate>(entity);
                if (existingGov != null)
                {
                    var isGovernrateExist = _context.Governrates.IgnoreQueryFilters()
                   .Where(a => !a.IsDeleted && a.Id != entity.Id && (a.NameEN == entity.NameEN || a.NameAR == entity.NameAR))
                   .OrderByDescending(a => a.Id).ToList();

                    if (isGovernrateExist.Count == 0)
                    {
                        _governrateRepository.Update(existingGov);
                        await unityOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        throw new DomainException(_localizer[Keys.Messages.GovernrateExist]);

                    }



                }
            });
            return new CreateUpdateGovernarateViewModel
            {
                Id = entity.Id,
                IsSystem = entity.IsSystem,
                IsActive = entity.IsActive,
                NameAR = entity.NameAR,
                NameEN = entity.NameEN,
                CountryId = entity.CountryId,
            };
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var existingGov = await _governrateRepository.GetAllIQueryable(c => c.Id == id)
                   .FirstOrDefaultAsync();
            if (existingGov != null)
            {
                await unityOfWork.RunTransaction(async () =>
                {
                    existingGov.SetIsDeleted(true);
                    _governrateRepository.Update(existingGov);
                    await unityOfWork.SaveChangesAsync();
                });

                return true;
            }
            return false;
        }

        public async Task<CreateUpdateGovernarateViewModel> ActiveInActiveAsync(CreateUpdateGovernarateViewModel entity)
        {
            var existingManager = await _governrateRepository.GetAllIQueryable(mng => mng.Id == entity.Id)
                 .FirstOrDefaultAsync();
            if (existingManager != null)
            {
                await unityOfWork.RunTransaction(async () =>
                {
                    existingManager.IsActive = entity.IsActive;
                    _governrateRepository.Update(existingManager);
                    await unityOfWork.SaveChangesAsync();
                });
            }
            return new CreateUpdateGovernarateViewModel
            {
                Id = entity.Id,
                IsSystem = entity.IsSystem,
                IsActive = entity.IsActive,
                NameEN = entity.NameEN,
                NameAR = entity.NameAR,
                CountryId = entity.CountryId,
            };
        }
    }
}
