﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public interface IAddressProvider
    {
        public int Priority { get; }

        Task<AddressResultViewModel> SearchForAddressAsync(Address address);
        Task<AddressResultViewModel> SearchForAddressByPACIAsync(int paciNumber);
    }
}
