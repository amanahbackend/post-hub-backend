﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IAddressService
    {
        Task<AddressResultViewModel> SearchForAddressAsync(Address address);
        Task<AddressResultViewModel> SearchForAddressByPACIAsync(int paciNumber);
    }
}
