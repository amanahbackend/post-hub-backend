﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public interface IAreaManager : IBaseManager<AreaViewModel, Area>
    {
        public Task<IReadOnlyCollection<AreaViewModel>> GetAllByGovernorateIdAsync(string governorateId);
        public Task<IReadOnlyCollection<AreaViewModel>> GetAllKwuitAreasAsync();

    }
}
