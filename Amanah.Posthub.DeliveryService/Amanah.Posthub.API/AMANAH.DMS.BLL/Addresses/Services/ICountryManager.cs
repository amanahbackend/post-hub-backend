﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public interface ICountryManager : IBaseManager<CountryResultDto, Country>
    {
        Task<CountryResultDto> Get(int id);
        Task<bool> ActiveInActiveAsync(CountryResultDto entity);
        Task<List<CountryResultDto>> GetAllActiveCountriesAsync();
        Task<List<CountryResultDto>> GetAllCountriesesAsync();
        //new 
        Task<CountryResultDto> CreateAsync(CountryResultDto entity);
        Task<CountryResultDto> AsyncUpdate(CountryResultDto entity);
        Task<bool> DeleteAsync(int id);

    }
}
