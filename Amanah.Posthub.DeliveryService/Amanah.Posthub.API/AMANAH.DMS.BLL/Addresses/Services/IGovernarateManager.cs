﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public interface IGovernarateManager
    {
        Task<List<GovernarateViewModel>> GetAllGovernarateAsync();
        Task<GovernarateViewModel> GetGovernarateAsync(string id);
        Task<string> GetGovernarateNameAsync(string id);
    }
}
