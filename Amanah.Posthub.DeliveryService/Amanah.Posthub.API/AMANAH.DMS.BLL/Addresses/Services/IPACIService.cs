﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public interface IPACIService
    {
        Task<List<StreetViewModel>> GetStreetsAsync(string areaNameEn, string blockName);

        Task<List<BlockViewModel>> GetBlocksAsync(string areaNameEn);

        Task<(string English, string Arabic)> GetAddressSummaryAsync(Address address);

        Task<string> GetAddressLocalizedSummaryAsync(Address address);
    }
}
