﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;
using Utilites.PACI;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public class PACIAddressProvider : IAddressProvider
    {
        private readonly PACISettings _settings;
        private readonly ApplicationDbContext _context;

        public int Priority { get; }

        public PACIAddressProvider(
            ApplicationDbContext context,
            IOptions<PACISettings> settings,
            IConfiguration configuration)
        {
            _settings = settings.Value;
            _context = context;
            Priority = configuration.GetValue<int>("Address:PACIAddressProvider:Priority");
        }

        public async Task<AddressResultViewModel> SearchForAddressAsync(Address address)
        {
            if (string.IsNullOrWhiteSpace(address.Street) || string.IsNullOrWhiteSpace(address.Block))
            {
                return null;
            }
            var defaultValue = default(StreetPACIResultModel);
            var policy = PolicyHelper.TimeoutEachCallAndRetry(
                _settings.RequestTimeOut,
                _settings.NumberOfRetris,
                defaultValue,
                value => value == defaultValue);
            var paciResult = await policy.ExecuteAsync(() =>
                PACIHelper.GetStreetPACItModelAsync(
                    address.Street,
                    address.Block,
                    _settings.ProxyUrl,
                    _settings.StreetServiceUrl,
                    _settings.StreetFieldNameStreetService,
                    _settings.BlockNameFieldNameStreetService));
            var addressResult = Map(paciResult);
            if (addressResult != null)
            {
                await SaveResultToCacheAsync(paciResult);
            }
            return addressResult;
        }
        public async Task<AddressResultViewModel> SearchForAddressByPACIAsync(int paciNumber)
        {

            var defaultValue = default(PACIHelper.PACIInfo);
            var policy = PolicyHelper.TimeoutEachCallAndRetry(
                _settings.RequestTimeOut,
                _settings.NumberOfRetris,
                defaultValue,
                value => value == defaultValue);
            var paciResult = await policy.ExecuteAsync(() =>
                 PACIHelper.GetLocationByPACIAsync(
                     paciNumber,
                    _settings.ProxyUrl,
                    _settings.PaciServiceUrl,
                    _settings.PACIFieldNamePaciService,
                    _settings.BlockServiceUrl,
                    _settings.BlockNameFieldNameBlockService,
                    _settings.AreaNameFieldNameBlockService,
                    _settings.StreetServiceUrl,
                    _settings.BlockNameFieldNameStreetService,
                    _settings.StreetFieldNameStreetService));
            var addressResult = Map(paciResult);
            if (addressResult != null)
            {
                await SaveResultToCacheAsync(paciResult, paciNumber, addressResult);
            }
            return addressResult;

        }


        private Task SaveResultToCacheAsync(StreetPACIResultModel address)
        {
            if (address != null && address.features.Any())
            {
                if (address.features[0] != null)
                {
                    PACI paci = new PACI
                    {
                        PACINumber = address.features[0].attributes.OBJECTID.ToString(),
                        GovernorateNameEnglish = address.features[0].attributes.GovernorateEnglish,
                        GovernorateNameArabic = address.features[0].attributes.GovernorateArabic,
                        GovernorateId = address.features[0].attributes.Gov_no,
                        AreaNameEnglish = address.features[0].attributes.NeighborhoodEnglish,
                        AreaNameArabic = address.features[0].attributes.NeighborhoodArabic,
                        AreaId = address.features[0].attributes.Nhood_No,
                        BlockNameEnglish = address.features[0].attributes.BlockEnglish,
                        BlockNameArabic = address.features[0].attributes.BlockArabic,
                        StreetNameArabic = address.features[0].attributes.StreetArabic,
                        StreetNameEnglish = address.features[0].attributes.StreetEnglish,
                        StreetId = (int)address.features[0].attributes.StreetNumber,
                        Latitude = address.features[0].attributes.CENTROID_Y.Value,
                        Longtiude = address.features[0].attributes.CENTROID_X.Value
                    };
                    _context.PACI.Add(paci);
                    return _context.SaveChangesAsync();
                }
            }
            return Task.CompletedTask;
        }

        private Task SaveResultToCacheAsync(PACIHelper.PACIInfo address, int paciNumber, AddressResultViewModel addressResult)
        {
            PACI paci = new PACI
            {
                PACINumber = paciNumber.ToString(),
                GovernorateNameEnglish = address.Governorate.NameEnglish,
                GovernorateNameArabic = address.Governorate.NameEnglish,
                AreaNameEnglish = address.Area.NameEnglish,
                AreaNameArabic = address.Area.NameArabic,
                BlockNameEnglish = address.Block.NameEnglish,
                BlockNameArabic = address.Block.NameArabic,
                StreetNameEnglish = address.Street.NameEnglish,
                StreetNameArabic = address.Street.NameArabic,
                Latitude = addressResult.Latitude,
                Longtiude = addressResult.Longtiude
            };
            _context.PACI.Add(paci);
            return _context.SaveChangesAsync();
        }
        private AddressResultViewModel Map(StreetPACIResultModel streetPACIResultModel)
        {
            var features = streetPACIResultModel?.features ?? Enumerable.Empty<StreetFeature>();
            var firstFeature = features.FirstOrDefault();
            if (firstFeature != null)
            {
                return new AddressResultViewModel
                {
                    Location = Map(firstFeature.attributes),
                    Latitude = firstFeature.attributes.CENTROID_Y.Value,
                    Longtiude = firstFeature.attributes.CENTROID_X.Value
                };
            }
            return null;
        }
        private AddressResultViewModel Map(PACIHelper.PACIInfo paciInfoModel)
        {
            if (paciInfoModel != null)
            {

                return new AddressResultViewModel
                {
                    Location = new Address
                    {
                        Governorate = paciInfoModel.Governorate.NameEnglish,
                        Area = paciInfoModel.Area.NameEnglish,
                        Block = paciInfoModel.Block.NameEnglish,
                        Street = paciInfoModel.Street.NameEnglish,
                    },
                    Latitude = paciInfoModel.Latitude,
                    Longtiude = paciInfoModel.Longtiude
                };
            }
            return null;
        }

        private Address Map(StreetAttribute streetAttribute)
        {
            return new Address
            {
                Governorate = streetAttribute.GovernorateEnglish,
                Area = streetAttribute.NeighborhoodEnglish,
                Block = streetAttribute.BlockEnglish,
                Street = streetAttribute.StreetEnglish,
            };
        }
        //private string PACIInfoToString(PACIHelper.PACIInfo address)
        //{
        //    address.Latitude = address.Latitude;
        //    address.Longtiude = address.Longtiude;
        //    StringBuilder addressString = new StringBuilder();
        //    addressString.Append("Kwuit-");
        //    addressString.Append(address.Governorate.Name);
        //    addressString.Append("-");
        //    addressString.Append(address.Area.Name);
        //    addressString.Append("-Street");
        //    addressString.Append(address.Street.Name);
        //    addressString.Append("-Block");
        //    addressString.Append(address.Block.Name);
        //    return addressString.ToString();

        //}
    }




}

