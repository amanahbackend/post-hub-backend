﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilites.PACI;
using Utilities.Extensions;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public class PACIService : IPACIService
    {
        private readonly PACISettings _settings;
        private readonly ApplicationDbContext _context;

        public PACIService(
            IOptions<PACISettings> settings,
            ApplicationDbContext context)
        {
            _settings = settings.Value;
            _context = context;
        }

        public async Task<List<StreetViewModel>> GetStreetsAsync(string areaNameEN, string blockName)
        {
            var area = await _context.Area
                .Where(x => x.NameEN == areaNameEN)
                .Select(x => new { Id = x.Id, NameEN = x.NameEN, FK_Governrate_Id = x.FK_Governrate_Id })
                .FirstOrDefaultAsync();
            var defaultValue = default(List<PACIHelper.DropPACI>);
            var result = defaultValue;
            if (area != null)
            {


                // int.TryParse(area.FK_Governrate_Id, out var governrateId);
                var policy = PolicyHelper.TimeoutEachCallAndRetry(
                    _settings.RequestTimeOut,
                    _settings.NumberOfRetris,
                    defaultValue,
                    result => result == defaultValue);
                result = await policy.ExecuteAsync(() =>
                    PACIHelper.GetStreetsAsync(
                        area.FK_Governrate_Id,
                        _settings.GovernorateIdFieldNameAreaService,
                        Convert.ToInt32(area.Id), _settings.AreaIdFieldNameStreetService,
                        blockName,
                        _settings.BlockNameFieldNameStreetService,
                        _settings.ProxyUrl,
                        _settings.StreetServiceUrl));
                // TODO: Cache the result

            }
            var streets = result.Select(x => new StreetViewModel()
            {
                Id = x.NameEnglish,
                Name = CultureInfo.CurrentCulture.Name == "en"
                             ? x.NameEnglish
                             : x.NameArabic
            }).ToList();

            return streets;
        }

        public async Task<List<BlockViewModel>> GetBlocksAsync(string areaNameEN)
        {
            var area = await _context.Area
                .Where(x => x.NameEN == areaNameEN)
                .Select(x => new { Id = x.Id, NameEN = x.NameEN })
                .FirstOrDefaultAsync();
            if (area != null)
            {
                // int.Parse(area.Id, out var areaId);
                var defaultValue = default(List<PACIHelper.DropPACI>);
                var policy = PolicyHelper.TimeoutEachCallAndRetry(
                    _settings.RequestTimeOut,
                    _settings.NumberOfRetris,
                    defaultValue,
                    result => result == defaultValue);
                var result = await policy.ExecuteAsync(() =>
                     PACIHelper.GetBlocksAsync(
                          area.Id,
                         _settings.AreaIdFieldNameBlockService,
                         _settings.ProxyUrl,
                         _settings.BlockServiceUrl));
                // TODO: Cache the result
                var blocks = result.Select(x => new BlockViewModel()
                {
                    Id = x.NameEnglish,
                    Name = CultureInfo.CurrentCulture.Name == "en"
                     ? x.NameEnglish
                     : x.NameArabic
                }).ToList();

                return blocks;
            }
            return new List<BlockViewModel>();
        }

        public Task<(string English, string Arabic)> GetAddressSummaryAsync(Address address)
        {
#if false
            var targetGovernorate = await _context.Governrates.FirstOrDefaultAsync(
                governorate => governorate.NameEN == address.Governorate);
            var targetArea = await _context.Area.FirstOrDefaultAsync(area => area.NameEN == address.Area);
            var paci = await _context.PACI.FirstOrDefaultAsync(paci =>
                    paci.GovernorateName.ToString() == targetGovernorate.Id &&
                    paci.AreaName == address.Area &&
                    paci.BlockName == address.Block &&
                    paci.StreetName == address.Street); 
            var 
#endif
            // temporary solution return english for both ar and english
            // later we can get all these values from paci table
            return Task.FromResult((address.ToString(), $"{address} Arabic"));
        }

        public async Task<string> GetAddressLocalizedSummaryAsync(Address address)
        {
            var (en, ar) = await GetAddressSummaryAsync(address);
            return Thread.CurrentThread.CurrentCulture.IsArabic()
                ? ar
                : en;
        }
    }
}
