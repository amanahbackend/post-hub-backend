﻿using Polly;
using Polly.Timeout;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Addresses.Services
{
    public static class PolicyHelper
    {
        public static IAsyncPolicy<TResult> TimeoutEachCallAndRetry<TResult>(
            TimeSpan tryTimeout,
            int retryCount,
            TResult fallbackValue,
            Func<TResult, bool> handleValuePredicate = null)
        {
            var handlePolicy = Policy
                .Handle<TimeoutRejectedException>()
                .Or<HttpRequestException>()
                .Or<WebException>()
                .OrResult<TResult>(result => handleValuePredicate == null || handleValuePredicate(result));

            var fallbackPolicy = handlePolicy
                .FallbackAsync((ct) => Task.FromResult(fallbackValue));

            var waitAndRetryPloicy = handlePolicy.WaitAndRetryAsync(retryCount, retry => TimeSpan.FromSeconds(retry));

            var timeoutPolicy = Policy.TimeoutAsync(tryTimeout, TimeoutStrategy.Pessimistic)
                .AsAsyncPolicy<TResult>();

            var finalPolicy = Policy
                .WrapAsync(fallbackPolicy, waitAndRetryPloicy, timeoutPolicy);

            return finalPolicy;
        }
    }
}
