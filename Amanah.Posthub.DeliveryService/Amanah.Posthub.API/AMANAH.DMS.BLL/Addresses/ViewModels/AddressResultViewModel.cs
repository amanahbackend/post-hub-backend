﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;

namespace Amanah.Posthub.BLL.Addresses.ViewModels
{
    public class AddressResultViewModel
    {
        public Address Location { get; set; }

        public string Address { get; set; }

        public decimal Longtiude { get; set; }

        public decimal Latitude { get; set; }
    }
}
