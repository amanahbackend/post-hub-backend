﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class AddressResultDto
    {
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }

    }
}
