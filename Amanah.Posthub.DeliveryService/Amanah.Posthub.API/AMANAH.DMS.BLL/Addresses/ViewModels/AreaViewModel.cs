﻿namespace Amanah.Posthub.BLL.Addresses.ViewModels
{
    public class AreaViewModel
    {
        public string Id { set; get; }
        public string Name { set; get; }
        public int CountryId { set; get; }
        public int GovernorateId { set; get; }
    }
}
