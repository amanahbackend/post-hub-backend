﻿namespace Amanah.Posthub.BLL.Addresses.ViewModels
{
    public class BlockViewModel
    {
        public int BlockId { set; get; }
        public string Id { set; get; }
        public string Name { set; get; }
    }
}
