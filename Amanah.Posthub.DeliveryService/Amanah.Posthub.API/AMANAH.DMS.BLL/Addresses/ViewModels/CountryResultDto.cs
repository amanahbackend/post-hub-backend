﻿namespace Amanah.Posthub.BLL.Addresses.ViewModels
{
    public class CountryResultDto
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Code { set; get; }
        public string Flag { set; get; }
        public string FlagUrl { set; get; }
        public string TopLevel { set; get; }
        public int? CourierZoneId { set; get; }
        public string CourierZoneName { set; get; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

    }
}
