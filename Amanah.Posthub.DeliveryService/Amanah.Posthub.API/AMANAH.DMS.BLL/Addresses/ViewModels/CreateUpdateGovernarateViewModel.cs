﻿namespace Amanah.Posthub.DeliverService.BLL.Addresses.ViewModels
{
    public class CreateUpdateGovernarateViewModel
    {
        public int Id { get; set; }
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public int? CountryId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsSystem { get; set; }

    }
}
