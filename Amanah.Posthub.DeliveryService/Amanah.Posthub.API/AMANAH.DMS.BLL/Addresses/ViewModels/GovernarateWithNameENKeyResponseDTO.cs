﻿namespace Amanah.Posthub.BLL.Addresses.ViewModels
{
    public class GovernarateWithNameENKeyResponseDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsSystem { get; set; }

    }
}
