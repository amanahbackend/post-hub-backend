﻿namespace Amanah.Posthub.BLL.Addresses.ViewModels
{
    public class StreetViewModel
    {
        public string Id { set; get; }
        public string Name { set; get; }
    }
}
