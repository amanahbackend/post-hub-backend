﻿using Amanah.Posthub.BLL.ViewModels.Admin;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Admins.Entities;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IAdminManager : IBaseManager<AdminViewModel, Admin>
    {
        Task<AdminViewModel> GetAsync(string id);
        Task<string> GetTenantByCustomerCodeAsync(string customerCode);
        Task<bool> UpdateProfileAsync(AdminProfileViewModel adminProfileViewModel);
        Task<AdminLanguageViewModel> UpdateLanguageAsync(AdminLanguageViewModel adminLanguageViewModel, string userid);
        Task<bool> SetDeactivationReason(string UserId, string reason);
        Task<AdminDefaultBranchViewModel> UpdateDefaultBranchAsync(AdminDefaultBranchViewModel DefaultbranchViewModel, string userid);
        Task AddTenantAdminAsync(AdminViewModel adminViewModel);
    }
}
