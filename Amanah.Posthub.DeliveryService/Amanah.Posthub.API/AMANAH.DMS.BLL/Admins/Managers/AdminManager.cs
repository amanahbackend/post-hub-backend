﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels.Admin;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Admins.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class AdminManager : BaseManager<AdminViewModel, Admin>, IAdminManager
    {
        private readonly IRepositry<Admin> _repository;

        public AdminManager(ApplicationDbContext context, IRepositry<Admin> repository, IMapper mapper)
            : base(context, repository, mapper)
        {
            _repository = repository;
        }

        public async Task<AdminViewModel> GetAsync(string id)
        {
            var admin = await context.Admins
                .AsNoTracking()
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.Id == id);
            return mapper.Map<Admin, AdminViewModel>(admin);
        }
        public async Task<string> GetTenantByCustomerCodeAsync(string customerCode)
        {
            var admin = await context.Admins.IgnoreQueryFilters().AsNoTracking().FirstOrDefaultAsync(x => x.CustomerCode.Trim() == customerCode.Trim());
            return admin.Tenant_Id;
        }

        public async Task<AdminLanguageViewModel> UpdateLanguageAsync(AdminLanguageViewModel languageViewModel, string userid)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == userid);
            if (admin == null)
                return null;
            admin.DashBoardLanguage = languageViewModel.DashBoardLanguage;
            admin.TrackingPanelLanguage = languageViewModel.TrackingPanelLanguage;
            myContext.Admins.Update(admin);
            var num = await myContext.SaveChangesAsync();
            if (num > 0)
                return languageViewModel;
            return null;
        }

        public async Task<AdminDefaultBranchViewModel> UpdateDefaultBranchAsync(AdminDefaultBranchViewModel DefaultbranchViewModel, string userid)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == userid);
            if (admin == null)
                return null;
            admin.BranchId = DefaultbranchViewModel.DefaultBranchId;
            myContext.Admins.Update(admin);
            var num = await myContext.SaveChangesAsync();
            if (num > 0)
                return DefaultbranchViewModel;
            return null;
        }

        public async Task<bool> UpdateProfileAsync(AdminProfileViewModel adminProfileViewModel)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == adminProfileViewModel.Id);
            admin.CompanyAddress = adminProfileViewModel.CompanyAddress;
            admin.CompanyName = adminProfileViewModel.CompanyName;
            admin.DisplayImage = adminProfileViewModel.DisplayImage;
            myContext.Admins.Update(admin);
            var num = await myContext.SaveChangesAsync();
            if (num > 0)
                return true;
            return false;
        }

        public async Task<bool> SetDeactivationReason(string UserId, string reason)
        {
            var myContext = context as ApplicationDbContext;
            var admin = await myContext.Admins.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == UserId);
            admin.DeactivationReason = reason;
            myContext.Admins.Remove(admin);
            var num = await myContext.SaveChangesAsync();
            if (num > 0)
                return true;
            return false;
        }

        public async Task AddTenantAdminAsync(AdminViewModel adminViewModel)
        {
            Admin admin = new Admin(adminViewModel.Tenant_Id)
            {
                Id = adminViewModel.Id,
                CustomerCode = adminViewModel.CustomerCode,
                CompanyAddress = adminViewModel.CompanyAddress,
                CompanyName = adminViewModel.CompanyName,
                ResidentCountryId = adminViewModel.ResidentCountryId,
                BranchId = adminViewModel.BranchId
            };
            _repository.AddAsync(admin);
        }

    }
}
