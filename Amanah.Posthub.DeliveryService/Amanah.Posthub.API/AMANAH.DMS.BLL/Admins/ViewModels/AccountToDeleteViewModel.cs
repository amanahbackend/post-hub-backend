﻿using Amanah.Posthub.VALIDATOR.Extensions;
using FluentValidation;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.ViewModels.Admin
{
    public class AccountToDeleteViewModel
    {
        public string deactivationReason { get; set; }
        public string password { get; set; }
    }

    public class DeleteAccountValidator : AbstractValidator<AccountToDeleteViewModel>
    {
        public DeleteAccountValidator(ILocalizer localizer)
        {
            RuleFor(x => x.deactivationReason).NotNull().NotEmpty();
            RuleFor(x => x.password).Password(localizer);
        }
    }
}
