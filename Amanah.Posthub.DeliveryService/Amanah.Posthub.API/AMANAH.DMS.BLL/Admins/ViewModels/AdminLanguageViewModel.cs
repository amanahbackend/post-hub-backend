﻿using FluentValidation;

namespace Amanah.Posthub.BLL.ViewModels.Admin
{
    public class AdminLanguageViewModel
    {
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; }
    }

    public class AdminLanguageViewModelValidator : AbstractValidator<AdminLanguageViewModel>
    {
        public AdminLanguageViewModelValidator()
        {
            RuleFor(x => x.DashBoardLanguage).NotEmpty().NotNull();
            RuleFor(x => x.TrackingPanelLanguage).NotEmpty().NotNull();

        }
    }
}
