﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IRestaurantManager : IBaseManager<RestaurantViewModel, Restaurant>
    {
        Task<RestaurantViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> DeleteAsync(int id);
        Task<bool> SetActivate(int id, bool isActive, string reason);
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> Ids);
        Task<PagedResult<RestaurantViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel, List<int> Ids);
        Task<List<int>> GetManagerRestaurantsAsync();
    }
}
