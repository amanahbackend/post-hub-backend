﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers
{

    public class RestaurantManager : BaseManager<RestaurantViewModel, Restaurant>, IRestaurantManager
    {
        private readonly ICurrentUser _currentUser;

        public RestaurantManager(
            ApplicationDbContext context,
            IRepositry<Restaurant> repository,
            IMapper mapper,
            ICurrentUser currentUser)
            : base(context, repository, mapper)
        {
            _currentUser = currentUser;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var resturant = context.Restaurant
                .Include(t => t.Branchs)
                .SingleOrDefault(x => x.Id == id);

            if (resturant == null)
            {
                return false;
            }
            var data = await base.SoftDeleteAsync(mapper.Map<RestaurantViewModel>(resturant));

            await context.SaveChangesAsync();
            return data;

        }


        public async Task<RestaurantViewModel> Get(int id)
        {
            return await context.Restaurant
                .ProjectTo<RestaurantViewModel>(mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            return await context.Restaurant
                .Where(x => x.Id == id)
                .ProjectTo<TQueryModel>(mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
        }

        public async Task<bool> SetActivate(int id, bool isActive, string reason)
        {
            var restaurant = await context.Restaurant
                .Include(t => t.Branchs)
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync();
            if (restaurant == null)
            {
                return false;
            }
            restaurant.IsActive = isActive;
            restaurant.Reason = reason;
            if (!isActive)
                foreach (var t in restaurant.Branchs.ToList())
                {
                    t.IsActivePrevious = t.IsActive;
                    t.IsActive = isActive;
                }
            else
                foreach (var t in restaurant.Branchs.ToList())
                {
                    var prev = t.IsActivePrevious;
                    if (!t.IsActivePrevious)
                    {
                        t.IsActive = prev;
                    }
                    else
                    {
                        t.IsActive = isActive;
                    }
                    t.IsActivePrevious = t.IsActive;

                }
            context.Entry(restaurant);
            await context.SaveChangesAsync();
            return true;
        }

        public Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> Ids)
        {
            var restaurants = repository.GetAll()
                .Where(x => Ids.Contains(x.Id) && x.IsActive == true)
                .ProjectTo<TProjectedModel>(mapper.ConfigurationProvider)
                .ToListAsync();
            return restaurants;
        }

        public Task<PagedResult<RestaurantViewModel>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel, List<int> Ids)
        {
            var pagedResult = repository.GetAll()
                .Where(x => Ids.Contains(x.Id))
                .ToPagedResultAsync<Restaurant, RestaurantViewModel>(pagingparametermodel, mapper.ConfigurationProvider);
            return pagedResult;
        }

        public async Task<List<int>> GetManagerRestaurantsAsync()
        {
            if (string.IsNullOrEmpty(_currentUser.Id))
            {
                return null;
            }
            var restaurants = await context.ManagerDispatching
                .Include(x => x.Manager)
                .Where(x => x.Manager.UserId == _currentUser.Id)
                .Select(x => x.Restaurants)
                .ToListAsync();

            var createdRestaurantsIds = await context.Restaurant
                .Where(x => x.CreatedBy_Id == _currentUser.UserName)
                .Select(x => x.Id).ToListAsync();

            var restaurantsIds = new List<int>();
            if (restaurants != null && restaurants.Any())
            {
                foreach (var r in restaurants)
                {
                    if (r.Trim() == "All")
                    {

                        restaurantsIds = await context.Restaurant
                           .Select(x => x.Id)
                           .ToListAsync();
                        break;
                    }
                    else
                    {
                        restaurantsIds.AddRange(r.Split(',').Select(s => int.Parse(s)));
                    }
                }
            }

            createdRestaurantsIds.ForEach(x =>
            {
                if (restaurantsIds.Where(y => y == x).Count() == 0)
                {
                    restaurantsIds.Add(x);
                }
            });

            return restaurantsIds;
        }
    }
}
