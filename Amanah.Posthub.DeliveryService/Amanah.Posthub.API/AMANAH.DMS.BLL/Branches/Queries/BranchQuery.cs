﻿using Amanah.Posthub.BLL.Branches.Queries.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.Branches.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.Branches.Queries.DTOs.Express;
using Amanah.Posthub.DeliverService.BLL.DriverFloor.Queries;
using Amanah.Posthub.DeliverService.BLL.DriverFloor.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Branches.Queries
{
    public interface IBranchQuery
    {
        Task<List<BranchResponseDTO>> GetAllAsync(List<int> branchIds = null);
        Task<List<BranchResponseDTO>> GetAllBranchesHaveMailItemAsync(List<int> branchIds = null);
        Task<PagedResult<BranchResponseDTO>> GetAllByPaginationAsync(BranchFilterDTO filter, List<int> branchIds = null);
        Task<List<BranchResponseDTO>> GetRestaurantBranchesAsync(int resturantId, List<int> branchIds = null);
        Task<List<BranchResponseDTO>> GetBranchesByAsync(int[] resturantIds, int[] geoFenceIds, List<int> branchIds = null);
        Task<List<BranchResponseDTO>> GetBranchesByNameAsync(string name, List<int> branchIds = null);
        Task<BranchResponseDTO> GetAsync(int id);
        Task<BranchResponseForExpressDTO> GetExpressAsync(int id);
        Task<List<BranchResponseDTO>> GetCompanyBranchesAsync(int companyId, List<int> branchIds = null);
        Task<List<ExportBranchResponseDTO>> ExportCompanyBranchesAsync(int companyId, List<int> branchIds = null);
        Task<List<FloorListResponseDTO>> GetDriverFloorsByBranchId(DriverFloorsByBranchIdDto query);


    }

    internal class BranchQuery : IBranchQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IDriverFloorQuery _driverFloorQuery;

        public BranchQuery(
            ApplicationDbContext context,
            IMapper mapper,
            IDriverFloorQuery driverFloorQuery)
        {
            this.context = context;
            this.mapper = mapper;
            _driverFloorQuery = driverFloorQuery;
        }

        public async Task<List<BranchResponseDTO>> GetAllAsync(List<int> branchIds = null)
        {
            return await context.Branch
                .Include(branch => branch.Restaurant)
                .IgnoreQueryFilters()
                .Where(branch => (branchIds != null && branchIds.Contains(branch.Id))
                    || (branchIds == null)
                    && branch.IsActive == true)
                .IgnoreQueryFilters()
                .ProjectTo<BranchResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        public async Task<List<BranchResponseDTO>> GetAllBranchesHaveMailItemAsync(List<int> branchIds = null)
        {
           var  mailItems = await context.MailItems
                   .Where(x => ! x.IsDeleted &&  x.StatusId == 1)
                   .Include(c => c.ConsigneeInfo)
                   .Include(c => c.Workorder)
                   .Select(c=> new {c.ConsigneeInfo.BranchName, c.ConsigneeInfo.BranchId})
                   .ToListAsync();

            return await context.Branch
                .Include(branch => branch.Restaurant)
                .Where(branch => (branchIds != null && branchIds.Contains(branch.Id))
                    || (branchIds == null)
                    && branch.IsActive == true && (mailItems.Any()&&( mailItems.Select(x=>x.BranchId).Contains(branch.Id) || mailItems.Select(x => x.BranchName).Contains(branch.Name))))
                .IgnoreQueryFilters()
                .ProjectTo<BranchResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        
        public Task<PagedResult<BranchResponseDTO>> GetAllByPaginationAsync(
            BranchFilterDTO filter,
            List<int> branchIds = null)
        {
            var query = context.Branch.Where ( c => !c.IsDeleted).IgnoreQueryFilters().OrderByDescending(c => c.Id).AsQueryable();
            // .Where(branch => branch.IsActive == true);

            if (branchIds != null && branchIds.Any())
            {
                query = query.Where(branch => branchIds.Contains(branch.Id) && !branch.IsDeleted);
            }

            //if (filter.RestaurantId.HasValue && filter.RestaurantId.Value > 0)
            //{
            //    query = query.Where(branch => branch.Restaurant.IsActive == true
            //        && branch.RestaurantId == filter.RestaurantId);
            //}
            if (filter.CompanyId.HasValue && filter.CompanyId.Value > 0)
            {
                query = query.Where(branch =>
                    branch.CompanyId == filter.CompanyId);
            }
            query = query.Where(branch =>
           string.IsNullOrWhiteSpace(filter.SearchBy) ||
               branch.Name.Contains(filter.SearchBy) ||
               branch.Mobile.Contains(filter.SearchBy) ||
               branch.Phone.Contains(filter.SearchBy)

            ); ;

            return query
                .ToPagedResultAsync<Branch, BranchResponseDTO>(filter, mapper.ConfigurationProvider);
        }

        public async Task<List<BranchResponseDTO>> GetRestaurantBranchesAsync(
            int resturantId,
            List<int> branchIds = null)
        {
            var query = context.Branch.IgnoreQueryFilters().Where(branch => branch.RestaurantId == resturantId);
            if (branchIds != null && branchIds.Any())
            {
                query = query.Where(branch => branchIds.Contains(branch.Id));
            }

            return await query
                .ProjectTo<BranchResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }


        public async Task<List<BranchResponseDTO>> GetBranchesByAsync(
            int[] resturantIds,
            int[] geoFenceIds,
            List<int> branchIds = null)
        {
            var query = context.Branch.IgnoreQueryFilters().AsQueryable();
            //.Where(branch => branch.IsActive);
            if (branchIds != null && branchIds.Any())
            {
                query = query.Where(branch => branchIds.Contains(branch.Id));
            }
            if (resturantIds != null)
            {
                query = query.Where(branch => resturantIds.Contains(branch.RestaurantId.Value));
            }
            if (geoFenceIds != null)
            {
                query = query.Where(branch => geoFenceIds.Contains(branch.GeoFenceId.Value));
            }

            return await query
                .ProjectTo<BranchResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<BranchResponseDTO>> GetBranchesByNameAsync(
            string name,
            List<int> branchIds = null)
        {
            var query = context.Branch
                .IgnoreQueryFilters()
                .Where(branch => branch.IsActive
                    && branch.Restaurant != null && branch.Restaurant.IsActive
                    && branch.Name.ToLower().Contains(name.ToLower()));
            if (branchIds != null && branchIds.Any())
            {
                query = query.Where(branch => branchIds.Contains(branch.Id));
            }
            return await query
                .Take(10)
                .ProjectTo<BranchResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<BranchResponseDTO> GetAsync(int id)
        {
            return await context.Branch
                .IgnoreQueryFilters()
                .Where(branch => branch.Id == id)
                .ProjectTo<BranchResponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<BranchResponseForExpressDTO> GetExpressAsync(int id)
        {
            return await context.Branch
                .IgnoreQueryFilters()
                .Where(branch => branch.Id == id)
                .ProjectTo<BranchResponseForExpressDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<List<BranchResponseDTO>> GetCompanyBranchesAsync(int companyId, List<int> branchIds = null)
        {

            var query = context.Branch.IgnoreQueryFilters().Where(branch => branch.CompanyId == companyId);
            if (branchIds != null && branchIds.Any())
            {
                query = query.Where(branch => branchIds.Contains(branch.Id));
            }

            return await query
                .ProjectTo<BranchResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<ExportBranchResponseDTO>> ExportCompanyBranchesAsync(int companyId, List<int> branchIds = null)
        {

            var query = context.Branch.IgnoreQueryFilters().Where(branch => branch.CompanyId == companyId);
            if (branchIds != null && branchIds.Any())
            {
                query = query.Where(branch => branchIds.Contains(branch.Id));
            }


            return await query
                .ProjectTo<ExportBranchResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<FloorListResponseDTO>> GetDriverFloorsByBranchId(DriverFloorsByBranchIdDto query)
        {
            List<FloorListResponseDTO> floors = new List<FloorListResponseDTO>();
            List<string> floorList = new List<string>();

            var floorsCount = await context.Branch.Where(b => b.Id == query.branchId && !b.IsDeleted)
                .ProjectTo<FloorCountResponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            for (int i = 1; i <= floorsCount.FloorCount; i++)
            {
                floors.Add(new FloorListResponseDTO { Floor = i.ToString() });
            }

            //var driverFloors = await context.DriverFloors.Where(c => c.BranchId == branchId && !c.Driver.IsDeleted)
            // .ProjectTo<DriverFloorResponseDTO>(mapper.ConfigurationProvider)
            // .ToListAsync();
            var driverFloorsToBeExcluded = await _driverFloorQuery.GetDriverFloors(query.branchId, query.driverId);

            if (driverFloorsToBeExcluded.Any())
            {
                floorList = driverFloorsToBeExcluded.Select(c => c.Floor).ToList();
                floors = floors.Where(f => !floorList.Contains(f.Floor)).Select(c => c).ToList();
            }

            return floors;

        }
    }
}
