﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace Amanah.Posthub.BLL.Branches.Queries.DTOs
{
    public class BranchFilterDTO : PaginatedItemsViewModel
    {
        public int? CompanyId { get; set; }
        public int? RestaurantId { get; set; }
    }
}
