﻿using System;

namespace Amanah.Posthub.BLL.Branches.Queries.DTOs
{
    public class BranchResponseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Code { get; set; }

        public int RestaurantId { get; set; }
        public int GeoFenceId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool IsActivePrevious { get; set; }
        public string Reason { get; set; }

        public string BranchManagerId { get; set; }
        public string BranchManagerName { get; set; }
        public int FloorsCount { get; set; }

        public BranchCustomerResponseDTO Customer { set; get; }
        public BranchRestaurantResponseDTO Restaurant { get; set; }
        public BranchAddressResponseDTO Location { set; get; }

        public string Tenant_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy_Id { set; get; }

    }

    public class BranchCustomerResponseDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public string Tags { set; get; }
        public int? CountryId { set; get; }
        public int? BranchId { set; get; }

        public BranchCustomerCountryResponseDTO Country { get; set; }
        public BranchAddressResponseDTO Location { set; get; }

    }
    public class BranchCustomerCountryResponseDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Code { set; get; }
        public string Flag { set; get; }
        public string FlagUrl { set; get; }
        public string TopLevel { set; get; }
    }
    public class BranchAddressResponseDTO
    {
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }

    }
    public class BranchRestaurantResponseDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
