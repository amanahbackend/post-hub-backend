﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Branches.Queries.DTOs
{
    public class DriverFloorsByBranchIdDto
    {
        public int branchId { get; set; }
        public int? driverId { get; set; }
    }
}
