﻿namespace Amanah.Posthub.BLL.Branches.Queries.DTOs
{
    public class ExportBranchResponseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }

}
