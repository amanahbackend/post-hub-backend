﻿using Amanah.Posthub.BLL.Branches.Queries.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Branches.Queries.DTOs.Express
{
    public class BranchResponseForExpressDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Code { get; set; }

        public int RestaurantId { get; set; }
        public int GeoFenceId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool IsActivePrevious { get; set; }
        public string Reason { get; set; }

        public string BranchManagerId { get; set; }
        public string BranchManagerName { get; set; }

        public BranchCustomerResponseDTO Customer { set; get; }
        public BranchRestaurantResponseDTO Restaurant { get; set; }
        public BranchAddressResponseDTO Location { set; get; }

        public string Tenant_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string CreatedBy_Id { set; get; }

    }
}
