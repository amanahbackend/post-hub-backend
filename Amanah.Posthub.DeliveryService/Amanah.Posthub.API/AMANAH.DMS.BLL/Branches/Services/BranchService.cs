﻿using Amanah.Posthub.BLL.Branches.Queries.DTOs;
using Amanah.Posthub.BLL.Branches.Services.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Branches.Queries.DTOs.Express;
using Amanah.Posthub.DeliverService.BLL.Branches.Services.DTOs.Express;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Branches.Services
{
    public interface IBranchService
    {
        Task<BranchResponseDTO> CreateAsync(CreateBranchInputDTO createdBranch);
        Task<BranchResponseForExpressDTO> CreateExpressAsync(CreateBranchForExpressInputDTO createdBranch); 
        Task UpdateExpressAsync(UpdateBranchForExpressInputDTO updatedBranch); 
        Task UpdateAsync(UpdateBranchInputDTO updatedBranch);
        Task DeleteAsync(int id);
        Task<bool> ActivateDeactivateAsync(int id, bool isActive, string Reason);
    }
    internal class BranchService : IBranchService
    {
        private readonly IRepository<Branch> branchRepository;
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly ApplicationDbContext context;
        private readonly ILocalizer localizer;

        public BranchService(IRepository<Branch> branchRepository,
            IRepository<Customer> customerRepository,
            IRepository<AccountLogs> accountLogRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
             ApplicationDbContext context,
             ILocalizer localizer)
        {
            this.branchRepository = branchRepository;
            this.customerRepository = customerRepository;
            this.accountLogRepository = accountLogRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.context = context;
            this.localizer = localizer;
        }

        public async Task<BranchResponseDTO> CreateAsync(CreateBranchInputDTO createdBranch)
        {
            // avoid duplicate
            var isBranchExist = await branchRepository.AnyAsync(a => !a.IsDeleted && a.Name == createdBranch.Name);

            if (isBranchExist)
                throw new DomainException(localizer[Keys.Messages.BranchExist]);

            Branch branch = mapper.Map<Branch>(createdBranch);
            branch.Customer = mapper.Map<Customer>(createdBranch);

            await unitOfWork.RunTransaction(async () =>
            {

                    branchRepository.Add(branch);
                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Branch",
                        ActivityType = "Create",
                        Description = $"Branch {branch.Name}  has been created ",
                        Record_Id = branch.Id
                    });
                    await unitOfWork.SaveChangesAsync();

                
            });

            return mapper.Map<BranchResponseDTO>(branch);
        }

        public async Task<BranchResponseForExpressDTO> CreateExpressAsync(CreateBranchForExpressInputDTO createdBranch)
        {
            // avoid duplicate
            var isBranchExist = await branchRepository.AnyAsync(a => !a.IsDeleted && a.Name == createdBranch.Name);

            if (isBranchExist)
                throw new DomainException(localizer[Keys.Messages.BranchExist]);

            Branch branch = mapper.Map<Branch>(createdBranch);
            branch.Customer = mapper.Map<Customer>(createdBranch);

            await unitOfWork.RunTransaction(async () =>
            {
                branchRepository.Add(branch);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Branch",
                    ActivityType = "Create",
                    Description = $"Branch {branch.Name}  has been created ",
                    Record_Id = branch.Id
                });

                await unitOfWork.SaveChangesAsync();
            });

            return mapper.Map<BranchResponseForExpressDTO>(branch);
        }

        public async Task UpdateAsync(UpdateBranchInputDTO updatedBranch)
        {
            // avoid duplicate
          
            var existedBranch = await branchRepository.GetFirstOrDefaultAsync(branch => branch.Id == updatedBranch.Id);
            if (existedBranch == null)
            {
                return;
            }
            var isBranchExist = await branchRepository.AnyAsync(a => !a.IsDeleted && a.Id != updatedBranch.Id && a.Name == updatedBranch.Name);

            if (isBranchExist)
                throw new DomainException(localizer[Keys.Messages.BranchExist]);

            var existingCustomer = await customerRepository.GetFirstOrDefaultAsync(customer => customer.BranchId == updatedBranch.Id);
            existingCustomer = mapper.Map(updatedBranch, existingCustomer);
            existedBranch = mapper.Map(updatedBranch, existedBranch);
            await unitOfWork.RunTransaction(async () =>
            {
               
                    customerRepository.Update(existingCustomer);
                    branchRepository.Update(existedBranch);
                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Branch",
                        ActivityType = "Update",
                        Description = $"Branch { existedBranch.Name} has been updated ",
                        Record_Id = existedBranch.Id
                    });

                    await unitOfWork.SaveChangesAsync();

            });
        }

        public async Task UpdateExpressAsync(UpdateBranchForExpressInputDTO updatedBranch)
        {
            var existedBranch = await branchRepository.GetFirstOrDefaultAsync(branch => branch.Id == updatedBranch.Id);
            if (existedBranch == null)
            {
                return;
            }
            var isBranchExist = await branchRepository.AnyAsync(a => !a.IsDeleted && a.Id != updatedBranch.Id && a.Name == updatedBranch.Name);

            if (isBranchExist)
                throw new DomainException(localizer[Keys.Messages.BranchExist]);

            var existingCustomer = await customerRepository.GetFirstOrDefaultAsync(customer => customer.BranchId == updatedBranch.Id);
            existingCustomer = mapper.Map(updatedBranch, existingCustomer);
            existedBranch = mapper.Map(updatedBranch, existedBranch);
            await unitOfWork.RunTransaction(async () =>
            {
                customerRepository.Update(existingCustomer);
                branchRepository.Update(existedBranch);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Branch",
                    ActivityType = "Update",
                    Description = $"Branch { existedBranch.Name} has been updated ",
                    Record_Id = existedBranch.Id
                });

                await unitOfWork.SaveChangesAsync();
            });
        }

        public async Task DeleteAsync(int id)
        {
            var existedBranch = await branchRepository.GetFirstOrDefaultAsync(branch => branch.Id == id,
             branch => branch.Customer);
            if (existedBranch == null)
            {
                return;
            }
            await unitOfWork.RunTransaction(async () =>
            {
                existedBranch.Customer.SetIsDeleted(true);
                existedBranch.SetIsDeleted(true);
                customerRepository.Update(existedBranch.Customer);
                branchRepository.Update(existedBranch);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Branch",
                    ActivityType = "Delete",
                    Description = $"Branch { existedBranch.Name} has been deleted ",
                    Record_Id = existedBranch.Id
                });

                await unitOfWork.SaveChangesAsync();
            });
        }

        public async Task<bool> ActivateDeactivateAsync(int id, bool isActive, string Reason)
        {
            var existedBranch = await branchRepository.GetFirstOrDefaultAsync(branch => branch.Id == id,
              branch => branch.Restaurant);
            if (existedBranch == null
                || existedBranch.Restaurant == null
                || !existedBranch.Restaurant.IsActive)
            {
                return false;
            }
            await unitOfWork.RunTransaction(async () =>
            {
                existedBranch.IsActivePrevious = existedBranch.IsActive;
                existedBranch.IsActive = isActive;
                existedBranch.Reason = Reason;
                branchRepository.Update(existedBranch);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Branch",
                    ActivityType = "SetActivate",
                    Description = isActive
                    ? $"Branch {existedBranch.Name } has been activated "
                    : $"Branch {existedBranch.Name } has been deactivated ",
                    Record_Id = existedBranch.Id
                });

                await unitOfWork.SaveChangesAsync();
            });

            return true;
        }

    }
}
