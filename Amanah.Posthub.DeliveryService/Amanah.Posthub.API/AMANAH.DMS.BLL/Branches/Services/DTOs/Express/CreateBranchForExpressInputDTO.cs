﻿using Amanah.Posthub.BLL.Branches.Services.DTOs;
using FluentValidation;

namespace Amanah.Posthub.DeliverService.BLL.Branches.Services.DTOs.Express
{
    public class CreateBranchForExpressInputDTO
    {
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Code { get; set; }


        public int CompanyId { get; set; }
        public int? RestaurantId { get; set; }
        public int? GeoFenceId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool IsActive { get; set; } = true;
        //public bool IsActivePrevious { get; set; }
        //public string Reason { get; set; }
        public string BranchManagerId { get; set; }

        public CreateBranchAddressInputDTO Location { set; get; }

        public class CreateBranchInputDTOValidation : AbstractValidator<CreateBranchForExpressInputDTO>
        {
            public CreateBranchInputDTOValidation()
            {
                RuleFor(branch => branch.Name).NotEmpty().NotNull().MaximumLength(500);
                RuleFor(branch => branch.Phone).NotEmpty().NotNull().MaximumLength(50);
                RuleFor(branch => branch.CountryId).NotEmpty().NotNull();
                RuleFor(branch => branch.Address).NotEmpty().NotNull().MaximumLength(1000);
                RuleFor(branch => branch.Latitude).NotNull();
                RuleFor(branch => branch.Longitude).NotNull();
            }
        }
    }


}
