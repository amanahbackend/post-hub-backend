﻿using Amanah.Posthub.BLL.ViewModel;
using FluentValidation;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class BranchViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public string Phone { get; set; }
        public int RestaurantId { get; set; }
        public int GeoFenceId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        //public int ManagerId { get; set; }
        public bool IsActive { get; set; }
        public bool IsActivePrevious { get; set; }
        public string Reason { get; set; }
        //public List<CustomerViewModel> Customers { set; get; }
        public CustomerResultDto Customer { set; get; }
        //public virtual GeoFence GeoFence { get; set; }
        public LkpResultDto Restaurant { get; set; }
        public AddressResultDto Location { set; get; }

    }

    public class BranchViewModelValidation : AbstractValidator<BranchViewModel>
    {
        public BranchViewModelValidation()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
            //RuleFor(x => x.Phone).NotEmpty().NotNull().MaximumLength(50);
            //RuleFor(x => x.RestaurantId).NotEmpty().NotNull();
            //RuleFor(x => x.GeoFenceId).NotEmpty().NotNull();
            //RuleFor(x => x.Address).NotEmpty().NotNull().MaximumLength(200);
            //RuleFor(x => x.Latitude).NotEmpty().NotNull().MaximumLength(200);
            //RuleFor(x => x.Longitude).NotEmpty().NotNull().MaximumLength(200);
            //RuleFor(x => x.ManagerId).NotEmpty().NotNull();


        }


    }
}