﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class CreateRestaurantViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
