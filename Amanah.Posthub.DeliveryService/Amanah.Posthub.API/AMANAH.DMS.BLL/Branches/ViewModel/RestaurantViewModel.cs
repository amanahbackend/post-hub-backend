﻿using Amanah.Posthub.BLL.ViewModel;
using FluentValidation;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class RestaurantViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Reason { get; set; }

    }

    public class RestaurantViewModelValidation : AbstractValidator<RestaurantViewModel>
    {
        public RestaurantViewModelValidation()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
        }

    }
}
