﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.BusinessContactAccounts.Queries
{
    public interface IBusinessContactAccountQuery
    {
        Task<BusinessContactAccountListResponseDTO> GetByIdAsync(int id);
        Task<List<BusinessContactAccountListResponseDTO>> GetAllAsync();
        Task<PagedResult<BusinessContactAccountListResponseDTO>> GetAllByPaginationAsync(PaginatedItemsViewModel filter);
        Task<PagedResult<BusinessContactAccountListResponseDTO>> GetAllByBusinssCustomerPaginationAsync(PaginatedItemsViewModel filter);
        Task<List<BusinessContactAccountListResponseDTO>> GetAllByBussinessCutomerAsync(int businessCustomerId);


    }

    public class BusinessContactAccountQuery : IBusinessContactAccountQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<BusinessContactAccount> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public BusinessContactAccountQuery
            (
            IRepository<BusinessContactAccount> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<BusinessContactAccountListResponseDTO>> GetAllAsync()
        {
            var bcustomerlist = await context.BusinessConactAccounts
                      .AsNoTracking().IgnoreQueryFilters()
                      .Where(x =>  ! x.IsDeleted)
                      .ProjectTo<BusinessContactAccountListResponseDTO>(mapper.ConfigurationProvider)
                      .ToListAsync();
            return bcustomerlist;
        }

        public async Task<PagedResult<BusinessContactAccountListResponseDTO>> GetAllByBusinssCustomerPaginationAsync(PaginatedItemsViewModel filter)
        {
            var bcustomerlist = await context.BusinessConactAccounts
                                .IgnoreQueryFilters()
                                .OrderByDescending(x=>x.Id)
                                .Where(x => x.BusinessCustomerId == filter.Id && ! x.IsDeleted )
                                .ProjectTo<BusinessContactAccountListResponseDTO>(mapper.ConfigurationProvider)
                                .ToPagedResultAsync(filter);
            return bcustomerlist;
        }

        public async Task<List<BusinessContactAccountListResponseDTO>> GetAllByBussinessCutomerAsync(int businessCustomerId)
        {
            var bcustomerlist = await context.BusinessConactAccounts
                                .Where(x => x.BusinessCustomerId == businessCustomerId && ! x.IsDeleted)
                                .AsNoTracking().IgnoreQueryFilters()
                                .OrderByDescending(c=>c.Id)
                                .ProjectTo<BusinessContactAccountListResponseDTO>(mapper.ConfigurationProvider)
                                .ToListAsync();
            return bcustomerlist;
        }

        public async Task<PagedResult<BusinessContactAccountListResponseDTO>> GetAllByPaginationAsync(PaginatedItemsViewModel filter)
        {
            var bcustomerlist = await context.BusinessConactAccounts.IgnoreQueryFilters()
                                 .Where(x => ! x.IsDeleted)
                                .ProjectTo<BusinessContactAccountListResponseDTO>(mapper.ConfigurationProvider)
                                .ToPagedResultAsync(filter);
            return bcustomerlist;
        }

        public async Task<BusinessContactAccountListResponseDTO> GetByIdAsync(int id)
        {
            var request = await context.BusinessConactAccounts
                .Where(b => b.Id == id)
                .AsNoTracking().IgnoreQueryFilters()
                .ProjectTo<BusinessContactAccountListResponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            return request;
        }

    }
}
