﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.BusinessCustomerBranchs.Queries
{
    public interface IBusinessCustomerBranchQuery
    {
        Task<BusinessCustomerBranchReponseDTO> GetByIdAsync(int id);
        Task<List<BusinessCustomerBranchReponseDTO>> GetAllAsync();
        Task<PagedResult<BusinessCustomerBranchReponseDTO>> GetAllByPaginationAsync(PaginatedItemsViewModel filter);
        Task<PagedResult<BusinessCustomerBranchReponseDTO>> GetAllByBusinssCustomerPaginationAsync(PaginatedItemsViewModel filter);
        Task<List<BusinessCustomerBranchReponseDTO>> GetAllByBussinessCutomerAsync(int businessCustomerId);
        Task<List<BusinessCustomerBranchReponseDTO>> GetAllByBussinessCutomerUserIdAsync(string businessCustomerUserId);

    }

    public class BusinessCustomerBranchQuery : IBusinessCustomerBranchQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<BusinessCustomerBranch> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;



        public BusinessCustomerBranchQuery
            (
            IRepository<BusinessCustomerBranch> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<BusinessCustomerBranchReponseDTO>> GetAllAsync()
        {
            var bcustomerlist = await context.BusinessCustomerBranchs
                      .AsNoTracking().IgnoreQueryFilters()
                      .ProjectTo<BusinessCustomerBranchReponseDTO>(mapper.ConfigurationProvider)
                      .ToListAsync();
            return bcustomerlist;
        }

        public async Task<PagedResult<BusinessCustomerBranchReponseDTO>> GetAllByBusinssCustomerPaginationAsync(PaginatedItemsViewModel filter)
        {
            var bcustomerlist = await context.BusinessCustomerBranchs
                                .Where(x => x.BusinessCustomerId == filter.Id)
                                .AsNoTracking().IgnoreQueryFilters()
                                .ProjectTo<BusinessCustomerBranchReponseDTO>(mapper.ConfigurationProvider)
                                .ToPagedResultAsync(filter);
            return bcustomerlist;
        }

        public async Task<List<BusinessCustomerBranchReponseDTO>> GetAllByBussinessCutomerAsync(int businessCustomerId)
        {

            var bcustomerlist = await context.BusinessCustomerBranchs
                      .Where(x => x.BusinessCustomerId == businessCustomerId && !x.IsDeleted)
                      .AsNoTracking().IgnoreQueryFilters()
                      .OrderByDescending(c => c.Id)
                      .ProjectTo<BusinessCustomerBranchReponseDTO>(mapper.ConfigurationProvider)
                      .ToListAsync();
            return bcustomerlist;
        }
        public async Task<List<BusinessCustomerBranchReponseDTO>> GetAllByBussinessCutomerUserIdAsync(string businessCustomerUserId)
        {
            var bcustomerId = await context.BusinessConactAccounts
                      .Where(x => x.UserID == businessCustomerUserId).FirstOrDefaultAsync();

            var bcustomerlist = await context.BusinessCustomerBranchs
                      .Where(x => x.BusinessCustomerId == bcustomerId.BusinessCustomerId)
                      .AsNoTracking()
                      .IgnoreQueryFilters()
                      .OrderByDescending(c => c.Id)
                      .ProjectTo<BusinessCustomerBranchReponseDTO>(mapper.ConfigurationProvider)
                      .ToListAsync();

            return bcustomerlist;
        }

        public async Task<PagedResult<BusinessCustomerBranchReponseDTO>> GetAllByPaginationAsync(PaginatedItemsViewModel filter)
        {
            var bcustomerlist = await context.BusinessCustomerBranchs
                                .AsNoTracking().IgnoreQueryFilters()
                                .ProjectTo<BusinessCustomerBranchReponseDTO>(mapper.ConfigurationProvider)
                                .ToPagedResultAsync(filter);
            return bcustomerlist;
        }

        public async Task<BusinessCustomerBranchReponseDTO> GetByIdAsync(int id)
        {
            var request = await context.BusinessCustomerBranchs
                .Where(b => b.Id == id)
                .AsNoTracking().IgnoreQueryFilters()
                .ProjectTo<BusinessCustomerBranchReponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            return request;
        }

    }
}
