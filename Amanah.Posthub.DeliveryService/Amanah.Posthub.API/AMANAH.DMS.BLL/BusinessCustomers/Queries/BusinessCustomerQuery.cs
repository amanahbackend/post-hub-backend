﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.BusinessCustomers.Queries
{
    public interface IBusinessCustomerQuery
    {
        Task<BusinessCustomerDetailsResponseDTO> GetByIdAsync(int id);
        Task<List<BusinessCustomerListResponseDTO>> GetAllAsync();
        Task<PagedResult<BusinessCustomerListResponseDTO>> GetAllByPaginationAsync(GeBusinessCustomertPagginatedInputDTO filter);
        Task<List<ExportBusinessCustomerListResponseDTO>> GetExportBCustomersAsync(GeBusinessCustomertPagginatedInputDTO filter);
        Task<List<BusinessCustomerListBusinessIndustryResponseDTO>> GetAllByBussinesTypeAsync(int bussinessTypeId);
        //Task<BusinessCustomerDetailsResponseDTO> GetBusinessCustomerCounts(int id);
        Task<int> GetBussinessCutomerCountAsync();
        Task<int> GetBussinessCustomerIdAsync();

    }

    public class BusinessCustomerQuery : IBusinessCustomerQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<BusinessCustomer> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public BusinessCustomerQuery
            (
            IRepository<BusinessCustomer> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<BusinessCustomerListResponseDTO>> GetAllAsync()
        {
            var bcustomerlist = await context.BusinessCustomers
                      .Where(x => x.IsCompleted && x.IsDeleted == false)
                      .AsNoTracking().IgnoreQueryFilters()
                      .ProjectTo<BusinessCustomerListResponseDTO>(mapper.ConfigurationProvider)
                      .ToListAsync();
            return bcustomerlist;
        }

        public async Task<List<BusinessCustomerListBusinessIndustryResponseDTO>> GetAllByBussinesTypeAsync(int bussinessTypeId)
        {
            var bussinssList = await context.BusinessCustomers.IgnoreQueryFilters()
                .Where(b => b.BusinessTypeId == bussinessTypeId
                && b.IsDeleted == false
                && b.IsCompleted
                &&
                      (!_currentUser.IsBusinessCustomer()
                           || (b.Id == _currentUser.BusinessCustomerId)
                      ))
                .AsNoTracking().IgnoreQueryFilters()
                .ProjectTo<BusinessCustomerListBusinessIndustryResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return bussinssList;
        }

        public async Task<PagedResult<BusinessCustomerListResponseDTO>> GetAllByPaginationAsync(GeBusinessCustomertPagginatedInputDTO filter)
        {
            var bcustomerlist = await context.BusinessCustomers
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .OrderByDescending(c=>c.Id)
                 .Where(ApplyFilter(filter))
                 .Where(b => b.IsCompleted == true && b.IsDeleted == false && (filter.IsApproved == null || b.IsApproved == filter.IsApproved))
                .ProjectTo<BusinessCustomerListResponseDTO>(mapper.ConfigurationProvider)
                .ToPagedResultAsync(filter);
            return bcustomerlist;
        }

        public async Task<BusinessCustomerDetailsResponseDTO> GetByIdAsync(int id)
        {
            var request = await context.BusinessCustomers
                .Where(b => b.Id == id)
                .AsNoTracking().IgnoreQueryFilters()
                .ProjectTo<BusinessCustomerDetailsResponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            return request;
        }

        public async Task<List<ExportBusinessCustomerListResponseDTO>> GetExportBCustomersAsync(GeBusinessCustomertPagginatedInputDTO filter)
        {
            var bcustomerlist = await context.BusinessCustomers
                .Where(x => x.IsCompleted && x.IsDeleted == false)
                .AsNoTracking().IgnoreQueryFilters()
                .Where(ApplyFilter(filter))
                 .Where(b => b.IsCompleted == true && b.IsDeleted == false && (filter.IsApproved == null || b.IsApproved == filter.IsApproved))
                .ProjectTo<ExportBusinessCustomerListResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return bcustomerlist;
        }

        private Expression<Func<BusinessCustomer, bool>> ApplyFilter(PaginatedItemsViewModel bCustomerFilterDTO)
        {
            return bcustomer =>
               (string.IsNullOrWhiteSpace(bCustomerFilterDTO.SearchBy) ||
                bcustomer.BusinessName.Contains(bCustomerFilterDTO.SearchBy) ||
                bcustomer.BusinessCode.Contains(bCustomerFilterDTO.SearchBy) ||
                bcustomer.BusinessCID.Contains(bCustomerFilterDTO.SearchBy));

        }

        public async Task<int> GetBussinessCutomerCountAsync()
        {
            var count = context.BusinessCustomers
                .Where(b => !b.IsDeleted && b.IsCompleted == true).Count();

            return count;
        }

        public async Task<int> GetBussinessCustomerIdAsync()
        {
            var businessCustomer = await context.BusinessCustomers.IgnoreQueryFilters()
                                    .SingleOrDefaultAsync(x => x.Id == _currentUser.BusinessCustomerId);

            return businessCustomer.Id;
        }
    }
}
