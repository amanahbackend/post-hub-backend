﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries
{
    public interface ICollectionMethodQuery
    {
        Task<List<CollectionMethodResponseDTO>> GetAllAsync();
    }

    public class CollectionMethodQuery : ICollectionMethodQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<CollectionMethod> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public CollectionMethodQuery
            (
            IRepository<CollectionMethod> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<CollectionMethodResponseDTO>> GetAllAsync()
        {

            var request = await context.CollectionMethods
                .AsNoTracking()
                .IgnoreQueryFilters()
                .ProjectTo<CollectionMethodResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return request;
        }



    }

}
