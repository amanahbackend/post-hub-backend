﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries
{
    public interface IContactFunctionQuery
    {
        Task<List<ContactFunctionResponseDTO>> GetAllAsync();
    }

    public class ContactFunctionQuery : IContactFunctionQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<ContactFunction> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public ContactFunctionQuery
            (
            IRepository<ContactFunction> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<ContactFunctionResponseDTO>> GetAllAsync()
        {

            var request = await context.ContactFunctions
                .AsNoTracking()
                .IgnoreQueryFilters()
                .ProjectTo<ContactFunctionResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return request;
        }



    }

}
