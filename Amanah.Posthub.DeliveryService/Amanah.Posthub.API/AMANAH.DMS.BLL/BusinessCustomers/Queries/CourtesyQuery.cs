﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries
{
    public interface ICourtesyQuery
    {
        Task<List<CourtesyResponseDTO>> GetAllAsync();
    }

    public class CourtesyQuery : ICourtesyQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<Courtesy> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public CourtesyQuery
            (
            IRepository<Courtesy> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<CourtesyResponseDTO>> GetAllAsync()
        {

            var request = await context.Courtesys
                .AsNoTracking()
                .IgnoreQueryFilters()
                .ProjectTo<CourtesyResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return request;
        }



    }

}
