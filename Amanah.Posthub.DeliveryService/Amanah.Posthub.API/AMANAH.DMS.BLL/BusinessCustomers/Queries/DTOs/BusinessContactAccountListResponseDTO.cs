﻿namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs
{
    public class BusinessContactAccountListResponseDTO
    {

        public int Id { set; get; }
        public string FullName { set; get; }
        public string UserName { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }

        ///Business-Account Contacts
        public string UserID { set; get; }
        public string ContactCode { set; get; }
        public int ContactFunctionId { set; get; }
        public string ContactFunctionName { set; get; }

        public int CourtesyId { set; get; }
        public string CourtesyName { set; get; }

        public int AccountStatusId { get; set; }
        public string AccountStatusName { get; set; }
        public string PersonalPhotoURL { get; set; }
        public int? CountryId { set; get; }
        public string MobileNumber { get; set; }
        public string Fax { get; set; }
        public int BusinessCustomerId { set; get; }
        public int BusinessCustomerBranchId { set; get; }
        public string BusinessCustomerBranchName { set; get; }

    }
}
