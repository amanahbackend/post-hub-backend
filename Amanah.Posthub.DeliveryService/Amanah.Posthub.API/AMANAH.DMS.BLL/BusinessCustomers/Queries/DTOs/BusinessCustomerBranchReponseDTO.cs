﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs
{
    public class BusinessCustomerBranchReponseDTO
    {
        public int Id { set; get; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public Country Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string FaxNo { get; set; }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Address Location { set; get; }
        public int BranchContactId { set; get; }
        public int PickupContactId { set; get; }
        public string CloseTime { get; set; }
        public string PreferedPickupTimeFrom { get; set; }
        public string PreferedPickupTimeTo { get; set; }
        public string PickupNotes { get; set; }
        public string DeliveryNotes { get; set; }
        public int BusinessCustomerId { set; get; }
        public int OrderByContactId { set; get; }

    }
}
