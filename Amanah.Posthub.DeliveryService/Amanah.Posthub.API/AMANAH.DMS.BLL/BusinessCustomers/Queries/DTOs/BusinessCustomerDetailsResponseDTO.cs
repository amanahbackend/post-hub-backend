﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs
{
    public class BusinessCustomerDetailsResponseDTO
    {
        public int Id { set; get; }
        ///Bussines Info
        public string BusinessName { set; get; }
        public string BusinessCID { set; get; }
        public string BusinessCode { set; get; }

        public int BusinessTypeId { set; get; }
        public string BusinessTypeName { set; get; }
        public string BusinessLogoUrl { set; get; }
        public int CollectionMethodId { set; get; }
        public string CollectionMethodName { set; get; }
        public int? HQBranchId { set; get; }
        public string HQBranchAddress { set; get; }
        public int? MainContactId { set; get; }
        public Address HQBranchLocation { set; get; }
        public DateTime JoiningDate { set; get; }

        //
        public string OrderByName { get; set; }
        public string DepartmentName { get; set; }
        public string BusinessCustomerName { get; set; }
        public string BusinessCustomerCode { get; set; }
        public string HQBranchName { get; set; }
        //public string ContractCode { get; set; }
        //public string ContractStatus { get; set; }

        //public int? PickupLocationId { get; set; }
        //public int? ContractId { get; set; }
        //public int? PickupRequestNotificationId { get; set; }
        //public int? DepartmentId { get; set; }
        //
        public int? AccountType { get; set; } // 1 for business and 2 for individual
        public bool? IsApproved { get; set; }

        public int? BusinessCustomerId { get; set; }
        //public int? OrderById { get; set; }
        ////


    }
}
