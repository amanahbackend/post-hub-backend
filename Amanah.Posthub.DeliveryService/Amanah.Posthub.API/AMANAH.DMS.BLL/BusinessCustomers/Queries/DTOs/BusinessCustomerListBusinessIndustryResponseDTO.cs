﻿namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs
{
    public class BusinessCustomerListBusinessIndustryResponseDTO
    {
        public int Id { set; get; }
        ///Bussines Info
        ///
        public string BusinessName { set; get; }
        public string BusinessCode { set; get; }
        public int BusinessTypeId { set; get; }
    }
}
