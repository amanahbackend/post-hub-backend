﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs
{
    public class BusinessCustomerListResponseDTO
    {
        public int Id { set; get; }
        ///Bussines Info
        ///
        public string BusinessName { set; get; }
        public string BusinessCID { set; get; }
        public string BusinessCode { set; get; }

        public int BusinessTypeId { set; get; }
        public string BusinessTypeName { set; get; }
        public string BusinessLogoUrl { set; get; }
        public int CollectionMethodId { set; get; }
        public string CollectionMethodName { set; get; }
        public int? HQBranchId { set; get; }
        public string HQBranchAddress { set; get; }
        public Address HQBranchLocation { set; get; }
        public DateTime JoiningDate { set; get; }
        public int? AccountType { get; set; }
        public int AccountStatusId { get; set; }
        public bool? IsApproved { get; set; }

    }
}
