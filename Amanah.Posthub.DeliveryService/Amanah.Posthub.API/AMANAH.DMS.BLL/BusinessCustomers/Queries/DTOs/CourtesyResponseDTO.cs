﻿namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs
{
    public class CourtesyResponseDTO
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
    }
}
