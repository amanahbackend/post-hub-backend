﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs
{
    public class ExportBusinessCustomerListResponseDTO
    {
        public int Id { set; get; }
        ///Bussines Info
        ///
        public string BusinessName { set; get; }
        public string BusinessCID { set; get; }
        public string BusinessCode { set; get; }
        public string BusinessTypeName { set; get; }
        public string CollectionMethodName { set; get; }
        public string HQBranchAddress { set; get; }
        public Address HQBranchLocation { set; get; }
        public DateTime JoiningDate { set; get; }

    }
}
