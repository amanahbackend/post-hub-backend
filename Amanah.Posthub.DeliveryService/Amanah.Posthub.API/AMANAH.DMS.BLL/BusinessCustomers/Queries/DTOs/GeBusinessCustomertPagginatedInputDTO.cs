﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs
{
    public class GeBusinessCustomertPagginatedInputDTO : PaginatedItemsViewModel
    {
        public bool? IsApproved { set; get; }
    }
}
