﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomersBranchss.Services
{
    public interface IBusinessCustomerBranchService
    {
        Task<object> CreateAsync(CreateBusinessCustomerBranchInputDTO model);
        Task DeleteAsync(int id);
        Task<object> UpdateAsync(UpdateBusinessCustomerBranchInputDTO model);

    }
    public class BusinessCustomerBranchService : IBusinessCustomerBranchService
    {
        private readonly IRepository<BusinessCustomerBranch> _bussinesCustomerBranchRepository;
        private readonly IRepository<BusinessCustomer> _bussinesCustomerRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IUserDomainService _userDomainService;
        private readonly IRepository<AccountLogs> _accountLogRepository;
        private readonly ApplicationDbContext _context;
        public BusinessCustomerBranchService(
            ApplicationDbContext context,
            IRepository<BusinessCustomerBranch> bussinesCustomerBranchRepository,
            IRepository<BusinessCustomer> bussinesCustomerRepository,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IUserDomainService userDomainService,
            IUploadFormFileService uploadFormFileService,
            IRepository<AccountLogs> accountLogRepository)
        {
            _bussinesCustomerBranchRepository = bussinesCustomerBranchRepository;
            _bussinesCustomerRepository = bussinesCustomerRepository;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _uploadFormFileService = uploadFormFileService;
            _userDomainService = userDomainService;
            _accountLogRepository = accountLogRepository;
            _context = context;
        }

        public async Task<object> CreateAsync(CreateBusinessCustomerBranchInputDTO model)
        {
            BusinessCustomerBranch bCustumerBranch = null;
            await _unityOfWork.RunTransaction(async () =>
            {
                bCustumerBranch = _mapper.Map<BusinessCustomerBranch>(model);

                _bussinesCustomerBranchRepository.Add(bCustumerBranch);

                await _unityOfWork.SaveChangesAsync();

                if (model.IshqBranch)
                {
                    var bCustomer = await _context.BusinessCustomers.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == model.BusinessCustomerId);
                    //await _bussinesCustomerRepository.GetByIdAsync(model.BusinessCustomerId);
                    bCustomer.HQBranchId = bCustumerBranch.Id;

                    _bussinesCustomerRepository.Update(bCustomer);
                    await _unityOfWork.SaveChangesAsync();
                }


            });

            return new { Id = bCustumerBranch.Id, Name = bCustumerBranch.Name };
        }

        public async Task DeleteAsync(int id)
        {
            var existedBCustomer = await _bussinesCustomerBranchRepository.GetByIdAsync(id);
            if (existedBCustomer == null)
            {
                return;
            }
            await _unityOfWork.RunTransaction(async () =>
            {
                existedBCustomer.SetIsDeleted(true);
                _bussinesCustomerBranchRepository.Update(existedBCustomer);
                _accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "BusinessCusomerContact",
                    ActivityType = "Delete",
                    Description = $"Business Customer Contact  { existedBCustomer.Name} has been deleted ",
                    Record_Id = existedBCustomer.Id
                });

                await _unityOfWork.SaveChangesAsync();
            });
        }

        public async Task<object> UpdateAsync(UpdateBusinessCustomerBranchInputDTO model)
        {
            BusinessCustomerBranch bCustumerBranch = null;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                bCustumerBranch = await _context.BusinessCustomerBranchs.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == model.Id);
                _mapper.Map(model, bCustumerBranch);

                _bussinesCustomerBranchRepository.Update(bCustumerBranch);
                await _unityOfWork.SaveChangesAsync();
                if (model.IshqBranch)
                {
                    var bCustomer = await _context.BusinessCustomers.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == model.BusinessCustomerId);
                    bCustomer.HQBranchId = bCustumerBranch.Id;

                    _bussinesCustomerRepository.Update(bCustomer);
                }
                _accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "BussinesCustomerBranch",
                    ActivityType = "Update",
                    Description = $"Business Customer {bCustumerBranch.Name} has been created ",
                    Record_Id = bCustumerBranch.Id
                });
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return new { Id = bCustumerBranch.Id, BranchName = bCustumerBranch.Name };
            }
            else
            {
                throw new DomainException(Keys.Validation.GeneralError);
            }
        }
    }
}
