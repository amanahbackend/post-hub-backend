﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.Service.Domain.Enums;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.SharedKernel.Authentication;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomersContactss.Services
{

    public interface IBusinessCustomerContactService
    {
        Task<object> CreateAsync(CreateBusinessCustomerContactInputDTO model);
        Task<int> AccountRegistrationAsync(AccountRegistrationDto model);
        Task DeleteAsync(int id);
        Task<object> UpdateAsync(UpdateBusinessCustomerContactInputDTO model);
    }
    public class BusinessCustomerContactService : IBusinessCustomerContactService
    {
        private readonly IRepository<BusinessContactAccount> _bussinesCustomerContactRepository;
        private readonly IRepository<BusinessCustomer> _bussinesCustomerRepository;
        private readonly IRepository<BusinessCustomerBranch> _bussinesCustomerBranchRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IUserDomainService _userDomainService;
        private readonly IRepository<AccountLogs> _accountLogRepository;
        private readonly IRoleDomainService _roleDomainService;
        private readonly ICurrentUser _currentUser;
        private readonly ApplicationDbContext _context;
        public BusinessCustomerContactService(
            ApplicationDbContext context,
            IRepository<BusinessContactAccount> bussinesCustomerContactRepository,
            IRepository<BusinessCustomer> bussinesCustomerRepository,
            IRepository<BusinessCustomerBranch> bussinesCustomerBranchRepository,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IUserDomainService userDomainService,
            IUploadFormFileService uploadFormFileService,
            IRoleDomainService roleDomainService,
            IRepository<AccountLogs> accountLogRepository,
            ICurrentUser currentUser)
        {
            _context = context;
            _bussinesCustomerContactRepository = bussinesCustomerContactRepository;
            _bussinesCustomerRepository = bussinesCustomerRepository;
            _bussinesCustomerBranchRepository = bussinesCustomerBranchRepository;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _uploadFormFileService = uploadFormFileService;
            _userDomainService = userDomainService;
            _accountLogRepository = accountLogRepository;
            _roleDomainService = roleDomainService;
            _currentUser = currentUser;
        }

        public async Task<object> CreateAsync(CreateBusinessCustomerContactInputDTO model)
        {
            var bCustomer = await _bussinesCustomerRepository.GetByIdAsync(model.BusinessCustomerId);

            #region Create Login User

            model.RoleName = model.RoleName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            BusinessContactAccount bCustumerContact = null;
            //await _unityOfWork.RunTransaction(async () =>
            //{

            var user = _mapper.Map<ApplicationUser>(model);

            if (!(bCustomer.IsApproved.HasValue && bCustomer.IsApproved.Value))
            {
                user.IsLocked = true;   // (int)AccountStatuses.Inactive;

            }
            else
            {
                user.IsLocked = false;
            }
            if (_currentUser != null && !string.IsNullOrEmpty(_currentUser.TenantId))
            {
                user = await _userDomainService.CreateAsync(user, model.Password, null);
            }
            else
            {
                user = await _userDomainService.CreateAsync(user, model.Password, null, true);
            }

            List<string> permissions = BsinessCustomerPermissionPovider.GetDafaultPermissions();
            var newrole = new ApplicationRole(model.RoleName, user.Tenant_Id);
            newrole.Type = (int)RoleType.BusinessCustomer;
            var role = await _roleDomainService.CreateAsync(newrole, permissions);
            user.RoleNames = new List<string> { role.Name };
            await _userDomainService.AddUserToRolesAsync(user);
            #endregion

            bCustumerContact = _mapper.Map<BusinessContactAccount>(model);
            bCustumerContact.UserID = user.Id;

            if (model.PersonalPhotoFile != null)
            {
                bCustumerContact.PersonalPhotoURL = await SetBusinessContactProfileAsync(model.PersonalPhotoFile);
            }

            if (!(bCustomer.IsApproved.HasValue && bCustomer.IsApproved.Value))
            {
                bCustumerContact.AccountStatusId = (int)AccountStatuses.Inactive;
            }
            else
            {
                bCustumerContact.AccountStatusId = (int)AccountStatuses.Active; //isactive
            }

            if (bCustumerContact.AccountType == (int)BusinessCustomerType.IndividualCustomer)
                bCustumerContact.BusinessCustomer.IsApproved = true;

            _bussinesCustomerContactRepository.Add(bCustumerContact);

            await _unityOfWork.SaveChangesAsync();

            if (model.IsMainContact)
            {
                bCustomer.MainContactId = bCustumerContact.Id;

                _bussinesCustomerRepository.Update(bCustomer);
                await _unityOfWork.SaveChangesAsync();
            }


            //}
            //);

            return new { Id = bCustumerContact.Id, Name = bCustumerContact.User.FirstName };
        }

        public async Task<int> AccountRegistrationAsync(AccountRegistrationDto model)
        {
            int resultId = 0;
            //await _unityOfWork.RunTransaction(async () =>
            //{
            var bCustumer = new BusinessCustomer();

            bCustumer.AccountType = (int)BusinessCustomerType.IndividualCustomer;
            bCustumer.IsApproved = true; // for individual

            bCustumer.IsCompleted = true;
            bCustumer.BusinessTypeId = (int)Service.Domain.BusinessCustomers.BusinessTypes.Individual;
            bCustumer.BusinessName = model.FullName; // need test
            bCustumer.CollectionMethodId = model.CollectionMethodId; 
            bCustumer.BusinessCID = model.BusinessCID;
            if (model.BusinessPhotoFile != null)
            {
                bCustumer.BusinessLogoUrl = await SetBusinessCustomerLogosAsync(model.BusinessPhotoFile);
            }
            _bussinesCustomerRepository.Add(bCustumer);

            #region Create Login User

            var roleName = "individualCustomer_" + DateTime.Now.ToString("yyyyMMddHHmmss");

            var user = new ApplicationUser();
            user.UserName = model.UserName;
            user.FirstName = model.FullName;
            user.Email = model.UserName;
            user.PhoneNumber = model.MobileNumber;
            user.CountryId = model.CountryId;

            user.IsLocked = false;  // false to auto approved from Mobile

            var newUser = await _userDomainService.CreateAsync(user, model.Password, null, true);

            List<string> permissions = BsinessCustomerPermissionPovider.GetDafaultPermissions();
            var newrole = new ApplicationRole(roleName, newUser.Tenant_Id);
            newrole.Type = (int)RoleType.BusinessCustomer;
            var role = await _roleDomainService.CreateAsync(newrole, permissions);
            newUser.RoleNames = new List<string> { role.Name };
            await _userDomainService.AddUserToRolesAsync(newUser);
            #endregion

            var bCustumerContact = new BusinessContactAccount();
            bCustumerContact.UserID = newUser.Id;
            bCustumerContact.BusinessCustomerId = bCustumer.Id;
            if (model.PersonalPhotoFile != null)
            {
                bCustumerContact.PersonalPhotoURL = await SetBusinessContactProfileAsync(model.PersonalPhotoFile);
            }
            bCustumerContact.AccountStatusId = (int)AccountStatuses.Inactive;
            _bussinesCustomerContactRepository.Add(bCustumerContact);
            await _context.SaveChangesAsync();


            var bCustumerBranch = new BusinessCustomerBranch();
            bCustumerBranch.BusinessCustomerId = bCustumer.Id;
            bCustumerBranch.Name = model.FullName;
            bCustumerBranch.Email = model.UserName;
            bCustumerBranch.Mobile = model.MobileNumber;
            bCustumerBranch.CountryId = model.CountryId;
            bCustumerBranch.BranchContactId = bCustumerContact.Id;
            bCustumerBranch.PickupContactId = bCustumerContact.Id;
            bCustumerBranch.Location = model.Location;

            _bussinesCustomerBranchRepository.Add(bCustumerBranch);
            await _context.SaveChangesAsync();
            bCustumer.MainContactId = bCustumerContact.Id;
            bCustumer.HQBranchId = bCustumerBranch.Id;
            _bussinesCustomerRepository.Update(bCustumer);

            _accountLogRepository.Add(new AccountLogs()
            {
                TableName = "BussinesCustomer",
                ActivityType = "Create",
                Description = $"Manager {bCustumer.BusinessName} has been created from Mobile",
                Record_Id = bCustumer.Id
            });

            await _context.SaveChangesAsync();
            resultId = bCustumer.Id;

            return resultId;
        }
        private async Task<string> SetBusinessLogoAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "BusinessCustomerLogos";
                var processResult = await _uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }
        private List<string> GetAllBuisinessCustomerPermissions()
        {
            List<string> permissions = new List<string>();
            TenantPermissions.FeaturePermissions
                .Where(featurePermission => featurePermission.Name != PermissionFeatures.PlatformAgent)
                .ToList()
                .ForEach(featurePermission =>
                {
                    permissions.AddRange(
                         featurePermission.Permissions.Select(permission => permission).ToList());
                });
            return permissions
                .Distinct()
                .ToList();
        }

        public async Task DeleteAsync(int id)
        {
            var existedBCustomer = await _bussinesCustomerContactRepository.GetByIdAsync(id);
            if (existedBCustomer == null)
            {
                return;
            }
            await _unityOfWork.RunTransaction(async () =>
            {
                existedBCustomer.SetIsDeleted(true);
                _bussinesCustomerContactRepository.Update(existedBCustomer);
                _accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "BusinessCusomerContact",
                    ActivityType = "Delete",
                    Description = $"Business Customer Contact  { existedBCustomer.MobileNumber} has been deleted ",
                    Record_Id = existedBCustomer.Id
                });

                await _unityOfWork.SaveChangesAsync();
            });
        }



        public async Task<object> UpdateAsync(UpdateBusinessCustomerContactInputDTO model)
        {
            BusinessContactAccount bCustumerContact = null;
            var user = _mapper.Map<ApplicationUser>(model);


            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                user.Email = model.UserName;
                user = await _userDomainService.EditAsync(user, model.RoleName);
                bCustumerContact = await _context.BusinessConactAccounts.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == model.Id);//await _bussinesCustomerContactRepository.GetByIdAsync(model.Id);
                _mapper.Map(model, bCustumerContact);
                if (model.PersonalPhotoFile != null)
                {
                    bCustumerContact.PersonalPhotoURL = await SetBusinessContactProfileAsync(model.PersonalPhotoFile);
                }
                if (model.PersonalPhotoFile == null)
                {
                    bCustumerContact.PersonalPhotoURL = "";
                }
                _bussinesCustomerContactRepository.Update(bCustumerContact);
                await _unityOfWork.SaveChangesAsync();
                if (model.IsMainContact)
                {
                    var bCustomer = await _context.BusinessCustomers.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == model.BusinessCustomerId); //await _bussinesCustomerRepository.GetByIdAsync(model.BusinessCustomerId);
                    bCustomer.MainContactId = bCustumerContact.Id;
                    _bussinesCustomerRepository.Update(bCustomer);
                }

                _accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "BussinesCustomer",
                    ActivityType = "Update",
                    Description = $"Business Customer {bCustumerContact.Id} has been Updated ",
                    Record_Id = bCustumerContact.Id
                });
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return new { Id = bCustumerContact.Id, ContactName = bCustumerContact.Id };
            }
            else
            {
                throw new DomainException(Keys.Validation.GeneralError);
            }

        }
        
        private async Task<string> SetBusinessCustomerLogosAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "BusinessCustomerLogos";
                var processResult = await _uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }
        private async Task<string> SetBusinessContactProfileAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "BusinessContactProfile";
                var processResult = await _uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }
    }
}
