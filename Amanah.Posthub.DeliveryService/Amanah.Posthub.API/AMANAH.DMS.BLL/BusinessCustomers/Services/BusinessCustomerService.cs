﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services
{
    public interface IBusinessCustomerService
    {
        Task<object> CreateAsync(CreateBusinessCustomerInputDTO model);
        Task<object> UpdateAsync(UpdateBusinessCustomerInputDTO model);
        Task DeleteAsync(int id);
    }
    public class BusinessCustomerService : IBusinessCustomerService
    {
        private readonly IRepository<BusinessCustomer> _bussinesCustomerRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IUserDomainService _userDomainService;
        private readonly IRepository<AccountLogs> _accountLogRepository;
        private readonly ApplicationDbContext _context;
        public BusinessCustomerService(ApplicationDbContext context,
            IRepository<BusinessCustomer> bussinesCustomerRepository,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IUserDomainService userDomainService,
            IUploadFormFileService uploadFormFileService,
            IRepository<AccountLogs> accountLogRepository)
        {
            _bussinesCustomerRepository = bussinesCustomerRepository;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _uploadFormFileService = uploadFormFileService;
            _userDomainService = userDomainService;
            _accountLogRepository = accountLogRepository;
            _context = context;
        }

        public async Task<object> CreateAsync(CreateBusinessCustomerInputDTO model)
        {
            BusinessCustomer bCustumer = null;
            await _unityOfWork.RunTransaction(async () =>
            {
                bCustumer = _mapper.Map<BusinessCustomer>(model);
                if (model.BusinessLogoFile != null)
                {
                    bCustumer.BusinessLogoUrl = await SetBusinessLogoAsync(model.BusinessLogoFile);
                }
                bCustumer.AccountType = model.AccountType;

                if (model.IsApproved.HasValue && model.IsApproved.Value)
                    bCustumer.IsApproved = model.IsApproved;
                else
                    bCustumer.IsApproved = false;

                if (model.IsCompleted.HasValue && model.IsCompleted.Value)
                    bCustumer.IsCompleted = model.IsCompleted.Value;
                else
                    bCustumer.IsCompleted = false;

                if (bCustumer.AccountType == (int)BusinessCustomerType.IndividualCustomer)
                {
                    bCustumer.BusinessTypeId = (int)Service.Domain.BusinessCustomers.BusinessTypes.Individual;
                    bCustumer.IsApproved = true;
                }
                _bussinesCustomerRepository.Add(bCustumer);
                await _unityOfWork.SaveChangesAsync();
                _accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "BussinesCustomer",
                    ActivityType = "Create",
                    Description = $"Manager {bCustumer.BusinessName} has been created ",
                    Record_Id = bCustumer.Id
                });
                await _unityOfWork.SaveChangesAsync();
            });

            return new { Id = bCustumer.Id, BusinessName = bCustumer.BusinessName };
        }



        public async Task<object> UpdateAsync(UpdateBusinessCustomerInputDTO model)
        {
            BusinessCustomer bCustumer = null;
            bool result = await _unityOfWork.RunTransaction(async () =>
              {
                  bCustumer = await _context.BusinessCustomers.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == model.Id);
                  _mapper.Map(model, bCustumer);
                  if (model.BusinessLogoFile != null)
                  {
                      bCustumer.BusinessLogoUrl = await SetBusinessLogoAsync(model.BusinessLogoFile);
                  }
                  if (bCustumer.AccountType == (int)BusinessCustomerType.IndividualCustomer)
                  {
                      bCustumer.BusinessTypeId = (int)Service.Domain.BusinessCustomers.BusinessTypes.Individual;
                      bCustumer.IsApproved = true;
                  }
                  bCustumer.IsCompleted = true;
                  if (model.IsApproved.HasValue && model.IsApproved.Value)
                      bCustumer.IsApproved = model.IsApproved;
                  else
                      bCustumer.IsApproved = false;

                  _bussinesCustomerRepository.Update(bCustumer);
                  await _unityOfWork.SaveChangesAsync();
                  _accountLogRepository.Add(new AccountLogs()
                  {
                      TableName = "BussinesCustomer",
                      ActivityType = "Update",
                      Description = $"Business Customer {bCustumer.BusinessName} has been created ",
                      Record_Id = bCustumer.Id
                  });
                  await _unityOfWork.SaveChangesAsync();
              });
            if (result)
            {
                return new { Id = bCustumer.Id, BusinessName = bCustumer.BusinessName };
            }
            else
            {
                throw new DomainException(Keys.Validation.GeneralError);
            }

        }
        public async Task DeleteAsync(int id)
        {
            var existedBCustomer = await _context.BusinessCustomers.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == id);
            //await _bussinesCustomerRepository.GetByIdAsync(id);
            if (existedBCustomer == null)
            {
                return;
            }
            await _unityOfWork.RunTransaction(async () =>
            {
                existedBCustomer.SetIsDeleted(true);
                _bussinesCustomerRepository.Update(existedBCustomer);
                _accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "BusinessCusomer",
                    ActivityType = "Delete",
                    Description = $"BusinessCusomer { existedBCustomer.BusinessName} has been deleted ",
                    Record_Id = existedBCustomer.Id
                });

                await _unityOfWork.SaveChangesAsync();
            });
        }


        private async Task<string> SetBusinessLogoAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "BusinessCustomerLogos";
                var processResult = await _uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }


    }
}
