﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs
{
    public class AccountRegistrationDto
    {
        public string BusinessName { set; get; }
        public string BusinessCID { set; get; }
        public IFormFile PersonalPhotoFile { set; get; }
        public IFormFile BusinessPhotoFile { set; get; }

        public string UserName { set; get; }
        public string Password { set; get; }
        public string FullName { set; get; }
        public string MobileNumber { get; set; }
        public int CountryId { set; get; }
        public Address Location { set; get; }
        public int? CollectionMethodId { set; get; }

    }
}
