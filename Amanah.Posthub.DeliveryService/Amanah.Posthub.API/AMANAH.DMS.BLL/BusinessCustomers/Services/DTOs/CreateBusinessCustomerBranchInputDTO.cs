﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs
{
    public class CreateBusinessCustomerBranchInputDTO
    {
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string FaxNo { get; set; }

        public string Email { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Address Location { set; get; }
        ////
        public int BranchContactId { set; get; }
        public int PickupContactId { set; get; }
        public string CloseTime { get; set; }
        public string PreferedPickupTimeFrom { get; set; }
        public string PreferedPickupTimeTo { get; set; }
        public string PickupNotes { get; set; }
        public string DeliveryNotes { get; set; }
        public int BusinessCustomerId { set; get; }
        public bool IshqBranch { get; set; }
    }
}
