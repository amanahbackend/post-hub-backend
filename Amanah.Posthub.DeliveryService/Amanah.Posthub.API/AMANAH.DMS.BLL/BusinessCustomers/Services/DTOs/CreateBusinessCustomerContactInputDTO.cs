﻿using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs
{
    public class CreateBusinessCustomerContactInputDTO
    {
        public string FullName { set; get; }
        public int CountryId { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }
        public string UserName { set; get; }
        public string Password { set; get; }
        public string RoleName { set; get; }
        public string ContactCode { set; get; }
        public int? ContactFunctionId { set; get; }
        public int? CourtesyId { set; get; }
        public int AccountStatusId { get; set; }
        public IFormFile PersonalPhotoFile { get; set; }
        public string PersonalPhotoURL { get; set; }
        public string MobileNumber { get; set; }
        public string Fax { get; set; }
        public int BusinessCustomerId { set; get; }
        //public int? AccountType { get; set; } // 1 for business 2 for individual
        //public bool mainContactId { get; set; }
        public bool IsMainContact { get; set; }

    }
}
