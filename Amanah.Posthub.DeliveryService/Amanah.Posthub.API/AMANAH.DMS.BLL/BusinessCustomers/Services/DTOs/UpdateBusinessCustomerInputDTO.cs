﻿using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs
{
    public class UpdateBusinessCustomerInputDTO
    {
        public int Id { set; get; }

        ///Bussines Info
        public string BusinessName { set; get; }
        public int BusinessTypeId { set; get; }
        public IFormFile BusinessLogoFile { set; get; }
        //public string BusinessLogoUrl { set; get; }
        public string BusinessCID { set; get; }
        public string BusinessCode { set; get; }
        public int CollectionMethodId { set; get; }
        public bool IsCompleted { set; get; }
        public bool? IsApproved { get; set; }
        public int? HQBranchId { set; get; }
        public int? MainContactId { set; get; }
        //public List<CreateBusinessCustomerContactInputDTO> BusinessCustomerContacts { set; get; }
        //public List<CreateBusinessCustomerBranchInputDTO> BusinessCustomerBranchs { set; get; }
    }
}
