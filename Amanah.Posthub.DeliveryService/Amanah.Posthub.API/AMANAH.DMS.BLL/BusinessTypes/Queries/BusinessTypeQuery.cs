﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.BusinessTypes.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.DeliverService.BLL.BusinessTypes.Queries
{
    public interface IBusinessTypeQuery
    {
        Task<PagedResult<BusinessTypeResponseDTO>> GetByPaginationAsync(BusinessTypeFilterDTO filter);
        Task<List<BusinessTypeResponseDTO>> GetAllAsync();
        Task<List<BusinessTypeResponseDTO>> GetByBusinessCustomerIdAsync(int businessCustomerId);
    }

    public class BusinessTypeQuery : IBusinessTypeQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public BusinessTypeQuery(ApplicationDbContext context,IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<PagedResult<BusinessTypeResponseDTO>> GetByPaginationAsync(BusinessTypeFilterDTO filter)
        {
            PagedResult<BusinessTypeResponseDTO> result = new PagedResult<BusinessTypeResponseDTO>();
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;

            var query = context.BusinessTypes.IgnoreQueryFilters().Where(b => !b.IsDeleted).OrderByDescending(c => c.Id).AsQueryable();

            if (!string.IsNullOrEmpty(filter.SearchBy))
            {
                int.TryParse(filter.SearchBy, out var businessTypeId);
                query = query.Where(b => b.Id == businessTypeId
                    || b.Name_ar.ToLower().Contains(filter.SearchBy.ToLower())
                    || b.Name_en.ToLower().Contains(filter.SearchBy.ToLower())
                    );
            }

            var businessTypes = await query
                  .ProjectTo<BusinessTypeResponseDTO>(mapper.ConfigurationProvider)
                  .Skip((filter.PageNumber - 1) * filter.PageSize).Take(filter.PageSize)
                  .ToListAsync();

            result.TotalCount = await query.CountAsync();
            result.Result = businessTypes;

            return result;
        }

        public async Task<List<BusinessTypeResponseDTO>> GetAllAsync()
        {

            var request = await context.BusinessTypes
                .AsNoTracking()
                .IgnoreQueryFilters()
                .ProjectTo<BusinessTypeResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return request;
        }

        public async Task<List<BusinessTypeResponseDTO>> GetByBusinessCustomerIdAsync(int businessCustomerId)
        {
            var res = await context.BusinessTypes.IgnoreQueryFilters().Where(c => c.BusinessCustomers.Where(c => c.Id == businessCustomerId).FirstOrDefault().Id == businessCustomerId)
               .AsNoTracking()
               .ProjectTo<BusinessTypeResponseDTO>(mapper.ConfigurationProvider)
               .ToListAsync();
            return res;
        }
    }
}
