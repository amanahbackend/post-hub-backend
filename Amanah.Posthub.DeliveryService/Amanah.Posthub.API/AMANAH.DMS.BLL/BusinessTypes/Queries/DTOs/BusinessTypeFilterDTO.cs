﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.BusinessTypes.Queries.DTOs
{
    public class BusinessTypeFilterDTO
    {
        public int TotalNumbers { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchBy { get; set; }
        public int Id { get; set; }
    }
}
