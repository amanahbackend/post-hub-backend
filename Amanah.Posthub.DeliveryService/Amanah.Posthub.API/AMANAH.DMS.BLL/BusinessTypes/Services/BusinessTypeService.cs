﻿using Amanah.Posthub.DeliverService.BLL.BusinessTypes.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.BusinessTypes.Services.DTOs;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.BusinessTypes.Services
{
    public interface IBusinessTypeService 
    {
        Task<BusinessTypeResponseDTO> CreateAsync(CreateBusinessTypeInputDTO businessTypeInput);
        Task<BusinessTypeResponseDTO> UpdateAsync(UpdateBusinessTypeInputDTO businessTypeInput);
        Task<bool> DeleteAsync(int id);
    }
    public class BusinessTypeService : IBusinessTypeService
    {
        private readonly IRepository<BusinessType> businessTypeRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unityOfWork;

        public BusinessTypeService(IRepository<BusinessType> businessTypeRepository, IMapper mapper, IUnitOfWork unityOfWork)
        {
            this.businessTypeRepository = businessTypeRepository;
            this.mapper = mapper;
            this.unityOfWork = unityOfWork;
        }

        public async Task<BusinessTypeResponseDTO> CreateAsync(CreateBusinessTypeInputDTO businessTypeInput)
        {
            BusinessType businessType = null;

            await unityOfWork.RunTransaction(async () =>
            {
                businessType = mapper.Map<BusinessType>(businessTypeInput);
                businessTypeRepository.Add(businessType);
                await unityOfWork.SaveChangesAsync();
            });

            return new BusinessTypeResponseDTO { Id = businessType.Id, Name_en = businessType.Name_en, Name_ar = businessType.Name_ar, IsActive = businessType.IsActive };
        }

        public async Task<BusinessTypeResponseDTO> UpdateAsync(UpdateBusinessTypeInputDTO businessTypeInput)
        {
            await unityOfWork.RunTransaction(async () =>
            {
                var existingBusinessType = await businessTypeRepository.GetAllIQueryable(b => b.Id == businessTypeInput.Id).FirstOrDefaultAsync();

                if (existingBusinessType != null)
                {
                    existingBusinessType = mapper.Map<BusinessType>(businessTypeInput);
                    businessTypeRepository.Update(existingBusinessType);
                    await unityOfWork.SaveChangesAsync();
                }
            });

            return new BusinessTypeResponseDTO { Id = businessTypeInput.Id, Name_en = businessTypeInput.Name_en, Name_ar = businessTypeInput.Name_ar, IsActive = businessTypeInput.IsActive };
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var existingBusinessType = await businessTypeRepository.GetAllIQueryable(b => b.Id == id).FirstOrDefaultAsync();

            if (existingBusinessType != null)
            {
                await unityOfWork.RunTransaction(async () =>
                {
                    existingBusinessType.SetIsDeleted(true);
                    businessTypeRepository.Update(existingBusinessType);
                    await unityOfWork.SaveChangesAsync();
                });
                return true;
            }
            return false;
        }
    }
}
