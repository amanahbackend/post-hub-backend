﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.BusinessTypes.Services.DTOs
{
    public class UpdateBusinessTypeInputDTO
    {
        public int Id { get; set; } 
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsActive { get; set; }
    }
}
