﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.CompanyProfile;
using Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.CompanyProfile.Queries
{
    public interface ICompanyQuery
    {
        Task<CompanyProfileDetailsDTO> GetByIdAsync(int id);
        Task<CompanyProfileDetailsDTO> GetCurrentCompanyProfileAsync();
        Task<CompanyProfileDetailsForReportsDTO> GetCurrentCompanyProfileForReportsAsync();

    }

    public class CompanyQuery : ICompanyQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<Company> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public CompanyQuery
            (
            IRepository<Company> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }


        public async Task<CompanyProfileDetailsDTO> GetByIdAsync(int id)
        {
            var request = await context.Company
                .IgnoreQueryFilters()
                .Where(registration => registration.Id == id)
                .AsNoTracking()
                .ProjectTo<CompanyProfileDetailsDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            return request;
        }

        public async Task<CompanyProfileDetailsDTO> GetCurrentCompanyProfileAsync()
        {
            var company = await context.Company
             .IgnoreQueryFilters()
             .Include(c => c.MainBranch)
             .ThenInclude(c => c.Location)
             .Include(c => c.WorkingHours)
             .Include(c => c.Branches)
             .ThenInclude(c => c.Location)

             .Where(x => x.Tenant_Id == this._currentUser.TenantId)
             .AsNoTracking()
             .FirstOrDefaultAsync();

            // .ProjectTo<CompanyProfileDetailsDTO>(mapper.ConfigurationProvider)
            // .FirstOrDefaultAsync();
            var mappedObj = new CompanyProfileDetailsDTO();
            if (company != null)
            {
                mappedObj = mapper.Map<CompanyProfileDetailsDTO>(company);
                if (company.Branches.Any())
                    mappedObj.Branches = mapper.Map<List<CompanyBranchResponseDTO>>(company.Branches);
                if (company.WorkingHours.Any())
                    mappedObj.WorkingHours = mapper.Map<List<CompanyWorkingHoursResultDTO>>(company.WorkingHours);
            }

            return mappedObj;

        }

        public async Task<CompanyProfileDetailsForReportsDTO> GetCurrentCompanyProfileForReportsAsync()
        {
            var company =  context.Company
             .IgnoreQueryFilters()
             .AsNoTracking()
             .AsQueryable();

            Company companyProfile = null;

            if (this._currentUser.IsBusinessCustomer())
            {
                companyProfile =  company.FirstOrDefault();
            }
            else
            {
                companyProfile =  company.Where(x => x.Tenant_Id == this._currentUser.TenantId).FirstOrDefault();
            }
            var mappedObj = new CompanyProfileDetailsForReportsDTO();
            if (companyProfile != null)
            {
                mappedObj = mapper.Map<CompanyProfileDetailsForReportsDTO>(companyProfile);
               
            }

            return mappedObj;

        }
    }
}
