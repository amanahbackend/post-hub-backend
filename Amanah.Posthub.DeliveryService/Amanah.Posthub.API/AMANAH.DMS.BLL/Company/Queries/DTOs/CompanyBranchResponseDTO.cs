﻿using Amanah.Posthub.BLL.Branches.Queries.DTOs;
using System;

namespace Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs
{
    public class CompanyBranchResponseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public string Phone { get; set; }
        public int CompanyId { get; set; }
        public int GeoFenceId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool IsActivePrevious { get; set; }
        public string Reason { get; set; }
        public BranchAddressResponseDTO Location { set; get; }
        public string Tenant_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}
