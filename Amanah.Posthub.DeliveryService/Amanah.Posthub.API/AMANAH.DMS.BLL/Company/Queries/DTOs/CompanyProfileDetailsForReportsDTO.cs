﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs
{
    public class CompanyProfileDetailsForReportsDTO
    {
        public int Id { get; set; }
        public string LogoUrl { get; set; }
        public string MissionSlogan { get; set; }
        public string Name { get; set; }
        public string Name_ar { get; set; }
        public string Phone { get; set; }
        public string TradeName { get; set; }
        public string FaxNo { get; set; }

    }
}
