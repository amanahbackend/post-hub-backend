﻿namespace Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs
{
    public class DepartmentResponseDTO
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
    }
}
