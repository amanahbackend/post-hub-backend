﻿namespace Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs
{
    public class ServiceSectorResponseDTO
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }

    }
}
