﻿namespace Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs
{
    public class SocialLinksResponseDTO
    {
        public int Id { set; get; }
        public int CompanyId { set; get; }
        public string SiteName { set; get; }
        public string SiteLink { set; get; }

    }
}
