﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.DepartmentProfile.Queries
{
    public interface IDepartmentQuery
    {
        Task<List<DepartmentResponseDTO>> GetAllAsync();
    }

    public class DepartmentQuery : IDepartmentQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<Department> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public DepartmentQuery
            (
            IRepository<Department> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<DepartmentResponseDTO>> GetAllAsync()
        {

            var request = await context.Departments.Where(d=>!d.IsDeleted)
                .AsNoTracking()
                .IgnoreQueryFilters()
                .ProjectTo<DepartmentResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return request;
        }



    }
}
