﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.ServiceSectorProfile.Queries
{

    public interface IServiceSectorQuery
    {
        Task<List<ServiceSectorResponseDTO>> GetAllAsync();

    }

    public class ServiceSectorQuery : IServiceSectorQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<ServiceSector> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IConfiguration _configuration;

        public ServiceSectorQuery
            (
            IRepository<ServiceSector> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser, IConfiguration configuration)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
            _configuration = configuration;
        }

        public async Task<List<ServiceSectorResponseDTO>> GetAllAsync()
        {
            //remove internal post if internalpost flag is false
            var InternalPostFlag = _configuration.GetSection("FeatureFlags")["InternalPost"];
            if (!string.IsNullOrEmpty(InternalPostFlag) && InternalPostFlag.ToLower().ToString().Contains("true"))
            {
                var serviceSectorsWithoutInternalPost = await context.ServiceSectors
               .AsNoTracking()
               .Where(c => !c.IsDeleted && c.IsActive == true)
               .IgnoreQueryFilters()
               .ProjectTo<ServiceSectorResponseDTO>(mapper.ConfigurationProvider)
               .ToListAsync();
                serviceSectorsWithoutInternalPost.RemoveAll(i => i.Name_en == "Internal post");
                return serviceSectorsWithoutInternalPost;

            }
            else
            {
                return await context.ServiceSectors
                    .IgnoreQueryFilters()
                    .AsNoTracking()
                    .Where(c => !c.IsDeleted && c.IsActive == true)
                    .ProjectTo<ServiceSectorResponseDTO>(mapper.ConfigurationProvider)
                    .ToListAsync();
            }
        }



    }
}
