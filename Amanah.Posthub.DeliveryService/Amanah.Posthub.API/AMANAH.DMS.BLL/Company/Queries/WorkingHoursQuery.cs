﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Company.Queries
{
    public interface IWorkingHoursQuery
    {
        public Task<List<CompanyWorkingHoursResultDTO>> GetCompanyWorkingHoursAsync(int CompanyId);
    }

    public class WorkingHoursQuery : IWorkingHoursQuery
    {

        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public WorkingHoursQuery(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<CompanyWorkingHoursResultDTO>> GetCompanyWorkingHoursAsync(int CompanyId)
        {
            var request = await _context.WorkingHours
                         .IgnoreQueryFilters()
                         .Where(c => c.Id == CompanyId)
                         .AsNoTracking()
                         .ProjectTo<CompanyWorkingHoursResultDTO>(_mapper.ConfigurationProvider)
                         .ToListAsync();
            return request;
        }
    }
}
