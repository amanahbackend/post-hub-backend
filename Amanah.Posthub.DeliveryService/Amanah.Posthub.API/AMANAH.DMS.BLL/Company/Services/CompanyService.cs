﻿using Amanah.Posthub.DATA.CompanyProfile;
using Amanah.Posthub.DeliverService.BLL.CompanyProfile.Services.DTOs;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Utilites.UploadFile;

namespace Amanah.Posthub.BLL.CompanyProfile.Services
{
    public interface ICompanyService
    {
        public Task UpdateAsync(UpdateCompanyProfileDTO model);
    }
    public class CompanyService : ICompanyService
    {
        private readonly IRepository<Company> _companyRepository;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IMapper _mapper;
        private readonly IRepository<WorkingHours> _workinghoursRepository;
        private readonly IRepository<CompanySocialLinks> _companySocialRepository;
        private readonly IUploadFormFileService _uploadFormFileService;

        public CompanyService(
           IRepository<Company> repository,
           IUnitOfWork unityOfWork,
           IMapper mapper,
           IRepository<WorkingHours> workinghoursRepository,
           IRepository<CompanySocialLinks> companySocialRepository,
           IUploadFormFileService uploadFormFileService)
        {
            _mapper = mapper;
            _companyRepository = repository;
            _unityOfWork = unityOfWork;
            _workinghoursRepository = workinghoursRepository;
            _companySocialRepository = companySocialRepository;
            _uploadFormFileService = uploadFormFileService;
        }

        public async Task UpdateAsync(UpdateCompanyProfileDTO model)
        {
            await _unityOfWork.RunTransaction(async () =>
            {

                var company = await _companyRepository.GetByIdAsync(model.Id);
                _mapper.Map(model, company);
                if (model.LogoFile != null)
                {
                    company.LogoUrl = await SetCompanyLogoAsync(model.LogoFile);
                }
                var workinghours = await _workinghoursRepository
                .GetAllIQueryable(x => x.CompanyId == company.Id)
                .ToListAsync();
                //_workinghoursRepository.DeleteRange(workinghours);
                var companySocial = await _companySocialRepository
                 .GetAllIQueryable(x => x.CompanyId == model.Id)
                 .ToListAsync();
                _companySocialRepository.DeleteRange(companySocial);
                await _unityOfWork.SaveChangesAsync();

                _companyRepository.Update(company);
                await _unityOfWork.SaveChangesAsync();
            });

        }


        private async Task<string> SetCompanyLogoAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "CompanyImages";
                var processResult = await _uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }

    }


}
