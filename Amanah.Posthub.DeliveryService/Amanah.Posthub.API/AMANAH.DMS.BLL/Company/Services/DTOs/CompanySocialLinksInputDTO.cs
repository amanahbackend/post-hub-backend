﻿namespace Amanah.Posthub.DeliverService.BLL.Company.Services.DTOs
{
    public class CompanySocialLinksInputDTO
    {
        public int CompanyId { set; get; }
        public string SiteName { set; get; }
        public string SiteLink { set; get; }
    }
}
