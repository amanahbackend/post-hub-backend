﻿
using Amanah.Posthub.DeliverService.BLL.Company.Services.DTOs;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace Amanah.Posthub.DeliverService.BLL.CompanyProfile.Services.DTOs
{
    public class UpdateCompanyProfileDTO
    {

        /// <summary>
        /// Slogan data
        /// </summary>
        public int Id { get; set; }
        public string LogoUrl { get; set; }
        public IFormFile LogoFile { set; get; }
        public string MissionSlogan { get; set; }

        /// Company Data
        public string Name { get; set; }
        public string Name_ar { get; set; }

        public string BussinesDiscription { get; set; }
        public Address CompanyAddress { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string TradeName { get; set; }
        public int MainBranchId { get; set; }
        public string Address { get; set; }

        public List<CompanySocialLinksInputDTO> SocialLinks { set; get; }
        public List<CreateWorkinghoursInputDTO> WorkingHours { set; get; }
    }
}
