﻿using System;

namespace Amanah.Posthub.DeliverService.BLL.Company.Services.DTOs
{
    public class UpdateWorkinghoursDTO
    {
        public int Id { set; get; }
        public int CompanyId { set; get; }
        public DayOfWeek Day { set; get; }
        public TimeSpan? From { set; get; }
        public TimeSpan? To { set; get; }
        public TimeSpan? SecondFrom { set; get; }
        public TimeSpan? SecondTo { set; get; }
    }
}
