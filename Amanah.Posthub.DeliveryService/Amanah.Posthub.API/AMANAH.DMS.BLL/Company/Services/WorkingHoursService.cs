﻿using Amanah.Posthub.DeliverService.BLL.Company.Services.DTOs;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Company.Services
{
    public interface IWorkingHoursService
    {
        public Task<bool> AddAsync(CreateWorkinghoursInputDTO model);
        public Task<bool> UpdateAsync(UpdateWorkinghoursDTO model);


    }
    public class WorkingHoursService : IWorkingHoursService
    {
        private readonly IRepository<WorkingHours> _workingRepository;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IMapper _mapper;

        public WorkingHoursService(
           IRepository<WorkingHours> workinHoursrepository,
           IUnitOfWork unityOfWork,
           IMapper mapper)
        {
            _mapper = mapper;
            _workingRepository = workinHoursrepository;
            _unityOfWork = unityOfWork;
        }

        public async Task<bool> AddAsync(CreateWorkinghoursInputDTO model)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                var workingHour = _mapper.Map<WorkingHours>(model);
                _workingRepository.Add(workingHour);
                await _unityOfWork.SaveChangesAsync();
            });
            return true;
        }

        public async Task<bool> UpdateAsync(UpdateWorkinghoursDTO model)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                var workinghours = await _workingRepository.GetByIdAsync(model.Id);
                _mapper.Map(model, workinghours);
                _workingRepository.Update(workinghours);
                await _unityOfWork.SaveChangesAsync();
            });
            return true;
        }

    }
}
