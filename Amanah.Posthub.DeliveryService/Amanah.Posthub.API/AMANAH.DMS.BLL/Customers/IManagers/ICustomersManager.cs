﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.DeliverService.BLL.Customers.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;
namespace Amanah.Posthub.BLL.IManagers
{
    public interface ICustomersManager : IBaseManager<CustomerResultDto, Customer>
    {
        Task<CustomerResultDto> Get(int id);
        Task<List<CustomerResultDto>> GetByNameAsync(string name);
        Task<List<CustomerResultDto>> GetByPhoneAsync(string phone);
        Task<bool> IsEmailExistAsync(string email);
        Task<bool> IsPhoneExistAsync(string phone);
        Task<PagedResult<CustomerResultDto>> GetAllCustomersByPaginationAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<List<CustomerExportToExcelViewModel>> ExportExcel(PaginatedItemsViewModel pagingparametermodel);
        Task<List<CustomerNamesDto>> GetAllCustomerNamesAsync();
        Task<List<CustomerViewModel>> GetAllWithoutBranchCoditionAsync<CustomerViewModel>();



    }
}
