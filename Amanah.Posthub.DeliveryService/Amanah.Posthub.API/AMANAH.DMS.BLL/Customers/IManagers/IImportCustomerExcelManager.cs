﻿using Amanah.Posthub.BLL.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IImportCustomerExcelManager
    {
        Task<ProcessResult<List<ImportCustomerResultDto>>> AddFromExcelSheetAsync(string path, IFormFile Uploadfile);
    }
}

