﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.Customers.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers
{
    public class CustomersManager : BaseManager<CustomerResultDto, Customer>, ICustomersManager
    {


        public CustomersManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Customer> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<CustomerResultDto> Get(int id)
        {
            return await (context as ApplicationDbContext).Customers
                .Where(x => x.Id == id)
                .ProjectTo<CustomerResultDto>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<List<CustomerResultDto>> GetByNameAsync(string name)
        {
            return await (context as ApplicationDbContext).Customers
                .IgnoreQueryFilters()
                .Where(x =>
                    x.BranchId == null &&
                    x.Name.ToLower().Contains(name.ToLower()))
                .Take(10)
                .ProjectTo<CustomerResultDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }


        public async Task<List<CustomerResultDto>> GetByPhoneAsync(string phone)
        {
            return await (context as ApplicationDbContext).Customers
                .IgnoreQueryFilters()
                .Where(x =>
                    x.BranchId == null &&
                    x.Phone.ToLower().Contains(phone.ToLower()))
                .Take(10)
                .ProjectTo<CustomerResultDto>(mapper.ConfigurationProvider)
                .ToListAsync();
        }



        public async Task<bool> IsEmailExistAsync(string email)
        {
            return await (context as ApplicationDbContext).Customers.IgnoreQueryFilters().AnyAsync(x => x.Email == email);
        }

        public async Task<bool> IsPhoneExistAsync(string phone)
        {
            return await (context as ApplicationDbContext).Customers.AnyAsync(x => x.Phone == phone);
        }

        public override Task<List<CustomerViewModel>> GetAllAsync<CustomerViewModel>()
        {
            return repository.GetAll()
                .IgnoreQueryFilters()
                .Where(x => x.BranchId == null)
                .ProjectTo<CustomerViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        public Task<List<CustomerViewModel>> GetAllWithoutBranchCoditionAsync<CustomerViewModel>()
        {
            return repository.GetAll()
                .ProjectTo<CustomerViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<PagedResult<CustomerResultDto>> GetAllCustomersByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var source = repository.GetAll().Where(x => x.BranchId == null);
            if (pagingparametermodel.SearchBy != null)
            {
                source = source.Where(ApplyFilter(pagingparametermodel));
            }
            var pagedResult = source.ToPagedResultAsync<Customer, CustomerResultDto>(
                pagingparametermodel, mapper.ConfigurationProvider);
            return pagedResult;
        }

        public override Task<PagedResult<CustomerResultDto>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = repository
                .GetAll()
                .Where(x => x.BranchId == null)
                .ToPagedResultAsync<Customer, CustomerResultDto>(pagingparametermodel, mapper.ConfigurationProvider);
            return pagedResult;
        }

        public async Task<List<CustomerExportToExcelViewModel>> ExportExcel(PaginatedItemsViewModel pagingparametermodel)
        {

            var source = repository.GetAll().Where(x => x.BranchId == null);
            if (pagingparametermodel.SearchBy != null)
            {
                source = source.Where(ApplyFilter(pagingparametermodel));
            }
            var result = await source.ProjectTo<CustomerExportToExcelViewModel>(mapper.ConfigurationProvider).ToListAsync();

            return result;
        }



        private Expression<Func<Customer, bool>> ApplyFilter(PaginatedItemsViewModel pagingparametermodel)
        {
            return customer =>
                customer.Name.Contains(pagingparametermodel.SearchBy) ||
                customer.Email.Contains(pagingparametermodel.SearchBy) ||
                customer.Phone.Contains(pagingparametermodel.SearchBy) ||
                customer.Address.Contains(pagingparametermodel.SearchBy);


        }

        public async Task<List<CustomerNamesDto>> GetAllCustomerNamesAsync()
        {
            var names = await (context as ApplicationDbContext).Customers
              .IgnoreQueryFilters()
              .ProjectTo<CustomerNamesDto>(mapper.ConfigurationProvider)
              .ToListAsync();
            return names;
        }
    }
}
