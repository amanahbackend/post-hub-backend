﻿using Amanah.Posthub.BLL.Addresses.Services;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Managers
{
    public class ImportCustomerExcelManager : IImportCustomerExcelManager
    {
        private readonly IMapper _mapper;
        private readonly IApplicationUserManager _appUserManager;
        private readonly ICustomersManager _customerManager;
        private readonly ICountryManager _countryManager;
        private readonly ILocalizer _localizer;
        private readonly IUploadFormFileService _uploadFormFileService;

        public ImportCustomerExcelManager(
            IMapper mapper,
            IApplicationUserManager appUserManager,
            ICustomersManager customerManager,
            ICountryManager countryManager,
            ILocalizer localizer,
            IUploadFormFileService uploadFormFileService)
        {
            _mapper = mapper;
            _appUserManager = appUserManager;
            _customerManager = customerManager;
            _countryManager = countryManager;
            _localizer = localizer;
            _uploadFormFileService = uploadFormFileService;
        }

        public async Task<ProcessResult<List<ImportCustomerResultDto>>> AddFromExcelSheetAsync(string path, IFormFile Uploadfile)
        {
            ProcessResult<List<ImportCustomerResultDto>> result = new ProcessResult<List<ImportCustomerResultDto>>();
            try
            {
                var countries = await _countryManager.GetAllAsync<CountryResultDto>(true);
                ImportCustomerResultDto GetCustomer(IList<string> rowData, IList<string> columnNames)
                {
                    return new ImportCustomerResultDto()
                    {
                        //  Id = rowData[columnNames.IndexFor("Id")].ToInt32(),
                        Name = rowData[columnNames.IndexFor("Name")].ToString(),
                        Email = rowData[columnNames.IndexFor("Email")].ToString(),
                        Phone = rowData[columnNames.IndexFor("Phone")].ToString(),
                        Address = rowData[columnNames.IndexFor("Address")].ToString(),
                        Latitude = rowData[columnNames.IndexFor("Latitude")].ToDoubleNullable(),
                        Longitude = rowData[columnNames.IndexFor("Longitude")].ToDoubleNullable(),
                        Tags = rowData[columnNames.IndexFor("Tags")].ToString(),
                        CountryId = countries.Single(t => t.Name == rowData[columnNames.IndexFor("Country")].ToString()).Id,

                    };
                }
                if (string.IsNullOrEmpty(path))
                {
                    result.IsSucceeded = false;
                    result.Exception = new Exception("There is no file uploaded");
                    return result;
                }

                ProcessResult<string> uploadedFile = await _uploadFormFileService.UploadFileAsync(Uploadfile, path);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(uploadedFile.ReturnData, path);
                    var dataList = ExcelReader.GetDataToList(excelPath, GetCustomer);
                    result.ReturnData = await AddCustomerAsync((List<ImportCustomerResultDto>)dataList);
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }

        public async Task<List<ImportCustomerResultDto>> AddCustomerAsync(List<ImportCustomerResultDto> createCustomersVM)
        {
            var ImportCustomerViewModels = new List<ImportCustomerResultDto>();
            foreach (var Customer in createCustomersVM)
            {
                var result = await AddCustomerAsync(Customer);
                ImportCustomerViewModels.Add(result);
            }
            return ImportCustomerViewModels;
        }

        public async Task<ImportCustomerResultDto> AddCustomerAsync(ImportCustomerResultDto createCustomerVM)
        {
            createCustomerVM = CustomerValidator(createCustomerVM);
            if (!string.IsNullOrEmpty(createCustomerVM.Error))
                return createCustomerVM;

            var existUser = await _customerManager.IsEmailExistAsync(createCustomerVM.Email);
            if (existUser == true)
            {
                createCustomerVM.Error = _localizer[Keys.Validation.EmailAlreadyExists];
                return createCustomerVM;
            }

            var existUserphone = await _customerManager.IsPhoneExistAsync(createCustomerVM.Phone);
            if (existUserphone == true)
            {
                createCustomerVM.Error = "This Phone is already exist";
                return createCustomerVM;
            }

            var customerToSave = _mapper.Map<CustomerResultDto>(createCustomerVM);
            customerToSave.Tags = string.IsNullOrEmpty(customerToSave.Tags) ? null : customerToSave.Tags;
            await _customerManager.AddAsync(customerToSave);

            var CustomerToCreate = _mapper.Map<CustomerResultDto>(createCustomerVM);
            if (!string.IsNullOrEmpty(createCustomerVM.Error))
                return createCustomerVM;

            return createCustomerVM;

        }

        private ImportCustomerResultDto CustomerValidator(ImportCustomerResultDto createCustomerVM)
        {
            const int MinCharacters = 1;
            const int MaxCharacters = 100;
            if (string.IsNullOrWhiteSpace(createCustomerVM.Name) || createCustomerVM.Name.Length > MaxCharacters)
            {
                createCustomerVM.Error = _localizer.Format(
                    Keys.Validation.NameMustBeBetweenMinAndMaxCharacters,
                    MinCharacters,
                    MaxCharacters);
                return createCustomerVM;
            }

            if (string.IsNullOrEmpty(createCustomerVM.Phone))
            {
                createCustomerVM.Error = _localizer[Keys.Validation.PhoneNumberIsRequired];
                return createCustomerVM;
            }


            if (string.IsNullOrEmpty(createCustomerVM.Name))
            {
                createCustomerVM.Error = _localizer[Keys.Validation.NameIsRequired];
                return createCustomerVM;
            }

            if (string.IsNullOrEmpty(createCustomerVM.Email))
            {
                createCustomerVM.Error = _localizer[Keys.Validation.EmailIsRequired];
                return createCustomerVM;
            }

            if (string.IsNullOrEmpty(createCustomerVM.Address))
            {
                createCustomerVM.Error = _localizer[Keys.Validation.AddressIsRequired];
                return createCustomerVM;
            }

            return createCustomerVM;
        }
    }
}

