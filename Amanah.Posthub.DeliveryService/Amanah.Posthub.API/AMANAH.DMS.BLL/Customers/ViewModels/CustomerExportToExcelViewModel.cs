﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class CustomerExportToExcelViewModel
    {
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public string Tags { set; get; }
        public string Country { set; get; }
    }
}
