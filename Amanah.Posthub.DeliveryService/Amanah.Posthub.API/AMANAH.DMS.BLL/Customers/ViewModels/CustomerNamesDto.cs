﻿namespace Amanah.Posthub.DeliverService.BLL.Customers.ViewModels
{
    public class CustomerNamesDto
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
