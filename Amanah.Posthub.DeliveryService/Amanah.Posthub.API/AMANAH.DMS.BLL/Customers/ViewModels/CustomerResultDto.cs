﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using FluentValidation;
using System.ComponentModel.DataAnnotations;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class CustomerResultDto
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        [Required]
        public double? Latitude { set; get; }
        [Required]
        public double? Longitude { set; get; }
        public string Tags { set; get; }
        public int? CountryId { set; get; }
        public CountryResultDto Country { get; set; }
        public int? BranchId { set; get; }
        public AddressResultDto Location { set; get; }
        public string Code { get; set; }

    }


    public class ImportCustomerResultDto : CustomerResultDto
    {
        public string Error { get; set; }
    }

    public class CustomerResultDtoValidator : AbstractValidator<CustomerResultDto>
    {
        public CustomerResultDtoValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.Email).EmailAddress().MaximumLength(250);
            RuleFor(x => x.Phone).NotEmpty().NotNull().MaximumLength(50);
            RuleFor(x => x.Address).NotEmpty().NotNull();
            RuleFor(x => x.Longitude).NotEmpty().NotNull();
            RuleFor(x => x.Latitude).NotEmpty().NotNull();

        }
    }
}
