﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.DriverFloor.Queries.DTOs
{
   public class DriverFloorResponseDTO
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public string Floor { get; set; }
        public int DriverId { get; set; }
    }
}
