﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.DriverFloor.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.DriverFloor.Queries
{
    public interface IDriverFloorQuery
    {
        Task<List<DriverFloorResponseDTO>> GetDriverFloors(int BranchId, int? DriverId = null);


    }
    public class DriverFloorQuery : IDriverFloorQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public DriverFloorQuery(
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }
        public async Task<List<DriverFloorResponseDTO>> GetDriverFloors(int BranchId, int? DriverId = null)
        {
            try
            {
                var driverFloors = await context.DriverFloors.Where(c => c.BranchId == BranchId && !c.Driver.IsDeleted && (DriverId == null || c.DriverId != DriverId))
                                 .ProjectTo<DriverFloorResponseDTO>(mapper.ConfigurationProvider)
                                 .ToListAsync();
                return driverFloors;
            }
            catch (Exception ex)
            {
                throw new Exception("exception " + ex.Message);
            }

        }
    }
}
