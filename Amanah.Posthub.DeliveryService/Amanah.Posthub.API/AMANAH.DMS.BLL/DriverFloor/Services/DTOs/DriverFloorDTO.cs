﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.DriverFloor.Services.DTOs
{
    public class DriverFloorDTO
    {
        public int BranchId { get; set; }
        public string Floor { get; set; }
        public int DriverId { get; set; }
    }
}
