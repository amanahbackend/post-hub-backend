﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs
{
    public class DriverAttachmentDTO
    {
        public int Id { get; set; }
        //public int DriverRegistrationId { get; set; }
        public DriverAttachmentType Type { get; set; }
        public string File { get; set; }

        public IFormFile FormFile { get; set; }
    }
}
