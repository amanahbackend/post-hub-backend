﻿namespace Amanah.Posthub.BLL.DriverRegistrations.ViewModels
{
    public class DriverRegisterIdDTO
    {
        public int DriverId { get; set; }
        public string Username { get; set; }
        public int? DriverRegistrationId { get; set; }
    }
}
