﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using System;

namespace Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs
{
    public class DriverRegistrationRequestsResultDTO
    {
        public int Id { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string AreaName => AreaId;
        public string AreaId { get; set; }
        public string JobStatusName { get; set; }
        public string TransportTypeName { get; set; }
        public string Comment { get; set; }
        public UserRegistrationStatus UserRegistrationStatus { get; set; }
        public string ApprovedOrRejectedByUserFullName { get; set; }
    }
}
