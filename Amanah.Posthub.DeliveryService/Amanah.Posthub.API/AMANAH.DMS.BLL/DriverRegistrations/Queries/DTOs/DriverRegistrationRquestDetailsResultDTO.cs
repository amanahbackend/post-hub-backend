﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs
{
    public class DriverRegistrationRquestDetailsResultDTO
    {
        public int Id { get; set; }
        public UserRegistrationStatus UserRegistrationStatus { get; set; }
        public string RejectReason { get; set; }
        public string LicensePlate { get; set; }
        public int TransportTypeId { set; get; }
        public string TransportColor { get; set; }
        public string TransportYearAndModel { get; set; }
        public int TrainingRate { get; set; }
        public string TrainingLanguage { get; set; }
        public string TrainingComments { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string AreaId { get; set; }
        public string Email { get; set; }
        public string JobStatusName { get; set; }
        public string Comment { get; set; }
        public List<DriverAttachmentDTO> DriverAttachments { get; set; }
    }
}
