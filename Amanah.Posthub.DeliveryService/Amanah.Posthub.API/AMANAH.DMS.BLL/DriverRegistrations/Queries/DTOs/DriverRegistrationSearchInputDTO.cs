﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs
{
    public class DriverRegistrationSearchInputDTO : PaginatedItemsViewModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string UserID { get; set; }

        public List<UserRegistrationStatus> UserRegistrationStatuses { get; set; }
    }
}