﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.DriverRegistrations.ViewModels
{
    public class GetDriverRegistrationDuplicationInputDTO
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
    }

    public class DriverRegistrationDuplicateResponseDTO
    {
        public IEnumerable<string> Errors { get; set; }
    }
}
