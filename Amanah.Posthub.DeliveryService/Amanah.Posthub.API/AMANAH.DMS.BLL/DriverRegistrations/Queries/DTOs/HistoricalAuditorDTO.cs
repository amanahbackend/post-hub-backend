﻿namespace Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs
{
    public class HistoricalAuditorDTO
    {
        public string UserId { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string FirstName { get; internal set; }

        public string LastName { get; internal set; }
    }
}
