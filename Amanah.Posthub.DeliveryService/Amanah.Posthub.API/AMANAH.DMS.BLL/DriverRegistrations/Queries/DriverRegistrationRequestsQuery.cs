﻿using Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs;
using Amanah.Posthub.BLL.DriverRegistrations.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.DriverRegistrations.Queries
{
    public interface IDriverRegistrationRequestsQuery
    {
        public Task<PagedResult<DriverRegistrationRequestsResultDTO>> GetAllDriverByPaginationAsync(DriverRegistrationSearchInputDTO filter);
        public Task<DriverRegistrationRquestDetailsResultDTO> GetByIdAsync(int id);
        public Task<PagedResult<HistoricalAuditorDTO>> GetHistoricalAuditorsAsync(PaginatedItemsViewModel paginatedItems);
        public Task<DriverRegistration> GetAsync(int id);
        public Task<DriverRegistrationDuplicateResponseDTO> GetDuplicateValidationAsync(
         GetDriverRegistrationDuplicationInputDTO model);
        Task<string> GetPersonalPhotoAsync(int id);
        Task<int?> GetRegistrationIdByDriverIdAsync(int driverId, string username);
        Task<List<DriverRegisterIdDTO>> GetRegistrationIdByDriverIdAsync(List<DriverRegisterIdDTO> drivers);


    }
    public class DriverRegistrationRequestsQuery : IDriverRegistrationRequestsQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ILocalizer _localizer;
        public DriverRegistrationRequestsQuery(
            IMapper mapper,
            ApplicationDbContext context,
           ILocalizer localizer)
        {
            this.context = context;
            this.mapper = mapper;
            _localizer = localizer;
        }
        public async Task<PagedResult<DriverRegistrationRequestsResultDTO>> GetAllDriverByPaginationAsync(DriverRegistrationSearchInputDTO filter)
        {
            var source = context.DriverRegistrations.IgnoreQueryFilters().AsQueryable();
            if (filter.StartDate.HasValue)
            {
                source = source.Where(registration => filter.StartDate.Value.Date <= registration.CreatedDate.Date);
            }
            if (filter.EndDate.HasValue)
            {
                source = source.Where(registration => filter.EndDate.Value.Date >= registration.CreatedDate.Date);
            }
            if (filter.UserRegistrationStatuses != null && filter.UserRegistrationStatuses.Any())
            {
                source = source.Where(registration => filter.UserRegistrationStatuses.Contains(registration.UserRegistrationStatus));
            }
            if (!string.IsNullOrWhiteSpace(filter.UserID))
            {
                source = source.Where(registration => registration.ApprovedORejectedByUserId == filter.UserID);
            }
            var pagedResult = await source
                .OrderByDescending(registration => registration.CreatedDate)
                .ToPagedResultAsync<DriverRegistration, DriverRegistrationRequestsResultDTO>(
                filter, mapper.ConfigurationProvider);
            return pagedResult;
        }
        public async Task<DriverRegistrationRquestDetailsResultDTO> GetByIdAsync(int id)
        {
            return await context.DriverRegistrations
                .IgnoreQueryFilters()
                .Where(registration => registration.Id == id)
                .ProjectTo<DriverRegistrationRquestDetailsResultDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<DriverRegistration> GetAsync(int id)
        {
            return await context.DriverRegistrations
                .IgnoreQueryFilters()
                .Include(driverRegistration => driverRegistration.DriverAttachments)
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }


        public async Task<PagedResult<HistoricalAuditorDTO>> GetHistoricalAuditorsAsync(PaginatedItemsViewModel paginatedItems)
        {
            var statuses = new[] { UserRegistrationStatus.Approved, UserRegistrationStatus.Rejected };
            var result = await context.DriverRegistrations
                .IgnoreQueryFilters()
                .Where(registration => statuses.Contains(registration.UserRegistrationStatus))
                .Select(registration => registration.ApprovedORejectedByUser)
                .Distinct()
                .Where(user =>
                    string.IsNullOrWhiteSpace(paginatedItems.SearchBy) ||
                    user.FirstName.Contains(paginatedItems.SearchBy) ||
                    user.LastName.Contains(paginatedItems.SearchBy) ||
                    user.Email.Contains(paginatedItems.SearchBy) ||
                    user.UserName.Contains(paginatedItems.SearchBy))
                .Select(user =>
                    new HistoricalAuditorDTO
                    {
                        UserId = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName
                    })
                .ToPagedResultAsync(paginatedItems);
            return result;
        }

        public async Task<string> GetPersonalPhotoAsync(int id)
        {
            var driverRegistration = await InternalGetByIdAsync(id);
            return driverRegistration.DriverAttachments
               .FirstOrDefault(attachment => attachment.Type == DriverAttachmentType.PersonalPhoto)
               ?.File;
        }

        public async Task<int?> GetRegistrationIdByDriverIdAsync(int driverId, string username)
        {
            var driverRegistration = await context.DriverRegistrations.IgnoreQueryFilters()
                 .Where(registration => registration.UserRegistrationStatus == UserRegistrationStatus.Approved)
                 .Where(registration => registration.DriverId == driverId ||
                  registration.UserName.Trim().ToLower() == username.Trim().ToLower())
                 .AsNoTracking()
                 .FirstOrDefaultAsync();
            if (driverRegistration == null)
            {
                return null;
            }
            if (!driverRegistration.DriverId.HasValue)
            {
                driverRegistration.DriverId = driverId;
                context.Update(driverRegistration);
                await context.SaveChangesAsync();
            }
            return driverRegistration.Id;
        }

        public async Task<List<DriverRegisterIdDTO>> GetRegistrationIdByDriverIdAsync(List<DriverRegisterIdDTO> drivers)
        {
            if (drivers != null)
            {
                var driverIds = drivers.Select(driver => driver.DriverId).ToList();
                var driverUserNames = drivers.Select(driver => driver.Username.Trim().ToLower()).ToList();

                var driverRegistrations = await context.DriverRegistrations
                    .IgnoreQueryFilters()
                   .Where(registration => registration.UserRegistrationStatus == UserRegistrationStatus.Approved)
                   .Where(registration => (registration.DriverId.HasValue && driverIds.Contains(registration.DriverId.Value)) ||
                    driverUserNames.Contains(registration.UserName.Trim().ToLower()))
                   .AsNoTracking()
                   .ToListAsync();

                foreach (var driver in drivers)
                {
                    var matchedDriverRegistration = driverRegistrations.FirstOrDefault(registration => (registration.DriverId.HasValue && driverIds.Contains(registration.DriverId.Value)) ||
                     driverUserNames.Contains(registration.UserName.Trim().ToLower()));
                    if (matchedDriverRegistration != null)
                    {
                        driver.DriverRegistrationId = matchedDriverRegistration.Id;
                        if (!matchedDriverRegistration.DriverId.HasValue)
                        {
                            matchedDriverRegistration.DriverId = driver.DriverId;
                            context.Update(matchedDriverRegistration);
                        }
                    }
                }
                await context.SaveChangesAsync();
            }
            return drivers;
        }

        private async Task<DriverRegistration> InternalGetByIdAsync(int id)
        {
            return await context.DriverRegistrations
                .IgnoreQueryFilters()
                .Include(driverRegistration => driverRegistration.DriverAttachments)
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }




        public async Task<DriverRegistrationDuplicateResponseDTO> GetDuplicateValidationAsync(GetDriverRegistrationDuplicationInputDTO model)
        {
            var allUsers = context.Users.IgnoreQueryFilters();
            var allRegistrations = context.DriverRegistrations.IgnoreQueryFilters();
            var errors = new List<string>();

            async Task<bool> EmailExistsAsyncAsync(string email)
            {
                var registrationExists = await allRegistrations.AnyAsync(
                    registration => registration.Email.ToUpper() == email.ToUpper());
                var userExists = await allUsers.AnyAsync(
                    user => user.NormalizedEmail == email.ToUpper());
                return registrationExists || userExists;
            }

            async Task<bool> UserNameExistsAsync(string userName)
            {
                var registrationExists = await allRegistrations.AnyAsync(
                   registration => registration.UserName.ToUpper() == model.UserName.ToUpper());
                var userExists = await allUsers.AnyAsync(
                    user => user.NormalizedUserName == model.UserName.ToUpper());
                return registrationExists || userExists;
            }

            async Task<bool> PhoneExistsAsync(string phone)
            {
                var registrationExists = await allRegistrations.AnyAsync(
                    registration => registration.PhoneNumber.ToUpper() == model.Phone.ToUpper());
                var userExists = await allUsers.AnyAsync(
                    user => user.PhoneNumber.ToUpper() == model.Phone.ToUpper());
                return registrationExists || userExists;
            }

            if (!string.IsNullOrWhiteSpace(model.Email))
            {
                if (await EmailExistsAsyncAsync(model.Email))
                {
                    errors.Add(_localizer[Keys.Validation.EmailAlreadyExists]);
                }
            }
            if (!string.IsNullOrWhiteSpace(model.UserName))
            {
                if (await UserNameExistsAsync(model.UserName))
                {
                    errors.Add(_localizer.Format(Keys.Validation.UserNameAlreadyExists, model.UserName));
                }
            }
            if (!string.IsNullOrWhiteSpace(model.Phone))
            {
                if (await PhoneExistsAsync(model.Phone))
                {
                    errors.Add(_localizer[Keys.Validation.PhoneAlreadyExists]);
                }
            }

            return new DriverRegistrationDuplicateResponseDTO
            {
                Errors = errors
            };
        }
    }
}
