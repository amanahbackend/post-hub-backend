﻿namespace Amanah.Posthub.ViewModels
{
    public class ApproveDriverRegistrationRequestInputDTO : CreateDriverWithoutPasswordViewModel
    {
        public int? DriverRegistrationId { get; set; }
    }

}
