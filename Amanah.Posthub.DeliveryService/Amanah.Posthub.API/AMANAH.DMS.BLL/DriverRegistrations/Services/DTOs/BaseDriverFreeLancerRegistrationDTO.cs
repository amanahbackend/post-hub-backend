﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class BaseDriverFreeLancerRegistrationDTO
    {
        public string AreaId { get; set; }
        public int CountryId { get; set; }

        public string UserName { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public List<int> PlatFormZones { set; get; }
        public string JobStatusName { get; set; }
        public string LicensePlate { get; set; }
        public int TransportTypeId { set; get; }

        public int TrainingRate { get; set; }
        public string TrainingLanguage { get; set; }
        public string TrainingComments { get; set; }

        public string TransportColor { get; set; }
        public string TransportYearAndModel { get; set; }

        public List<CreatedDriverAttachmentDTO> CreatedDriverAttachments { get; set; }

    }
}
