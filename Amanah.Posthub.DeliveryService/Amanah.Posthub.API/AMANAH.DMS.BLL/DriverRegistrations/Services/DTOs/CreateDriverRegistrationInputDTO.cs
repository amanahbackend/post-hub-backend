﻿using Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class CreateDriverRegistrationInputDTO : CreateDriverTenantRegistrationDTO
    {
        public int Id { get; set; }
        public UserRegistrationStatus UserRegistrationStatus { get; set; }
        public string ApprovedOrRejectedByUserFullName { get; set; }
        public string ApprovedORejectedByUserId { get; set; }
        public string RejectReason { get; set; }
        public List<int> PlatFormZones { set; get; }
        public string Comment { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string AreaName => AreaId;
        public string Tenant_Id { get; set; }
        public string TransportTypeName { get; set; }

        public List<DriverAttachmentDTO> DriverAttachments { get; set; }

    }

    public class UpdateDriverRegistrationDetailsInputDTO
    {
        public int Id { get; set; }

        public UserRegistrationStatus UserRegistrationStatus { get; set; }

        public string RejectReason { get; set; }
        // Vechicle

        public string LicensePlate { get; set; }

        public int TransportTypeId { set; get; }

        public string TransportColor { get; set; }

        public string TransportYearAndModel { get; set; }



        public int TrainingRate { get; set; }

        public string TrainingLanguage { get; set; }

        public string TrainingComments { get; set; }


        // Personal Info
        public string FullName { get; set; }
        public string UserName { get; set; }

        public string PhoneNumber { get; set; }
        public string AreaId { get; set; }
        public string Email { get; set; }
        public string JobStatusName { get; set; }
        //Auditing
        public string Comment { get; set; }
        public List<DriverAttachmentDTO> DriverAttachments { get; set; }

        public class DriverRegistrationDetailsViewModelValidator : AbstractValidator<UpdateDriverRegistrationDetailsInputDTO>
        {
            public DriverRegistrationDetailsViewModelValidator()
            {
                RuleForEach(model => model.DriverAttachments)
                    .Must(attachment =>
                        attachment.Id != default ||
                        attachment.FormFile != null && attachment.Type == DriverAttachmentType.Auditing);
            }
        }
    }
}
