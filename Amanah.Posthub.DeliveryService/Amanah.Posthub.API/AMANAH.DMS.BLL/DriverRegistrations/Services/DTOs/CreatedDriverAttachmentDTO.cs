﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class CreatedDriverAttachmentDTO
    {
        public DriverAttachmentType Type { get; set; }
        public IFormFile FormFile { get; set; }
    }
}
