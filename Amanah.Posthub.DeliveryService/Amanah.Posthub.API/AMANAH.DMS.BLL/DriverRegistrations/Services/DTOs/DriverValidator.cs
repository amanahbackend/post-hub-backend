﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.DriverRegistrations.ViewModels
{
    public class DriverValidator : AbstractValidator<BaseDriverFreeLancerRegistrationDTO>
    {
        private static readonly List<DriverAttachmentType> _requriredTypes = new List<DriverAttachmentType>()
            {
                DriverAttachmentType. PersonalPhoto,
                DriverAttachmentType.NationalId,
                DriverAttachmentType.DrivingLicense,
                DriverAttachmentType.CarFrontSide,
                DriverAttachmentType.CarBackSide,
                DriverAttachmentType.CarLeftSide,
                DriverAttachmentType.CarRightSide,
            };
        private static readonly string[] _imageExtentions = { ".jpg", ".jpeg", ".png", ".png", ".bmp" };
        private static readonly string[] _pdfextentions = { ".pdf" };

        public DriverValidator(ILocalizer localizer)
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(driverRegistration => driverRegistration.Password)
                .NotNull()
                .NotEmpty()
                .WithMessage(localizer[Keys.Validation.MissingDriverPassword]);

            RuleFor(driverRegistration => driverRegistration.CreatedDriverAttachments)
                .NotEmpty()
                .Must(HasRequiredAttachments)
                    .WithMessage(localizer[Keys.Validation.MissingAttachmentInCreateDriver]);

            RuleForEach(driverRegistration => driverRegistration.CreatedDriverAttachments)
                .Must(HasValidExtension)
                    .WithMessage(localizer[Keys.Validation.InvalidAttachmentExtention]);
        }

        private bool HasRequiredAttachments(List<CreatedDriverAttachmentDTO> attachments)
        {
            return !_requriredTypes.Except(attachments.Select(attachment => attachment.Type)).Any();
        }

        private bool HasValidExtension(CreatedDriverAttachmentDTO attachment)
        {
            var extension = Path.GetExtension(attachment.FormFile.FileName);
            return _imageExtentions.Contains(extension, StringComparer.OrdinalIgnoreCase) ||
                attachment.Type == DriverAttachmentType.TermsAndConditions &&
                _pdfextentions.Contains(extension, StringComparer.OrdinalIgnoreCase);
        }
    }
}
