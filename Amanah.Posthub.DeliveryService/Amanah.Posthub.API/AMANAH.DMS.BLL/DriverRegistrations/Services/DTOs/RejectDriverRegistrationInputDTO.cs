﻿namespace Amanah.Posthub.BLL.DriverRegistrations.ViewModels
{
    public class RejectDriverRegistrationInputDTO
    {
        public int DriverId { get; set; }

        public string Reason { get; set; }
    }
}
