﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Extensions;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.DriverRegistrations.Services
{
    public interface IDriverRegistrationService
    {

        Task AddAsync(CreateDriverRegistrationInputDTO driverRegistrationViewModel);

        Task UpdateAsync(UpdateDriverRegistrationDetailsInputDTO model);

        Task RejectAsync(int id, string rejectReason);

        Task ApproveAsync(ApproveDriverRegistrationRequestInputDTO approvedDTO);


    }
    public class DriverRegistrationRequestsService : IDriverRegistrationService
    {
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;
        private readonly ILocalizer _localizer;
        private readonly IRepository<DriverRegistration> _repository;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IAccountLogManager _accountLogManager;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher;
        private readonly IUserDomainService _userDomainService;
        private readonly IRepository<Driver> _driverRepository;

        public DriverRegistrationRequestsService(
            IAccountLogManager accountLogManager,
            IMapper mapper,
            IRepository<DriverRegistration> repository,
            IUnitOfWork unityOfWork,
            ILocalizer localizer,
           ICurrentUser currentUser,
           IUploadFormFileService uploadFormFileService,
           IPasswordHasher<ApplicationUser> passwordHasher,
           IUserDomainService userDomainService,
           IRepository<Driver> driverRepository)
        {
            _currentUser = currentUser;
            _repository = repository;
            _unityOfWork = unityOfWork;
            _accountLogManager = accountLogManager;
            _localizer = localizer;
            _mapper = mapper;
            _uploadFormFileService = uploadFormFileService;
            _passwordHasher = passwordHasher;
            _userDomainService = userDomainService;
            _driverRepository = driverRepository;
        }


        public async Task AddAsync(CreateDriverRegistrationInputDTO driverRegistrationViewModel)
        {
            var hasRegistration = await _repository.GetAllIQueryable()
                .IgnoreQueryFilters()
                .Where(registration => registration.UserRegistrationStatus != UserRegistrationStatus.Rejected)
                .Where(registration =>
                    !string.IsNullOrWhiteSpace(driverRegistrationViewModel.Email) &&
                    driverRegistrationViewModel.Email.Trim().ToLower() == registration.Email.Trim().ToLower() ||
                    !string.IsNullOrWhiteSpace(driverRegistrationViewModel.PhoneNumber) &&
                    driverRegistrationViewModel.PhoneNumber.Trim() == registration.PhoneNumber.Trim() ||
                    !string.IsNullOrWhiteSpace(driverRegistrationViewModel.UserName) &&
                    driverRegistrationViewModel.UserName.Trim() == registration.UserName.Trim())
                .AnyAsync();

            if (hasRegistration)
            {
                throw new DomainException("RegisteredBefore");
            }
            await _unityOfWork.RunTransaction(async () =>
            {
                _unityOfWork.IsTenantFilterEnabled = false;

                driverRegistrationViewModel.DriverAttachments = await UploadDriverAttachmentAsync(
                driverRegistrationViewModel.CreatedDriverAttachments);

                DriverRegistration driverRegistration = _mapper.Map<DriverRegistration>(driverRegistrationViewModel);
                driverRegistration.UserRegistrationStatus = UserRegistrationStatus.New;
                driverRegistration.SecurityStamp = Guid.NewGuid().ToString("D");
                driverRegistration.PasswordHash = _passwordHasher.HashPassword(new ApplicationUser(), driverRegistrationViewModel.Password);
                _repository.Add(driverRegistration);
                await _unityOfWork.SaveChangesAsync();
            });
            await _accountLogManager.Create(new AccountLogs()
            {
                TableName = "DriverRegistration",
                ActivityType = "Create",
                Description = $"{driverRegistrationViewModel.FullName} has been registered ",
                Record_Id = 1
            });
        }

        public async Task UpdateAsync(UpdateDriverRegistrationDetailsInputDTO model)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                _unityOfWork.IsTenantFilterEnabled = false;

                var existingRegistration = await _repository.GetAllIQueryable()
                .IgnoreQueryFilters()
                .Include(registration => registration.DriverAttachments)
                .Where(registration => registration.Id == model.Id)
                .FirstOrDefaultAsync();
                _mapper.Map(model, existingRegistration);

                existingRegistration.UserRegistrationStatus = UserRegistrationStatus.Pending;
                var auditingFiles = (model.DriverAttachments ?? Enumerable.Empty<DriverAttachmentDTO>())
                    .Where(attachment => attachment.Type == DriverAttachmentType.Auditing);

                var oldAuditingFileIds = auditingFiles.Where(attachment => attachment.Id != default)
                    .Select(attachment => attachment.Id)
                    .ToArray();
                var addedFiles = auditingFiles.Where(attachment => attachment.Id == default)
                    .Select(attachment => new CreatedDriverAttachmentDTO
                    {
                        FormFile = attachment.FormFile,
                        Type = attachment.Type
                    })
                    .ToArray();
                var deletedFiles = existingRegistration.DriverAttachments
                    .Where(attachment => attachment.Type == DriverAttachmentType.Auditing)
                    .Where(attachment => !oldAuditingFileIds.Contains(attachment.Id))
                    .ToArray();
                existingRegistration.DriverAttachments.RemoveRange(deletedFiles);
                foreach (var file in deletedFiles)
                {
                    try
                    {
                        File.Delete(file.File);
                    }
                    catch (IOException)
                    {
                    }
                }
                var addedAttachments = (await UploadDriverAttachmentAsync(addedFiles))
                    .Select(attacmentModel => new DriverAttachment
                    {
                        File = attacmentModel.File,
                        Type = attacmentModel.Type,
                    })
                    .ToArray();
                existingRegistration.DriverAttachments.AddRange(addedAttachments);
                _repository.Update(existingRegistration);
                await _unityOfWork.SaveChangesAsync();
                await _accountLogManager.Create(new AccountLogs()
                {
                    TableName = "DriverRegistration",
                    ActivityType = "Update",
                    Description = $"{existingRegistration.FullName} ( {existingRegistration.Id} ) has been updated ",
                    Record_Id = existingRegistration.Id
                });
            });

        }

        public async Task RejectAsync(int id, string rejectReason)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                _unityOfWork.IsTenantFilterEnabled = false;
                var driverRegistration = await InternalGetByIdAsync(id);
                driverRegistration.UserRegistrationStatus = UserRegistrationStatus.Rejected;
                driverRegistration.RejectReason = rejectReason;
                driverRegistration.ApprovedORejectedByUserId = _currentUser.Id;
                _repository.Update(driverRegistration);
                await _unityOfWork.SaveChangesAsync();
                await NotifyDriverAboutRejection(driverRegistration);
                await _accountLogManager.Create(new AccountLogs()
                {
                    TableName = "DriverRegistration",
                    ActivityType = "Reject",
                    Description = $"{driverRegistration.FullName} ( {driverRegistration.Id} ) has been rejected ",
                    Record_Id = driverRegistration.Id
                });

            });
        }

        public async Task ApproveAsync(ApproveDriverRegistrationRequestInputDTO approveInputDTO)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                _unityOfWork.IsTenantFilterEnabled = false;
                var driverRegistration = await InternalGetByIdAsync((int)approveInputDTO.DriverRegistrationId);
                CreateDriverViewModel createDriverVM = _mapper.Map<CreateDriverViewModel>(approveInputDTO);

                driverRegistration.UserRegistrationStatus = UserRegistrationStatus.Approved;
                driverRegistration.ApprovedORejectedByUserId = _currentUser.Id;
                driverRegistration.Driver = await CreateDriverAsync(createDriverVM, driverRegistration); ;
                _repository.Update(driverRegistration);

                await _accountLogManager.Create(new AccountLogs()
                {
                    TableName = "DriverRegistration",
                    ActivityType = "Approve",
                    Description = $"{driverRegistration.FullName} ( {driverRegistration.Id} ) has been approved ",
                    Record_Id = driverRegistration.Id
                });
                await NotifyDriverAboutApproval(driverRegistration);
                await _unityOfWork.SaveChangesAsync();
            });
        }

        private async Task<Driver> CreateDriverAsync(CreateDriverViewModel createDriverVM, DriverRegistration driverRegistration)
        {
            if (driverRegistration.CountryId == default)
            {
                createDriverVM.CountryId = 121;
            }
            else
            {
                createDriverVM.CountryId = driverRegistration.CountryId;
            }
            var userManagerToCreate = _mapper.Map<ApplicationUserViewModel>(createDriverVM);
            var userToCreate = _mapper.Map<ApplicationUser>(userManagerToCreate);
            userToCreate.PasswordHash = driverRegistration.PasswordHash;
            userToCreate.SecurityStamp = driverRegistration.SecurityStamp;
            var driverPersonalPhoto = driverRegistration.DriverAttachments
              .FirstOrDefault(attachment => attachment.Type == DriverAttachmentType.PersonalPhoto)
              ?.File;
            if (!string.IsNullOrEmpty(createDriverVM.RoleName) || !string.IsNullOrWhiteSpace(createDriverVM.RoleName))
            {
                userToCreate.RoleNames.Add(createDriverVM.RoleName);
            }

            var driverUser = await _userDomainService.CreateAsync(userToCreate);
            if (driverUser != null)
            {
                var driverToCreate = _mapper.Map<Driver>(createDriverVM);
                driverToCreate.UserId = driverUser.Id;
                if (createDriverVM.FormFile != null)
                {
                    var path = "DriverImages";
                    var processResult = await _uploadFormFileService.UploadFileAsync(createDriverVM.FormFile, path);
                    if (processResult.IsSucceeded)
                    {
                        driverToCreate.ImageUrl = processResult.ReturnData;
                    }
                    else
                    {
                        throw new DomainException(processResult.Exception.Message);
                    }
                }
                else
                {
                    driverToCreate.ImageUrl = driverPersonalPhoto.Replace("DriverImages", "");
                }
                driverToCreate.AgentStatusId = (int)AgentStatusesEnum.Offline;
                driverToCreate.AllPickupGeoFences = true;
                driverToCreate.AllDeliveryGeoFences = true;
                _driverRepository.Add(driverToCreate);
                await _accountLogManager.Create(
                    new AccountLogs
                    {
                        TableName = "Driver",
                        ActivityType = "Create",
                        Description = $"{driverUser.FirstName} has been created ",
                        Record_Id = 1
                    });

                return driverToCreate;
            }
            else
            {
                throw new DomainException("Can't Create User..");
            }
        }


        private async Task<List<DriverAttachmentDTO>> UploadDriverAttachmentAsync(
            IEnumerable<CreatedDriverAttachmentDTO> attachments)
        {
            List<DriverAttachmentDTO> driverAttachments = new List<DriverAttachmentDTO>();
            foreach (var attachment in attachments)
            {
                var path = "DriverImages";
                var processResult = await _uploadFormFileService.UploadFileAsync(attachment.FormFile, path);
                if (!processResult.IsSucceeded)
                {
                    continue;
                }
                var fileName = $"{path}/{processResult.ReturnData}";
                driverAttachments.Add(new DriverAttachmentDTO
                {
                    Type = attachment.Type,
                    File = fileName
                });
            }
            return driverAttachments;
        }

        private async Task<DriverRegistration> InternalGetByIdAsync(int id)
        {
            return await _repository.GetAllIQueryable()
                .Include(driverRegistration => driverRegistration.DriverAttachments)
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        private async Task NotifyDriverAboutRejection(DriverRegistration driverRegistration)
        {
            //todo: will be by email or SMS 
        }
        private async Task NotifyDriverAboutApproval(DriverRegistration driverRegistration)
        {
            //todo: will be by email or SMS 
        }

    }
}
