﻿using Amanah.Posthub.BLL.DriverWorkingHour.Managers;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Extensions;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers
{

    public class DriverWorkingHourTrackingManager : IDriverWorkingHourTrackingManager
    {
        private readonly ApplicationDbContext _context;

        public DriverWorkingHourTrackingManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public Task AddDutyAsync(int driverId, bool isOn, bool byAdmin)
        {
            var status = isOn
                ? AgentStatusesEnum.Available
                : AgentStatusesEnum.Unavailable;
            var duty = isOn
                ? "On"
                : "Off";
            var note = byAdmin
                ? $"AdminSetDuty{duty}"
                : null;
            _context.Add(new DriverWorkingHourTracking(driverId, status, note));
            return _context.SaveChangesAsync();
        }



        public async Task AddLoginAsync(int driverId, string tenantId)
        {
            _context.Roles.AsNoTracking().IgnoreQueryFilters();

            _context.Add(DriverWorkingHourTracking.CreateLogin(tenantId, driverId));
            await _context.SaveChangesAsync();
        }

        public Task AddLogoutAsync(int driverId, bool byAdmin)
        {
            var note = byAdmin
                ? "LoggedOutByAdmin"
                : null;
            _context.Add(new DriverWorkingHourTracking(driverId, AgentStatusesEnum.Offline, note));
            return _context.SaveChangesAsync();
        }

        public Task AddSessionExpiredAsync(int driverId)
        {
            var note = "SessionExpired";
            _context.Add(new DriverWorkingHourTracking(driverId, AgentStatusesEnum.Offline, note));
            return _context.SaveChangesAsync();
        }

        public async Task<(int TotalCount, IReadOnlyCollection<DriverWorkingHourTrackingViewModel>)> GetAllAsync(
            PaginatedDriverWorkingHourTrackingViewModel filter)
        {
            var (totalCount, entries) = await GetFilteredTrackingEntriesAsync(filter, false);
            var result = DriverWorkingHoursCalculator.CalculateOld(entries);
            return (totalCount, result);
        }

        public async Task<PagedResult<DriverWorkingHourTrackingViewModel>> GetAllByPaginationAsync(
            PaginatedDriverWorkingHourTrackingViewModel filter)
        {
            var (totalCount, entries) = await GetFilteredTrackingEntriesAsync(filter, applyPaging: true);
            var resultItems = DriverWorkingHoursCalculator.CalculateOld(entries);
            var pagedResult = new PagedResult<DriverWorkingHourTrackingViewModel>
            {
                TotalCount = totalCount,
                Result = resultItems
            };
            return pagedResult;
        }

        private async Task<(int TotalCount, IReadOnlyCollection<DriverWorkingHourTracking>)> GetFilteredTrackingEntriesAsync(
            PaginatedDriverWorkingHourTrackingViewModel filter, bool applyPaging)
        {
            var startDate = filter.StartDate ?? DateTime.MinValue;
            var endDate = filter.EndDate ?? DateTime.MaxValue;
            var entriesQuery = _context.DriverWorkingHourTracking
              .IgnoreQueryFilters()
              .OrderBy(entry => entry.ActivityDateTime)
               .Where(entry =>
                   entry.ActivityDateTime.Date >= startDate.Date &&
                   entry.ActivityDateTime.Date <= endDate.Date);
            if (filter.DriverIds != null && filter.DriverIds.Any())
            {
                entriesQuery = entriesQuery.Where(driverTrackingEntry =>
                    filter.DriverIds.Contains(driverTrackingEntry.DriverId));
            }
            // Note: paging is on day and driver not ordinary paging
            var pagingQuery = entriesQuery.GroupBy(entry => new { entry.DriverId, entry.ActivityDateTime.Date })
                .Select(group => group.Key);
            var totalCount = await pagingQuery.CountAsync();
            //#if DEBUG
            //            var allPages = await pagingQuery.ToArrayAsync();
            //#endif
            if (applyPaging)
            {
                pagingQuery = pagingQuery.ApplyPaging(filter);
            }
            //#if DEBUG
            //            var pages = await pagingQuery.ToArrayAsync();
            //            var _entries =await (
            //                from entry in entriesQuery
            //                join driverDay in pagingQuery
            //                on new { entry.DriverId, entry.ActivityDateTime.Date } equals driverDay
            //                select new { entry, driverDay }).ToArrayAsync();
            //            var grouped = _entries.GroupBy(e => new { e.entry.ActivityDateTime, e.entry.DriverId }).ToArray();
            //#endif
            var entries =
                from entry in entriesQuery
                join driverDay in pagingQuery
                on new { entry.DriverId, entry.ActivityDateTime.Date } equals driverDay
                select entry;

            var result = await entries.Include(entry => entry.Driver.User).ToArrayAsync();
            foreach (var item in result)
            {
                item.ActivityDateTime = new DateTime(item.ActivityDateTime.Ticks, DateTimeKind.Utc);
            }
            return (totalCount, result);
        }
    }
}
