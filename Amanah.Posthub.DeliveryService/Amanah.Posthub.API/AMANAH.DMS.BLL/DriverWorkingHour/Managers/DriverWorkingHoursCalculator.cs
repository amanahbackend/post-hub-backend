﻿using Amanah.Posthub.BLL.DriverWorkingHour.Queries.DTOs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;

namespace Amanah.Posthub.BLL.DriverWorkingHour.Managers
{
    internal static class DriverWorkingHoursCalculator
    {
        public static IReadOnlyCollection<DriverWorkingHourTrackingViewModel> CalculateOld(
            IEnumerable<DriverWorkingHourTracking> entries)
        {
            return entries.GroupBy(entry => entry.Driver)
                .Select(driverEntries => new
                {
                    DriverId = driverEntries.Key.Id,
                    DriverName = driverEntries.Key.User.FullName,
                    AdjustedEntries = Adjust(driverEntries)
                })
                .SelectMany(driverAdjustedEntries =>
                    driverAdjustedEntries.AdjustedEntries
                        .WhereIsEvenIndex()
                        .Zip(driverAdjustedEntries.AdjustedEntries.WhereIsOddIndex())
                        .Select(period => new DriverStatusPeriod(period.First, period.Second))
                        .SelectMany(GetOrSplitIfCoverTwoDays)
                        .GroupBy(period => period.From.Date)
                        .Select(dayPeriods =>
                            new DriverWorkingHourTrackingViewModel(
                                dayPeriods.Key,
                                TimeSpan.FromHours(dayPeriods.Sum(period => period.TotalHours)),
                                driverAdjustedEntries.DriverId,
                                driverAdjustedEntries.DriverName)))
                .ToArray();
        }


        public static IReadOnlyCollection<DriverWorkingHourTrackingResponseDTO> Calculate(
        IEnumerable<DriverWorkingHourTracking> entries)
        {
            return entries.GroupBy(entry => entry.Driver)
                .Select(driverEntries => new
                {
                    DriverId = driverEntries.Key.Id,
                    DriverName = driverEntries.Key.User.FullName,
                    AdjustedEntries = Adjust(driverEntries)
                })
                .SelectMany(driverAdjustedEntries =>
                    driverAdjustedEntries.AdjustedEntries
                        .WhereIsEvenIndex()
                        .Zip(driverAdjustedEntries.AdjustedEntries.WhereIsOddIndex())
                        .Select(period => new DriverStatusPeriod(period.First, period.Second))
                        .SelectMany(GetOrSplitIfCoverTwoDays)
                        .GroupBy(period => period.From.Date)
                        .Select(dayPeriods =>
                            new DriverWorkingHourTrackingResponseDTO(
                                dayPeriods.Key,
                                TimeSpan.FromHours(dayPeriods.Sum(period => period.TotalHours)),
                                driverAdjustedEntries.DriverId,
                                driverAdjustedEntries.DriverName)))
                .ToArray();
        }

        private static IEnumerable<DriverWorkingHourTracking> Adjust(
           IEnumerable<DriverWorkingHourTracking> entries)
        {
            if (!entries.Any())
            {
                return entries;
            }
            var result = entries;
            if (entries.First().IsEnd())
            {
                result = result.Prepend(entries.First().CreateDayOpening());
            }
            if (entries.Last().IsStart())
            {
                result = result.Append(entries.Last().CreateDayClose());
            }
            result = RemoveConsecutiveDuplicates(result);
            return result;
        }

        private static IEnumerable<DriverWorkingHourTracking> RemoveConsecutiveDuplicates(
            IEnumerable<DriverWorkingHourTracking> entries)
        {
            DriverWorkingHourTracking previous = null;
            foreach (var entry in entries)
            {
                if (!entry.HasEquivalentStatus(previous))
                {
                    yield return entry;
                    previous = entry;
                }

            }
        }

        private static IEnumerable<DriverStatusPeriod> GetOrSplitIfCoverTwoDays(DriverStatusPeriod period)
        {
            if (period.From.Date == period.To.Date)
            {
                return Enumerable.Repeat(period, 1);
            }
            return new[]
            {
                new DriverStatusPeriod(period.Status, period.From, period.From.EndOfDay()),
                new DriverStatusPeriod(period.Status, period.To.BeginingOfDay(), period.To)
            };
        }

        internal class DriverStatusPeriod
        {
            public DateTime From { get; set; }

            public DateTime To { get; set; }

            public double TotalHours => Math.Round((To - From).TotalHours, 2, MidpointRounding.AwayFromZero);

            public AgentStatusesEnum Status { get; set; }

            public DriverStatusPeriod(AgentStatusesEnum status, DateTime from, DateTime to)
            {
                if (to < from)
                {
                    throw new InvalidOperationException();
                }
                Status = status;
                From = from;
                To = to;
            }

            public DriverStatusPeriod(DriverWorkingHourTracking first, DriverWorkingHourTracking second)
                : this((AgentStatusesEnum)first.StatusId, first.ActivityDateTime, second.ActivityDateTime)
            {
            }
        }
    }
}
