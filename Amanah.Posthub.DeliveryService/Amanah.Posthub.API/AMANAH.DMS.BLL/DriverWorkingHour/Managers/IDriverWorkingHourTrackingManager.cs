﻿using Amanah.Posthub.BLL.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IDriverWorkingHourTrackingManager
    {
        Task AddDutyAsync(int driverId, bool isOn, bool byAdmin);

        Task AddSessionExpiredAsync(int driverId);

        Task AddLogoutAsync(int driverId, bool byAdmin);

        Task AddLoginAsync(int driverId, string tenantId);

        Task<PagedResult<DriverWorkingHourTrackingViewModel>> GetAllByPaginationAsync(
            PaginatedDriverWorkingHourTrackingViewModel filter);

        Task<(int TotalCount, IReadOnlyCollection<DriverWorkingHourTrackingViewModel>)> GetAllAsync(
            PaginatedDriverWorkingHourTrackingViewModel filter);
    }
}
