﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.DriverWorkingHour.Queries.DTOs
{
    public class DriverWorkingHourTrackingFilterDTO : PaginatedItemsViewModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public List<int> DriverIds { get; set; }
        public List<int> TeamIds { get; set; }
    }
}
