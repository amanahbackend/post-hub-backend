﻿using System;

namespace Amanah.Posthub.BLL.DriverWorkingHour.Queries.DTOs
{
    public class DriverWorkingHourTrackingResponseDTO
    {
        public DriverWorkingHourTrackingResponseDTO(
         DateTime dayOfMonth,
         TimeSpan totalHours,
         int driverId,
         string driverName)
        {
            DayOfMonth = dayOfMonth;
            TotalHours = totalHours.ToString(@"hh\:mm");
            InternalTotalHours = totalHours;
            DriverName = driverName;
            DriverId = driverId;
        }

        internal TimeSpan InternalTotalHours { get; set; }
        public DateTime DayOfMonth { get; set; }
        public string TotalHours { get; set; }
        public int DriverId { get; set; }
        public string DriverName { get; set; }

    }
}
