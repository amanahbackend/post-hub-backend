﻿using Amanah.Posthub.BLL.DriverWorkingHour.Managers;
using Amanah.Posthub.BLL.DriverWorkingHour.Queries.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.DriverWorkingHour.Queries
{
    public interface IDriverWorkingHourTrackingQuery
    {
        Task<PagedResult<DriverWorkingHourTrackingResponseDTO>> GetAllByPaginationAsync(DriverWorkingHourTrackingFilterDTO filter);
        Task<(int TotalCount, IReadOnlyCollection<DriverWorkingHourTrackingResponseDTO>)> GetAllAsync(
           DriverWorkingHourTrackingFilterDTO filter);
    }
    internal class DriverWorkingHourTrackingQuery : IDriverWorkingHourTrackingQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public DriverWorkingHourTrackingQuery(
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<PagedResult<DriverWorkingHourTrackingResponseDTO>> GetAllByPaginationAsync(DriverWorkingHourTrackingFilterDTO filter)
        {
            var (totalCount, entries) = await GetFilteredTrackingEntriesAsync(filter, applyPaging: true);
            var resultItems = DriverWorkingHoursCalculator.Calculate(entries);

            return new PagedResult<DriverWorkingHourTrackingResponseDTO>
            {
                TotalCount = totalCount,
                Result = resultItems
            };
        }

        public async Task<(int TotalCount, IReadOnlyCollection<DriverWorkingHourTrackingResponseDTO>)> GetAllAsync(
           DriverWorkingHourTrackingFilterDTO filter)
        {
            var (totalCount, entries) = await GetFilteredTrackingEntriesAsync(filter, false);
            var result = DriverWorkingHoursCalculator.Calculate(entries);
            return (totalCount, result);
        }

        private async Task<(int TotalCount, IReadOnlyCollection<DriverWorkingHourTracking>)> GetFilteredTrackingEntriesAsync(
            DriverWorkingHourTrackingFilterDTO filter,
            bool applyPaging)
        {
            var startDate = filter.StartDate ?? DateTime.MinValue;
            var endDate = filter.EndDate ?? DateTime.MaxValue;
            var entriesQuery = context.DriverWorkingHourTracking
               .IgnoreQueryFilters()
              .OrderBy(entry => entry.ActivityDateTime)
               .Where(entry => entry.ActivityDateTime.Date >= startDate.Date
                && entry.ActivityDateTime.Date <= endDate.Date);
            if (filter.DriverIds != null && filter.DriverIds.Any())
            {
                entriesQuery = entriesQuery.Where(driverTrackingEntry =>
                    filter.DriverIds.Contains(driverTrackingEntry.DriverId));
            }
            if (filter.TeamIds != null && filter.TeamIds.Any())
            {
                entriesQuery = entriesQuery.Where(driverTrackingEntry => driverTrackingEntry.Driver != null
                     && filter.TeamIds.Contains(driverTrackingEntry.Driver.TeamId));
            }
            var pagingQuery = entriesQuery.GroupBy(entry => new { entry.DriverId, entry.ActivityDateTime.Date })
                .Select(group => group.Key);
            var totalCount = await pagingQuery.CountAsync();
            if (applyPaging)
            {
                pagingQuery = pagingQuery.ApplyPaging(filter);
            }

            var entries = from entry in entriesQuery
                          join driverDay in pagingQuery
                          on new { entry.DriverId, entry.ActivityDateTime.Date } equals driverDay
                          select entry;

            var result = await entries.Include(entry => entry.Driver.User).ToArrayAsync();
            foreach (var item in result)
            {
                item.ActivityDateTime = new DateTime(item.ActivityDateTime.Ticks, DateTimeKind.Utc);
            }
            return (totalCount, result);
        }



    }
}
