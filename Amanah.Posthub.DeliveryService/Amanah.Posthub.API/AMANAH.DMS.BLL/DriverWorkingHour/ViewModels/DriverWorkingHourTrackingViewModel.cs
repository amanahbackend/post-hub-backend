﻿using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class DriverWorkingHourTrackingViewModel
    {
        internal TimeSpan InternalTotalHours { get; set; }

        public DateTime DayOfMonth { get; set; }

        public string TotalHours { get; set; }

        public int DriverId { get; set; }

        public string DriverName { get; set; }

        public DriverWorkingHourTrackingViewModel(DateTime dayOfMonth, TimeSpan totalHours, int driverId, string driverName)
        {
            DayOfMonth = dayOfMonth;
            TotalHours = totalHours.ToString(@"hh\:mm");
            InternalTotalHours = totalHours;
            DriverName = driverName;
            DriverId = driverId;
        }
    }
}
