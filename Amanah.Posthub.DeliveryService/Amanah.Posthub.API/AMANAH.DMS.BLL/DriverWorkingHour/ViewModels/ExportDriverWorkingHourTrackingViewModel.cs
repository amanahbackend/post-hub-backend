﻿using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class ExportDriverWorkingHourTrackingViewModel
    {
        public string DriverName { get; set; }
        public DateTime DayOfMonth { get; set; }
        public string TotalHours { get; set; }

    }
}
