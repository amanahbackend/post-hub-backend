﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class PaginatedDriverWorkingHourTrackingViewModel : PaginatedItemsViewModel
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public List<int> DriverIds { get; set; }

    }
}
