﻿using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.ExternalServices.Settings;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes;
using Amanah.Posthub.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.BackgroundServcies
{
    [MinutelyJob]
    public class DriverMonitorService : IPeriodicBackgroundJob
    {
        private readonly ApplicationDbContext _context;
        private readonly IBackGroundNotificationSercvice _notificationService;

        private string CurrentNotificationSenderName { get; set; }
        public DriverMonitorService(
            IOptions<EmailSettings> emailSettings,
            ApplicationDbContext context,
            IBackGroundNotificationSercvice notificationService)
        {
            _context = context;
            _notificationService = notificationService;
            CurrentNotificationSenderName = emailSettings.Value.SenderName;
        }

        public Task ExecuteAsync()
        {
            return LogoutDriversWithExpirdTokensAsync();
        }

        public async Task LogoutDriversWithExpirdTokensAsync()
        {
            var matchedAgentStatuses = new int?[] { (int)AgentStatusesEnum.Available, (int)AgentStatusesEnum.Busy };
            var drivers = await _context.Driver
                .IgnoreQueryFilters()
                .Include(d => d.AgentStatus)
                .Include(d => d.User)
                .Include(d => d.CurrentLocation)
                .Where(driver => matchedAgentStatuses.Contains(driver.AgentStatusId)
                 && driver.TokenTimeExpiration.Value <= DateTime.UtcNow)
                .ToListAsync();
            if (!drivers.Any())
            {
                return;
            }

            foreach (var driver in drivers)
            {
                driver.AgentStatusId = (int)AgentStatusesEnum.Offline;
                driver.TokenTimeExpiration = null;
                _context.Driver.Update(driver);
                _notificationService.SendToUser(
                   driver.UserId,
                   "SessionEnded",
                   CurrentNotificationSenderName,
                   "Your session expired you need to login again",
                   new { IsLoggedOut = true });

                var userdevices = await _context.UserDevice
                    .Where(x => x.UserId == driver.UserId)
                    .IgnoreQueryFilters()
                    .ToListAsync();
                foreach (var userDevice in userdevices)
                {
                    userDevice.SetIsDeleted(true);
                    _context.Update(userDevice);
                }
                _context.Add(new DriverWorkingHourTracking(driver.Id, AgentStatusesEnum.Offline, "SessionExpired"));

                _notificationService.SendToUserManagers(driver.UserId, "DriverLoggedOut", "", "", new DriverViewModel
                {
                    Id = driver.Id,
                    UserId = driver.UserId,
                    FirstName = driver.User.FirstName,
                    LastName = driver.User.LastName,
                    AgentStatusId = driver.AgentStatusId,
                    AgentStatusName = AgentStatusesEnum.Offline.ToString(),
                    PhoneNumber = driver.User.PhoneNumber,
                    ImageUrl = driver.ImageUrl.Replace("DriverImages", string.Empty)
                });
            }
            await _context.SaveChangesAsync();

        }
    }
}
