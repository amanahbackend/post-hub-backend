﻿using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class CalenderMobileInputDTO
    {
        public int Driver_id { set; get; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
