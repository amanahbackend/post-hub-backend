﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class DriverExportToExcelViewModel
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public string TeamName { get; set; }
        public string Tags { set; get; }
        public string AgentStatusName { get; set; }
    }
}
