﻿using Amanah.Posthub.BLL.Settings.Constant;
using System;

namespace Amanah.Posthub.ViewModels
{
    public class DriverNearestAvailableDTO
    {
        private readonly DefaultSettingValue _defaultSettingValue;
        public DriverNearestAvailableDTO()
        {
            _defaultSettingValue = new DefaultSettingValue();
            MaxOrdersWeightsCapacity = _defaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
            MinOrdersNoWait = _defaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
        }
        public int Id { set; get; }
        public string UserId { get; set; }


        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public int MaxOrdersWeightsCapacity { get; set; }
        public int MinOrdersNoWait { get; set; }


        public DateTime? ReachedTime { get; set; }

        public double? DistanceToTaskRealRoute { set; get; }
        public double? DistanceToTaskAsStraight { set; get; }

    }
}
