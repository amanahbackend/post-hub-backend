﻿using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.ViewModels;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IAgentTypeManager : IBaseManager<AgentTypeViewModel, AgentType>
    {
        Task<AgentTypeViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
    }
}
