﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Driver;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{

    public interface IDriverManager : IBaseManager<DriverViewModel, Driver>
    {
        Task<PagedResult<DriverViewModel>> GetAllDriverByPaginationAsync(DriverSearchViewModel pagingparametermodel);
        Task<PagedResult<DriverViewModel>> GetAllDriverByPaginationAsync(DriverSearchViewModel pagingparametermodel, List<int> managerTeamIds);
        Task<PagedResult<DriverViewModel>> GetPlatformDriverByPaginationAsync(DriverSearchViewModel pagingparametermodel);

        Task CheckInAsync(int branchid);
        Task<DriverViewModel> GetCurrentDriverAsync();
        Task<List<DriverForAssignViewModel>> GetAssignDriversAsync();
        Task<DriverTransportionTypeViewModel> GetDriverTransportionTypeAsync(string userId);
        Task<List<DriverExportToExcelViewModel>> ExportExcelAsync(DriverSearchViewModel pagingparametermodel);
        Task<DriverViewModel> GetAsync(int id);
        Task<TQueryModel> GetAsync<TQueryModel>(int id);
        Task<DriverViewModel> GetByUserIDAsync(string UserId);
        Task<bool> ChangeDriverStatusByUserIdAsync(string userId, int statusId);
        Task<List<string>> GetTagsAsync();
        Task ChangeDriversTypeAsync(ChangeDriversTypeViewModel changeDriverTypeViewModel);
        Task BlockDriversAsync(BlockDriversViewModel blockDriverViewModel);
        Task UnBlockDriversAsync(BlockDriversViewModel blockDriverViewModel);
        Task UpdateDriverGeoFenceAsync(DriverViewModel viewModel);
        Task<List<DriverViewModel>> GetAllAsync(List<int> managerTeamIds);
        Task<DriverCurrentLocation> GetDriverCurrentLocationAsync(int driverId);
        Task<List<GeoFence>> GetDriverPickUpGeoFencesAsync(int driverId);
        GeoFence GetDriverCurrentGeoFence(List<GeoFence> driverGeoFences, DriverCurrentLocation driverCurrentLocation);
        Task<List<Branch>> GetDriverCurrentGeoFenceBranchesAsync(GeoFence driverCurrentGeoFence);

        Task<List<GeoFence>> GetDriverDeliveryGeoFencesAsync(int driverId);
        Task<DriverWithTasksViewModel> GetReachedDriverWithTasksAsync(int driverId);
        Task<DriverWithTasksViewModel> GetDriverAsync(GetDriverInput input);
        Task<List<DriverWithTasksViewModel>> GetDriversAsync(GetDriversInput input);
        Task<List<Driver>> GetDriversWithExpireCluppingTimeAsync();
        Task UpdateDriverClubbingTimeAsync(UpdateDriverClubbingTimeViewModel viewModel);
        void Update(Driver driver);
        Task<List<int>> GetDriverIdsByGeofenceIdAsync(int geofenceId);
        Task<List<int>> GetManagerDriversAsync(IEnumerable<int> teamIds);
        Task<bool> IsDriverAsync(string UserId);

        Task SetDriverCurrentLocationAsync(DriverCurrentLocation driverCurrentLocation);

    }
}
