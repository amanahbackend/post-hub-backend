﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IDriverRateManager : IBaseManager<DriverRateViewModel, DriverRate>
    {
        Task<DriverRateViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> DeleteAsync(int id);
    }
}
