﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Mobile;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IDriverTaskNotificationManager : IBaseManager<DriverTaskNotificationViewModel, DriverTaskNotification>
    {
        Task MarkAsRead(int driverId);
        int GetUnReadCount(int driverId);
        Task<PagedResult<DriverTaskNotificationResultDTO>> GetByPaginationAsync(
            PaginatedTasksDriverMobileInputDTO pagingparametermodel);
        Task Clear(int driverId);
    }
}
