﻿using Amanah.Posthub.ViewModels;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilites.ProcessingResult;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IImportExcelManager
    {
        Task<ProcessResult<List<ImportDriverViewModel>>> AddFromExcelSheetAsync(string path, IFormFile Uploadfile);
    }
}
