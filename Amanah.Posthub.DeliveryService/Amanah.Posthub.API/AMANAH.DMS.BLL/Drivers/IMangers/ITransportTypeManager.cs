﻿using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.ViewModels;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface ITransportTypeManager : IBaseManager<TransportTypeViewModel, TransportType>
    {
    }
}