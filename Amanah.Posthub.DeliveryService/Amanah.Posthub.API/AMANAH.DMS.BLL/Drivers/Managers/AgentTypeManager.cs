﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class AgentTypeManager : BaseManager<AgentTypeViewModel, AgentType>, IAgentTypeManager
    {
        public AgentTypeManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<AgentType> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<AgentTypeViewModel> Get(int id)
        {
            return await (context as ApplicationDbContext).AgentType
                .IgnoreQueryFilters()
                .Where(x => x.Id == id)
                .ProjectTo<AgentTypeViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            return await (context as ApplicationDbContext).AgentType
                .IgnoreQueryFilters()
                .Where(x => x.Id == id)
                .ProjectTo<TQueryModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
    }
}
