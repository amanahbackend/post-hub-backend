﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.DriverRegistrations.Queries;
using Amanah.Posthub.BLL.DriverRegistrations.Services;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Managers.TaskAssignment;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Driver;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers
{
    public class DriverManager : BaseManager<DriverViewModel, Driver>, IDriverManager
    {
        private readonly ITaskAutoAssignmentFactory _taskAutoAssignmentFactory;
        private readonly ICurrentUser _currentUser;
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly IRepositry<GeoFence> _geofenceRepository;
        private readonly ITasksManager _tasksManager;
        private readonly IDriverRegistrationService _driverRegistrationRequestManager;
        private readonly ISettingsManager _settingsManager;
        private readonly DefaultSettingValue _defaultSettingValue;
        private readonly IDriverRegistrationRequestsQuery _driverRegistrationRequestQuery;

        public DriverManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Driver> repository,
            IApplicationRoleManager iApplicationRoleManager,
            ITaskAutoAssignmentFactory taskAutoAssignmentFactory,
            ICurrentUser currentUser,
            IRepositry<GeoFence> geofenceRepository,
            ISettingsManager settingsManager,
            ITasksManager tasksManager,
            IDriverRegistrationService driverRegistrationRequestManager,
            IDriverRegistrationRequestsQuery driverRegistrationRequestQuery)
            : base(context, repository, mapper)
        {
            _applicationRoleManager = iApplicationRoleManager;
            _taskAutoAssignmentFactory = taskAutoAssignmentFactory;
            _currentUser = currentUser;
            _geofenceRepository = geofenceRepository;
            _settingsManager = settingsManager;
            _tasksManager = tasksManager;
            _driverRegistrationRequestManager = driverRegistrationRequestManager;
            _defaultSettingValue = new DefaultSettingValue();
            _driverRegistrationRequestQuery = driverRegistrationRequestQuery;
        }

        public async Task<DriverViewModel> GetAsync(int id)
        {
            var driverVM = await context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .IgnoreQueryFilters()
                .Where(x => x.Id == id && !x.IsDeleted)
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            //need to validate driver tenant is the current tenant
            if (driverVM != null && _currentUser.TenantId == driverVM.Tenant_Id)
            {
                driverVM.DriverRegistrationId = await _driverRegistrationRequestQuery.GetRegistrationIdByDriverIdAsync(driverVM.Id, driverVM.Username);
            }
            else if (driverVM == null || !string.IsNullOrEmpty(driverVM.Tenant_Id))
            {
                //invalid access
                return null;
            }
            return driverVM;
        }

        public Task<TQueryModel> GetAsync<TQueryModel>(int id)
        {
            return context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .Include(x => x.User)
                .Include(x => x.Team)
                .IgnoreQueryFilters()
                .Where(x => x.Id == id)
                .ProjectTo<TQueryModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public Task<DriverViewModel> GetByUserIDAsync(string UserId)
        {
            return context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .Include(x => x.Team).ThenInclude(x => x.LocationAccuracy)
                .Where(x => x.UserId == UserId)
                .IgnoreQueryFilters()
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> ChangeDriverStatusByUserIdAsync(string userId, int statusId)
        {
            try
            {
                var driver = await context.Driver
                    .IgnoreQueryFilters()
                    .Where(driver => !driver.IsDeleted)
                    .FirstOrDefaultAsync(userDriver => userDriver.UserId == userId);
                driver.AgentStatusId = statusId;
                if (statusId != (int)AgentStatusesEnum.Available)
                {
                    driver.ReachedTime = null;
                    driver.ClubbingTimeExpiration = null;
                    driver.IsInClubbingTime = false;
                    driver.MaxOrdersWeightsCapacity = 1;
                    driver.MinOrdersNoWait = 1;
                }
                context.Driver.Update(driver);
                await context.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<List<string>> GetTagsAsync()
        {
            var tags = await repository.GetAll().IgnoreQueryFilters().Where(x => !string.IsNullOrEmpty(x.Tags))
                .Select(t => t.Tags)
                .ToListAsync();
            var splitTags = tags.SelectMany(tag => tag.Split(','))
                .Distinct()
                .ToList();
            return splitTags;
        }

        public async Task ChangeDriversTypeAsync(ChangeDriversTypeViewModel changeDriverTypeViewModel)
        {
            if (!changeDriverTypeViewModel.DriverIds.Any())
            {
                return;
            }
            var drivers = await context.Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .IgnoreQueryFilters()
                .Where(x => changeDriverTypeViewModel.DriverIds.Contains(x.Id))
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();

            foreach (var driver in drivers)
            {
                driver.AgentTypeId = changeDriverTypeViewModel.AgentTypeId;
                await UpdateAsync(driver);
            }
        }

        public async Task BlockDriversAsync(BlockDriversViewModel blockDriverViewModel)
        {
            if (!blockDriverViewModel.DriverIds.Any())
            {
                return;
            }
#warning cleanup - reuse duplicated code
            var drivers = await (context as ApplicationDbContext).Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .IgnoreQueryFilters()
                .Where(x => blockDriverViewModel.DriverIds.Contains(x.Id))
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
            foreach (var driver in drivers)
            {
                driver.AgentStatusId = (int)AgentStatusesEnum.Blocked;
                driver.Reason = blockDriverViewModel.Reason;
                await UpdateAsync(driver);
            }
        }

        public async Task UnBlockDriversAsync(BlockDriversViewModel blockDriverViewModel)
        {
            if (!blockDriverViewModel.DriverIds.Any())
            {
                return;
            }
#warning cleanup - reuse duplicate code includes, wheres, ...
#warning critical - cleanup - Why we map then update mapped!!!!!
            var drivers = await (context as ApplicationDbContext).Driver
                .Include(x => x.DriverPickUpGeoFences)
                .Include(x => x.DriverDeliveryGeoFences)
                .IgnoreQueryFilters()
                .Where(x => blockDriverViewModel.DriverIds.Contains(x.Id))
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
            foreach (var driver in drivers)
            {
                driver.AgentStatusId = (int)AgentStatusesEnum.Offline;
                driver.Reason = blockDriverViewModel.Reason;
                await UpdateAsync(driver);
            }
        }

        public Task<List<DriverViewModel>> GetAllAsync(List<int> managerTeamIds)
        {
            return repository.GetAll()
                .IgnoreQueryFilters()
                .Where(x => managerTeamIds.Contains(x.TeamId))
                .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<PagedResult<DriverViewModel>> GetAllDriverByPaginationAsync(DriverSearchViewModel driverSearchViewModel)
        {
            return await InternalGetAllDriverByPaginationAsync(driverSearchViewModel);
        }

        public async Task<PagedResult<DriverViewModel>> GetAllDriverByPaginationAsync(DriverSearchViewModel driverSearchViewModel, List<int> managerTeamIds)
        {
            return await InternalGetAllDriverByPaginationAsync(driverSearchViewModel, managerTeamIds);
        }

        public async Task<PagedResult<DriverViewModel>> GetPlatformDriverByPaginationAsync(DriverSearchViewModel driverSearchViewModel)
        {
            var notCompletedList = new List<int>
            {
                (int)TaskStatusEnum.Assigned,
                (int)TaskStatusEnum.Accepted,
                (int) TaskStatusEnum.Started,
                (int)TaskStatusEnum.Inprogress
            };

            var tasks = await context.Tasks
                .IgnoreQueryFilters()
                .Where(task => notCompletedList.Contains(task.TaskStatusId))
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId)
                .Select(task => new { task.DriverId, Status = task.TaskStatusId })
                .ToListAsync();

            var query = context.Driver
                .Include(driver => driver.User)
                .Include(driver => driver.CurrentLocation)
                .IgnoreQueryFilters()
                .Where(driver => tasks.Select(task => task.DriverId).Contains(driver.Id))
                .Where(ApplyFilter(driverSearchViewModel));

            var pagedResult = new PagedResult<DriverViewModel>
            {
                TotalCount = await query.CountAsync()
            };

            int currentPage = driverSearchViewModel.PageNumber;
            int pageSize = driverSearchViewModel.PageSize;
            var drivers = await query.Skip((currentPage - 1) * pageSize).Take(pageSize).ToListAsync();

            pagedResult.Result = mapper.Map<List<DriverViewModel>>(drivers);
            foreach (var driver in pagedResult.Result)
            {
                var statusId = driver.AgentStatusId != (int)AgentStatusesEnum.Available && driver.AgentStatusId != (int)AgentStatusesEnum.Busy
                    ? driver.AgentStatusId
                    : tasks.Where(task => task.Status == (int)TaskStatusEnum.Inprogress || task.Status == (int)TaskStatusEnum.Started).Any()
                        ? (int)AgentStatusesEnum.Busy
                        : (int)AgentStatusesEnum.Available;

                driver.ImageUrl = string.IsNullOrEmpty(driver.ImageUrl)
                    ? string.Empty
                    : driver.ImageUrl.Replace("DriverImages", "");
                driver.AgentStatusId = statusId;
                driver.AgentStatusName = ((AgentStatusesEnum)statusId).ToString();
            };

            return pagedResult;
        }

        public override async Task<bool> DeleteAsync(DriverViewModel viewModel)
        {
            var driver = await GetAsync(viewModel.Id);
            viewModel.AgentStatusId = driver.AgentStatusId;
            await RemoveDriverGeoFenceAsync(viewModel.Id);
            return await base.DeleteAsync(viewModel);
        }

        public override async Task<bool> DeleteAsync(List<DriverViewModel> driverViewModels)
        {
            foreach (var driverViewModel in driverViewModels)
            {
                await RemoveDriverGeoFenceAsync(driverViewModel.Id);
            }
            return await base.DeleteAsync(driverViewModels);
        }

        public async Task<DriverCurrentLocation> GetDriverCurrentLocationAsync(int driverId)
        {
            return await context.DriverCurrentLocation
                    .IgnoreQueryFilters()
                    .Where(x => x.Id == driverId)
                    .OrderByDescending(x => x.CreatedDate)
                    .FirstOrDefaultAsync();
        }

        private async Task RemoveDriverGeoFenceAsync(int id)
        {
            var pickupGeoFences = await context.DriverPickUpGeoFence
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == id)
                .ToListAsync();
            var deliveryGeoFences = await context.DriverDeliveryGeoFence
                .Where(x => x.DriverId == id)
                .ToListAsync();

            context.DriverPickUpGeoFence.RemoveRange(pickupGeoFences);
            context.DriverDeliveryGeoFence.RemoveRange(deliveryGeoFences);

            await context.SaveChangesAsync();
        }

        public async Task<List<GeoFence>> GetDriverPickUpGeoFencesAsync(int driverId)
        {
            var dbContext = context;

            var driver = await dbContext.Driver.FindAsync(driverId);
            if (driver.AllPickupGeoFences)
            {
                return await _geofenceRepository.GetAll().ToListAsync();
            }
            return await dbContext.DriverPickUpGeoFence
                .IgnoreQueryFilters()
                .Include(x => x.GeoFence)
                .ThenInclude(x => x.GeoFenceLocations)
                .Where(x => x.DriverId == driverId && x.GeoFence != null && !x.GeoFence.IsDeleted)
                .Select(x => x.GeoFence)
                .ToListAsync();
        }

        public GeoFence GetDriverCurrentGeoFence(List<GeoFence> driverGeoFences, DriverCurrentLocation driverCurrentLocation)
        {
            var driverCurrentPoint = new MapPoint
            {
                Latitude = driverCurrentLocation.Latitude.Value,
                Longitude = driverCurrentLocation.Longitude.Value
            };
            foreach (var geoFence in driverGeoFences)
            {
                var zone = context.GeoFenceLocation
                    .IgnoreQueryFilters()
                    .Where(x => x.GeoFenceId == geoFence.Id && x.GeoFence != null && !x.GeoFence.IsDeleted)
                    .Select(x => new MapPoint { Latitude = x.Latitude.Value, Longitude = x.Longitude.Value })
                    .Select(x => new MapPoint { Latitude = x.Latitude, Longitude = x.Longitude })
                    .ToArray();

                if (MapsUtilities.MapsHelper.IsPointInPolygon(zone, driverCurrentPoint))
                {
                    return geoFence;
                }
            }
            return null;
        }

        public async Task<List<Branch>> GetDriverCurrentGeoFenceBranchesAsync(GeoFence driverCurrentGeoFence)
        {
            return await context.Branch
                .IgnoreQueryFilters()
                .Where(x => x.GeoFenceId == driverCurrentGeoFence.Id & !x.IsDeleted)
                .ToListAsync();
        }

        public async Task CheckInAsync(int branchid)
        {
            //var driverId =_currentUser.DriverId;
            var driver = await GetCurrentDriverAsync();

            driver.AgentStatusId = (int)AgentStatusesEnum.Available;
            driver.ReachedTime = DateTime.UtcNow;
            driver.IsInClubbingTime = false;
            driver.ClubbingTimeExpiration = null;
            var drivercurrentlocation = await context.DriverCurrentLocation
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(x => x.Id == driver.Id);
            drivercurrentlocation.BranchId = branchid;
            context.Update(drivercurrentlocation);
            await context.SaveChangesAsync();
            await UpdateAsync(driver);

            var autoAssignmentTask = await _taskAutoAssignmentFactory.CreateTaskAutoAssignmentAsync();
            await autoAssignmentTask.AssignTasksToDriverAsync(driver.Id);
        }

        public async Task<DriverViewModel> GetCurrentDriverAsync()
        {
            var driver = await context.Driver
                .IgnoreQueryFilters()
                .Where(driver => !driver.IsDeleted)
                .FirstOrDefaultAsync(driver => driver.Id == _currentUser.DriverId);
            var driverVM = mapper.Map<Driver, DriverViewModel>(driver);
            return driverVM;
        }

        public Task<List<DriverForAssignViewModel>> GetAssignDriversAsync()
        {
            return repository.GetAll()
                .IgnoreQueryFilters()
                .Where(x => x.AgentStatusId != (int)AgentStatusesEnum.Blocked)
                .ProjectTo<DriverForAssignViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<DriverTransportionTypeViewModel> GetDriverTransportionTypeAsync(string userId)
        {
            return await context.Driver
                .Include(x => x.TransportType)
                .IgnoreQueryFilters()
                .Where(x => x.UserId == userId)
                .ProjectTo<DriverTransportionTypeViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public override async Task<PagedResult<DriverViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var drivers = await base.GetAllByPaginationAsync(pagingparametermodel);
            foreach (var item in drivers.Result)
            {
                item.RoleNames = _applicationRoleManager.GetAllRolesByUserId(item.UserId);
            }
            return drivers;
        }

        public async Task<List<DriverExportToExcelViewModel>> ExportExcelAsync(DriverSearchViewModel driverSearchViewModel)
        {
            var query = repository.GetAll();
            query = query.IgnoreQueryFilters().Where(ApplyFilter(driverSearchViewModel))
                .Include(t => t.Team)
                .Include(t => t.AgentStatus)
                .Include(t => t.User)
                .ThenInclude(t => t.UserDvices);

            var result = await query.ProjectTo<DriverExportToExcelViewModel>(mapper.ConfigurationProvider).ToListAsync();
            return result;
        }

        public async Task UpdateDriverGeoFenceAsync(DriverViewModel viewModel)
        {
            await RemoveDriverGeoFenceAsync(viewModel.Id);
            viewModel.DriverDeliveryGeoFences.ForEach(x => x.DriverId = viewModel.Id);
            var entityDriverDeliveryGeoFence = mapper.Map<List<DriverDeliveryGeoFenceViewModel>, List<DriverDeliveryGeoFence>>(viewModel.DriverDeliveryGeoFences);
            await context.DriverDeliveryGeoFence.AddRangeAsync(entityDriverDeliveryGeoFence);

            viewModel.DriverPickUpGeoFences.ForEach(x => x.DriverId = viewModel.Id);
            var entityDriverPickUpGeoFence = mapper.Map<List<DriverPickUpGeoFenceViewModel>, List<DriverPickUpGeoFence>>(viewModel.DriverPickUpGeoFences);
            await context.DriverPickUpGeoFence.AddRangeAsync(entityDriverPickUpGeoFence);

            await context.SaveChangesAsync();
        }

        private async Task<PagedResult<DriverViewModel>> InternalGetAllDriverByPaginationAsync(
            DriverSearchViewModel driverSearchViewModel, IEnumerable<int> managerTeamIds = null)
        {
            var query = repository.GetAll();
            query = query.IgnoreQueryFilters().Where(ApplyFilter(driverSearchViewModel));
            if (managerTeamIds != null)
            {
                query = query.Where(driver => managerTeamIds.Contains(driver.TeamId));
            }
            var pagedResult = await query.ToPagedResultAsync<Driver, DriverViewModel>(driverSearchViewModel, mapper.ConfigurationProvider);

            foreach (var driver in pagedResult.Result)
            {
                driver.ImageUrl = string.IsNullOrEmpty(driver.ImageUrl) ? string.Empty : driver.ImageUrl.Replace("DriverImages", "");
                driver.RoleNames = _applicationRoleManager.GetAllRolesByUserId(driver.UserId);
                driver.DriverRegistrationId = await _driverRegistrationRequestQuery.GetRegistrationIdByDriverIdAsync(driver.Id, driver.Username);
            };
            return pagedResult;
        }

        public async Task UpdateDriverClubbingTimeAsync(UpdateDriverClubbingTimeViewModel viewModel)
        {
            var driver = await context.Driver
                .IgnoreQueryFilters()
                .Where(x => x.Id == viewModel.DriverId)
                .FirstOrDefaultAsync();
            driver.IsInClubbingTime = viewModel.IsInClubbingTime;
            driver.ClubbingTimeExpiration = viewModel.ClubbingTimeExpiration;
            context.Driver.Update(driver);
            await context.SaveChangesAsync();
        }
        public async Task<List<GeoFence>> GetDriverDeliveryGeoFencesAsync(int driverId)
        {
            var dbContext = context;

            var driver = await dbContext.Driver.FindAsync(driverId);
            if (driver.AllDeliveryGeoFences)
            {
                return await _geofenceRepository.GetAll().ToListAsync();
            }
            return await dbContext.DriverDeliveryGeoFence
                .Include(x => x.GeoFence)
                .ThenInclude(x => x.GeoFenceLocations)
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == driverId && x.GeoFence != null && !x.GeoFence.IsDeleted)
                .Select(x => x.GeoFence)
                .ToListAsync();
        }
        private IQueryable<Driver> GetReachedDriversWithTasksQueryable()
        {
#warning cleanup - public methods should not expose iquerable
            return (context as ApplicationDbContext).Driver
                .Include(d => d.AssignedTasks)
                    .ThenInclude(t => t.MainTask)
                    .IgnoreQueryFilters()
                .Where(d =>
                    d.ReachedTime != null &&
                    d.AssignedTasks.All(t =>
                        t.TaskStatusId == (int)Enums.TaskStatusEnum.Unassigned ||
                        t.TaskStatusId == (int)Enums.TaskStatusEnum.Assigned));
        }
        public async Task<DriverWithTasksViewModel> GetReachedDriverWithTasksAsync(int driverId)
        {
            var driver = await GetReachedDriversWithTasksQueryable()
                .IgnoreQueryFilters()
                .Where(d => d.Id == driverId)
                .FirstOrDefaultAsync();
            if (driver == null)
            {
                return null;
            }
            var driverVM = mapper.Map<Driver, DriverWithTasksViewModel>(driver);
            driverVM.AssignedMainTasks = mapper.Map<List<MainTask>, List<MainTaskViewModel>>(
                driver.AssignedTasks.Select(t => t.MainTask).Distinct().ToList());
            return driverVM;
        }
        private IQueryable<Driver> GetDriversQueryable(GetDriverBaseInput input)
        {
            var query = context.Driver
                .IgnoreQueryFilters()
                .Include(d => d.CurrentLocation)
                .Include(d => d.DriverDeliveryGeoFences)
                .Include(d => d.DriverPickUpGeoFences)
                .AsQueryable();
            if (input.IsReached.HasValue)
            {
                if (input.IsReached.Value)
                {
                    query = query.Where(d => d.ReachedTime.HasValue);
                }
                else
                {
                    query = query.Where(d => !d.ReachedTime.HasValue);
                }
            }

            if (input.Status.HasValue)
            {
                query = query.Where(d => d.AgentStatusId == (int)input.Status);
            }
            if (input.Statuses != null && input.Statuses.Any())
            {
                query = query.Where(d => input.Statuses.Contains((AgentStatusesEnum)d.AgentStatusId));
            }


            return query;
        }
        public async Task<DriverWithTasksViewModel> GetDriverAsync(GetDriverInput input)
        {
            var q = GetDriversQueryable(input);

            q = q.IgnoreQueryFilters().Where(d => d.Id == input.DriverId);

            var driver = q.FirstOrDefault();

            var driverVM = mapper.Map<Driver, DriverWithTasksViewModel>(driver);
            if (driverVM == null) return null;
            int driverOrderCapacity = _defaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
            if (input.IsApplyingAutoAllocation)
            {
                if (input.AutoAllocationType == AutoAllocationTypeEnum.Fifo)
                {
                    var driverOrderCapacitySetting = await _settingsManager.GetSettingByKeyAsync(
                        SettingsKeys.FirstInFirstOutMethodDriverOrderCapacity);
                    if (driverOrderCapacitySetting != null)
                    {
                        int.TryParse(driverOrderCapacitySetting.Value, out driverOrderCapacity);
                    }
                }
            }

            if (input.IncludeAssignedTasks)
            {
                var getMainTasksInput = new GetMainTasksInput
                {
                    Status = input.TaskStatus,
                    Statuses = input.TaskStatuses,
                    DriverId = driverVM?.Id
                };
                var tasksVM = await _tasksManager.GetMainTasks(getMainTasksInput);
                driverVM.AssignedMainTasks = tasksVM.Where(t => t.Tasks.First().DriverId == driverVM.Id).ToList();
            }
            else if (input.IncludeOrdersWeights)
            {
                var getMainTasksInput = new GetMainTasksInput
                {
                    Status = input.TaskStatus,
                    Statuses = input.TaskStatuses,
                    DriverId = driverVM.Id
                };
                driverVM.MaxOrdersWeightsCapacity = driverOrderCapacity;
                driverVM.MinOrdersNoWait = driverOrderCapacity;
                driverVM.OrdersWeights = await _tasksManager.GetTasksWeights(getMainTasksInput);
                driverVM.AssignedTasksCount = driverVM.OrdersWeights;
            }

            return driverVM;
        }
        public async Task<List<DriverWithTasksViewModel>> GetDriversAsync(GetDriversInput input)
        {
            var query = GetDriversQueryable(input);

            if (input.PickupBranchId.HasValue)
            {
                query = query.IgnoreQueryFilters().Where(d => d.CurrentLocation.BranchId == input.PickupBranchId);
            }
            else if (input.SamePickupBranchAsDriverId.HasValue)
            {
                var pickupBranchId = await context.Driver
                    .IgnoreQueryFilters()
                    .Where(d => d.Id == input.SamePickupBranchAsDriverId)
                    .Select(d => d.CurrentLocation.BranchId)
                    .FirstOrDefaultAsync();
                if (pickupBranchId.HasValue)
                {
                    query = query.Where(d => d.CurrentLocation.BranchId == pickupBranchId);
                }
            }
            if (input.DeliveryGeoFenceId.HasValue)
            {
                query = query.Where(d => d.AllDeliveryGeoFences || d.DriverDeliveryGeoFences.Any(g => g.Id == input.DeliveryGeoFenceId));
            }
            if (input.DeliveryGeoFencesIds != null && input.DeliveryGeoFencesIds.Any())
            {
                query = query.Where(d => d.AllDeliveryGeoFences || d.DriverDeliveryGeoFences.Any(g => input.DeliveryGeoFencesIds.Contains(g.Id)));
            }

            var drivers = await query.ToListAsync();
            var driversVM = mapper.Map<List<Driver>, List<DriverWithTasksViewModel>>(drivers);

            int driverOrderCapacity = _defaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
            if (input.IsApplyingAutoAllocation)
            {
                if (input.AutoAllocationType == AutoAllocationTypeEnum.Fifo)
                {
                    var driverOrderCapacitySetting = await _settingsManager.GetSettingByKeyAsync(
                        SettingsKeys.FirstInFirstOutMethodDriverOrderCapacity);
                    if (driverOrderCapacitySetting != null) int.TryParse(driverOrderCapacitySetting.Value, out driverOrderCapacity);
                }
            }


            if (driversVM != null && driversVM.Count > 0)
                if (input.IncludeAssignedTasks)
                {
                    var getMainTasksInput = new GetMainTasksInput
                    {
                        PickupBranchId = input.PickupBranchId,
                        Status = input.TaskStatus,
                        Statuses = input.TaskStatuses,
                        DriverIds = drivers.Select(d => d.Id).ToList()
                    };
                    var tasksVM = await _tasksManager.GetMainTasks(getMainTasksInput);
                    driversVM?.ForEach(d =>
                    d.AssignedMainTasks = tasksVM.Where(t => t.Tasks.First().DriverId == d.Id).ToList());
                }
                else if (input.IncludeOrdersWeights)
                {
                    var getMainTasksInput = new GetMainTasksInput
                    {
                        Status = input.TaskStatus,
                        Statuses = input.TaskStatuses,
                    };
                    foreach (var driverVM in driversVM)
                    {
                        getMainTasksInput.DriverId = driverVM.Id;
                        driverVM.MaxOrdersWeightsCapacity = driverOrderCapacity;
                        driverVM.MinOrdersNoWait = driverOrderCapacity;
                        driverVM.OrdersWeights = await _tasksManager.GetTasksWeights(getMainTasksInput);
                        driverVM.AssignedTasksCount = driverVM.OrdersWeights;
                    }
                }

            return driversVM;
        }

        public async Task<List<Driver>> GetDriversWithExpireCluppingTimeAsync()
        {
            return await context.Driver
                .IgnoreQueryFilters()
                .Where(e => e.IsInClubbingTime && e.ClubbingTimeExpiration.HasValue && e.ClubbingTimeExpiration.Value <= DateTime.UtcNow)
                .ToListAsync();
        }

        public void Update(Driver driver)
        {
            repository.Update(driver);
        }

        public async Task<List<int>> GetDriverIdsByGeofenceIdAsync(int geofenceId)
        {
            var driverPickUpGeoFences = await context.DriverPickUpGeoFence
                .IgnoreQueryFilters()
                .Where(e => e.GeoFenceId == geofenceId)
                .Select(e => e.DriverId)
                .ToArrayAsync();
            var driverDeliveryUpGeoFences = await context.DriverDeliveryGeoFence
                .Where(e => e.GeoFenceId == geofenceId)
                .Select(e => e.DriverId)
                .ToArrayAsync();
            var result = driverPickUpGeoFences.Concat(driverDeliveryUpGeoFences)
                .Distinct()
                .ToList();
            return result;
        }

        public async Task<List<int>> GetManagerDriversAsync(IEnumerable<int> teamIds)
        {
            if (teamIds == null)
            {
                return null;
            }
            return await context.Driver
                .IgnoreQueryFilters()
                .Where(x => teamIds.Contains(x.TeamId))
                .Select(x => x.Id)
                .ToListAsync();
        }

        public async Task<bool> IsDriverAsync(string UserId)
        {
            return await context.Driver.IgnoreQueryFilters().AnyAsync(x => x.UserId == UserId);
        }

        public async Task SetDriverCurrentLocationAsync(DriverCurrentLocation driverCurrentLocation)
        {
            var existDriverCurrentLocation = await context.DriverCurrentLocation
                .IgnoreQueryFilters()
                .Where(currentLocation => currentLocation.Id == driverCurrentLocation.Id)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            if (existDriverCurrentLocation == null)
            {
                context.DriverCurrentLocation.Add(driverCurrentLocation);
            }
            else
            {
                existDriverCurrentLocation.Latitude = driverCurrentLocation.Latitude;
                existDriverCurrentLocation.Longitude = driverCurrentLocation.Longitude;
                context.DriverCurrentLocation.Update(existDriverCurrentLocation);
            }
            await context.SaveChangesAsync();
        }

        private Expression<Func<Driver, bool>> ApplyFilter(DriverSearchViewModel driverSearchViewModel)
        {
            return driver =>
               (string.IsNullOrWhiteSpace(driverSearchViewModel.SearchBy) ||
                driver.User.UserName.Contains(driverSearchViewModel.SearchBy) ||
                driver.User.UserName.Contains(driverSearchViewModel.SearchBy) ||
                driver.User.FirstName.Contains(driverSearchViewModel.SearchBy) ||
                driver.User.MiddleName.Contains(driverSearchViewModel.SearchBy) ||
                driver.User.LastName.Contains(driverSearchViewModel.SearchBy) ||
                driver.User.Email.Contains(driverSearchViewModel.SearchBy) ||
                driver.User.PhoneNumber.Contains(driverSearchViewModel.SearchBy) ||
                driver.DeviceType.Contains(driverSearchViewModel.SearchBy) ||
                driver.Version.Contains(driverSearchViewModel.SearchBy) ||
                driver.Team.Name.Contains(driverSearchViewModel.SearchBy) ||
                driver.Team.Tags.Contains(driverSearchViewModel.SearchBy) ||
                driver.AgentStatus.Name.Contains(driverSearchViewModel.SearchBy))
               &&
               (driverSearchViewModel.statusType == default ||
                driver.AgentStatusId == (int)driverSearchViewModel.statusType)
               &&
                (driverSearchViewModel.driverType == default ||
                 driver.AgentTypeId == driverSearchViewModel.driverType);
        }

    }
}
