﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class DriverRateManager : BaseManager<DriverRateViewModel, DriverRate>, IDriverRateManager
    {

        public DriverRateManager(
            ApplicationDbContext context,
            IRepositry<DriverRate> repository,
            IMapper mapper)
            : base(context, repository, mapper)
        {
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var branch = context.DriverRate.IgnoreQueryFilters().SingleOrDefaultAsync(t => t.Id == id);

            if (branch == null)
            {
                return false;
            }
            var data = await base.SoftDeleteAsync(mapper.Map<DriverRateViewModel>(branch));
            return data;
        }

        public async Task<DriverRateViewModel> Get(int id)
        {
            return await context.DriverRate
                .IgnoreQueryFilters()
                .ProjectTo<DriverRateViewModel>(mapper.ConfigurationProvider)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            return await context.DriverRate
                .IgnoreQueryFilters()
                .Where(x => x.Id == id)
                .ProjectTo<TQueryModel>(mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
        }

        public async override Task<DriverRateViewModel> AddAsync(DriverRateViewModel viewModel)
        {
            var result = await base.AddAsync(viewModel);
            await UpdateDriverAvgRateAsync(viewModel.DriverId);
            return result;
        }

        private async Task UpdateDriverAvgRateAsync(int driverId)
        {
            Driver driver = await context.Driver.Include(x => x.DriverRates).FirstOrDefaultAsync(x => x.Id == driverId);
            if (driver == null)
            {
                return;
            }
#warning performance-issue one day the driver rate is going to be ver large number of records
            //we should not load all rates in memroy to calculate the average
            driver.DriverAvgRate = driver.DriverRates.Sum(x => x.Rate) / driver.DriverRates.Count;
            await context.SaveChangesAsync();
        }

    }
}
