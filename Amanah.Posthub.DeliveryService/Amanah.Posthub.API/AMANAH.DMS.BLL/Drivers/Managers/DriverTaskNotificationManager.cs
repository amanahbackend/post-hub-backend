﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Mobile;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers
{
    public class DriverTaskNotificationManager :
        BaseManager<DriverTaskNotificationViewModel, DriverTaskNotification>,
        IDriverTaskNotificationManager
    {

        public DriverTaskNotificationManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<DriverTaskNotification> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task MarkAsRead(int driverId)
        {
            var dbDriverTaskNotifications = await context.DriverTaskNotification
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == driverId)
                .IgnoreQueryFilters()
                .ToListAsync();

            foreach (var notification in dbDriverTaskNotifications)
            {
                notification.IsRead = true;
            }

            await context.SaveChangesAsync();
        }

        public int GetUnReadCount(int driverId)
        {
            return context.DriverTaskNotification
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == driverId && x.IsRead != true && !x.IsDeleted)
                .IgnoreQueryFilters()
                .Count();
        }

        public async Task<PagedResult<DriverTaskNotificationResultDTO>> GetByPaginationAsync(
            PaginatedTasksDriverMobileInputDTO pagingparametermodel)
        {
            var notificationQuery = context.DriverTaskNotification
                //.Include(x => x.Task.TaskType)
                //.Include(x => x.Task.TaskStatus)
                //.Include(x => x.Task.Customer)
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == pagingparametermodel.DriverId && !x.IsDeleted)
                .IgnoreQueryFilters()
                .OrderByDescending(x => x.CreatedDate);
            //.AsNoTracking();

            int currentPage = pagingparametermodel.PageNumber;
            int pageSize = pagingparametermodel.PageSize;

            var result = await notificationQuery.Skip((currentPage - 1) * pageSize).Take(pageSize).ProjectTo<DriverTaskNotificationResultDTO>(mapper.ConfigurationProvider).ToListAsync();

            var pagedResult = new PagedResult<DriverTaskNotificationResultDTO>
            {
                TotalCount = await notificationQuery.CountAsync(),
                Result = result
            };

            return pagedResult;
        }

        public async Task Clear(int driverId)
        {
            // TODO: (performance) this need reconsideration why should I load all notifications of user in memory just to mark as deleted, what if user has hundreds of notifications!
            // we may add EntityFramework.Extended for bulk operations + reuse code to put our aspects like soft-delete, auditing, ...
            // maybe better place to handle this is the DbContext itself
            var dbDriverTaskNotifications = await context.DriverTaskNotification
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == driverId)
                .IgnoreQueryFilters()
                .ToListAsync();
            context.RemoveRange(dbDriverTaskNotifications);
            await context.SaveChangesAsync();
        }
    }
}
