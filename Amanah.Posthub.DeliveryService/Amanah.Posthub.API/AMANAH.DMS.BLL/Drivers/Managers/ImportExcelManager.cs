﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Account;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ExcelToGenericList;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Managers
{
    public class ImportExcelManager : IImportExcelManager
    {
        private readonly IMapper _mapper;
        private readonly IApplicationUserManager _appUserManager;

        private readonly IDriverManager _driverManager;
        private readonly ILocalizer _localizer;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly ApplicationDbContext _dbContext;

        public ImportExcelManager(
            IMapper mapper,
            IApplicationUserManager appUserManager,
            IDriverManager driverManager,
            ILocalizer localizer,
            IUploadFormFileService uploadFormFileService,
             ApplicationDbContext dbContext)
        {
            _mapper = mapper;
            _appUserManager = appUserManager;
            _driverManager = driverManager;
            _localizer = localizer;
            _uploadFormFileService = uploadFormFileService;
            _dbContext = dbContext;
        }

        public ImportDriverViewModel GetDriver(IList<string> rowData, IList<string> columnNames)
        {
            return new ImportDriverViewModel()
            {
                UserName = rowData[columnNames.IndexFor("UserName")].ToString(),
                Password = rowData[columnNames.IndexFor("Password")].ToString(),
                Email = rowData[columnNames.IndexFor("Email")].ToString(),
                PhoneNumber = rowData[columnNames.IndexFor("PhoneNumber")].ToString(),
                FirstName = rowData[columnNames.IndexFor("FirstName")].ToString(),
                LastName = rowData[columnNames.IndexFor("LastName")].ToString(),
                Tags = rowData[columnNames.IndexFor("Tags")].ToString(),
                TransportDescription = rowData[columnNames.IndexFor("TransportDescription")].ToString(),
                LicensePlate = rowData[columnNames.IndexFor("LicensePlate")].ToString(),
                Color = rowData[columnNames.IndexFor("Color")].ToString(),
                RoleName = rowData[columnNames.IndexFor("RoleName")].ToString(),
                AgentTypeId = _dbContext.AgentType.FirstOrDefault(t => t.Name == rowData[columnNames.IndexFor("AgentType")].ToString()).Id,
                TeamId = rowData[columnNames.IndexFor("TeamId")].ToInt32(),
                AgentStatusId = _dbContext.AgentStatus.FirstOrDefault(t => t.Name == rowData[columnNames.IndexFor("AgentStatus")].ToString()).Id,
                TransportTypeId = _dbContext.TransportType.FirstOrDefault(t => t.Name == rowData[columnNames.IndexFor("TransportType")].ToString()).Id,
                CountryId = _dbContext.Country.FirstOrDefault(t => t.Name == rowData[columnNames.IndexFor("Country")].ToString()).Id,


            };
        }

        public async Task<ProcessResult<List<ImportDriverViewModel>>> AddFromExcelSheetAsync(string path, IFormFile Uploadfile)
        {
            ProcessResult<List<ImportDriverViewModel>> result = new ProcessResult<List<ImportDriverViewModel>>();
            try
            {
                if (string.IsNullOrEmpty(path))
                {
                    result.IsSucceeded = false;
                    result.Exception = new Exception("There is no file uploaded");
                    return result;
                }

                ProcessResult<string> uploadedFile = await _uploadFormFileService.UploadFileAsync(Uploadfile, path);
                if (uploadedFile.IsSucceeded)
                {
                    string excelPath = ExcelReader.CheckPath(uploadedFile.ReturnData, path);
                    var dataList = ExcelReader.GetDataToList(excelPath, GetDriver);
                    result.ReturnData = await AddDriverAsync((List<ImportDriverViewModel>)dataList);
                    result.IsSucceeded = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.Exception = ex;
            }
            return result;
        }

        public async Task<List<ImportDriverViewModel>> AddDriverAsync(List<ImportDriverViewModel> createDriversVM)
        {
            var importDriverViewModels = new List<ImportDriverViewModel>();
            foreach (var driver in createDriversVM)
            {
                var result = await AddDriverAsync(driver);
                importDriverViewModels.Add(result);
            }
            return importDriverViewModels;
        }

        public async Task<ImportDriverViewModel> AddDriverAsync(ImportDriverViewModel createDriverVM)
        {
            createDriverVM = DriverValidator(createDriverVM);
            if (!string.IsNullOrEmpty(createDriverVM.Error))
            {
                return createDriverVM;
            }
            var existUser = await _appUserManager.GetByEmailAsync(createDriverVM.Email);
            if (existUser != null)
            {
                createDriverVM.Error = _localizer[Keys.Validation.EmailAlreadyExists];
                return createDriverVM;
            }
            var teamDoNotExist = !await _dbContext.Teams.AnyAsync(team => team.Id == createDriverVM.TeamId);
            if (teamDoNotExist)
            {
                createDriverVM.Error = _localizer[Keys.Validation.TeamNotFound];
                return createDriverVM;
            }
            var result = await AddDriverUserAsync(createDriverVM);
            if (result == null || result.GetType() == typeof(string) || !result.Succeeded)
            {
                createDriverVM.Error = result.Error;
                return createDriverVM;
            }

            var driverToCreate = _mapper.Map<DriverViewModel>(createDriverVM);
            driverToCreate.UserId = result.User.Id;
            if (!string.IsNullOrEmpty(createDriverVM.Error))
                return createDriverVM;

            var created_driver = await _driverManager.AddAsync(driverToCreate);
            await _driverManager.ChangeDriverStatusByUserIdAsync(created_driver.UserId, (int)AgentStatusesEnum.Offline);
            return createDriverVM;
        }

        private async Task<UserManagerResult> AddDriverUserAsync(ImportDriverViewModel createDriverVM)
        {
            var userManagerToCreate = _mapper.Map<ApplicationUserViewModel>(createDriverVM);
            var userToCreate = _mapper.Map<ApplicationUser>(userManagerToCreate);
            userToCreate.RoleNames.Add(createDriverVM.RoleName);
            var result = await _appUserManager.AddUserAsync(userToCreate, userManagerToCreate.Password);
            return result;
        }

        private ImportDriverViewModel DriverValidator(ImportDriverViewModel createDriverVM)
        {
            const int MinCharacters = 1;
            const int MaxCharacters = 100;
            if (string.IsNullOrEmpty(createDriverVM.FirstName) || createDriverVM.FirstName.Length > MaxCharacters)
            {
                createDriverVM.Error = _localizer.Format(
                    Keys.Validation.NameMustBeBetweenMinAndMaxCharacters,
                    MinCharacters,
                    MaxCharacters);
                return createDriverVM;
            }

            if (string.IsNullOrEmpty(createDriverVM.PhoneNumber))
            {
                createDriverVM.Error = _localizer[Keys.Validation.PhoneNumberIsRequired];
                return createDriverVM;
            }

            if (createDriverVM.TeamId == 0)
            {
                createDriverVM.Error = _localizer[Keys.Validation.TeamIdIsRequired];
                return createDriverVM;
            }

            if (string.IsNullOrEmpty(createDriverVM.UserName))
            {
                createDriverVM.Error = _localizer[Keys.Validation.UserNameIsRequired];
                return createDriverVM;
            }

            if (createDriverVM.TransportTypeId == 0)
            {
                createDriverVM.Error = _localizer[Keys.Validation.TransportIdIsRequired];
                return createDriverVM;
            }

            return createDriverVM;
        }

    }
}
