﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.ViewModels;
using AutoMapper;

namespace Amanah.Posthub.BLL.Managers
{
    public class TransportTypeManager : BaseManager<TransportTypeViewModel, TransportType>, ITransportTypeManager
    {
        public TransportTypeManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<TransportType> repository)
            : base(context, repository, mapper)
        {
        }
    }
}