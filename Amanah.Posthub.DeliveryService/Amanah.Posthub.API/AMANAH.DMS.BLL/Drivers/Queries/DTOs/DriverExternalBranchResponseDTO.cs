﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Drivers.Queries.DTOs
{
    public class DriverExternalBranchResponseDTO
    {
        public int Id { get; set; }
        public int DriverId { get; set; }
        public int? BranchId { get; set; }
    }
}
