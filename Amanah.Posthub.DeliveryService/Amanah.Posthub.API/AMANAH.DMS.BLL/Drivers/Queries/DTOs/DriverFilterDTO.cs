﻿using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class DriverFilterDTO : PaginatedItemsViewModel
    {
        public AgentStatusesEnum StatusType { set; get; }
        public int DriverType { set; get; }
    }
}
