﻿namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class DriverGeoFenceResultDTO
    {
        public int Id { set; get; }
        public int? DriverId { set; get; }
        public int GeoFenceId { set; get; }
        public string GeoFenceName { set; get; }
    }
}
