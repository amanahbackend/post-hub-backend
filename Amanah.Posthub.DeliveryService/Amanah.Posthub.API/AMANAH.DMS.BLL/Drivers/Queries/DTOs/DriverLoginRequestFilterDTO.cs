﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class DriverLoginRequestFilterDTO : PaginatedItemsViewModel
    {
        public List<int> StatusIDs { get; set; }
        public List<int> DriverIds { get; set; }
        public DateTime? FilterDate { get; set; }
        public DateTime? FilterTime { get; set; }
    }
}
