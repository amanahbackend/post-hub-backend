﻿using System;

namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class DriverLoginRequestResponseDTO
    {
        public int Id { get; set; }
        public byte Status { get; set; }
        public string Token { get; set; }
        public int DriverId { get; set; }
        public string ApprovedOrRejectBy { get; set; }
        public string DriverName { get; set; }
        public int? TeamId { set; get; }
        public string TeamName { get; set; }
        public int? AgentTypeId { set; get; }
        public string AgentTypeName { get; set; }
        public string Reason { get; set; }
        public bool IsExpired { get; set; }

        public string Tenant_Id { get; set; }
        public string CreatedBy_Id { get; set; }
        public string UpdatedBy_Id { get; set; }
        public string DeletedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
    }
}
