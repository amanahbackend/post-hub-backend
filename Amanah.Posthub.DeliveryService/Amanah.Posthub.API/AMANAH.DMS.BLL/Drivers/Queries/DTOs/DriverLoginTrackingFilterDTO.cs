﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;

namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class DriverLoginTrackingFilterDTO : PaginatedItemsViewModel
    {
        public string TimeZone { get; set; }
        public int? DriverId { get; set; }
        public DateTime? LogoutDate { get; set; }
    }
}
