﻿namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class DriverRegistrationLookupDTO
    {
        public int RegistrationId { get; internal set; }
        public int DriverId { get; internal set; }
    }
}
