﻿using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.DeliverService.BLL.DriverFloor.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.Drivers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class DriverResultDTO
    {
        private readonly DefaultSettingValue _defaultSettingValue;
        public DriverResultDTO()
        {
            _defaultSettingValue = new DefaultSettingValue();
            MaxOrdersWeightsCapacity = _defaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
            MinOrdersNoWait = _defaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
        }

        public int Id { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string ImageUrl { get; set; }
        //public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public List<string> RoleNames { get; set; }
        public int TeamId { set; get; }
        public string TeamName { get; set; }
        public int? AgentTypeId { set; get; }
        public string AgentTypeName { get; set; }
        public DateTime AgentStatusUpdateDate { get; set; }
        public int? TransportTypeId { set; get; }
        public string TransportTypeName { get; set; }
        public int? CountryId { set; get; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {(string.IsNullOrEmpty(LastName) ? "" : LastName)}";
        public int? AgentStatusId { set; get; }
        public string Reason { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public int MaxOrdersWeightsCapacity { get; set; }
        public int MinOrdersNoWait { get; set; }
        public int AssignedTasksCount { get; set; }
        public DateTime? TokenExpirationDate { get; set; }

        public UserRegistrationStatus UserRegistrationStatus { get; set; }

        public double? DriverAvgRate { get; set; }
        public string AgentStatusName
        {
            get; set;
        }
        public DateTime? ReachedTime { get; set; }
        public bool IsReached => ReachedTime.HasValue;
        public double? RouteDistanceToTask { set; get; }
        public string LocationAccuracyName { set; get; }
        public int? LocationAccuracyDuration { set; get; }
        public int? BranchId { get; set; }
        public int? ExistInBranchId { get; set; }

        public bool IsInClubbingTime { get; set; }
        public DateTime? ClubbingTimeExpiration { get; set; }
        public DateTime? TokenTimeExpiration { get; set; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }
        public string Tenant_Id { set; get; }
        public int? DriverRegistrationId { set; get; }
        public List<DriverGeoFenceResultDTO> DriverPickUpGeoFences { get; set; }
        public List<DriverGeoFenceResultDTO> DriverDeliveryGeoFences { get; set; }
        public List<DriverFloorResponseDTO> DriverFloors { get; set; }
        public List<DriverExternalBranchResponseDTO> DriverExternalBranchs { get; set; }


        //////////
        public AddressResultDto Address { set; get; }

        public string MiddleName { get; set; }
        public CountryResposeDTO Country { get; set; }
        public int DepartmentId { set; get; }
        public string DepartmentName { set; get; }
        public int ServiceSectorId { get; set; }
        public string ServiceSectorName { set; get; }
        public DateTime? DateOfBirth { get; set; }
        public string NationalId { get; set; }
        public int NationalityId { get; set; }
        public string Nationality { get; set; }
        public string SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public string BranchName { get; set; }
        public int GenderId { get; set; }
        public string GenderName { get; set; }
        public int AccountStatusId { get; set; }
        public string AccountStatusName { get; set; }

        public string MobileNumber { get; set; }
        public string LicenseNumber { get; set; }
        public string CompanyEmail { get; set; }
        public string Code { get; set; }
        public int? DriverRoleId { get; set; }

    }
}
