﻿namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class DriverRoleLookupDTO
    {
        public string RoleName { get; internal set; }
        public string UserId { get; internal set; }
    }
}
