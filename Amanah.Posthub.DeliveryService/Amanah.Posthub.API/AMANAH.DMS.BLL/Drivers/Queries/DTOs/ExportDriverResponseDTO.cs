﻿namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class ExportDriverResponseDTO
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Nationality { get; set; }
        public string PhoneNumber { get; set; }
        public string TeamName { get; set; }
        public string DepartmentName { set; get; }
        public string ServiceSectorName { set; get; }
        public string SupervisorName { get; set; }
        public string BranchName { get; set; }
        public string CompanyEmail { get; set; }
    }
}
