﻿using System;

namespace Amanah.Posthub.BLL.Drivers.Queries.DTOs
{
    public class ExportDriverloginTrackingResponseDTO
    {
        public int Id { get; set; }
        public int DriverId { get; set; }
        public DateTime? LoginDate { get; set; }
        public double? LoginLatitude { get; set; }
        public double? LoginLongitude { get; set; }
        public DateTime? LogoutDate { get; set; }
        public double? LogoutLatitude { get; set; }
        public double? LogoutLongitude { get; set; }
        public string LogoutActionBy { get; set; }
        public string DriverName { get; set; }
    }
}
