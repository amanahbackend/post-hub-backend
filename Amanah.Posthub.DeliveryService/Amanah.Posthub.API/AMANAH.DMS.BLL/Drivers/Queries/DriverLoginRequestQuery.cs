﻿using Amanah.Posthub.BLL.Drivers.Queries.DTOs;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Drivers.Queries
{
    public interface IDriverLoginRequestQuery
    {
        Task<PagedResult<DriverLoginRequestResponseDTO>> GetAllByPaginationAsync(DriverLoginRequestFilterDTO filter);
    }
    internal class DriverLoginRequestQuery : IDriverLoginRequestQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public DriverLoginRequestQuery(
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<PagedResult<DriverLoginRequestResponseDTO>> GetAllByPaginationAsync(DriverLoginRequestFilterDTO filter)
        {
            var query = context.DriverLoginRequests.IgnoreQueryFilters().AsQueryable();
            if (!string.IsNullOrWhiteSpace(filter.SearchBy))
            {
                query = query.Where(loginRequest => loginRequest.Driver.User.FirstName.ToLower().Contains(filter.SearchBy.ToLower())
                || loginRequest.Driver.User.LastName.ToLower().Contains(filter.SearchBy.ToLower())
                || loginRequest.Driver.User.UserName.ToLower().Contains(filter.SearchBy.ToLower())
                || loginRequest.Driver.User.Email.ToLower().Contains(filter.SearchBy.ToLower()));
            }
            if (filter.StatusIDs != null && filter.StatusIDs.Any())
            {
                query = query.Where(loginRequest => filter.StatusIDs.Contains(loginRequest.Status));
            }
            if (filter.FilterTime.HasValue)
            {
                query = query.Where(loginRequest => loginRequest.CreatedDate >= TimeZoneInfo.ConvertTimeToUtc(filter.FilterTime.Value));
            }
            if (filter.DriverIds != null && filter.DriverIds.Any())
            {
                query = query.Where(loginRequest => filter.DriverIds.Contains(loginRequest.DriverId));
            }
            var expireHours = await GetLoginRequestExpireHoursAsync();
            var pagedResult = await query
                .OrderByDescending(driverLogin => driverLogin.CreatedDate)
                .ToPagedResultAsync<DriverLoginRequest, DriverLoginRequestResponseDTO>(filter, mapper.ConfigurationProvider);
            pagedResult.Result?.ToList().ForEach(driverLoginRequest =>
            {
                driverLoginRequest.IsExpired = driverLoginRequest.CreatedDate.AddHours(expireHours) <= DateTime.UtcNow;
            });

            return pagedResult;
        }

        private async Task<int> GetLoginRequestExpireHoursAsync()
        {
            int expireHour = 24;
            var expireHourSetting = await context.Settings
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync(s => s.SettingKey == SettingKeyConstant.DriverLoginRequestExpiration);
            if (expireHourSetting != null)
            {
                int.TryParse(expireHourSetting.Value, out expireHour);
            }
            return expireHour;
        }
    }
}
