﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Drivers.Queries.DTOs;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.Drivers.Queries.DTOs.Express;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Drivers.Queries
{
    public interface IDriverQuery
    {
        Task<PagedResult<DriverResultDTO>> GetAllDriverByPaginationAsync(DriverFilterDTO driverFilterDTO, List<int> managerTeamIds = null);
        Task<PagedResult<DriverResultForExpressDTO>> GetAllDriverByPaginationExpressAsync(DriverFilterDTO driverFilterDTO, List<int> managerTeamIds = null);
        Task<List<DriverResultDTO>> GetAllAsync(List<int> managerTeamIds = null);
        Task<List<DriverResultForExpressDTO>> GetAllExpressAsync(List<int> managerTeamIds = null);
        Task<List<DriverResultDTO>> GetAllByServiceSectorAsync(int? ServiceSectorId, List<int> managerTeamIds = null);
        Task<List<DriverResultForExpressDTO>> GetAllByServiceSectorExpressAsync(int? ServiceSectorId, List<int> managerTeamIds = null);
        Task<DriverResultDTO> GetAsync(int id);
        Task<DriverResultForExpressDTO> GetExpressAsync(int id);
        Task<Driver> GetDriverByUserIdAsync(string userId);
        Task<DriverResultDTO> GetByUserIdAsync(string userId);
        Task<DriverResultDTO> GetCurrentDriverAsync();
        Task<List<DriverValidForAssigningResultDTO>> GetAssignDriversAsync();
        Task<DriverTransportionTypeResultDTO> GetDriverTransportionTypeAsync(string userId);
        Task<List<string>> GetTagsAsync();
        Task<List<ExportDriverResponseDTO>> ExportExcelAsync(DriverFilterDTO filter);
        Task<PagedResult<DriverResultDTO>> GetPlatformDriverByPaginationAsync(DriverFilterDTO filter);
        Task<bool> AnyActiveDriverHasGeofencesAsync(List<int> GeoFenceIDs, int? DriverId = null);
        Task<int> GetDriverCountAsync();
        Task<bool> HasAnyActiveWorkordersAsync(int driverId);

    }

    public class DriverQuery : IDriverQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public DriverQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        public async Task<PagedResult<DriverResultDTO>> GetAllDriverByPaginationAsync(DriverFilterDTO driverFilterDTO, List<int> managerTeamIds = null)
        {
            return await InternalGetAllDriverByPaginationAsync(driverFilterDTO, managerTeamIds);
        }

        public async Task<PagedResult<DriverResultForExpressDTO>> GetAllDriverByPaginationExpressAsync(DriverFilterDTO driverFilterDTO, List<int> managerTeamIds = null)
        {
            return await InternalGetAllDriverByPaginationExpressAsync(driverFilterDTO, managerTeamIds);
        }

        public async Task<List<DriverResultDTO>> GetAllAsync(List<int> managerTeamIds = null)
        {
            var query = context.Driver
                .IgnoreQueryFilters()
                .Where(c => !c.IsDeleted)
                .AsQueryable();
            if (managerTeamIds != null && managerTeamIds.Any())
            {
                query = query.Where(driver => managerTeamIds.Contains(driver.TeamId));
            }
            return await query
                 .ProjectTo<DriverResultDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();
        }
        public async Task<List<DriverResultForExpressDTO>> GetAllExpressAsync(List<int> managerTeamIds = null)
        {
            var query = context.Driver
                .IgnoreQueryFilters()
                .Where(c => !c.IsDeleted)
                .AsQueryable();
            if (managerTeamIds != null && managerTeamIds.Any())
            {
                query = query.Where(driver => managerTeamIds.Contains(driver.TeamId));
            }
            return await query
                 .ProjectTo<DriverResultForExpressDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();
        }
        public async Task<List<DriverResultDTO>> GetAllByServiceSectorAsync(int? ServiceSectorId, List<int> managerTeamIds = null)
        {
            var query = context.Driver
                .IgnoreQueryFilters()
                .Where(d => !d.IsDeleted && (!ServiceSectorId.HasValue || d.ServiceSectorId == ServiceSectorId))
                .AsQueryable();
            if (managerTeamIds != null && managerTeamIds.Any())
            {
                query = query.Where(driver => managerTeamIds.Contains(driver.TeamId));
            }
            return await query
                 .ProjectTo<DriverResultDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();
        }

        public async Task<List<DriverResultForExpressDTO>> GetAllByServiceSectorExpressAsync(int? ServiceSectorId, List<int> managerTeamIds = null)
        {
            var query = context.Driver
                .IgnoreQueryFilters()
                .Where(d => !d.IsDeleted && (!ServiceSectorId.HasValue || d.ServiceSectorId == ServiceSectorId))
                .AsQueryable();
            if (managerTeamIds != null && managerTeamIds.Any())
            {
                query = query.Where(driver => managerTeamIds.Contains(driver.TeamId));
            }
            return await query
                 .ProjectTo<DriverResultForExpressDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();
        }

        public async Task<List<DriverValidForAssigningResultDTO>> GetAssignDriversAsync()
        {
            return await context.Driver
                .IgnoreQueryFilters()
                .Where(d => !d.IsDeleted)
                .Where(driver => driver.AgentStatusId != (int)AgentStatusesEnum.Blocked)
                .ProjectTo<DriverValidForAssigningResultDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        public async Task<DriverResultDTO> GetAsync(int id)
        {
            var driverDTO = await InternalGetWithIgnoreFilter()
                .Where(driver => driver.Id == id)
                .ProjectTo<DriverResultDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            if (driverDTO != null && currentUser.TenantId == driverDTO.Tenant_Id)
            {
                driverDTO.DriverRegistrationId = (await GetDriverRegistrationAsQueryable(driverDTO.Id).FirstOrDefaultAsync())
                    ?.RegistrationId;
            }
            else if (driverDTO == null || !string.IsNullOrEmpty(driverDTO.Tenant_Id))
            {
                return null;
            }
            return driverDTO;
        }

        public async Task<DriverResultForExpressDTO> GetExpressAsync(int id)
        {
            var driverDTO = await InternalGetWithIgnoreFilterForExpress()
                .Where(driver => driver.Id == id)
                .ProjectTo<DriverResultForExpressDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            if (driverDTO != null && currentUser.TenantId == driverDTO.Tenant_Id)
            {
                driverDTO.DriverRegistrationId = (await GetDriverRegistrationAsQueryable(driverDTO.Id).FirstOrDefaultAsync())
                    ?.RegistrationId;
            }
            else if (driverDTO == null || !string.IsNullOrEmpty(driverDTO.Tenant_Id))
            {
                return null;
            }
            return driverDTO;
        }

        public async Task<DriverResultDTO> GetByUserIdAsync(string userId)
        {
            return await context.Driver
                   .IgnoreQueryFilters()
                   .Where(d => !d.IsDeleted)
                   .ProjectTo<DriverResultDTO>(mapper.ConfigurationProvider)
                   .FirstOrDefaultAsync(user => user.UserId == userId);
        }
        public async Task<Driver> GetDriverByUserIdAsync(string userId)
        {
            return await context.Driver.IgnoreQueryFilters()
                   .Where(d => !d.IsDeleted)
                   .FirstOrDefaultAsync(user => user.UserId == userId);
        }
        public async Task<DriverResultDTO> GetCurrentDriverAsync()
        {
            return await InternalGetWithIgnoreFilter()
                  .Where(d => !d.IsDeleted)
                  .Where(driver => driver.Id == currentUser.DriverId)
                  .ProjectTo<DriverResultDTO>(mapper.ConfigurationProvider)
                  .FirstOrDefaultAsync();
        }

        public async Task<DriverTransportionTypeResultDTO> GetDriverTransportionTypeAsync(string userId)
        {
            return await context.Driver
                .IgnoreQueryFilters()
                .Where(driver => driver.UserId == userId && !driver.IsDeleted)
                .ProjectTo<DriverTransportionTypeResultDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
        public async Task<List<string>> GetTagsAsync()
        {
            var tags = await context.Driver
                .Where(driver => !string.IsNullOrEmpty(driver.Tags))
                .Select(driver => driver.Tags)
                .ToListAsync();
            var splitTags = tags.SelectMany(tag => tag.Split(','))
                .Distinct()
                .ToList();
            return splitTags;
        }

        public async Task<List<ExportDriverResponseDTO>> ExportExcelAsync(DriverFilterDTO filter)
        {
            return await context.Driver
                .IgnoreQueryFilters()
                .Where(c=>!c.IsDeleted)
                .Where(ApplyFilter(filter))
                .ProjectTo<ExportDriverResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<PagedResult<DriverResultDTO>> GetPlatformDriverByPaginationAsync(DriverFilterDTO filter)
        {
            var notCompletedList = new List<int>
            {
                (int)TaskStatusEnum.Assigned,
                (int)TaskStatusEnum.Accepted,
                (int) TaskStatusEnum.Started,
                (int)TaskStatusEnum.Inprogress
            };

            var driverCurrentTasks = await context.Tasks
                .IgnoreQueryFilters()
                .Where(task => notCompletedList.Contains(task.TaskStatusId)
                    && !task.IsDeleted
                    && task.Tenant_Id == currentUser.TenantId)
                .Select(task => new { task.DriverId, Status = task.TaskStatusId })
                .ToListAsync();

            var query = context.Driver
                .Include(driver => driver.User)
                .Include(driver => driver.CurrentLocation)
                .IgnoreQueryFilters()
                .Where(driver => driverCurrentTasks.Select(task => task.DriverId).Contains(driver.Id))
                .Where(ApplyFilter(filter));

            var pagedResult = new PagedResult<DriverResultDTO>
            {
                TotalCount = await query.CountAsync(),
                Result = await query
                .Skip((filter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ProjectTo<DriverResultDTO>(mapper.ConfigurationProvider)
                .ToListAsync()
            };
            foreach (var driver in pagedResult.Result)
            {
                var statusId = driver.AgentStatusId != (int)AgentStatusesEnum.Available && driver.AgentStatusId != (int)AgentStatusesEnum.Busy
                    ? driver.AgentStatusId
                    : driverCurrentTasks.Where(task => task.Status == (int)TaskStatusEnum.Inprogress || task.Status == (int)TaskStatusEnum.Started).Any()
                        ? (int)AgentStatusesEnum.Busy
                        : (int)AgentStatusesEnum.Available;

                driver.ImageUrl = string.IsNullOrEmpty(driver.ImageUrl)
                    ? string.Empty
                    : driver.ImageUrl.Replace("DriverImages", string.Empty);
                driver.AgentStatusId = statusId;
                driver.AgentStatusName = ((AgentStatusesEnum)statusId).ToString();
            };

            return pagedResult;
        }



        private IQueryable<Driver> InternalGetWithIgnoreFilterForExpress()
        {
            return context.Driver.Where(driver => !driver.IsDeleted).IgnoreQueryFilters();
        }

        private IQueryable<Driver> InternalGetWithIgnoreFilter()
        {
            return context.Driver.Include(a => a.DriverExternalBranchs.Where(a => !a.IsDeleted))
                                 .Where(driver => !driver.IsDeleted);
        }
        private async Task<PagedResult<DriverResultDTO>> InternalGetAllDriverByPaginationAsync(
                   DriverFilterDTO driverFilterDTO,
                   IEnumerable<int> managerTeamIds = null)
        {
            IQueryable<Driver> query = context.Driver
               .Where(ApplyFilter(driverFilterDTO))
               .IgnoreQueryFilters()
                .Where(c => !c.IsDeleted)
                .OrderByDescending(c => c.Id)
                .AsQueryable();
            if (managerTeamIds != null)
            {
                query = query.Where(driver => managerTeamIds.Contains(driver.TeamId));
            }

            //  var  pagedResult = await query.ToPagedResultAsync<Driver, DriverResultDTO>(driverFilterDTO, mapper.ConfigurationProvider);
            var pagedResult = await query.ProjectTo<DriverResultDTO>(mapper.ConfigurationProvider).ToPagedResultAsync(driverFilterDTO);


            var roles = await GetDriverRoleAsQueryable(pagedResult.Result?.Select(driver => driver.UserId).ToList())
                .ToListAsync();
            var driverRegistrations = await GetDriverRegistrationAsQueryable(pagedResult.Result?.Select(driver => driver.Id).ToList())
                .ToListAsync();
            foreach (DriverResultDTO driverDTO in pagedResult.Result)
            {
                //    driverDTO.ImageUrl = string.IsNullOrEmpty(driverDTO.ImageUrl)
                //        ? string.Empty
                //        : driverDTO.ImageUrl.Replace("DriverImages", "");
                driverDTO.RoleNames = roles.Where(role => role.UserId == driverDTO.UserId)
                    .Select(role => role.RoleName)
                    .Distinct()
                    .ToList();
                driverDTO.DriverRegistrationId = driverRegistrations
                    .Where(registration => registration.DriverId == driverDTO.Id)
                    .Select(registration => registration.RegistrationId)
                    .FirstOrDefault();
            }

            return pagedResult;
        }



        private async Task<PagedResult<DriverResultForExpressDTO>> InternalGetAllDriverByPaginationExpressAsync(
          DriverFilterDTO driverFilterDTO,
          IEnumerable<int> managerTeamIds = null)
        {

            IQueryable<Driver> query = context.Driver
               .Where(ApplyFilter(driverFilterDTO))
               .IgnoreQueryFilters()
                .Where(c => !c.IsDeleted)
                .OrderByDescending(c => c.Id)
                .AsQueryable();
            if (managerTeamIds != null)
            {
                query = query.Where(driver => managerTeamIds.Contains(driver.TeamId));
            }

            //  var  pagedResult = await query.ToPagedResultAsync<Driver, DriverResultDTO>(driverFilterDTO, mapper.ConfigurationProvider);
            var pagedResult = await query.ProjectTo<DriverResultForExpressDTO>(mapper.ConfigurationProvider).ToPagedResultAsync(driverFilterDTO);


            var roles = await GetDriverRoleAsQueryable(pagedResult.Result?.Select(driver => driver.UserId).ToList())
                .ToListAsync();
            var driverRegistrations = await GetDriverRegistrationAsQueryable(pagedResult.Result?.Select(driver => driver.Id).ToList())
                .ToListAsync();
            foreach (DriverResultForExpressDTO driverDTO in pagedResult.Result)
            {
                //    driverDTO.ImageUrl = string.IsNullOrEmpty(driverDTO.ImageUrl)
                //        ? string.Empty
                //        : driverDTO.ImageUrl.Replace("DriverImages", "");
                driverDTO.RoleNames = roles.Where(role => role.UserId == driverDTO.UserId)
                    .Select(role => role.RoleName)
                    .Distinct()
                    .ToList();
                driverDTO.DriverRegistrationId = driverRegistrations
                    .Where(registration => registration.DriverId == driverDTO.Id)
                    .Select(registration => registration.RegistrationId)
                    .FirstOrDefault();
            }

            return pagedResult;
        }






        private IQueryable<DriverRoleLookupDTO> GetDriverRoleAsQueryable(List<string> userIds)
        {
            return from role in context.Roles.IgnoreQueryFilters()
                   join userRole in context.UserRoles on role.Id equals userRole.RoleId
                   where userIds.Contains(userRole.UserId)
                   select new DriverRoleLookupDTO
                   {
                       RoleName = role.Name,
                       UserId = userRole.UserId
                   };
        }
        private IQueryable<DriverRegistrationLookupDTO> GetDriverRegistrationAsQueryable(int driver)
        {
            return context.DriverRegistrations
                  .IgnoreQueryFilters()
                  .Where(registration => registration.UserRegistrationStatus == UserRegistrationStatus.Approved &&
                      registration.DriverId.HasValue)
                  .Where(registration => driver == registration.DriverId.Value)
                  .Select(registration => new DriverRegistrationLookupDTO
                  {
                      RegistrationId = registration.Id,
                      DriverId = registration.DriverId.Value
                  });
        }
        private IQueryable<DriverRegistrationLookupDTO> GetDriverRegistrationAsQueryable(List<int> drivers)
        {
            return context.DriverRegistrations
                  .IgnoreQueryFilters()
                  .Where(registration => registration.UserRegistrationStatus == UserRegistrationStatus.Approved &&
                      registration.DriverId.HasValue)
                  .Where(registration => drivers.Contains(registration.DriverId.Value))
                  .Select(registration => new DriverRegistrationLookupDTO
                  {
                      RegistrationId = registration.Id,
                      DriverId = registration.DriverId.Value
                  });
        }

        private Expression<Func<Driver, bool>> ApplyFilter(DriverFilterDTO driverFilterDTO)
        {
            return driver =>
               (string.IsNullOrWhiteSpace(driverFilterDTO.SearchBy) ||
                driver.User.FirstName.Contains(driverFilterDTO.SearchBy) ||
                driver.User.MiddleName.Contains(driverFilterDTO.SearchBy) ||
                driver.User.LastName.Contains(driverFilterDTO.SearchBy) ||
                driver.CompanyEmail.Contains(driverFilterDTO.SearchBy) ||
                driver.MobileNumber.Contains(driverFilterDTO.SearchBy)
               )
               && driver.IsDeleted == false
               && (driverFilterDTO.StatusType == default ||
               driver.AgentStatusId == (int)driverFilterDTO.StatusType)
               && (driverFilterDTO.DriverType == default ||
               driver.AgentTypeId == driverFilterDTO.DriverType);
        }

        public Task<bool> AnyActiveDriverHasGeofencesAsync(List<int> GeoFenceIDs, int? DriverId = null)
        {
            return context.Driver
                .IgnoreQueryFilters()
                .Where(x => !x.IsDeleted && x.AccountStatusId == 1 && (DriverId == null || x.Id != DriverId))
                .AnyAsync(x => x.DriverDeliveryGeoFences.Any(x => GeoFenceIDs.Contains(x.GeoFenceId)));
        }

        public async Task<int> GetDriverCountAsync()
        {
            return context.Driver.IgnoreQueryFilters()
                .Where(x => !x.IsDeleted).Count();
        }

        public async Task<bool> HasAnyActiveWorkordersAsync(int driverId)
        {
            return await context.Workorders.IgnoreQueryFilters().AnyAsync(a => a.DriverId == driverId && !a.IsDeleted
             && (a.WorkOrderStatusId == (int)WorkOrderStatuses.Active || a.WorkOrderStatusId == (int)WorkOrderStatuses.Dispatched));
        }
    }
}
