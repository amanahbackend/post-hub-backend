﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Mobile;
using Amanah.Posthub.Context;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Drivers.Queries
{
    public interface IDriverTaskNotificationQuery
    {
        int GetUnReadCount(int driverId);
        Task<PagedResult<DriverTaskNotificationResultDTO>> GetByPaginationAsync(
            PaginatedTasksDriverMobileInputDTO pagingparametermodel);
    }

    public class DriverTaskNotificationQuery : IDriverTaskNotificationQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public DriverTaskNotificationQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }


        public int GetUnReadCount(int driverId)
        {
            return context.DriverTaskNotification
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == driverId && x.IsRead != true && !x.IsDeleted)
                .Count();
        }

        public async Task<PagedResult<DriverTaskNotificationResultDTO>> GetByPaginationAsync(
            PaginatedTasksDriverMobileInputDTO pagingparametermodel)
        {
            var notificationQuery = context.DriverTaskNotification
                .Where(x => x.DriverId == pagingparametermodel.DriverId && !x.IsDeleted)
                .IgnoreQueryFilters()
                .OrderByDescending(x => x.CreatedDate);

            int currentPage = pagingparametermodel.PageNumber;
            int pageSize = pagingparametermodel.PageSize;

            var result = await notificationQuery.Skip((currentPage - 1) * pageSize).Take(pageSize).ProjectTo<DriverTaskNotificationResultDTO>(mapper.ConfigurationProvider).ToListAsync();

            var pagedResult = new PagedResult<DriverTaskNotificationResultDTO>
            {
                TotalCount = await notificationQuery.CountAsync(),
                Result = result
            };

            return pagedResult;
        }










    }
}
