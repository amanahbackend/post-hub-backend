﻿using Amanah.Posthub.BLL.Drivers.Queries.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Amanah.Posthub.SharedKernel.Utilites.Helpers;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Drivers.Queries
{
    public interface IDriverloginTrackingQuery
    {
        Task<PagedResult<DriverloginTrackingResultDTO>> GetByPaginationAsync(DriverLoginTrackingFilterDTO driverLoginTrackingFilterDTO);
        Task<DriverloginTrackingResultDTO> GetLoggedInAsync(int driverId);
        Task<List<ExportDriverloginTrackingResponseDTO>> ExportToExcelAsync(DriverLoginTrackingFilterDTO filter);
    }

    public class DriverloginTrackingQuery : IDriverloginTrackingQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public DriverloginTrackingQuery(
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<PagedResult<DriverloginTrackingResultDTO>> GetByPaginationAsync(DriverLoginTrackingFilterDTO filter)
        {
            var query = context.DriverloginTracking
                .IgnoreQueryFilters()
                .AsQueryable();
            if (filter.LogoutDate.HasValue)
            {
                query = query.Where(driverLoginRequest => driverLoginRequest.LogoutDate.HasValue
                    && driverLoginRequest.LogoutDate.Value >= TimeZoneInfo.ConvertTimeToUtc(filter.LogoutDate.Value));
            }
            if (!string.IsNullOrEmpty(filter.SearchBy))
            {
                query = query.Where(driverLoginRequest => driverLoginRequest.Driver != null &&
                    driverLoginRequest.Driver.User != null &&
                    (driverLoginRequest.Driver.User.UserName.ToLower().Contains(filter.SearchBy.ToLower()) ||
                    driverLoginRequest.Driver.User.FirstName.ToLower().Contains(filter.SearchBy.ToLower()) ||
                    driverLoginRequest.Driver.User.LastName.ToLower().Contains(filter.SearchBy.ToLower())));
            }
            if (filter.DriverId.HasValue)
            {
                query = query.Where(driverLoginRequest => driverLoginRequest.DriverId == filter.DriverId.Value);
            }

            return await query
                .OrderByDescending(driverLoginRequest => driverLoginRequest.LoginDate)
                .ToPagedResultAsync<DriverloginTracking, DriverloginTrackingResultDTO>(filter, mapper.ConfigurationProvider);
        }

        public async Task<DriverloginTrackingResultDTO> GetLoggedInAsync(int driverId)
        {
            return await context.DriverloginTracking
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == driverId)
                .OrderByDescending(x => x.LoginDate)
                .ProjectTo<DriverloginTrackingResultDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<List<ExportDriverloginTrackingResponseDTO>> ExportToExcelAsync(DriverLoginTrackingFilterDTO filter)
        {
            Expression<Func<DriverloginTracking, bool>> predicate = driverLoginTracking => true;
            if (filter.LogoutDate.HasValue)
            {
                predicate = PredicateBuilder.And(predicate,
                    driverLoginTracking => driverLoginTracking.LogoutDate.HasValue
                        && filter.LogoutDate.Value.Date == driverLoginTracking.LogoutDate.Value.Date);
            }
            if (!string.IsNullOrEmpty(filter.SearchBy))
            {
                predicate = PredicateBuilder.And(predicate,
                    driverLoginTracking => driverLoginTracking.Driver != null
                        && driverLoginTracking.Driver.User != null
                        && (driverLoginTracking.Driver.User.UserName.ToLower().Contains(filter.SearchBy.ToLower())
                        || driverLoginTracking.Driver.User.FirstName.ToLower().Contains(filter.SearchBy.ToLower())
                        || driverLoginTracking.Driver.User.LastName.ToLower().Contains(filter.SearchBy.ToLower())));
            }

            var city = filter.TimeZone.Split('/').Length > 1
               ? filter.TimeZone.Split('/')[1]
               : filter.TimeZone;
            TimeZoneInfo currentTimeZone = TimeZoneUtility.GetCurrent(city);

            return await context.DriverloginTracking
                .Include(driverLoginTracking => driverLoginTracking.Driver).ThenInclude(x => x.User)
                .Where(predicate)
                .Select(driverLoginTracking => new ExportDriverloginTrackingResponseDTO
                {
                    Id = driverLoginTracking.Id,
                    DriverId = driverLoginTracking.DriverId,
                    LoginDate = driverLoginTracking.LoginDate.HasValue && currentTimeZone != null
                        ? TimeZoneInfo.ConvertTimeFromUtc(driverLoginTracking.LoginDate.Value, currentTimeZone)
                        : driverLoginTracking.LoginDate,
                    LoginLatitude = driverLoginTracking.LoginLatitude,
                    LoginLongitude = driverLoginTracking.LoginLongitude,
                    LogoutDate = driverLoginTracking.LogoutDate.HasValue && currentTimeZone != null
                        ? TimeZoneInfo.ConvertTimeFromUtc(driverLoginTracking.LogoutDate.Value, currentTimeZone)
                        : driverLoginTracking.LogoutDate,
                    LogoutLatitude = driverLoginTracking.LogoutLatitude,
                    LogoutLongitude = driverLoginTracking.LogoutLongitude,
                    LogoutActionBy = driverLoginTracking.LogoutActionBy,
                    DriverName = driverLoginTracking.Driver.User.UserName
                })
                .ToListAsync();
        }


    }
}
