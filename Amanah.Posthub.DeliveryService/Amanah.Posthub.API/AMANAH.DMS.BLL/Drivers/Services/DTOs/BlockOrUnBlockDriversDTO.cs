﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Drivers.Services.DTOs
{
    public class BlockOrUnBlockDriversDTO
    {
        public List<int> DriverIds { get; set; }
        public string Reason { get; set; }
    }
}
