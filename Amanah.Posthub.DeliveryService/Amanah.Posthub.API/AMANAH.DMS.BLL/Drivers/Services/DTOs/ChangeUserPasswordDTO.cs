﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.DeliverService.BLL.Drivers.Services.DTOs
{
   public class ChangeUserPasswordDTO
    {
        public string UserId { get; set; }
        public string NewPassword { get; set; }
    }
}
