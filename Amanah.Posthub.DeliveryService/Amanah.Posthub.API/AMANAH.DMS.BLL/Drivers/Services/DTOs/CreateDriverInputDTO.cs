﻿using Amanah.Posthub.DeliverService.BLL.DriverFloor.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.Drivers.Services.DTOs;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Drivers.Services.DTOs
{
    public class CreateDriverInputDTO
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public IFormFile FormFile { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

       
        public string PhoneNumber { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        // public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public string RoleName { get; set; } = "Delegate";
        public int TeamId { set; get; }
        public int? AgentTypeId { set; get; } = 1;
        public int? AgentStatusId { set; get; } = (int)AgentStatusesEnum.Offline;
        public int? TransportTypeId { set; get; }
        public int? CountryId { set; get; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Address Address { set; get; }
        public string MobileNumber { get; set; }
        public string MiddleName { get; set; }
        public string LicenseNumber { get; set; }
        public int DepartmentId { set; get; }
        public int BranchId { get; set; }
        public int ServiceSectorId { set; get; }
        public DateTime? DateOfBirth { get; set; }
        public int NationalityId { get; set; }
        public string NationalId { get; set; }
        public string SupervisorId { get; set; }
        public int AccountStatusId { get; set; }
        public int GenderId { get; set; }
        public string CompanyEmail { get; set; }
        public int? DriverRoleId { get; set; }


        public List<CreateDriverGeofenceInputDTO> DriverPickUpGeoFences { get; set; }
        public List<CreateDriverGeofenceInputDTO> DriverDeliveryGeoFences { get; set; }
        public class CreateDriverInputDTOValidator : AbstractValidator<CreateDriverInputDTO>
        {


            private readonly IUserRepository userRepository;

            public CreateDriverInputDTOValidator(IUserRepository userRepository,
                ILocalizer localizer)
            {
                this.userRepository = userRepository;
                RuleFor(driver => driver.FirstName)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(100);
                //RuleFor(driver => driver.PhoneNumber)
                //    .NotEmpty()
                //    .NotNull();
                RuleFor(driver => driver.TeamId)
                    .NotEmpty()
                    .NotNull();
                RuleFor(driver => driver.UserName)
                    .NotEmpty()
                    .NotNull()
                    .MustAsync(IsUsernameExistAsync)
                    .WithMessage(driver => localizer.Format(Keys.Validation.UserNameAlreadyExists, driver.UserName));
                //RuleFor(registration => registration.Email)
                //       .NotEmpty()
                //       .MustAsync(IsEmailExistAsync)
                //       .WithMessage(localizer[Keys.Validation.EmailAlreadyExists]);
            }

            public async Task<bool> IsUsernameExistAsync(string username, CancellationToken cancellationToken)
            {
                var user = await userRepository.GetAsync(user => user.UserName.ToLower().Trim() == username.ToLower().Trim());
                if (user == null)
                {
                    return true;
                }
                return false;
            }
            public async Task<bool> IsEmailExistAsync(string email, CancellationToken cancellationToken)
            {
                var user = await userRepository.GetAsync(user => user.NormalizedEmail == email.ToUpper().Trim());
                if (user == null)
                {
                    return true;
                }
                return false;
            }

        }
        public List<DriverFloorDTO> DriverFloors { get; set; }
        public List<DriverExternalBranchInputDTO> DriverExternalBranchs { get; set; }

    }
    public class CreateDriverGeofenceInputDTO
    {
        public int Id { set; get; }
        public int? DriverId { set; get; }
        public int GeoFenceId { set; get; }
    }



}
