﻿namespace Amanah.Posthub.BLL.Drivers.Services.DTOs
{
    public class DriverGeofenceInputDTO
    {
        public int Id { set; get; }
        public int? DriverId { set; get; }
        public int GeoFenceId { set; get; }
    }


}
