﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;

namespace Amanah.Posthub.BLL.Drivers.Services.DTOs
{
    public class DriverLoginResponseDTO : UserLoginResponseDTO
    {
        public DriverLoginResponseDTO()
        {
            this.User = new UserResponseDTO();
        }

        public string ProfilePictureURL { set; get; }

        public int DriverID { set; get; }
        public int AccountStatusId { get; set; }
        public int? TransportTypeId { set; get; }
        public string TransportTypeName { get; set; }
        public int? AgentStatusId { set; get; }
        public int NewTasksCount { get; set; }

        public string LocationAccuracyName { set; get; }
        public int? LocationAccuracyDuration { set; get; }
        public int? ServiceSectorId { get; set; }
    }


    public class UserLoginResponseDTO
    {
        public UserLoginResponseDTO()
        {
            Errors = new List<IdentityError>();
            Succeeded = true;
        }

        public string Error => string.Join("-", Errors.Select(x => x.Description));
        public ICollection<IdentityError> Errors { get; set; }

        private bool succeeded;
        public bool Succeeded
        {
            get => succeeded && !Errors.Any();
            set => succeeded = value;
        }

        public UserResponseDTO User { get; set; }
        public TokenResponseDTO TokenResponse { get; set; }

    }

    public class UserResponseDTO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int CountryId { set; get; }
        public List<string> RoleNames { get; set; }
        public List<string> Permissions { get; set; }
    }

    public class TokenResponseDTO
    {
        public string AccessToken { get; set; }
        public string TokenType { get; } = "Bearer";
    }

}
