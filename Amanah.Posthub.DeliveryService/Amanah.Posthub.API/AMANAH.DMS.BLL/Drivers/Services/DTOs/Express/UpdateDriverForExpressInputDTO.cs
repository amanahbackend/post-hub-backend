﻿using System;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using Amanah.Posthub.BLL.Drivers.Services.DTOs;

namespace Amanah.Posthub.DeliverService.BLL.Drivers.Services.DTOs.Express
{
    public class UpdateDriverForExpressInputDTO
    {
        public int Id { set; get; }
        public string File { get; set; }
        public IFormFile FormFile { get; set; }
        //public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public int? TransportTypeId { set; get; }
        public string Color { get; set; }
        public string Role { get; set; } = "Delegate";
        public int TeamId { set; get; }
        public int? AgentTypeId { set; get; } = 1;
        public int? CountryId { set; get; }
        public string UserId { get; set; }

        public string Email { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; } = "Delegate";
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageUrl { get; set; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }


        /// <summary>
        /// 
        /// </summary>

        public Address Address { set; get; }
        public string MobileNumber { get; set; }
        public string MiddleName { get; set; }
        public string LicenseNumber { get; set; }
        public int DepartmentId { set; get; }
        public int BranchId { get; set; }
        public int ServiceSectorId { set; get; }
        public DateTime? DateOfBirth { get; set; }
        public int NationalityId { get; set; }
        public string NationalId { get; set; }
        public string SupervisorId { get; set; }
        public int AccountStatusId { get; set; }
        public int GenderId { get; set; }

        public string CompanyEmail { get; set; }


        public List<UpdateDriverGeofenceInputDTO> DriverPickUpGeoFences { get; set; }
        public List<UpdateDriverGeofenceInputDTO> DriverDeliveryGeoFences { get; set; }

        public class UpdateDriverInputDTOValidator : AbstractValidator<UpdateDriverForExpressInputDTO>
        {
            public UpdateDriverInputDTOValidator()
            {
                RuleFor(x => x.FirstName)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(100);
                //RuleFor(x => x.PhoneNumber)
                //    .NotEmpty()
                //    .NotNull();
                RuleFor(x => x.TeamId)
                    .NotEmpty()
                    .NotNull();

            }
        }

    }




}
