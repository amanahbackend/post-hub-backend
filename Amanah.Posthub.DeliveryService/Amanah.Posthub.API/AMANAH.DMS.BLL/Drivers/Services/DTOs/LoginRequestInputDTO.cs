﻿using System.ComponentModel.DataAnnotations;

namespace Amanah.Posthub.BLL.Drivers.Services.DTOs
{
    public class LoginRequestInputDTO
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public string FCMDeviceId { get; set; }
    }
}