﻿namespace Amanah.Posthub.BLL.Drivers.Services.DTOs
{
    public class SetDriverDutyInputDTO
    {
        public string DriverId { set; get; }  //UserId need to be changed
        public bool IsAvailable { set; get; }
    }
}
