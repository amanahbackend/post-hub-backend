﻿using Amanah.Posthub.DeliverService.BLL.DriverFloor.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.Drivers.Services.DTOs;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Drivers.Services.DTOs
{
    public class UpdateDriverInputDTO
    {
        public int Id { set; get; }
        public string File { get; set; }
        public IFormFile FormFile { get; set; }
        //public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public int? TransportTypeId { set; get; }
        public string Color { get; set; }
        public string Role { get; set; } = "Delegate";
        public int TeamId { set; get; }
        public int? AgentTypeId { set; get; } = 1;
        public int? CountryId { set; get; }
        public string UserId { get; set; }

        public string Email { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; } = "Delegate";
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageUrl { get; set; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }


        /// <summary>
        /// 
        /// </summary>

        public Address Address { set; get; }
        public string MobileNumber { get; set; }
        public string MiddleName { get; set; }
        public string LicenseNumber { get; set; }
        public int DepartmentId { set; get; }
        public int BranchId { get; set; }
        public int ServiceSectorId { set; get; }
        public DateTime? DateOfBirth { get; set; }
        public int NationalityId { get; set; }
        public string NationalId { get; set; }
        public string SupervisorId { get; set; }
        public int AccountStatusId { get; set; }
        public int GenderId { get; set; }
        public string CompanyEmail { get; set; }
        public int? DriverRoleId { get; set; }




        public List<UpdateDriverGeofenceInputDTO> DriverPickUpGeoFences { get; set; }
        public List<UpdateDriverGeofenceInputDTO> DriverDeliveryGeoFences { get; set; }
        public List<DriverFloorDTO> DriverFloors { get; set; }
        public List<DriverExternalBranchInputDTO> DriverExternalBranchs { get; set; }

        public class UpdateDriverInputDTOValidator : AbstractValidator<UpdateDriverInputDTO>
        {
            public UpdateDriverInputDTOValidator()
            {
                RuleFor(x => x.FirstName)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(100);
                //RuleFor(x => x.PhoneNumber)
                //    .NotEmpty()
                //    .NotNull();
                RuleFor(x => x.TeamId)
                    .NotEmpty()
                    .NotNull();

            }
        }

    }


    public class UpdateDriverGeofenceInputDTO
    {
        public int Id { set; get; }
        public int? DriverId { set; get; }
        public int GeoFenceId { set; get; }
    }

}
