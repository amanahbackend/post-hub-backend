﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Drivers.Services.DTOs
{
    public class UpdateDriversAgentTypeInputDTO
    {
        public List<int> DriverIds { get; set; }
        public int AgentTypeId { set; get; }
    }
}
