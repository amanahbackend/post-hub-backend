﻿using Amanah.Posthub.BLL.Drivers.Services.DTOs;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.ViewModels.Account;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.ViewModels;
using IdentityModel.Client;
using MapsUtilities;
using MapsUtilities.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Drivers.Services
{
    public partial class DriverService : IDriverService
    {
        public async Task<KeyValuePair<bool, string>> CheckInBranchAsync(double Latitude, double Longitude)
        {
            if (!currentUser.DriverId.HasValue)
            {
                return new KeyValuePair<bool, string>(false, Keys.Validation.CurrentUserIsNotDriver);
            }
            var driver = await driverRepository.GetWithCurrentLocationAsync(currentUser.DriverId.Value);
            if (driver == null)
            {
                return new KeyValuePair<bool, string>(false, Keys.Validation.DriverNotFound);
            }
            if (driver.CurrentLocation == null)
            {
                return new KeyValuePair<bool, string>(false, Keys.Validation.CanNotCheckInOutOfRangeOfAnyBranch);
            }
            if (!driver.AllPickupGeoFences && (driver.DriverPickUpGeoFences == null || !driver.DriverPickUpGeoFences.Any()))
            {
                return new KeyValuePair<bool, string>(false, Keys.Validation.CanNotCheckInNoGeofencesFound);
            }
            if (driver.AgentStatusId != (int)AgentStatusesEnum.Available)
            {
                return new KeyValuePair<bool, string>(false, Keys.Validation.CanNotCheckInTasksnNotCompleted);
            }

            driver.CurrentLocation.Latitude = Latitude;
            driver.CurrentLocation.Longitude = Longitude;

            //TODO later get geofences with branches only
            var driverCurrentGeoFence = await GetDriverCurrentGeoFenceAsync(driver.AllPickupGeoFences,
                driver.DriverPickUpGeoFences.Select(geofence => geofence.GeoFenceId).ToList(),
                driver.CurrentLocation);
            if (driverCurrentGeoFence == null)
            {
                return new KeyValuePair<bool, string>(false, Keys.Validation.CanNotCheckInOutOfRangeOfAnyGeofence);
            }

            var geofenceBranches = await branchRepository.GetAllAsync(branch => branch.GeoFenceId == driverCurrentGeoFence.Id);
            if (geofenceBranches == null || !geofenceBranches.Any())
            {
                return new KeyValuePair<bool, string>(false, Keys.Validation.CanNotCheckInOutOfRangeOfAnyBranch);
            }

            var driverCurrentBranch = await GetRelatedBranchAsync(geofenceBranches.ToList(), new MapPoint
            {
                Latitude = Latitude,
                Longitude = Longitude
            });
            if (driverCurrentBranch == null)
            {
                return new KeyValuePair<bool, string>(false, Keys.Validation.CanNotCheckInOutOfRangeOfAnyBranch);
            }

            driver.CurrentLocation.BranchId = driverCurrentBranch.Id;
            driver.AgentStatusId = (int)AgentStatusesEnum.Available;
            driver.ReachedTime = DateTime.UtcNow;
            driver.IsInClubbingTime = false;
            driver.ClubbingTimeExpiration = null;
            driver.DriverPickUpGeoFences = null;

            driverRepository.Update(driver);
            await unityOfWork.SaveChangesAsync();

            var autoAssignmentTask = await taskAutoAssignmentFactory.CreateTaskAutoAssignmentAsync();
            await autoAssignmentTask.AssignTasksToDriverAsync(driver.Id);

            return new KeyValuePair<bool, string>(true, driverCurrentBranch.Name);
        }
        public async Task<DriverLoginResponseDTO> LoginAsync(DriverLoginInputDTO driverLogin)
        {
            DriverLoginResponseDTO driverLoginResponse = new DriverLoginResponseDTO();

            var user = await userDomainService.LoginAsync(driverLogin.Password, userName: driverLogin.Email, null);
            if (user == null)
            {
                driverLoginResponse.Errors.Add(new IdentityError
                {
                    //Code = "UserNotFound",
                    //Description = localizer[Keys.Validation.UserNotFound],
                     Code = "InvalidloginAttempts",
                    Description = localizer[Keys.Validation.InvalidloginAttempts]
                    
                });
                return driverLoginResponse;
            }

            var driver = await driverRepository.GetAllIQueryable(d => d.UserId == user.Id)
                .Include(d => d.CurrentLocation)
                .Include(d => d.TransportType)
                .Include(d => d.Team).ThenInclude(t => t.LocationAccuracy)
                .FirstOrDefaultAsync();
            if (driver == null || driver.AgentStatusId == (int)AgentStatusesEnum.Blocked)
            {
                driverLoginResponse.Errors.Add(new IdentityError()
                {
                    Code = "NotDriver",
                    Description = driver == null
                    ? localizer[Keys.Validation.UserIsNotRegisteredAsDriver]
                    : localizer[Keys.Validation.DriverAccountBlocked]
                });
                return driverLoginResponse;
            }




            var tokenResponse = new PosthubTokenResponse();
            var IdentityUrl = _configuration.GetValue<string>("IdentityUrl");
            var Secret = _configuration.GetValue<string>("Secret");
            var ClientId = _configuration.GetValue<string>("ClientId");



            var client = _httpClientFactory.CreateClient();

            var disco = await client.GetDiscoveryDocumentAsync(IdentityUrl);
            //var disco = await client.GetDiscoveryDocumentAsync(
            //    new DiscoveryDocumentRequest
            //    {
            //        Address = IdentityUrl,
            //        Policy = {
            //             Authority=IdentityUrl,
            //             ValidateIssuerName=false, 
            //            RequireHttps = false }
            //    }
            //    );

            if (disco.IsError) throw new Exception(disco.Error);

            var tokenClient = _httpClientFactory.CreateClient();

            this._logger.LogWarning("[LoginResponse[]" + JsonConvert.SerializeObject(tokenClient), tokenClient);
            this._logger.LogWarning($"[LoginResponse[]  -{IdentityUrl} - {Secret} - {ClientId}" + JsonConvert.SerializeObject(tokenClient), tokenClient);
            this._logger.LogInformation($"[LoginResponse[]  -{driverLogin.Email} - {driverLogin.Password}");

            var tokenResult = await tokenClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = ClientId,
                ClientSecret = Secret,
                UserName = driverLogin.Email,
                Password = driverLogin.Password
            });

            this._logger.LogWarning($"[LoginResponse]   {tokenResult.AccessToken} -- {tokenResult.Error} -- {tokenResult.ErrorType.ToString()} -- {tokenResult.Json} --  {tokenResult.ErrorDescription} ---", tokenResult);

            var tokenResponseDTO = new TokenResponseDTO();
            if (tokenResult.AccessToken == null)
            {
                driverLoginResponse.Errors.Add(new IdentityError
                {
                    Code = "InvalidLogin",
                    Description = localizer[Keys.Validation.InvalidUserNameOrPassword]
                });
                return driverLoginResponse;
            }
            else {
                tokenResponseDTO.AccessToken = tokenResult.AccessToken;
            }

            await unityOfWork.RunTransaction(async () =>
            {
                driver.AgentStatusId = await taskCommonService.CheckIfDriverHasOnGoingTasksAsync(driver.Id)
                    ? (int)AgentStatusesEnum.Busy : (int)AgentStatusesEnum.Available;
                driver.ReachedTime = null;
                driver.ClubbingTimeExpiration = null;
                driver.IsInClubbingTime = false;
                driver.MaxOrdersWeightsCapacity = 1;
                driver.MinOrdersNoWait = 1;
                driver.DeviceType = driverLogin.DeviceType;
                driver.Version = driverLogin.Version;
                driver.TokenTimeExpiration = DateTime.UtcNow.AddDays(1);
                if (driver.CurrentLocation == null)
                {
                    driver.CurrentLocation = new DriverCurrentLocation(driver.Tenant_Id);
                }
                driver.CurrentLocation.Latitude = driverLogin.Latitude;
                driver.CurrentLocation.Longitude = driverLogin.Longitude;

                driverRepository.Update(driver);

                await userDeviceRepository.AddOrUpdateAsync(new UserDevice
                {
                    FCMDeviceId = driverLogin.FCMDeviceId,
                    UserId = user.Id,
                    Version = driverLogin.Version,
                    DeviceType = driverLogin.DeviceType,
                    IsLoggedIn = true
                });
                driverLoginTrackingRepository.Add(new DriverloginTracking(driver.Tenant_Id)
                {
                    DriverId = driver.Id,
                    LoginDate = DateTime.UtcNow,
                    LoginLatitude = driverLogin.Latitude,
                    LoginLongitude = driverLogin.Longitude,
                    LogoutDate = null
                });
                driverWorkingHourTrackingRepository.Add(DriverWorkingHourTracking.CreateLogin(driver.Tenant_Id, driver.Id));

                await unityOfWork.SaveChangesAsync();
                driver.User = user;
                await LoginNotificationAsync(driver);
            });
            driverLoginResponse = mapper.Map<DriverLoginResponseDTO>(driver);
            driverLoginResponse.NewTasksCount = await taskCommonService.CountDriverNewMainTasksAsync(driver.Id);
            driverLoginResponse.TokenResponse = tokenResponseDTO;

            return driverLoginResponse;
        }
        public async Task<bool> LogoutAsync(string userId, double latitude, double longitude)
        {
            var driver = await driverRepository.GetAllIQueryable(d => d.UserId == userId)
                .IgnoreQueryFilters()
                .Include(d => d.CurrentLocation)
                .Include(d => d.User)
                .FirstOrDefaultAsync();
            if (driver != null)
            {
                await unityOfWork.RunTransaction(async () =>
                {
                    driver.AgentStatusId = (int)AgentStatusesEnum.Offline;
                    driver.ReachedTime = null;
                    driver.ClubbingTimeExpiration = null;
                    driver.IsInClubbingTime = false;
                    driver.MaxOrdersWeightsCapacity = 1;
                    driver.MinOrdersNoWait = 1;
                    if (driver.CurrentLocation == null)
                    {
                        driver.CurrentLocation = new DriverCurrentLocation(driver.Tenant_Id);
                    }
                    driver.CurrentLocation.Latitude = latitude;
                    driver.CurrentLocation.Longitude = longitude;
                    driverRepository.Update(driver);

                    await userDeviceRepository.SoftDeleteAsync(userdevice => userdevice.UserId == userId);
                    await driverLoginTrackingRepository.UpdateAsync(driver.Id, latitude, longitude);
                    driverWorkingHourTrackingRepository
                        .Add(new DriverWorkingHourTracking(driver.Id, AgentStatusesEnum.Offline, string.Empty));

                    await unityOfWork.SaveChangesAsync();
                    await LoggedoutNotificationAsync(latitude, longitude, driver);
                });

                return true;
            }

            return false;
        }
        public async Task<KeyValuePair<bool, string>> LoginRequestAsync(LoginRequestInputDTO loginRequest)
        {
            var user = await userDomainService.GetAsync(u => u.Email.ToLower().Trim() == loginRequest.Email.ToLower().Trim()
              || u.UserName.ToLower().Trim() == loginRequest.Email.ToLower().Trim());
            if (user == null)
            {
                return new KeyValuePair<bool, string>(false, localizer[Keys.Validation.DriverUserNameNotFound]);
            }

            var driver = await driverRepository.GetFirstOrDefaultAsync(d => d.UserId == user.Id);
            if (driver == null || driver.AgentStatusId == (int)AgentStatusesEnum.Blocked)
            {
                return new KeyValuePair<bool, string>(false, localizer[Keys.Validation.DriverAccountBlocked]);
            }

            var userDevices = await userDeviceRepository.GetAllAsync(userdevice => userdevice.UserId == user.Id && !userdevice.IsDeleted);
            if (userDevices != null && userDevices.Any(userdevice => userdevice.IsLoggedIn == true))
            {
                return new KeyValuePair<bool, string>(false, localizer[Keys.Validation.DriverAlreadyLoggedIn]);
            }
            var penddingLoginRequest = await driverLoginRequestRepository.GetFirstOrDefaultAsync(driverloginrequest =>
                 driverloginrequest.DriverId == driver.Id && driverloginrequest.Status == (int)DriverLoginRequestStatus.Pending);
            if (penddingLoginRequest != null)
            {
                int expireHour = 24;
                var expireHourSetting = await settingRepository.GetFirstOrDefaultAsync(setting => setting.SettingKey == SettingKeyConstant.DriverLoginRequestExpiration);
                if (expireHourSetting != null)
                {
                    int.TryParse(expireHourSetting.Value, out expireHour);
                }
                if (penddingLoginRequest.CreatedDate.AddHours(expireHour) > DateTime.UtcNow)
                {
                    return new KeyValuePair<bool, string>(false, localizer[Keys.Validation.DriverLoginRequestIsPending]);
                }
            }

            await unityOfWork.RunTransaction(async () =>
            {
                if (penddingLoginRequest != null)
                {
                    penddingLoginRequest.SetIsDeleted(true);
                    driverLoginRequestRepository.Update(penddingLoginRequest);
                    userDevices?.ToList().ForEach(userDevice =>
                    {
                        userDevice.SetIsDeleted(true);
                        userDeviceRepository.Update(userDevice);
                    });
                }

                await userDeviceRepository.AddOrUpdateAsync(new UserDevice
                {
                    FCMDeviceId = loginRequest.FCMDeviceId,
                    UserId = user.Id,
                    Version = loginRequest.Version,
                    DeviceType = loginRequest.DeviceType,
                    IsLoggedIn = false
                });
                driverLoginRequestRepository.Add(new DriverLoginRequest(driver.Tenant_Id)
                {
                    DriverId = driver.Id,
                    Status = (int)DriverLoginRequestStatus.Pending,
                    Token = string.Empty
                });

                await unityOfWork.SaveChangesAsync();
            });

            notificationService.SendToUserManagers(
               user.Id,
               "LoginRequest",
               localizer[Keys.Notifications.DriverSentLoginRequest],
               localizer.Format(Keys.Notifications.DriverNameSentLoginRequest, user.FullName),
               user.UserName);

            return new KeyValuePair<bool, string>(true, string.Empty);
        }
        public async Task<KeyValuePair<bool, string>> CancelLoginRequestAsync(LoginRequestInputDTO loginRequest)
        {
            var user = await userDomainService.GetAsync(u => u.Email.ToLower().Trim() == loginRequest.Email.ToLower().Trim()
             || u.UserName.ToLower().Trim() == loginRequest.Email.ToLower().Trim());
            if (user == null)
            {
                return new KeyValuePair<bool, string>(false, localizer[Keys.Validation.DriverUserNameNotFound]);
            }

            var driver = await driverRepository.GetFirstOrDefaultAsync(d => d.UserId == user.Id);
            if (driver == null || driver.AgentStatusId == (int)AgentStatusesEnum.Blocked)
            {
                return new KeyValuePair<bool, string>(false, localizer[Keys.Validation.DriverAccountBlocked]);
            }

            await unityOfWork.RunTransaction(async () =>
            {
                var userDevices = await userDeviceRepository.GetAllAsync(userdevice => userdevice.UserId == user.Id && !userdevice.IsDeleted);
                userDevices?.ToList().ForEach(userdevice =>
                {
                    userdevice.SetIsDeleted(true);
                    userDeviceRepository.Update(userdevice);
                });

                var penddingLoginRequests = await driverLoginRequestRepository.GetAllAsync(driverloginrequest =>
                     driverloginrequest.DriverId == driver.Id && driverloginrequest.Status == (int)DriverLoginRequestStatus.Pending);
                foreach (var penddingLoginRequest in penddingLoginRequests)
                {
                    penddingLoginRequest.Status = (int)DriverLoginRequestStatus.Canceled;
                    driverLoginRequestRepository.Update(penddingLoginRequest);
                }

                await unityOfWork.SaveChangesAsync();
            });

            notificationService.SendToUserManagers(
               user.Id,
               "CancelLoginRequest",
               localizer[Keys.Notifications.DriverLoginRequestCanceled],
               localizer.Format(Keys.Notifications.DriverNameLoginRequestCanceled, user.FullName),
               user.UserName);

            return new KeyValuePair<bool, string>(true, string.Empty);
        }

        public async Task<bool> ApproveOrRejectLoginRequestAsync(int loginRequestId, DriverLoginRequestStatus status, string reason)
        {
            var driverLoginRequest = await driverLoginRequestRepository
                .GetAllIQueryable(loginRequest => loginRequest.Id == loginRequestId)
                .Include(loginRequest => loginRequest.Driver)
                .FirstOrDefaultAsync();
            if (driverLoginRequest == null)
            {
                return false;
            }

            await unityOfWork.RunTransaction(async () =>
            {
                driverLoginRequest.Status = (byte)status;
                driverLoginRequest.ApprovedOrRejectBy = currentUser.UserName;
                driverLoginRequest.Reason = reason;
                driverLoginRequestRepository.Update(driverLoginRequest);
                await unityOfWork.SaveChangesAsync();

                if (status == DriverLoginRequestStatus.Approved)
                {
                    notificationService.SendToUser(
                       driverLoginRequest.Driver.UserId,
                       "ApprovedLoginRequst",
                       CurrentNotificationSenderName,
                       localizer[Keys.Notifications.AdminApprovedLoginRequest],
                       new { isAdminApproved = true });
                }
                else if (status == DriverLoginRequestStatus.Rejected)
                {
                    notificationService.SendToUser(
                       driverLoginRequest.Driver.UserId,
                       "RejectedLoginRequst",
                       CurrentNotificationSenderName,
                       localizer[Keys.Notifications.AdminRejectedLoginRequest],
                       new { isAdminApproved = false });
                    await userDeviceRepository.SoftDeleteAsync(userDevice => userDevice.UserId == driverLoginRequest.Driver.UserId);
                    await unityOfWork.SaveChangesAsync();
                }
            });

            #region
            //if (status == DriverLoginRequestStatus.Approved && driverLoginRequest.Status == (byte)DriverLoginRequestStatus.Approved)
            //{
            //    int expireHour = 24;
            //    var expireHourSetting = await _settingManager.GetSettingByKeyAsync("DriverLoginRequestExpiration");
            //    if (expireHourSetting != null)
            //    {
            //        int.TryParse(expireHourSetting.Value, out expireHour);
            //    }
            //    if (driverLoginRequest.UpdatedDate.AddHours(expireHour).Date > DateTime.UtcNow)
            //    {
            //        return Ok(false);
            //    }
            //}
            #endregion

            return true;
        }


        private async Task<GeoFence> GetDriverCurrentGeoFenceAsync(bool isAllGeofences,
            List<int> driverGeoFences,
            DriverCurrentLocation driverCurrentLocation)
        {
            var driverCurrentPoint = new MapPoint
            {
                Latitude = driverCurrentLocation.Latitude.Value,
                Longitude = driverCurrentLocation.Longitude.Value
            };
            var geofences = await geofenceRepository.GetWithGeofencLocationAsync(geofence => isAllGeofences
                || (driverGeoFences != null && driverGeoFences.Contains(geofence.Id)));

            foreach (var geoFence in geofences)
            {
                var zone = geoFence.GeoFenceLocations
                    .Select(geo => new MapPoint { Latitude = geo.Latitude.Value, Longitude = geo.Longitude.Value })
                    .Select(geo => new MapPoint { Latitude = geo.Latitude, Longitude = geo.Longitude })
                    .ToArray();
                if (MapsHelper.IsPointInPolygon(zone, driverCurrentPoint))
                {
                    return geoFence;
                }
            }

            return null;
        }
        private async Task<Branch> GetRelatedBranchAsync(List<Branch> branches, MapPoint point)
        {
            double reachedLimitDistanceInM = new DefaultSettingValue().ReachedDistanceRestrictionInMeter;
            var reachedLimitDistanceInMSetting = await settingRepository
                .GetFirstOrDefaultAsync(setting => setting.SettingKey == SettingsKeys.ReachedDistanceRestriction);
            if (reachedLimitDistanceInMSetting != null && !string.IsNullOrEmpty(reachedLimitDistanceInMSetting.Value))
            {
                double.TryParse(reachedLimitDistanceInMSetting.Value, out reachedLimitDistanceInM);
            }

            List<(Branch, double)> branchesDistances = new List<(Branch, double)>();
            foreach (var branch in branches)
            {
                var distance = MapsHelper.GetDistanceBetweenPoints(branch.Latitude.Value,
                    branch.Longitude.Value,
                    point.Latitude,
                    point.Longitude);
                branchesDistances.Add((branch, distance));
            }
            var nearestBranch = branchesDistances
                .OrderBy(branchDistance => branchDistance.Item2)
                .FirstOrDefault();
            if (nearestBranch.Item2 <= reachedLimitDistanceInM)
            {
                return nearestBranch.Item1;
            }

            return null;
        }

        private async Task LoginNotificationAsync(Driver driver)
        {
            notificationService.SendToUserManagers(driver.UserId,
               "DriverLoggedin",
               localizer[Keys.Notifications.DriverHasLoggedIn],
               localizer.Format(Keys.Notifications.DriverNameHasLoggeIn, driver.User.FullName),
               new DriverViewModel
               {
                   Id = driver.Id,
                   UserId = driver.UserId,
                   FirstName = driver.User.FirstName,
                   LastName = driver.User.LastName,
                   AgentTypeId = driver.AgentTypeId,
                   AgentStatusUpdateDate = DateTime.UtcNow,
                   AgentStatusId = driver.AgentStatusId,
                   AgentStatusName = ((AgentStatusesEnum)driver.AgentStatusId).ToString(),
                   Latitude = driver.CurrentLocation.Latitude,
                   Longitude = driver.CurrentLocation.Longitude,
                   BranchId = driver.CurrentLocation.BranchId,
                   PhoneNumber = driver.User.PhoneNumber,
                   ImageUrl = string.IsNullOrEmpty(driver.ImageUrl) ? string.Empty : driver.ImageUrl.Replace("DriverImages", string.Empty)
               });
        }
        private async Task LoggedoutNotificationAsync(double latitude, double longitude, Driver driver)
        {
            notificationService.SendToUserManagers(driver.UserId,
               "DriverLoggedOut",
               localizer[Keys.Notifications.DriverHasBeenLoggedOut],
               localizer.Format(Keys.Notifications.DriverNameHasBeenLoggedOut, driver.User.FullName),
               new DriverViewModel
               {
                   Id = driver.Id,
                   UserId = driver.UserId,
                   FirstName = driver.User.FirstName,
                   LastName = driver.User.LastName,
                   AgentStatusId = driver.AgentStatusId,
                   AgentStatusName = AgentStatusesEnum.Offline.ToString(),
                   Latitude = latitude,
                   Longitude = longitude,
                   PhoneNumber = driver.User.PhoneNumber,
                   ImageUrl = string.IsNullOrEmpty(driver.ImageUrl) ? string.Empty : driver.ImageUrl.Replace("DriverImages", string.Empty)
               });
        }
    }
}
