﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Drivers.Queries.DTOs;
using Amanah.Posthub.BLL.Drivers.Services.DTOs;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Managers.TaskAssignment;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Tasks.WebServices;
using Amanah.Posthub.DeliverService.BLL.Drivers.Queries.DTOs.Express;
using Amanah.Posthub.DeliverService.BLL.Drivers.Services.DTOs.Express;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.GeoFences.Repositories;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities;
using Amanah.Posthub.Service.Domain.Enums;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;
using Task = System.Threading.Tasks.Task;

namespace Amanah.Posthub.BLL.Drivers.Services
{
    public interface IDriverService
    {
        Task<ProcessResult<DriverResultDTO>> CreateAsync(CreateDriverInputDTO driverInputDTO);
        Task<ProcessResult<DriverResultDTO>> EditAsync(UpdateDriverInputDTO driverInputDTO);
        Task<ProcessResult<DriverResultForExpressDTO>> CreateExpressAsync(CreateDriverForExpressInputDTO driverInputDTO);
        Task<ProcessResult<DriverResultForExpressDTO>> EditExpressAsync(UpdateDriverForExpressInputDTO driverInputDTO);
        Task EditAsync(UpdateDriversAgentTypeInputDTO changeDriverTypeViewModel);
        Task DeleteAsync(List<DeleteDriverInputDTO> deleteDriverInputDTOs);
        Task DeleteAsync(DeleteDriverInputDTO deleteDriverInputDTO);
        Task DeleteAsync(int driverId);
        Task BlockAsync(BlockOrUnBlockDriversDTO driversDTO);
        Task UnBlockAsync(BlockOrUnBlockDriversDTO driversDTO);
        Task SetDutyAsync(SetDriverDutyInputDTO driverDutyInputDTO);
        Task<KeyValuePair<bool, string>> CheckInBranchAsync(double Latitude, double Longitude);
        Task ForceLogoutDriverAsync(int driverId);
        Task<DriverLoginResponseDTO> LoginAsync(DriverLoginInputDTO driverLogin);
        Task<bool> LogoutAsync(string userId, double latitude, double longitude);
        Task<KeyValuePair<bool, string>> LoginRequestAsync(LoginRequestInputDTO loginRequest);
        Task<KeyValuePair<bool, string>> CancelLoginRequestAsync(LoginRequestInputDTO loginRequest);
        Task<bool> ApproveOrRejectLoginRequestAsync(int loginRequestId, DriverLoginRequestStatus status, string reason);
        Task<bool> ChangePasswordAsync(string userId, string newPassword);

        Task<bool> ChangeUserPasswordAsync(string userId, string newPassword);

    }
    public partial class DriverService : IDriverService
    {
        private readonly IDriverRepository driverRepository;
        private readonly IUserDomainService userDomainService;
        private readonly IUploadFormFileService uploadFormFileService;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IRepository<DriverWorkingHourTracking> driverWorkingHourTrackingRepository;
        private readonly IUserDeviceRepository userDeviceRepository;
        private readonly IDriverLoginTrackingRepository driverLoginTrackingRepository;
        private readonly IGeofenceRepository geofenceRepository;
        private readonly IRepository<Branch> branchRepository;
        private readonly IRepository<Setting> settingRepository;
        private readonly IRepository<DriverLoginRequest> driverLoginRequestRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unityOfWork;
        private readonly ICurrentUser currentUser;
        private readonly ITaskAutoAssignmentFactory taskAutoAssignmentFactory;

        private readonly ITaskCommonService taskCommonService;
        private readonly IBackGroundNotificationSercvice notificationService;
        private readonly ILocalizer localizer;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<DriverService> _logger;
        private readonly IConfiguration _configuration;
        private readonly IFirebasePushNotificationService pushNotificationService;

        private string CurrentNotificationSenderName { get; set; }

        public DriverService(
            IDriverRepository driverRepository,
            IUserDomainService userDomainService,
            IUploadFormFileService uploadFormFileService,
            IRepository<AccountLogs> accountLogRepository,
            IRepository<DriverWorkingHourTracking> driverWorkingHourTrackingRepository,
            IUserDeviceRepository userDeviceRepository,
            IDriverLoginTrackingRepository driverLoginTrackingRepository,
            IGeofenceRepository geofenceRepository,
            IRepository<Branch> branchRepository,
            IRepository<Setting> settingRepository,
            IRepository<DriverLoginRequest> driverLoginRequestRepository,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            ICurrentUser currentUser,
            ITaskAutoAssignmentFactory taskAutoAssignmentFactory,
            ITaskCommonService taskCommonService,
            IBackGroundNotificationSercvice notificationService,
            ILocalizer localizer,
            IHttpClientFactory httpClientFactory,
            ILogger<DriverService> logger,
            IConfiguration configuration,
            IFirebasePushNotificationService pushNotificationService)
        {
            this.driverRepository = driverRepository;
            this.userDomainService = userDomainService;
            this.uploadFormFileService = uploadFormFileService;
            this.accountLogRepository = accountLogRepository;
            this.driverWorkingHourTrackingRepository = driverWorkingHourTrackingRepository;
            this.userDeviceRepository = userDeviceRepository;
            this.driverLoginTrackingRepository = driverLoginTrackingRepository;
            this.geofenceRepository = geofenceRepository;
            this.branchRepository = branchRepository;
            this.settingRepository = settingRepository;
            this.driverLoginRequestRepository = driverLoginRequestRepository;
            this.mapper = mapper;
            this.unityOfWork = unityOfWork;
            this.currentUser = currentUser;
            this.taskAutoAssignmentFactory = taskAutoAssignmentFactory;
            this.taskCommonService = taskCommonService;
            this.notificationService = notificationService;
            this.localizer = localizer;
            CurrentNotificationSenderName = "Post-Express";
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _configuration = configuration;
            this.pushNotificationService = pushNotificationService;
        }

        public async Task<ProcessResult<DriverResultDTO>> CreateAsync(CreateDriverInputDTO driverInputDTO)
        {
            ProcessResult<DriverResultDTO> createDriverResult = new ProcessResult<DriverResultDTO>();

            var driver = mapper.Map<Driver>(driverInputDTO);
            var user = mapper.Map<ApplicationUser>(driverInputDTO);

            await unityOfWork.RunTransaction(async () =>
            {
                user = await userDomainService.CreateAsync(user, driverInputDTO.Password, driverInputDTO.RoleName);
                if (!string.IsNullOrEmpty(user.Id))
                {
                    driver.UserId = user.Id;
                    driver.ImageUrl = await SetDriverPhotoAsync(driverInputDTO.FormFile);
                    driver.CurrentLocation = new DriverCurrentLocation(driver.Tenant_Id);
                    if (driverInputDTO.DriverPickUpGeoFences != null)
                        driver.DriverPickUpGeoFences = driverInputDTO.DriverPickUpGeoFences.Select(a => new DriverPickUpGeoFence { GeoFenceId = a.GeoFenceId }).ToList();
                    if (driverInputDTO.DriverDeliveryGeoFences != null)
                        driver.DriverDeliveryGeoFences = driverInputDTO.DriverDeliveryGeoFences.Select(a => new DriverDeliveryGeoFence { GeoFenceId = a.GeoFenceId }).ToList();
                    if (driverInputDTO.DriverExternalBranchs != null)
                        driver.DriverExternalBranchs = driverInputDTO.DriverExternalBranchs.Select(a => new DriverExternalBranch { BranchId = a.BranchId }).ToList();

                    driverRepository.Add(driver);

                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Driver",
                        ActivityType = "Create",
                        Description = $"{user.FullName } ( {driver.Id} ) has been created ",
                        Record_Id = driver.Id
                    });  //todo: refactor needed to be event after create driver
                    await unityOfWork.SaveChangesAsync();

                    driver.User = user;
                    createDriverResult.IsSucceeded = true;
                    if (driver.AccountStatusId == (int)AccountStatuses.Inactive)
                    {
                        await pushNotificationService.SendAsync(driver.UserId, "Post-Hub", "Account Status Is Set To InActive.", new { NotificationType = NotificationTypeEnum.AccountStatus });
                    }
                    //createDriverResult.ReturnData = mapper.Map<DriverResultDTO>(driver);
                }
            });
            return createDriverResult;
        }

        public async Task<ProcessResult<DriverResultForExpressDTO>> CreateExpressAsync(CreateDriverForExpressInputDTO driverInputDTO)
        {
            ProcessResult<DriverResultForExpressDTO> createDriverResult = new ProcessResult<DriverResultForExpressDTO>();

            var driver = mapper.Map<Driver>(driverInputDTO);
            var user = mapper.Map<ApplicationUser>(driverInputDTO);

            await unityOfWork.RunTransaction(async () =>
            {
                user = await userDomainService.CreateAsync(user, driverInputDTO.Password, driverInputDTO.RoleName);
                if (!string.IsNullOrEmpty(user.Id))
                {
                    driver.UserId = user.Id;
                    driver.ImageUrl = await SetDriverPhotoAsync(driverInputDTO.FormFile);
                    driver.CurrentLocation = new DriverCurrentLocation(driver.Tenant_Id);
                    driverRepository.Add(driver);

                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Driver",
                        ActivityType = "Create",
                        Description = $"{user.FullName } ( {driver.Id} ) has been created ",
                        Record_Id = driver.Id
                    });  //todo: refactor needed to be event after create driver
                    await unityOfWork.SaveChangesAsync();

                    driver.User = user;
                    createDriverResult.IsSucceeded = true;
                    if (driver.AccountStatusId == (int)AccountStatuses.Inactive)
                    {
                        await pushNotificationService.SendAsync(driver.UserId, "Post-Hub", "Account Status Is Set To InActive.", new { NotificationType = NotificationTypeEnum.AccountStatus });
                    }
                    //createDriverResult.ReturnData = mapper.Map<DriverResultDTO>(driver);
                }
            });
            return createDriverResult;
        }

        public async Task<ProcessResult<DriverResultDTO>> EditAsync(UpdateDriverInputDTO driverInputDTO)
        {
            ProcessResult<DriverResultDTO> editDriverResult = new ProcessResult<DriverResultDTO>();

            var existingUser = await userDomainService.GetAsync(driverInputDTO.UserId);
            existingUser = mapper.Map(driverInputDTO, existingUser);
            var existingDriver = await driverRepository.GetWithGeofencesAsync(driverInputDTO.Id);
            existingDriver = mapper.Map(driverInputDTO, existingDriver);

            await unityOfWork.RunTransaction(async () =>
            {
                await userDomainService.EditAsync(existingUser, driverInputDTO.RoleName);
                if (driverInputDTO.FormFile != null)
                {
                    existingDriver.ImageUrl = await SetDriverPhotoAsync(driverInputDTO.FormFile);
                }
                var driverPickupGeofences = mapper.Map<List<DriverPickUpGeoFence>>(driverInputDTO.DriverPickUpGeoFences);
                var driverDeliveryGeofences = mapper.Map<List<DriverDeliveryGeoFence>>(driverInputDTO.DriverDeliveryGeoFences);
                var driverFloors = mapper.Map<List<DriverFloor>>(driverInputDTO.DriverFloors);
                var driverBranchs = mapper.Map<List<DriverExternalBranch>>(driverInputDTO.DriverExternalBranchs);
                driverRepository.Update(existingDriver, driverPickupGeofences, driverDeliveryGeofences, driverFloors, driverBranchs);

                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Update",
                    Description = $"{existingUser.FullName } ( {existingDriver.Id} ) has been updated ",
                    Record_Id = existingDriver.Id
                });
                await unityOfWork.SaveChangesAsync();

                editDriverResult.IsSucceeded = true;
                if (driverInputDTO.AccountStatusId == (int)AccountStatuses.Inactive)
                {
                    await pushNotificationService.SendAsync(existingDriver.UserId, "Post-Hub", "Account Status Is Set To InActive.", new { NotificationType = NotificationTypeEnum.AccountStatus });
                }
                //editDriverResult.ReturnData = mapper.Map<DriverResultDTO>(existingDriver);
            });

            return editDriverResult;
        }

        public async Task<ProcessResult<DriverResultForExpressDTO>> EditExpressAsync(UpdateDriverForExpressInputDTO driverInputDTO)
        {
            ProcessResult<DriverResultForExpressDTO> editDriverResult = new ProcessResult<DriverResultForExpressDTO>();

            var existingUser = await userDomainService.GetAsync(driverInputDTO.UserId);
            existingUser = mapper.Map(driverInputDTO, existingUser);
            var existingDriver = await driverRepository.GetWithGeofencesExpressAsync(driverInputDTO.Id);
            existingDriver = mapper.Map(driverInputDTO, existingDriver);

            await unityOfWork.RunTransaction(async () =>
            {
                await userDomainService.EditAsync(existingUser, driverInputDTO.RoleName);
                if (driverInputDTO.FormFile != null)
                {
                    existingDriver.ImageUrl = await SetDriverPhotoAsync(driverInputDTO.FormFile);
                }
                var driverPickupGeofences = mapper.Map<List<DriverPickUpGeoFence>>(driverInputDTO.DriverPickUpGeoFences);
                var driverDeliveryGeofences = mapper.Map<List<DriverDeliveryGeoFence>>(driverInputDTO.DriverDeliveryGeoFences);
                driverRepository.Update(existingDriver, driverPickupGeofences, driverDeliveryGeofences);

                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Update",
                    Description = $"{existingUser.FullName } ( {existingDriver.Id} ) has been updated ",
                    Record_Id = existingDriver.Id
                });
                await unityOfWork.SaveChangesAsync();

                editDriverResult.IsSucceeded = true;
                if (driverInputDTO.AccountStatusId == (int)AccountStatuses.Inactive)
                {
                    await pushNotificationService.SendAsync(existingDriver.UserId, "Post-Hub", "Account Status Is Set To InActive.", new { NotificationType = NotificationTypeEnum.AccountStatus });
                }
                //editDriverResult.ReturnData = mapper.Map<DriverResultDTO>(existingDriver);
            });

            return editDriverResult;
        }

        public async Task EditAsync(UpdateDriversAgentTypeInputDTO changeDriverTypeViewModel)
        {
            var drivers = await driverRepository.GetAllAsync(driver => changeDriverTypeViewModel.DriverIds.Contains(driver.Id));
            await unityOfWork.RunTransaction(async () =>
            {
                if (drivers != null)
                {
                    foreach (var driver in drivers)
                    {
                        driver.AgentTypeId = changeDriverTypeViewModel.AgentTypeId;
                        driverRepository.Update(driver);

                        accountLogRepository.Add(new AccountLogs()
                        {
                            TableName = "Driver",
                            ActivityType = "Change Driver Type",
                            Description = $"Driver with Id ( {driver.Id} ) has been changed ",
                            Record_Id = driver.Id
                        });  //todo: refactor needed to be event after create driver
                    }

                    await unityOfWork.SaveChangesAsync();
                }
            });
        }
        public async Task DeleteAsync(List<DeleteDriverInputDTO> deleteDriverInputDTOs)
        {
            var driverIds = deleteDriverInputDTOs
                .Select(driver => driver.Id)
                .ToList();
            await unityOfWork.RunTransaction(async () =>
            {
                if (driverIds != null)
                {
                    await driverRepository.Remove(driverIds);
                    foreach (DeleteDriverInputDTO deleteDriverInputDTO in deleteDriverInputDTOs)
                    {
                        accountLogRepository.Add(new AccountLogs()
                        {
                            TableName = "Driver",
                            ActivityType = "Delete",
                            Description = $"{deleteDriverInputDTO.FirstName } ( {deleteDriverInputDTO.Id} ) has been deleted ",
                            Record_Id = deleteDriverInputDTO.Id
                        });  //todo: refactor needed to be event after create driver
                    }
                    await unityOfWork.SaveChangesAsync();
                }
            });
        }
        public async Task DeleteAsync(DeleteDriverInputDTO deleteDriverInputDTO)
        {
            await unityOfWork.RunTransaction(async () =>
            {
                await driverRepository.Remove(deleteDriverInputDTO.Id);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Delete",
                    Description = $"{deleteDriverInputDTO.FirstName } ( {deleteDriverInputDTO.Id} ) has been deleted ",
                    Record_Id = deleteDriverInputDTO.Id
                });  //todo: refactor needed to be event after create driver
                await unityOfWork.SaveChangesAsync();
            });
        }
        public async Task DeleteAsync(int driverId)
        {
            var DriverUser = await driverRepository.GetAsync(driverId);
            var user = await userDomainService.GetAsync(DriverUser.UserId);
            user.SetIsDeleted(true);
            user.UserName = user.UserName + "Deleted";
            user.NormalizedUserName = user.NormalizedUserName + "Deleted";
            user.Email = user.Email + "Deleted";
            user.NormalizedEmail = user.NormalizedEmail + "Deleted";
            await unityOfWork.RunTransaction(async () =>
            {
                await driverRepository.Remove(driverId);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "Delete",
                    Description = $"Driver with Id ( {driverId} ) has been deleted ",
                    Record_Id = driverId
                });  //todo: refactor needed to be event after create driver
                await unityOfWork.SaveChangesAsync();
            });
        }
        public async Task BlockAsync(BlockOrUnBlockDriversDTO driversDTO)
        {
            var drivers = await driverRepository.GetAllAsync(driver => driversDTO.DriverIds.Contains(driver.Id));
            await unityOfWork.RunTransaction(async () =>
            {
                foreach (var driver in drivers)
                {
                    driver.AgentStatusId = (int)AgentStatusesEnum.Blocked;
                    driver.Reason = driversDTO.Reason;
                    driverRepository.Update(driver);

                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Driver",
                        ActivityType = "Block",
                        Description = $"Driver with Id ( {driver.Id} ) has been blocked ",
                        Record_Id = driver.Id
                    });  //todo: refactor needed to be event after create driver
                }
                var devices = await userDeviceRepository.SoftDeleteAsync(userDevice => drivers.Select(d => d.UserId).Contains(userDevice.UserId));
                await driverLoginTrackingRepository.UpdateAsync(driversDTO.DriverIds);
                int changes = await unityOfWork.SaveChangesAsync();
                NotifyDriverAboutBlock(devices); //need to add in back

            });
        }
        public async Task UnBlockAsync(BlockOrUnBlockDriversDTO driversDTO)
        {
            var drivers = await driverRepository.GetAllAsync(driver => driversDTO.DriverIds.Contains(driver.Id));
            await unityOfWork.RunTransaction(async () =>
            {
                foreach (var driver in drivers)
                {
                    driver.AgentStatusId = (int)AgentStatusesEnum.Offline;
                    driver.Reason = driversDTO.Reason;
                    driverRepository.Update(driver);

                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Driver",
                        ActivityType = "Unblock",
                        Description = $"Driver with Id ( {driver.Id} ) has been unblocked ",
                        Record_Id = driver.Id
                    });  //todo: refactor needed to be event after create driver
                }
                await unityOfWork.SaveChangesAsync();
            });
        }
        public async Task SetDutyAsync(SetDriverDutyInputDTO driverDutyInputDTO)
        {
            var driver = await driverRepository.GetByUserIdAsync(driverDutyInputDTO.DriverId);
            await unityOfWork.RunTransaction(async () =>
            {
                driver.AgentStatusId = driverDutyInputDTO.IsAvailable
                ? (int)AgentStatusesEnum.Available
                : (int)AgentStatusesEnum.Unavailable;
                if (driver.AgentStatusId != (int)AgentStatusesEnum.Available)
                {
                    driver.ReachedTime = null;
                    driver.ClubbingTimeExpiration = null;
                    driver.IsInClubbingTime = false;
                    driver.MaxOrdersWeightsCapacity = 1;
                    driver.MinOrdersNoWait = 1;
                }
                driverRepository.Update(driver);

                string note = $"AdminSetDuty{(driverDutyInputDTO.IsAvailable ? "On" : "Off")}";
                driverWorkingHourTrackingRepository.Add(
                    new DriverWorkingHourTracking(driver.Id, (AgentStatusesEnum)driver.AgentStatusId.Value, note));

                await unityOfWork.SaveChangesAsync();
            });

            NotifyDriverAboutChangingDuty(driverDutyInputDTO);
        }
        public async Task ForceLogoutDriverAsync(int driverId)
        {
            var driver = await driverRepository.GetAsync(driverId);
            await unityOfWork.RunTransaction(async () =>
            {
                driver.AgentStatusId = (int)AgentStatusesEnum.Offline;
                driver.ReachedTime = null;
                driver.ClubbingTimeExpiration = null;
                driver.IsInClubbingTime = false;
                driver.MaxOrdersWeightsCapacity = 1;
                driver.MinOrdersNoWait = 1;
                driverRepository.Update(driver);

                NotifyDriverAboutLogout(driver);  //Keep this order to avoid deleting of userDevice

                await userDeviceRepository.SoftDeleteAsync(userDevice => driver.UserId == userDevice.UserId);
                await driverLoginTrackingRepository.UpdateAsync(new List<int> { driverId });

                driverWorkingHourTrackingRepository.Add(
                    new DriverWorkingHourTracking(driverId, AgentStatusesEnum.Offline, "LoggedOutByAdmin"));

                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Driver",
                    ActivityType = "ForceLogout",
                    Description = $"Driver with Id ( {driver.Id} ) has been forced to logout",
                    Record_Id = driver.Id
                });  //todo: refactor needed to be event after create driver

                await unityOfWork.SaveChangesAsync();
            });
        }

        private async Task<string> SetDriverPhotoAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "DriverImages";
                var processResult = await uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }
        private void NotifyDriverAboutBlock(List<UserDevice> userDevices)
        {
            foreach (var userDevice in userDevices)
            {
                notificationService.SendToUser(
                   userDevice.UserId,
                   "DriverBlocked",
                   CurrentNotificationSenderName,
                   localizer[Keys.Notifications.YourAccountBlocked],
                   new
                   {
                       notificationType = NotificationTypeEnum.Blocked,
                       IsLoggedOut = true
                   });
            }
        }
        private void NotifyDriverAboutChangingDuty(SetDriverDutyInputDTO driverDutyInputDTO)
        {
            var message = driverDutyInputDTO.IsAvailable
                            ? localizer[Keys.Notifications.AdminPutYouOnDuty]
                            : localizer[Keys.Notifications.AdminPutYouOffDuty];

            notificationService.SendToUser(
                driverDutyInputDTO.DriverId,
               "AdminPutYouOffDuty",
               CurrentNotificationSenderName,
               message,
               new
               {
                   notificationType = driverDutyInputDTO.IsAvailable
                       ? NotificationTypeEnum.UpdateDriverDutyToOn
                       : NotificationTypeEnum.UpdateDriverDutyToOff
               });
        }
        private void NotifyDriverAboutLogout(Driver driver)
        {
            notificationService.SendToUser(
               driver.UserId,
               "AdminEndedYourSession",
               CurrentNotificationSenderName,
               localizer[Keys.Notifications.AdminEndedYourSession],
               new { IsLoggedOut = true });
        }
        public async Task<bool> ChangeUserPasswordAsync(string userId, string newPassword)
        {
            return await userDomainService
                   .ChangeUserPassword(userId, newPassword);
        }

        public async Task<bool> ChangePasswordAsync(string userId, string newPassword)
        {
            // var user = await userDomainService.GetAsync(userId);

            return await userDomainService
                   .ChangeCurrentPasswordAsync(userId, newPassword);


        }

    }
}
