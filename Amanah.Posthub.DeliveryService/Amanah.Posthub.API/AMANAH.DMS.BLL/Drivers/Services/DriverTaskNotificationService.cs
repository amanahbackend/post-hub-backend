﻿using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
namespace Amanah.Posthub.BLL.Drivers.Queries
{
    public interface IDriverTaskNotificationService
    {
        Task<bool> MarkAsReadAsync(int driverId);
        Task<bool> ClearAsync(int driverId);
    }

    public class DriverTaskNotificationService : IDriverTaskNotificationService
    {


        private readonly IRepository<DriverTaskNotification> _driverTaskNotificationRepositry;
        private readonly IUnitOfWork unityOfWork;

        public DriverTaskNotificationService(
           IRepository<DriverTaskNotification> driverTaskNotificationRepositry,
            IUnitOfWork unityOfWork
            )
        {
            _driverTaskNotificationRepositry = driverTaskNotificationRepositry;
            this.unityOfWork = unityOfWork;
        }



        public async Task<bool> MarkAsReadAsync(int driverId)
        {
            var notificationsToUpdate = await _driverTaskNotificationRepositry.GetAllIQueryable()
                .IgnoreQueryFilters()
                .Where(x => x.IsRead != true && x.DriverId == driverId).ToListAsync();

            if (notificationsToUpdate.Count == 0)
                return true;

            var isSuccess = false;
            await unityOfWork.RunTransaction(async () =>
            {
                foreach (var notification in notificationsToUpdate)
                {
                    notification.IsRead = true;
                    _driverTaskNotificationRepositry.Update(notification);
                }
                var changes = await unityOfWork.SaveChangesAsync();
                isSuccess = changes > 0;

            });
            return isSuccess;
        }


        public async Task<bool> ClearAsync(int driverId)
        {
            var notificationsToClear = _driverTaskNotificationRepositry.GetAllIQueryable()
                .IgnoreQueryFilters()
               .Where(x => x.DriverId == driverId);
            if (notificationsToClear.Count() == 0)
                return true;
            var isSuccess = false;
            await unityOfWork.RunTransaction(async () =>
            {
                _driverTaskNotificationRepositry.DeleteRange(notificationsToClear);
                var changes = await unityOfWork.SaveChangesAsync();
                isSuccess = changes > 0;
            });
            return isSuccess;
        }
    }
}
