﻿namespace Amanah.Posthub.ViewModels
{
    public class AgentTypeViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
