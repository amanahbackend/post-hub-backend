﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class BlockDriversViewModel
    {
        public List<int> DriverIds { get; set; }
        public string Reason { get; set; }
    }
}
