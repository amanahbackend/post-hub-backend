﻿namespace Amanah.Posthub.ViewModels
{
    public class ChangeDriverTransportTypeViewModel
    {
        public string DriverId { set; get; }
        public string TransportDescription { get; set; }

        public string LicensePlate { get; set; }

        public int? TransportTypeId { set; get; }

        public string Color { get; set; }
    }
}
