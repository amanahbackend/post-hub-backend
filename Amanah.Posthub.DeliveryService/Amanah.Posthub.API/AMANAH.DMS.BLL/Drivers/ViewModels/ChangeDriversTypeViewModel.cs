﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class ChangeDriversTypeViewModel
    {
        public List<int> DriverIds { get; set; }
        public int AgentTypeId { set; get; }
    }
}
