﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Amanah.Posthub.ViewModels
{
    public class CreateDriverViewModel : CreateDriverWithoutPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

    }

    public class CreateDriverViewModelValidator : AbstractValidator<CreateDriverViewModel>
    {
        public CreateDriverViewModelValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.PhoneNumber).NotEmpty().NotNull();
            RuleFor(x => x.TeamId).NotEmpty().NotNull();
            RuleFor(x => x.UserName).NotEmpty().NotNull();
            //RuleFor(x => x.TransportTypeId).NotEmpty().NotNull();
        }
    }

    public class ImportDriverViewModel : CreateDriverViewModel
    {
        public string Error { get; set; }
    }


    public class CreateDriverWithoutPasswordViewModel
    {
        public IFormFile FormFile { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public string RoleName { get; set; }
        public int TeamId { set; get; }
        public int? AgentTypeId { set; get; }
        public int? AgentStatusId { set; get; }
        public int? TransportTypeId { set; get; }
        public int? CountryId { set; get; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }

        public List<DriverPickUpGeoFenceViewModel> DriverPickUpGeoFences { get; set; }
        public List<DriverDeliveryGeoFenceViewModel> DriverDeliveryGeoFences { get; set; }
    }

}
