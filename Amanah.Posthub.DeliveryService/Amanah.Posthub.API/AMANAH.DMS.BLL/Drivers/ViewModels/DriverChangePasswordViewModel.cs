﻿namespace Amanah.Posthub.BLL.ViewModels.Driver
{
    public class DriverChangePasswordViewModel
    {      
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }

    }
}
