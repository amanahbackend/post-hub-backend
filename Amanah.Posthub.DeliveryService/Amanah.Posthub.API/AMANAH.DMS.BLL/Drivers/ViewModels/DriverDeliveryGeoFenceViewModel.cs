﻿namespace Amanah.Posthub.ViewModels
{
    public class DriverDeliveryGeoFenceViewModel
    {
        public int Id { set; get; }
        public int? DriverId { set; get; }
        public int GeoFenceId { set; get; }
        public string GeoFenceName { set; get; }
    }
}