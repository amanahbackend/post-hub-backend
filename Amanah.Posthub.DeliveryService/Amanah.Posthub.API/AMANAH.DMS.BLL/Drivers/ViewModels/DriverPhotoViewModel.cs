﻿using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.BLL.ViewModels.Driver
{
    public class DriverPhotoViewModel
    {
        public IFormFile photo { get; set; }
    }
}
