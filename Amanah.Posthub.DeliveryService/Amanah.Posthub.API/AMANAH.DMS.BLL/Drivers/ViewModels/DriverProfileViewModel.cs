﻿namespace Amanah.Posthub.BLL.ViewModels.Driver
{
    public class EditDriverProfileViewModel
    {
        //public string phone { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }

    }
}
