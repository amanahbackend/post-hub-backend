﻿using Amanah.Posthub.BLL.ViewModel;
using FluentValidation;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class DriverRateViewModel : BaseViewModel
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int DriverId { get; set; }
        public int TasksId { get; set; }
        public double Rate { get; set; }
        public string Note { get; set; }
    }

    public class DriverRateViewModelValidation : AbstractValidator<DriverRateViewModel>
    {
        public DriverRateViewModelValidation()
        {
            //RuleFor(x => x.Id).NotEmpty().NotNull() ;
            RuleFor(x => x.CustomerId).NotEmpty().NotNull();
            RuleFor(x => x.TasksId).NotEmpty().NotNull();
            RuleFor(x => x.Rate).NotEmpty().NotNull();
        }
    }
}
