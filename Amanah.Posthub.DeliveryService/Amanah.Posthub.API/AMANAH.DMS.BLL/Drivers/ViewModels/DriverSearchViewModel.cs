﻿using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace Amanah.Posthub.ViewModels
{
    public class DriverSearchViewModel : PaginatedItemsViewModel
    {
        public AgentStatusesEnum statusType { set; get; }
        public int driverType { set; get; }
    }
}