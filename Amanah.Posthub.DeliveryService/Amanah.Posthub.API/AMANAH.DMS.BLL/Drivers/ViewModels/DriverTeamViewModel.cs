﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class DriverTeamViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string ImageUrl { get; set; }
    }
}
