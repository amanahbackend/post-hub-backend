﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class DriverTransportionTypeViewModel
    {
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public int? TransportTypeId { set; get; }
        public string TransportTypeName { get; set; }
    }
}
