﻿using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.ViewModel;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.ViewModels
{
    public class DriverViewModel : BaseViewModel
    {
        private readonly DefaultSettingValue _defaultSettingValue;
        public DriverViewModel()
        {
            _defaultSettingValue = new DefaultSettingValue();
            MaxOrdersWeightsCapacity = _defaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
            MinOrdersNoWait = _defaultSettingValue.FirstInFirstOutMethodDriverOrderCapacity;
        }
        public int Id { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string ImageUrl { get; set; }
        public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public List<string> RoleNames { get; set; }
        public int TeamId { set; get; }
        public string TeamName { get; set; }
        public int? AgentTypeId { set; get; }
        public string AgentTypeName { get; set; }
        public DateTime AgentStatusUpdateDate { get; set; }
        public int? TransportTypeId { set; get; }
        public string TransportTypeName { get; set; }
        public int? CountryId { set; get; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {(string.IsNullOrEmpty(LastName) ? "" : LastName)}";
        public int? AgentStatusId { set; get; }
        public string Reason { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public int MaxOrdersWeightsCapacity { get; set; }
        public int MinOrdersNoWait { get; set; }
        public int AssignedTasksCount { get; set; }
        public DateTime? TokenExpirationDate { get; set; }

        public UserRegistrationStatus UserRegistrationStatus { get; set; }

        public double? DriverAvgRate { get; set; }
        public string AgentStatusName { get; set; }
        public DateTime? ReachedTime { get; set; }
        public bool IsReached => ReachedTime.HasValue;
        public double? RouteDistanceToTask { set; get; }
        public string LocationAccuracyName { set; get; }
        public int? LocationAccuracyDuration { set; get; }
        public int? BranchId { get; set; }
        public bool IsInClubbingTime { get; set; }
        public DateTime? ClubbingTimeExpiration { get; set; }
        public DateTime? TokenTimeExpiration { get; set; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }
        public int? DriverRegistrationId { set; get; }
        public List<DriverPickUpGeoFenceViewModel> DriverPickUpGeoFences { get; set; }
        public List<DriverDeliveryGeoFenceViewModel> DriverDeliveryGeoFences { get; set; }
    }
}
