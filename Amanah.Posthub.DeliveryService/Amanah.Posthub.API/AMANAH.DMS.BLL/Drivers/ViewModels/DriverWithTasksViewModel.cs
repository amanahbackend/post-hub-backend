﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace Amanah.Posthub.BLL.ViewModels.Driver
{
    public class DriverWithTasksViewModel : DriverViewModel
    {
        private int? ordersWeights;
        public List<MainTaskViewModel> AssignedMainTasks { get; set; }
        public int? MainTaskDeliveryGeoFenceId =>
            AssignedMainTasks == null || !AssignedMainTasks.Any()
                ? null
                : AssignedMainTasks.First().Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId;
        public int OrdersWeights
        {
            get => ordersWeights ?? (AssignedMainTasks == null ? 0 : AssignedMainTasks.Sum(t => t.OrderWeight));
            set => ordersWeights = value;
        }
        public int RemainingOrdersWeights => MaxOrdersWeightsCapacity - OrdersWeights;
        public int RemainingOrdersWeightsNoWait => MinOrdersNoWait - OrdersWeights;
    }
}
