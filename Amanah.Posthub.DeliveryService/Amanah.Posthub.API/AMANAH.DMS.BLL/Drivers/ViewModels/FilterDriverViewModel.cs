﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels.Mobile
{
    public class FilterDriverViewModel
    {
        public List<int> TeamIds { set; get; }
        public List<string> Tags { set; get; }
        public List<int> GeoFenceIds { set; get; }

    }
}
