﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class GetAailableDriversInput
    {
        public IEnumerable<int> TeamIds { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public IEnumerable<int> GeoFenceIds { get; set; }
        public IEnumerable<int> PickupGeoFenceIds { get; set; }
        public IEnumerable<int> DeliveryGeoFenceIds { get; set; }
    }
}
