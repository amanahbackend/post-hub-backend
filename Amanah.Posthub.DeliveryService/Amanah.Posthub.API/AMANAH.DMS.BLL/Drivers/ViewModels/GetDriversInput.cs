﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels.Driver
{
    public class GetDriverBaseInput
    {
        public bool IncludeAssignedTasks { get; set; }
        public TaskStatusEnum? TaskStatus { get; set; }
        public IList<TaskStatusEnum> TaskStatuses { get; set; }
        public IList<MainTaskStatus> MainTaskStatuses { get; set; }
        public bool IncludeGeoFences { get; set; }
        public bool IncludeDeliveryGeoFences { get; set; }
        public bool IncludePickupGeoFences { get; set; }
        public AgentStatusesEnum? Status { get; set; }
        public IList<AgentStatusesEnum> Statuses { get; set; }
        public bool? IsReached { get; set; }
        public bool IncludeCurrentLocation { get; set; } = true;
        public bool IncludeOrdersWeights { get; set; }
        public bool IsApplyingAutoAllocation { get; set; }
        public AutoAllocationTypeEnum AutoAllocationType { get; set; }

    }
    public class GetDriverInput : GetDriverBaseInput
    {
        public int DriverId { get; set; }
    }
    public class GetDriversInput : GetDriverBaseInput
    {
        public int? PickupBranchId { get; set; }
        public int? DeliveryGeoFenceId { get; set; }
        public List<int> DeliveryGeoFencesIds { get; set; }
        public int? SamePickupBranchAsDriverId { get; set; }
    }
}
