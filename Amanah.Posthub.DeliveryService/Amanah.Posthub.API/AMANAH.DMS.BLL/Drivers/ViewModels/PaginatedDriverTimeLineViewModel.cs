﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class PaginatedDriverTimeLineViewModel : PaginatedItemsViewModel
    {
        public int DriverId { get; set; }
    }
}
