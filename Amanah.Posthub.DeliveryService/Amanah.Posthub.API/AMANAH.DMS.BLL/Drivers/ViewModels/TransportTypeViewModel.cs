﻿namespace Amanah.Posthub.ViewModels
{

    public class TransportTypeViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}