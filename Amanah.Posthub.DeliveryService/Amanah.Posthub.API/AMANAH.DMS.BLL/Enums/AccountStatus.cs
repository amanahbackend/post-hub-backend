﻿namespace Amanah.Posthub.DeliverService.BLL.Enums
{
    public enum AccountStatus
    {
        Pending = 1,
        Approved = 2
    }
}
