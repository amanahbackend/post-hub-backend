﻿namespace Amanah.Posthub.DeliverService.BLL.Generic.Models
{
    public class LookupResultDto<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
    }
}
