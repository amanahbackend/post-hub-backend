﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IGeoFenceManager : IBaseManager<GeoFenceResultDTO, GeoFence>
    {
        Task<GeoFenceResultDTO> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> DeleteAsync(int id);
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> ids);

        Task<List<GeoFenceLookupViewModel>> GetAllPlatformZonesAsync();

        Task<List<GeoFenceExportToExcelViewModel>> ExportExcel();
        Task<List<int>> GetManagerZonesAsync();
    }
}
