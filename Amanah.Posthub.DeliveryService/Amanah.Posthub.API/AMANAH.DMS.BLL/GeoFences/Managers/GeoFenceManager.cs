﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers
{
    public class GeoFenceManager : BaseManager<GeoFenceResultDTO, GeoFence>, IGeoFenceManager
    {
        private readonly ICurrentUser _currentUser;

        public GeoFenceManager(
            ApplicationDbContext context,
            IRepositry<GeoFence> repository,
            IMapper mapper,
            ICurrentUser currentUser)
            : base(context, repository, mapper)
        {
            _currentUser = currentUser;
        }

        public async Task<GeoFenceResultDTO> Get(int id)
        {
            var geoFence = await context.GeoFence
                .IgnoreQueryFilters()
                .Include(t => t.GeoFenceLocations)
                .FirstOrDefaultAsync(x => x.Id == id);
            return mapper.Map<GeoFence, GeoFenceResultDTO>(geoFence);
        }

        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            var geoFence = await context.GeoFence.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == id);
            return mapper.Map<GeoFence, TQueryModel>(geoFence);
        }

        public override async Task<GeoFenceResultDTO> AddAsync(GeoFenceResultDTO viewModel)
        {
            var geoFence = mapper.Map<GeoFence>(viewModel);
            foreach (var t in viewModel.Locations)
            {
                var geoFenceLocation = new GeoFenceLocation
                {
                    Latitude = t.Latitude,
                    Longitude = t.Longitude
                };
                geoFence.GeoFenceLocations.Add(geoFenceLocation);
                context.Add(geoFenceLocation);
            }
            context.Add(geoFence);
            await context.SaveChangesAsync();
            return mapper.Map<GeoFenceResultDTO>(geoFence);
        }

        public override async Task<bool> UpdateAsync(GeoFenceResultDTO viewModel)
        {
            var geoFence = context.GeoFence.IgnoreQueryFilters().Include(t => t.GeoFenceLocations).AsNoTracking().SingleOrDefault(t => t.Id == viewModel.Id);
            geoFence.Name = viewModel.Name;
            geoFence.Description = viewModel.Description;
            context.Entry(geoFence).State = EntityState.Modified;

            foreach (var item in geoFence.GeoFenceLocations)
            {
                context.Entry(item).State = EntityState.Deleted;
            }
            await context.SaveChangesAsync();


            viewModel.Locations.ForEach(t =>
            {
                geoFence.GeoFenceLocations.Add(new GeoFenceLocation { Latitude = t.Latitude, Longitude = t.Longitude, GeoFenceId = geoFence.Id });
            });
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var geoFence = await context.GeoFence.IgnoreQueryFilters().SingleOrDefaultAsync(t => t.Id == id);

            if (geoFence == null)
            {
                return false;
            }
            context.GeoFence.Remove(geoFence);
            await context.SaveChangesAsync();
            return true;

        }

        public override async Task<List<GeoFenceViewModel>> GetAllAsync<GeoFenceViewModel>()
        {
            var geoFences = await repository.GetAll()
                .IgnoreQueryFilters()
                .Include(t => t.GeoFenceLocations)
                .OrderByDescending(x => x.CreatedDate)
                .ToListAsync();

            return mapper.Map<List<GeoFence>, List<GeoFenceViewModel>>(geoFences);
        }

        public override async Task<PagedResult<GeoFenceResultDTO>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<GeoFenceResultDTO>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;


            var source = repository.GetAll().IgnoreQueryFilters();

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var geoFences = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            pagedResult.Result = mapper.Map<List<GeoFence>, List<GeoFenceResultDTO>>(geoFences);

            return pagedResult;
        }

        public async Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> ids)
        {
            var geoFences = await repository.GetAll()
                .IgnoreQueryFilters()
                .Include(geofence => geofence.GeoFenceLocations)
                .Where(geofence => ids.Contains(geofence.Id))
                .OrderByDescending(geofence => geofence.CreatedDate)
                .ToListAsync();
            return mapper.Map<List<GeoFence>, List<TProjectedModel>>(geoFences);
        }

        public Task<List<GeoFenceExportToExcelViewModel>> ExportExcel()
        {
            var result = repository.GetAll()
                .IgnoreQueryFilters()
                .Select(g =>
                    new GeoFenceExportToExcelViewModel
                    {
                        GeoFenceId = g.Id,
                        GeoFenceName = g.Name,
                    })
                .ToListAsync();
            return result;
        }

        public async Task<List<int>> GetManagerZonesAsync()
        {
            string userId = _currentUser.Id;
            if (string.IsNullOrEmpty(userId))
            {
                return null;
            }
            var zones = await context.ManagerDispatching
                .IgnoreQueryFilters()
                .Include(x => x.Manager)
                .Where(x => x.Manager.UserId == userId)
                .Select(x => x.Zones)
                .ToListAsync();
            var createdZonesIds = await context.GeoFence
                .Where(x => x.CreatedBy_Id == _currentUser.UserName)
                .Select(x => x.Id).ToListAsync();
            List<int> zonesIds = new List<int>();
            if (zones != null && zones.Any())
            {
                zones.ForEach(x => zonesIds.AddRange(x.Split(',').Select(s => int.Parse(s))));
            }
            createdZonesIds.ForEach(x =>
            {
                if (zonesIds.Where(y => y == x).Count() == 0)
                {
                    zonesIds.Add(x);
                }
            });
            return zonesIds;
        }

        public async Task<List<GeoFenceLookupViewModel>> GetAllPlatformZonesAsync()
        {
            return await context.GeoFence
                .IgnoreQueryFilters()
                .Where(zone => zone.Tenant_Id == null)
                .ProjectTo<GeoFenceLookupViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
