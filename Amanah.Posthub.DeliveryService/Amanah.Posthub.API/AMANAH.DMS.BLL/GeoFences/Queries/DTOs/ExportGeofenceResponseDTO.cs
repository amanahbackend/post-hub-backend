﻿namespace Amanah.Posthub.BLL.GeoFences.Queries.DTOs
{
    public class ExportGeofenceResponseDTO
    {
        public int GeoFenceId { get; set; }
        public string GeoFenceName { get; set; }
        public int DriversCount { get; set; }
    }
}
