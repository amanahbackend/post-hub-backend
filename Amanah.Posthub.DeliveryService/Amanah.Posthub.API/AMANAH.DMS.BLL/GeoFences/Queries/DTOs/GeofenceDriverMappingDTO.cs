﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.GeoFences.Queries.DTOs
{
    public class GeofenceDriverMappingDTO
    {
        public int DriverId { get; internal set; }
        public IEnumerable<int> PickupGeofenceIds { get; internal set; }
        public IEnumerable<int> DeliveryGeofenceIds { get; internal set; }
        public bool AllPickup { get; internal set; }
        public bool AllDelivery { get; internal set; }
    }
}
