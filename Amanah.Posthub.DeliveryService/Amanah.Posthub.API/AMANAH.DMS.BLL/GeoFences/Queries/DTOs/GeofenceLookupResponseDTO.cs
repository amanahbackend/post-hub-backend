﻿namespace Amanah.Posthub.BLL.GeoFences.Queries.DTOs
{
    public class GeofenceLookupResponseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
