﻿using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.GeoFences.Queries.DTOs
{
    public class GeofenceResponseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<int> Drivers { get; set; }
        public virtual List<GeoFenceLocationResponseDTO> Locations { get; set; }
        public  List<GeoFenceBlocksResponseDTO> GeoFenceBlocks { get; set; }
        public virtual List<GeofenceTeamResponseDTO> Teams { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }

    public class GeoFenceLocationResponseDTO
    {
        public int Id { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public int PointIndex { get; set; }
    }

    public class GeoFenceBlocksResponseDTO
    {
        public int BlockId { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
    }

    public class GeofenceTeamResponseDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }

}
