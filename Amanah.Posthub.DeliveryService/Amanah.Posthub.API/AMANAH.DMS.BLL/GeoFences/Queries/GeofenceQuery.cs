﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.GeoFences.Queries.DTOs;
using Amanah.Posthub.Context;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.GeoFences.Queries
{
    public interface IGeofenceQuery
    {
        Task<List<GeofenceResponseDTO>> GetAllAsync(List<int> geofenceIds = null, bool? isUsedAsLookup = false);
        Task<List<int>> GetManagerDispatchingGeofenceAsync();
        Task<GeofenceResponseDTO> GetAsync(int id);
        Task<List<GeofenceLookupResponseDTO>> GetAllPlatformGeofencesAsync();
        Task<List<ExportGeofenceResponseDTO>> ExportExcelAsync();
    }
    internal class GeofenceQuery : IGeofenceQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public GeofenceQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        public async Task<List<GeofenceResponseDTO>> GetAllAsync(List<int> geofenceIds = null,
            bool? isUsedAsLookup = false)
        {
            var query = context.GeoFence.Where(c => !c.IsDeleted).Include(geofence => geofence.GeoFenceBlocks.Where(c => !c.IsDeleted)).Where(c => !c.IsDeleted).AsQueryable().IgnoreQueryFilters();
            if (!(isUsedAsLookup.HasValue && isUsedAsLookup.Value))
            {
                query = query.Include(geofence => geofence.GeoFenceLocations.Where(c => !c.IsDeleted));

            }
            if (geofenceIds != null && geofenceIds.Any())
            {
                query = query.Where(geofence => geofenceIds.Contains(geofence.Id) || geofence.CreatedBy_Id == currentUser.UserName);
            }
            var geoFences = await query
                .OrderByDescending(geofence => geofence.CreatedDate)
                .ProjectTo<GeofenceResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            if (geoFences != null && geoFences.Any() && !(isUsedAsLookup.HasValue && isUsedAsLookup.Value))
            {
                var driverGeofenceIds = await GetGeofenceDriversAsync(
                    geoFences.Select(geofence => geofence.Id).ToList());
                if (driverGeofenceIds != null && driverGeofenceIds.Any())
                {
                    foreach (var geofence in geoFences)
                    {
                        geofence.Drivers = driverGeofenceIds
                            .Where(driver => driver.AllDelivery
                                || driver.AllPickup
                                || driver.PickupGeofenceIds.Contains(geofence.Id)
                                || driver.DeliveryGeofenceIds.Contains(geofence.Id))
                            .Select(driver => driver.DriverId)
                            .Distinct()
                            .ToList();
                    }
                }
            }

            return geoFences;
        }

        public async Task<List<int>> GetManagerDispatchingGeofenceAsync()
        {
            List<int> geofenceIds = new List<int>();

            if (string.IsNullOrEmpty(currentUser.Id))
            {
                return null;
            }
            var dispatchingGeofenceIds = await context.ManagerDispatching
                .IgnoreQueryFilters()
                .Include(dispatching => dispatching.Manager)
                .Where(dispatching => dispatching.Manager.UserId == currentUser.Id && !string.IsNullOrEmpty(dispatching.Zones))
                .Select(dispatching => dispatching.Zones)
                .ToListAsync();
            if (dispatchingGeofenceIds != null && dispatchingGeofenceIds.Any())
            {
                dispatchingGeofenceIds.ForEach(dispatchingGeofenceId =>
                    geofenceIds.AddRange(dispatchingGeofenceId.Split(',').Select(geofenceId => int.Parse(geofenceId))));
            }

            //var createdByUserGeofenceIds = await context.GeoFence
            //    .Where(geofence => geofence.CreatedBy_Id == currentUser.UserName)
            //    .Select(geofence => geofence.Id).ToListAsync();
            //createdByUserGeofenceIds.ForEach(createdByUserGeofenceId =>
            //{
            //    if (geofenceIds.Where(geofenceId => geofenceId == createdByUserGeofenceId).Count() == 0)
            //    {
            //        geofenceIds.Add(createdByUserGeofenceId);
            //    }
            //});

            return geofenceIds;
        }

        public async Task<GeofenceResponseDTO> GetAsync(int id)
        {
            var geoFence = await context.GeoFence
                 .IgnoreQueryFilters()
                 .Include(geofence => geofence.GeoFenceLocations.Where(c => !c.IsDeleted))
                 .Include(geofence => geofence.GeoFenceBlocks.Where(c => !c.IsDeleted))
                 .ProjectTo<GeofenceResponseDTO>(mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync(geofence => geofence.Id == id);
            return geoFence;
        }

        public async Task<List<GeofenceLookupResponseDTO>> GetAllPlatformGeofencesAsync()
        {
            return await context.GeoFence
                .IgnoreQueryFilters()
                .Where(geofence => string.IsNullOrEmpty(geofence.Tenant_Id))
                .Select(geofence => new GeofenceLookupResponseDTO
                {
                    Id = geofence.Id,
                    Name = geofence.Name
                })
                .ToListAsync();
        }

        public async Task<List<ExportGeofenceResponseDTO>> ExportExcelAsync()
        {
            var geofences = await context.GeoFence
                .IgnoreQueryFilters()
                .Select(geofence => new ExportGeofenceResponseDTO
                {
                    GeoFenceId = geofence.Id,
                    GeoFenceName = geofence.Name,
                })
                .ToListAsync();
            if (geofences != null && geofences.Any())
            {
                var driverGeofenceIds = await GetGeofenceDriversAsync(
                  geofences.Select(geofence => geofence.GeoFenceId).ToList());
                if (driverGeofenceIds != null && driverGeofenceIds.Any())
                {
                    foreach (var geofence in geofences)
                    {
                        geofence.DriversCount = driverGeofenceIds
                            .Where(driver => driver.AllDelivery
                                || driver.AllPickup
                                || driver.PickupGeofenceIds.Contains(geofence.GeoFenceId)
                                || driver.DeliveryGeofenceIds.Contains(geofence.GeoFenceId))
                            .Select(driver => driver.DriverId)
                            .Distinct()
                            .Count();
                    }
                }
            }
            return geofences;
        }

        private async Task<List<GeofenceDriverMappingDTO>> GetGeofenceDriversAsync(List<int> matchedGeofenceIds)
        {
            return await context.Driver
              .IgnoreQueryFilters()
              .Include(driver => driver.DriverPickUpGeoFences)
              .Include(driver => driver.DriverDeliveryGeoFences)
              .Where(driver => driver.AllDeliveryGeoFences
                || driver.AllDeliveryGeoFences
                || (driver.DriverPickUpGeoFences.Any() && driver.DriverPickUpGeoFences.Where(pickup => matchedGeofenceIds.Contains(pickup.GeoFenceId)).Any())
                || (driver.DriverDeliveryGeoFences.Any() && driver.DriverDeliveryGeoFences.Where(delivery => matchedGeofenceIds.Contains(delivery.GeoFenceId)).Any()))
              .Select(driver => new GeofenceDriverMappingDTO
              {
                  DriverId = driver.Id,
                  PickupGeofenceIds = driver.DriverPickUpGeoFences.Select(x => x.GeoFenceId),
                  DeliveryGeofenceIds = driver.DriverDeliveryGeoFences.Select(x => x.GeoFenceId),
                  AllPickup = driver.AllPickupGeoFences,
                  AllDelivery = driver.AllDeliveryGeoFences
              })
              .ToListAsync();
        }

    }
}
