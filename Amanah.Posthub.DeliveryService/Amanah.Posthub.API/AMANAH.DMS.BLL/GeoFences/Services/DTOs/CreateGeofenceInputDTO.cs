﻿using FluentValidation;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.GeoFences.Services.DTOs
{
    public class CreateGeofenceInputDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<CreateGeoFenceLocationInputDTO> Locations { get; set; }
        public List<CreateGeoFenceBlockInputDTO> GeoFenceBlocks { get; set; }


        public class CreateGeofenceInputDTOValidation : AbstractValidator<CreateGeofenceInputDTO>
        {
            public CreateGeofenceInputDTOValidation()
            {
                RuleFor(geofence => geofence.Name)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(100);
                //RuleFor(geofence => geofence.Locations)
                //   .NotNull();
            }
        }
    }

    //ToDo: Longitude & Latitude not null here 
    public class CreateGeoFenceLocationInputDTO
    {
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public int PointIndex { get; set; }

    }

    public class CreateGeoFenceBlockInputDTO
    {
        public string Area { get; set; }
        public string Block { get; set; }
    }

}
