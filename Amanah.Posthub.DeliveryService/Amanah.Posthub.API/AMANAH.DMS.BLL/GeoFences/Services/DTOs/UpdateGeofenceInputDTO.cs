﻿using FluentValidation;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.GeoFences.Services.DTOs
{
    public class UpdateGeofenceInputDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<UpdateGeoFenceLocationInputDTO> Locations { get; set; }
        public List<UpdateGeoFenceBlocksInputDTO> GeoFenceBlocks { get; set; }
        public class UpdateGeofenceInputDTOValidation : AbstractValidator<UpdateGeofenceInputDTO>
        {
            public UpdateGeofenceInputDTOValidation()
            {
                RuleFor(geofence => geofence.Id)
                  .NotNull();
                RuleFor(geofence => geofence.Name)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(100);
                //RuleFor(geofence => geofence.Locations)
                //   .NotNull();
            }
        }
    }

    //ToDo: Longitude & Latitude not null here 
    public class UpdateGeoFenceLocationInputDTO
    {
        public int Id { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public int PointIndex { get; set; }
    }

    public class UpdateGeoFenceBlocksInputDTO
    {
        public int BlockId { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
    }

}
