﻿using Amanah.Posthub.BLL.GeoFences.Services.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.GeoFences.Repositories;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.BLL.GeoFences.Services
{
    public interface IGeofenceService
    {
        Task CreateAsync(CreateGeofenceInputDTO geofenceInput);
        Task<bool> UpdateAsync(UpdateGeofenceInputDTO geofenceInput);
        Task<bool> DeleteAsync(int id);
    }
    internal class GeofenceService : IGeofenceService
    {
        private readonly IGeofenceRepository geofenceRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IRepository<GeoFenceLocation> geoFenceLocationRepository;
        private readonly IRepository<GeoFenceBlocks> geoFenceBlocksRepository;
        private readonly ILocalizer _localizer;

        private readonly IMapper mapper;
        private readonly IUnitOfWork unityOfWork;
        private readonly ApplicationDbContext context;
        public GeofenceService(
            IGeofenceRepository geofenceRepository,
            IRepository<GeoFenceLocation> geoFenceLocationRepository,
            IRepository<AccountLogs> accountLogRepository,
            IRepository<GeoFenceBlocks> geoFenceBlocksRepository,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            ILocalizer localizer,
            ApplicationDbContext context
            )
        {
            this.geofenceRepository = geofenceRepository;
            this.geoFenceLocationRepository = geoFenceLocationRepository;
            this.geoFenceBlocksRepository = geoFenceBlocksRepository;
            this.accountLogRepository = accountLogRepository;
            this.mapper = mapper;
            this.unityOfWork = unityOfWork;
            this._localizer = localizer;
            this.context = context;
        }

        public async Task CreateAsync(CreateGeofenceInputDTO geofenceInput)
        {
            var geofence = mapper.Map<GeoFence>(geofenceInput);
            var geofencehavesameBlock = await GeofenceHaveTheSameBlockAsync(geofence);
            if (geofencehavesameBlock != null)
            {
                throw new DomainException(_localizer.Format(Messages.BlockExistonGeoFence, geofencehavesameBlock.Name));
                // return geofencehavesameBlock.Name;
            }

            var geofenceHaveTheSameName = await GeofenceHaveTheSameNameAsync(geofence);
            if (geofenceHaveTheSameName != null)
            {
                throw new DomainException(string.Format("There is geofence exist has the same name", geofenceHaveTheSameName.Name));
            }

            await unityOfWork.RunTransaction(async () =>
            {
                geofenceRepository.Add(geofence);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "GeoFence",
                    ActivityType = "Create",
                    Description = $"Geofence {geofence.Name } has been created",
                    Record_Id = geofence.Id
                });

                await unityOfWork.SaveChangesAsync();
            });

        }

        public async Task<bool> UpdateAsync(UpdateGeofenceInputDTO geofenceInput)
        {
            var existingGeofence = await geofenceRepository.GetAllIQueryable(geofence => geofence.Id == geofenceInput.Id, geofence => geofence.GeoFenceLocations)
                .Include(x => x.GeoFenceBlocks)
                .FirstOrDefaultAsync();



            if (existingGeofence != null)
            {
                await unityOfWork.RunTransaction(async () =>
                {
                   // geoFenceBlocksRepository.DeleteRange(existingGeofence.GeoFenceBlocks);
                    geoFenceLocationRepository.DeleteRange(existingGeofence.GeoFenceLocations);
                    var existingGeoFenceBlocks = existingGeofence.GeoFenceBlocks;

                    if (existingGeoFenceBlocks.Count > 0 && existingGeoFenceBlocks != null)
                    {
                        // oldPqItems = _context.RFQItems.IgnoreQueryFilters().Where(x => x.RFQId == rfQ.Id).ToList();
                        if (existingGeoFenceBlocks == null)
                        {
                            existingGeoFenceBlocks = mapper.Map<List<GeoFenceBlocks>>(geofenceInput.GeoFenceBlocks);
                        }
                        else
                        {
                            foreach (var blk in geofenceInput.GeoFenceBlocks)
                            {
                                if (existingGeoFenceBlocks.Where(c => c.Id == blk.BlockId && blk.BlockId > 0).Any())
                                {
                                    var geoFenceBlk = existingGeoFenceBlocks.Where(c => c.Id == blk.BlockId).FirstOrDefault();
                                    geoFenceBlk.Area = blk.Area;
                                    geoFenceBlk.Block = blk.Block;


                                }
                                else
                                {
                                    var geoFenceBlk = new GeoFenceBlocks();
                                    geoFenceBlk.Area = blk.Area;
                                    geoFenceBlk.Block = blk.Block;

                                    existingGeoFenceBlocks.Add(geoFenceBlk);
                                }
                            }
                        }
                        var geofenceInputBlocks = geofenceInput.GeoFenceBlocks.Select(cm => cm.BlockId);
                        var toBeDeletedBlocks = existingGeoFenceBlocks.Where(m => !geofenceInputBlocks.Contains(m.Id));
                        if (toBeDeletedBlocks != null && toBeDeletedBlocks.Count() > 0)
                        {
                            foreach (var geoBlk in toBeDeletedBlocks)
                            {
                                geoBlk.IsDeleted = true;
                                context.GeoFenceBlocks.Update(geoBlk);
                            }
                        }


                    }

                    //  existingGeofence = mapper.Map(geofenceInput, existingGeofence);
                    existingGeofence.Name = geofenceInput.Name;
                    existingGeofence.Description = geofenceInput.Description;
                    
                    existingGeofence.GeoFenceBlocks = existingGeoFenceBlocks;

                    var geofencehavesameBlock = await GeofenceHaveTheSameBlockAsync(existingGeofence);
                    if (geofencehavesameBlock != null)
                    {
                        throw new DomainException(_localizer.Format(Messages.BlockExistonGeoFence, geofencehavesameBlock.Name));
                    }

                    var geofenceHaveTheSameName = await GeofenceHaveTheSameNameAsync(existingGeofence);
                    if (geofenceHaveTheSameName != null)
                    {
                        throw new DomainException(string.Format("There is geofence exist has the same name", geofenceHaveTheSameName.Name));
                    }

                   // geofenceRepository.Update(existingGeofence);
                    context.GeoFence.Update(existingGeofence);  
                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "GeoFence",
                        ActivityType = "Update",
                        Description = $"Geofence {existingGeofence.Name } has been updated",
                        Record_Id = existingGeofence.Id
                    });

                    await unityOfWork.SaveChangesAsync();
                });
                return true;
            }
            return false;
        }

        public async Task<bool> DeleteAsync(int id)
        {

            var existingGeofence = await geofenceRepository.GetAllIQueryable(geofence => geofence.Id == id)
                .Include(x => x.GeoFenceBlocks)
                .Include(x => x.GeoFenceLocations)
                .FirstOrDefaultAsync();
            //await geofenceRepository.GetFirstOrDefaultAsync(geofence => geofence.Id == id);
            if (existingGeofence != null)
            {
                await unityOfWork.RunTransaction(async () =>
                {
                    if (existingGeofence.GeoFenceBlocks != null && existingGeofence.GeoFenceBlocks.Count > 0)
                        geoFenceBlocksRepository.DeleteRange(existingGeofence.GeoFenceBlocks);

                    if (existingGeofence.GeoFenceLocations != null && existingGeofence.GeoFenceLocations.Count > 0)
                        geoFenceLocationRepository.DeleteRange(existingGeofence.GeoFenceLocations);

                    existingGeofence.SetIsDeleted(true);
                    geofenceRepository.Update(existingGeofence);
                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "GeoFence",
                        ActivityType = "Delete",
                        Description = $"Geofence {existingGeofence.Name } has been updated",
                        Record_Id = existingGeofence.Id
                    });

                    await unityOfWork.SaveChangesAsync();
                });
                return true;
            }
            return false;
        }


        private async Task<GeoFence> GeofenceHaveTheSameBlockAsync(GeoFence geoFence)
        {
            foreach (var geofenceblocks in geoFence.GeoFenceBlocks)
            {
                var ExistingeoFence = await this.geofenceRepository.GetAllIQueryable()
                      .Where(x => (geoFence.Id <= 0 || x.Id != geoFence.Id) && x.GeoFenceBlocks.Any(x => x.Area == geofenceblocks.Area && x.Block == geofenceblocks.Block))
                      .FirstOrDefaultAsync();
                if (ExistingeoFence != null)
                {
                    return ExistingeoFence;
                }
            }
            return null;
        }

        private async Task<GeoFence> GeofenceHaveTheSameNameAsync(GeoFence geoFence)
        {
            var ExistingeoFence = await this.geofenceRepository.GetAllIQueryable()
                  .Where(x => (geoFence.Id <= 0 || x.Id != geoFence.Id) && x.Name == geoFence.Name)
                  .FirstOrDefaultAsync();
            if (ExistingeoFence != null)
            {
                return ExistingeoFence;
            }

            return null;
        }



    }
}
