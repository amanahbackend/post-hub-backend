﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class CreateGeoFenceViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<int> Drivers { get; set; }
        public virtual List<GeoFenceLocationResultDTO> Locations { get; set; }
    }

}
