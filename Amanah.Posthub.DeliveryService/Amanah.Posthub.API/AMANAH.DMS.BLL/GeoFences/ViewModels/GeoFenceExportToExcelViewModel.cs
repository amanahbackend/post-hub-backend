﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class GeoFenceExportToExcelViewModel
    {
        public int GeoFenceId { get; set; }
        public string GeoFenceName { get; set; }
        public int DriversCount { get; set; }
    }
}
