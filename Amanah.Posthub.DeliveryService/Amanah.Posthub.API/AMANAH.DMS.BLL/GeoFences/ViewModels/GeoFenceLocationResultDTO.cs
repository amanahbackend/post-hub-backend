﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class GeoFenceLocationResultDTO
    {
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public int PointIndex { get; set; }
    }
}
