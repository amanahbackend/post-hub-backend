﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class GeoFenceLookupViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
