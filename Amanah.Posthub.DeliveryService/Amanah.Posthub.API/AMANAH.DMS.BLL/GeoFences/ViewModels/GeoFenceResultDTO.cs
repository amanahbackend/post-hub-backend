﻿using Amanah.Posthub.BLL.ViewModel;
using FluentValidation;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class GeoFenceResultDTO : BaseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual List<int> Drivers { get; set; }
        public virtual List<GeoFenceLocationResultDTO> Locations { get; set; }
        public virtual List<LkpResultDto> Teams { get; set; }
    }

    public class GeoFenceResultDTOValidation : AbstractValidator<GeoFenceResultDTO>
    {
        public GeoFenceResultDTOValidation()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.Drivers).NotNull();
        }
    }
}
