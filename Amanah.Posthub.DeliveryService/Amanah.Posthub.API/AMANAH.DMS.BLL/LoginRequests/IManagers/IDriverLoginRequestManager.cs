﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Amanah.Posthub.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IDriverLoginRequestManager : IBaseManager<DriverLoginRequestViewModel, DriverLoginRequest>
    {
        Task<bool> Create(CreateDriverLoginRequestViewModel ent);
        Task<DriverLoginRequestViewModel> Get(int id);
        Task<DriverLoginRequestViewModel> GetByPendingDriverId(int driverId);
        Task<bool> CancelDriverRequest(int driverId);
        Task<List<TProjectedModel>> GetAllAsync<TProjectedModel>(List<int> driverIds);
        Task<PagedResult<DriverLoginRequestViewModel>> GetAllByPaginationAsync(DriverLoginRequestSearchViewModel pagingparametermodel, List<int> driverIds);
    }
}
