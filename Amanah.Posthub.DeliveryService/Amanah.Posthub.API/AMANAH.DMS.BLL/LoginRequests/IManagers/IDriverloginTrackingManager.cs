﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IDriverloginTrackingManager : IBaseManager<DriverloginTrackingViewModel, DriverloginTracking>
    {
        Task<DriverloginTrackingViewModel> GetLoggedIn(int driverId);
        Task<PagedResult<DriverloginTrackingViewModel>> GetByPaginationAsync(PaginatedDriverLoginTrackingViewModel pagingparametermodel);
        Task<List<ExportDriverloginTrackingViewModel>> ExportToExcel(PaginatedDriverLoginTrackingViewModel paginatedDriverLoginTrackingViewModel);
    }
}
