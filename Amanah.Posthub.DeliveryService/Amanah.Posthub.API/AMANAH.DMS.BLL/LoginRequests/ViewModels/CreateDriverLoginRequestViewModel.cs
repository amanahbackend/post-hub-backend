﻿using FluentValidation;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class CreateDriverLoginRequestViewModel
    {
        public int Id { get; set; }
        public byte Status { get; set; }
        public string Token { get; set; }
        public int DriverId { get; set; }
    }

    public class CreateDriverLoginRequestViewModelValidation : AbstractValidator<CreateDriverLoginRequestViewModel>
    {
        public CreateDriverLoginRequestViewModelValidation()
        {
            RuleFor(x => x.DriverId).NotNull();
            RuleFor(x => x.Status).NotNull();
            RuleFor(x => x.Token).NotNull().NotEmpty();

        }
    }
}
