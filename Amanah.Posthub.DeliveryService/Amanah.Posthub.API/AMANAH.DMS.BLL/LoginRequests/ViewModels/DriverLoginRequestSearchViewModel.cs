﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.ViewModels
{
    public class DriverLoginRequestSearchViewModel : PaginatedItemsViewModel
    {
        public List<int> StatusIDs { get; set; }
        public DateTime? FilterDate { get; set; }
        public DateTime? FilterTime { get; set; }
    }
}