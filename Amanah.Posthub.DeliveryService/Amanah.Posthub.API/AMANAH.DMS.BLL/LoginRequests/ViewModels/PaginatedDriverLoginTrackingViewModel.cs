﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class PaginatedDriverLoginTrackingViewModel : PaginatedItemsViewModel
    {
        public int? DriverId { get; set; }
        public DateTime? LogoutDate { get; set; }
    }
}
