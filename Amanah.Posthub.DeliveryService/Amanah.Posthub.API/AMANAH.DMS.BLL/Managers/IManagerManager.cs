﻿using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.ViewModels;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{

    public interface IManagerManager : IBaseManager<ManagerViewModel, Manager>
    {
        Task<ManagerViewModel> Get(int id);
        Task<PagedResult<ManagerViewModel>> GetAllMyByPaginationAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<PagedResult<ManagerViewModel>> GetPaginationByAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<PagedResult<ManagerViewModel>> GetMyPaginationByAsync(PaginatedItemsViewModel pagingparametermodel);
        Task<List<ManagerViewModel>> GetDriverManagersAsync(int driverId);
    }
}
