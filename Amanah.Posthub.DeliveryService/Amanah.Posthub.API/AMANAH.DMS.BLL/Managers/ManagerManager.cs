﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers
{
    public class ManagerManager : BaseManager<ManagerViewModel, Manager>, IManagerManager
    {
        private readonly IApplicationRoleManager _applicationRoleManager;
        private readonly ICurrentUser _currentUser;

        public ManagerManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Manager> repository,
            IApplicationRoleManager iApplicationRoleManager,
            ICurrentUser currentUser)
            : base(context, repository, mapper)
        {
            _applicationRoleManager = iApplicationRoleManager;
            _currentUser = currentUser;
        }

        public async Task<ManagerViewModel> Get(int id)
        {
            var manager = await (context as ApplicationDbContext).Manager
                .Include(x => x.TeamManagers)
                .ThenInclude(x => x.Team)
                .Include(x => x.User)
                .ThenInclude(x => x.Country)
                .FirstOrDefaultAsync(x => x.Id == id);
            manager.TeamManagers = manager.TeamManagers.Where(x => x.Team.IsDeleted == false).ToList();
            var managerVM = mapper.Map<Manager, ManagerViewModel>(manager);
            managerVM.RoleNames = _applicationRoleManager.GetAllRolesByUserId(managerVM.UserId);
            return managerVM;
        }

        public async Task<PagedResult<ManagerViewModel>> GetPaginationByAsync(PaginatedItemsViewModel pagingparametermodel)
        {
#warning cleanup- simplify and clean paging ToPagedResult
            var pagedResult = new PagedResult<ManagerViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;


            int.TryParse(pagingparametermodel.SearchBy, out var id);
            var source = repository.GetAll()
                .Include(x => x.User)
                .Include(x => x.TeamManagers)
                .ThenInclude(t => t.Team)
                .Include(x => x.User)
                .Where(x =>
                    string.IsNullOrEmpty(pagingparametermodel.SearchBy) ||
                    x.Id == id ||
                    x.User.FirstName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                    x.User.LastName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                    x.User.UserName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                    x.User.Email.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()));

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var managers = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            managers.ForEach(x => x.TeamManagers = x.TeamManagers.Where(d => d.Team.IsDeleted == false).ToList());
            var managersVM = mapper.Map<List<Manager>, List<ManagerViewModel>>(managers);
            managersVM.ForEach(x => x.RoleNames = _applicationRoleManager.GetAllRolesByUserId(x.UserId));
            pagedResult.Result = managersVM;

            return pagedResult;
        }
        public async Task<PagedResult<ManagerViewModel>> GetMyPaginationByAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<ManagerViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;


            int.TryParse(pagingparametermodel.SearchBy, out int id);
            var source = repository.GetAll().Include(x => x.User)
                .Include(x => x.TeamManagers)
                .ThenInclude(t => t.Team)
                .Include(x => x.User)
                .Where(x =>
                    (
                        string.IsNullOrEmpty(pagingparametermodel.SearchBy) ||
                        x.Id == id ||
                        x.User.FirstName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                        x.User.LastName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                        x.User.UserName.ToLower().Contains(pagingparametermodel.SearchBy.ToLower()) ||
                        x.User.Email.ToLower().Contains(pagingparametermodel.SearchBy.ToLower())
                    )
                    && x.CreatedBy_Id == _currentUser.UserName);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var managers = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            managers.ForEach(x => x.TeamManagers = x.TeamManagers.Where(d => d.Team.IsDeleted == false).ToList());
            var managersVM = mapper.Map<List<Manager>, List<ManagerViewModel>>(managers);
            managersVM.ForEach(x => x.RoleNames = _applicationRoleManager.GetAllRolesByUserId(x.UserId));
            pagedResult.Result = managersVM;

            return pagedResult;
        }

        public async override Task<PagedResult<ManagerViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<ManagerViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;


            var source = repository.GetAll().Include(x => x.User)
                        .Include(x => x.TeamManagers)
                        .ThenInclude(t => t.Team);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var managers = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            managers.ForEach(x => x.TeamManagers = x.TeamManagers.Where(d => d.Team.IsDeleted == false).ToList());
            var managersVM = mapper.Map<List<Manager>, List<ManagerViewModel>>(managers);
            managersVM.ForEach(x => x.RoleNames = _applicationRoleManager.GetAllRolesByUserId(x.UserId));
            pagedResult.Result = managersVM;

            return pagedResult;
        }


        public async Task<PagedResult<ManagerViewModel>> GetAllMyByPaginationAsync(PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<ManagerViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;


            var source = repository.GetAll()
                .Include(x => x.User)
                .Include(x => x.TeamManagers)
                .ThenInclude(t => t.Team)
                .Where(x => x.CreatedBy_Id == _currentUser.UserName);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var managers = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            managers.ForEach(x => x.TeamManagers = x.TeamManagers.Where(d => d.Team.IsDeleted == false).ToList());
            var managersVM = mapper.Map<List<Manager>, List<ManagerViewModel>>(managers);
            managersVM.ForEach(x => x.RoleNames = _applicationRoleManager.GetAllRolesByUserId(x.UserId));
            pagedResult.Result = managersVM;

            return pagedResult;
        }

        public async override Task<bool> UpdateAsync(ManagerViewModel viewModel)
        {
            await RemoveTeamManagerAsync(viewModel.Id);
            return await base.UpdateAsync(viewModel);
        }


        private async Task RemoveTeamManagerAsync(int id)
        {
            var dbManagerTeams = (context as ApplicationDbContext).TeamManager.Where(x => x.ManagerId == id);
            (context as ApplicationDbContext).TeamManager.RemoveRange(dbManagerTeams);
            await context.SaveChangesAsync();
        }

        public override async Task<bool> DeleteAsync(ManagerViewModel viewModel)
        {
            await RemoveTeamManagerAsync(viewModel.Id);
            return await base.DeleteAsync(viewModel);
        }

        public override async Task<bool> DeleteAsync(List<ManagerViewModel> managers)
        {
            foreach (var manager in managers)
            {
                await RemoveTeamManagerAsync(manager.Id);
            }
            return await base.DeleteAsync(managers);
        }

        public async override Task<ManagerViewModel> AddAsync(ManagerViewModel viewModel)
        {
            var manager = await base.AddAsync(viewModel);
            return await Get(manager.Id);
        }


        public override async Task<List<ManagerViewModel>> GetAllAsync<ManagerViewModel>()
        {
            var managers = await repository.GetAll()
                .Include(x => x.User)
                .Include(t => t.TeamManagers)
                .ThenInclude(t => t.Team)
                .ToListAsync();

            managers.ForEach(x => x.TeamManagers = x.TeamManagers.Where(d => d.Team.IsDeleted == false).ToList());
            return mapper.Map<List<Manager>, List<ManagerViewModel>>(managers);
        }

        public async Task<List<ManagerViewModel>> GetDriverManagersAsync(int driverId)
        {
            var driver = await context.Driver
                .IgnoreQueryFilters()
                .Where(driver => !driver.IsDeleted)
                .FirstOrDefaultAsync(x => x.Id == driverId);

            if (driver == null || driver.TeamId == 0)
            {
                return null;
            }
            var managerIds = await context.TeamManager
                .Where(x => x.TeamId == driver.TeamId)
                .IgnoreQueryFilters()
                .Select(x => x.ManagerId)
                .ToListAsync();

            if (managerIds == null || !managerIds.Any())
            {
                return new List<ManagerViewModel>();
            }
            return await context.Manager
                .Include(x => x.User)
                .Include(x => x.TeamManagers)
                .ThenInclude(t => t.Team)
                .Where(x => managerIds.Contains(x.Id))
                .IgnoreQueryFilters()
                .Where(manager => !manager.IsDeleted)
                .ProjectTo<ManagerViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
