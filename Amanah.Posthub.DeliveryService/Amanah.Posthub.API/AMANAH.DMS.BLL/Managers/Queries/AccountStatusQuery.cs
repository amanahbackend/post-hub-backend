﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Managers.Queries
{
    public interface IAccountStatusQuery
    {
        Task<List<AccountStatusResponseDTO>> GetAllAsync();
    }

    public class AccountStatusQuery : IAccountStatusQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<AccountStatus> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public AccountStatusQuery
            (
            IRepository<AccountStatus> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<AccountStatusResponseDTO>> GetAllAsync()
        {

            var request = await context.AccountStatus
                .AsNoTracking()
                .IgnoreQueryFilters()
                .ProjectTo<AccountStatusResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return request;
        }



    }
}
