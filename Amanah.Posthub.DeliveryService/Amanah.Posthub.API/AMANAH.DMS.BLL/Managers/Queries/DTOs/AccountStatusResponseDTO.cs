﻿namespace Amanah.Posthub.DeliverService.BLL.Managers.Queries.DTOs
{
    public class AccountStatusResponseDTO
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
    }
}
