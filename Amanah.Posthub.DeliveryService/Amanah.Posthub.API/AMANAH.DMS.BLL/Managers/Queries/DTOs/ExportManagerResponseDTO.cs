﻿namespace Amanah.Posthub.BLL.Managers.Queries.DTOs
{
    public class ExportManagerResponseDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string DepartmentName { set; get; }
        public string ServiceSectorName { set; get; }
        public string Nationality { get; set; }
        public string SupervisorName { get; set; }

        public string TeamName { get; set; }
        public string BranchName { get; set; }
        public string GenderName { get; set; }
        public string CompanyEmail { get; set; }


    }


}
