﻿namespace Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs
{
    public class GenderResponseDTO
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }

    }
}
