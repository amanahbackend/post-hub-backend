﻿
using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.Enums;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace Amanah.Posthub.DeliverService.BLL.Managers.Queries.DTOs
{
    public class ManagerByAccountStatusFilterDTO : PaginatedItemsViewModel
    {
        public int TotalNumbers { get; set; }
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string CompayEmail { get; set; }
        public bool? OnlyCreatedByCurrentUser { get; set; }
        public AccountStatus? AccountStatus { get; set; }
    }
}
