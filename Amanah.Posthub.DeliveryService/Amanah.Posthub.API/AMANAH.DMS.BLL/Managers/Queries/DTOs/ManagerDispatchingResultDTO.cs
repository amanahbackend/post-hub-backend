﻿using Amanah.Posthub.BLL.ViewModels;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Managers.Queries.DTOs
{
    public class ManagerDispatchingResultDTO
    {

        public int Id { get; set; }
        public string DesignationName { get; set; }
        public string Zones { get; set; }
        public List<LkpResultDto> LkpZones { get; set; }
        public string Restaurants { get; set; }
        public List<LkpResultDto> LkpRestaurants { get; set; }
        public string Branches { get; set; }
        public List<LkpResultDto> LkpBranches { get; set; }
        public int ManagerId { get; set; }
        public string ManagerName { get; set; }

    }
}
