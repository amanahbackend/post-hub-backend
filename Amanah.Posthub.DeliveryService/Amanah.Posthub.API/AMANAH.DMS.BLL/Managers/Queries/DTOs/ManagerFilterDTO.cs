﻿namespace Amanah.Posthub.BLL.Managers.Queries.DTOs
{
    public class ManagerFilterDTO
    {
        public int TotalNumbers { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchBy { get; set; }

        public int Id { get; set; }
        public string UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string CompayEmail { get; set; }
        public bool? OnlyCreatedByCurrentUser { get; set; }
    }
}
