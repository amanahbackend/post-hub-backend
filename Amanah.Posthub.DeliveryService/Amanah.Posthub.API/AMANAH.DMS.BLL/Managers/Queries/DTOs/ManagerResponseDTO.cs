﻿using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Managers.Queries.DTOs
{
    public class ManagerResponseDTO
    {
        public int Id { set; get; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int? CountryId { set; get; }
        public CountryResposeDTO Country { get; set; }
        public List<string> RoleNames { get; set; }
        public List<ManagerTeamResposeDTO> TeamManagers { set; get; }
        public int DepartmentId { set; get; }
        public string DepartmentName { set; get; }

        public int ServiceSectorId { get; set; }
        public string ServiceSectorName { set; get; }
        public DateTime? DateOfBirth { get; set; }
        public string NationalId { get; set; }
        public DateTime? NationalIdExtractDate { get; set; }
        public DateTime? NationalIdExpiryDate { get; set; }
        public int NationalityId { get; set; }
        public string Nationality { get; set; }
        public string ProfilePicURL { get; set; }
        public string SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public int BranchId { get; set; }
        public string BranchName { get; set; }

        public int GenderId { get; set; }
        public string GenderName { get; set; }
        public int AccountStatusId { get; set; }
        public string AccountStatusName { get; set; }
        public string CompanyEmail { get; set; }
        public string MobileNumber { get; set; }

        public string Tenant_Id { get; set; }
        public string CreatedBy_Id { get; set; }
        public string UpdatedBy_Id { get; set; }
        public string DeletedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }


    }

    public class ManagerTeamResposeDTO
    {
        public int Id { set; get; }
        public int TeamId { set; get; }
        public string TeamName { set; get; }
        public int ManagerId { set; get; }
        public bool IsDeleted { set; get; }
    }

    public class CountryResposeDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Code { set; get; }
        public string Flag { set; get; }
        public string FlagUrl { set; get; }
        public string TopLevel { set; get; }
    }
}
