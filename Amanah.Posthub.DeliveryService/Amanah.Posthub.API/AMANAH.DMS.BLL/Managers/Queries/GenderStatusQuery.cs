﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Managers.Queries
{
    public interface IGenderQuery
    {
        Task<List<GenderResponseDTO>> GetAllAsync();
    }

    public class GenderQuery : IGenderQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<Gender> repository;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;


        public GenderQuery
            (
            IRepository<Gender> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer,
            ICurrentUser currentUser)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
            _currentUser = currentUser;
        }

        public async Task<List<GenderResponseDTO>> GetAllAsync()
        {

            var request = await context.Genders
                .AsNoTracking()
                .IgnoreQueryFilters()
                .ProjectTo<GenderResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            return request;
        }



    }
}
