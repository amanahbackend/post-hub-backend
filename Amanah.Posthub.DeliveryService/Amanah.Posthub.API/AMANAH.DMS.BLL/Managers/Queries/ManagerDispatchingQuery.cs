﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers.Queries
{

    public interface IManagerDispatchingQuery
    {
        public Task<ManagerDispatchingResultDTO> GetAsync(int id);
        public Task<List<ManagerDispatchingResultDTO>> GetAllAsync();
        public Task<PagedResult<ManagerDispatchingResultDTO>> GetAllByPaginationAsync(
                PaginatedItemsViewModel pagingparametermodel);
    }
    public class ManagerDispatchingQuery : IManagerDispatchingQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;
        private readonly IRepository<ManagerDispatching> repository;


        public ManagerDispatchingQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser,
            IRepository<ManagerDispatching> repository)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
            this.repository = repository;
        }

        public async Task<ManagerDispatchingResultDTO> GetAsync(int id)
        {
            var manager = await (context as ApplicationDbContext).ManagerDispatching
                .Include(x => x.Manager)
                .Include(x => x.Manager.User)
                .IgnoreQueryFilters()
                .Where(x => x.Id == id)
                .ProjectTo<ManagerDispatchingResultDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            await GetManagerDispatchingMembersAsync(manager);
            return manager;
        }

        public async Task<List<ManagerDispatchingResultDTO>> GetAllAsync()
        {
            var list = await repository.GetAllIQueryable()
                .IgnoreQueryFilters()
                .ProjectTo<ManagerDispatchingResultDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            foreach (var x in list)
            {
                await GetManagerDispatchingMembersAsync(x);
            }
            return list;
        }

        public async Task<PagedResult<ManagerDispatchingResultDTO>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {


            var list = await repository.GetAllIQueryable()
                .IgnoreQueryFilters()
                .ToPagedResultAsync<ManagerDispatching, ManagerDispatchingResultDTO>(pagingparametermodel, mapper.ConfigurationProvider);
            foreach (var x in list.Result)
            {
                await GetManagerDispatchingMembersAsync(x);
            }
            return list;
        }

        private async Task GetManagerDispatchingMembersAsync(ManagerDispatchingResultDTO manger)
        {
            if (!string.IsNullOrEmpty(manger.Zones))
            {
                if (manger.Zones.ToLower() == "all")
                {
                    manger.LkpZones = await (context as ApplicationDbContext).GeoFence
                        .ProjectTo<LkpResultDto>(mapper.ConfigurationProvider)
                        .ToListAsync();
                }
                else
                {
                    var zones = manger.Zones.Split(',').ToList();
                    if (zones != null && zones.Any())
                    {
                        var zonesIds = new List<int>();
                        foreach (var zone in zones)
                        {
                            int.TryParse(zone, out int zoneId);
                            if (zoneId > 0) zonesIds.Add(zoneId);
                        }
                        manger.LkpZones = await (context as ApplicationDbContext).GeoFence
                            .Where(x => zonesIds.Any() && zonesIds.Contains(x.Id))
                            .ProjectTo<LkpResultDto>(mapper.ConfigurationProvider)
                            .ToListAsync();
                    }
                }
            }

            if (!string.IsNullOrEmpty(manger.Restaurants))
            {
                if (manger.Restaurants.ToLower() == "all")
                {
                    manger.LkpRestaurants = await (context as ApplicationDbContext).Restaurant
                          .ProjectTo<LkpResultDto>(mapper.ConfigurationProvider)
                          .ToListAsync();
                }
                else
                {
                    var restaurants = manger.Restaurants.Split(',').ToList();
                    if (restaurants != null && restaurants.Any())
                    {
                        var restaurantsIds = new List<int>();
                        foreach (var restaurant in restaurants)
                        {
                            int.TryParse(restaurant, out int restaurantId);
                            if (restaurantId > 0) restaurantsIds.Add(restaurantId);
                        }
                        manger.LkpRestaurants = await (context as ApplicationDbContext).Restaurant
                            .IgnoreQueryFilters()
                            .Where(x => restaurantsIds.Any() && restaurantsIds.Contains(x.Id))
                            .ProjectTo<LkpResultDto>(mapper.ConfigurationProvider)
                            .ToListAsync();
                    }
                }
            }

            if (!string.IsNullOrEmpty(manger.Branches))
            {
                if (manger.Branches.ToLower() == "all")
                {
                    manger.LkpBranches = await (context as ApplicationDbContext).Branch
                        .IgnoreQueryFilters()
                        .ProjectTo<LkpResultDto>(mapper.ConfigurationProvider)
                        .ToListAsync();
                }
                else
                {
                    var branches = manger.Branches.Split(',').ToList();
                    if (branches != null && branches.Any())
                    {
                        var branchesIds = new List<int>();
                        foreach (var branch in branches)
                        {
                            int.TryParse(branch, out int branchId);
                            if (branchId > 0) branchesIds.Add(branchId);
                        }
                        manger.LkpBranches = await (context as ApplicationDbContext).Branch
                            .Where(x => branchesIds.Any() && branchesIds.Contains(x.Id))
                            .ProjectTo<LkpResultDto>(mapper.ConfigurationProvider)
                            .ToListAsync();
                    }
                }
            }
        }

    }
}
