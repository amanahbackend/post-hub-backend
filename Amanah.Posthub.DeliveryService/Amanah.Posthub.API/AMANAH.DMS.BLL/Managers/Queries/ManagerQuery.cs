﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.Enums;
using Amanah.Posthub.DeliverService.BLL.Managers.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Managers.Queries
{
    public interface IManagerQuery
    {
        Task<List<ManagerResponseDTO>> GetAllAsync();
        Task<PagedResult<ManagerResponseDTO>> GetByPaginationAsync(ManagerFilterDTO filter);
        Task<PagedResult<ManagerResponseDTO>> GetPaginationByAccountStatusAsync(ManagerByAccountStatusFilterDTO filter);
        Task<ManagerResponseDTO> GetAsync(int id);

        Task<List<ExportManagerResponseDTO>> ExportManagersAsync(ManagerFilterDTO filter);

    }
    internal class ManagerQuery : IManagerQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public ManagerQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        public async Task<List<ManagerResponseDTO>> GetAllAsync()
        {
            var managers = await context.Manager.IgnoreQueryFilters()
                 .ProjectTo<ManagerResponseDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();
            managers.ForEach(manager => manager.TeamManagers = manager.TeamManagers.Where(team => !team.IsDeleted).ToList());

            return managers;
        }

        public async Task<PagedResult<ManagerResponseDTO>> GetByPaginationAsync(ManagerFilterDTO filter)
        {
            PagedResult<ManagerResponseDTO> result = new PagedResult<ManagerResponseDTO>();
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;

            var query = context.Manager.IgnoreQueryFilters().OrderByDescending(c => c.Id).AsQueryable();

            if (filter.OnlyCreatedByCurrentUser.HasValue && filter.OnlyCreatedByCurrentUser.Value)
            {
                query = query.Where(manager => manager.CreatedBy_Id == currentUser.UserName);
            }

            if (!string.IsNullOrEmpty(filter.SearchBy))
            {
                int.TryParse(filter.SearchBy, out var managerId);
                query = query.Where(manager => manager.Id == managerId
                    || manager.User.FirstName.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.User.LastName.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.CompanyEmail.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.MobileNumber.ToLower().Contains(filter.SearchBy.ToLower())
                    );
            }


            var managers = await query
                 .ProjectTo<ManagerResponseDTO>(mapper.ConfigurationProvider)
                 .Skip((filter.PageNumber - 1) * filter.PageSize).Take(filter.PageSize)
                 .ToListAsync();

            var managersRoleNames = await (from roles in context.Roles
                                           join userRoles in context.UserRoles on roles.Id equals userRoles.RoleId
                                           where managers.Select(manager => manager.UserId).Contains(userRoles.UserId)
                                           select new
                                           {
                                               UserId = userRoles.UserId,
                                               RoleName = roles.Name
                                           }).ToListAsync();

            managers.ForEach(manager =>
            {
                manager.TeamManagers = manager.TeamManagers.Where(team => !team.IsDeleted).Distinct().ToList();
                manager.RoleNames = managersRoleNames?.Where(mng => mng.UserId == manager.UserId)
                    .Select(role => role.RoleName)
                    .Distinct()
                    .ToList();
            });

            result.TotalCount = await query.CountAsync();
            result.Result = managers;

            return result;
        }

        public async Task<PagedResult<ManagerResponseDTO>> GetPaginationByAccountStatusAsync(ManagerByAccountStatusFilterDTO filter)
        {
            PagedResult<ManagerResponseDTO> result = new PagedResult<ManagerResponseDTO>();
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;

            var query = context.Manager
                .Include(m => m.User)
                .OrderByDescending(c => c.Id)
                .AsQueryable();

            if (filter.OnlyCreatedByCurrentUser.HasValue && filter.OnlyCreatedByCurrentUser.Value)
            {
                query = query.Where(manager => manager.CreatedBy_Id == currentUser.UserName);
            }

            if (!string.IsNullOrEmpty(filter.SearchBy))
            {
                int.TryParse(filter.SearchBy, out var managerId);
                query = query.Where(manager => manager.Id == managerId
                    || manager.User.FirstName.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.User.LastName.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.CompanyEmail.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.MobileNumber.ToLower().Contains(filter.SearchBy.ToLower())
                    );
            }

            if (filter.AccountStatus.HasValue && filter.AccountStatus == AccountStatus.Pending)
            {
                query = query.Where(m => m.User.IsLocked == true);
            }
            else if (filter.AccountStatus.HasValue && filter.AccountStatus == AccountStatus.Approved)
            {
                query = query.Where(m => m.User.IsLocked == false);
            }

            var managers = await query
                .IgnoreQueryFilters()
                .ProjectTo<ManagerResponseDTO>(mapper.ConfigurationProvider)
                .ToPagedResultAsync(filter);
            //.ProjectTo<ManagerResponseDTO>(mapper.ConfigurationProvider)
            //.Skip((filter.PageNumber - 1) * filter.PageSize).Take(filter.PageSize)
            //.ToListAsync();

            var managersRoleNames = await (from roles in context.Roles
                                           join userRoles in context.UserRoles on roles.Id equals userRoles.RoleId
                                           where managers.Result.Select(manager => manager.UserId).Contains(userRoles.UserId)
                                           select new
                                           {
                                               UserId = userRoles.UserId,
                                               RoleName = roles.Name
                                           }).ToListAsync();

            foreach (ManagerResponseDTO m in managers.Result)
            {
                //m.TeamManagers = m.TeamManagers.Where(team => !team.IsDeleted).ToList();
                m.RoleNames = managersRoleNames?.Where(mng => mng.UserId == m.UserId)
                    .Select(role => role.RoleName)
                    .Distinct()
                    .ToList();
            }

            //managers.Result.ForEach(manager =>
            //{
            //    manager.TeamManagers = manager.TeamManagers.Where(team => !team.IsDeleted).ToList();
            //    manager.RoleNames = managersRoleNames?.Where(mng => mng.UserId == manager.UserId)
            //        .Select(role => role.RoleName)
            //        .Distinct()
            //        .ToList();
            //});

            //result.TotalCount = await query.CountAsync();
            //result.Result = managers;

            return managers;
        }

        public async Task<ManagerResponseDTO> GetAsync(int id)
        {
            var manager = await context.Manager.IgnoreQueryFilters()
                .Where(mng => mng.Id == id)
                .ProjectTo<ManagerResponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            var managerRoleNames = await (from roles in context.Roles
                                          join userRoles in context.UserRoles on roles.Id equals userRoles.RoleId
                                          where manager.UserId == userRoles.UserId
                                          select new
                                          {
                                              UserId = userRoles.UserId,
                                              RoleName = roles.Name
                                          }).ToListAsync();

            manager.TeamManagers = manager.TeamManagers.Where(team => !team.IsDeleted).ToList();
            manager.RoleNames = managerRoleNames?.Where(mng => mng.UserId == manager.UserId)
                .Select(role => role.RoleName)
                .Distinct()
                .ToList();

            return manager;
        }

        public async Task<List<ExportManagerResponseDTO>> ExportManagersAsync(ManagerFilterDTO filter)
        {

            var query = context.Manager.IgnoreQueryFilters().AsQueryable();

            if (filter.OnlyCreatedByCurrentUser.HasValue && filter.OnlyCreatedByCurrentUser.Value)
            {
                query = query.Where(manager => manager.CreatedBy_Id == currentUser.UserName);
            }

            if (!string.IsNullOrEmpty(filter.SearchBy))
            {
                int.TryParse(filter.SearchBy, out var managerId);
                query = query.Where(manager => manager.Id == managerId
                    || manager.User.FirstName.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.User.LastName.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.CompanyEmail.ToLower().Contains(filter.SearchBy.ToLower())
                    || manager.MobileNumber.ToLower().Contains(filter.SearchBy.ToLower())
                    );
            }

            var managers = await query
                 .ProjectTo<ExportManagerResponseDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();

            return managers;
        }

    }
}
