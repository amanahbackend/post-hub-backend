﻿namespace Amanah.Posthub.BLL.Managers.Queries.DTOs
{
    public class CreateManagerDispatchingInputDTO
    {
        public string DesignationName { get; set; }
        public string Zones { get; set; }
        public string Restaurants { get; set; }
        public string Branches { get; set; }
        public int ManagerId { get; set; }
    }
}
