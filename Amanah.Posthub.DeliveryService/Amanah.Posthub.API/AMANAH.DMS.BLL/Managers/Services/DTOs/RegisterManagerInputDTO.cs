﻿using Amanah.Posthub.BLL.Managers.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.Enums;

namespace Amanah.Posthub.DeliverService.BLL.Managers.Services.DTOs
{
    public class RegisterManagerInputDTO : CreateManagerInputDTO
    {
        public AccountStatus? AccountStatus { get; set; }
    }
}
