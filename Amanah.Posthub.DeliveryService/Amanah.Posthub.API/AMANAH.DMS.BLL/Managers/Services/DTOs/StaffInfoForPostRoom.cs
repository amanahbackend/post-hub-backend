﻿using Amanah.Posthub.BLL.Branches.Queries.DTOs;

namespace Amanah.Posthub.BLL.Managers.Services.DTOs
{
    public class StaffInfoForPostRoom
    {
        public int Branchid { get; set; }
        public string BranchName { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName_ar { get; set; }
        public string DepartmentName_en { get; set; }
        public int FloorNo { get; set; }
        public BranchAddressResponseDTO Location { get; set; }
    }
}