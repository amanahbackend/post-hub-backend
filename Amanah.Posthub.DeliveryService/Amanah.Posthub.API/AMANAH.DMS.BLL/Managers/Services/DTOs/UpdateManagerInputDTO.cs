﻿using Amanah.Posthub.Domain.Users.Repositories;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Managers.Services.DTOs
{
    public class UpdateManagerInputDTO
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int? CountryId { set; get; }
        public string MobileNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public List<string> RoleNames { get; set; }
        public int DepartmentId { set; get; }
        public int BranchId { get; set; }
        public int? ServiceSectorId { set; get; }
        public DateTime? DateOfBirth { get; set; }
        public int NationalityId { get; set; }
        public IFormFile ProfilePicFile { get; set; }
        public string ProfilePicURL { get; set; }
        public string NationalId { get; set; }
        public DateTime? NationalIdExtractDate { get; set; }
        public DateTime? NationalIdExpiryDate { get; set; }
        public string SupervisorId { get; set; }
        public int AccountStatusId { get; set; }
        public int GenderId { get; set; }
        public string CompanyEmail { get; set; }


        public List<UpdateManagerTeamInputDTO> TeamManagers { set; get; }

        public class UpdateManagerInputDTOValidator : AbstractValidator<UpdateManagerInputDTO>
        {
            private readonly IUserRepository userRepository;

            public UpdateManagerInputDTOValidator(
                IUserRepository userRepository,
                ILocalizer localizer)
            {
                this.userRepository = userRepository;

                RuleFor(manager => manager.FirstName).NotEmpty().NotNull().MaximumLength(100);
               // RuleFor(manager => manager.PhoneNumber).NotEmpty().NotNull();
                RuleFor(manager => manager.TeamManagers)
                    .NotNull()
                    .WithMessage(localizer[Keys.Validation.CreateMangerTeamIsRequired]);
                //RuleFor(manager => manager.Username)
                //    .NotEmpty()
                //    .NotNull()
                //    .MustAsync(IsUsernameExistAsync)
                //    .WithMessage(manager => localizer.Format(Keys.Validation.UserNameAlreadyExists, manager.Username));
                //RuleFor(manager => manager.Email)
                //       .NotEmpty()
                //       .MustAsync(IsEmailExistAsync)
                //       .WithMessage(localizer[Keys.Validation.EmailAlreadyExists]);
            }

            public async Task<bool> IsUsernameExistAsync(string username, CancellationToken cancellationToken)
            {
                var user = await userRepository.GetAsync(user => user.UserName.ToLower().Trim() == username.ToLower().Trim());
                if (user == null)
                {
                    return true;
                }
                return false;
            }
            public async Task<bool> IsEmailExistAsync(string email, CancellationToken cancellationToken)
            {
                var user = await userRepository.GetAsync(user => user.NormalizedEmail == email.ToUpper().Trim());
                if (user == null)
                {
                    return true;
                }
                return false;
            }

        }
    }

    public class UpdateManagerTeamInputDTO
    {
        public int Id { set; get; }
        public int TeamId { set; get; }
        public int ManagerId { set; get; }
    }
}
