﻿using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers.Services
{
    public interface IManagerDispatchingService
    {
        public Task<int?> AddAsync(CreateManagerDispatchingInputDTO managerDispatching);
        public Task<int?> UpdateAsync(UpdateManagerDispatchingInputDTO managerDispatching);
        public Task<int?> DeleteAsync(int id);

    }
    public class ManagerDispatchingService : IManagerDispatchingService
    {
        private readonly IRepository<ManagerDispatching> managerDispatchingRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unityOfWork;

        public ManagerDispatchingService(
            IRepository<ManagerDispatching> managerDispatchingRepository,
            IRepository<AccountLogs> accountLogRepository,
            IUnitOfWork unityOfWork,
            IMapper mapper)
        {
            this.managerDispatchingRepository = managerDispatchingRepository;
            this.accountLogRepository = accountLogRepository;
            this.mapper = mapper;
            this.unityOfWork = unityOfWork;
        }

        public async Task<int?> AddAsync(CreateManagerDispatchingInputDTO createManagerDispatching)
        {
            var managerDispatching = mapper.Map<ManagerDispatching>(createManagerDispatching);
            await unityOfWork.RunTransaction(async () =>
            {
                managerDispatchingRepository.Add(managerDispatching);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "ManagerDispatching",
                    ActivityType = "Create",
                    Description = $" Manager Dispatching  { managerDispatching.Id } has been created ",
                    Record_Id = managerDispatching.Id
                });

                await unityOfWork.SaveChangesAsync();
            });

            return managerDispatching.Id;
        }

        public async Task<int?> UpdateAsync(UpdateManagerDispatchingInputDTO updateManagerDispatching)
        {
            var existedManagerDispatching = mapper.Map<ManagerDispatching>(updateManagerDispatching);
            await unityOfWork.RunTransaction(async () =>
            {
                managerDispatchingRepository.Update(existedManagerDispatching);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "ManagerDispatching",
                    ActivityType = "Update",
                    Description = $" Manager Dispatching { updateManagerDispatching.Id } has been updated ",
                    Record_Id = updateManagerDispatching.Id
                });

                await unityOfWork.SaveChangesAsync();
            });

            return existedManagerDispatching.Id;
        }

        public async Task<int?> DeleteAsync(int id)
        {
            var existedManagerDispatching = await managerDispatchingRepository.GetByIdAsync(id);
            await unityOfWork.RunTransaction(async () =>
            {
                managerDispatchingRepository.Delete(existedManagerDispatching);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "ManagerDispatching",
                    ActivityType = "Delete",
                    Description = $"Manager Dispatching {id } has been deleted ",
                    Record_Id = id
                });

                await unityOfWork.SaveChangesAsync();
            });
            return existedManagerDispatching.Id;
        }
    }
}
