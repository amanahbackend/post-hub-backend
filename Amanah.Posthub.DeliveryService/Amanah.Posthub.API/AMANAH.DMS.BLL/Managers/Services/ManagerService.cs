﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.BLL.Managers.Services.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Managers.Services.DTOs;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.SharedKernel.Authentication.PermissionsProvider;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;

namespace Amanah.Posthub.BLL.Managers.Services
{
    public interface IManagerService
    {
        Task<ManagerResponseDTO> CreateAsync(CreateManagerInputDTO managerInput);
        Task<bool> ApproveStaffAsync(int staffId);
        Task<ManagerResponseDTO> RegisterStaffAsync(RegisterManagerInputDTO managerInput);
        Task<StaffInfoForPostRoom> GetStaffInfoForPostRoomAsync(string userId);
        Task<ManagerResponseDTO> UpdateAsync(UpdateManagerInputDTO managerInput);
        Task<bool> DeleteAsync(int id);
        Task<bool> ChangeUserPasswordAsync(string userId, string newPassword);
    }
    internal class ManagerService : IManagerService
    {
        private readonly IRepository<Manager> managerRepository;
        private readonly IUserDomainService userDomainService;
        private readonly IUserRepository userRepository;
        private readonly IRepository<TeamManager> teamManagerRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unityOfWork;
        private readonly IUploadFormFileService uploadFormFileService;
        private readonly ApplicationDbContext context;
        private readonly ICurrentUser _currentUser;
        private readonly IRoleDomainService _roleDomainService;
        private readonly IRoleRepository roleRepository;
        private readonly UserManager<ApplicationUser> userManager;

        public ManagerService(
            IRepository<Manager> managerRepository,
            IUserDomainService userDomainService,
            IUserRepository userRepository,
            IRepository<TeamManager> teamManagerRepository,
            IRepository<AccountLogs> accountLogRepository,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IUploadFormFileService uploadFormFileService,
            ApplicationDbContext context,
            ICurrentUser currentUser,
            IRoleDomainService roleDomainService,
            IRoleRepository roleRepository,
            UserManager<ApplicationUser> userManager
            )
        {
            this.managerRepository = managerRepository;
            this.userDomainService = userDomainService;
            this.userRepository = userRepository;
            this.teamManagerRepository = teamManagerRepository;
            this.accountLogRepository = accountLogRepository;
            this.mapper = mapper;
            this.unityOfWork = unityOfWork;
            this.uploadFormFileService = uploadFormFileService;
            this.context = context;
            _currentUser = currentUser;
            _roleDomainService = roleDomainService;
            this.roleRepository = roleRepository;
            this.userManager = userManager;
        }

        public async Task<ManagerResponseDTO> CreateAsync(CreateManagerInputDTO managerInput)
        {
            Manager manager = null;
            var user = mapper.Map<ApplicationUser>(managerInput);
            await unityOfWork.RunTransaction(async () =>
            {
                user = await userDomainService.CreateAsync(user, managerInput.Password, managerInput.RoleNames?.FirstOrDefault());
                if (!string.IsNullOrEmpty(user.Id))
                {
                    managerInput.GenderId = managerInput.GenderId.HasValue ? managerInput.GenderId : 1;

                    manager = mapper.Map<Manager>(managerInput);
                    manager.UserId = user.Id;
                    manager.ProfilePicURL = await SetManagerPhotoAsync(managerInput.ProfilePicFile);
                    managerRepository.Add(manager);
                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Manager",
                        ActivityType = "Create",
                        Description = $"Manager {managerInput.Username} has been created ",
                        Record_Id = manager.Id
                    });
                    try
                    {
                        await unityOfWork.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
            });

            return new ManagerResponseDTO { Id = manager.Id, UserId = manager.UserId };
        }
        public async Task<ManagerResponseDTO> RegisterStaffAsync(RegisterManagerInputDTO managerInput)
        {


            Manager manager = null;
            var user = mapper.Map<ApplicationUser>(managerInput);
            //user.Tenant_Id = "AV59A46B-72BF-4849-82D0-43851B574590";
            if (managerInput.AccountStatus.HasValue && managerInput.AccountStatus == DeliverService.BLL.Enums.AccountStatus.Approved)
                user.IsLocked = false;
            else
                user.IsLocked = true;

            user.RoleNames = new List<string>() { "InternalPost" };

            await unityOfWork.RunTransaction(async () =>
            {
                // user = await userDomainService.CreateAsync(user, managerInput.Password, "InternalPost");
                if (_currentUser != null && !string.IsNullOrEmpty(_currentUser.TenantId))
                {
                    user = await userDomainService.CreateAsync(user, managerInput.Password, null);
                }
                else
                {
                    user = await userDomainService.CreateAsync(user, managerInput.Password, null, true);
                }
                List<string> permissions = ManagerPermissionProvider.GetDafaultPermissions();
                var newrole = new ApplicationRole("InternalPost", user.Tenant_Id);
                newrole.Type = (int)RoleType.Manager;
                if (!await roleRepository.IsRoleExistAsync("InternalPost"))
                {
                    var role = await _roleDomainService.CreateAsync(newrole, permissions);

                }
                user.RoleNames = new List<string> { "InternalPost" };
                await userManager.AddToRolesAsync(user, user.RoleNames);

                await userDomainService.AddUserToRolesAsync(user);

                if (!string.IsNullOrEmpty(user.Id))
                {
                    manager = mapper.Map<Manager>(managerInput);
                    manager.UserId = user.Id;
                    manager.ServiceSectorId = 4;
                    manager.AccountStatusId = 1;
                    manager.Tenant_Id = user.Tenant_Id;
                    manager.CompanyEmail = user.UserName;
                    managerRepository.Add(manager);


                    await unityOfWork.SaveChangesAsync();
                }
            });

            return new ManagerResponseDTO { Id = manager.Id, UserId = manager.UserId };
        }

        public async Task<StaffInfoForPostRoom> GetStaffInfoForPostRoomAsync(string userId)
        {
            var staff = await context.Manager
                .Include(m => m.Branch)
                .Include(m => m.Department)
                .Where(m => m.UserId == userId)
                .Select(m => new StaffInfoForPostRoom
                {
                    Branchid = m.BranchId,
                    BranchName = m.Branch.Name,
                    DepartmentId = m.DepartmentId,
                    DepartmentName_ar = m.Department.Name_ar,
                    DepartmentName_en = m.Department.Name_en,
                    FloorNo = m.Department.FloorNo,
                    Location = new Branches.Queries.DTOs.BranchAddressResponseDTO
                    {
                        Area = m.Branch != null ? m.Branch.Location != null ? m.Branch.Location.Area : "" : "",
                        Block = m.Branch != null ? m.Branch.Location != null ? m.Branch.Location.Block : "" : "",
                        Building = m.Branch != null ? m.Branch.Location != null ? m.Branch.Location.Building : "" : "",
                        Flat = m.Branch != null ? m.Branch.Location != null ? m.Branch.Location.Flat : "" : "",
                        Floor = m.Branch != null ? m.Branch.Location != null ? m.Branch.Location.Floor : "" : "",
                        Governorate = m.Branch != null ? m.Branch.Location != null ? m.Branch.Location.Governorate : "" : "",
                        Street = m.Branch != null ? m.Branch.Location != null ? m.Branch.Location.Governorate : "" : "",
                    }
                })
                .FirstOrDefaultAsync();

            return staff;
        }
        public async Task<bool> ApproveStaffAsync(int staffId)
        {
            try
            {


                var manager = await context.Manager
                    .Include(m => m.User)
                    .Where(m => m.Id == staffId)
                    .IgnoreQueryFilters()
                    .FirstOrDefaultAsync();
                if (manager != null)
                {
                    //manager.User.IsLocked = true;
                    var user = manager.User;
                    if (user != null)
                    {
                        user.IsLocked = false;
                        await userManager.UpdateAsync(user);

                    }

                    manager.AccountStatusId = (int)Amanah.Posthub.DeliverService.BLL.Enums.AccountStatus.Approved;
                    context.Update(manager);
                }

                await context.SaveChangesAsync();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
                //throw;
            }

        }
        public async Task<ManagerResponseDTO> UpdateAsync(UpdateManagerInputDTO managerInput)
        {
            var user = mapper.Map<ApplicationUser>(managerInput);
            await unityOfWork.RunTransaction(async () =>
            {
                user = await userDomainService.EditAsync(user, managerInput.RoleNames?.FirstOrDefault());
                await unityOfWork.SaveChangesAsync();

                var existingManager = await managerRepository.GetAllIQueryable(mng => mng.Id == managerInput.Id, mng => mng.TeamManagers)
                    .Include(c => c.TeamManagers).FirstOrDefaultAsync();
                ICollection<TeamManager> TeamManagersExisting = existingManager.TeamManagers;
                existingManager = mapper.Map<Manager>(managerInput);


                if (existingManager != null)
                {

                    if (managerInput.TeamManagers != null)
                    {
                        if (TeamManagersExisting == null)
                        {
                            existingManager.TeamManagers = managerInput.TeamManagers.Select(c => new TeamManager
                            {
                                ManagerId = c.ManagerId,
                                TeamId = c.TeamId
                            }).ToList();
                        }
                        else
                        {
                            foreach (var item in managerInput.TeamManagers)
                            {
                                if (TeamManagersExisting.Where(c => c.Id == item.Id && c.Id > 0).Any())
                                {
                                    var teammanager = managerInput.TeamManagers.Where(c => c.Id == item.Id).FirstOrDefault();
                                    teammanager.ManagerId = item.ManagerId;
                                    teammanager.TeamId = item.TeamId;

                                }
                                else
                                {
                                    var teamManager = new TeamManager();
                                    teamManager.ManagerId = item.ManagerId;
                                    teamManager.TeamId = item.TeamId;
                                    TeamManagersExisting.Add(teamManager);
                                }
                            }
                        }
                        var teamManagersExist = managerInput.TeamManagers.Select(cm => cm.Id);
                        var toBeDeletedTeamManagers = TeamManagersExisting.Where(m => !teamManagersExist.Contains(m.Id));
                        if (toBeDeletedTeamManagers != null && toBeDeletedTeamManagers.Count() > 0)
                        {
                            foreach (var item in toBeDeletedTeamManagers)
                            {
                                item.IsDeleted = true;
                                context.TeamManager.Update(item);
                            }
                        }



                    }
                    else
                    {
                        existingManager.TeamManagers = new List<TeamManager>();
                    }


                    if (managerInput.ProfilePicFile != null)
                    {
                        existingManager.ProfilePicURL = await SetManagerPhotoAsync(managerInput.ProfilePicFile);
                    }

                    existingManager.TeamManagers = TeamManagersExisting;
                    managerRepository.Update(existingManager);

                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Manager",
                        ActivityType = "Update",
                        Description = $"Manager {managerInput.Email} has been updated ",
                        Record_Id = existingManager.Id
                    });

                    await unityOfWork.SaveChangesAsync();
                }
            });
            return new ManagerResponseDTO { Id = managerInput.Id, UserId = managerInput.UserId };
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var existingManager = await managerRepository.GetAllIQueryable(mng => mng.Id == id, mng => mng.TeamManagers, mng => mng.User)
                   .FirstOrDefaultAsync();
            if (existingManager != null)
            {
                await unityOfWork.RunTransaction(async () =>
                {
                    existingManager.User.SetIsDeleted(true);
                    existingManager.SetIsDeleted(true);
                    managerRepository.Update(existingManager);

                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Manager",
                        ActivityType = "Delete",
                        Description = $"Manager {existingManager.User?.Email} has been updated ",
                        Record_Id = existingManager.Id
                    });

                    await unityOfWork.SaveChangesAsync();
                });

                return true;
            }
            return false;
        }



        private async Task<string> SetManagerPhotoAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "StaffImages";
                var processResult = await uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }
        public async Task<bool> ChangeUserPasswordAsync(string userId, string newPassword)
        {
            return await userDomainService
                   .ChangeUserPassword(userId, newPassword);
        }

    }
}
