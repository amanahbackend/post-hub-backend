﻿using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.BLL.ViewModel;
using Amanah.Posthub.BLL.ViewModels;
using FluentValidation;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Amanah.Posthub.ViewModels
{
    public class BaseManagerViewModel : BaseViewModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? CountryId { set; get; }
        public List<string> RoleNames { get; set; }
        public List<TeamManagerViewModel> TeamManagers { set; get; }

    }

    public class ManagerViewModel : BaseManagerViewModel
    {
        public int Id { set; get; }
        public string UserId { get; set; }
        public CountryResultDto Country { get; set; }
    }

    public class ManagerViewModelValidator : AbstractValidator<ManagerViewModel>
    {
        public ManagerViewModelValidator()
        {
            RuleFor(x => x.Username).NotEmpty().NotNull();
            RuleFor(x => x.FirstName).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.PhoneNumber).NotEmpty().NotNull();
            RuleFor(x => x.Email).EmailAddress().NotEmpty().NotNull();
        }
    }

    public class CreateManagerViewModel : BaseManagerViewModel
    {
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class CreateManagerViewModelValidator : AbstractValidator<CreateManagerViewModel>
    {
        public CreateManagerViewModelValidator()
        {
            RuleFor(x => x.Username).NotEmpty().NotNull();
            RuleFor(x => x.Password).NotEmpty().NotNull().MinimumLength(6).MaximumLength(100);
            RuleFor(x => x.FirstName).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.PhoneNumber).NotEmpty().NotNull();
            RuleFor(x => x.Email).EmailAddress().NotEmpty().NotNull();
        }
    }
}
