﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using GoogleMapsApi;
using GoogleMapsApi.Entities.Directions.Request;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MapsUtilities.MapsAccessor
{
    public class GoogleMapsAccessor : IMapsAccessor
    {
        private readonly string _apiKey;
        protected readonly IRepositry<MapRequestLog> _repository;
        protected readonly DbContext _context;
        private MapRequestLog _log = new MapRequestLog { Provider = "Google" };

        public GoogleMapsAccessor(string apiKey, IRepositry<MapRequestLog> repository = null, DbContext context = null)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
            {
                throw new ArgumentException("Api key cannot be empty");
            }
            _apiKey = apiKey;
            _repository = repository;
            _context = context;


            _log.ApiKey = apiKey;
        }


        public async Task<MapMinDurationOrDistanceResponse> GetMinDurationOrDistance(MapPoint from, MapPoint to)
        {
            _log.StackTrace = Environment.StackTrace;
            var resp = new MapMinDurationOrDistanceResponse();

            try
            {
                DirectionsRequest directionsRequest = new DirectionsRequest()
                {
                    Origin = $"{from.Latitude},{from.Longitude}",
                    Destination = $"{to.Latitude},{to.Longitude}",
                    ApiKey = _apiKey
                };

                _log.Request = Newtonsoft.Json.JsonConvert.SerializeObject(directionsRequest);

                var directions = await GoogleMaps.Directions.QueryAsync(directionsRequest);

                _log.Response = Newtonsoft.Json.JsonConvert.SerializeObject(directions);

                if (directions == null || !directions.Routes.Any() || !directions.Routes.First().Legs.Any())
                {
                    resp.Error = MapResponseError.RouteNotAvailable;
                }
                else
                {

                    var d = directions.Routes.Select(r => r.Legs.Sum(l => l.Duration.Value.TotalSeconds)).Min();
                    var t = Convert.ToInt32(Math.Round(d));
                    resp.Success = true;
                    resp.MinDurationOrDistance = t;
                }
            }
            catch (Exception)
            {
                resp.Error = MapResponseError.Exception;
            }

            _log.Error = resp.Error.ToString();
            _log.ErrorMessage = resp.ErrorMessage;
            _log.Success = resp.Success;

            if (_repository != null && _context != null)
            {
                _repository.AddAsync(_log);
                //await _context.SaveChangesAsync();
            }

            return resp;
        }
        public async Task<MapMinDistanceResponse> GetDistance(MapPoint from, MapPoint to)
        {
            _log.StackTrace = Environment.StackTrace;
            var resp = new MapMinDistanceResponse();
            try
            {
                DirectionsRequest directionsRequest = new DirectionsRequest()
                {
                    Origin = $"{from.Latitude},{from.Longitude}",
                    Destination = $"{to.Latitude},{to.Longitude}",
                    TravelMode = TravelMode.Driving,
                    ApiKey = _apiKey
                };

                _log.Request = Newtonsoft.Json.JsonConvert.SerializeObject(directionsRequest);

                var directions = await GoogleMaps.Directions.QueryAsync(directionsRequest);

                _log.Response = Newtonsoft.Json.JsonConvert.SerializeObject(directions);
                if (directions == null || !directions.Routes.Any() || !directions.Routes.First().Legs.Any())
                {
                    resp.Error = MapResponseError.RouteNotAvailable;
                }
                else
                {
                    var d = directions.Routes.Select(r => r.Legs.Sum(l => l.Distance.Value)).Min();

                    resp.Success = true;
                    resp.Distance = d;
                }
            }
            catch (Exception)
            {
                resp.Error = MapResponseError.Exception;
            }

            _log.Error = resp.Error.ToString();
            _log.ErrorMessage = resp.ErrorMessage;
            _log.Success = resp.Success;

            if (_repository != null && _context != null)
            {
                _repository.AddAsync(_log);
                //await _context.SaveChangesAsync();
            }

            return resp;
        }


        public async Task<MapMinDurationOrDistanceResponse> GetMinDuration(MapPoint from, MapPoint to, int DrivingMode)
        {

            _log.StackTrace = Environment.StackTrace;
            var resp = new MapMinDurationOrDistanceResponse();

            try
            {
                DirectionsRequest directionsRequest = new DirectionsRequest()
                {
                    Origin = $"{from.Latitude},{from.Longitude}",
                    Destination = $"{to.Latitude},{to.Longitude}",
                    ApiKey = _apiKey
                };

                if (DrivingMode == (int)TransportTypeEnum.Walk)
                {
                    directionsRequest.TravelMode = TravelMode.Walking;
                }
                else if (DrivingMode == (int)TransportTypeEnum.Car || DrivingMode == (int)TransportTypeEnum.Scooter)
                {
                    directionsRequest.TravelMode = TravelMode.Driving;
                }
                else if (DrivingMode == (int)TransportTypeEnum.Truck)
                {
                    directionsRequest.TravelMode = TravelMode.Driving;
                }
                else if (DrivingMode == (int)TransportTypeEnum.Scooter)
                {
                    directionsRequest.TravelMode = TravelMode.Bicycling;
                }



                _log.Request = Newtonsoft.Json.JsonConvert.SerializeObject(directionsRequest);

                var directions = await GoogleMaps.Directions.QueryAsync(directionsRequest);

                _log.Response = Newtonsoft.Json.JsonConvert.SerializeObject(directions);

                if (directions == null || !directions.Routes.Any() || !directions.Routes.First().Legs.Any())
                {
                    resp.Error = MapResponseError.RouteNotAvailable;
                }
                else
                {

                    var d = directions.Routes.Select(r => r.Legs.Sum(l => l.Duration.Value.TotalSeconds)).Min();
                    var t = Convert.ToInt32(Math.Round(d));
                    resp.Success = true;
                    resp.MinDurationOrDistance = t;
                }
            }
            catch (Exception)
            {
                resp.Error = MapResponseError.Exception;
            }

            _log.Error = resp.Error.ToString();
            _log.ErrorMessage = resp.ErrorMessage;
            _log.Success = resp.Success;

            if (_repository != null && _context != null)
            {
                _repository.AddAsync(_log);
                //await _context.SaveChangesAsync();
            }

            return resp;
        }



    }
}
