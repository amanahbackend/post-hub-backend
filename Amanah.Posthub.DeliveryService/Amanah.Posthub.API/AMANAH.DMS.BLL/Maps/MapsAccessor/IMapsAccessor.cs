﻿using MapsUtilities.Models;
using System.Threading.Tasks;

namespace MapsUtilities.MapsAccessor
{
    public interface IMapsAccessor
    {
        Task<MapMinDurationOrDistanceResponse> GetMinDurationOrDistance(MapPoint from, MapPoint to);
        Task<MapMinDistanceResponse> GetDistance(MapPoint from, MapPoint to);

        Task<MapMinDurationOrDistanceResponse> GetMinDuration(MapPoint from, MapPoint to, int DrivingMode);



    }
}
