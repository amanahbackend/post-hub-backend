﻿//using System;
//using System.IO;
//using System.Net.Http;
//using System.Threading.Tasks;
//using Itinero;
//using Itinero.IO.Osm;
//using Itinero.Osm.Vehicles;
//using MapsUtilities.Models;

//namespace MapsUtilities.MapsAccessor
//{
//    public class OpenStreetMapsAccessor : IMapsAccessor
//    {
//        public async Task<MapMinDistanceResponse> GetDistance(MapPoint from, MapPoint to)
//        {
//            var resp = new MapMinDistanceResponse();

//            try
//            {
//                var dist = await GetDistanceInternal(from, to);
//                resp.Success = true;
//                resp.Distance = dist;
//            }
//            catch (Exception ex)
//            {
//                resp.Error = MapResponseError.Exception;
//                resp.ErrorMessage = ex.Message;
//            }

//            return resp;
//        }
//        public async Task<MapMinDurationOrDistanceResponse> GetMinDurationOrDistance(MapPoint from, MapPoint to)
//        {
//            var resp = new MapMinDurationOrDistanceResponse();

//            try
//            {
//                var dist = await GetDistanceInternal(from, to);
//                resp.Success = true;
//                resp.MinDurationOrDistance = dist;
//            }
//            catch (Exception ex)
//            {
//                resp.Error = MapResponseError.Exception;
//                resp.ErrorMessage = ex.Message;
//            }

//            return resp;

//        }
//        private async Task<int> GetDistanceInternal(MapPoint from, MapPoint to)
//        {

//            var routerDb = new RouterDb();

//            await DownloadToFile("https://download.geofabrik.de/africa/egypt-latest.osm.pbf", "egypt-latest.osm.pbf");
//            using (var stream = File.OpenRead("egypt-latest.osm.pbf"))
//            {
//                routerDb.LoadOsmData(stream, Vehicle.Car);
//            }
//            var car = routerDb.GetSupportedProfile("car");
//            routerDb.AddContracted(car);
//            var router = new Router(routerDb);
//            var route = router.Calculate(car, new Itinero.LocalGeo.Coordinate(float.Parse(from.Latitude.ToString()), float.Parse(from.Longitude.ToString())),
//                new Itinero.LocalGeo.Coordinate(float.Parse(to.Latitude.ToString()), float.Parse(to.Latitude.ToString())));

//            return Convert.ToInt32(Math.Round(route.TotalDistance));

//        }

//        /// <summary>
//        /// Downloads a file if it doesn't exist yet.
//        /// </summary>
//        private static async Task DownloadToFile(string url, string filename)
//        {
//            if (!File.Exists(filename))
//            {
//                var client = new HttpClient();
//                using (var stream = await client.GetStreamAsync(url))
//                using (var outputStream = File.OpenWrite(filename))
//                {
//                    await stream.CopyToAsync(outputStream);
//                }
//            }
//        }


//        public async Task<MapMinDurationOrDistanceResponse> GetMinDuration(MapPoint from, MapPoint to, int DrivingMode)
//        {
//            var resp = new MapMinDurationOrDistanceResponse();

//            try
//            {
//                var dist = await GetDistanceInternal(from, to);
//                resp.Success = true;
//                resp.MinDurationOrDistance = dist;
//            }
//            catch (Exception ex)
//            {
//                resp.Error = MapResponseError.Exception;
//                resp.ErrorMessage = ex.Message;
//            }

//            return resp;

//        }






//    }
//}
