﻿namespace MapsUtilities.Models
{
    public enum MapResponseError
    {
        QuotaLimit = 1,
        RouteNotAvailable,
        Exception
    }
}
