﻿using Amanah.Posthub.BLL.Notifications.DTOs;
using Amanah.Posthub.BLL.Notifications.Services;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Notifications.BackGroundJobs
{

    public interface IBackGroundNotificationJob : IBackgroundJob<BackGroundNotificationSercviceDTO>
    {

    }

    public class BackGroundNotificationJob : IBackGroundNotificationJob
    {
        private readonly INotificationSenderService _sendNotificationService;

        public BackGroundNotificationJob(INotificationSenderService sendNotificationService)
        {
            _sendNotificationService = sendNotificationService;
        }
        public async Task ExecuteAsync(BackGroundNotificationSercviceDTO jobModel)
        {
            await _sendNotificationService.ExcuteNotificationAsync(jobModel);
        }
    }

}
