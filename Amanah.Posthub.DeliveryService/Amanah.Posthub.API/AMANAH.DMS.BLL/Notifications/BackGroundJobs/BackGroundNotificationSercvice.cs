﻿using Amanah.Posthub.BLL.Notifications.DTOs;
using Amanah.Posthub.SharedKernel.BackgroundJobs;

namespace Amanah.Posthub.BLL.Notifications.BackGroundJobs
{
    public interface IBackGroundNotificationSercvice
    {
        public void SendToUserManagers(string taskTenantId, string userId, string notificationName, string title, string message, object payload);
        public void SendToUserManagers(string userId, string notificationName, string title, string message, object payload);
        public void BroadcastToTenant(string userId, string notificationName, string title, string message, object payload);
        public void SendToUserAndTenantAdmin(string userId, string notificationName, string title, string message, object payload);
        public void SendToUser(string userId, string notificationName, string title, string message, object payload);
    }


    public class BackGroundNotificationSercvice : IBackGroundNotificationSercvice
    {
        private readonly IBackgroundJobManager _jobManager;

        public BackGroundNotificationSercvice(
             IBackgroundJobManager jobManager
            )
        {
            _jobManager = jobManager;
        }

        public void BroadcastToTenant(string userId, string notificationName, string title, string message, object payload)
        {
            _jobManager.Enqueue(new BackGroundNotificationSercviceDTO(BackgroundNotificationTypeEnum.BroadcastToTenantAsync, userId, notificationName, title, message, payload));
        }

        public void SendToUserAndTenantAdmin(string userId, string notificationName, string title, string message, object payload)
        {
            _jobManager.Enqueue(new BackGroundNotificationSercviceDTO(BackgroundNotificationTypeEnum.SendToUserAndTenantAdminAsync, userId, notificationName, title, message, payload));
        }

        public void SendToUser(string userId, string notificationName, string title, string message, object payload)
        {
            _jobManager.Enqueue(new BackGroundNotificationSercviceDTO(BackgroundNotificationTypeEnum.SendToUserAsync, userId, notificationName, title, message, payload));
        }

        public void SendToUserManagers(string taskTenantId, string userId, string notificationName, string title, string message, object payload)
        {
            _jobManager.Enqueue(new BackGroundNotificationSercviceDTO(BackgroundNotificationTypeEnum.SendToUserManagersAsync, taskTenantId, userId, notificationName, title, message, payload));
        }

        public void SendToUserManagers(string userId, string notificationName, string title, string message, object payload)
        {
            _jobManager.Enqueue(new BackGroundNotificationSercviceDTO(BackgroundNotificationTypeEnum.SendToUserManagersAsync, userId, notificationName, title, message, payload));
        }
    }

}
