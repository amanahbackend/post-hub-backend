﻿namespace Amanah.Posthub.BLL.Notifications.DTOs
{
    public class BackGroundNotificationSercviceDTO
    {
        public string TaskTenantId { get; set; }
        public string UserId { get; set; }
        public string NotificationName { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public object Payload { get; set; }
        public BackgroundNotificationTypeEnum BackgroundNotificationType { get; set; }

        public BackGroundNotificationSercviceDTO(BackgroundNotificationTypeEnum backgroundNotificationType, string taskTenantId, string userId, string notificationName, string title, string message, object payload)
            : this(backgroundNotificationType, userId, notificationName, title, message, payload)
        {
            TaskTenantId = taskTenantId;
        }
        public BackGroundNotificationSercviceDTO(BackgroundNotificationTypeEnum backgroundNotificationType, string userId, string notificationName, string title, string message, object payload) : this()
        {
            UserId = userId;
            NotificationName = notificationName;
            Title = title;
            Message = message;
            Payload = payload;
            BackgroundNotificationType = backgroundNotificationType;
        }
        public BackGroundNotificationSercviceDTO()
        {

        }

    }
}
