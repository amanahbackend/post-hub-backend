﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.ViewModel;

namespace Amanah.Posthub.BLL.Notifications.DTOs
{

    public class NotificationResultDTO : BaseViewModel
    {
        public int Id { get; set; }
        public NotificationTypeWeb? NotificationType { get; set; }
        public string Body { get; set; }
        public string Title { get; set; }
        public bool IsSeen { get; set; }
        public string ToUserId { get; set; }
        public string FromUserId { get; set; }


    }
}
