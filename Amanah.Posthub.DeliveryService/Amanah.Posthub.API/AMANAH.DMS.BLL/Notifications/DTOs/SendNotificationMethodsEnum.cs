﻿namespace Amanah.Posthub.BLL.Notifications.DTOs
{
    public enum BackgroundNotificationTypeEnum
    {
        SendToUserManagersAsync_TaskTenantId,
        SendToUserManagersAsync,
        BroadcastToTenantAsync,
        SendToUserAndTenantAdminAsync,
        SendToUserAsync
    }
}
