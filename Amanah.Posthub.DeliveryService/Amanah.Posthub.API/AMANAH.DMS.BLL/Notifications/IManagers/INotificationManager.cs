﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Notifications.Entities;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface INotificationManager : IBaseManager<NotificationViewModel, Notification>
    {
        //public Task<PagedResult<NotificationViewModel>> GetAllByPaginationAsyncForUser(NotificatonFilterInputDTO pagingparametermodel);
        Task<NotificationViewModel> Get(int id);
        Task<TQueryModel> Get<TQueryModel>(int id);
        Task<bool> MarkAsReadsync(string userId);
        Task<int> UnReadCountsync(string userId);

    }


}
