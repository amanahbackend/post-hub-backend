﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Notifications.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    internal class NotificationManager : BaseManager<NotificationViewModel, Notification>, INotificationManager
    {
        public NotificationManager(
            ApplicationDbContext context,
            IRepositry<Notification> repository,
            IMapper mapper)
            : base(context, repository, mapper)
        {
        }

        public async Task<NotificationViewModel> Get(int id)
        {
            var notifications = await repository.GetAll().FirstOrDefaultAsync(x => x.Id == id);

            return mapper.Map<Notification, NotificationViewModel>(notifications);
        }

        public async Task<TQueryModel> Get<TQueryModel>(int id)
        {
            var notifications = await repository.GetAll().SingleOrDefaultAsync(x => x.Id == id);

            return mapper.Map<Notification, TQueryModel>(notifications);
        }


        public async Task<bool> MarkAsReadsync(string userId)
        {
            try
            {
                var notifications = await context.Notifications
                    .Where(x => x.ToUserId == userId)
                    .ToListAsync();

                foreach (var noti in notifications)
                {
                    noti.IsSeen = true;
                    var r = context.Notifications.Update(noti);

                }
                await context.SaveChangesAsync();

                return true;
            }
            catch
            {
                return false;
            }

        }

        public async Task<int> UnReadCountsync(string userId)
        {
            try
            {
                var UnReadCount = await context.Notifications
                    .Where(x => x.ToUserId == userId && x.IsSeen == false)
                    .CountAsync();

                return UnReadCount;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}


