﻿using Amanah.Posthub.BLL.Notifications.DTOs;
using Amanah.Posthub.DeliverService.BLL.Notifications.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Notifications.Queries
{
    public interface INotificationQuery
    {
        public Task<PagedResult<NotificationResultDTO>> GetAllByPaginationAsyncForUserAsync(NotificatonFilterInputDTO pagingparametermodel);
        public Task<NotificationResultDTO> GetAsync(int id);
        public Task<List<NotificationResultDTO>> GetAllAsync();
        public Task<int> GetUnReadCountAsync(string userId);
    }
}
