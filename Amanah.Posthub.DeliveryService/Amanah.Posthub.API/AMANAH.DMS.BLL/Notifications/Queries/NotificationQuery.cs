﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.Notifications.DTOs;
using Amanah.Posthub.BLL.Notifications.Queries;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.DeliverService.BLL.Notifications.DTOs;
using Amanah.Posthub.Service.Domain.Notifications.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Notifications.Services
{

    public class NotificationQuery : INotificationQuery
    {
        private readonly IRepositry<Notification> _notificationRepositry;
        private readonly IMapper _mapper;

        public NotificationQuery(
            IRepositry<Notification> notificationRepositry,
            IMapper mapper
           )
        {
            _notificationRepositry = notificationRepositry;
            _mapper = mapper;
        }

        public async Task<PagedResult<NotificationResultDTO>> GetAllByPaginationAsyncForUserAsync(NotificatonFilterInputDTO pagingparametermodel)

        {
            var pagedResult = await _notificationRepositry.AsQueryable()
                .Where(x => x.ToUserId == pagingparametermodel.UserID)
                .ProjectTo<NotificationResultDTO>(_mapper.ConfigurationProvider)
                .ToPagedResultAsync(pagingparametermodel);

            return pagedResult;
        }

        public async Task<NotificationResultDTO> GetAsync(int id)
        {
            var notifications = await _notificationRepositry.GetAsync(x => x.Id == id);
            return _mapper.Map<Notification, NotificationResultDTO>(notifications);
        }

        public async Task<List<NotificationResultDTO>> GetAllAsync()
        {
            return await _notificationRepositry.AsQueryable().ProjectTo<NotificationResultDTO>(_mapper.ConfigurationProvider).ToListAsync();
        }

        public async Task<int> GetUnReadCountAsync(string userId)
        {
            return await _notificationRepositry.AsQueryable().Where(x => x.ToUserId == userId && !x.IsSeen).CountAsync();
        }
    }
}
