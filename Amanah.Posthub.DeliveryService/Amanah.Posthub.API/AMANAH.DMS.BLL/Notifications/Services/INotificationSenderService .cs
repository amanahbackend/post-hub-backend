﻿using Amanah.Posthub.BLL.Notifications.DTOs;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Notifications.Services
{
    public interface INotificationSenderService
    {
        Task ExcuteNotificationAsync(BackGroundNotificationSercviceDTO notification);
    }
}
