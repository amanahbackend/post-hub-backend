﻿using Amanah.Posthub.Service.Domain.Notifications.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Notifications.Services
{
    public interface INotificationService
    {
        Task<bool> MarkAsReadAsync([NotNull] Expression<Func<Notification, bool>> predicate);
        Task<bool> MarkAsReadAsync(List<int> notificationIdslist);
        Task<bool> MarkAsReadAsync(string userId);
        Task AddAsync(Notification notification);
    }
}
