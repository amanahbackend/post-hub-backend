﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Notifications.Repositories;
using Amanah.Posthub.Service.Domain.Notifications.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
//using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Notifications.Services
{

    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _notificationRepositry;
        private readonly IUnitOfWork _unityOfWork;
        private readonly ApplicationDbContext _dbContext;


        public NotificationService(
            INotificationRepository notificationRepositry,
            IUnitOfWork unityOfWork,
            ApplicationDbContext dbContext
                   )
        {
            _notificationRepositry = notificationRepositry;
            _unityOfWork = unityOfWork;
            _dbContext = dbContext;
        }


        public async Task<bool> MarkAsReadAsync([NotNull] Expression<Func<Notification, bool>> predicate)
        {
            var notificationsToUpdate = _notificationRepositry.GetAllIQueryable().Where(predicate);
            var isSuccess = false;
            await _unityOfWork.RunTransaction(async () =>
            {
                foreach (var notification in notificationsToUpdate)
                {
                    notification.IsSeen = true;
                    _notificationRepositry.Update(notification);
                }
                var changes = await _unityOfWork.SaveChangesAsync();
                isSuccess = changes > 0;
            });
            return isSuccess;
        }

        public async Task<bool> MarkAsReadAsync(List<int> notificationIdslist)
            => await MarkAsReadAsync(notification => notificationIdslist.Contains(notification.Id));

        public async Task<bool> MarkAsReadAsync(string userId)
            => await MarkAsReadAsync(notification => notification.ToUserId == userId);


        public async Task AddAsync(Notification notification)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                _notificationRepositry.Add(notification);
                await _unityOfWork.SaveChangesAsync();
            });
        }

    }
}
