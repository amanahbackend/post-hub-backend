﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BASE.Services;
using Amanah.Posthub.BLL.Notifications.DTOs;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.Service.Domain.Notifications.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Notifications.Services
{
    /*
     * remaining issues
     * 1 - Title, message re literals and should not be translated, solutions
     *      - just send notificationName and payload and the front-ent has to build notification
     *      - receive the key of message, title and this service will translate according to the language of receivers
     *      
     * 2 - the title of the push notification is hard-coded it should be configured in the tenant (Admin entity)
     * 
     */
    public class NotificationSenderService : INotificationSenderService
    {
        private readonly INotificationService _notificationService;
        private readonly IFirebasePushNotificationService _pushNotificationService;
        private readonly IRealTimeNotifier _realTimeNotifier;
        private readonly ICurrentUser _currentUser;
        private readonly IUserRepository _userRepository;
        private readonly IDriverRepository _driverRepository;
        private readonly IRepository<Manager> _managerRepository;


        public NotificationSenderService(
            INotificationService notificationService,
            IRealTimeNotifier realTimeNotifier,
            IFirebasePushNotificationService pushNotificationService,
            ICurrentUser currentUser,
            IUserRepository userRepository,
            IDriverRepository driverRepository,
            IRepository<Manager> managerRepository
            )
        {
            _notificationService = notificationService;
            _realTimeNotifier = realTimeNotifier;
            _pushNotificationService = pushNotificationService;
            _currentUser = currentUser;
            _userRepository = userRepository;
            _driverRepository = driverRepository;
            _managerRepository = managerRepository;


        }



        public async Task ExcuteNotificationAsync(BackGroundNotificationSercviceDTO notification)
        {

            switch (notification.BackgroundNotificationType)
            {
                case BackgroundNotificationTypeEnum.SendToUserManagersAsync_TaskTenantId:
                    await SendToUserManagersAsync(notification.TaskTenantId,
                        notification.UserId,
                        notification.NotificationName,
                        notification.Title,
                        notification.Message,
                        notification.Payload);
                    return;
                case BackgroundNotificationTypeEnum.SendToUserManagersAsync:
                    await SendToUserManagersAsync(
                      notification.UserId,
                      notification.NotificationName,
                      notification.Title,
                      notification.Message,
                      notification.Payload);
                    return;
                case BackgroundNotificationTypeEnum.BroadcastToTenantAsync:
                    await BroadcastToTenantAsync(
                    notification.UserId,
                    notification.NotificationName,
                    notification.Title,
                    notification.Message,
                    notification.Payload);
                    return;
                case BackgroundNotificationTypeEnum.SendToUserAndTenantAdminAsync:
                    await SendToUserAndTenantAdminAsync(
                   notification.UserId,
                   notification.NotificationName,
                   notification.Title,
                   notification.Message,
                   notification.Payload);
                    return;
                case BackgroundNotificationTypeEnum.SendToUserAsync:
                    await SendToUserAsync(
                 notification.UserId,
                 notification.NotificationName,
                 notification.Title,
                 notification.Message,
                 notification.Payload);
                    return;
                default:
                    throw new NotImplementedException("add case here for the new BackgroundNotificationTypeEnum");
            }
        }


        protected async Task SendToUserManagersAsync(string taskTenantId,
           string userId,
           string notificationName,
           string title,
           string message,
           object payload)
        {
            var userTenantId = await GetUserTenantIdAsync(userId);
            await SendToUserManagersAsync(userId, notificationName, title, message, payload);
            if (userTenantId != taskTenantId)
            {
                await SendToTenantAdminAsync(userId, notificationName, title, message, payload, taskTenantId);
                //need get all active tentant managers userId !uerId
                var taskTenantManagers = await _managerRepository.GetAllIQueryable()
                            .IgnoreQueryFilters()
                            .Where(manager => manager.Tenant_Id == taskTenantId && !manager.IsDeleted)
                            .Select(manager => manager.UserId)
                            .ToListAsync();

                if (taskTenantManagers != null && taskTenantManagers.Any())
                {
                    foreach (var managerUserId in taskTenantManagers)
                    {
                        await InternalSendToUserAsync(managerUserId, notificationName, title, message, payload, taskTenantId, fromUserId: userId);
                    }
                }
            }
        }

        protected async Task SendToUserManagersAsync(string userId,
            string notificationName,
            string title,
            string message,
            object payload)
        {
            var driver = await _driverRepository.GetByUserIdAsync(userId);
            string tenant_Id = driver != null? driver.Tenant_Id:"";

            await SendToTenantAdminAsync(userId, notificationName, title, message, payload, tenant_Id);
            if (driver != null)
            {
                var driverManagersQuarable = await _driverRepository.GetDriverManagersByTeamIdAsync(driver.TeamId);
                var driverManagersUserIds = await driverManagersQuarable.Select(manager => manager.UserId).ToListAsync();
                foreach (var managerUserId in driverManagersUserIds)
                {
                    await InternalSendToUserAsync(managerUserId, notificationName, title, message, payload, tenant_Id, fromUserId: userId);
                }
            }
        }

        protected async Task SendToUserAsync(string userId,
            string notificationName,
            string title,
            string message,
            object payload)
        {
            bool isDriver = await _driverRepository.IsDriverAsync(userId);
            if (isDriver)
            {
                await _pushNotificationService.SendAsync(userId, title, message, payload);
            }
            else
            {
                var tenantId = await GetUserTenantIdAsync(userId);
                await InternalSendToUserAsync(userId, notificationName, title, message, payload, tenantId);
            }
        }

        protected async Task SendToUserAndTenantAdminAsync(string userId,
            string notificationName,
            string title,
            string message,
            object payload)
        {
            var tenantId = await GetUserTenantIdAsync(userId);
            await SendToTenantAdminAsync(userId, notificationName, title, message, payload, tenantId);
            var isNotTenantAdmin = userId != tenantId && userId != NotificationGroupNames.PlatformSuperAdmin;
            if (isNotTenantAdmin)
            {
                await InternalSendToUserAsync(userId, notificationName, title, message, payload, tenantId);
            }
        }

        protected async Task BroadcastToTenantAsync(string userId,
            string notificationName,
            string title,
            string message,
            object payload)
        {
            string tenant_Id = await GetUserTenantIdAsync(userId);
            var boradcastGroupName = string.IsNullOrEmpty(tenant_Id)
                ? NotificationGroupNames.Platform
                : $"Tenant_{tenant_Id}";
            await _realTimeNotifier.SendAsync(boradcastGroupName, notificationName, message, payload);
        }

        private async Task InternalSendToUserAsync(string userId,
            string notificationName,
            string title,
            string message,
            object payload,
            string tenantId,
            string fromUserId = null)
        {
            await _realTimeNotifier.SendAsync(userId, notificationName, message, payload);
            await _notificationService.AddAsync(new Notification(tenantId)
            {
                Title = title,
                Body = message,
                FromUserId = fromUserId ?? _currentUser.Id,
                ToUserId = userId,
            });
        }

        private async Task SendToTenantAdminAsync(string userId,
           string notificationName,
           string title,
           string message,
           object payload,
           string tenant_Id)
        {
            var tenantAdminId = string.IsNullOrEmpty(tenant_Id) ? NotificationGroupNames.PlatformSuperAdmin : tenant_Id;
            await InternalSendToUserAsync(tenantAdminId, notificationName, title, message, payload, tenant_Id, fromUserId: userId);
        }

        private async Task<string> GetUserTenantIdAsync(string userId)
        {
            return await _userRepository.GetAllIQueryable()
                            .Where(user => user.Id == userId && !user.IsDeleted)
                            .Select(user => user.Tenant_Id)
                            .FirstOrDefaultAsync();
        }






























    }
}
