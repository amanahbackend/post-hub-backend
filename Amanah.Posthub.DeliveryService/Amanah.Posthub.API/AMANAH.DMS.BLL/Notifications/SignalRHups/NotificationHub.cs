﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Notifications.Services;
using Amanah.Posthub.BLL.UserManagement.Permissions;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.API.SignalRHups
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class NotificationHub : Hub
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IManagerManager _managerManager;

        public NotificationHub(
            ApplicationDbContext dbContext,
            IManagerManager managerManager)
        {
            _dbContext = dbContext;
            _managerManager = managerManager;
        }

        public async override Task OnConnectedAsync()
        {
            var tenantId = Context.User.FindFirst(CustomClaimTypes.TenantId)?.Value;
            var scope = Context.User.FindFirst(CustomClaimTypes.Scope)?.Value;
            var tenantBroadCastGroup = scope == Scope.Platform
                ? NotificationGroupNames.Platform
                : "Tenant_" + tenantId;

            await Groups.AddToGroupAsync(Context.ConnectionId, tenantBroadCastGroup);
            await Groups.AddToGroupAsync(Context.ConnectionId, Context.UserIdentifier);

            await base.OnConnectedAsync();
        }

        public async Task UpdateCurrentLocationAsync(string driverId, double longitude, double latitude)
        {
            var driver = _dbContext.Driver.Where(x => x.UserId == driverId).FirstOrDefault();
            if (driver != null)
            {
                var OldLocation = _dbContext.DriverCurrentLocation.Where(x => x.Id == driver.Id).FirstOrDefault();
                if (OldLocation == null)
                {
                    _dbContext.DriverCurrentLocation.Add(
                        new DriverCurrentLocation(driver.Tenant_Id)
                        {
                            Id = driver.Id,
                            Longitude = longitude,
                            Latitude = latitude,
                        });
                }
                else
                {
                    OldLocation.Longitude = longitude;
                    OldLocation.Latitude = latitude;
                    _dbContext.DriverCurrentLocation.Update(OldLocation);
                }

                await _dbContext.SaveChangesAsync();

                var tenantAdminId = driver.Tenant_Id ?? NotificationGroupNames.PlatformSuperAdmin;
                IEnumerable<Task> sendTasks = new List<Task>();
                List<string> driverManagerUserIds = new List<string> { tenantAdminId };

                var driverManagers = await _managerManager.GetDriverManagersAsync(driver.Id);
                if (driverManagers != null && driverManagers.Any())
                {
                    driverManagerUserIds.AddRange(driverManagers.Select(manager => manager.UserId).ToList());
                }

                if (string.IsNullOrEmpty(driver.Tenant_Id))
                {
                    var tenantIds = await GetPlatformDriverCurrentWorkingTenantAsync(driver.Id);
                    if (tenantIds != null && tenantIds.Any())
                    {
                        driverManagerUserIds.AddRange(tenantIds);
                        var managerIds = await GetTenantManagerAsync(tenantIds);
                        if (managerIds != null && managerIds.Any())
                        {
                            driverManagerUserIds.AddRange(managerIds);
                        }
                    }
                }

                sendTasks = driverManagerUserIds.Select(
                   userId => Clients.Group(userId).SendAsync("LocationChanged", driverId, longitude, latitude));

                await Task.WhenAll(sendTasks);
            }
        }

        private async Task<List<string>> GetPlatformDriverCurrentWorkingTenantAsync(int driverId)
        {
            var notCompletedList = new List<int>
            {
                (int)TaskStatusEnum.Assigned,
                (int)TaskStatusEnum.Accepted,
                (int) TaskStatusEnum.Started,
                (int)TaskStatusEnum.Inprogress
            };

            return await _dbContext.Tasks
                 .IgnoreQueryFilters()
                 .Where(task => notCompletedList.Contains(task.TaskStatusId))
                 .Where(task => !task.IsDeleted)
                 .Where(task => task.DriverId == driverId)
                 .Where(task => !string.IsNullOrEmpty(task.Tenant_Id))
                 .Select(task => task.Tenant_Id)
                 .Distinct()
                 .ToListAsync();
        }

        private async Task<List<string>> GetTenantManagerAsync(List<string> tenantIds)
        {
            return await _dbContext.Manager
                   .IgnoreQueryFilters()
                   .Where(manager => !manager.IsDeleted)
                   .Where(manager => tenantIds.Contains(manager.Tenant_Id))
                   .Select(manager => manager.UserId)
                   .Distinct()
                   .ToListAsync();
        }
    }
}
