﻿using FluentValidation;
using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class CreateNotificationViewModel
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set; }
        public string ToUserId { get; set; }
        public string FromUserId { get; set; }
    }

    public class CreateNotificationViewModelValidator : AbstractValidator<CreateNotificationViewModel>
    {
        public CreateNotificationViewModelValidator()
        {
            RuleFor(x => x.FromUserId).NotEmpty().NotNull().MaximumLength(450);
            RuleFor(x => x.ToUserId).NotEmpty().NotNull().MaximumLength(450);
            RuleFor(x => x.Date).NotEmpty().NotNull();
            RuleFor(x => x.Body).NotNull().NotEmpty();
        }
    }
}
