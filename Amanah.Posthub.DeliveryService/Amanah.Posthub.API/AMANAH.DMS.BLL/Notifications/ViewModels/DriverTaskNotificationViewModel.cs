﻿using FluentValidation;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class DriverTaskNotificationViewModel
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int DriverId { set; get; }
        public string Description { set; get; }
        public TasksViewModel Tasks { set; get; }
    }

    public class DriverTaskNotificationViewModelValidator : AbstractValidator<DriverTaskNotificationViewModel>
    {
        public DriverTaskNotificationViewModelValidator()
        {
            RuleFor(x => x.TaskId).NotEmpty().NotNull();
            RuleFor(x => x.DriverId).NotEmpty().NotNull();
        }
    }
}
