﻿using System;

namespace Amanah.Posthub.BLL.ViewModels.Mobile
{
    public class DriverTaskNotificationResultDTO
    {
        public int? TaskStatusId { set; get; }
        public string TaskStatusName { set; get; }
        public DateTime? StartDate { get; set; }
        public DateTime? SuccessfulDate { get; set; }
        public DateTime? TaskDate { get; set; }

        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }

        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
    }
}
