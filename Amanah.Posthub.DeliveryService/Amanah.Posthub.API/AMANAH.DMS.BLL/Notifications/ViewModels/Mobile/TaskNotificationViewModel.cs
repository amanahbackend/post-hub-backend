﻿namespace Amanah.Posthub.BLL.ViewModels.Mobile
{
    public class TaskNotificationViewModel : TaskHistoryDetailsViewModel
    {
        public string ActionStatus { get; set; }
    }
}
