﻿namespace Amanah.Posthub.BLL.PriceLists.Queries.DTOs
{
    public class RFQFilterDTO
    {
        public int TotalNumbers { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchBy { get; set; }

        public int Id { get; set; }

    }
}
