﻿namespace Amanah.Posthub.BLL.PriceLists.Queries.DTOs
{
    public class RFQResponseDTO
    {
        public string CustomerName { get; set; }
        public string RFQDescription { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
    }


}
