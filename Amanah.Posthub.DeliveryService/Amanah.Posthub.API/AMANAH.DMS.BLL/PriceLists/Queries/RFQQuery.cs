﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.PriceLists.Queries.DTOs;
using Amanah.Posthub.Context;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.PriceLists.Queries
{
    public interface IRFQQuery
    {
        Task<List<RFQResponseDTO>> GetAllAsync();
        Task<PagedResult<RFQResponseDTO>> GetByPaginationAsync(RFQFilterDTO filter);
        // Task<ManagerResponseDTO> GetAsync(int id);

        //   Task<List<ExportManagerResponseDTO>> ExportManagersAsync(ManagerFilterDTO filter);

    }
    public class RFQQuery : IRFQQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public RFQQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        public async Task<List<RFQResponseDTO>> GetAllAsync()
        {
            var rFQs = await context.Manager
                 .IgnoreQueryFilters()
                 .ProjectTo<RFQResponseDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();
            //managers.ForEach(manager => manager.TeamManagers = manager.TeamManagers.Where(team => !team.IsDeleted).ToList());

            return rFQs;
        }

        public async Task<PagedResult<RFQResponseDTO>> GetByPaginationAsync(RFQFilterDTO filter)
        {
            PagedResult<RFQResponseDTO> result = new PagedResult<RFQResponseDTO>();
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;

            var query = context.RFQ.AsQueryable().IgnoreQueryFilters();

            var managers = await query
                 .ProjectTo<RFQResponseDTO>(mapper.ConfigurationProvider)
                 .Skip((filter.PageNumber - 1) * filter.PageSize).Take(filter.PageSize)
                 .ToListAsync();

            result.TotalCount = await query.CountAsync();
            result.Result = managers;

            return result;
        }

        //public async Task<ManagerResponseDTO> GetAsync(int id)
        //{
        //    var manager = await context.Manager
        //        .Where(mng => mng.Id == id)
        //        .ProjectTo<ManagerResponseDTO>(mapper.ConfigurationProvider)
        //        .FirstOrDefaultAsync();

        //    var managerRoleNames = await (from roles in context.Roles
        //                                  join userRoles in context.UserRoles on roles.Id equals userRoles.RoleId
        //                                  where manager.UserId == userRoles.UserId
        //                                  select new
        //                                  {
        //                                      UserId = userRoles.UserId,
        //                                      RoleName = roles.Name
        //                                  }).ToListAsync();

        //    manager.TeamManagers = manager.TeamManagers.Where(team => !team.IsDeleted).ToList();
        //    manager.RoleNames = managerRoleNames?.Where(mng => mng.UserId == manager.UserId)
        //        .Select(role => role.RoleName)
        //        .Distinct()
        //        .ToList();

        //    return manager;
        //}



    }
}
