﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;

namespace Amanah.Posthub.BLL.Restaurants.Queries.DTOs
{
    public class RestaurantFilterDTO : PaginatedItemsViewModel
    {
        public bool IncludeActiveOnly { get; set; }
    }
}
