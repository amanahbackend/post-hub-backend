﻿using System;

namespace Amanah.Posthub.BLL.Restaurants.Queries.DTOs
{
    public class RestaurantResponseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Reason { get; set; }

        public string Tenant_Id { get; set; }
        //public string CreatedBy_Id { get; set; }
        //public string UpdatedBy_Id { get; set; }
        //public string DeletedBy_Id { get; set; }
        //public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        //public DateTime DeletedDate { get; set; }
    }
}
