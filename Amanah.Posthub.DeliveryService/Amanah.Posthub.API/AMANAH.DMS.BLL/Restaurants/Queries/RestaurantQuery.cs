﻿using Amanah.Posthub.BLL.Restaurants.Queries.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Restaurants.Queries
{
    public interface IRestaurantQuery
    {
        Task<List<RestaurantResponseDTO>> GetAllAsync(List<int> restaurantIds = null);
        Task<PagedResult<RestaurantResponseDTO>> GetAllByPaginationAsync(RestaurantFilterDTO pagingparametermodel, List<int> restaurantIds);
        Task<RestaurantResponseDTO> GetAsync(int id);

    }
    internal class RestaurantQuery : IRestaurantQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public RestaurantQuery(
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<List<RestaurantResponseDTO>> GetAllAsync(List<int> restaurantIds = null)
        {
            var query = context.Restaurant.Where(restaurant => restaurant.IsActive);
            if (restaurantIds != null && restaurantIds.Any())
            {
                query = query.Where(restaurant => restaurantIds.Contains(restaurant.Id));
            }

            return await query
                .ProjectTo<RestaurantResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<PagedResult<RestaurantResponseDTO>> GetAllByPaginationAsync(
            RestaurantFilterDTO filter,
            List<int> restaurantIds)
        {
            var query = context.Restaurant.AsQueryable();
            if (filter.IncludeActiveOnly)
            {
                query = query.Where(restaurant => restaurant.IsActive);
            }
            if (restaurantIds != null && restaurantIds.Any())
            {
                query = query.Where(restaurant => restaurantIds.Contains(restaurant.Id));
            }

            return query
                .ToPagedResultAsync<Restaurant, RestaurantResponseDTO>(filter, mapper.ConfigurationProvider);
        }

        public async Task<RestaurantResponseDTO> GetAsync(int id)
        {
            return await context.Restaurant
                .ProjectTo<RestaurantResponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

    }
}
