﻿using FluentValidation;

namespace Amanah.Posthub.BLL.Restaurants.Services.DTOs
{
    public class CreateRestaurantInputDTO
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public class CreateRestaurantInputDTOValidation : AbstractValidator<CreateRestaurantInputDTO>
        {
            public CreateRestaurantInputDTOValidation()
            {
                RuleFor(restaurant => restaurant.Name).NotEmpty().NotNull().MaximumLength(500);
            }

        }
    }
}
