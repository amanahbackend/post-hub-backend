﻿using FluentValidation;

namespace Amanah.Posthub.BLL.Restaurants.Services.DTOs
{
    public class UpdateRestaurantInputDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public class UpdateRestaurantInputDTOValidation : AbstractValidator<UpdateRestaurantInputDTO>
        {
            public UpdateRestaurantInputDTOValidation()
            {
                RuleFor(restaurant => restaurant.Id)
                   .NotEmpty()
                   .NotNull();
                RuleFor(restaurant => restaurant.Name)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(500);
            }

        }
    }
}
