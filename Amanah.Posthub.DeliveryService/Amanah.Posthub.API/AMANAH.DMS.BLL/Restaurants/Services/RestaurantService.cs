﻿using Amanah.Posthub.BLL.Restaurants.Services.DTOs;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Restaurants.Services
{
    public interface IRestaurantService
    {
        Task CreateAsync(CreateRestaurantInputDTO createRestaurant);
        Task UpdateAsync(UpdateRestaurantInputDTO updateRestaurant);
        Task DeleteAsync(int id);
        Task ActivateOrDeActivateAsync(int id, bool isActive, string Reason);
    }
    internal class RestaurantService : IRestaurantService
    {
        private readonly IRepository<Restaurant> restaurantRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public RestaurantService(IRepository<Restaurant> restaurantRepository,
            IRepository<AccountLogs> accountLogRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            this.restaurantRepository = restaurantRepository;
            this.accountLogRepository = accountLogRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task CreateAsync(CreateRestaurantInputDTO createRestaurant)
        {
            await unitOfWork.RunTransaction(async () =>
            {
                var restaurant = mapper.Map<Restaurant>(createRestaurant);
                restaurant.IsActive = true;
                restaurantRepository.Add(restaurant);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Restaurant",
                    ActivityType = "Create",
                    Description = $"Restaurant {restaurant.Name } has been created ",
                    Record_Id = restaurant.Id
                });

                await unitOfWork.SaveChangesAsync();
            });
        }
        public async Task UpdateAsync(UpdateRestaurantInputDTO updateRestaurant)
        {
            var existingRestaurant = await restaurantRepository.GetFirstOrDefaultAsync(restaurant => restaurant.Id == updateRestaurant.Id);
            if (existingRestaurant == null)
            {
                return;
            }
            await unitOfWork.RunTransaction(async () =>
            {
                existingRestaurant = mapper.Map(updateRestaurant, existingRestaurant);
                restaurantRepository.Update(existingRestaurant);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Restaurant",
                    ActivityType = "Update",
                    Description = $"Restaurant {existingRestaurant.Name } has been updated ",
                    Record_Id = existingRestaurant.Id
                });

                await unitOfWork.SaveChangesAsync();
            });
        }
        public async Task DeleteAsync(int id)
        {
            var existingRestaurant = await restaurantRepository.GetFirstOrDefaultAsync(restaurant => restaurant.Id == id);
            if (existingRestaurant == null)
            {
                return;
            }
            await unitOfWork.RunTransaction(async () =>
            {
                existingRestaurant.SetIsDeleted(true);
                restaurantRepository.Update(existingRestaurant);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Restaurant",
                    ActivityType = "Delete",
                    Description = $"Restaurant {existingRestaurant.Name } has been deleted ",
                    Record_Id = existingRestaurant.Id
                });

                await unitOfWork.SaveChangesAsync();
            });
        }
        public async Task ActivateOrDeActivateAsync(int id, bool isActive, string Reason)
        {
            var existingRestaurant = await restaurantRepository.GetFirstOrDefaultAsync(restaurant => restaurant.Id == id
                , restaurant => restaurant.Branchs);
            if (existingRestaurant == null)
            {
                return;
            }
            await unitOfWork.RunTransaction(async () =>
            {
                existingRestaurant.IsActive = isActive;
                existingRestaurant.Reason = Reason;

                if (!isActive)
                {
                    foreach (var branch in existingRestaurant.Branchs.ToList())
                    {
                        branch.IsActivePrevious = branch.IsActive;
                        branch.IsActive = isActive;
                    }
                }
                else
                {
                    foreach (var branch in existingRestaurant.Branchs.ToList())
                    {
                        var branchPreviousState = branch.IsActivePrevious;
                        if (!branch.IsActivePrevious)
                        {
                            branch.IsActive = branchPreviousState;
                        }
                        else
                        {
                            branch.IsActive = isActive;
                        }
                        branch.IsActivePrevious = branch.IsActive;
                    }
                }

                restaurantRepository.Update(existingRestaurant);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Restaurant",
                    ActivityType = "SetActivate",
                    Description = isActive
                    ? $"Restaurant {existingRestaurant.Name } has been activated "
                    : $"Restaurant {existingRestaurant.Name } has been deactivated ",
                    Record_Id = existingRestaurant.Id
                });

                await unitOfWork.SaveChangesAsync();
            });
        }



    }
}
