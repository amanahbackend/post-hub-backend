﻿using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface ISettingsManager : IBaseManager<SettingViewModel, Setting>
    {
        Task<int> GetAutoAllocationTypeidAsync();
        Task<List<SettingViewModel>> GetSettingByGroupIdAsync(string key);
        Task<SettingViewModel> GetSettingByKeyAsync(string key);
        Task<SettingViewModel> GetAsync(int id);

    }
}
