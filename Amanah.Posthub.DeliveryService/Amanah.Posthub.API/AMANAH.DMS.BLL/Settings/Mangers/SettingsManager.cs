﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class SettingsManager : BaseManager<SettingViewModel, Setting>, ISettingsManager
    {
        private readonly ICurrentUser _currentUser;

        public SettingsManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Setting> repository,
            ICurrentUser currentUser)
            : base(context, repository, mapper)
        {

            _currentUser = currentUser;
        }

        public async Task<List<SettingViewModel>> GetSettingByGroupIdAsync(string GroupId)
        {
            ///Return Setting by Key for specific tenant 
            await AddUserDefaultSettingsAsync(_currentUser.TenantId, new DefaultSettingValue());
            return await context.Settings
                .IgnoreQueryFilters()
                .Where(x => x.GroupId == GroupId)
                .ProjectTo<SettingViewModel>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        /// <summary>
        /// Get Setting by key for current Tenant that come from token 
        /// </summary>
        /// <param name="settingkey">Setting Key </param>
        /// <returns>Setting Object </returns>
        public async Task<SettingViewModel> GetSettingByKeyAsync(string settingkey)
        {
            return await context.Settings
                .IgnoreQueryFilters()
                .Where(x => x.SettingKey.Trim().ToLower() == settingkey.Trim().ToLower())
                .ProjectTo<SettingViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<SettingViewModel> GetAsync(int id)
        {
            return await (context as ApplicationDbContext).Settings
                .IgnoreQueryFilters()
                .Where(x => x.Id == id)
                .ProjectTo<SettingViewModel>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<bool> AddUserDefaultSettingsAsync(string tenantId, DefaultSettingValue defaultSetting)
        {
            var resourceSet = SettingsKeys.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            if (resourceSet == null)
            {
                return false;
            }
            var settings = await CreateSettingListAsync(tenantId, resourceSet, defaultSetting);
            if (settings == null || !settings.Any())
            {
                return false;
            }
            await context.Settings.AddRangeAsync(settings);
            return (await context.SaveChangesAsync()) > 0;
        }

        private async Task<List<Setting>> CreateSettingListAsync(string tenantId, ResourceSet resourceSet, DefaultSettingValue defaultSetting)
        {
            var oldSettings = await context.Settings
                .IgnoreQueryFilters()
                .Where(setting => setting.Tenant_Id == tenantId && !setting.IsDeleted)
                .ToListAsync();
            var settings = new List<Setting>();
            foreach (DictionaryEntry entry in resourceSet)
            {
                var isExist = oldSettings.Any(e => e.SettingKey.ToLower().Trim() == entry.Key.ToString().ToLower().Trim());
                if (isExist)
                {
                    continue;
                }
                settings.Add(CreateSetting(tenantId, entry.Key.ToString(), defaultSetting));
            }
            return settings;
        }

        private Setting CreateSetting(string tenantId, string settingKey, DefaultSettingValue defaultSettingValue)
        {
            string dataType = "string";
            string value = string.Empty;
            string group = ((int)SettingGroupEnum.AutoAllocation).ToString();
            switch (settingKey)
            {
                case SettingKeyConstant.IsEnableAutoAllocation:
                    {
                        value = defaultSettingValue.IsEnableAutoAllocation.ToString().Trim().ToLower();
                        dataType = "boolean";
                    }
                    break;
                case SettingKeyConstant.SelectedAutoAllocationMethodName:
                    {
                        value = defaultSettingValue.AutoAllocationMethod;
                    }
                    break;
                case SettingKeyConstant.NearestAvailableMethodOrderCapacity:
                    {
                        value = defaultSettingValue.NearestAvailableMethodOrderCapacity.ToString();
                    }
                    break;
                case SettingKeyConstant.NearestAvailableMethodRadiusInKM:
                    {
                        value = defaultSettingValue.NearestAvailableMethodRadiusInKM.ToString();
                    }
                    break;
                case SettingKeyConstant.NearestAvailableMethodNumberOfRetries:
                    {
                        value = defaultSettingValue.NearestAvailableMethodNumberOfRetries.ToString();
                    }
                    break;

                case SettingKeyConstant.ReachedDistanceRestriction:
                    {
                        value = defaultSettingValue.ReachedDistanceRestrictionInMeter.ToString();
                        group = ((int)SettingGroupEnum.General).ToString();
                    }
                    break;
                case SettingKeyConstant.DriverLoginRequestExpiration:
                    {
                        value = defaultSettingValue.DriverLoginRequestExpirationInHour.ToString();
                        group = ((int)SettingGroupEnum.General).ToString();
                    }
                    break;
            }

            return new Setting(tenantId: tenantId)
            {
                GroupId = group,
                SettingDataType = dataType,
                SettingKey = settingKey,
                Value = value,
            };
        }

        public async Task<int> GetAutoAllocationTypeidAsync()
        {
            var autoallocationSetting = await GetSettingByKeyAsync(SettingKeyConstant.SelectedAutoAllocationMethodName);
            var enableAutoallocationSetting = await GetSettingByKeyAsync(SettingKeyConstant.IsEnableAutoAllocation);
            int autoAllocationType = enableAutoallocationSetting == null
                ? (int)AutoAllocationTypeEnum.Manual
                : enableAutoallocationSetting.Value.ToLower() == "false"
                    ? (int)AutoAllocationTypeEnum.Manual
                    : autoallocationSetting == null
                        ? (int)AutoAllocationTypeEnum.Manual
                        : autoallocationSetting.Value == SettingKeyConstant.FirstInFirstOutMethod
                            ? (int)AutoAllocationTypeEnum.Fifo
                            : autoallocationSetting.Value == SettingKeyConstant.NearestAvailableMethod
                                ? (int)AutoAllocationTypeEnum.Nearest
                                : autoallocationSetting.Value == SettingKeyConstant.OneByOneMethod
                                    ? (int)AutoAllocationTypeEnum.OneByOne
                                    : (int)AutoAllocationTypeEnum.Manual;

            return autoAllocationType;
        }
    }
}
