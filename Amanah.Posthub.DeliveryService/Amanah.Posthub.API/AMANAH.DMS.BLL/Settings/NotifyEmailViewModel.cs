﻿namespace Amanah.Posthub.BLL.Settings
{
    public class NotifyEmailViewModel
    {
        public string ReceiverEmail { get; set; }
        public string ReceiverName { get; set; }
        public string ProjectName { get; set; }
        public string ResetPasswordLink { get; set; }
    }
}
