﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.Tasks.AutoAssignServices.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Tasks.AutoAssignServices
{
    public interface IAutoAssignTaskService
    {
        Task<List<AutoAssignDriverDTO>> GetDriversAllowedToAssignAsync(AutoAssignDriverFilterDTO filter);
        Task<List<MainTask>> GetUnAssignedMainTasksAsync(AutoAssignTaskFilterDTO filter);

        Task<bool> UpdateAssignedDriversToMainTasksAsync(TimeSpan clubbingTime, IEnumerable<AutoAssignDriverDTO> driversWithNewAssignedTasks);
        Task UpdateDriversWithExpireCluppingTimeAsync();
        void SendPushNotificationToDriver(string userId, string message, dynamic dynamicObject);

    }
    public class AutoAssignTaskService : IAutoAssignTaskService
    {
        private readonly ApplicationDbContext context;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly ILocalizer localizer;
        private readonly ICurrentUser currentUser;
        private readonly IBackGroundNotificationSercvice notificationService;
        private readonly IRepository<TaskHistory> taskHistoryRepository;

        private string CurrentNotificationSenderName { get; set; }

        public AutoAssignTaskService(
            ApplicationDbContext context,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILocalizer localizer,
            ICurrentUser currentUser,
            IBackGroundNotificationSercvice notificationService,
            IRepository<TaskHistory> taskHistoryRepository)
        {
            this.context = context;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.localizer = localizer;
            this.currentUser = currentUser;
            this.notificationService = notificationService;
            this.taskHistoryRepository = taskHistoryRepository;
            CurrentNotificationSenderName = "D-Hub";//TODO: replace with Config
        }

        public async Task<List<AutoAssignDriverDTO>> GetDriversAllowedToAssignAsync(AutoAssignDriverFilterDTO filter)
        {
            var query = GetDriverAsQueryable(filter)
                .AsNoTracking();

            if (filter.PickupBranchId.HasValue)
            {
                query = query.Where(driver => driver.CurrentLocation.BranchId == filter.PickupBranchId);
            }
            if (filter.DeliveryGeoFenceId.HasValue)
            {
                query = query.Where(driver => driver.AllDeliveryGeoFences
                    || driver.DriverDeliveryGeoFences.Any(geofence => geofence.Id == filter.DeliveryGeoFenceId));
            }
            if (filter.DeliveryGeoFencesIds != null && filter.DeliveryGeoFencesIds.Any())
            {
                query = query.Where(driver => driver.AllDeliveryGeoFences
                    || driver.DriverDeliveryGeoFences.Any(geofence => filter.DeliveryGeoFencesIds.Contains(geofence.Id)));
            }

            var drivers = await query
                .Where(driver => driver.ReachedTime.HasValue)
                .OrderBy(driver => driver.ReachedTime.Value)
                .ProjectTo<AutoAssignDriverDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            if (drivers == null)
            {
                return null;
            }

            if (filter.IncludeOrdersWeights)
            {
                int driverOrderCapacity = await GetDriverOrderCapacityAsync(filter);
                var driversTasksWeights = await GetDriverTasksWeightsAsync(new AutoAssignTaskFilterDTO
                {
                    Status = filter.TaskStatus,
                    Statuses = filter.TaskStatuses,
                    DriverIds = drivers?.Select(driver => driver.Id).ToList()
                });

                foreach (var driver in drivers)
                {
                    driver.MaxOrdersWeightsCapacity = driverOrderCapacity;
                    driver.MinOrdersNoWait = driverOrderCapacity;
                    if (driversTasksWeights != null)
                    {
                        driver.OrdersWeights = driversTasksWeights
                            .Where(weight => weight.Id == driver.Id)
                            .Select(weight => weight.Count)
                            .FirstOrDefault();
                    }
                    driver.AssignedTasksCount = driver.OrdersWeights;
                }
            }

            return drivers;
        }
        public async Task<List<MainTask>> GetUnAssignedMainTasksAsync(AutoAssignTaskFilterDTO filter)
        {
            var query = context.MainTask
                .Include(maintask => maintask.Tasks)
                .AsQueryable()
                .AsNoTracking();

            if (filter.Status.HasValue)
            {
                query = query.Where(maintask => maintask.Tasks.First().TaskStatusId == (int)filter.Status);
            }
            else if (filter.Statuses != null && filter.Statuses.Any())
            {
                query = query.Where(mainTask => filter.Statuses.Contains((TaskStatusEnum)mainTask.Tasks.First().TaskStatusId));
            }
            if (filter.PickupBranchId.HasValue)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.TaskTypeId == (int)TaskTypesEnum.Pickup)
                    && mainTask.Tasks.First(task => task.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId == filter.PickupBranchId);
            }

            return await query
                .OrderBy(e => e.CreatedDate)
                .ToListAsync();
        }

        public async Task UpdateDriversWithExpireCluppingTimeAsync()
        {
            var drivers = await context.Driver
                .Where(e => e.IsInClubbingTime
                    && e.ClubbingTimeExpiration.HasValue
                    && e.ClubbingTimeExpiration.Value <= DateTime.UtcNow)
                .AsNoTracking()
                .ToListAsync();
            foreach (var driver in drivers)
            {
                driver.IsInClubbingTime = false;
                driver.ClubbingTimeExpiration = null;

                context.Update(driver);

                SendPushNotificationToDriver(driver.UserId,
                    localizer[Keys.Notifications.YourAreReadyToStartTasks],
                    new { notificationType = NotificationTypeEnum.ReadyToGo });
            }

            await unitOfWork.SaveChangesAsync();
        }
        public async Task<bool> UpdateAssignedDriversToMainTasksAsync(TimeSpan clubbingTime, IEnumerable<AutoAssignDriverDTO> driversWithNewAssignedTasks)
        {
            bool isTheirDriverInClubbingTime = false;
            await unitOfWork.RunTransaction(async () =>
            {
                foreach (var driverWithNewAssignedTasks in driversWithNewAssignedTasks)
                {
                    await UpdateMainTasksAsync(driverWithNewAssignedTasks.NewAssignedMainTasks);

                    SendPushNotificationToDriver(driverWithNewAssignedTasks.UserId,
                        localizer[Keys.Notifications.NewTasksAssignedToYou],
                        new { notificationType = NotificationTypeEnum.NewTask });

                    var driverOrderCapacityAfterAssign = driverWithNewAssignedTasks.RemainingOrdersWeightsNoWait - driverWithNewAssignedTasks.NewAssignedMainTasks.Count();
                    if (!driverWithNewAssignedTasks.IsInClubbingTime && driverOrderCapacityAfterAssign > 0)
                    {
                        if (!isTheirDriverInClubbingTime)
                        {
                            isTheirDriverInClubbingTime = true;
                        }
                        driverWithNewAssignedTasks.IsInClubbingTime = true;
                        driverWithNewAssignedTasks.ClubbingTimeExpiration = DateTime.UtcNow.Add(clubbingTime);
                    }
                    else if (driverOrderCapacityAfterAssign <= 0 || (driverWithNewAssignedTasks.ClubbingTimeExpiration.HasValue && driverWithNewAssignedTasks.ClubbingTimeExpiration.Value <= DateTime.UtcNow))
                    {
                        driverWithNewAssignedTasks.IsInClubbingTime = false;
                        driverWithNewAssignedTasks.ClubbingTimeExpiration = null;

                        SendPushNotificationToDriver(driverWithNewAssignedTasks.UserId,
                            localizer[Keys.Notifications.YourAreReadyToStartTasks],
                            new { notificationType = NotificationTypeEnum.ReadyToGo });
                    }
                }
                await UpdateDriverClubbingTimeAsync(driversWithNewAssignedTasks);

                await unitOfWork.SaveChangesAsync();
            });

            return isTheirDriverInClubbingTime;
        }

        private async Task UpdateMainTasksAsync(List<MainTask> mainTasks)
        {
            foreach (var mainTask in mainTasks)
            {
                context.MainTask.Update(mainTask);
                foreach (var task in mainTask.Tasks)
                {
                    taskHistoryRepository.Add(new TaskHistory(currentUser.TenantId)
                    {
                        TaskId = task.Id,
                        MainTaskId = task.MainTaskId,
                        ActionName = "ASSIGNED",
                        ToStatusId = (int)TaskStatusEnum.Assigned
                    });
                }

                notificationService.SendToUserAndTenantAdmin(
                   currentUser.Id,
                   "AutoAllocationSucessfully",
                   localizer[Keys.Notifications.TaskAutoAllocationSucceded],
                   localizer.Format(Keys.Notifications.TaskAutoAllocationSuccededDetailsTaskId, mainTask.Id),
                   mainTask.Id);
            }
        }
        private async Task UpdateDriverClubbingTimeAsync(IEnumerable<AutoAssignDriverDTO> autoAssignDrivers)
        {
            var driverIds = autoAssignDrivers.Select(driver => driver.Id).ToArray();
            var drivers = await context.Driver
                .Where(driver => driverIds.Contains(driver.Id))
                .AsNoTracking()
                .ToListAsync();

            foreach (var autoAssignDriver in autoAssignDrivers)
            {
                var driver = drivers
                    .Where(d => d.Id == autoAssignDriver.Id)
                    .FirstOrDefault();
                if (driver != null)
                {
                    driver.IsInClubbingTime = autoAssignDriver.IsInClubbingTime;
                    driver.ClubbingTimeExpiration = autoAssignDriver.ClubbingTimeExpiration;
                    context.Driver.Update(driver);
                }
                //TODO: else need to reassign their tasks as driver not exist
            }
        }
        private IQueryable<Driver> GetDriverAsQueryable(AutoAssignDriverFilterDTO filter)
        {
            var query = context.Driver
                .Include(driver => driver.CurrentLocation)
                .Include(driver => driver.DriverDeliveryGeoFences)
                .Include(driver => driver.DriverPickUpGeoFences)
                .AsQueryable();
            if (filter.IsReached.HasValue)
            {
                if (filter.IsReached.Value)
                {
                    query = query.Where(driver => driver.ReachedTime.HasValue);
                }
                else
                {
                    query = query.Where(driver => !driver.ReachedTime.HasValue);
                }
            }
            if (filter.AgentStatus.HasValue)
            {
                query = query.Where(driver => driver.AgentStatusId == (int)filter.AgentStatus);
            }
            if (filter.AgentStatuses != null && filter.AgentStatuses.Any())
            {
                query = query.Where(driver => filter.AgentStatuses.Contains((AgentStatusesEnum)driver.AgentStatusId));
            }
            if (filter.DriverId.HasValue)
            {
                query = query.Where(driver => driver.Id == filter.DriverId);
            }

            return query;
        }
        private IQueryable<SubTask> GetSubTaskAsQueryable(AutoAssignTaskFilterDTO filter)
        {
            var query = context.Tasks
               .Include(task => task.MainTask)
               .Where(task => !task.IsDeleted);

            if (filter.DriverId.HasValue)
            {
                query = query.Where(task => task.DriverId == filter.DriverId);
            }
            else if (filter.DriverIds != null && filter.DriverIds.Any())
            {
                query = query.Where(task => task.DriverId.HasValue && filter.DriverIds.Contains(task.DriverId.Value));
            }

            if (filter.Status.HasValue)
            {
                query = query.Where(task => task.TaskStatusId == (int)filter.Status);
            }
            else if (filter.Statuses != null && filter.Statuses.Any())
            {
                query = query.Where(task => filter.Statuses.Contains((TaskStatusEnum)task.TaskStatusId));
            }

            if (filter.PickupBranchId.HasValue)
            {
                query = query.Where(task => task.TaskTypeId == (int)TaskTypesEnum.Pickup &&
                    task.BranchId == filter.PickupBranchId);
            }

            return query;
        }
        private IQueryable<MainTask> GetMainTaskAsQueryable(AutoAssignTaskFilterDTO filter)
        {
            var query = context.MainTask
               .Include(task => task.Tasks)
               .Where(task => !task.IsDeleted);

            if (filter.DriverId.HasValue)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.DriverId == filter.DriverId));
            }
            else if (filter.DriverIds != null && filter.DriverIds.Any())
            {
                query = query.Where(mainTask => mainTask.Tasks
                    .Where(task => task.DriverId.HasValue && filter.DriverIds.Contains(task.DriverId.Value)).Any());
            }

            if (filter.Status.HasValue)
            {
                query = query.Where(mainTask => mainTask.Tasks.All(task => task.TaskStatusId == (int)filter.Status));
            }
            else if (filter.Statuses != null && filter.Statuses.Any())
            {
                query = query.Where(mainTask => mainTask.Tasks.All(task => filter.Statuses.Contains((TaskStatusEnum)task.TaskStatusId)));
            }

            if (filter.PickupBranchId.HasValue)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.TaskTypeId == (int)TaskTypesEnum.Pickup &&
                    task.BranchId == filter.PickupBranchId));
            }

            return query;
        }

        private async Task<List<ObjectCountDTO<int>>> GetDriverTasksWeightsAsync(AutoAssignTaskFilterDTO filter)
        {
            List<ObjectCountDTO<int>> result = new List<ObjectCountDTO<int>>();

            var driverMainTasks = await GetMainTaskAsQueryable(filter)
                           .SelectMany(maintask => maintask.Tasks.Select(task => new
                           {
                               DriverId = task.DriverId.Value,
                               task.MainTaskId
                           }))
                           .ToListAsync();
            var driverIds = driverMainTasks
                .Select(driverMainTask => driverMainTask.DriverId)
                .Distinct()
                .ToList();
            foreach (var driverId in driverIds)
            {
                result.Add(new ObjectCountDTO<int>
                {
                    Id = driverId,
                    Count = driverMainTasks
                    .Select(driverMainTask => driverMainTask.MainTaskId)
                    .Distinct()
                    .Count()
                });
            }

            return result;
        }
        private async Task<Setting> GetSettingByKeyAsync(string settingkey)
        {
            return await context.Settings
                .Where(x => x.SettingKey.Trim().ToLower() == settingkey.Trim().ToLower())
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }
        private async Task<int> GetDriverOrderCapacityAsync(AutoAssignDriverFilterDTO filter)
        {
            int driverOrderCapacity = new DefaultSettingValue().FirstInFirstOutMethodDriverOrderCapacity;
            if (filter.IsApplyingAutoAllocation)
            {
                if (filter.AutoAllocationType == AutoAllocationTypeEnum.Fifo)
                {
                    var driverOrderCapacitySetting = await GetSettingByKeyAsync(SettingsKeys.FirstInFirstOutMethodDriverOrderCapacity);
                    if (driverOrderCapacitySetting != null)
                    {
                        int.TryParse(driverOrderCapacitySetting.Value, out driverOrderCapacity);
                    }
                }
            }

            return driverOrderCapacity;
        }
        public void SendPushNotificationToDriver(string userId, string message, dynamic dynamicObject)
        {
            notificationService.SendToUser(
               userId,
               message,
               CurrentNotificationSenderName,
               message,
               dynamicObject);
        }
    }
}
