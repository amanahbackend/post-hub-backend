﻿using Amanah.Posthub.Service.Domain.Tasks.Entities;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Tasks.AutoAssignServices.DTOs
{
    public class AutoAssignDriverDTO
    {
        public int Id { set; get; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {(string.IsNullOrEmpty(LastName) ? string.Empty : LastName)}";

        public DateTime? ReachedTime { get; set; }
        public bool IsReached => ReachedTime.HasValue;

        public int? BranchId { get; set; }
        public bool AllDeliveryGeoFences { get; set; }
        public List<AutoAssignDriverDeliveryGeofenceDTO> DriverDeliveryGeoFences { get; set; }

        public int MaxOrdersWeightsCapacity { get; set; }
        public int MinOrdersNoWait { get; set; }
        public int AssignedTasksCount { get; set; }
        public int OrdersWeights { get; set; }
        public int RemainingOrdersWeights => MaxOrdersWeightsCapacity - OrdersWeights;
        public int RemainingOrdersWeightsNoWait => MinOrdersNoWait - OrdersWeights;

        public List<MainTask> NewAssignedMainTasks { get; set; }
        public bool IsInClubbingTime { get; set; }
        public DateTime? ClubbingTimeExpiration { get; set; }
    }

    public class AutoAssignDriverDeliveryGeofenceDTO
    {
        public int Id { set; get; }
        public int GeoFenceId { set; get; }
    }

    public class ObjectCountDTO<T>
    {
        public T Id { get; set; }
        public int Count { get; set; }
    }
}
