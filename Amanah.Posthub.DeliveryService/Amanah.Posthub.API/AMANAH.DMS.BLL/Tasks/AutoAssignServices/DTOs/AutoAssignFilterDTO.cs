﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Tasks.AutoAssignServices.DTOs
{
    public class AutoAssignDriverFilterDTO
    {
        public bool IncludeAssignedTasks { get; set; }
        public TaskStatusEnum? TaskStatus { get; set; }
        public IList<TaskStatusEnum> TaskStatuses { get; set; }
        //public IList<MainTaskStatus> MainTaskStatuses { get; set; }
        //public bool IncludeGeoFences { get; set; }
        //public bool IncludePickupGeoFences { get; set; }

        public AgentStatusesEnum? AgentStatus { get; set; }
        public IList<AgentStatusesEnum> AgentStatuses { get; set; }
        public bool? IsReached { get; set; }
        //public bool IncludeCurrentLocation { get; set; } = true;

        public bool IncludeDeliveryGeoFences { get; set; }
        public bool IncludeOrdersWeights { get; set; }
        public bool IsApplyingAutoAllocation { get; set; }
        public AutoAllocationTypeEnum AutoAllocationType { get; set; }

        public int? DriverId { get; set; }

        public int? PickupBranchId { get; set; }
        public int? DeliveryGeoFenceId { get; set; }
        public List<int> DeliveryGeoFencesIds { get; set; }

        //public int? SamePickupBranchAsDriverId { get; set; }    //why this 

    }

    public class AutoAssignTaskFilterDTO
    {
        public int? DriverId { get; set; }
        public List<int> DriverIds { get; set; }
        public int? PickupBranchId { get; set; }
        public TaskStatusEnum? Status { get; set; }
        public IList<TaskStatusEnum> Statuses { get; set; }

        public IList<int> DeliverGeofenceIds { get; set; }
    }



}
