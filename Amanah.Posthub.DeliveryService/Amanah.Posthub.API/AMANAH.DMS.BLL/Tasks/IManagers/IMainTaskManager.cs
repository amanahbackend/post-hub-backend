﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IMainTaskManager : IBaseManager<MainTaskViewModel, MainTask>
    {
        Task<MainTaskViewModel> Get(int id);
        Task<string> AcceptAsync(int id, double? longitude = null, double? latitude = null);
        Task<string> DeclineAsync(int id, double? longitude = null, double? latitude = null);
        Task<TaskPagedResult<StattusMainTaskViewModel>> GetUnassignedAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        Task<TaskPagedResult<StattusMainTaskViewModel>> GetAssignedAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        Task<TaskPagedResult<StattusMainTaskViewModel>> GetCompletedAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        bool IsCompleted(int id);
        Task<PagedResult<MainTaskViewModel>> GetDriverTasks(PaginatedTasksDriverMobileInputDTO pagingparametermodel);
        Task<PagedResult<MainTaskViewModel>> GetDriverUncompletedTasks(PaginatedTasksDriverMobileInputDTO pagingparametermodel);
        Task<List<MainTaskViewModel>> GetDriverNewTasks(int DriverId);
        Task<MainTaskViewModel> AddMainTaskToDriver(MainTaskViewModel mainTask, int driverId);
        Task<bool> TaskHasGeofences(MainTaskViewModel mainTask);
        Task<CreateTaskResponse> AssignMainTaskAsync(AssignDriverMainTaskViewModel assignDriverMainTaskViewModel);
        Task ReassignMainTaskAsync(ReassignDriverMainTaskViewModel reassignDriverMainTaskViewModel);
        Task<PagedResult<HistoryMainTaskViewModel>> GetTaskHistoryDetailsAsync(PaginatedTasksDriverMobileInputDTO pagingparametermodel);
        Task<int> GetDriverNewTasksCountAsync(int DriverId);
        Task<PagedResult<StattusMainTaskViewModel>> GetMainTasksByAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        Task<MainTaskViewModel> TryAutoAssignmentAgain(int id);

    }
}
