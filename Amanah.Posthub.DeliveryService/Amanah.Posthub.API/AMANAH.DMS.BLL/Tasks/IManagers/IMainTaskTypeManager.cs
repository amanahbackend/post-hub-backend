﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Tasks.Entities;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IMainTaskTypeManager : IBaseManager<LkpResultDto, MainTaskType>
    {
    }
}
