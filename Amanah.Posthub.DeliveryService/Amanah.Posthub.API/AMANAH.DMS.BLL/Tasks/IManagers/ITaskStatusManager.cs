﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface ITaskStatusManager : IBaseManager<TaskStatusResultDto, Amanah.Posthub.Service.Domain.Tasks.Entities.TaskStatus>
    {
        Task<TaskStatusResultDto> GetAsync(int id);
    }
}
