﻿using Amanah.Posthub.BLL.Managers.TaskAssignment;
using Amanah.Posthub.BLL.Tasks.Services.DTOs;
using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Mobile;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.ViewModels;
using MapsUtilities.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface ITasksManager : IBaseManager<TasksViewModel, SubTask>
    {
        Task<List<MainTaskAutoAssignInputDTO>> GetMainTasksAsync(GetMainTasksInput input);
        Task<TasksViewModel> GetAsync(int id);
        Task ChangeStatusAsync(int id, int statusId, string reason);
        Task<string> SuccessfulAsync(SuccessOrFailTaskInputDTO VM);
        Task<string> FailedWithSinatureAsync(SuccessOrFailTaskInputDTO VM);
        Task<string> CancelAsync(int id, string reason, double? longitude = null, double? latitude = null);
        Task<string> StartAsync(int id, double? longitude = null, double? latitude = null);
        Task<string> DeclineAsync(int id, double? longitude = null, double? latitude = null);
        Task<string> SetReachedAsync(int taskId, bool isTaskReached);

        Task<PagedResult<TaskHistoryDetailsViewModel>> GetDriverFinishedTasksAsync(PaginatedTasksDriverMobileInputDTO pagingparametermodel);
        Task<PagedResult<TasksViewModel>> GetByPaginationAsync(PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null);
        Task ReasignDriverTasksAsync(ReasignDriverViewModel reasignDriverViewModel);
        Task<PagedResult<TaskHistoryViewModel>> GetDriverTimeLineAsync(PaginatedDriverTimeLineViewModel pagingparametermodel);
        Task<List<TaskCalenderViewModel>> GetTaskCalenderAsync(CalenderMobileInputDTO calenderViewModel);
        Task<PagedResult<TasksViewModel>> GetTasksReportAsync(PaginatedTasksReportViewModel pagingparametermodel);
        Task<List<ExportTasksWithoutProgressViewModel>> ExportTasksWithoutProgressToExcelAsync(PaginatedTasksReportViewModel pagingparametermodel);
        Task<List<ExportTasksWithProgressViewModel>> ExportTasksWithProgressToExcelAsync(PaginatedTasksReportViewModel pagingparametermodel);
        Task<List<ExportTaskViewModel>> ExportTaskToExcelAsync(int id);
        Task<TravelSummeryViewModel> TravelSummeryAsync(DateTime date, int driverId);
        Task<List<TaskStatusCountViewModel>> GetTaskStatusCountAsync(DateTime taskDate);

        Task AddTaskHistory(CreateTaskHistoryViewModel createTaskHistoryViewModel);
        Task AddTaskHistory(List<CreateTaskHistoryViewModel> TaskHistoryViewModelLst);

        Task AddDriverTaskNotification(TasksViewModel TasksViewModel, string msg, int notificationType = 0);
        Task AddDriverTaskNotification(List<TasksViewModel> TasksViewModelLst, string msg, int notificationType = 0);

        TasksViewModel InitStatusTask(TasksViewModel task);
        Task<CustomerResultDto> AddCustomerIsNotExist(CustomerResultDto viewModel);
        Task<TasksViewModel> AddCustomerToTaskAsync(TasksViewModel task);
        Task<List<TasksViewModel>> AddCustomerAndStatusAsync(List<TasksViewModel> ViewModelLst);

        Task SetMainTaskCompleted(int mainTaskId);
        Task SoftDeleteTaskHistory(List<int> taskIds);
        Task SendPushNotificationToDriver(int driverId, string message, dynamic MessageBody);
        //Task<MainTaskViewModel> AddAcceptedAsync(MainTaskViewModel viewModel);
        Task UpdateTasks(List<MainTaskViewModel> mainTasks);
        //Task UpdateTaskDriverAsync(int mainTaskId, int? driverId, int? intervalInSeconds = null);
        Task UpdateTaskDriverAsync(int mainTaskId, int? driverId, int? expirationIntervalInSeconds = null, Enums.TaskStatusEnum? status = null, TaskAssignmentType? assignmentType = null, DateTime? expirationDate = null, DateTime? reachedTime = null);
        Task<MainTaskViewModel> AddAsync(MainTaskViewModel viewModel);
        public Task<List<DriverViewModel>> GetAvailableDrivers(IEnumerable<int> teamIds = null, IEnumerable<string> tags = null, IEnumerable<int> geoFenceIds = null);
        Task<List<DriverViewModel>> GetAvailableDrivers(GetAailableDriversInput input);
        Task<List<DriverViewModel>> GetAvailablePlatformDriversAsync();

        Task CreateTaskDriverRequest(TaskDriverRequestViewModel input);
        Task UpdateTaskDriverRequest(TaskDriverRequestViewModel input);
        Task<List<TaskDriverRequestViewModel>> GetTaskDriverRequests(int mainTaskId, int? driverId = null);
        Task<bool> TaskHasGeofences(MainTaskViewModel mainTask);
        Task<GetTaskGeofencesOutput> GetGeofencesForTask(MainTaskViewModel mainTask);
        Task<Dictionary<MainTaskViewModel, GetTaskGeofencesOutput>> GetGeofencesForTasks(IEnumerable<MainTaskViewModel> mainTasks);
        Task<CustomerResultDto> GetCustomerAsync(CustomerResultDto viewModel);
        double GetTaskTotalTime(SubTask task);
        Task<int> GetTasksWeights(GetMainTasksInput input);
        Task<List<MainTaskViewModel>> GetMainTasks(GetMainTasksInput input);
        Task<Branch> GetRelatedBranchAsync(List<Branch> branches, MapPoint point);
        Task MakeDriverAvailable(int driverId);
        Task SetTaskDistanceHours(SubTask dbTask);
        Task AddTaskRoute(TaskRouteDtoInput taskRouteViewModel);
        Task UpdateMainTaskStatus(int mainTaskId);
        Task<bool> AutoAllocationFail(int mainTaskId);
        Task<bool> AutoAllocationSucssesfull(int mainTaskId);
        Task<MainTask> GetMainTaskStatus(int mainTaskId);
        Task<bool> CheckDriverBusy(int driverId);
        Task<bool> SetDeliveryBranch(MainTask mainTask);
        Task<TasksViewModel> UpdateTaskGeoFence(TasksViewModel Task);
        Task<List<TasksViewModel>> UpdateTaskGeoFence(List<TasksViewModel> Tasks);

        Task<bool> CheckTenantRestrictGeofenceAsync();
    }
}