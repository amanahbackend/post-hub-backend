﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Managers.TaskAssignment;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Mobile;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;
using TaskStatusEnum = Amanah.Posthub.BLL.Enums.TaskStatusEnum;

namespace Amanah.Posthub.BLL.Managers
{
    public class MainTaskManager : BaseManager<MainTaskViewModel, MainTask>, IMainTaskManager
    {
        private readonly ITasksManager _tasksManager;
        private readonly ITaskAutoAssignmentFactory _taskAutoAssignmentFactory;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IBackGroundNotificationSercvice _notificationService;
        private readonly IBackgroundJobManager _jobManager;

        public MainTaskManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<MainTask> repository,
            ITasksManager tasksManager,
            ITaskAutoAssignmentFactory taskAutoAssignmentFactory,
            ILocalizer localizer,
            ICurrentUser currentUser,
            IBackGroundNotificationSercvice notificationService,
            IBackgroundJobManager jobManager)
            : base(context, repository, mapper)
        {
            _tasksManager = tasksManager;
            _taskAutoAssignmentFactory = taskAutoAssignmentFactory;
            _localizer = localizer;
            _currentUser = currentUser;
            _notificationService = notificationService;
            _jobManager = jobManager;
        }

        public async Task<MainTaskViewModel> Get(int id)
        {
            var mainTask = await context.MainTask
                .Include(x => x.MainTaskType)
                .Include(x => x.Tasks)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                    (_currentUser.DriverId.HasValue &&
                     task.Tasks.Any(subTask => subTask.DriverId == _currentUser.DriverId) &&
                     string.IsNullOrEmpty(_currentUser.TenantId)))
                .FirstOrDefaultAsync(x => x.Id == id);

            mainTask.Tasks = mainTask.Tasks.ToList();
            return mapper.Map<MainTask, MainTaskViewModel>(mainTask); ;
        }

        public async override Task<MainTaskViewModel> AddAsync(MainTaskViewModel viewModel)
        {
            viewModel.Tasks = await _tasksManager.AddCustomerAndStatusAsync(viewModel.Tasks);
            var entity = mapper.Map<MainTaskViewModel, MainTask>(viewModel);
            var mainTaskEntity = repository.AddAsync(entity);
            var mainTaskVM = mapper.Map<MainTask, MainTaskViewModel>(mainTaskEntity);
            int? driverId = viewModel.Tasks.FirstOrDefault()?.DriverId;
            var historiesVM = new List<CreateTaskHistoryViewModel>();
            foreach (var taskVM in mainTaskVM.Tasks)
            {
                historiesVM.Add(new CreateTaskHistoryViewModel()
                {
                    TaskVM = taskVM,
                    ActionName = "CREATED",
                    TenantId = _currentUser.TenantId
                });
                if (driverId != null && driverId > 0)
                {
                    historiesVM.Add(new CreateTaskHistoryViewModel()
                    {
                        TaskVM = taskVM,
                        ActionName = "ASSIGNED",
                        ToStatusId = (int)TaskStatusEnum.Assigned,
                        TenantId = _currentUser.TenantId
                    });
                }
            }
            await _tasksManager.SetDeliveryBranch(mainTaskEntity);
            await _tasksManager.AddTaskHistory(historiesVM);
            var notifyDriver = new NotifyDriverOfAssignedTasksJobModel
            {
                Tasks = mainTaskVM.Tasks,
                Message = _localizer[Keys.Notifications.NewTasksAssignedToYou],
                NotificationType = (int)NotificationTypeEnum.NewTask
            };
            _jobManager.Enqueue(notifyDriver);
            await _tasksManager.UpdateMainTaskStatus(mainTaskVM.Id);
            return mainTaskVM;
        }

        public async override Task<List<MainTaskViewModel>> AddAsync(List<MainTaskViewModel> ViewModelLst)
        {
            var mainTaskEntity = new List<MainTask>();
            foreach (var mainTaskVM in ViewModelLst)
            {
                mainTaskVM.Tasks = await _tasksManager.AddCustomerAndStatusAsync(mainTaskVM.Tasks);
                var entity = mapper.Map<MainTaskViewModel, MainTask>(mainTaskVM);
                var mainTask = repository.AddAsync(entity);
                mainTaskEntity.Add(mainTask);
                int? driverId = mainTaskVM.Tasks.FirstOrDefault()?.DriverId;

                var historiesVM = new List<CreateTaskHistoryViewModel>();
                foreach (var taskVM in mainTaskVM.Tasks)
                {
                    historiesVM.Add(new CreateTaskHistoryViewModel()
                    {
                        TaskVM = taskVM,
                        ActionName = "CREATED",
                        TenantId = _currentUser.TenantId
                    });
                    if (driverId != null && driverId > 0)
                    {
                        historiesVM.Add(new CreateTaskHistoryViewModel()
                        {
                            TaskVM = taskVM,
                            ActionName = "ASSIGNED",
                            ToStatusId = (int)TaskStatusEnum.Assigned,
                            TenantId = _currentUser.TenantId
                        });
                    }
                }

                await _tasksManager.AddTaskHistory(historiesVM);
                await _tasksManager.AddDriverTaskNotification(
                    mainTaskVM.Tasks,
                    _localizer[Keys.Notifications.NewTasksAssignedToYou]);
                await _tasksManager.UpdateMainTaskStatus(mainTaskVM.Id);
            }
            var mainTasks = mapper.Map<List<MainTask>, List<MainTaskViewModel>>(mainTaskEntity);
            return mainTasks;
        }

        public async Task<string> AcceptAsync(int id, double? longitude = null, double? latitude = null)
        {
            var loggedInDriverId = _currentUser.DriverId;

            var dbMainTask = await context.MainTask
                .Include(x => x.Tasks).ThenInclude(x => x.TaskStatus)
                .IgnoreQueryFilters()
                .Where(x => x.Id == id && x.Tasks.First().DriverId == loggedInDriverId)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                    (_currentUser.DriverId.HasValue &&
                     task.Tasks.Any(subTask => subTask.DriverId == _currentUser.DriverId) &&
                     string.IsNullOrEmpty(_currentUser.TenantId)))
                .FirstOrDefaultAsync();

            if (dbMainTask == null)
            {
                throw new DomainException(Keys.Validation.NoMainTaskWithThisId);
            }
            var taskDriverRequest = await context.TaskDriverRequests
                .Where(x => x.MainTaskId == id && x.DriverId == loggedInDriverId)
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync();
            if (taskDriverRequest != null)
            {
                if (taskDriverRequest.ResponseStatus == (int)TaskDriverResponseStatusEnum.Expired ||
                    taskDriverRequest.ResponseStatus == (int)TaskDriverResponseStatusEnum.Declined ||
                    taskDriverRequest.ResponseStatus == (int)TaskDriverResponseStatusEnum.Canceled)
                {
                    throw new InvalidOperationException($"Task request is {(TaskDriverResponseStatusEnum)taskDriverRequest.ResponseStatus}");
                }
                if (taskDriverRequest.ResponseStatus == (int)TaskDriverResponseStatusEnum.NoResponse)
                {
                    taskDriverRequest.ResponseStatus = (int)TaskDriverResponseStatusEnum.Accepted;
                    context.TaskDriverRequests.Update(taskDriverRequest);
                }
            }

            var tasksVM = mapper.Map<List<SubTask>, List<TasksViewModel>>(dbMainTask.Tasks.ToList());

            foreach (var dbTask in dbMainTask.Tasks.Where(x => x.DriverId == loggedInDriverId).ToList())
            {
                dbTask.TaskStatusId = (int)TaskStatusEnum.Accepted;
                context.Tasks.Update(dbTask);
            }

            await context.SaveChangesAsync();
            var actionName = TaskStatusEnum.Accepted.ToString().ToUpper();
            var historiesVM = new List<CreateTaskHistoryViewModel>();


            foreach (var taskVM in tasksVM)
            {
                historiesVM.Add(new CreateTaskHistoryViewModel()
                {
                    TaskVM = taskVM,
                    ActionName = actionName,
                    FromStatusId = taskVM.TaskStatusId,
                    ToStatusId = (int)TaskStatusEnum.Accepted,
                    Longitude = longitude,
                    Latitude = latitude,
                    TenantId = dbMainTask.Tenant_Id
                });
            }
            await _tasksManager.AddTaskHistory(historiesVM);
            await _tasksManager.UpdateMainTaskStatus(id);

            return dbMainTask.Tenant_Id;
        }

        public async Task<string> DeclineAsync(int id, double? longitude = null, double? latitude = null)
        {
            var loggedInDriverId = _currentUser.DriverId;

            var dbMainTask = await context.MainTask
                .Include(x => x.Tasks).ThenInclude(x => x.TaskStatus)
                .IgnoreQueryFilters()
                .Where(x => x.Id == id && x.Tasks.First().DriverId == loggedInDriverId)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                    (_currentUser.DriverId.HasValue &&
                     task.Tasks.Any(subTask => subTask.DriverId == _currentUser.DriverId) &&
                     string.IsNullOrEmpty(_currentUser.TenantId)))
                .FirstOrDefaultAsync();

            if (dbMainTask == null)
            {
                throw new DomainException(Keys.Validation.NoMainTaskWithThisId);
            }
            var taskDriverRequest = await context.TaskDriverRequests
                .Where(x => x.MainTaskId == id && x.DriverId == loggedInDriverId)
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync();
            if (taskDriverRequest != null)
            {
                taskDriverRequest.ResponseStatus = (int)TaskDriverResponseStatusEnum.Declined;
                context.TaskDriverRequests.Update(taskDriverRequest);
                //if (dbMainTask.AssignmentType != (int)TaskAssignmentType.Manual)
                //{
                //    await _taskAutoAssignmentFactory.AssignNextDriver(id);
                //}
            }

            var tasksVM = mapper.Map<List<SubTask>, List<TasksViewModel>>(dbMainTask.Tasks.ToList());

            foreach (var task in dbMainTask.Tasks.Where(x => x.DriverId == loggedInDriverId).ToList())
            {
                task.TaskStatusId = (int)TaskStatusEnum.Declined;
                context.Tasks.Update(task);
            }
            await context.SaveChangesAsync();
            var actionName = TaskStatusEnum.Declined.ToString().ToUpper();
            var historiesVM = new List<CreateTaskHistoryViewModel>();
            foreach (var taskVM in tasksVM)
            {
                historiesVM.Add(new CreateTaskHistoryViewModel()
                {
                    TaskVM = taskVM,
                    ActionName = actionName,
                    FromStatusId = taskVM.TaskStatusId,
                    ToStatusId = (int)TaskStatusEnum.Declined,
                    Longitude = longitude,
                    Latitude = latitude,
                    TenantId = dbMainTask.Tenant_Id
                });
            }
            await _tasksManager.AddTaskHistory(historiesVM);
            await _tasksManager.UpdateMainTaskStatus(id);

            return dbMainTask.Tenant_Id;
        }

        public async Task<TaskPagedResult<StattusMainTaskViewModel>> GetUnassignedAsync(
            PaginatedTasksViewModel pagingparametermodel,
            List<int> managerTeamIds = null)
        {
            List<int> statusIds = new List<int>() { (int)TaskStatusEnum.Unassigned };
            return await GetMainTasksByStatusAsync(pagingparametermodel, statusIds, managerTeamIds);
        }

        public async Task<TaskPagedResult<StattusMainTaskViewModel>> GetAssignedAsync(
            PaginatedTasksViewModel pagingparametermodel,
            List<int> managerTeamIds = null)
        {
            List<int> statusIds = new List<int>() {
              (int)TaskStatusEnum.Assigned,
              (int)TaskStatusEnum.Accepted,
              (int)TaskStatusEnum.Cancelled,
              (int)TaskStatusEnum.Declined,
              (int)TaskStatusEnum.Failed,
              (int)TaskStatusEnum.Inprogress,
              (int)TaskStatusEnum.Started,
              (int)TaskStatusEnum.Successful,
            };

            return await GetMainTasksByStatusAsync(pagingparametermodel, statusIds, managerTeamIds);
        }

        public async Task<TaskPagedResult<StattusMainTaskViewModel>> GetCompletedAsync(
            PaginatedTasksViewModel pagingparametermodel,
            List<int> managerTeamIds = null)
        {
            var pagedResult = new TaskPagedResult<StattusMainTaskViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            Expression<Func<SubTask, bool>> searchExpression = e => true;
            if (!string.IsNullOrEmpty(pagingparametermodel.SearchBy))
                searchExpression = e => e.Customer != null && e.Customer.Name.Trim().ToLower().Contains(pagingparametermodel.SearchBy) || e.Address.Contains(pagingparametermodel.SearchBy);


            var source = (context as ApplicationDbContext).Tasks
                .Include(x => x.Customer)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskType)
                .Include(x => x.Driver)
                .ThenInclude(x => x.User)
                .Include(x => x.Driver)
                .ThenInclude(x => x.Team)
                .Include(x => x.MainTask)
                .ThenInclude(x => x.MainTaskType)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.FromStatus)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.ToStatus)
                .Include(x => x.TaskGallaries)
                .Include(x => x.Driver)
                .ThenInclude(x => x.DriverRates)
                .Where(searchExpression)
                .IgnoreQueryFilters()
                .Where(x => x.MainTask.IsCompleted == true
                        && (managerTeamIds == null || !managerTeamIds.Any() || managerTeamIds.Contains(x.Driver.TeamId))
                        && (pagingparametermodel.Id == 0 || x.Id == pagingparametermodel.Id)
                        && (!pagingparametermodel.FromDate.HasValue
                            || (x.PickupDate.HasValue && x.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                            || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                            )
                        && (!pagingparametermodel.ToDate.HasValue
                            || (x.PickupDate.HasValue && x.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                            || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                            )
                        //  && (pagingparametermodel.TeamIds == null || (!pagingparametermodel.TeamIds.Any() || pagingparametermodel.TeamIds.Contains(x.Driver.TeamId)))
                        && (pagingparametermodel.BranchesIds == null
                            || (!pagingparametermodel.BranchesIds.Any()
                                || pagingparametermodel.BranchesIds.Contains(x.BranchId ?? 0))
                            || (x.BranchId == null && pagingparametermodel.GetCustomerTasks)
                           ))
                .IgnoreQueryFilters()
                .Where(x => !x.IsDeleted && x.Tenant_Id == _currentUser.TenantId)
                .OrderByDescending(x => x.MainTaskId);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;
            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalOrderCount = await source.GroupBy(x => x.MainTaskId).Select(x => x.Key).CountAsync();
            pagedResult.TotalCount = await source.CountAsync();
            var tasks = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            pagedResult.Result = await BuildMainTasks(tasks);

            return pagedResult;

        }

        public async Task<TaskPagedResult<StattusMainTaskViewModel>> GetMainTasksByStatusAsync(
            PaginatedTasksViewModel pagingparametermodel,
            List<int> statusIds,
            List<int> managerTeamIds = null)
        {
            var pagedResult = new TaskPagedResult<StattusMainTaskViewModel>();
            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;
            Expression<Func<SubTask, bool>> searchExpression = e => true;
            if (!string.IsNullOrEmpty(pagingparametermodel.SearchBy))
                searchExpression = e => e.Customer != null && e.Customer.Name.Trim().ToLower().Contains(pagingparametermodel.SearchBy) || e.Address.Contains(pagingparametermodel.SearchBy);

            var source = context.Tasks
                             .Include(x => x.Customer)
                             .Include(x => x.TaskStatus)
                             .Include(x => x.TaskType)
                             .Include(x => x.Driver).ThenInclude(x => x.User)
                             .Include(x => x.Driver).ThenInclude(x => x.Team)
                             .Include(x => x.MainTask).ThenInclude(x => x.MainTaskType)
                             .Include(x => x.TaskHistories).ThenInclude(x => x.FromStatus)
                             .Include(x => x.TaskHistories).ThenInclude(x => x.ToStatus)
                             .Include(x => x.TaskGallaries)
                             .Where(searchExpression)
                             .Where(x => x.MainTask.IsCompleted != true
                                      && statusIds.Contains(x.TaskStatusId)
                                      && (managerTeamIds == null
                                        || !managerTeamIds.Any()
                                        || managerTeamIds.Contains(x.Driver.TeamId)
                                        || x.CreatedBy_Id == _currentUser.UserName)
                                      && (pagingparametermodel.Id == 0
                                        || x.Id == pagingparametermodel.Id)
                                      && (!pagingparametermodel.FromDate.HasValue
                                        || (x.PickupDate.HasValue && x.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                                        || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                                         )
                                      && (!pagingparametermodel.ToDate.HasValue
                                        || (x.PickupDate.HasValue && x.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                                        || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                                        )
                                      && ((pagingparametermodel.BranchesIds == null
                                            || (!pagingparametermodel.BranchesIds.Any()
                                            || pagingparametermodel.BranchesIds.Contains(x.BranchId ?? 0))
                                         || (x.BranchId == null && pagingparametermodel.GetCustomerTasks)))
                             )
                             .IgnoreQueryFilters()
                             .Where(x => !x.IsDeleted && x.Tenant_Id == _currentUser.TenantId)
                             .OrderByDescending(x => x.MainTaskId);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;
            pagedResult.TotalOrderCount = await source.GroupBy(x => x.MainTaskId).Select(x => x.Key).CountAsync();
            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = await source.CountAsync();
            pagedResult.TotalOrderCount = await source.GroupBy(x => x.MainTaskId).Select(x => x.Key).CountAsync();
            var tasks = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            pagedResult.Result = await BuildMainTasks(tasks);
            return pagedResult;

        }

        private async Task<List<StattusMainTaskViewModel>> BuildMainTasks(List<SubTask> tasks)
        {
            var mainTasks = new List<StattusMainTaskViewModel>();
            foreach (var task in tasks)
            {
                var taskVM = mapper.Map<SubTask, TasksViewModel>(task);
                var existMainTask = mainTasks.FirstOrDefault(t => t.Id == task.MainTaskId);
                if (existMainTask != null)
                {
                    existMainTask.Tasks.Add(taskVM);
                    continue;
                }

                var dbMainTask = await context.MainTask
                                .Include(t => t.MainTaskStatus)
                                .Include(t => t.Tasks)
                                .IgnoreQueryFilters()
                                .Where(task => !task.IsDeleted)
                                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                                    (_currentUser.DriverId.HasValue &&
                                    task.Tasks.Any(subTask => subTask.DriverId == _currentUser.DriverId) &&
                                    string.IsNullOrEmpty(_currentUser.TenantId)))
                                .FirstOrDefaultAsync(t => t.Id == task.MainTaskId);

                var tasksVM = new List<TasksViewModel>() { taskVM };
                var mainTask = new StattusMainTaskViewModel
                {
                    Id = task.MainTaskId,
                    DriverId = task.DriverId,
                    DriverImageUrl = task.Driver?.ImageUrl,
                    DriverName = task.Driver?.User?.FirstName + " " + task.Driver?.User?.LastName,
                    Tasks = tasksVM,
                    MainTaskTypeId = task.MainTask?.MainTaskTypeId,
                    MainTaskTypeName = task.MainTask?.MainTaskType?.Name,
                    IsCompleted = task.MainTask.IsCompleted,
                    IsDelayed = task.MainTask.IsDelayed,
                    NoOfTasks = dbMainTask.Tasks.Count(),
                    NoOfCompletedTasks = dbMainTask.Tasks.Count(t => t.TaskStatusId == (int)TaskStatusEnum.Successful),
                    MainTaskStatus = task.MainTask?.MainTaskStatusId ?? 0,
                    MainTaskStatusName = task.MainTask?.MainTaskStatus?.Name,
                    TeamName = task?.Driver?.Team?.Name,
                    TotalEstimationTime = HandleTotalEstimationTime(task.MainTask?.EstimatedTime),
                    IsFailToAutoAssignDriver = (bool)task.MainTask?.IsFailToAutoAssignDriver,
                    AssignmentType = (int)task.MainTask?.AssignmentType

                };
                mainTasks.Add(mainTask);
            }

            return mainTasks;
        }

        private TimeViewModel HandleTotalEstimationTime(double? estimationTime)
        {
            TimeViewModel EstimationTime = null;

            if (estimationTime.HasValue)
            {

                TimeSpan EstimationTimeSpan = TimeSpan.FromSeconds((double)estimationTime);
                EstimationTime = new TimeViewModel
                {
                    Time = EstimationTimeSpan,
                    Day = EstimationTimeSpan.Days,
                    Hour = EstimationTimeSpan.Hours,
                    Minute = EstimationTimeSpan.Minutes,
                    Second = EstimationTimeSpan.Seconds
                };
            }
            return EstimationTime;
        }

        public bool IsCompleted(int id)
        {
            return repository.Get(id).IsCompleted ?? false;
        }

        /// <summary>
        /// Get In Progress Tasks That 
        /// </summary>
        /// <param name="DriverId"></param>
        /// <returns></returns>
        public async Task<PagedResult<MainTaskViewModel>> GetDriverTasks(PaginatedTasksDriverMobileInputDTO pagingparametermodel)
        {
            var pagedResult = new PagedResult<MainTaskViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            pagingparametermodel.FromDate = pagingparametermodel.FromDate.HasValue ? pagingparametermodel.FromDate : DateTime.UtcNow;
            pagingparametermodel.ToDate = pagingparametermodel.ToDate.HasValue ? pagingparametermodel.ToDate : DateTime.UtcNow;

            var statusIds = new List<int>()
               {
                    (int)TaskStatusEnum.Assigned,
                    (int)TaskStatusEnum.Accepted,
                    (int)TaskStatusEnum.Started,
                    (int)TaskStatusEnum.Inprogress
               };

            var source = context.Tasks
                        .Include(x => x.TaskType)
                        .Include(x => x.MainTask).ThenInclude(x => x.MainTaskType)
                        .Include(x => x.Driver).ThenInclude(x => x.Team)
                        .Include(x => x.TaskStatus)
                        .Include(x => x.TaskHistories)
                        .Include(x => x.Customer).ThenInclude(x => x.Country)
                        .Where(t => t.MainTask.AssignmentType != (int)TaskAssignmentType.OneByOne
                         && t.DriverId == pagingparametermodel.DriverId
                         && statusIds.Contains(t.TaskStatusId)
                         && (string.IsNullOrEmpty(pagingparametermodel.SearchBy)
                             || t.Customer.Name.Contains(pagingparametermodel.SearchBy)
                             || t.Customer.Address.Contains(pagingparametermodel.SearchBy)
                             || t.OrderId.Contains(pagingparametermodel.SearchBy))
                         && (pagingparametermodel.TaskStatusIds == null
                             || !pagingparametermodel.TaskStatusIds.Any()
                             || pagingparametermodel.TaskStatusIds.Contains(t.TaskStatusId))
                         && ((t.PickupDate.HasValue && t.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                            || (t.DeliveryDate.HasValue && t.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date))
                         && ((t.PickupDate.HasValue && t.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                             || (t.DeliveryDate.HasValue && t.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)))
                        .IgnoreQueryFilters()
                        .Where(task => !task.IsDeleted)
                        .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                            (_currentUser.DriverId.HasValue &&
                            _currentUser.DriverId == task.DriverId &&
                            string.IsNullOrEmpty(_currentUser.TenantId)))
                        .OrderByDescending(x => x.MainTaskId);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int currentPage = pagingparametermodel.PageNumber;
            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int pageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();
            var tasks = await source.Skip((currentPage - 1) * pageSize).Take(pageSize).ToListAsync();
            pagedResult.Result = CreateMainTasks(tasks);
            return pagedResult;
        }

        /// <summary>
        /// Get In Progress Tasks That 
        /// </summary>
        /// <param name="DriverId"></param>
        /// <returns></returns>
        public async Task<PagedResult<MainTaskViewModel>> GetDriverUncompletedTasks(PaginatedTasksDriverMobileInputDTO pagingparametermodel)
        {
            var pagedResult = new PagedResult<MainTaskViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            pagingparametermodel.FromDate = pagingparametermodel.FromDate.HasValue ? pagingparametermodel.FromDate : DateTime.UtcNow;
            pagingparametermodel.ToDate = pagingparametermodel.ToDate.HasValue ? pagingparametermodel.ToDate : DateTime.UtcNow;

            var statusIds = new List<int>()
               {
                    (int)TaskStatusEnum.Assigned,
                    (int)TaskStatusEnum.Accepted,
                    (int)TaskStatusEnum.Started,
                    (int)TaskStatusEnum.Inprogress
               };

            var source = context.Tasks
                        .Include(x => x.TaskType)
                        .Include(x => x.MainTask).ThenInclude(x => x.MainTaskType)
                        .Include(x => x.Driver).ThenInclude(x => x.Team)
                        .Include(x => x.TaskStatus)
                        .Include(x => x.TaskHistories)
                        .Include(x => x.Customer)
                        .Where(t => t.DriverId == pagingparametermodel.DriverId
                         && (statusIds.Contains(t.TaskStatusId))
                         && (string.IsNullOrEmpty(pagingparametermodel.SearchBy)
                             || t.Customer.Name.Contains(pagingparametermodel.SearchBy)
                             || t.Customer.Address.Contains(pagingparametermodel.SearchBy)
                             || t.OrderId.Contains(pagingparametermodel.SearchBy))
                         && (pagingparametermodel.TaskStatusIds == null
                             || !pagingparametermodel.TaskStatusIds.Any()
                             || pagingparametermodel.TaskStatusIds.Contains(t.TaskStatusId))
                         && ((t.PickupDate.HasValue && t.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                            || (t.DeliveryDate.HasValue && t.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date))
                         && ((t.PickupDate.HasValue && t.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                             || (t.DeliveryDate.HasValue && t.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)))
                        .IgnoreQueryFilters()
                        .Where(task => !task.IsDeleted)
                        .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                            (_currentUser.DriverId.HasValue &&
                            _currentUser.DriverId == task.DriverId &&
                            string.IsNullOrEmpty(_currentUser.TenantId)))
                        .OrderByDescending(x => x.MainTaskId);


            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();
            var tasks = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            pagedResult.Result = CreateMainTasks(tasks);
            return pagedResult;
        }

        private List<MainTaskViewModel> CreateMainTasks(List<SubTask> tasks)
        {
            var mainTasks = new List<MainTaskViewModel>();
            foreach (var task in tasks)
            {
                var taskVM = mapper.Map<SubTask, TasksViewModel>(task);
                var existMainTask = mainTasks.FirstOrDefault(t => t.Id == task.MainTaskId);
                if (existMainTask != null)
                {
                    existMainTask.Tasks.Add(taskVM);
                    continue;
                }

                var tasksVM = new List<TasksViewModel>() { taskVM };
                var mainTask = new MainTaskViewModel
                {
                    Id = task.MainTaskId,
                    Tasks = tasksVM,
                    MainTaskTypeId = task.MainTask?.MainTaskTypeId ?? 0,
                    MainTaskTypeName = task.MainTask?.MainTaskType?.Name,
                    IsCompleted = task.MainTask?.IsCompleted,
                    IsDelayed = task.MainTask?.IsDelayed,
                    ExpirationDate = task.MainTask?.ExpirationDate,
                    AssignmentType = task.MainTask?.AssignmentType ?? 0
                };
                mainTasks.Add(mainTask);
            }

            return mainTasks;
        }

        public async Task<List<MainTaskViewModel>> GetDriverNewTasks(int DriverId)
        {
            var dbTasks = GetDriverNewTasksQuery(DriverId);

            var tasks = await dbTasks
                .IgnoreQueryFilters()
                .ProjectTo<MainTaskViewModel>(mapper.ConfigurationProvider)
                .OrderByDescending(x => x.Id)
                .ToListAsync();

            var now = DateTime.UtcNow;
            tasks.ForEach(t =>
            {
                t.RemainingTimeInSeconds = t.ExpirationDate.HasValue ? (int)Math.Round((t.ExpirationDate.Value - now).TotalSeconds) :
                  (int?)null;
            });
            return tasks;
        }

        public async Task<MainTaskViewModel> AddMainTaskToDriver(MainTaskViewModel mainTask, int driverId)
        {
            mainTask.Tasks.ForEach(t => t.DriverId = driverId);
            var mainTaskResult = await AddAsync(mainTask);
            return mainTaskResult;
        }

        public async Task<bool> TaskHasGeofences(MainTaskViewModel mainTask)
        {
            return await _tasksManager.TaskHasGeofences(mainTask);
        }

        public async Task<CreateTaskResponse> AssignMainTaskAsync(AssignDriverMainTaskViewModel assignDriverMainTaskViewModel)
        {
            var settings = assignDriverMainTaskViewModel.MainTask.Settings;
            //settings.Auto = true; //for test till solve frontend bug
            if (settings == null || !settings.Auto)
            {
                return await AssignManualMainTaskAsync(assignDriverMainTaskViewModel);
            }
            else
            {
                return await AssignAutoMainTaskAsync(assignDriverMainTaskViewModel);
            }
        }

        private async Task<CreateTaskResponse> AssignManualMainTaskAsync(AssignDriverMainTaskViewModel assignDriverMainTaskViewModel)
        {
            assignDriverMainTaskViewModel.MainTask.AssignmentType = (int)TaskAssignmentType.Manual;

            MainTaskViewModel mainTaskResult = new MainTaskViewModel();

            if (assignDriverMainTaskViewModel.MainTask.Settings.DriverIds == null)
            {
                mainTaskResult = await AddAsync(assignDriverMainTaskViewModel.MainTask);
                return new CreateTaskResponse { MainTask = mainTaskResult, StatusCode = HttpStatusCode.OK };
            }

            if (assignDriverMainTaskViewModel.MainTask.Settings.DriverIds != null)
            {
                foreach (var driverId in assignDriverMainTaskViewModel.MainTask.Settings.DriverIds)
                {
                    assignDriverMainTaskViewModel.MainTask.Tasks.ForEach(x => x.TaskStatusId = (int)TaskStatusEnum.Accepted);
                    mainTaskResult = await AddMainTaskToDriver(assignDriverMainTaskViewModel.MainTask, driverId);
                }
            }
            return new CreateTaskResponse { MainTask = mainTaskResult, StatusCode = HttpStatusCode.OK };

        }

        private async Task<CreateTaskResponse> AssignAutoMainTaskAsync(AssignDriverMainTaskViewModel assignDriverMainTaskViewModel)
        {
            var currentUserId = _currentUser.Id;

            assignDriverMainTaskViewModel.MainTask.Settings ??= new TaskSettingsViewModel();
            if (assignDriverMainTaskViewModel.MainTask.Settings.DriversCount == 0)
            {
                assignDriverMainTaskViewModel.MainTask.Settings.DriversCount = 1;
            }

            assignDriverMainTaskViewModel.MainTask.Settings.IsTenantAllowedToUsePlatformDrivers =
                await ValidateIfTenantIsAllowedToUsePlatformDriversAsync();

            var addedMainTaskVM = await _tasksManager.AddAsync(assignDriverMainTaskViewModel.MainTask);
            var taskAutoAssignment = await _taskAutoAssignmentFactory.CreateTaskAutoAssignmentAsync();

            var notificationTitle = _localizer[Keys.Notifications.TaskCreateAndAutoAllocationStartedTitle];
            var notificationBody = _localizer[Keys.Notifications.TaskCreateAndAutoAllocationStartedBody];
            _notificationService.SendToUserAndTenantAdmin(currentUserId, "AutoAllocationStarted", notificationTitle, notificationBody, assignDriverMainTaskViewModel?.MainTask?.Id);
            addedMainTaskVM.Settings = assignDriverMainTaskViewModel.MainTask.Settings;

            //  await taskAutoAssignment.AssignDriverToTaskAsync(addedMainTaskVM);

            return new CreateTaskResponse { MainTask = addedMainTaskVM, StatusCode = HttpStatusCode.Created };
        }

        public async Task ReassignMainTaskAsync(ReassignDriverMainTaskViewModel reassignDriverMainTaskViewModel)
        {
            var tasks = context.Tasks
                .Include(x => x.MainTask)
                .Include(x => x.Customer)
                .IgnoreQueryFilters()
                .Where(x => x.MainTaskId == reassignDriverMainTaskViewModel.MainTaskId)
                .ToList();

            if (tasks?.FirstOrDefault().DriverId != null)
            {
                await _tasksManager.SendPushNotificationToDriver(
                    (int)tasks.FirstOrDefault().DriverId,
                    string.Format(
                        _localizer[Keys.Notifications.TaskNoHasBeenDeletedFromYou], tasks.FirstOrDefault().Id),
                    new { notificationType = NotificationTypeEnum.ReassignTask });
            }

            var mainTask = tasks.FirstOrDefault().MainTask;
            foreach (var driverId in reassignDriverMainTaskViewModel.DriverIds)
            {
                if (driverId == reassignDriverMainTaskViewModel.DriverIds.FirstOrDefault())
                {
                    foreach (var task in tasks)
                    {
                        task.TaskStatusId = (int)TaskStatusEnum.Accepted;
                        task.DriverId = driverId;
                    }

                    await context.SaveChangesAsync();
                    var tasksVM = mapper.Map<List<SubTask>, List<TasksViewModel>>(tasks);

                    var historiesVM = new List<CreateTaskHistoryViewModel>();
                    foreach (var taskVM in tasksVM)
                    {
                        historiesVM.Add(new CreateTaskHistoryViewModel() { TaskVM = taskVM, ActionName = "ASSIGNED", ToStatusId = (int)TaskStatusEnum.Assigned });
                    }

                    await _tasksManager.AddTaskHistory(historiesVM);

                    await _tasksManager.AddDriverTaskNotification(
                        tasksVM,
                        _localizer[Keys.Notifications.NewTasksAssignedToYou],
                        (int)NotificationTypeEnum.NewTask);
                    continue;
                }

                var mainTaskVM = mapper.Map<MainTask, MainTaskViewModel>(mainTask);

                mainTaskVM.Id = 0;
                foreach (var task in mainTaskVM.Tasks)
                {
                    task.Id = 0;
                    task.TaskStatusId = (int)TaskStatusEnum.Accepted;
                    task.DriverId = driverId;
                }
                await AddAsync(mainTaskVM);

            }

            await _tasksManager.UpdateMainTaskStatus(mainTask.Id);

        }

        public async Task<PagedResult<HistoryMainTaskViewModel>> GetTaskHistoryDetailsAsync(PaginatedTasksDriverMobileInputDTO pagingparametermodel)
        {
            var pagedResult = new PagedResult<HistoryMainTaskViewModel>();
            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;
            pagingparametermodel.FromDate ??= DateTime.UtcNow;
            pagingparametermodel.ToDate ??= DateTime.UtcNow;
            var statuslst = new List<int>
            {
                (int)TaskStatusEnum.Successful,
                (int)TaskStatusEnum.Failed,
                (int)TaskStatusEnum.Declined,
                (int)TaskStatusEnum.Cancelled,
            };
            if (pagingparametermodel.TaskStatusIds != null &&
                pagingparametermodel.TaskStatusIds.Count() != 0)
            {
                statuslst = pagingparametermodel.TaskStatusIds;
            }

            var source = context.Tasks
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskType)
                .Include(x => x.Driver).ThenInclude(x => x.User)
                .Include(x => x.Customer)
                .Include(x => x.MainTask).ThenInclude(x => x.MainTaskType)
                .Include(x => x.TaskHistories).ThenInclude(x => x.FromStatus)
                .Include(x => x.TaskHistories).ThenInclude(x => x.ToStatus)
                .Include(x => x.TaskGallaries)
                .Where(tasks => statuslst.Contains(tasks.TaskStatusId) &&
                    tasks.DriverId == pagingparametermodel.DriverId)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                   (_currentUser.DriverId.HasValue &&
                    _currentUser.DriverId == task.DriverId &&
                    string.IsNullOrEmpty(_currentUser.TenantId)));


            if (pagingparametermodel.Id > 0)
            {
                source = source.Where(tasks => pagingparametermodel.Id == tasks.Id);
            }
            if (!string.IsNullOrEmpty(pagingparametermodel.SearchBy))
            {
                source = source.Where(tasks =>
                    tasks.Customer.Name.ToLower().Trim().Contains(pagingparametermodel.SearchBy.ToLower().Trim()) ||
                    tasks.Customer.Address.ToLower().Trim().Contains(pagingparametermodel.SearchBy.ToLower().Trim()));
            }
            if (pagingparametermodel.FromDate.HasValue)
            {
                source = source.Where(tasks =>
                    tasks.PickupDate.HasValue &&
                    tasks.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date ||
                    tasks.DeliveryDate.HasValue &&
                    tasks.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date);
            }
            if (pagingparametermodel.ToDate.HasValue)
            {
                source = source.Where(tasks =>
                    tasks.PickupDate.HasValue &&
                    tasks.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date ||
                    tasks.DeliveryDate.HasValue &&
                    tasks.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date);
            }
            source = source.OrderByDescending(x => x.MainTaskId);

            int currentPage = pagingparametermodel.PageNumber;
            int pageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = await source.CountAsync();
            var tasks = await source.Skip((currentPage - 1) * pageSize).Take(pageSize).ToListAsync();
            pagedResult.Result = await BuildMainTaskHistoryDetailsAsync(tasks);
            return pagedResult;
        }

        private async Task<List<HistoryMainTaskViewModel>> BuildMainTaskHistoryDetailsAsync(List<SubTask> tasks)
        {
            var mainTasks = new List<HistoryMainTaskViewModel>();
            foreach (var task in tasks)
            {
                var taskVM = mapper.Map<SubTask, TaskHistoryDetailsViewModel>(task);
                var existMainTask = mainTasks.FirstOrDefault(t => t.Id == task.MainTaskId);
                if (existMainTask != null)
                {
                    existMainTask.Tasks.Add(taskVM);
                    continue;
                }

                var dbMainTask = await context.MainTask
                                .Include(t => t.Tasks)
                                .IgnoreQueryFilters()
                                .Where(task => !task.IsDeleted)
                                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                                    (_currentUser.DriverId.HasValue &&
                                    task.Tasks.Any(subTask => subTask.DriverId == _currentUser.DriverId) &&
                                    string.IsNullOrEmpty(_currentUser.TenantId)))
                                .FirstOrDefaultAsync(t => t.Id == task.MainTaskId);

                var tasksVM = new List<TaskHistoryDetailsViewModel>() { taskVM };
                var mainTask = new HistoryMainTaskViewModel
                {
                    Id = task.MainTaskId,
                    DriverId = task.DriverId,
                    DriverImageUrl = task.Driver?.ImageUrl,
                    DriverName = task.Driver?.User?.FirstName,
                    Tasks = tasksVM,
                    MainTaskTypeId = task.MainTask?.MainTaskTypeId,
                    MainTaskTypeName = task.MainTask?.MainTaskType?.Name,
                    IsCompleted = task.MainTask.IsCompleted,
                    IsDelayed = task.MainTask.IsDelayed,
                    NoOfTasks = dbMainTask.Tasks.Count(),
                    NoOfCompletedTasks = dbMainTask.Tasks.Count(t => t.TaskStatusId == (int)TaskStatusEnum.Successful),
                };
                mainTasks.Add(mainTask);
            }

            return mainTasks;
        }

        public override async Task<List<MainTaskViewModel>> GetAllAsync<MainTaskViewModel>()
        {
            var mainTasks = await repository.GetAll()
                .IgnoreQueryFilters()
                .Include(t => t.Tasks)
                .ToListAsync();
            mainTasks.ForEach(x => x.Tasks = x.Tasks.Where(d => d.IsDeleted == false).ToList());
            return mapper.Map<List<MainTask>, List<MainTaskViewModel>>(mainTasks);

        }

        public override async Task<PagedResult<MainTaskViewModel>> GetAllByPaginationAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<MainTaskViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;


            var source = repository.GetAll().Include(x => x.Tasks);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();

            var mainTasks = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            mainTasks.ForEach(x => x.Tasks = x.Tasks.Where(d => d.IsDeleted == false).ToList());
            pagedResult.Result = mapper.Map<List<MainTask>, List<MainTaskViewModel>>(mainTasks);

            return pagedResult;
        }

        private IQueryable<MainTask> GetDriverNewTasksQuery(int DriverId)
        {
            var bufferTime = TimeSpan.FromSeconds(5);
            var now = DateTime.UtcNow;
            var afterBufferTime = now + bufferTime;
            return context.MainTask
                .Include(mainTask => mainTask.Tasks)
                .Where(mainTask => mainTask.Tasks.Any(task => task.DriverId == DriverId &&
                    task.TaskStatusId == (int)TaskStatusEnum.Assigned) &&
                    (!mainTask.ExpirationDate.HasValue || mainTask.ExpirationDate.Value >= afterBufferTime))
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                    (_currentUser.DriverId.HasValue &&
                     task.Tasks.Any(subTask => subTask.DriverId == _currentUser.DriverId) &&
                     string.IsNullOrEmpty(_currentUser.TenantId)));
        }

        public Task<int> GetDriverNewTasksCountAsync(int DriverId)
        {
            return GetDriverNewTasksQuery(DriverId).CountAsync();
        }

        public async Task<PagedResult<StattusMainTaskViewModel>> GetMainTasksByAsync(
            PaginatedTasksViewModel pagingparametermodel, List<int> managerTeamIds = null)
        {
            var pagedResult = new PagedResult<StattusMainTaskViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            Expression<Func<SubTask, bool>> searchExpression = task => true;
            if (!string.IsNullOrEmpty(pagingparametermodel.SearchBy))
            {
                searchExpression = task =>
                    task.Customer != null &&
                    (
                        task.Customer.Name.Contains(pagingparametermodel.SearchBy) ||
                        task.Address.Contains(pagingparametermodel.SearchBy) ||
                        task.OrderId.Contains(pagingparametermodel.SearchBy) ||
                        (task.Driver.User.FirstName + " " + task.Driver.User.LastName).Contains(pagingparametermodel.SearchBy) ||
                        task.MainTaskId.ToString().Contains(pagingparametermodel.SearchBy)
                    );
            }

            var source = context.Tasks
                .Include(x => x.Customer)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskType)
                .Include(x => x.Driver).ThenInclude(x => x.Team)
                .Include(x => x.Driver)
                .ThenInclude(x => x.User)
                .Include(x => x.MainTask)
                .ThenInclude(x => x.MainTaskType)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.FromStatus)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.ToStatus)
                .Include(x => x.TaskHistories)
                .Include(x => x.Customer)
                .Include(x => x.TaskStatus)
                .Include(x => x.TaskType)
                .Include(x => x.Driver.Team)
                .Include(x => x.Driver.User)
                .Include(x => x.MainTask)
                .ThenInclude(x => x.MainTaskType)
                .Where(searchExpression)
                .Where(x =>
                    (pagingparametermodel.TaskStatusIds == null
                        || !pagingparametermodel.TaskStatusIds.Any()
                        || pagingparametermodel.TaskStatusIds.Contains(x.TaskStatusId))
                    && (managerTeamIds == null || !managerTeamIds.Any() || managerTeamIds.Contains(x.Driver.TeamId))
                    && (pagingparametermodel.Id == 0 || x.Id == pagingparametermodel.Id)
                    && (!pagingparametermodel.FromDate.HasValue
                        || (x.PickupDate.HasValue && x.PickupDate.Value.Date >= pagingparametermodel.FromDate.Value.Date)
                        || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date >= pagingparametermodel.FromDate.Value.Date))
                    && (!pagingparametermodel.ToDate.HasValue
                        || (x.PickupDate.HasValue && x.PickupDate.Value.Date <= pagingparametermodel.ToDate.Value.Date)
                        || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date <= pagingparametermodel.ToDate.Value.Date))
                    && (pagingparametermodel.BranchesIds == null
                        || !pagingparametermodel.BranchesIds.Any()
                        || pagingparametermodel.BranchesIds.Contains(x.BranchId ?? 0)
                        || (x.BranchId == null && pagingparametermodel.GetCustomerTasks)))
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted && task.Tenant_Id == _currentUser.TenantId)
                .OrderByDescending(x => x.MainTaskId);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();
            var tasks = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();
            pagedResult.Result = await BuildMainTasks(tasks);
            return pagedResult;

        }

        public async Task<PagedResult<MainTasksReportViewModel>> GetMainTasksReportAsync(
            PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<MainTasksReportViewModel>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            var source = context.Tasks
                .Include(x => x.Driver)
                .ThenInclude(x => x.User)
                .Include(x => x.MainTask)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.FromStatus)
                .Include(x => x.TaskHistories)
                .ThenInclude(x => x.ToStatus)
                .IgnoreQueryFilters()
                .OrderByDescending(x => x.MainTaskId);

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = source.Count();
            var tasks = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();

            var mainTasks = CreateMainTasks(tasks);
            if (mainTasks == null || !mainTasks.Any())
                return new PagedResult<MainTasksReportViewModel>();

            var reports = new List<MainTasksReportViewModel>();
            foreach (var mainTask in mainTasks)
            {
                var histories = mainTask.Tasks.Select(x => x.TaskHistories).ToList();

                var driversNames = mainTask.Tasks.Select(x => x.DriverName).ToList();

                var pickup = mainTask.Tasks.Where(x => x.TaskTypeId == (int)Enums.TaskTypeEnum.Pickup);
                var pickupAddresses = pickup.Select(x => x.Address).ToList();
                var pickupDates = pickup.Select(x => x.PickupDate).ToList();

                var delivery = mainTask.Tasks.Where(x => x.TaskTypeId == (int)Enums.TaskTypeEnum.Delivery);
                var deliveryAddresses = delivery.Select(x => x.Address).ToList();
                var deliveryDates = delivery.Select(x => x.DeliveryDate).ToList();

                var driverRates = mainTask.Tasks.Select(x => x.DriverRates);
                List<double> rates = new List<double>();
                List<string> notes = new List<string>();
                if (driverRates != null && driverRates.Any())
                {
                    foreach (var rate in driverRates)
                    {
                        rates = rate.Select(x => x.Rate).ToList();
                        notes = rate.Select(x => x.Note).ToList();
                    }
                }

                var report = new MainTasksReportViewModel()
                {
                    Id = mainTask.Id,
                    DriversNames = driversNames,
                    Histories = histories,
                    PickupAddresses = pickupAddresses,
                    PickupDates = pickupDates,
                    DeliveryAddresses = deliveryAddresses,
                    DeliverDates = deliveryDates,
                    Rates = rates,
                    Notes = notes,
                };
                // Assign means all Tasks status Assigned
                // Inprogress means any tasks status start or accept
                // Failed means any task status failed
                // sucsess means all tasks status success
                reports.Add(report);
            }

            pagedResult.Result = reports;
            return pagedResult;

        }


        public async Task<MainTaskViewModel> TryAutoAssignmentAgain(int id)
        {
            var mainTask = await Get(id);
            mainTask.IsFailToAutoAssignDriver = false;
            mainTask.Settings = new TaskSettingsViewModel
            {
                DriversCount = 1,
                RetriesCount = 1,
                Auto = true
            };

            mainTask.Settings.IsTenantAllowedToUsePlatformDrivers =
                await ValidateIfTenantIsAllowedToUsePlatformDriversAsync();

            await UpdateAsync(mainTask);
            var taskAutoAssignment = await _taskAutoAssignmentFactory.CreateTaskAutoAssignmentAsync();
            //ToDo need to be handled
            // await taskAutoAssignment.AssignDriverToTaskAsync(mainTask);

            return mainTask;
        }

        private async Task<bool> ValidateIfTenantIsAllowedToUsePlatformDriversAsync()
        {
            var userRoleForTenant = await context.UserRoles
                  .IgnoreQueryFilters()
                  .Where(userRole => userRole.UserId == _currentUser.TenantId)
                  .FirstOrDefaultAsync();
            if (userRoleForTenant != null)
            {
                return await context.RoleClaims
                 .Where(roleClaim => roleClaim.RoleId == userRoleForTenant.RoleId &&
                    roleClaim.ClaimValue == TenantPermissions.PlatformAgent.ReadPlatformAgent)
                 .AnyAsync();
            }
            return false;
        }

    }

    internal class NotifyDriverOfAssignedTasksJobModel
    {
        public List<TasksViewModel> Tasks { get; internal set; }
        public string Message { get; internal set; }
        public int NotificationType { get; internal set; }
    }

    internal class NotifyDriverOfAssignedTasksJob : IBackgroundJob<NotifyDriverOfAssignedTasksJobModel>
    {
        private readonly ITasksManager _tasksManager;
        private readonly ILocalizer _localizer;

        public NotifyDriverOfAssignedTasksJob(ITasksManager tasksManager, ILocalizer localizer)
        {
            _tasksManager = tasksManager;
            _localizer = localizer;
        }

        public Task ExecuteAsync(NotifyDriverOfAssignedTasksJobModel jobModel)
        {
            return _tasksManager.AddDriverTaskNotification(
                jobModel.Tasks,
                _localizer[Keys.Notifications.NewTasksAssignedToYou],
                (int)NotificationTypeEnum.NewTask);
        }
    }
}
