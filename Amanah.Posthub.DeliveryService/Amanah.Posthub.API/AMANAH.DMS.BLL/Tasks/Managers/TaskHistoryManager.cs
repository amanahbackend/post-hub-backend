﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using AutoMapper;

namespace Amanah.Posthub.BLL.Managers
{
    public class TaskHistoryManager : BaseManager<TaskHistoryViewModel, TaskHistory>, ITaskHistoryManager
    {
        public TaskHistoryManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<TaskHistory> repository)
            : base(context, repository, mapper)
        {
        }
    }
}
