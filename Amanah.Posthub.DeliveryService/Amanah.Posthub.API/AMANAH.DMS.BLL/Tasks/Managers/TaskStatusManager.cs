﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class TaskStatusManager : BaseManager<TaskStatusResultDto, Amanah.Posthub.Service.Domain.Tasks.Entities.TaskStatus>, ITaskStatusManager
    {
        public TaskStatusManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<Amanah.Posthub.Service.Domain.Tasks.Entities.TaskStatus> repository)
            : base(context, repository, mapper)
        {
        }

        public async Task<TaskStatusResultDto> GetAsync(int id)
        {
            return await context.TaskStatus
                .Where(x => x.Id == id)
                .ProjectTo<TaskStatusResultDto>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

    }
}
