﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Managers.TaskAssignment;
using Amanah.Posthub.BLL.TasksFolder.ViewModels.MainTasksDTO;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;
using TaskStatusEnum = Amanah.Posthub.BLL.Enums.TaskStatusEnum;

namespace Amanah.Posthub.BLL.TasksFolder.Queries
{
    public interface ISubTaskQuery
    {
        Task<TasksMobileResultDTO> GetAsync(int id);
        Task<PagedResult<TaskHistoryDetailsResultDTO>> GetDriverFinishedTasksAsync(
         PaginatedTasksDriverMobileInputDTO pagingparametermodel);

        Task<List<TaskStatusCountResultDTO>> GetTaskStatusCountAsync(DateTime taskDate);
        Task<TravelSummeryResultDTO> TravelSummeryAsync(DateTime date, int driverId);
        Task<List<TaskCalenderResultDTO>> GetTaskCalenderAsync(CalenderMobileInputDTO calenderViewModel);

    }
    public class SubTaskQuery : ISubTaskQuery
    {
        private readonly ApplicationDbContext context;
        private readonly ICurrentUser _currentUser;
        private readonly IMapper _mapper;

        public SubTaskQuery(
            ApplicationDbContext _context,
            IMapper mapper,
            ITaskAutoAssignmentFactory taskAutoAssignmentFactory,
            ICurrentUser currentUser)
        {
            _currentUser = currentUser;
            _mapper = mapper;
            context = _context;
        }


        public async Task<TasksMobileResultDTO> GetAsync(int id)
        {
            var task = await context.Tasks
                .Include(x => x.TaskType)
                .Include(x => x.TaskStatus)
                .Include(x => x.Customer)
                .Include(x => x.Driver)
                .Include(x => x.TaskHistories)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                   (_currentUser.DriverId.HasValue &&
                    _currentUser.DriverId == task.DriverId &&
                    string.IsNullOrEmpty(_currentUser.TenantId)))
                .FirstOrDefaultAsync(x => x.Id == id);
            if (task != null)
            {
                task.TaskHistories = task.TaskHistories.Where(d => d.IsDeleted == false).ToList();
            }
            return _mapper.Map<SubTask, TasksMobileResultDTO>(task);
        }

        public Task<PagedResult<TaskHistoryDetailsResultDTO>> GetDriverFinishedTasksAsync(
        PaginatedTasksDriverMobileInputDTO pagingparametermodel)
        {
            var statuslst = new List<int>
            {
                (int)TaskStatusEnum.Successful,
                (int)TaskStatusEnum.Failed,
                (int)TaskStatusEnum.Declined,
                (int)TaskStatusEnum.Cancelled,
            };

            var pagedResult = context.Tasks
                .Where(t => statuslst.Contains(t.TaskStatusId) &&
                    t.DriverId == pagingparametermodel.DriverId &&
                    (pagingparametermodel.Id == 0 || pagingparametermodel.Id == t.Id) &&
                        (string.IsNullOrEmpty(pagingparametermodel.SearchBy)
                        || t.Customer.Name.Contains(pagingparametermodel.SearchBy)
                        || t.Customer.Address.Contains(pagingparametermodel.SearchBy)))
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                    (_currentUser.DriverId.HasValue &&
                    task.DriverId == _currentUser.DriverId &&
                    string.IsNullOrEmpty(_currentUser.TenantId)))
                .OrderByDescending(t => t.DeliveryDate)

                .ToPagedResultAsync<SubTask, TaskHistoryDetailsResultDTO>(
                    pagingparametermodel,
                    _mapper.ConfigurationProvider);

            return pagedResult;
        }

        public async Task<List<TaskCalenderResultDTO>> GetTaskCalenderAsync(CalenderMobileInputDTO calenderViewModel)
        {
            var completedStatus = new List<int>
            {
                  (int)TaskStatusEnum.Successful,
                  (int)TaskStatusEnum.Failed,

            };

            var PendingStatus = new List<int>
            {
                  (int)TaskStatusEnum.Accepted,
                  (int)TaskStatusEnum.Assigned,
                  (int)TaskStatusEnum.Inprogress,

            };

            var tasks = context.Tasks
                .IgnoreQueryFilters()
                .Where(x => x.DriverId == calenderViewModel.Driver_id)
                .Where(task => !task.IsDeleted)
                              .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                   (_currentUser.DriverId.HasValue &&
                    _currentUser.DriverId == task.DriverId &&
                    string.IsNullOrEmpty(_currentUser.TenantId)))
                .Select(x => new
                {
                    TaskDate = x.PickupDate ?? x.DeliveryDate,
                    x.TaskStatusId,
                    x.TotalDistance,
                    x.TotalTaskTime
                });
            //Fixing query to be translated according to https://stackoverflow.com/questions/58102821/translating-query-with-group-by-and-count-to-linq
            return await tasks
            .Where(x => (!calenderViewModel.FromDate.HasValue ||
                    x.TaskDate.Value.Date >= calenderViewModel.FromDate.Value.Date)
                && (!calenderViewModel.ToDate.HasValue ||
                    x.TaskDate.Value.Date <= calenderViewModel.ToDate.Value.Date))
            .GroupBy(x => x.TaskDate.Value.Date)
            .Select(group => new TaskCalenderResultDTO
            {
                TaskDate = group.Key,
                CompletedCount = group.Sum(x => completedStatus.Contains(x.TaskStatusId) ? 1 : 0),
                PendingCount = group.Sum(x => (PendingStatus.Contains(x.TaskStatusId) && x.TaskStatusId != (int)TaskStatusEnum.Unassigned) ? 1 : 0),
                TotalKM = group.Sum(x => x.TotalDistance.HasValue ? x.TotalDistance : 0),
                TotalTime = group.Sum(x => x.TotalTaskTime.HasValue ? x.TotalTaskTime : 0)
            })
            .OrderBy(x => x.TaskDate)
            .ToListAsync();
        }

        public async Task<TravelSummeryResultDTO> TravelSummeryAsync(DateTime date, int driverId)
        {
            var completedStatus = new List<int>
            {
               (int)TaskStatusEnum.Successful,
               (int)TaskStatusEnum.Failed
            };

            var tasks = context.Tasks
                .Include(x => x.TaskHistories)
                .Include(x => x.TaskRoutes)
                .IgnoreQueryFilters()
                .Where(x =>
                    x.IsDeleted == false
                    && x.DriverId == driverId
                    && completedStatus.Contains(x.TaskStatusId)
                    && ((x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date == date.Date) || (x.PickupDate.HasValue && x.PickupDate.Value.Date == date.Date)));

            List<TaskRoute> taskRoutes = new List<TaskRoute>();
            var tasksCount = await tasks.CountAsync();
            var tasksKMs = await tasks.SumAsync(x => x.TotalDistance);
            var tasksHours = await tasks.SumAsync(x => x.TotalTime);
            var tasklist = tasks.ToList();
            foreach (var task in tasklist)
            {
                ////tasksHours += GetTaskHours(task);

                if (task.TaskRoutes != null || task.TaskRoutes.Any())
                    taskRoutes.AddRange(task.TaskRoutes.ToList());
            }

            taskRoutes.OrderByDescending(x => x.CreatedDate);
            List<TaskRouteDTO> taskRoutesVM = _mapper.Map<List<TaskRoute>, List<TaskRouteDTO>>(taskRoutes);

            tasksHours = tasksHours.HasValue ? tasksHours.Value : 0;
            tasksKMs = tasksKMs.HasValue ? tasksKMs.Value : 0;



            return new TravelSummeryResultDTO()
            {
                TasksCount = tasksCount,
                TasksHours = Math.Round((double)tasksHours, 2),
                TasksKMs = Math.Round((double)tasksKMs / 1000),
                TasksRoutes = taskRoutesVM,
            };

        }


        public async Task<List<TaskStatusCountResultDTO>> GetTaskStatusCountAsync(DateTime taskDate)
        {

            var tasks = context.Tasks
                .IgnoreQueryFilters()
                .Where(x =>
                (x.PickupDate.Value.Date == taskDate.Date
                || x.DeliveryDate.Value.Date == taskDate.Date)
                &&
                x.DriverId == _currentUser.DriverId

                );

            return new List<TaskStatusCountResultDTO>
                {
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Accepted),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Assigned),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Cancelled),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Declined),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Failed),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Inprogress),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Started),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Successful),
                    await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Unassigned)
                };
        }

        private async Task<TaskStatusCountResultDTO> GetTaskStatusCountAsync(IQueryable<SubTask> query, TaskStatusEnum taskStatus)
        {
            return new TaskStatusCountResultDTO()
            {
                TaskStatusId = (int)taskStatus,
                TaskStatusName = taskStatus.ToString(),
                Count = await query.CountAsync(x => x.TaskStatusId == (int)taskStatus)
            };
        }

    }
}
