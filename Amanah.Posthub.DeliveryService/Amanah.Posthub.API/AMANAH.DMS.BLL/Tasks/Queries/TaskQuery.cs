﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.ExtentionMethods;
using Amanah.Posthub.BLL.TasksFolder.ViewModels.MainTasksDTO;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;
using TaskStatusEnum = Amanah.Posthub.BLL.Enums.TaskStatusEnum;

namespace Amanah.Posthub.BLL.TasksFolder.Queries
{
    public interface IMainTaskQuery
    {
        Task<PagedResult<MainTaskMobileResultWithTasksDTO>> GetDriverTasksAsync(PaginatedTasksDriverMobileInputDTO pagingparametermodel);
        IQueryable<MainTaskResultDTO> GetDriverNewTasks(int DriverId);
        Task<PagedResult<HistoryMainTaskMobileResultWithTasksDTO>> GetTaskHistoryDetailsAsync(PaginatedTasksDriverMobileInputDTO pagingparametermodel);
    }

    public class MainTaskQuery : IMainTaskQuery
    {
        private readonly ApplicationDbContext context;
        private readonly ICurrentUser _currentUser;
        private readonly IMapper _mapper;
        private readonly List<int> completedTaskStatues = new List<int>
        {
            (int)TaskStatusEnum.Successful,
            (int)TaskStatusEnum.Failed,
            (int)TaskStatusEnum.Declined,
            (int)TaskStatusEnum.Cancelled,
        };
        private readonly List<int> notCompletedTaskStatues = new List<int>
        {
            (int)TaskStatusEnum.Accepted,
            (int)TaskStatusEnum.Assigned,
            (int)TaskStatusEnum.Inprogress,
            (int)TaskStatusEnum.Started
        };

        public MainTaskQuery(
            ApplicationDbContext _context,
            IMapper mapper,
            ICurrentUser currentUser)
        {
            _currentUser = currentUser;
            _mapper = mapper;
            context = _context;
        }

        public async Task<PagedResult<MainTaskMobileResultWithTasksDTO>> GetDriverTasksAsync(PaginatedTasksDriverMobileInputDTO filter)
        {
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;
            filter.FromDate = filter.FromDate.HasValue ? filter.FromDate : DateTime.UtcNow;
            filter.ToDate = filter.ToDate.HasValue ? filter.ToDate : DateTime.UtcNow;

            var query = context.Tasks
                        .IgnoreQueryFilters()
                        .Where(task => task.DriverId == filter.DriverId
                            && notCompletedTaskStatues.Contains(task.TaskStatusId)
                            && !task.IsDeleted);
            if (!string.IsNullOrEmpty(filter.SearchBy))
            {
                query = query.Where(task => task.Customer.Name.Contains(filter.SearchBy)
                    || task.Customer.Address.Contains(filter.SearchBy)
                    || task.OrderId.Contains(filter.SearchBy));
            }
            if (filter.TaskStatusIds != null && filter.TaskStatusIds.Any())
            {
                query = query.Where(task => filter.TaskStatusIds.Contains(task.TaskStatusId));
            }
            if (filter.FromDate.HasValue)
            {
                filter.FromDate = filter.FromDate.Value.StartOfDay();
                query = query.Where(task => (task.PickupDate.HasValue && filter.FromDate.Value.Date <= task.PickupDate.Value.Date)
                    || (task.DeliveryDate.HasValue && filter.FromDate.Value.Date <= task.DeliveryDate.Value.Date));
            }
            if (filter.ToDate.HasValue)
            {
                filter.ToDate = filter.ToDate.Value.EndOfDay();
                query = query.Where(task => (task.PickupDate.HasValue && filter.ToDate.Value.Date >= task.PickupDate.Value.Date)
                    || (task.DeliveryDate.HasValue && filter.ToDate.Value.Date >= task.DeliveryDate.Value.Date));
            }

            var pagedResult = new PagedResult<MainTaskMobileResultWithTasksDTO>
            {
                TotalCount = await query.CountAsync(),
                Result = GroupForMainTasksForMobile(
                await query
                .Skip((filter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .OrderByDescending(task => task.MainTaskId)
                .ProjectTo<TasksResultWithMainTaskDTO>(_mapper.ConfigurationProvider)
                .ToListAsync())
            };

            return pagedResult;
        }

        public IQueryable<MainTaskResultDTO> GetDriverNewTasks(int DriverId)
        {
            return GetDriverNewTasksQuery(DriverId)
                .ProjectTo<MainTaskResultDTO>(_mapper.ConfigurationProvider)
                .OrderByDescending(x => x.Id);
        }

        public async Task<PagedResult<HistoryMainTaskMobileResultWithTasksDTO>> GetTaskHistoryDetailsAsync(PaginatedTasksDriverMobileInputDTO filter)
        {
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;
            filter.FromDate ??= DateTime.UtcNow;
            filter.ToDate ??= DateTime.UtcNow;

            if (filter.TaskStatusIds == null)
            {
                filter.TaskStatusIds = completedTaskStatues;
            }

            var query = context.Tasks
                .IgnoreQueryFilters()
                .Where(task => filter.TaskStatusIds.Contains(task.TaskStatusId)
                    && task.DriverId == filter.DriverId
                    && !task.IsDeleted);

            if (filter.Id > 0)
            {
                query = query.Where(tasks => filter.Id == tasks.Id);
            }
            if (!string.IsNullOrEmpty(filter.SearchBy))
            {
                query = query.Where(tasks => tasks.Customer.Name.ToLower().Trim().Contains(filter.SearchBy.ToLower().Trim())
                    || tasks.Customer.Address.ToLower().Trim().Contains(filter.SearchBy.ToLower().Trim()));
            }
            if (filter.FromDate.HasValue)
            {
                filter.FromDate = filter.FromDate.Value.StartOfDay();
                query = query.Where(tasks => tasks.PickupDate.HasValue && tasks.PickupDate.Value.Date >= filter.FromDate.Value.Date
                    || tasks.DeliveryDate.HasValue && tasks.DeliveryDate.Value.Date >= filter.FromDate.Value.Date);
            }
            if (filter.ToDate.HasValue)
            {
                filter.ToDate = filter.ToDate.Value.EndOfDay();
                query = query.Where(tasks => tasks.PickupDate.HasValue && tasks.PickupDate.Value.Date <= filter.ToDate.Value.Date
                    || tasks.DeliveryDate.HasValue && tasks.DeliveryDate.Value.Date <= filter.ToDate.Value.Date);
            }

            var pagedResult = new PagedResult<HistoryMainTaskMobileResultWithTasksDTO>
            {
                TotalCount = await query.CountAsync(),
                Result = BuildMainTaskHistoryDetails(
                await query
                .Skip((filter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ProjectTo<TaskHistoryDetailsResultWithMainTaskDTO>(_mapper.ConfigurationProvider)
                .ToListAsync())
            };

            return pagedResult;
        }


        private List<HistoryMainTaskMobileResultWithTasksDTO> BuildMainTaskHistoryDetails(List<TaskHistoryDetailsResultWithMainTaskDTO> tasks)
        {
            var mainTasks = new List<HistoryMainTaskMobileResultWithTasksDTO>();
            foreach (var task in tasks)
            {
                var existMainTask = mainTasks.FirstOrDefault(t => t.Id == task.MainTaskId);
                var taskwithoutMaintask = _mapper.Map<TaskHistoryDetailsResultWithMainTaskDTO, TaskHistoryDetailsResultDTO>(task);
                if (existMainTask != null)
                {
                    existMainTask.Tasks.Add(taskwithoutMaintask);
                    continue;
                }
                var mainTask = _mapper.Map<HistoryMainTaskMobileResultDTO, HistoryMainTaskMobileResultWithTasksDTO>(task.MainTask);
                mainTask.Tasks = new List<TaskHistoryDetailsResultDTO>() { taskwithoutMaintask };
                mainTasks.Add(mainTask);
            }

            return mainTasks;

        }

        private List<MainTaskMobileResultWithTasksDTO> GroupForMainTasksForMobile(List<TasksResultWithMainTaskDTO> tasks)
        {
            var mainTasks = new List<MainTaskMobileResultWithTasksDTO>();
            foreach (var task in tasks)
            {
                var taskwithoutMaintask = _mapper.Map<TasksResultWithMainTaskDTO, TasksResultDTO>(task);
                var existMainTask = mainTasks.FirstOrDefault(mainTask => mainTask.Id == task.MainTaskId);
                if (existMainTask != null)
                {
                    existMainTask.Tasks.Add(taskwithoutMaintask);
                    continue;
                }

                var mainTask = _mapper.Map<MainTaskMobileMiniResultDTO, MainTaskMobileResultWithTasksDTO>(task.MainTask);
                mainTask.Tasks = new List<TasksResultDTO>() { taskwithoutMaintask };
                mainTasks.Add(mainTask);
            }

            return mainTasks;
        }

        private IQueryable<MainTask> GetDriverNewTasksQuery(int DriverId)
        {
            return context.MainTask
                .Include(mainTask => mainTask.Tasks)
                .IgnoreQueryFilters()
                .Where(mainTask => mainTask.Tasks.Any(task => task.DriverId == DriverId
                    && task.TaskStatusId == (int)TaskStatusEnum.Assigned)
                    && (!mainTask.ExpirationDate.HasValue || mainTask.ExpirationDate.Value >= (DateTime.UtcNow + TimeSpan.FromSeconds(5))))
                .Where(task => !task.IsDeleted && task.Tenant_Id == _currentUser.TenantId
                    || (_currentUser.DriverId.HasValue
                        && task.Tasks.Any(subTask => subTask.DriverId == _currentUser.DriverId)
                        && string.IsNullOrEmpty(_currentUser.TenantId)));
        }
    }
}
