﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class MainTaskCountDTO
    {
        public int DriverId { get; set; }
        public double TotalMainTasksCount { get; set; }
        public double TotalOrderWeightCapacity { get; set; }

    }
}
