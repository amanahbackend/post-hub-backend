﻿using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{


    public class MainTaskMobileResultWithTasksDTO : MainTaskMobileMiniResultDTO
    {
        public List<TasksResultDTO> Tasks { set; get; }
    }

    public class MainTaskMobileMiniResultDTO
    {
        public int Id { set; get; }
        public int MainTaskTypeId { set; get; }
        public string MainTaskTypeName { set; get; }
        public bool? IsCompleted { set; get; }
        public bool? IsDelayed { set; get; }
        public int AssignmentType { get; set; }
        public int? RemainingTimeInSeconds { set; get; }
        public DateTime? ExpirationDate { get; set; }
        public bool IsNewlyAssigned { get; set; }
        public bool IsFailToAutoAssignDriver { get; set; }
        //TODO: Temp. return 1 until setting it is implemented
        public int OrderWeight { get; set; }
        public int? MainTaskStatusId { set; get; }
        public string MainTaskStatusName { set; get; }
    }

}
