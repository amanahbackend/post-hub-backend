﻿using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.BLL.Tasks.Services.DTOs
{
    public class SuccessOrFailTaskInputDTO
    {
        public int TaskId { set; get; }
        public int MainTaskId { set; get; }
        public string Notes { set; get; }
        public IFormFile SignatureForm { set; get; }
        public IFormFileCollection GallaryFiles { set; get; }

        public double Longitude { set; get; }
        public double Latitude { set; get; }
        public string Reason { set; get; }

    }
}
