﻿using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class TaskCalenderResultDTO
    {
        public DateTime? TaskDate { get; set; }
        public int CompletedCount { get; set; }
        public int PendingCount { get; set; }
        public double? TotalKM { get; set; }
        public double? TotalTime { get; set; }
    }
}
