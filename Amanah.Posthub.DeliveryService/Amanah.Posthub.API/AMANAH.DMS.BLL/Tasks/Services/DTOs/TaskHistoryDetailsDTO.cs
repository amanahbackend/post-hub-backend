﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.TasksFolder.ViewModels.MainTasksDTO
{
    public class TaskHistoryDetailsResultDTO
    {
        public DateTime? TaskDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string Notes { set; get; }
        public string SignatureFileName { set; get; }
        public string SignatureURL { set; get; }
        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }

        public int Id { get; set; }
        public int MainTaskId { set; get; }

        public int? TaskStatusId { set; get; }
        public string TaskStatusName { set; get; }
        public string Image { set; get; }
        public IFormFile FormImage { set; get; }
        public string OrderId { set; get; }
        public string Address { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public int CustomerId { get; set; }
        public DateTime? StartDate { get; set; }
        public double? DelayTime { get; set; }
        public DateTime? SuccessfulDate { get; set; }
        public double? TotalTime { get; set; }
        public double? TotalDistance { get; set; }
        public double? TotalTaskTime { get; set; }

        public List<TaskGallaryResultDTO> TaskGallaries { set; get; }
    }


    public class TaskHistoryDetailsResultWithMainTaskDTO : TaskHistoryDetailsResultDTO
    {
        public HistoryMainTaskMobileResultDTO MainTask { set; get; }
    }
}
