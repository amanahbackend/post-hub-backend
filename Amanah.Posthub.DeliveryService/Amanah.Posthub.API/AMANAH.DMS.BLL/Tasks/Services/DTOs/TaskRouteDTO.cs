﻿using FluentValidation;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class TaskRouteDTO
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int DriverId { set; get; }
        public int? TaskStatusId { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }

    public class TaskRouteDTOlValidator : AbstractValidator<TaskRouteDTO>
    {
        public TaskRouteDTOlValidator()
        {
            RuleFor(x => x.TaskId).NotEmpty().NotNull();
            RuleFor(x => x.DriverId).NotEmpty().NotNull();
            RuleFor(x => x.Latitude).NotEmpty().NotNull();
            RuleFor(x => x.Longitude).NotEmpty().NotNull();
        }
    }
}
