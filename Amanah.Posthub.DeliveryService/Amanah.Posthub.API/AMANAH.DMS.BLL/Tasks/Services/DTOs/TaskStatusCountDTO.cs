﻿namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class TaskStatusCountViewModel
    {
        public int TaskStatusId { set; get; }
        public string TaskStatusName { set; get; }
        public int Count { set; get; }
    }
}
