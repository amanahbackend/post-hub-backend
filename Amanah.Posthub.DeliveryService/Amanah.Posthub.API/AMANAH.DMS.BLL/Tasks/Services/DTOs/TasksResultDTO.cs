﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class TasksResultDTO
    {
        public int MainTaskId { set; get; }
        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }
        public int? DriverId { set; get; }
        public string DriverName { set; get; }
        public string DriverPhoneNumber { set; get; }
        public string DriverImageUrl { set; get; }
        public string TeamName { set; get; }
        public string Description { set; get; }
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int? GeoFenceId { get; set; }
        public bool? IsTaskReached { get; set; }
        public DateTime? ReachedTime { get; set; }
        public TimeViewModel TotalWaitingTime { get; set; }
        public TimeViewModel TotalEstimationTime { get; set; }

        public CustomerResultDTO Customer { set; get; }


        public string Notes { set; get; }
        public string SignatureFileName { set; get; }
        public string SignatureURL { set; get; }

        public double? TotalDistance { get; set; }
        public double? TotalTaskTime { get; set; }

        public List<TasksFolder.ViewModels.MainTasksDTO.TaskGallaryResultDTO> TaskGallaries { set; get; }





        public int Id { get; set; }
        public int? TaskStatusId { set; get; }
        public string TaskStatusName { set; get; }
        public string Image { set; get; }
        public IFormFile FormImage { set; get; }
        public string OrderId { set; get; }
        public string Address { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public int CustomerId { get; set; }
        public DateTime? StartDate { get; set; }
        public double? DelayTime { get; set; }
        public DateTime? SuccessfulDate { get; set; }
        public double? TotalTime { get; set; }
    }



    public class TasksResultWithMainTaskDTO : TasksResultDTO
    {
        public MainTaskMobileMiniResultDTO MainTask { set; get; }
    }

    public class CustomerResultDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public string Tags { set; get; }
        public int? CountryId { set; get; }
        public int? BranchId { set; get; }
    }
}
