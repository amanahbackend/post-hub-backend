﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class TravelSummeryResultDTO
    {
        public int TasksCount { get; set; }
        public double TasksHours { get; set; }
        public double? TasksKMs { get; set; }
        public List<TaskRouteDTO> TasksRoutes { get; set; }
    }
}
