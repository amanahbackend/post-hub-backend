﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Tasks.WebServices;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Domain.SubTasks.Repositories;
using Amanah.Posthub.Domain.TaskHistorys.Repositories;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.TasksFolder.Queries
{
    public interface IMobileMainTaskService
    {
        Task<ProcessResult<string>> DriverAcceptOrDeclineOperationAsync(int id, bool OperationIsAcceptNorDeclined, double? longitude = null, double? latitude = null);
    }
    public class MobileMainTaskService : IMobileMainTaskService
    {

        private readonly ICurrentUser _currentUser;
        private readonly ILocalizer _localizer;

        private readonly ITaskCommonService _taskCommonService;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IMainTaskRepository _mainTaskRepository;
        private readonly ISubTaskRepository _subTaskRepository;
        private readonly ITaskDriverRequestRepository _taskDriverRequestRepository;
        private readonly ITaskHistoryRepository _taskHistoryRepository;
        private readonly IBackGroundNotificationSercvice _notification;

        public MobileMainTaskService(
             ILocalizer localizer,
            ITaskCommonService taskCommonService,
            ICurrentUser currentUser,
            IUnitOfWork unityOfWork,
            IMainTaskRepository mainTaskRepository,
            ISubTaskRepository subTaskRepository,
            ITaskDriverRequestRepository taskDriverRequestRepository,
            ITaskHistoryRepository taskHistoryRepository,
            IBackGroundNotificationSercvice notification)
        {
            _taskCommonService = taskCommonService;
            _currentUser = currentUser;
            _unityOfWork = unityOfWork;
            _mainTaskRepository = mainTaskRepository;
            _subTaskRepository = subTaskRepository;
            _taskDriverRequestRepository = taskDriverRequestRepository;
            _taskHistoryRepository = taskHistoryRepository;
            _localizer = localizer;
            _notification = notification;
        }


        public async Task<ProcessResult<string>> DriverAcceptOrDeclineOperationAsync(
            int id,
            bool OperationIsAcceptNorDeclined,
            double? longitude = null,
            double? latitude = null)
        {
            ProcessResult<string> result = new ProcessResult<string>();
            var loggedInDriverId = _currentUser.DriverId;

            var dbMainTask = await _mainTaskRepository.GetAllIQueryable()
                .Include(maintask => maintask.Tasks).ThenInclude(maintask => maintask.TaskStatus)
                .Where(maintask => maintask.Id == id && maintask.Tasks.First().DriverId == loggedInDriverId)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == _currentUser.TenantId ||
                    (_currentUser.DriverId.HasValue &&
                     task.Tasks.Any(subTask => subTask.DriverId == _currentUser.DriverId) &&
                     string.IsNullOrEmpty(_currentUser.TenantId)))
                .FirstOrDefaultAsync();

            var taskDriverRequest = await _taskDriverRequestRepository.GetAllIQueryable()
                .Where(maintask => maintask.MainTaskId == id && maintask.DriverId == loggedInDriverId)
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync();


            TaskStatusEnum wantedOperation_AcceptOrDeclined = OperationIsAcceptNorDeclined
                ? TaskStatusEnum.Accepted
                : TaskStatusEnum.Declined;

            TaskDriverResponseStatusEnum driverResponceStatus = OperationIsAcceptNorDeclined
                ? TaskDriverResponseStatusEnum.Accepted
                : TaskDriverResponseStatusEnum.Declined;

            var validationResult = OperationIsAcceptNorDeclined
                ? DriverAcceptValidation(dbMainTask, taskDriverRequest)
                : DriverDeclineValidation(dbMainTask, taskDriverRequest);

            if (!validationResult.IsSucceeded || !validationResult.ReturnData)
            {
                result = validationResult.Fail<string>();
                return result;
            }

            await _unityOfWork.RunTransaction(async () =>
            {
                taskDriverRequest.ResponseStatus = (int)driverResponceStatus;
                _taskDriverRequestRepository.Update(taskDriverRequest);

                foreach (var task in dbMainTask.Tasks.Where(x => x.DriverId == loggedInDriverId).ToList())
                {
                    _taskHistoryRepository.Add(new TaskHistory(dbMainTask.Tenant_Id)
                    {
                        TaskId = task.Id,
                        ActionName = wantedOperation_AcceptOrDeclined.ToString().ToUpper(),
                        FromStatusId = task.TaskStatusId,
                        ToStatusId = (int)wantedOperation_AcceptOrDeclined,
                        Longitude = longitude,
                        Latitude = latitude,
                        MainTaskId = task.MainTaskId,
                    });
                    task.TaskStatusId = (int)wantedOperation_AcceptOrDeclined;
                    task.DriverId = OperationIsAcceptNorDeclined ? _currentUser.DriverId : null;
                    _subTaskRepository.Update(task);
                }
                dbMainTask = _taskCommonService.UpdateMainTaskStatus(dbMainTask, wantedOperation_AcceptOrDeclined);
                int changes = await _unityOfWork.SaveChangesAsync();
            });

            _notification.SendToUserManagers(dbMainTask.Tenant_Id, _currentUser.Id, "TaskAccept",
               _localizer[Keys.Notifications.DriverAcceptedTask],
               _localizer.Format(Keys.Notifications.DriverNameAcceptedTaskNo, "Driver", id), id);

            result.Success(dbMainTask.Tenant_Id);
            return result;
        }


        private ProcessResult<bool> DriverAcceptValidation(MainTask dbMainTask, TaskDriverRequests taskDriverRequest)
        {
            ProcessResult<bool> validation = new ProcessResult<bool>();
            if (dbMainTask == null)
            {
                validation.Fail(Keys.Validation.NoMainTaskWithThisId);
                return validation;
            }
            if (taskDriverRequest == null)
            {
                validation.Fail(Keys.Validation.NoMainTaskWithThisId);
                return validation;
            }

            if (taskDriverRequest.ResponseStatus == (int)TaskDriverResponseStatusEnum.Expired ||
                    taskDriverRequest.ResponseStatus == (int)TaskDriverResponseStatusEnum.Declined ||
                    taskDriverRequest.ResponseStatus == (int)TaskDriverResponseStatusEnum.Canceled)
            {
                validation.Fail(_localizer[Keys.Messages.TaskRequestIsFailed]);

                return validation;
            }
            validation.Success(true);
            return validation;
        }

        private ProcessResult<bool> DriverDeclineValidation(MainTask dbMainTask, TaskDriverRequests taskDriverRequest)
        {
            ProcessResult<bool> validation = new ProcessResult<bool>();

            if (dbMainTask == null)
            {
                validation.Fail(Keys.Validation.NoMainTaskWithThisId);
                return validation;
            }
            if (taskDriverRequest == null)
            {
                validation.Fail(Keys.Validation.NoMainTaskWithThisId);
                return validation;
            }
            validation.Success(true);
            return validation;
        }

    }
}
