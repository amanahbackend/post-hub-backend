﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Managers.TaskAssignment;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.Tasks.WebServices.DTOs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Domain.SubTasks.Repositories;
using Amanah.Posthub.Domain.TaskHistorys.Repositories;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MapsUtilities;
using MapsUtilities.MapsManager;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using TaskStatusEnum = Amanah.Posthub.BLL.Enums.TaskStatusEnum;

namespace Amanah.Posthub.BLL.TasksFolder.Queries
{

    public interface INearestAvailableMethods
    {
        Task<MainTask> GetMainTaskForStatusAndIncludeTasks(int mainTaskId);
        Task<GetTaskGeofencesOutput> GetGeofencesForTask(MainTask mainTask, TaskSettingInputDTO Settings);
        void UpdateTaskDriver(
                    MainTask mainTask,
                    int? driverId,
                    int? expirationIntervalInSeconds,
                    TaskStatusEnum? status,
                    TaskAssignmentType? assignmentType,
                    DateTime? expirationDate,
                    DateTime? reachedTime);
        Task<bool> AutoAllocationSucssesfullAsync(MainTask dbMainTask);
        void AddDriverTaskNotification(SubTask sunTask, string msg, int notificationType = 0);
        Task SendPushNotificationToDriverAsync(string msg, string driverUserId);
        Task SendPushNotificationToDriverAsync(string msg, string driverUserId, object objectToSend);
        Task<bool> AutoAllocationFailAsync(int mainTaskId);

        Task<List<DriverNearestAvailableDTO>> SortDriversAccordingToGoogleByNearestAsync(MainTask mainTask, List<DriverNearestAvailableDTO> drivers);
        IQueryable<Driver> GetAvailablePlatformDrivers();
        IQueryable<Driver> GetAvailableDrivers(MainTaskAutoAssignInputDTO task);

        List<DriverNearestAvailableDTO> SortAndFilterAvailableDriversByRedisDistance(MainTask task,
            List<DriverNearestAvailableDTO> availableDrivers, Setting radiusSetting = null);
    }
    public class NearestAvailableMethods : INearestAvailableMethods
    {
        protected readonly IConfiguration _configuration;
        protected readonly MapRequestSettings _mapsSettings;
        private readonly ICurrentUser _currentUser;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IMainTaskRepository _mainTaskRepository;
        private readonly IRepository<GeoFence> _geofenceRepository;
        private readonly IRepository<DriverTaskNotification> _driverTaskNotificationRepository;
        private readonly ISubTaskRepository _subTaskRepository;
        private readonly ITaskDriverRequestRepository _taskDriverRequestRepository;
        private readonly ITaskHistoryRepository _taskHistoryRepository;
        private readonly IMapper _mapper;
        private readonly ILocalizer _localizer;
        private readonly IBackGroundNotificationSercvice _notificationService;
        protected readonly IDriverRepository _driverRepo;
        protected readonly IMapsManager _mapsManager;

        public NearestAvailableMethods(
            IConfiguration configuration,
            IMapsManager mapsManager,
            IDriverRepository driverRepo,
            ICurrentUser currentUser,
            IUnitOfWork unityOfWork,
            IMainTaskRepository mainTaskRepository,
            ISubTaskRepository subTaskRepository,
            ITaskDriverRequestRepository taskDriverRequestRepository,
            ITaskHistoryRepository taskHistoryRepository,
            IRepository<GeoFence> geofenceRepository,
             IMapper mapper,
            ILocalizer localizer,
            IBackGroundNotificationSercvice notificationService,

             IRepository<DriverTaskNotification> driverTaskNotificationRepository
            )
        {
            _mapsManager = mapsManager;
            _driverRepo = driverRepo;
            _notificationService = notificationService;
            _localizer = localizer;
            _currentUser = currentUser;
            _unityOfWork = unityOfWork;
            _mainTaskRepository = mainTaskRepository;
            _subTaskRepository = subTaskRepository;
            _taskDriverRequestRepository = taskDriverRequestRepository;
            _taskHistoryRepository = taskHistoryRepository;
            _geofenceRepository = geofenceRepository;
            _mapper = mapper;
            _driverTaskNotificationRepository = driverTaskNotificationRepository;
            _configuration = configuration;
            _mapsSettings = new MapRequestSettings
            {
                Priority = MapPriority.Default,
                GoogleApiKey = _configuration["MapsSettings:GoogleKey"],
                BingApiKey = _configuration["MapsSettings:BingKey"]
            };
        }

        public async Task<MainTask> GetMainTaskForStatusAndIncludeTasks(int mainTaskId)
        {
            return await _mainTaskRepository.GetAllIQueryable().Include(x => x.Tasks).FirstOrDefaultAsync(e => e.Id == mainTaskId);
        }


        public async Task<GetTaskGeofencesOutput> GetGeofencesForTask(MainTask mainTask, TaskSettingInputDTO Settings)
        {
            Dictionary<MainTask, GetTaskGeofencesOutput> geofencesByTask;
            if (Settings.RestrictGeofences)
            {
                geofencesByTask = await GetGeofencesForTasksAsync(new List<MainTask> { mainTask });
            }
            else
            {
                geofencesByTask = await GetGeofencesAsync(new List<MainTask> { mainTask });
            }
            return geofencesByTask?.FirstOrDefault().Value;
        }



        async Task<Dictionary<MainTask, GetTaskGeofencesOutput>> GetGeofencesForTasksAsync(IEnumerable<MainTask> mainTasks)
        {
            var geofencesByTask = new Dictionary<MainTask, GetTaskGeofencesOutput>();
            var geoFences = await _geofenceRepository.GetAllIQueryable().ProjectTo<GeoFenceResultDTO>(_mapper.ConfigurationProvider).ToListAsync();

            foreach (var mainTask in mainTasks)
            {
                var output = new GetTaskGeofencesOutput();
                var pickupTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup);
                var deliveryTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery);
                if (!pickupTask.Latitude.HasValue || !pickupTask.Longitude.HasValue ||
                    !deliveryTask.Latitude.HasValue || !deliveryTask.Longitude.HasValue)
                    throw new ArgumentNullException("Latitude and longitude can't be null");

                var pickupPoint = new MapPoint
                {
                    Latitude = pickupTask.Latitude.Value,
                    Longitude = pickupTask.Longitude.Value
                };
                var deliveryPoint = new MapPoint
                {
                    Latitude = deliveryTask.Latitude.Value,
                    Longitude = deliveryTask.Longitude.Value
                };
                foreach (var geoFence in geoFences)
                {
                    var polygon = geoFence.Locations
                        .Select(l =>
                            new MapPoint
                            {
                                Latitude = l.Latitude ?? 0,
                                Longitude = l.Longitude ?? 0
                            })
                        .ToArray();

                    if (MapsHelper.IsPointInPolygon(polygon, pickupPoint))
                    {
                        output.PickupGeoFences.Add(geoFence);
                    }
                    if (MapsHelper.IsPointInPolygon(polygon, deliveryPoint))
                    {
                        output.DeliveryGeoFences.Add(geoFence);
                    }
                }

                geofencesByTask.Add(mainTask, output);
            }

            return geofencesByTask;
        }
        async Task<Dictionary<MainTask, GetTaskGeofencesOutput>> GetGeofencesAsync(IEnumerable<MainTask> mainTasks)
        {
            var geofencesByTask = new Dictionary<MainTask, GetTaskGeofencesOutput>();
            var geoFences = await _geofenceRepository.GetAllIQueryable().ProjectTo<GeoFenceResultDTO>(_mapper.ConfigurationProvider).ToListAsync();
            foreach (var mainTask in mainTasks)
            {
                var output = new GetTaskGeofencesOutput();
                var pickupTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup);
                var deliveryTask = mainTask.Tasks.FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery);
                foreach (var geoFence in geoFences)
                {
                    var polygon = geoFence.Locations
                        .Select(geoFenceLocationVioewModel =>
                            new MapPoint
                            {
                                Latitude = geoFenceLocationVioewModel.Latitude ?? 0,
                                Longitude = geoFenceLocationVioewModel.Longitude ?? 0
                            })
                        .ToArray();
                    output.PickupGeoFences.Add(geoFence);
                    output.DeliveryGeoFences.Add(geoFence);
                }

                geofencesByTask.Add(mainTask, output);
            }

            return geofencesByTask;
        }


        public void UpdateTaskDriver(
            MainTask mainTask,
            int? driverId,
            int? expirationIntervalInSeconds,
            TaskStatusEnum? status,
            TaskAssignmentType? assignmentType,
            DateTime? expirationDate,
            DateTime? reachedTime)
        {
            foreach (var task in mainTask.Tasks)
            {
                task.DriverId = driverId;
                if (driverId == null)
                {
                    task.TaskStatusId = (int)TaskStatusEnum.Unassigned;
                }
                else if (status != null)
                {


                    task.TaskStatusId = (int)status;

                    _taskHistoryRepository.Add(new TaskHistory(task.Tenant_Id)
                    {
                        TaskId = task.Id,
                        ActionName = "ASSIGNED",
                        FromStatusId = task.TaskStatusId,
                        ToStatusId = (int)TaskStatusEnum.Assigned,
                        MainTaskId = task.MainTaskId,
                    });


                }
                if (reachedTime.HasValue)
                {
                    task.ReachedTime = reachedTime;
                    task.IsTaskReached = true;
                }
                _subTaskRepository.Update(task);

            }

            mainTask.AssignmentType = (int?)assignmentType ?? mainTask.AssignmentType;
            mainTask.ExpirationDate = expirationDate ?? mainTask.ExpirationDate;

            if (mainTask.ExpirationDate.HasValue && expirationIntervalInSeconds.HasValue)
            {
                mainTask.ExpirationDate = DateTime.UtcNow.AddSeconds(expirationIntervalInSeconds.Value);
            }
            _mainTaskRepository.Update(mainTask);
        }


        public async Task<bool> AutoAllocationSucssesfullAsync(MainTask dbMainTask)
        {
            var CreatedUserName = _currentUser.UserName;
            var title = _localizer[Keys.Notifications.TaskAutoAllocationSucceded];
            var body = _localizer.Format(Keys.Notifications.TaskAutoAllocationSuccededDetailsTaskId, dbMainTask.Id);
            _notificationService.SendToUserAndTenantAdmin(_currentUser.Id, "AutoAllocationSucessfully", title, body, dbMainTask.Id);
            return true;
        }

        public void AddDriverTaskNotification(SubTask task, string msg, int notificationType = 0)
        {
            _driverTaskNotificationRepository.Add(new DriverTaskNotification
            {
                TaskId = task.Id,
                DriverId = task.DriverId ?? 0,
                Description = "Task number " + task.Id + " " + msg,
                ActionStatus = "Assigned"
            });
        }

        public async Task SendPushNotificationToDriverAsync(string msg, string driverUserId)
        => await SendPushNotificationToDriverAsync(msg, driverUserId, new { notificationType = NotificationTypeEnum.NewTask });
        public async Task SendPushNotificationToDriverAsync(string msg, string driverUserId, object objectToSend)
        {
            _notificationService.SendToUser(
                             driverUserId,
                             msg,
                             "D-Hub",
                             msg, objectToSend);
        }

        public async Task<bool> AutoAllocationFailAsync(int mainTaskId)
        {
            var dbMainTask = await _mainTaskRepository.GetAllIQueryable().FirstOrDefaultAsync(x => x.Id == mainTaskId);

            dbMainTask.IsFailToAutoAssignDriver = true;
            await _unityOfWork.RunTransaction(async () =>
            {
                _mainTaskRepository.Update(dbMainTask);
                await _unityOfWork.SaveChangesAsync();
            });

            var title = _localizer[Keys.Notifications.TaskAutoAllocationFailed];
            var body = _localizer.Format(Keys.Notifications.TaskAutoAllocationFailedDetailsTaskId, mainTaskId);
            _notificationService.SendToUserAndTenantAdmin(_currentUser.Id, "AutoAllocationFailed", title, body, mainTaskId);

            return true;
        }

        public IQueryable<Driver> GetAvailableDrivers(MainTaskAutoAssignInputDTO task)
        {
            var input = new GetAailableDriversInput
            {
                TeamIds = task.Settings?.TeamIds,
                Tags = task.Settings?.Tags,
                PickupGeoFenceIds = task.Settings?.PickupGeoFenceIds,
                DeliveryGeoFenceIds = task.Settings?.DeliveryGeoFenceIds
            };
            var driversQuery = _driverRepo.GetAllIQueryable()
                //context.Driver.Include(x => x.Team)
                .Where(x => x.AgentStatusId == (int)AgentStatusesEnum.Available);

            if (input.TeamIds != null && input.TeamIds.Any())
            {
                driversQuery = driversQuery.Where(driver => input.TeamIds.Contains(driver.TeamId));
            }
            if (input.Tags != null && input.Tags.Any())
            {
                var tagsPredicate = input.Tags
                    .Select(tag =>
                    {
                        Expression<Func<Driver, bool>> result =
                            driver => driver.Tags.ToLower().Contains(tag.ToLower());
                        return result;
                    })
                    .Aggregate((total, next) => total.Or(next));
                driversQuery = driversQuery.Where(tagsPredicate);
            }
            if (input.PickupGeoFenceIds != null && input.PickupGeoFenceIds.Any())
            {
                driversQuery = driversQuery.Where(driver =>
                    driver.AllPickupGeoFences ||
                    driver.DriverPickUpGeoFences.Any(pickupGeofence =>
                        input.PickupGeoFenceIds.Contains(pickupGeofence.GeoFenceId)));
            }
            if (input.DeliveryGeoFenceIds != null)
            {
                driversQuery = driversQuery.Where(driver =>
                    driver.AllDeliveryGeoFences ||
                    driver.DriverDeliveryGeoFences.Any(deliveryGeofence =>
                        input.DeliveryGeoFenceIds.Contains(deliveryGeofence.GeoFenceId)));
            }

            //var result = await driversQuery.AsNoTracking()
            //    .ProjectTo<DriverViewModel>(mapper.ConfigurationProvider)
            //    .ToListAsync();

            return driversQuery;
        }
        public IQueryable<Driver> GetAvailablePlatformDrivers()
        {
            var result = _driverRepo.GetAllIQueryable()
            .IgnoreQueryFilters()
            .Where(driver => driver.AgentStatusId == (int)AgentStatusesEnum.Available &&
                !driver.IsDeleted &&
                string.IsNullOrEmpty(driver.Tenant_Id) &&
                driver.AllPickupGeoFences &&
                driver.AllDeliveryGeoFences)
            .Where(driver => driver.CurrentLocation != null &&
                driver.CurrentLocation.Latitude.HasValue &&
                driver.CurrentLocation.Longitude.HasValue)
            .AsNoTracking();
            return result;
        }

        /// <summary>
        /// /Contact Other Remote Api To Get Real distance between task and point
        /// </summary>
        /// <param name="mainTask"></param>
        /// <param name="drivers"></param>
        /// <returns></returns>
        public async Task<List<DriverNearestAvailableDTO>> SortDriversAccordingToGoogleByNearestAsync(MainTask mainTask, List<DriverNearestAvailableDTO> drivers)
        {
            if (mainTask.Tasks == null || !mainTask.Tasks.Any())
            {
                throw new ArgumentException($"Main {mainTask.Id}task has no child tasks");
            }

            foreach (var driver in drivers)
            {
                if (!mainTask.Tasks.First().Latitude.HasValue ||
                    !mainTask.Tasks.First().Longitude.HasValue ||
                    !driver.Latitude.HasValue ||
                    !driver.Longitude.HasValue)
                {
                    throw new ArgumentNullException("Latitude and longitude can't be null");
                }
                var dist = await _mapsManager.GetMinDurationOrDistance(
                     new MapPoint { Latitude = mainTask.Tasks.First().Latitude.Value, Longitude = mainTask.Tasks.First().Longitude.Value },
                     new MapPoint { Latitude = driver.Latitude.Value, Longitude = driver.Longitude.Value },
                     _mapsSettings);

                if (dist.Success)
                    driver.DistanceToTaskRealRoute = dist.MinDurationOrDistance;
                else
                    driver.DistanceToTaskRealRoute = null;

            }
            drivers = drivers.Where(x => x.DistanceToTaskRealRoute.HasValue)
                .OrderBy(x => x.DistanceToTaskRealRoute)
                .ToList();

            return drivers;
        }

        public List<DriverNearestAvailableDTO> SortAndFilterAvailableDriversByRedisDistance(MainTask task,
            List<DriverNearestAvailableDTO> availableDrivers, Setting radiusSetting = null)
        {
            double radius = double.MaxValue;
            if (radiusSetting != null)
                double.TryParse(radiusSetting.Value, out radius);

            var filteredDrivers = new List<DriverNearestAvailableDTO>();
            foreach (var driver in availableDrivers)
            {

                var fTask = task.Tasks.First();
                if (!fTask.Latitude.HasValue || !fTask.Longitude.HasValue)
                    throw new ArgumentNullException("Latitude and longitude can't be null");

                var d = MapsHelper.GetDistanceBetweenPoints(fTask.Latitude.Value, fTask.Longitude.Value, driver.Latitude ?? 0, driver.Longitude ?? 0);
                //Convert to km
                driver.DistanceToTaskAsStraight = d / 1000;

                if (driver.DistanceToTaskAsStraight <= radius)
                {
                    filteredDrivers.Add(driver);
                }
            }
            filteredDrivers = filteredDrivers
                .OrderBy(x => x.DistanceToTaskAsStraight)
                .ToList();

            return filteredDrivers;
        }

    }
}
