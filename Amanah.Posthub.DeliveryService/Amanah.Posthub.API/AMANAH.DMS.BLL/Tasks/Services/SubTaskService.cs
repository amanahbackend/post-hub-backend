﻿using Amanah.Posthub.BASE;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.Tasks.Services.DTOs;
using Amanah.Posthub.BLL.Tasks.WebServices;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Domain.SubTasks.Repositories;
using Amanah.Posthub.Domain.TaskHistorys.Repositories;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MapsUtilities;
using MapsUtilities.MapsManager;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;
using TaskStatusEnum = Amanah.Posthub.BLL.Enums.TaskStatusEnum;

namespace Amanah.Posthub.BLL.TasksFolder.Queries
{
    public interface IMobileSubTaskService
    {
        Task<ProcessResult<string>> SetReachedAsync(int taskId, double longitude, double latitude);
        Task<KeyValuePair<bool, string>> StartAsync(int id, double longitude, double latitude);
        Task<string> MarkSubTaskAsSussessOrFailAsync(SuccessOrFailTaskInputDTO successOrFailTask, int taskStatusId);

        Task<string> MarkSubTaskAsCancelOrDeclineAsync(int id, TaskStatusEnum cancelOrDeclined, double longitude, double latitude, string reason = null);
    }
    public class MobileSubTaskService : IMobileSubTaskService
    {
        private readonly IConfiguration _configuration;
        private readonly ILocalizer _localizer;
        private readonly ICurrentUser _currentUser;
        private readonly IMapper _mapper;
        private readonly IRepository<Setting> _settingRepository;
        private readonly ITaskCommonService _taskCommonService;

        private readonly IUnitOfWork _unityOfWork;
        private readonly IMainTaskRepository _mainTaskRepository;
        private readonly ISubTaskRepository _subTaskRepository;
        private readonly ITaskGallaryRepository _taskGallaryRepository;
        private readonly ITaskHistoryRepository _taskHistoryRepository;
        private readonly IDriverRepository _driverRepository;
        private readonly IRepository<TaskRoute> _taskRouteRepository;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IMapsManager _mapsManager;
        private readonly MapRequestSettings _mapsSettings;

        private string TaskSignaturesPath { get; set; }
        private string TaskGallaryPath { get; set; }

        public MobileSubTaskService(
            IConfiguration configuration,
            IMapper mapper,
            ILocalizer localizer,
            ICurrentUser currentUser,
            ITaskGallaryRepository taskGallaryRepository,
            IUnitOfWork unityOfWork,
            IMainTaskRepository mainTaskRepository,
            ISubTaskRepository subTaskRepository,
            ITaskHistoryRepository taskHistoryRepository,
            IDriverRepository driverRepository,
            IRepository<TaskRoute> taskRouteRepo,
            IUploadFormFileService uploadFormFileService,
            IMapsManager mapsManager,
            IRepository<Setting> settingRepository,
            ITaskCommonService taskCommonService)
        {
            _taskGallaryRepository = taskGallaryRepository;
            _configuration = configuration;
            _driverRepository = driverRepository;
            _localizer = localizer;
            _currentUser = currentUser;
            _mapper = mapper;
            _settingRepository = settingRepository;
            _unityOfWork = unityOfWork;

            _mainTaskRepository = mainTaskRepository;
            _subTaskRepository = subTaskRepository;
            _taskHistoryRepository = taskHistoryRepository;
            _taskRouteRepository = taskRouteRepo;
            _uploadFormFileService = uploadFormFileService;
            _mapsManager = mapsManager;
            _taskCommonService = taskCommonService;

            _mapsSettings = new MapRequestSettings
            {
                Priority = MapPriority.Default,
                GoogleApiKey = _configuration["MapsSettings:GoogleKey"],
                BingApiKey = _configuration["MapsSettings:BingKey"]
            };
            TaskSignaturesPath = _configuration.GetValue<string>("TaskSignaturesPath");
            TaskGallaryPath = _configuration.GetValue<string>("TaskGallaryPath");
        }

        public async Task<ProcessResult<string>> SetReachedAsync(int taskId, double longitude, double latitude)
        {
            var result = new ProcessResult<string>();

            var existedTask = await _subTaskRepository.GetAllIQueryable(task => task.Id == taskId
                && _currentUser.DriverId.HasValue && task.DriverId == _currentUser.DriverId)
                .Include(task => task.Driver).ThenInclude(driver => driver.CurrentLocation)
                .IgnoreQueryFilters()
                .AsNoTracking()
                .FirstOrDefaultAsync();
            if (existedTask == null)
            {
                return result.Fail(_localizer[Keys.Validation.CurrentUserIsNotDriver]);
            }

            var distance = MapsHelper.GetDistanceBetweenPoints(existedTask.Latitude.Value, existedTask.Longitude.Value, latitude, longitude);
            double allowedDistanceInMeter = await GetAllowedDistanceInMeterAsync();

            if (distance <= allowedDistanceInMeter)
            {
                await _unityOfWork.RunTransaction(async () =>
                {
                    if (existedTask.DriverId.HasValue)
                    {
                        var exsitedTaskDriver = existedTask.Driver;
                        exsitedTaskDriver.AgentStatusId = await _taskCommonService.CheckIfDriverHasOnGoingTasksAsync(existedTask.DriverId.Value)
                            ? (int)AgentStatusesEnum.Busy
                            : (int)AgentStatusesEnum.Available;
                        exsitedTaskDriver.ReachedTime = DateTime.UtcNow;
                        exsitedTaskDriver.IsInClubbingTime = false;
                        exsitedTaskDriver.ClubbingTimeExpiration = null;
                        if (exsitedTaskDriver.CurrentLocation == null)
                        {
                            exsitedTaskDriver.CurrentLocation = new DriverCurrentLocation(exsitedTaskDriver.Tenant_Id);
                        }
                        exsitedTaskDriver.CurrentLocation.BranchId = existedTask.BranchId;
                        exsitedTaskDriver.CurrentLocation.Latitude = latitude;
                        exsitedTaskDriver.CurrentLocation.Longitude = longitude;
                        _driverRepository.Update(exsitedTaskDriver);
                    }
                    existedTask.IsTaskReached = true;
                    existedTask.ReachedTime = DateTime.UtcNow;
                    _subTaskRepository.Update(existedTask);
                    await _unityOfWork.SaveChangesAsync();
                });

                return result.Success(_localizer.Format(Keys.Validation.YouReachedPickupLocation));
            }
            return result.Fail(_localizer.Format(Keys.Validation.YouDidNotReachPickupLocation));
        }
        public async Task<KeyValuePair<bool, string>> StartAsync(int id, double longitude, double latitude)
        {
            float mainTaskTime = 0;
            var existedTask = await _subTaskRepository.GetSingleForStartAsync(id);
            if (existedTask == null)
            {
                return new KeyValuePair<bool, string>(false, _localizer[Keys.Validation.TaskNotFound]);
            }
            if (existedTask.TaskTypeId == (int)TaskTypesEnum.Pickup && !existedTask.ReachedTime.HasValue)
            {
                return new KeyValuePair<bool, string>(false, _localizer[Keys.Validation.CanNotStartTaskNotInLocation]);
            }
            if (existedTask.TaskStatusId == (int)TaskStatusEnum.Started)
            {
                return new KeyValuePair<bool, string>(false, _localizer[Keys.Validation.TaskActionAlreadyDoneBefore]);
            }
            var oldStatusId = existedTask.TaskStatusId;
            var existedTaskDriver = existedTask.Driver;

            await _unityOfWork.RunTransaction(async () =>
            {
                if (existedTaskDriver != null)
                {
                    existedTaskDriver.AgentStatusId = (int)AgentStatusesEnum.Busy;
                    existedTaskDriver.ReachedTime = null;
                    existedTaskDriver.IsInClubbingTime = false;
                    existedTaskDriver.ClubbingTimeExpiration = null;
                    if (existedTaskDriver.CurrentLocation == null)
                    {
                        existedTaskDriver.CurrentLocation = new DriverCurrentLocation(existedTaskDriver.Tenant_Id);
                    }
                    existedTaskDriver.CurrentLocation.Longitude = longitude;
                    existedTaskDriver.CurrentLocation.Latitude = latitude;
                    _driverRepository.Update(existedTaskDriver);
                }

                var otherMaintaskTasks = await _subTaskRepository.GetAllIQueryable(task => task.MainTaskId == existedTask.MainTaskId
                    && !task.IsDeleted && task.Id != existedTask.Id
                    && _currentUser.DriverId.HasValue && _currentUser.DriverId == task.DriverId)
                .ToListAsync();
                foreach (var task in otherMaintaskTasks)
                {
                    var duration = await _mapsManager.GetMinDuration(new MapPoint { Latitude = latitude, Longitude = longitude },
                            new MapPoint { Latitude = task.Latitude.Value, Longitude = task.Longitude.Value },
                            existedTask.Driver.TransportTypeId,
                            _mapsSettings);

                    task.EstimatedTime = duration.MinDurationOrDistance;
                    _subTaskRepository.Update(task);

                    mainTaskTime += duration.MinDurationOrDistance;
                }

                _taskHistoryRepository.Add(new TaskHistory(existedTask.Tenant_Id)
                {
                    TaskId = existedTask.Id,
                    ActionName = TaskStatusEnum.Started.ToString().ToUpper(),
                    FromStatusId = oldStatusId,
                    ToStatusId = (int)TaskStatusEnum.Started,
                    Longitude = longitude,
                    Latitude = latitude,
                });
                await SetTaskDistanceHoursAsync(existedTask);
                _taskRouteRepository.Add(new TaskRoute()
                {
                    TaskId = existedTask.Id,
                    DriverId = existedTask.DriverId ?? 0,
                    Longitude = longitude,
                    Latitude = latitude,
                    TaskStatusId = (int)TaskStatusEnum.Started,
                });

                existedTask.TaskStatusId = (int)TaskStatusEnum.Started;
                existedTask = SetIsDelayedTask(existedTask);
                existedTask.MainTask.EstimatedTime = mainTaskTime;
                _subTaskRepository.Update(existedTask);
                await _taskCommonService.UpdateMainTaskStatusAsync(existedTask.MainTaskId, TaskStatusEnum.Started, existedTask.Id);
                await _unityOfWork.SaveChangesAsync();
            });
            //TODO handle notification here 

            return new KeyValuePair<bool, string>(true, existedTask.Tenant_Id);
        }
        public async Task<string> MarkSubTaskAsSussessOrFailAsync(SuccessOrFailTaskInputDTO successOrFailTask, int taskStatusId)
        {
            var existedTask = await _subTaskRepository.GetAllIQueryable(task => task.Id == successOrFailTask.TaskId
                    && !task.IsDeleted && _currentUser.DriverId.HasValue && _currentUser.DriverId == task.DriverId)
                .Include(task => task.Driver).ThenInclude(driver => driver.User)
                .Include(task => task.TaskHistories)
                .IgnoreQueryFilters()
                .AsNoTracking()
               .FirstOrDefaultAsync();
            if (existedTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            if (existedTask.TaskStatusId == taskStatusId)
            {
                throw new DomainException(Keys.Validation.TaskActionAlreadyDoneBefore);
            }

            var oldStatusId = existedTask.TaskStatusId;
            var taskDriver = existedTask.Driver;
            await _unityOfWork.RunTransaction(async () =>
            {
                if (!string.IsNullOrEmpty(successOrFailTask.Notes))
                {
                    _taskHistoryRepository.Add(new TaskHistory(existedTask.Tenant_Id)
                    {
                        TaskId = existedTask.Id,
                        ActionName = "ADDED THIS NOTE ",
                        FromStatusId = oldStatusId,
                        Description = successOrFailTask.Notes,
                        ToStatusId = taskStatusId,
                        Latitude = successOrFailTask.Latitude,
                        Longitude = successOrFailTask.Longitude,
                    });
                }
                if (successOrFailTask.GallaryFiles != null)
                {
                    foreach (var file in successOrFailTask?.GallaryFiles.Where(gallaryFile => gallaryFile.Length > 0))
                    {
                        var gallaryUploadResult = await _uploadFormFileService.UploadFileAsync(file, TaskGallaryPath);
                        if (gallaryUploadResult.IsSucceeded && !string.IsNullOrEmpty(gallaryUploadResult.ReturnData))
                        {
                            _taskGallaryRepository.Add(new TaskGallary(existedTask.Tenant_Id)
                            {
                                MainTaskId = existedTask.MainTaskId,
                                TaskId = existedTask.Id,
                                FileName = gallaryUploadResult.ReturnData,
                                FileURL = TaskGallaryPath + "/" + gallaryUploadResult.ReturnData,
                                DriverId = existedTask.DriverId,
                            });
                            _taskHistoryRepository.Add(new TaskHistory(existedTask.Tenant_Id)
                            {
                                TaskId = existedTask.Id,
                                ActionName = "ADDED THIS IMAGE ",
                                FromStatusId = oldStatusId,
                                Description = successOrFailTask.GallaryFiles[0].FileName,
                                Reason = gallaryUploadResult.ReturnData,
                                ToStatusId = taskStatusId,
                                Latitude = successOrFailTask.Latitude,
                                Longitude = successOrFailTask.Longitude,
                            });
                        }
                    }
                }
                if (successOrFailTask.SignatureForm != null && successOrFailTask.SignatureForm.Length > 0)
                {
                    var uploadSignatureResult = await _uploadFormFileService.UploadFileAsync(successOrFailTask.SignatureForm, TaskSignaturesPath);
                    if (!string.IsNullOrEmpty(uploadSignatureResult.ReturnData))
                    {
                        existedTask.SignatureURL = uploadSignatureResult.ReturnData;
                        _taskHistoryRepository.Add(new TaskHistory(existedTask.Tenant_Id)
                        {
                            TaskId = existedTask.Id,
                            ActionName = "ADDED A SIGNATURE ",
                            FromStatusId = oldStatusId,
                            Description = uploadSignatureResult.ReturnData,
                            ToStatusId = taskStatusId,
                            Latitude = successOrFailTask.Latitude,
                            Longitude = successOrFailTask.Longitude,
                        });
                    }
                }
                _taskHistoryRepository.Add(new TaskHistory(existedTask.Tenant_Id)
                {
                    TaskId = existedTask.Id,
                    ActionName = ((TaskStatusEnum)taskStatusId).ToString().ToUpper(),
                    Reason = successOrFailTask.Reason,
                    Description = successOrFailTask.Reason,
                    FromStatusId = oldStatusId,
                    ToStatusId = taskStatusId,
                    Longitude = successOrFailTask.Longitude,
                    Latitude = successOrFailTask.Latitude
                });
                _taskRouteRepository.Add(new TaskRoute()
                {
                    TaskId = existedTask.Id,
                    DriverId = existedTask.DriverId ?? 0,
                    Longitude = successOrFailTask.Longitude,
                    Latitude = successOrFailTask.Latitude,
                    TaskStatusId = taskStatusId,
                });

                await SetTaskDistanceHoursAsync(existedTask);
                existedTask.Notes = successOrFailTask.Notes;
                existedTask.TaskStatusId = taskStatusId;
                existedTask.SuccessfulDate = DateTime.UtcNow;
                existedTask.TotalTime = GetTaskTotalTime(existedTask.TaskHistories);
                _subTaskRepository.Update(existedTask);
                await _unityOfWork.SaveChangesAsync();

                await _taskCommonService.ChangeMainTaskStatusAsync(existedTask.MainTaskId);
                await _taskCommonService.ChangeDriverAvailabilityAsync(taskDriver);
                await _unityOfWork.SaveChangesAsync();
            });
            //TODO: can handle notification here

            return existedTask.Tenant_Id;
        }
        public async Task<string> MarkSubTaskAsCancelOrDeclineAsync(int id, TaskStatusEnum cancelOrDeclined, double longitude, double latitude, string reason = null)
        {
            var dbTask = await _subTaskRepository.GetFirstOrDefaultAsync(task => task.Id == id && !task.IsDeleted
             && _currentUser.DriverId.HasValue && _currentUser.DriverId == task.DriverId,
                  task => task.TaskStatus, task => task.TaskHistories);
            if (dbTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            await _unityOfWork.RunTransaction(async () =>
            {
                _taskHistoryRepository.Add(new TaskHistory(dbTask.Tenant_Id)
                {
                    TaskId = dbTask.Id,
                    ActionName = $"MARKED AS {cancelOrDeclined.ToString().ToUpper()}",
                    FromStatusId = dbTask.TaskStatusId,//old status Id
                    ToStatusId = (int)cancelOrDeclined,
                    Longitude = longitude,
                    Latitude = latitude,
                    Reason = reason
                });

                if (longitude != null && latitude != null)
                {
                    _taskRouteRepository.Add(new TaskRoute()
                    {
                        TaskId = dbTask.Id,
                        DriverId = dbTask.DriverId ?? 0,
                        Longitude = longitude,
                        Latitude = latitude,
                        TaskStatusId = (int)cancelOrDeclined,
                    });
                }

                await SetTaskDistanceHoursAsync(dbTask);
                dbTask.TaskStatusId = (int)cancelOrDeclined;
                _subTaskRepository.Update(dbTask);

                //this part need to be change ---
                if (dbTask.DriverId != null)
                    await _driverRepository.ChangeDriverStatusByIdAsync((int)dbTask.DriverId, (int)AgentStatusesEnum.Available);

                await _taskCommonService.UpdateMainTaskStatusAsync(dbTask.MainTaskId, cancelOrDeclined, dbTask.Id);
                ////------------------------
                await _unityOfWork.SaveChangesAsync();
            });

            return dbTask.Tenant_Id;
        }

        private async Task<double> GetAllowedDistanceInMeterAsync()
        {
            double allowedDistanceInMeter = new DefaultSettingValue().ReachedDistanceRestrictionInMeter;
            var allowedDistanceInMeterSetting = await _settingRepository.GetFirstOrDefaultAsync(setting =>
                setting.SettingKey.Trim().ToLower() == SettingKeyConstant.ReachedDistanceRestriction.Trim().ToLower());
            if (allowedDistanceInMeterSetting != null)
            {
                double.TryParse(allowedDistanceInMeterSetting.Value, out allowedDistanceInMeter);
            }

            return allowedDistanceInMeter;
        }
        private SubTask SetIsDelayedTask(SubTask task)
        {
            task.StartDate = DateTime.UtcNow;
            DateTime? taskDate = null;

            if (task.PickupDate.HasValue)
            {
                taskDate = task.PickupDate.Value;
            }
            else if (task.DeliveryDate.HasValue)
            {
                taskDate = task.DeliveryDate.Value;
            }

            if (taskDate != null && task.StartDate > taskDate)
            {
                task.DelayTime = task.StartDate.Value.Subtract(taskDate.Value).TotalMilliseconds;
                task.MainTask.IsDelayed = true;
            }

            return task;
        }
        private double GetTaskTotalTime(ICollection<TaskHistory> alltaskHistory)
        {
            if (alltaskHistory == null || !alltaskHistory.Any())
            {
                return 0;
            }
            var taskHistory = alltaskHistory
                .FirstOrDefault(task => task.ToStatusId == (int)TaskStatusEnum.Started);
            if (taskHistory != null)
            {
                return DateTime.UtcNow.Subtract(taskHistory.CreatedDate).TotalSeconds;
            }
            return 0;
        }
        private async Task<SubTask> SetTaskDistanceHoursAsync(SubTask dbTask)
        {
            var taskDates = dbTask.TaskHistories.Select(task => task.CreatedDate)
                .OrderByDescending(x => x).ToArray();

            var taskPoints = await _taskRouteRepository.GetAllIQueryable()
                .Where(taskRoute => taskRoute.TaskId == dbTask.Id)
                .Where(taskRoute => (taskRoute.Latitude != null || taskRoute.Latitude == 0)
                    && (taskRoute.Longitude != null || taskRoute.Longitude == 0))
                .IgnoreQueryFilters()
                .OrderByDescending(x => x.CreatedDate)
                .ProjectTo<MapPoint>(_mapper.ConfigurationProvider)
                .ToArrayAsync();

            if (taskPoints != null)
            {
                var distance = GetTotalTaskKM(taskPoints);
                dbTask.TotalDistance = double.IsNaN(distance) ? 0 : distance;
            }
            dbTask.TotalTaskTime = taskDates != null ? GetTotalTaskDate(taskDates) : 0;
            return dbTask;
        }
        private double GetTotalTaskKM(MapPoint[] points)
        {
            double total = 0.0;
            for (var i = 0; i < points.Length; i++)
            {
                if (i == 0) continue;
                var distanceresponse = MapsHelper.GetDistanceBetweenPoints(points[i - 1], points[i]);
                total += (distanceresponse / 1000);
            }
            return total;
        }
        private double GetTotalTaskDate(DateTime[] dates)
        {
            double total = 0.0;
            for (var i = 0; i < dates.Length; i++)
            {
                if (i == 0) continue;
                total += dates[i - 1].Subtract(dates[i]).TotalMilliseconds;
            }
            return total;
        }
    }
}
