﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.BLL.Tasks.WebServices.DTOs;

namespace Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs
{
    public class MainTaskAutoAssignInputDTO : BaseEntity
    {
        public int? BranchId { get; set; }  // for first pickup
        public int? GeofenceId { get; set; } // for first delivery
        public TaskSettingInputDTO Settings { get; set; }
        public int OrderWeight => 1;
    }
}
