﻿namespace Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs
{
    public class MainTaskAutoAssignResultDTO
    {
        public bool IsSuccessed { get; set; }
    }
}
