﻿#if false
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.ViewModels;
using MapsUtilities.MapsManager;
using MapsUtilities.Models;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public class DriverTaskDistanceComparer : IComparer<DriverViewModel>
    {
        TasksBaseInfoViewModel _task;
        IMapsManager _mapsManager;
        MapRequestSettings _mapsSettings;
        public DriverTaskDistanceComparer(TasksBaseInfoViewModel task, IMapsManager mapsManager, MapRequestSettings mapsSettings)
        {
            _task = task;
            _mapsManager = mapsManager;
            _mapsSettings = mapsSettings;
        }
        public int Compare(DriverViewModel driver1, DriverViewModel driver2)
        {
            if (driver1 == null)
            {
                if (driver2 == null)
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                if (driver2 == null)
                {
                    return 1;
                }
                else
                {
#warning cleanup- the wrong item to compare, get those durations first and then pass to comparer, no async here
                    var distance1 = GetDurationOrDistanceAsync(_task, driver1).Result;
                    var distance2 = GetDurationOrDistanceAsync(_task, driver2).Result;

                    if (distance1.HasValue)
                    {
                        return distance1.Value.CompareTo(distance2);
                    }
                    else if (distance2.HasValue)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }
        }



        /// <summary>
        /// Gets distance in meters between between task location and driver location
        /// </summary>
        /// <param name="task"></param>
        /// <param name="driver"></param>
        /// <returns>Distnace in meters</returns>
        public async Task<double?> GetDurationOrDistanceAsync(TasksBaseInfoViewModel task, DriverViewModel driver)
        {
            try
            {
                if (!task.Latitude.HasValue || !task.Longitude.HasValue || !driver.Latitude.HasValue || !driver.Longitude.HasValue)
                    throw new ArgumentNullException("Latitude and longitude can't be null");

                var result = await _mapsManager.GetMinDurationOrDistance(
                     new MapPoint { Latitude = task.Latitude.Value, Longitude = task.Longitude.Value },
                     new MapPoint { Latitude = driver.Latitude.Value, Longitude = driver.Longitude.Value },
                     _mapsSettings);

                if (result.Success)
                {
                    return result.MinDurationOrDistance;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception e)
            {
                return null;
            }
        }


    }
}

#endif