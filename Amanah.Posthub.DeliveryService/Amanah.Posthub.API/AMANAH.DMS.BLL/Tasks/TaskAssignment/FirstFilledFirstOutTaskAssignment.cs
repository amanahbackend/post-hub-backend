﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.BLL.ViewModels.Driver;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public class FirstFilledFirstOutTaskAssignment : TaskAutoAssignment
    {

        private readonly IBackgroundJobManager _jobManager;

        public FirstFilledFirstOutTaskAssignment(IBackgroundJobManager jobManager)
        {
            _jobManager = jobManager;
        }

        /// <summary>
        /// Gets the proper tasks for this driver. This is typically caled when the driver checks in
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public override Task<CreateTaskResponse> AssignTasksToDriverAsync(int driverId)
        {
            var fFFODetermineTasksForDriverJobModel = new FFFODetermineTasksForDriverJobModel
            {
                DriverId = driverId,
                IsClubbingTimeOver = false
            };
            _jobManager.Enqueue(fFFODetermineTasksForDriverJobModel);
            var response = new CreateTaskResponse { StatusCode = HttpStatusCode.Created };
            return Task.FromResult(response);
        }

        public override Task<MainTaskAutoAssignResultDTO> AssignDriverToTaskAsync(MainTaskAutoAssignInputDTO task)
        {
            var fFODetermineDriverForTaskJobModel = new FFFODetermineDriverForTaskJobModel
            {
                MainTask = task
            };
            _jobManager.Enqueue(fFODetermineDriverForTaskJobModel);

            return Task.FromResult(new MainTaskAutoAssignResultDTO
            {
                IsSuccessed = true
            });
        }

    }

    internal class FFFODetermineTasksForDriverJobModel
    {
        public int DriverId { get; set; }
        public bool IsClubbingTimeOver { get; set; }
    }

    internal class FFFODetermineDriverForTaskJobModel
    {
        public MainTaskAutoAssignInputDTO MainTask { get; set; }
    }

    internal class FFFOAutoAssignJob :
        IBackgroundJob<FFFODetermineTasksForDriverJobModel>,
        IBackgroundJob<FFFODetermineDriverForTaskJobModel>
    {
        private static int CLUBBING_TIME_DEFAULT_VALUE = 120;
        private readonly IDriverManager _driverManager;
        private readonly ITasksManager _tasksManager;
        private readonly ISettingsManager _settingsManager;
        private readonly ILocalizer _localizer;
        private readonly IBackgroundJobManager _backgroundJobManager;

        public FFFOAutoAssignJob(
            IDriverManager driverManager,
            ITasksManager tasksManager,
            ISettingsManager settingsManager,
            ILocalizer localizer,
            IBackgroundJobManager backgroundJobManager)
        {
            _driverManager = driverManager;
            _tasksManager = tasksManager;
            _settingsManager = settingsManager;
            _localizer = localizer;
            _backgroundJobManager = backgroundJobManager;
        }

        #region Determine Tasks For Driver
        public Task ExecuteAsync(FFFODetermineTasksForDriverJobModel jobModel)
        {
            return DetermineTasksToDriverAsync(jobModel.DriverId, jobModel.IsClubbingTimeOver);
        }
        private async Task DetermineTasksToDriverAsync(
            int driverId,
            bool clubbingTimeOver)
        {
            /* Get driver with reached status along with tasks already assigned (tasks having this driverId), so we can
             * figure more suitable tasks for this driver
             * This can return null in case the driver is not reached now, this is typically when there are tasks created
             * and assigned to this driver and the driver is ready to go before the clubbing time is over,
             * so when this method is called from the background job after the clubbing time is over
             * it will find the driver status not reached and will not return the driver
             */
            var driver = await _driverManager.GetReachedDriverWithTasksAsync(driverId);
            if (driver == null)
            {
                return;
            }
            var remainingOrderWeights = driver.MaxOrdersWeightsCapacity - driver.OrdersWeights;
            var mainTasks = new List<MainTaskAutoAssignInputDTO>();

            /* The driver already has existing tasks, this is typically when this method is called from
             * the background job after clubbing time
             */
            if (driver.AssignedMainTasks.Any())
            {
                var mainTasksInput = new GetMainTasksInput
                {
                    /* Get tasks having the same delivery geofence as the tasks (first task is enough) 
                     * already assigned to the driver, as this geofence now has the highest priority to complete tasks
                     * from the same geofence with the same driver
                     */
                    DeliveryGeoFenceId = driver.MainTaskDeliveryGeoFenceId,
                    Status = TaskStatusEnum.Unassigned,
                    Take = remainingOrderWeights,
                };
                mainTasks = await _tasksManager.GetMainTasksAsync(mainTasksInput);
                //TODO later
                // remainingOrderWeights += mainTasks.Sum(t => t.OrderWeight);
            }

            //This will happen in either 3 cases:
            // 1. No tasks retruned with the same existing geofence
            // 2. Tasks with the same existing geofence didn't complete the driver's capacity
            // 3. Driver doent't have already existing tasks in the first place
            if (remainingOrderWeights < driver.MaxOrdersWeightsCapacity)
            {
                //var geoFencesIds = (await _driverManager.GetDriverDeliveryGeoFencesAsync(driver.Id))
                //    .Select(geofence => geofence.Id)
                //    .ToList();
                var geoFencesIds = driver.DriverDeliveryGeoFences.Select(e => e.Id).ToList();
                var mainTasksInput = new GetMainTasksInput
                {
                    Status = TaskStatusEnum.Unassigned,
                    DeliveryGeoFenceIds = geoFencesIds
                };
                mainTasks = await _tasksManager.GetMainTasksAsync(mainTasksInput);
                //TODO Later
                //mainTasks = mainTasks.GroupBy(t => t.Tasks.First(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId.Value)
                //    .OrderBy(g => g.First().Id)
                //    .SelectMany(g => g)
                //    .Take(remainingOrderWeights)
                //    .ToList();
            }

            await ProcessAssignTasksToDriverAsync(driver, mainTasks, clubbingTimeOver);
        }
        #endregion

        #region Determine Driver For Task
        public Task ExecuteAsync(FFFODetermineDriverForTaskJobModel jobModel)
        {
            return DetermineDriverForTaskAsync(jobModel.MainTask);
        }
        private async Task DetermineDriverForTaskAsync(MainTaskAutoAssignInputDTO mainTask)
        {
            var input = new GetDriversInput
            {
                PickupBranchId = mainTask.BranchId,
                Status = AgentStatusesEnum.Available,
                IsReached = true
            };
            var drivers = await _driverManager.GetDriversAsync(input);

            await DetermineDriverForTaskAsync(mainTask, drivers);
        }
        private async Task DetermineDriverForTaskAsync(MainTaskAutoAssignInputDTO mainTask,
            List<DriverWithTasksViewModel> drivers)
        {
            var taskDeliveryGeoFenceId = mainTask.GeofenceId.Value;

            var driver = drivers
                .Where(d =>
                    d.MainTaskDeliveryGeoFenceId == taskDeliveryGeoFenceId
                    && d.RemainingOrdersWeights <= mainTask.OrderWeight)
                .OrderBy(d => d.RemainingOrdersWeights)
                .OrderBy(d => d.DriverDeliveryGeoFences.Count)
                .FirstOrDefault();

            if (driver != null)
                await ProcessAssignTasksToDriverAsync(driver, new List<MainTaskAutoAssignInputDTO> { mainTask });
        }
        #endregion

        private async Task ProcessAssignTasksToDriverAsync(
            DriverWithTasksViewModel driver,
            List<MainTaskAutoAssignInputDTO> mainTasks,
            bool clubbingTimeOver = false)
        {
            //TODO Later
            //foreach (var mainTask in mainTasks)
            //{
            //    //mainTask.IsNewlyAssigned = true;
            //    mainTask.Tasks.ToList().ForEach(t => t.DriverId = driver.Id);
            //}

            //If the driver reached the min orders to go without waiting
            //Or if clubbing time is over
            if (driver.OrdersWeights >= driver.MinOrdersNoWait || clubbingTimeOver)
            {
                //TODO later
                //foreach (var mainTask in mainTasks)
                //{
                //    mainTask.Tasks.ToList().ForEach(t => t.TaskStatusId = (int)Enums.TaskStatusEnum.Accepted);
                //}
                //ToDo need to be handled After update  ----------
                //  await _tasksManager.UpdateTasks(mainTasks);
                await SendTaskNotificationToDriverAsync(driver.Id, mainTasks);
            }
            else if (driver.AssignedMainTasks.Any(t => t.IsNewlyAssigned) &&
                !driver.AssignedMainTasks.Any(t => !t.IsNewlyAssigned))
            {
                /* Start clubbing time only if all tasks assigned to the driver are new, if there are old tasks this means
                 * clubbing time already started, if no tasks at all this driver will keep waiting without clubbing time
                 * counting until the first task is assigned
                 * */
                int interval = CLUBBING_TIME_DEFAULT_VALUE;
                var expiresSetting = await _settingsManager.GetSettingByKeyAsync(
                    SettingsKeys.FirstInFirstOutClubbingTimeInSec);
                if (expiresSetting != null)
                {
                    int.TryParse(expiresSetting.Value, out interval);
                }

                var driverVM = new BackgroundJobViewModel<int> { ViewModel = driver.Id };
                _backgroundJobManager.Enqueue(
                    new FFFODetermineTasksForDriverJobModel
                    {
                        DriverId = driver.Id,
                        IsClubbingTimeOver = true
                    },
                    delay: TimeSpan.FromSeconds(interval));
                //BackgroundJob.Schedule(
                //    () => DetermineTasksToDriverAsync(driverVM, true),
                //    TimeSpan.FromSeconds(interval));
            }
        }

        private async Task SendTaskNotificationToDriverAsync(
            int driverId,
            List<MainTaskAutoAssignInputDTO> mainTasks)
        {
            await _tasksManager.SendPushNotificationToDriver(
                driverId,
                _localizer[Keys.Notifications.NewTasksAssignedToYou],
                new
                {
                    mainTasks,
                    notificationType = NotificationTypeEnum.NewTask
                });
        }
    }
}
