﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.Tasks.AutoAssignServices;
using Amanah.Posthub.BLL.Tasks.AutoAssignServices.DTOs;
using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public class FirstInFirstOutTaskAssignment : TaskAutoAssignment
    {
        private readonly ISettingsManager _settingsManager;
        private readonly IBackgroundJobManager _jobManager;

        public FirstInFirstOutTaskAssignment(
            ISettingsManager settingsManager,
            IBackgroundJobManager jobManager)
        {
            _settingsManager = settingsManager;
            _jobManager = jobManager;
        }

        /// <summary>
        /// Gets the proper tasks for this driver. This is typically caled when the driver checks in
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        public override async Task<CreateTaskResponse> AssignTasksToDriverAsync(int driverId)
        {
            _jobManager.Enqueue(new FIFODetermineTasksForDriverJobModel
            {
                DriverId = driverId,
                ClubbingTime = await GetClubbingTimeAsync()
            });
            var result = new CreateTaskResponse { StatusCode = HttpStatusCode.Created };
            return result;
        }
        public override async Task<MainTaskAutoAssignResultDTO> AssignDriverToTaskAsync(MainTaskAutoAssignInputDTO task)
        {
            _jobManager.Enqueue(new FIFODetermineDriverForTaskJobModel
            {
                MainTask = task,
                ClubbingTime = await GetClubbingTimeAsync()
            });
            return new MainTaskAutoAssignResultDTO
            {
                IsSuccessed = true
            };
        }

        private async Task<TimeSpan> GetClubbingTimeAsync()
        {
            var clubbingTime = new DefaultSettingValue().FirstInFirstOutClubbingTimeInSec;
            var expiresSetting = await _settingsManager.GetSettingByKeyAsync(
                SettingsKeys.FirstInFirstOutClubbingTimeInSec);
            if (expiresSetting != null)
            {
                int.TryParse(expiresSetting.Value, out clubbingTime);
            }

            return TimeSpan.FromSeconds(clubbingTime);
        }
    }

    public class FIFODetermineTasksForDriverJobModel
    {
        public int DriverId { get; set; }
        public TimeSpan ClubbingTime { get; set; }
    }
    public class FIFODetermineDriverForTaskJobModel
    {
        public MainTaskAutoAssignInputDTO MainTask { get; set; }
        public TimeSpan ClubbingTime { get; set; }
    }
    public class FIFOFinishDriverClubbingTimeJobModel
    {
    }

    public class FIFOAutoAssignJob :
        IBackgroundJob<FIFODetermineTasksForDriverJobModel>,
        IBackgroundJob<FIFODetermineDriverForTaskJobModel>,
        IBackgroundJob<FIFOFinishDriverClubbingTimeJobModel>
    {
        private readonly IAutoAssignTaskService autoAssignTaskService;
        private readonly IBackgroundJobManager _jobManager;

        public FIFOAutoAssignJob(
            IAutoAssignTaskService autoAssignTaskService,
            IBackgroundJobManager jobManager)
        {
            this.autoAssignTaskService = autoAssignTaskService;
            _jobManager = jobManager;
        }


        public async Task ExecuteAsync(FIFOFinishDriverClubbingTimeJobModel jobModel)
        {
            await autoAssignTaskService.UpdateDriversWithExpireCluppingTimeAsync();
        }
        public async Task ExecuteAsync(FIFODetermineDriverForTaskJobModel jobModel)
        {
            await DetermineDriversForTasksAsync(jobModel.MainTask, jobModel.ClubbingTime);
        }
        public async Task ExecuteAsync(FIFODetermineTasksForDriverJobModel jobModel)
        {
            await DetermineTasksForDriverAsync(jobModel.DriverId, jobModel.ClubbingTime);
        }

        private async Task DetermineDriversForTasksAsync(MainTaskAutoAssignInputDTO mainTaskVM, TimeSpan clubbingTime)
        {
            var branchId = mainTaskVM.BranchId;
            if (!branchId.HasValue)
            {
                return;
            }

            var drivers = await autoAssignTaskService.GetDriversAllowedToAssignAsync(new AutoAssignDriverFilterDTO
            {
                AgentStatus = AgentStatusesEnum.Available,
                IsReached = true,
                PickupBranchId = branchId,
                IncludeOrdersWeights = true,
                TaskStatuses = new List<TaskStatusEnum>
                {
                    TaskStatusEnum.Accepted,
                    TaskStatusEnum.Assigned
                },
                IncludeDeliveryGeoFences = true,
                IsApplyingAutoAllocation = true,
                AutoAllocationType = AutoAllocationTypeEnum.Fifo
            });
            if (drivers == null || !drivers.Any())
            {
                return;
            }

            await AssignDriverToMainTasksAsync(clubbingTime, branchId.Value, drivers);
        }
        private async Task DetermineTasksForDriverAsync(int driverId, TimeSpan clubbingTime)
        {
            var drivers = await autoAssignTaskService.GetDriversAllowedToAssignAsync(new AutoAssignDriverFilterDTO
            {
                AgentStatus = AgentStatusesEnum.Available,
                DriverId = driverId,
                IsReached = true,
                IncludeOrdersWeights = true,
                TaskStatuses = new List<TaskStatusEnum>
                {
                    TaskStatusEnum.Accepted,
                    TaskStatusEnum.Assigned
                },
                IncludeDeliveryGeoFences = true,
                IsApplyingAutoAllocation = true,
                AutoAllocationType = AutoAllocationTypeEnum.Fifo
            });
            if (drivers == null || !drivers.Any())
            {
                return;
            }
            var branchId = drivers.FirstOrDefault()?.BranchId;
            if (!branchId.HasValue)
            {
                return;
            }

            await AssignDriverToMainTasksAsync(clubbingTime, branchId.Value, drivers);
        }
        private async Task<List<AutoAssignDriverDTO>> AssignDriverToMainTasksAsync(TimeSpan clubbingTime, int branchId, List<AutoAssignDriverDTO> drivers)
        {
            drivers = await AssignMainTaskToDriverAsync(branchId, drivers);
            var driversWithNewAssignedTasks = drivers?
                .Where(assignedDriver => assignedDriver.NewAssignedMainTasks != null && assignedDriver.NewAssignedMainTasks.Any());
            if (driversWithNewAssignedTasks != null)
            {
                bool isTheirDriverInClubbingTime = await autoAssignTaskService
                    .UpdateAssignedDriversToMainTasksAsync(clubbingTime, driversWithNewAssignedTasks);
                if (isTheirDriverInClubbingTime)
                {
                    _jobManager.Enqueue(new FIFOFinishDriverClubbingTimeJobModel(), clubbingTime);
                }
            }

            return drivers;
        }
        private async Task<List<AutoAssignDriverDTO>> AssignMainTaskToDriverAsync(int branchId, List<AutoAssignDriverDTO> drivers)
        {
            //TODO: Include Delivery in get 
            var mainTasks = await autoAssignTaskService.GetUnAssignedMainTasksAsync(new AutoAssignTaskFilterDTO
            {
                Status = TaskStatusEnum.Unassigned,
                PickupBranchId = branchId,
            });
            if (mainTasks == null || !mainTasks.Any())
            {
                return null;
            }

            var mainTaskgroups = mainTasks
                .GroupBy(mainTask => mainTask.Tasks
                        ?.FirstOrDefault(task => task.TaskTypeId == (int)TaskTypesEnum.Delivery && task.GeoFenceId.HasValue)
                        ?.GeoFenceId.Value)
                .OrderBy(deliveryGeofence => deliveryGeofence.FirstOrDefault()?.Id).ToList();
            foreach (var mainTasksGroup in mainTaskgroups)
            {
                var tasksGroupDeliveryGeoFenceId = mainTasksGroup.Key;
                if (tasksGroupDeliveryGeoFenceId == null)
                {
                    continue;
                }

                var driver = drivers.Where(driverGeofence => (driverGeofence.AllDeliveryGeoFences
                    || driverGeofence.DriverDeliveryGeoFences != null
                    && driverGeofence.DriverDeliveryGeoFences.Any(g => g.GeoFenceId == tasksGroupDeliveryGeoFenceId))
                    && driverGeofence.RemainingOrdersWeights > 0)
                    .OrderBy(e => e.ReachedTime)
                    .FirstOrDefault();
                if (driver == null)
                {
                    continue;
                }

                var orderWeights = mainTasksGroup.Sum(mainTask => mainTask.OrderWeight);
                if (orderWeights > driver.RemainingOrdersWeightsNoWait)
                {
                    continue;
                }

                if (driver.NewAssignedMainTasks == null)
                {
                    driver.NewAssignedMainTasks = new List<MainTask>();
                }
                var unAssignedMainTasks = mainTasksGroup.Where(mainTask =>
                    mainTask.Tasks.All(task =>
                        !task.DriverId.HasValue ||
                        task.TaskStatusId == (int)TaskStatusEnum.Unassigned));
                foreach (var mainTask in unAssignedMainTasks)
                {
                    var ordersWeight = driver.NewAssignedMainTasks.Sum(mainTask => mainTask.OrderWeight)
                        + driver.OrdersWeights
                        + mainTask.OrderWeight;
                    if (ordersWeight > driver.MaxOrdersWeightsCapacity)
                    {
                        continue;
                    }

                    mainTask.AssignmentType = (int)TaskAssignmentType.FIFO;
                    mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Assigned;
                    mainTask.IsCompleted = false;
                    mainTask?.Tasks?.ToList().ForEach(task =>
                    {
                        task.DriverId = driver.Id;
                        task.TaskStatusId = (int)TaskStatusEnum.Accepted;
                        task.ReachedTime = driver.ReachedTime;
                        task.IsTaskReached = true;
                    });

                    driver.NewAssignedMainTasks.Add(mainTask);
                    driver.AssignedTasksCount++;
                }
            }

            return drivers;
        }
    }
}
