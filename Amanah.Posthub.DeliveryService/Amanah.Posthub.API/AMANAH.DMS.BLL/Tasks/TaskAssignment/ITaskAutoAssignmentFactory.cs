﻿using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public interface ITaskAutoAssignmentFactory
    {
        Task<TaskAutoAssignment> CreateTaskAutoAssignmentAsync();
        //Task AssignNextDriver(int mainTaskId);
    }
}
