﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.Tasks.WebServices;
using Amanah.Posthub.BLL.TasksFolder.Queries;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public class NearestAvailableBackGroundJob : IBackgroundJob<NearestAvailableDetermineDriverForTaskJobModel>
    {
        private readonly ILocalizer _localizer;
        private readonly INearestAvailableMethods _methods;
        private readonly IUnitOfWork _unityOfWork;
        private readonly ITaskCommonService _taskCommonService;
        private readonly IMapper _mapper;
        private readonly ISettingRepository _settingRepository;
        private readonly IBackgroundJobManager _jobManager;

        public NearestAvailableBackGroundJob(
            ISettingRepository settingRepository,
            ILocalizer localizer,
            IUnitOfWork unityOfWork,
             IMapper mapper,
             INearestAvailableMethods methods,
             ITaskCommonService taskCommonService,
             IBackgroundJobManager jobManager
            )
        {
            _settingRepository = settingRepository;
            _localizer = localizer;
            _unityOfWork = unityOfWork;
            _mapper = mapper;
            _methods = methods;
            _taskCommonService = taskCommonService;
            _jobManager = jobManager;
        }

        public async Task ExecuteAsync(NearestAvailableDetermineDriverForTaskJobModel jobModel)
        {
            DefaultSettingValue defaultSettingValue = new DefaultSettingValue();
            int autoAllocationNeasrestRetriesCount = defaultSettingValue.NearestAvailableMethodNumberOfRetries;
            var autoAllocationNeasrestRetriesSetting = await _settingRepository.GetSettingByKeyAsync(SettingKeyConstant.NearestAvailableMethodNumberOfRetries);
            if (autoAllocationNeasrestRetriesSetting != null)
            {
                int.TryParse(autoAllocationNeasrestRetriesSetting.Value, out autoAllocationNeasrestRetriesCount);
            }

            jobModel.MainTask.Settings.RetriesCount++;
            var maintask = jobModel.MainTask;
            var IsDriverAssignedToTask = await DetermineTaskDriverAsync(maintask);
            if (!IsDriverAssignedToTask)
            {
                if (maintask.Settings.RetriesCount < autoAllocationNeasrestRetriesCount)
                {
                    _jobManager.Enqueue(new NearestAvailableDetermineDriverForTaskJobModel
                    {
                        MainTask = maintask,
                    },
                    delay: TimeSpan.FromSeconds(defaultSettingValue.NearestAvailableMethodReScheduleRequestTimeInSec)
                    );
                }
                else
                {
                    await _methods.AutoAllocationFailAsync(maintask.Id);
                }
            }
        }


        private async Task<bool> DetermineTaskDriverAsync(MainTaskAutoAssignInputDTO task)
        {

            //get setting for one transaction from fb
            var neededSettings = await _settingRepository.GetSettingByKeyAsync(
                 new List<string> { SettingKeyConstant.NearestAvailableMethodRadiusInKM, SettingsKeys.NearestAvailableMethodOrderCapacity }).ToListAsync();
            var radiusSetting = neededSettings.FirstOrDefault(x => x.SettingKey.Trim().ToLower() == SettingKeyConstant.NearestAvailableMethodRadiusInKM.Trim().ToLower());
            var MaximumDriverCapacitySetting = neededSettings.FirstOrDefault(x => x.SettingKey.Trim().ToLower() == SettingsKeys.NearestAvailableMethodOrderCapacity.Trim().ToLower());

            var subTaskStatuses = new List<int>
                {
                    (int)TaskStatusEnum.Assigned,
                    (int) TaskStatusEnum.Accepted
                };


            if (task?.Settings?.DriversCount == 0)
            {
                return true;
            }
            //maintask with tasks included
            var mainTask = await _methods.GetMainTaskForStatusAndIncludeTasks(task.Id);

            if (mainTask == null || mainTask.IsDeleted) return true;
            if (mainTask.MainTaskStatusId.HasValue && mainTask.MainTaskStatusId.Value != 0)
            {
                return true;
            }

            IQueryable<Driver> availableDriversQuery = null;
            if (!task.Settings.IsTenantAllowedToUsePlatformDrivers)
            {
                var geoFences = await _methods.GetGeofencesForTask(mainTask, task.Settings);
                if (geoFences == null || geoFences.PickupGeoFences == null || geoFences.DeliveryGeoFences == null)
                {
                    return false;
                }
                task.Settings.PickupGeoFenceIds = geoFences.PickupGeoFences.Select(g => g.Id).ToList();
                task.Settings.DeliveryGeoFenceIds = geoFences.DeliveryGeoFences.Select(g => g.Id).ToList();

                availableDriversQuery = _methods.GetAvailableDrivers(task);
            }
            else
            {
                availableDriversQuery = _methods.GetAvailablePlatformDrivers();
            }

            if (availableDriversQuery == null || availableDriversQuery.Count() <= 0)
            {
                return false;
            }


            //locally =>  Without Contacting db => dont get drivers that "have maximum capacity" from db
            int NearstOrderCapacity = 5;
            if (MaximumDriverCapacitySetting != null)
            {
                int.TryParse(MaximumDriverCapacitySetting.Value, out NearstOrderCapacity);
            }

            //Drivers Current Capacity
            availableDriversQuery = availableDriversQuery.Where(d =>
               (d.AssignedTasks.Where(x => subTaskStatuses.Contains(x.TaskStatusId))//get currently working subtasks
              .Select(x => x.MainTaskId).Distinct().Count()//get currently working mainTasks of working subtasks and distinct them and then count
               * MainTask.OrderWeightValue //get weight for all Maintasks for a deiver
               ) < NearstOrderCapacity);

            //get drivers from database
            var availableDrivers = await availableDriversQuery.ProjectTo<DriverNearestAvailableDTO>(_mapper.ConfigurationProvider).ToListAsync();
            if (availableDrivers == null || availableDrivers.Count <= 0)
            {
                return false;
            }


            //locally => this one only get drivers within specific redis according to the system setting and its 
            availableDrivers = _methods.SortAndFilterAvailableDriversByRedisDistance(mainTask, availableDrivers, radiusSetting);
            if (availableDrivers == null || availableDrivers.Count <= 0)
            {
                return false;
            }


            //reduse list to half
            int numberOfDriversToGotoGoogle = (int)Math.Ceiling((double)availableDrivers.Count / 2);
            //if half is liss than 10 then take 10
            numberOfDriversToGotoGoogle = numberOfDriversToGotoGoogle < 10 ? 10 : numberOfDriversToGotoGoogle;
            availableDrivers = availableDrivers?.Take(numberOfDriversToGotoGoogle).ToList();


            availableDrivers = await _methods.SortDriversAccordingToGoogleByNearestAsync(mainTask, availableDrivers);
            availableDrivers = availableDrivers?.Take(task.Settings.DriversCount).ToList();

            if (availableDrivers == null || availableDrivers.Count <= 0)
            {
                return false;
            }

            foreach (var driver in availableDrivers)
            {
                await _unityOfWork.RunTransaction(async () =>
                {
                    _methods.UpdateTaskDriver(mainTask, driver.Id, null, status: TaskStatusEnum.Accepted, assignmentType: TaskAssignmentType.NearestAvailable, null, reachedTime: driver.ReachedTime);
                    mainTask = _taskCommonService.UpdateMainTaskStatus(mainTask);
                    _methods.AddDriverTaskNotification(mainTask.Tasks.FirstOrDefault(),
                    _localizer[Keys.Notifications.NewTasksAssignedToYou],
                    (int)NotificationTypeEnum.NewTask);
                    await _unityOfWork.SaveChangesAsync();
                });

                await _methods.AutoAllocationSucssesfullAsync(mainTask);

                await _methods.SendPushNotificationToDriverAsync(
                    _localizer[Keys.Notifications.NewTasksAssignedToYou],
                    driver.UserId);

            }

            return true;
        }

    }


    public class NearestAvailableDetermineDriverForTaskJobModel
    {
        public MainTaskAutoAssignInputDTO MainTask { get; set; }
    }

}
