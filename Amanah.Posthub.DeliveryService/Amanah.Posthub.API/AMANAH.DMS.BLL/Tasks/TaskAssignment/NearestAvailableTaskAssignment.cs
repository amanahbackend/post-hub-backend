﻿using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public class NearestAvailableTaskAssignment : TaskAutoAssignment
    {
        private readonly IBackgroundJobManager _jobManager;

        public NearestAvailableTaskAssignment(
             IBackgroundJobManager jobManager
            )
        {
            _jobManager = jobManager;
        }

        public override async Task<MainTaskAutoAssignResultDTO> AssignDriverToTaskAsync(MainTaskAutoAssignInputDTO task)
        {
            _jobManager.Enqueue(new NearestAvailableDetermineDriverForTaskJobModel
            {
                MainTask = task
            });
            return new MainTaskAutoAssignResultDTO
            {
                IsSuccessed = true
            };
        }
    }
}
