﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.Tasks.WebServices;
using Amanah.Posthub.BLL.TasksFolder.Queries;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{

    public interface IOneByOneBackGroundJob : IBackgroundJob<OneByOneDetermineDriverForTaskJobModel>
    {
        Task<bool> SchedualForAllAvailableDriversAsync(OneByOneDetermineDriverForTaskJobModel mainTaskVM, MainTask mainTask);
    }



    public class NOneByOneBackGroundJob : IOneByOneBackGroundJob
    {
        private readonly ILocalizer _localizer;
        private readonly INearestAvailableMethods _nearestAvailableUtilities;
        private readonly IUnitOfWork _unityOfWork;
        private readonly ITaskCommonService _taskCommonService;
        private readonly IMapper _mapper;
        private readonly IBackgroundJobManager _jobManager;
        private readonly ITaskDriverRequestRepository _taskDriverRequestRepository;
        private readonly ICurrentUser _currentUser;
        private readonly IBackGroundNotificationSercvice _notificationService;
        private readonly List<int> progressSubStatusTasks = new List<int>()
            {
                (int)TaskStatusEnum.Accepted,
                (int)TaskStatusEnum.Started,
                (int)TaskStatusEnum.Inprogress,
                (int)TaskStatusEnum.Successful,
                (int)TaskStatusEnum.Failed
            };
        public NOneByOneBackGroundJob(

            ILocalizer localizer,
            IUnitOfWork unityOfWork,
             IMapper mapper,
             INearestAvailableMethods nearestAvailableUtilotes,
             ITaskCommonService taskCommonService,
             IBackgroundJobManager jobManager,
             ITaskDriverRequestRepository taskDriverRequestRepository,
             ICurrentUser currentUser,
            IBackGroundNotificationSercvice notificationService
            )
        {
            _localizer = localizer;
            _unityOfWork = unityOfWork;
            _mapper = mapper;
            _nearestAvailableUtilities = nearestAvailableUtilotes;
            _taskCommonService = taskCommonService;
            _jobManager = jobManager;
            _taskDriverRequestRepository = taskDriverRequestRepository;
            _currentUser = currentUser;
            _notificationService = notificationService;
        }

        public async Task ExecuteAsync(OneByOneDetermineDriverForTaskJobModel jobModel)
        {
            var mainTask = await _nearestAvailableUtilities.GetMainTaskForStatusAndIncludeTasks(jobModel.MainTask.Id);


            //if driver a driver accepted the task
            if (progressSubStatusTasks.Contains(mainTask.Tasks.First()?.TaskStatusId ?? 0))
                return;

            var request = jobModel.DriverToAssign == null ? null : await _taskDriverRequestRepository.GetTaskDriverRequestsLastAsync(mainTask.Id, jobModel.DriverToAssign.Id);

            if (jobModel.IsFailToAutoAssignDriver)
            {
                //set autoallocation as expired 
                await EndAutoAllocationAsync(mainTask, request);
                return;
            }

            //Assign task and save to database
            if (jobModel.MainTask.Settings.RetriesCount <= jobModel.MainTask.Settings.MaxRetriesCount)
            {
                await AssignToDriverAsync(jobModel, mainTask, request);
            }


            //If this is the last enqued driver then re scheduall all drivers
            if (jobModel.IsLastDriverInQuee)
                await SchedualForAllAvailableDriversAsync(new OneByOneDetermineDriverForTaskJobModel()
                {
                    MainTask = jobModel.MainTask
                }, mainTask);
        }

        public async Task<bool> SchedualForAllAvailableDriversAsync(OneByOneDetermineDriverForTaskJobModel mainTaskVM, MainTask mainTask)
        {
            var lastAssignedDriverId = mainTask.Tasks.First().DriverId;

            //if driver a driver accepted the task
            if (progressSubStatusTasks.Contains(mainTask.Tasks.First()?.TaskStatusId ?? 0))
            {
                return true;
            }

            //if all queed tasks are done then re quee them if retriis coun is less than max
            if (mainTaskVM.MainTask.Settings.RetriesCount < mainTaskVM.MainTask.Settings.MaxRetriesCount)
            {
                //increase retries for the same driver
                mainTaskVM.MainTask.Settings.RetriesCount++;

                //Reset Availabale drivers 'If Drivers created or location changed'
                var availableDrivers = await SetAvailableDriversAsync(mainTask, mainTaskVM.MainTask);

                //enquee all drivers for their tasks
                List<int> addedTasksDrivers = new List<int>();

                foreach (var driverToAddTaskTo in availableDrivers)
                {
                    addedTasksDrivers.Add(driverToAddTaskTo.Id);

                    //if this is the first trial then dont wait 
                    int Interval = mainTaskVM.MainTask.Settings.RetriesCount == 1 ?
                          //dont wait to excute
                          mainTaskVM.MainTask.Settings.IntervalInSeconds * (addedTasksDrivers.Count - 1)
                          :
                          //wait to excute
                          mainTaskVM.MainTask.Settings.IntervalInSeconds * addedTasksDrivers.Count
                          ;

                    _jobManager.Enqueue(new OneByOneDetermineDriverForTaskJobModel()
                    {
                        MainTask = mainTaskVM.MainTask,
                        DriverToAssign = driverToAddTaskTo,
                        IsLastDriverInQuee = availableDrivers.Count == addedTasksDrivers.Count
                    }, TimeSpan.FromSeconds(Interval));
                }
                return true;
            }
            else
            {
                mainTaskVM.IsFailToAutoAssignDriver = true;
                _jobManager.Enqueue(mainTaskVM, TimeSpan.FromSeconds(mainTaskVM.MainTask.Settings.IntervalInSeconds));
            }

            //Notification
            //bool isSuccedded = await _nearestAvailableUtilities.AutoAllocationSucssesfullAsync(mainTask);
            return true;
        }

        private async Task<List<DriverNearestAvailableDTO>> SetAvailableDriversAsync(MainTask maintaskDb, MainTaskAutoAssignInputDTO mainTaskInput)
        {
            var availableDrivers = new List<DriverNearestAvailableDTO>();

            IQueryable<Driver> availableDriversQuery = null;
            if (!mainTaskInput.Settings.IsTenantAllowedToUsePlatformDrivers)
            {
                var geoFences = await _nearestAvailableUtilities.GetGeofencesForTask(maintaskDb, mainTaskInput.Settings);
                if (geoFences == null || geoFences.PickupGeoFences == null || geoFences.DeliveryGeoFences == null)
                {
                    return availableDrivers;
                }
                mainTaskInput.Settings.PickupGeoFenceIds = geoFences.PickupGeoFences.Select(g => g.Id).ToList();
                mainTaskInput.Settings.DeliveryGeoFenceIds = geoFences.DeliveryGeoFences.Select(g => g.Id).ToList();

                availableDriversQuery = _nearestAvailableUtilities.GetAvailableDrivers(mainTaskInput);
            }
            else
            {
                availableDriversQuery = _nearestAvailableUtilities.GetAvailablePlatformDrivers();
            }

            //get drivers from database
            availableDrivers = await availableDriversQuery.ProjectTo<DriverNearestAvailableDTO>(_mapper.ConfigurationProvider).ToListAsync();
            if (availableDrivers == null || availableDrivers.Count <= 0)
            {
                return availableDrivers;
            }

            //locally => this one only get drivers within specific redis according to the system setting and its 
            availableDrivers = _nearestAvailableUtilities.SortAndFilterAvailableDriversByRedisDistance(maintaskDb, availableDrivers);
            return availableDrivers;
        }

        internal async Task<bool> AssignToDriverAsync(OneByOneDetermineDriverForTaskJobModel mainTaskVM, MainTask mainTask, TaskDriverRequests lastRequest)
        {
            //if driver a driver accepted the task
            if (progressSubStatusTasks.Contains(mainTask.Tasks.First()?.TaskStatusId ?? 0))
                return true;
            bool isSuccedded = false;
            //var driver = mainTaskVM.AvailableDrivers.FirstOrDefault();

            await _unityOfWork.RunTransaction(async () =>
            {
                if (lastRequest != null)
                {
                    lastRequest.RetriesCount++;
                    _taskDriverRequestRepository.Update(lastRequest);
                }
                else
                {
                    _taskDriverRequestRepository.Add(new TaskDriverRequests()
                    {
                        MainTaskId = mainTask.Id,
                        DriverId = mainTaskVM.DriverToAssign.Id,
                        RetriesCount = 0,
                        ResponseStatus = (int)TaskDriverResponseStatusEnum.NoResponse
                    });
                }

                mainTask.AssignmentType = (int)TaskAssignmentType.OneByOne;
                mainTask.ExpirationDate = DateTime.UtcNow.AddSeconds(mainTaskVM.MainTask.Settings.IntervalInSeconds);


                _nearestAvailableUtilities.UpdateTaskDriver(mainTask, mainTaskVM.DriverToAssign.Id, null, status: TaskStatusEnum.Assigned, assignmentType: TaskAssignmentType.OneByOne, null, reachedTime: mainTaskVM.DriverToAssign.ReachedTime);
                mainTask = _taskCommonService.UpdateMainTaskStatus(mainTask);
                _nearestAvailableUtilities.AddDriverTaskNotification(mainTask.Tasks.FirstOrDefault(),
                _localizer[Keys.Notifications.NewTasksAssignedToYou],
                (int)NotificationTypeEnum.NewTask);

                int changes = await _unityOfWork.SaveChangesAsync();


                isSuccedded = changes > 0;
            });


            await _nearestAvailableUtilities.SendPushNotificationToDriverAsync(
               _localizer[Keys.Notifications.TaskPendingForYourAcceptance],
               mainTaskVM.DriverToAssign.UserId, new
               {
                   mainTask = mainTaskVM.MainTask,
                   expirationTime = mainTaskVM.MainTask.Settings.IntervalInSeconds,
                   notificationType = NotificationTypeEnum.NewTask
               });

            await _nearestAvailableUtilities.AutoAllocationSucssesfullAsync(mainTask);

            return isSuccedded;
        }

        internal async Task<bool> EndAutoAllocationAsync(MainTask mainTask, TaskDriverRequests lastRequest)
        {
            int changes = 0;
            await _unityOfWork.RunTransaction(async () =>
            {
                if (lastRequest != null)
                {
                    lastRequest.ResponseStatus = (int)TaskDriverResponseStatusEnum.Expired;
                    _taskDriverRequestRepository.Update(lastRequest);
                }
                //mainTask.MainTaskStatusId = null;
                mainTask.IsFailToAutoAssignDriver = true;
                mainTask.IsCompleted = false;
                _nearestAvailableUtilities.UpdateTaskDriver(mainTask, null, null, null, null, null, null);
                changes = await _unityOfWork.SaveChangesAsync();
            });
            var title = _localizer[Keys.Notifications.TaskAutoAllocationFailed];
            var body = _localizer.Format(Keys.Notifications.TaskAutoAllocationFailedDetailsTaskId, mainTask.Id);
            _notificationService.SendToUserAndTenantAdmin(_currentUser.Id, "AutoAllocationFailed", title, body, mainTask.Id);
            return true;
        }
    }


    public class OneByOneDetermineDriverForTaskJobModel
    {
        public bool IsFailToAutoAssignDriver { get; set; }
        public bool IsLastDriverInQuee { get; set; }
        public DriverNearestAvailableDTO DriverToAssign { get; set; }
        public MainTaskAutoAssignInputDTO MainTask { get; set; }
    }

}

