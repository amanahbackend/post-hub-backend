﻿using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.TasksFolder.Queries;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public class OneByOneTaskAssignment : TaskAutoAssignment
    {
        private readonly INearestAvailableMethods _nearestAvailableUtilities;
        private readonly IOneByOneBackGroundJob _oneByOneBackGroundJob;
        private readonly ISettingRepository _settingRepository;

        public OneByOneTaskAssignment(
             INearestAvailableMethods nearestAvailableUtilotes,
             IOneByOneBackGroundJob oneByOneBackGroundJob,
            ISettingRepository settingRepository
            )
        {
            _nearestAvailableUtilities = nearestAvailableUtilotes;
            _oneByOneBackGroundJob = oneByOneBackGroundJob;
            _settingRepository = settingRepository;
        }

        public override async Task<MainTaskAutoAssignResultDTO> AssignDriverToTaskAsync(MainTaskAutoAssignInputDTO task)
        {
            //init settings for the 'one by one' algorithm
            await SetTaskSettingsAsync(task);

            //get mainTask for schedual
            var mainTask = await _nearestAvailableUtilities.GetMainTaskForStatusAndIncludeTasks(task.Id);
            await _oneByOneBackGroundJob.SchedualForAllAvailableDriversAsync(new OneByOneDetermineDriverForTaskJobModel
            {
                MainTask = task
            }, mainTask);

            return new MainTaskAutoAssignResultDTO
            {
                IsSuccessed = true
            };
        }

        private async Task SetTaskSettingsAsync(MainTaskAutoAssignInputDTO task)
        {
            int interval = 180;
            var retries = 2;

            var expiresSetting = await _settingRepository.GetSettingByKeyAsync(
                SettingsKeys.OneByOneAllocationRequestExpiresInSEC);
            if (expiresSetting != null)
            {
                int.TryParse(expiresSetting.Value, out interval);
            }
            var retriesSetting = await _settingRepository.GetSettingByKeyAsync(
                SettingsKeys.OneByOneAllocationNumberOfRetries);
            if (retriesSetting != null)
            {
                int.TryParse(retriesSetting.Value, out retries);
            }

            task.Settings.IntervalInSeconds = interval;
            task.Settings.MaxRetriesCount = retries;
            task.Settings.RetriesCount = 0;
            task.Settings.RemainingDrivers = task.Settings.DriversCount;
        }

    }
}
