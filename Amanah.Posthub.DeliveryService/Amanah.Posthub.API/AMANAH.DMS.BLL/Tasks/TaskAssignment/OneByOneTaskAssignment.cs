﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AMANAH.DMS.BLL.Enums;
using AMANAH.DMS.BLL.IManagers;
using AMANAH.DMS.BLL.Settings.Resources;
using AMANAH.DMS.BLL.Tasks.TaskAssignment.DTOs;
using AMANAH.DMS.BLL.Tasks.WebServices.DTOs;
using AMANAH.DMS.BLL.TasksFolder.TaskAssignment;
using AMANAH.DMS.BLL.ViewModels;
using AMANAH.DMS.BLL.ViewModels.Tasks;
using AMANAH.DMS.DATA.Entities;
using AMANAH.DMS.SharedKernel.BackgroundJobs;
using Utilities.Utilites.Localization;

namespace AMANAH.DMS.BLL.Managers.TaskAssignment
{
    public class OneByOneTaskAssignmentOld : TaskAutoAssignment
    {
        private static int RETRIES_DEFAULT_VALUE = 3;
        private static int INTERVAL_DEFAULT_VALUE = 60;
        private readonly ITasksManager _tasksManager;
        private readonly ISettingsManager _settingsManager;
        private readonly IBackgroundJobManager _jobManager;

        public OneByOneTaskAssignmentOld(
            ITasksManager tasksManager,
            ISettingsManager settingsManager,
            IBackgroundJobManager jobManager)
        {
            _tasksManager = tasksManager;
            _settingsManager = settingsManager;
            _jobManager = jobManager;
        }

        public override async Task<MainTaskAutoAssignResultDTO> AssignDriverToTaskAsync(MainTaskAutoAssignInputDTO task)
        {
            await SetTaskSettingsAsync(task);
            _jobManager.Enqueue(new OneByOneDetermineDriverForTaskJobModel
            {
                MainTask = task
            });

            return new MainTaskAutoAssignResultDTO { IsSuccessed = true };
        }

        private async Task SetTaskSettingsAsync(MainTaskAutoAssignInputDTO task)
        {
            int interval = INTERVAL_DEFAULT_VALUE;
            var retries = RETRIES_DEFAULT_VALUE;
            var expiresSetting = await _settingsManager.GetSettingByKeyAsync(
                SettingsKeys.OneByOneAllocationRequestExpiresInSEC);
            if (expiresSetting != null)
            {
                int.TryParse(expiresSetting.Value, out interval);
            }
            var retriesSetting = await _settingsManager.GetSettingByKeyAsync(
                SettingsKeys.OneByOneAllocationNumberOfRetries);
            if (retriesSetting != null)
            {
                int.TryParse(retriesSetting.Value, out retries);
            }

            //ToDo after refactor-----
            //var geoFences = await _tasksManager.GetGeofencesForTask(task);
            //task.Settings.PickupGeoFenceIds = geoFences.PickupGeoFences.Select(g => g.Id).ToList();
            //task.Settings.DeliveryGeoFenceIds = geoFences.DeliveryGeoFences.Select(g => g.Id).ToList();
            task.Settings.MaxRetriesCount = retries;
            task.Settings.RetriesCount = 0;
            task.Settings.RemainingDrivers = task.Settings.DriversCount;
            task.Settings.IntervalInSeconds = interval;
        }
    }

    //internal class OneByOneDetermineDriverForTaskJobModel
    //{
    //    public MainTaskAutoAssignInputDTO Task { get; set; }
    //}
    internal class OneByOneDetermineDriverForTaskJob : IBackgroundJob<OneByOneDetermineDriverForTaskJobModel>
    {

        private readonly ILocalizer _localizer;
        private readonly IBackgroundJobManager _jobManager;
        private readonly ITasksManager _tasksManager;
        private readonly IAutoAllocationHelperService _autoAllocationHelperService;

        public OneByOneDetermineDriverForTaskJob(
            ITasksManager tasksManager,
            IAutoAllocationHelperService autoAllocationHelperService,
            ILocalizer localizer,
            IBackgroundJobManager jobManager)
        {
            _tasksManager = tasksManager;
            _autoAllocationHelperService = autoAllocationHelperService;
            _localizer = localizer;
            _jobManager = jobManager;
        }

        public Task ExecuteAsync(OneByOneDetermineDriverForTaskJobModel jobModel)
        {
            return DetermineDriverForTaskAsync(jobModel.MainTask);
        }

        private async Task DetermineDriverForTaskAsync(MainTaskAutoAssignInputDTO mainTask)
        {
            int interval = mainTask.Settings.IntervalInSeconds;

            //TODO Later
            //if (!mainTask.Tasks.First().DriverId.HasValue)//First time
            //{
            //    mainTask.Settings.RetriesCount = 0;
            //    //ToDo After Refactor---
            //    //var availableDrivers = await _autoAllocationHelperService.GetAvailableDriversAsync(mainTask);
            //    //availableDrivers = await _autoAllocationHelperService.SortDriversByNearestAsync(mainTask, availableDrivers);
            //    //var driver = availableDrivers.FirstOrDefault();

            //    //if (driver == null)  //if no availlable drivers 
            //    //{
            //    //    return;
            //    //}

            //    //mainTask.AssignmentType = (int)TaskAssignmentType.OneByOne;
            //    //mainTask.ExpirationDate = DateTime.UtcNow.AddSeconds(interval);

            //    //await _tasksManager.UpdateTaskDriverAsync(
            //    //    mainTask.Id,
            //    //    driver.Id,
            //    //    status: TaskStatusEnum.Assigned,
            //    //    assignmentType: TaskAssignmentType.OneByOne,
            //    //    expirationDate: DateTime.UtcNow.AddSeconds(interval));

            //    //mainTask.Tasks.ForEach(t => t.DriverId = driver.Id);

            //    //await SendTaskNotificationToDriverAsync(driver.Id, mainTask, interval);
            //    //var taskDriverRequest = new TaskDriverRequestViewModel
            //    //{
            //    //    MainTaskId = mainTask.Id,
            //    //    DriverId = driver.Id,
            //    //    ResponseStatus = TaskDriverResponseStatusEnum.NoResponse,
            //    //    RetriesCount = 0,
            //    //    CreatedDate = DateTime.UtcNow
            //    //};
            //    //await _tasksManager.CreateTaskDriverRequest(taskDriverRequest);
            //}
            //else //old task
            //{
            //    var driverId = mainTask.Tasks.First().DriverId.Value;
            //    var r = await _tasksManager.GetTaskDriverRequests(mainTask.Id, driverId);
            //    var request = r.FirstOrDefault();
            //    if (mainTask.Tasks.First().TaskStatusId == (int)TaskStatusEnum.Accepted ||
            //        request.ResponseStatus == TaskDriverResponseStatusEnum.Accepted)
            //    {
            //        //task.Settings.RemainingDrivers--;
            //        //if (task.Settings.RemainingDrivers < 1) return;// No needed drivers remainging so exit;

            //        ////create new task for the next driver
            //        //task.Id = 0;
            //        //await StartJob(task);
            //        return;
            //    }

            //    if (request.RetriesCount >= mainTask.Settings.MaxRetriesCount)
            //    {
            //        request.ResponseStatus = TaskDriverResponseStatusEnum.Expired;
            //        await _tasksManager.UpdateTaskDriverRequest(request);
            //    }

            //    var prevRequests = await _tasksManager.GetTaskDriverRequests(mainTask.Id);
            //    var excludedDriverIds = prevRequests.Where(p =>
            //    p.ResponseStatus == TaskDriverResponseStatusEnum.Expired ||
            //    p.ResponseStatus == TaskDriverResponseStatusEnum.Declined ||
            //    p.RetriesCount == mainTask.Settings.RetriesCount)
            //        .Select(p => p.DriverId.Value).ToList();
                
            //    //ToDo After Refactor---
            //    //var allAvailableDrivers = await _autoAllocationHelperService.GetAvailableDriversAsync(mainTask);

            //    //allAvailableDrivers = await _autoAllocationHelperService.SortDriversByNearestAsync(mainTask, allAvailableDrivers);
            //    ////allAvailableDrivers = allAvailableDrivers.OrderByDescending(x => x.RouteDistanceToTask).ToList();
            //    //var availableDriverIds = allAvailableDrivers.Select(d => d.Id).Except(excludedDriverIds);
            //    ////No more available drivers, start from the begining if task didn't reach max retries
            //    //if (!availableDriverIds.Any())
            //    //{
            //    //    if (mainTask.Settings.RetriesCount < mainTask.Settings.MaxRetriesCount && allAvailableDrivers.Any())
            //    //    {
            //    //        mainTask.Settings.RetriesCount++;
            //    //        await DetermineDriverForTaskAsync(mainTask);
            //    //        return;
            //    //    }
            //    //    else //Task reached max retries or no available drivers to retry so end task
            //    //    {
            //    //        await EndTaskAsync(mainTask);
            //    //        return;
            //    //    }
            //    //}

            //    ////Next driver Id
            //    //driverId = availableDriverIds.FirstOrDefault();
            //    var req = prevRequests.Where(p => p.DriverId == driverId).FirstOrDefault();
            //    if (req != null)//Driver has existing request
            //    {
            //        req.RetriesCount++;
            //        await _tasksManager.UpdateTaskDriverRequest(req);
            //    }
            //    else
            //    {
            //        req = new TaskDriverRequestViewModel
            //        {
            //            MainTaskId = mainTask.Id,
            //            DriverId = driverId,
            //            RetriesCount = 0,
            //            ResponseStatus = TaskDriverResponseStatusEnum.NoResponse
            //        };
            //        await _tasksManager.CreateTaskDriverRequest(req);
            //    }

            //    mainTask.Tasks.ToList().ForEach(t => t.DriverId = driverId);
            //    await _tasksManager.UpdateTaskDriverAsync(mainTask.Id, driverId, interval, status: TaskStatusEnum.Assigned);
            //    mainTask.Tasks.ToList().ForEach(t => t.DriverId = driverId);
            //    await SendTaskNotificationToDriverAsync(driverId, mainTask, interval);
            //}
            _jobManager.Enqueue(
                new OneByOneDetermineDriverForTaskJobModel { MainTask = mainTask },
                TimeSpan.FromSeconds(interval));
        }
        /// <summary>
        /// No more drivers to assign
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        private async Task EndTaskAsync(MainTaskAutoAssignInputDTO task)
        {
            await _tasksManager.UpdateTaskDriverAsync(task.Id, null);
            await _tasksManager.AutoAllocationFail(task.Id);


            return;
        }

        private async Task SendTaskNotificationToDriverAsync(int driverId, MainTaskAutoAssignInputDTO mainTask, int interval)
        {
            await _tasksManager.SendPushNotificationToDriver(
                driverId,
                _localizer[Keys.Notifications.TaskPendingForYourAcceptance],
                new
                {
                    mainTask,
                    expirationTime = interval,
                    notificationType = NotificationTypeEnum.NewTask
                });
            await _tasksManager.AutoAllocationSucssesfull(mainTask.Id);
        }
    }
}
