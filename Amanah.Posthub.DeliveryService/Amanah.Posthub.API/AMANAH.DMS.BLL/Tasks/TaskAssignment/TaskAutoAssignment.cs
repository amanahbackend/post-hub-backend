﻿using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.ViewModels.Tasks;
using System.Net;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public abstract class TaskAutoAssignment
    {
        public abstract Task<MainTaskAutoAssignResultDTO> AssignDriverToTaskAsync(MainTaskAutoAssignInputDTO task);
        public virtual Task<CreateTaskResponse> AssignTasksToDriverAsync(int driverId)
        {
            return Task.FromResult(new CreateTaskResponse
            {
                StatusCode = HttpStatusCode.OK
            });
        }
    }
}
