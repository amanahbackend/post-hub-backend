﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers.TaskAssignment
{
    public enum TaskAssignmentType
    {
        Manual,
        NearestAvailable,
        OneByOne,
        FIFO
    }

    public class TaskAutoAssignmentFactory : ITaskAutoAssignmentFactory
    {
        protected readonly ISettingsManager _settingsManager;
        private readonly IServiceProvider _serviceProvider;

        public TaskAutoAssignmentFactory(ISettingsManager settingsManager, IServiceProvider serviceProvider)
        {
            _settingsManager = settingsManager;
            _serviceProvider = serviceProvider;
        }

        public async Task<TaskAutoAssignment> CreateTaskAutoAssignmentAsync()
        {
            TaskAssignmentType type = TaskAssignmentType.NearestAvailable;

            var method = await _settingsManager.GetSettingByKeyAsync(SettingsKeys.SelectedAutoAllocationMethodName);
            if (method?.Value == SettingsKeys.OneByOneMethod)
            {
                type = TaskAssignmentType.OneByOne;
            }
            else if (method?.Value == SettingsKeys.FirstInFirstOutMethod)
            {
                type = TaskAssignmentType.FIFO;
            }
            return type switch
            {
                TaskAssignmentType.NearestAvailable =>
                    ActivatorUtilities.CreateInstance<NearestAvailableTaskAssignment>(_serviceProvider),
                TaskAssignmentType.OneByOne => ActivatorUtilities.CreateInstance<OneByOneTaskAssignment>(_serviceProvider),
                TaskAssignmentType.FIFO => ActivatorUtilities.CreateInstance<FirstInFirstOutTaskAssignment>(_serviceProvider),
                _ => throw new NotImplementedException($"Type {type} is not implemented"),
            };
        }

        //public async Task AssignNextDriver(int mainTaskId)
        //{

        //}
    }
}
