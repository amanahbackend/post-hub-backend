﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Threading.Tasks;
//using AMANAH.DMS.BLL.Constants;
//using AMANAH.DMS.BLL.Enums;
//using AMANAH.DMS.BLL.IManagers;
//using AMANAH.DMS.BLL.Settings.ViewModels;
//using AMANAH.DMS.BLL.Tasks.TaskAssignment.DTOs;
//using AMANAH.DMS.BLL.Tasks.WebServices;
//using AMANAH.DMS.BLL.TasksFolder.Queries;
//using AMANAH.DMS.BLL.ViewModels;
//using AMANAH.DMS.BLL.ViewModels.Tasks;
//using AMANAH.DMS.DATA.Entities;
//using AMANAH.DMS.Domain.Drivers.Repositories;
//using AMANAH.DMS.Domain.MainTasks.Repositories;
//using AMANAH.DMS.Domain.SubTasks.Repositories;
//using AMANAH.DMS.SharedKernel.Domain.Repositories;
//using AMANAH.DMS.ViewModels;
//using AutoMapper;
//using AutoMapper.QueryableExtensions;
//using Hangfire;
//using MapsUtilities.MapsManager;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using Utilities.Utilites.Localization;

//namespace AMANAH.DMS.BLL.Managers.TaskAssignment
//{
//    public class NearestAvailableTaskAssignment : TaskAutoAssignment
//    {
//        private readonly ILocalizer _localizer;
//        private readonly INearestAvailableMethods _methods;
//        private readonly IUnitOfWork _unityOfWork;
//        private readonly ITaskCommonService _taskCommonService;
//        private readonly IMapper _mapper;
//        private readonly ISettingRepository _settingRepository;

//        public NearestAvailableTaskAssignment(
//            //ISubTaskRepository subtaskRepo,
//            ISettingRepository settingRepository,
//            //IMapsManager mapsManager,
//            //IConfiguration configuration,
//            ILocalizer localizer,
//            //IDriverRepository driverRepo,
//            IUnitOfWork unityOfWork,
//             IMapper mapper,
//             INearestAvailableMethods methods,
//             ITaskCommonService taskCommonService
//            )
//        //: base(settingRepository, mapsManager, configuration, subtaskRepo, driverRepo, _mapper)
//        {
//            _settingRepository = settingRepository;
//            _localizer = localizer;
//            _unityOfWork = unityOfWork;
//            _mapper = mapper;
//            _methods = methods;
//            _taskCommonService = taskCommonService;
//        }

//        public override async Task<MainTaskAutoAssignResultDTO> AssignDriverToTaskAsync(MainTaskAutoAssignInputDTO task)
//        {
//            task.Settings.RetriesCount = 0;
//            var dataToEgnoreTasks = _mapper.Map<MainTaskAutoAssignForNearestInputDTO>(task);
//            task = _mapper.Map<MainTaskAutoAssignInputDTO>(dataToEgnoreTasks);
//            var taskVM = new BackgroundJobViewModel<MainTaskAutoAssignInputDTO> { ViewModel = task };

//            BackgroundJob.Enqueue(() => AssignDriverAsync(taskVM));
//            return new MainTaskAutoAssignResultDTO { IsSuccessed = true };
//        }


//        public async Task AssignDriverAsync(BackgroundJobViewModel<MainTaskAutoAssignInputDTO> mainTaskVM)
//        {
//            DefaultSettingValue defaultSettingValue = new DefaultSettingValue();
//            int autoAllocationNeasrestRetriesCount = defaultSettingValue.NearestAvailableMethodNumberOfRetries;
//            var autoAllocationNeasrestRetriesSetting = await _settingRepository.GetSettingByKeyAsync(SettingKeyConstant.NearestAvailableMethodNumberOfRetries);
//            if (autoAllocationNeasrestRetriesSetting != null)
//            {
//                int.TryParse(autoAllocationNeasrestRetriesSetting.Value, out autoAllocationNeasrestRetriesCount);
//            }

//            mainTaskVM.ViewModel.Settings.RetriesCount++;
//            var maintask = mainTaskVM.ViewModel;
//            var IsDriverAssignedToTask = await DetermineTaskDriverAsync(maintask);
//            if (!IsDriverAssignedToTask)
//            {
//                if (maintask.Settings.RetriesCount < autoAllocationNeasrestRetriesCount)
//                {
//                    BackgroundJob.Schedule(() => AssignDriverAsync(mainTaskVM),
//                        TimeSpan.FromSeconds(defaultSettingValue.NearestAvailableMethodReScheduleRequestTimeInSec));
//                }
//                else
//                {
//                    await _methods.AutoAllocationFailAsync(maintask.Id);   //  BackgroundJob.Schedule(() => AssignDriver(mainTaskVM), TimeSpan.FromMinutes(1));
//                }
//            }
//        }


//        private async Task<bool> DetermineTaskDriverAsync(MainTaskAutoAssignInputDTO task)
//        {

//            //get setting for one transaction from fb
//            var neededSettings = await _settingRepository.GetSettingByKeyAsync(
//                 new List<string> { SettingKeyConstant.NearestAvailableMethodRadiusInKM, Settings.Resources.SettingsKeys.NearestAvailableMethodOrderCapacity }).ToListAsync();
//            var radiusSetting = neededSettings.FirstOrDefault(x => x.SettingKey.Trim().ToLower() == SettingKeyConstant.NearestAvailableMethodRadiusInKM.Trim().ToLower());
//            var MaximumDriverCapacitySetting = neededSettings.FirstOrDefault(x => x.SettingKey.Trim().ToLower() == Settings.Resources.SettingsKeys.NearestAvailableMethodOrderCapacity.Trim().ToLower());

//            if (task?.Settings?.DriversCount == 0)
//            {
//                return true;
//            }
//            //maintask with tasks included
//            var mainTask = await _methods.GetMainTaskForStatusAndIncludeTasks(task.Id);

//            if (mainTask == null || mainTask.IsDeleted) return true;
//            if (mainTask.MainTaskStatusId.HasValue && mainTask.MainTaskStatusId.Value != 0)
//            {
//                return true;
//            }

//            IQueryable<Driver> availableDriversQuery = null;
//            if (!task.Settings.IsTenantAllowedToUsePlatformDrivers)
//            {
//                var geoFences = await _methods.GetGeofencesForTask(mainTask, task.Settings);
//                if (geoFences == null || geoFences.PickupGeoFences == null || geoFences.DeliveryGeoFences == null)
//                {
//                    return false;
//                }
//                task.Settings.PickupGeoFenceIds = geoFences.PickupGeoFences.Select(g => g.Id).ToList();
//                task.Settings.DeliveryGeoFenceIds = geoFences.DeliveryGeoFences.Select(g => g.Id).ToList();

//                availableDriversQuery = _methods.GetAvailableDrivers(task);
//            }
//            else
//            {
//                availableDriversQuery = _methods.GetAvailablePlatformDrivers();
//            }

//            if (availableDriversQuery == null || availableDriversQuery.Count() <= 0)
//            {
//                return false;
//            }
//            //locally =>  Without Contacting db => dont get drivers that "have maximum capacity" from db
//            availableDriversQuery = FilterAvailableDriversByCapacity(availableDriversQuery, MaximumDriverCapacitySetting);


//            //get drivers from database
//            var availableDrivers = await availableDriversQuery.ProjectTo<DriverViewModelNearestAvailable>(_mapper.ConfigurationProvider).ToListAsync();
//            if (availableDrivers == null || availableDrivers.Count <= 0)
//            {
//                return false;
//            }


//            //locally => this one only get drivers within specific redis according to the system setting and its 
//            availableDrivers = SortAndFilterAvailableDriversByRedisDistance(mainTask, availableDrivers, radiusSetting);
//            if (availableDrivers == null || availableDrivers.Count <= 0)
//            {
//                return false;
//            }


//            //reduse list to half
//            int numberOfDriversToGotoGoogle = (int)Math.Ceiling((double)availableDrivers.Count / 2);
//            //if half is liss than 10 then take 10
//            numberOfDriversToGotoGoogle = numberOfDriversToGotoGoogle < 10 ? 10 : numberOfDriversToGotoGoogle;
//            availableDrivers = availableDrivers?.Take(numberOfDriversToGotoGoogle).ToList();


//            availableDrivers = await _methods.SortDriversAccordingToGoogleByNearestAsync(mainTask, availableDrivers);
//            availableDrivers = availableDrivers?.Take(task.Settings.DriversCount).ToList();

//            if (availableDrivers == null || availableDrivers.Count <= 0)
//            {
//                return false;
//            }

//            foreach (var driver in availableDrivers)
//            {
//                await _unityOfWork.RunTransaction(async () =>
//                {
//                    _methods.UpdateTaskDriver(mainTask, driver.Id, null, status: TaskStatusEnum.Accepted, assignmentType: TaskAssignmentType.NearestAvailable, null, reachedTime: driver.ReachedTime);
//                    mainTask = _taskCommonService.UpdateMainTaskStatus(mainTask);
//                    _methods.AddDriverTaskNotification(mainTask.Tasks.FirstOrDefault(),
//                    _localizer[Keys.Notifications.NewTasksAssignedToYou],
//                    (int)NotificationTypeEnum.NewTask);
//                    await _unityOfWork.SaveChangesAsync();
//                });
//                //await _methods.UpdateTaskDriverAsync(task.Id, driver.Id, status: TaskStatusEnum.Accepted, assignmentType: TaskAssignmentType.NearestAvailable, reachedTime: driver.ReachedTime);
//                //dbMainTask = _taskCommonService.UpdateMainTaskStatus(dbMainTask);

//                //await _methods.UpdateMainTaskStatus(task.Id);
//                await _methods.AutoAllocationSucssesfullAsync(mainTask);

//                await _methods.SendPushNotificationToDriver(
//                    _localizer[Keys.Notifications.NewTasksAssignedToYou],
//                    driver.UserId);

//            }

//            return true;
//        }

//        private List<DriverViewModelNearestAvailable> SortAndFilterAvailableDriversByRedisDistance(MainTask task,
//            List<DriverViewModelNearestAvailable> availableDrivers, Setting radiusSetting)
//        {
//            if (radiusSetting != null && double.TryParse(radiusSetting.Value, out double radius))
//            {
//                var filteredDrivers = new List<DriverViewModelNearestAvailable>();

//                //var list = availableDrivers
//                //    //get distance for each driver 
//                //    .Select(x => new { d = x, distance = GetDistance(task, x) }).ToList()
//                //    //neglect driver that oustside the redius
//                //    .Where(x => x.distance <= radius)
//                //    //order by driver distance 
//                //    .OrderBy(x => x.distance);

//                ////drivers sorted by nearest by nearest to task
//                //var result = list.Select(x => x.d).ToList();

//                //return result;


//                foreach (var driver in availableDrivers)
//                {
//                    driver.DistanceToTaskAsStraight = _methods.GetDistance(task, driver);
//                    if (driver.DistanceToTaskAsStraight <= radius)
//                    {
//                        filteredDrivers.Add(driver);
//                    }
//                }
//                filteredDrivers = filteredDrivers
//                    .OrderBy(x => x.DistanceToTaskAsStraight)
//                    .ToList();

//                return filteredDrivers;
//            }

//            return availableDrivers;
//        }

//        private IQueryable<Driver> FilterAvailableDriversByCapacity(IQueryable<Driver> availableDrivers, Setting MaximumDriverCapacitySetting)
//        {
//            int NearstOrderCapacity = 5;


//            if (MaximumDriverCapacitySetting != null)
//            {
//                int.TryParse(MaximumDriverCapacitySetting.Value, out NearstOrderCapacity);
//            }

//            var Statuses = new List<int>
//                {
//                    (int)TaskStatusEnum.Assigned,
//                    (int) TaskStatusEnum.Accepted
//                };


//            availableDrivers = availableDrivers.Where(d => d.AssignedTasks.Count(t => Statuses.Contains(t.TaskStatusId)) < NearstOrderCapacity);

//            return availableDrivers;
//        }
//    }
//}
