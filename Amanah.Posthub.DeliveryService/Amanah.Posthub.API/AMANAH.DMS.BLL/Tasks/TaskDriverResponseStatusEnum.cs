﻿namespace Amanah.Posthub.BLL.Enums
{
    public enum TaskDriverResponseStatusEnum
    {
        Accepted = 1,
        Declined,
        Expired,
        NoResponse,
        Canceled
    }
}
