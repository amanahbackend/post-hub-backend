﻿namespace Amanah.Posthub.BLL.Enums
{
    public enum TaskStatusEnum
    {
        Unassigned = 1,
        Assigned = 2,
        Accepted = 3,
        Started = 4,
        Inprogress = 5,
        Successful = 6,
        Failed = 7,
        Declined = 8,
        Cancelled = 9,
    }

    public enum MainTaskStatus
    {
        Assigned = 1,
        Inprogress = 2,
        Successful = 3,
        Failed = 4,
        Accepted = 5,
        Canceled = 9,
        Declined = 10
    }
}
