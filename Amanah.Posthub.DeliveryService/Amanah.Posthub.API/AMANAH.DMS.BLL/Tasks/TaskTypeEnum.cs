﻿namespace Amanah.Posthub.BLL.Enums
{
    public enum TaskTypeEnum
    {
        Pickup = 1,
        Delivery = 2,
    }
}
