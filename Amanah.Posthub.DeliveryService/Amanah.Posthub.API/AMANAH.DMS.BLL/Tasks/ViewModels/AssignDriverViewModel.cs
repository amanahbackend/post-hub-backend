﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class DriverForAssignViewModel
    {
        public int Id { set; get; }
        public string ImageUrl { get; set; }
        public int TeamId { set; get; }
        public string Tags { set; get; }
        public string Name { get; set; }
        public int? AgentStatusId { get; set; }
        public string AgentStatusName { set; get; }
    }
}
