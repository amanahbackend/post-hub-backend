﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class BackgroundJobViewModel<T>
    {
        public BackgroundJobViewModel()
        {
        }

        public T ViewModel { get; set; }
    }
}
