﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class CreateTaskHistoryViewModel
    {
        public TasksViewModel TaskVM { get; set; }
        public string ActionName { get; set; }
        public int? FromStatusId { set; get; }
        public int? ToStatusId { set; get; }
        public double? Longitude { set; get; }
        public double? Latitude { set; get; }

        public string Reason { set; get; }
        public string Description { set; get; }

        public string TenantId { get; set; }

    }
}
