﻿using System.Net;

namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class CreateTaskResponse
    {
        public MainTaskViewModel MainTask { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}
