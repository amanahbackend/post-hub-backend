﻿using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class ExportTasksWithoutProgressViewModel
    {
        public string Order_Id { set; get; }
        public string Driver_Name { set; get; }
        public string Order_Type { set; get; }
        public string Order_Status { set; get; }
        public string Name { set; get; }
        public string Address { set; get; }
        public DateTime? Order_DateTime { get; set; }
        //public double? Rating { get; set; }
        //public string Comments { set; get; }
        public string Reference_Image { set; get; }
        public string Order_Description { set; get; }

    }
}
