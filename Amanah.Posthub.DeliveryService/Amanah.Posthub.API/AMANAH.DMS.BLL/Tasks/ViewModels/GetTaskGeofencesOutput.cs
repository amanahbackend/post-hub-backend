﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class GetTaskGeofencesOutput
    {
        public GetTaskGeofencesOutput()
        {
            PickupGeoFences = new List<GeoFenceResultDTO>();
            DeliveryGeoFences = new List<GeoFenceResultDTO>();
        }
        public List<GeoFenceResultDTO> PickupGeoFences { get; set; }
        public List<GeoFenceResultDTO> DeliveryGeoFences { get; set; }
    }
}
