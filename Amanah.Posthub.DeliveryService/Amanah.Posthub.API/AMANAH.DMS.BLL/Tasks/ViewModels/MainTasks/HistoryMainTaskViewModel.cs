﻿using Amanah.Posthub.BLL.ViewModels.Mobile;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class HistoryMainTaskViewModel : BaseMainTaskViewModel
    {
        public List<TaskHistoryDetailsViewModel> Tasks { set; get; }
    }
}
