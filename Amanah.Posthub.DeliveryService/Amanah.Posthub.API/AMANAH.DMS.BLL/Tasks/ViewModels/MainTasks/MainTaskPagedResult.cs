﻿using System;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class MainTaskPagedResult<T> : PagedResult<T>
    {
        public int DriverAgentStatusId { get; set; }
        public bool DriverIsSetReached { get; set; }
        public int AutoAllocationType { get; set; }
        public bool IsInClubbingTime { get; set; }
        public DateTime? ClubbingTimeExpiration { get; set; }
    }
}
