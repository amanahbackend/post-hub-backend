﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class ReassignDriverMainTaskViewModel
    {
        public int MainTaskId { get; set; }
        public List<int> DriverIds { get; set; }
        public int TaskStatusId { get; set; }
    }
}
