﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class PaginatedTasksDriverMobileInputDTO : PaginatedItemsViewModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<int> TaskStatusIds { get; set; }
        public int DriverId { get; set; }
    }
}
