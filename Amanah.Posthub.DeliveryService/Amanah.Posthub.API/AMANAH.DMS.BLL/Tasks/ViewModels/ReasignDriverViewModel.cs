﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class ReasignDriverViewModel
    {
        public int OldDriverId { get; set; }
        public int NewDriverId { get; set; }
        public List<int> TaskStatusIds { get; set; }
        public List<int> TaskIds { get; set; }
    }
}
