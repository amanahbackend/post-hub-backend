﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class TaskBasicSettingsViewModel
    {
        public List<int> DriverIds { get; set; }
        public List<int> TeamIds { get; set; }
        public List<string> Tags { get; set; }
        public List<int> PickupGeoFenceIds { get; set; }
        public List<int> DeliveryGeoFenceIds { get; set; }
    }
}
