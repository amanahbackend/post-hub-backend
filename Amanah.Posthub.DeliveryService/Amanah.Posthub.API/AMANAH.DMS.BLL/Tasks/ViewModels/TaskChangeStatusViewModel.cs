﻿namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class TaskChangeStatusViewModel
    {
        public int Id { set; get; }
        public int StatusId { set; get; }
        public string Reason { set; get; }
        public bool IsChangeConnectedTasks { set; get; }

    }
}
