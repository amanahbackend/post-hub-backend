﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.ViewModel;

namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class TaskDriverRequestViewModel : BaseViewModel
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int MainTaskId { set; get; }
        public int? DriverId { set; get; }
        public TaskDriverResponseStatusEnum ResponseStatus { set; get; }
        public int RetriesCount { get; set; }
        public MainTaskViewModel MainTask { get; set; }
    }
}
