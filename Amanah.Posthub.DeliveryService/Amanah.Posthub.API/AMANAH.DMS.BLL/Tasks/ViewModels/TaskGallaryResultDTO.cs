﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class TaskGallaryResultDTO
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public string FileName { set; get; }
        public string FileURL { set; get; }
    }
}
