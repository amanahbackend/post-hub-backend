﻿using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels.Mobile
{
    public class TaskHistoryDetailsViewModel : TasksBaseInfoViewModel
    {
        public DateTime? TaskDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string Notes { set; get; }
        public string SignatureFileName { set; get; }//----
        public string SignatureURL { set; get; }//----
        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }

        public List<TaskGallaryResultDTO> TaskGallaries { set; get; }//----
    }
}
