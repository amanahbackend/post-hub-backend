﻿using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class TaskPagedResult<T> : PagedResult<T>
    {
        public int TotalOrderCount { set; get; }
    }
}
