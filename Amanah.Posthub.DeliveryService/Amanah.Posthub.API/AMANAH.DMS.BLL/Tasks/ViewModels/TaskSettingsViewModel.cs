﻿namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class TaskSettingsViewModel : TaskBasicSettingsViewModel
    {
        public bool Auto { get; set; }
        public int DriversCount { get; set; }
        public bool RestrictGeofences { get; set; } = true;
        public int RemainingDrivers { get; set; }
        public int RetriesCount { get; set; }
        public int MaxRetriesCount { get; set; }
        public int IntervalInSeconds { get; set; }

        public bool IsTenantAllowedToUsePlatformDrivers { get; set; }
    }
}
