﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class TasksBaseInfoViewModel
    {
        public int Id { get; set; }
        public int? TaskStatusId { set; get; }
        public string TaskStatusName { set; get; }
        public string Image { set; get; }
        public IFormFile FormImage { set; get; }
        public string OrderId { set; get; }
        public string Address { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public int CustomerId { get; set; }
        public DateTime? StartDate { get; set; }
        public double? DelayTime { get; set; }
        public DateTime? SuccessfulDate { get; set; }
        public double? TotalTime { get; set; }
        public double? TotalDistance { get; set; }
        public double? TotalTaskTime { get; set; }
        public AddressResultDto Location { set; get; }

        public List<TaskHistoryViewModel> TaskHistories { set; get; }
        public List<DriverRateViewModel> DriverRates { get; set; }
    }


}
