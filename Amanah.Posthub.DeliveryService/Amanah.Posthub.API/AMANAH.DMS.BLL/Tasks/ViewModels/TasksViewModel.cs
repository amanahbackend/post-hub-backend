﻿using FluentValidation;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class TasksViewModel : TasksBaseInfoViewModel
    {
        public int MainTaskId { set; get; }
        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }
        public int? DriverId { set; get; }
        public string DriverName { set; get; }
        public string DriverPhoneNumber { set; get; }
        public string DriverImageUrl { set; get; }
        public string TeamName { set; get; }
        public string Description { set; get; } = "";
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int? GeoFenceId { get; set; }
        public bool? IsTaskReached { get; set; }
        public DateTime? ReachedTime { get; set; }
        public TimeViewModel TotalWaitingTime { get; set; }
        public TimeViewModel TotalEstimationTime { get; set; }

        public CustomerResultDto Customer { set; get; }


        public string Notes { set; get; }
        public string SignatureFileName { set; get; }
        public string SignatureURL { set; get; }

        public List<TaskGallaryResultDTO> TaskGallaries { set; get; }
    }

    public class TasksViewModelValidator : AbstractValidator<TasksViewModel>
    {
        public TasksViewModelValidator()
        {
            RuleFor(x => x.MainTaskId).NotEmpty().NotNull();
            RuleFor(x => x.TaskTypeId).NotEmpty().NotNull();
            RuleFor(x => x.Address).NotEmpty().NotNull();
            RuleFor(x => x.Customer).NotNull();

        }
    }
}
