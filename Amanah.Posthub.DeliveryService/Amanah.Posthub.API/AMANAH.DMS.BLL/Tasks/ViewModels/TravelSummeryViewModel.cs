﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels.Tasks
{
    public class TravelSummeryViewModel
    {
        public int TasksCount { get; set; }
        public double TasksHours { get; set; }
        public double? TasksKMs { get; set; }
        public List<TaskRouteDtoInput> TasksRoutes { get; set; }
    }
}
