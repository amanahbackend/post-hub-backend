﻿using System;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class UpdateDriverClubbingTimeViewModel
    {
        public int DriverId { get; set; }
        public bool IsInClubbingTime { get; set; }
        public DateTime? ClubbingTimeExpiration { get; set; }
    }
}
