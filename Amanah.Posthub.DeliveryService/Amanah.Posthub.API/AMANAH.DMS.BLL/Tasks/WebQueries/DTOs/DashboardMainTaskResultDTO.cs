﻿using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Tasks.WebQueries.DTOs
{
    public class DashboardMainTaskResultDTO
    {
        public int Id { set; get; }
        public int? MainTaskTypeId { set; get; }
        public string MainTaskTypeName { set; get; }
        public int? DriverId { set; get; }
        public string DriverName { set; get; }
        public string DriverPhoneNumber { set; get; }
        public string DriverImageUrl { set; get; }

        public int NoOfTasks { set; get; }
        public int NoOfCompletedTasks { set; get; }
        public bool? IsCompleted { set; get; }
        public bool? IsDelayed { set; get; }
        public DashboardTimeResultDTO TotalEstimationTime { get; set; }
        public int MainTaskStatus { get; set; }
        public string MainTaskStatusName { get; set; }

        public int? TeamId { get; set; }
        public string TeamName { get; set; }
        public bool IsFailToAutoAssignDriver { get; set; }

        public bool IsEnableAutoAllocation { get; set; }
        public int AutoAllocationType { get; set; }
        public int AssignmentType { get; set; }

        public List<DashboardSubTaskResultDTO> Tasks { set; get; }
    }

    public class DashboardSubTaskResultDTO
    {
        public bool IsDeleted { get; set; }
        public int Id { get; set; }
        public int? TaskStatusId { set; get; }
        public string TaskStatusName { set; get; }
        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public DashboardCustomerResultDTO Customer { set; get; }

        public int? TeamId { get; set; }
        public int? DriverId { set; get; }
        public string DriverName { set; get; }

    }

    public class DashboardTimeResultDTO
    {
        public int Day { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }
    }
    public class DashboardCustomerResultDTO
    {
        public string Name { set; get; }
        public string Address { set; get; }
    }

}
