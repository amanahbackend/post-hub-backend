﻿using System;

namespace Amanah.Posthub.BLL.Tasks.WebQueries.DTOs
{
    public class DriverTimelineResponseDTO
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public int TaskTypeId { set; get; }
        public int MainTaskId { set; get; }
        public int? FromStatusId { set; get; }
        public string FromStatusName { set; get; }
        public int? ToStatusId { set; get; }
        public string ToStatusName { set; get; }
        public string Reason { set; get; }
        public string Description { set; get; }
        public string ActionName { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Address { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { set; get; }

    }
}
