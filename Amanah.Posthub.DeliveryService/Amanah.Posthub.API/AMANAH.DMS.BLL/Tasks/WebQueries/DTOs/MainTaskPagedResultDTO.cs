﻿using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Tasks.WebQueries.DTOs
{
    public class MainTaskPagedResultDTO<T> : PagedResult<T>
    {
        public int TotalOrderCount { set; get; }
    }
}
