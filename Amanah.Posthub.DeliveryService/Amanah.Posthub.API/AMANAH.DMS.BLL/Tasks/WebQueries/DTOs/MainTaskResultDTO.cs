﻿using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Tasks.WebQueries.DTOs
{
    public class MainTaskResultDTO
    {
        public int Id { set; get; }
        public int MainTaskTypeId { set; get; }
        public string MainTaskTypeName { set; get; }
        public bool? IsCompleted { set; get; }
        public bool? IsDelayed { set; get; }
        public int AssignmentType { get; set; }
        public int? RemainingTimeInSeconds { set; get; }
        public DateTime? ExpirationDate { get; set; }
        public bool IsNewlyAssigned { get; set; }
        public bool IsFailToAutoAssignDriver { get; set; }
        public int OrderWeight => 1;
        public int? MainTaskStatusId { set; get; }
        public string MainTaskStatusName { set; get; }

        public List<SubTaskResultDTO> Tasks { set; get; }
        public SettingResultDTO Settings { get; set; }
    }
}
