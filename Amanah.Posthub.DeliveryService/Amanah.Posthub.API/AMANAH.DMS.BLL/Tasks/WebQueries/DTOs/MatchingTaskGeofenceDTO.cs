﻿using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Tasks.WebQueries.DTOs
{
    public class MatchingTaskGeofenceDTO
    {
        public MatchingTaskGeofenceDTO()
        {
            PickupGeoFences = new List<GeoFence>();
            DeliveryGeoFences = new List<GeoFence>();
        }
        public List<GeoFence> PickupGeoFences { get; set; }
        public List<GeoFence> DeliveryGeoFences { get; set; }

    }
}
