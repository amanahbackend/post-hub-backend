﻿using Amanah.Posthub.BLL.ViewModels;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Tasks.WebQueries.DTOs
{
    public class SubTaskResultDTO : TasksBaseInfoViewModel
    {
        public int MainTaskId { set; get; }
        public int TaskTypeId { set; get; }
        public string TaskTypeName { set; get; }
        public int? DriverId { set; get; }
        public string DriverName { set; get; }
        public string DriverPhoneNumber { set; get; }
        public string DriverImageUrl { set; get; }
        public int? TeamId { set; get; }
        public string TeamName { set; get; }
        public string Description { set; get; } = string.Empty;
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public int? GeoFenceId { get; set; }
        public bool? IsTaskReached { get; set; }
        public DateTime? ReachedTime { get; set; }
        public DashboardTimeResultDTO TotalWaitingTime { get; set; }
        public DashboardTimeResultDTO TotalEstimationTime { get; set; }

        public CustomerResultDto Customer { set; get; }

        public string Notes { set; get; }
        public string SignatureFileName { set; get; }
        public string SignatureURL { set; get; }

        public List<TaskGallaryResultDTO> TaskGallaries { set; get; }
    }
}
