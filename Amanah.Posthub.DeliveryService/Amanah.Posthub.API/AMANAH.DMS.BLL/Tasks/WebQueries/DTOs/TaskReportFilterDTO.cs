﻿using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Tasks.WebQueries.DTOs
{
    public class TaskReportFilterDTO : PaginatedItemsViewModel
    {
        public List<int> StatusIds { get; set; }
        public List<int> TaskTypeIds { get; set; }
        public List<int> DriversIds { get; set; }
        public List<int> ZonesIds { get; set; }
        public List<int> RestaurantIds { get; set; }
        public List<int> BranchIds { get; set; }
        public string OrderId { get; set; }
        public string Address { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool GetCustomerTasks { get; set; } = false;

    }
}
