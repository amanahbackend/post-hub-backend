﻿namespace Amanah.Posthub.BLL.Tasks.WebQueries.DTOs
{
    public class TaskStatusCountResultDTO
    {
        public int TaskStatusId { set; get; }
        public string TaskStatusName { set; get; }
        public int Count { set; get; }
    }
}
