﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Tasks.WebQueries.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MapsUtilities;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Tasks.WebQueries
{
    public interface IMainTaskQuery
    {
        Task<List<DTOs.MainTaskResultDTO>> GetAllAsync();
        Task<PagedResult<DTOs.MainTaskResultDTO>> GetAllByPaginationAsync(MainTaskFilterDTO mainTaskFilterDTO);
        Task<DTOs.MainTaskResultDTO> GetAsync(int id);
        Task<MainTaskPagedResultDTO<DashboardMainTaskResultDTO>> GetMainTasksByStatusAsync(TaskFilterDTO taskFilterDTO, bool? isCompleted);
        Task<PagedResult<DTOs.MainTaskResultDTO>> GetDriverUncompletedTasksAsync(TaskDriverFilterDTO taskFilterDTO);
        Task<bool> TaskHasGeofencesAsync(MainTaskInputDTO mainTask);

    }
    public class MainTaskQuery : IMainTaskQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public MainTaskQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        public async Task<List<DTOs.MainTaskResultDTO>> GetAllAsync()
        {
            return await ApplyInternalMainTaskQueryFilter(context.MainTask
                .Where(mainTask => mainTask.Tasks.Any(task => !task.IsDeleted)))
                .ProjectTo<DTOs.MainTaskResultDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        public async Task<PagedResult<DTOs.MainTaskResultDTO>> GetAllByPaginationAsync(MainTaskFilterDTO mainTaskFilterDTO)
        {
            return await ApplyInternalMainTaskQueryFilter(context.MainTask
                .Where(mainTask => mainTask.Tasks.Any(task => !task.IsDeleted)))
                .ToPagedResultAsync<MainTask, DTOs.MainTaskResultDTO>(mainTaskFilterDTO, mapper.ConfigurationProvider);
        }
        public async Task<DTOs.MainTaskResultDTO> GetAsync(int id)
        {
            var mainTask = await ApplyInternalMainTaskQueryFilter(context.MainTask
                .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.TaskHistories)
                .Where(mainTask => mainTask.Id == id))
                .ProjectTo<DTOs.MainTaskResultDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();

            return mainTask;
        }
        public async Task<MainTaskPagedResultDTO<DashboardMainTaskResultDTO>> GetMainTasksByStatusAsync(
            TaskFilterDTO filter,
            bool? isCompleted)
        {
            var pagedResult = new MainTaskPagedResultDTO<DashboardMainTaskResultDTO>();
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;

            var query = ApplyInternalMainTaskQueryFilter(context.MainTask.AsQueryable());
            query = ApplyInternalMainTaskQueryFilter(filter, isCompleted, query);

            pagedResult.TotalCount = await query
                .SelectMany(mainTask => mainTask.Tasks)
                .Where(task => !task.IsDeleted)
                .CountAsync();
            pagedResult.TotalOrderCount = await query.CountAsync();

            var mainTasks = await query
                 .Skip((filter.PageNumber - 1) * filter.PageSize)
                 .Take(filter.PageSize)
                 .OrderByDescending(mainTask => mainTask.Id)
                 .ToListAsync();

            var mappedMainTasks = mapper.Map<List<DashboardMainTaskResultDTO>>(mainTasks);
            mappedMainTasks.ForEach(mainTask =>
            {
                mainTask.Tasks = mainTask.Tasks.Where(task => !task.IsDeleted).ToList();
            });
            pagedResult.Result = mappedMainTasks;
            return pagedResult;
        }
        public async Task<PagedResult<DTOs.MainTaskResultDTO>> GetDriverUncompletedTasksAsync(TaskDriverFilterDTO taskDriverFilter)
        {
            var pagedResult = new PagedResult<DTOs.MainTaskResultDTO>();

            taskDriverFilter.PageNumber = (taskDriverFilter.PageNumber == 0)
                ? 1 :
                taskDriverFilter.PageNumber;
            taskDriverFilter.PageSize = (taskDriverFilter.PageSize == 0)
                ? 20
                : taskDriverFilter.PageSize;

            taskDriverFilter.FromDate = taskDriverFilter.FromDate.HasValue
                ? taskDriverFilter.FromDate
                : DateTime.UtcNow;
            taskDriverFilter.ToDate = taskDriverFilter.ToDate.HasValue
                ? taskDriverFilter.ToDate
                : DateTime.UtcNow;

            taskDriverFilter.TaskStatusIds = new List<int>()
            {
                    (int)TaskStatusEnum.Assigned,
                    (int)TaskStatusEnum.Accepted,
                    (int)TaskStatusEnum.Started,
                    (int)TaskStatusEnum.Inprogress
            };

            var query = ApplyInternalMainTaskQueryFilter(context.MainTask.Where(mainTask => mainTask.IsCompleted != true));
            query = ApplyInternalMainTaskQueryFilter(taskDriverFilter, query);

            pagedResult.TotalCount = await query.CountAsync();
            pagedResult.Result = await query
                .Skip((taskDriverFilter.PageNumber - 1) * taskDriverFilter.PageSize)
                .Take(taskDriverFilter.PageSize)
                .ProjectTo<DTOs.MainTaskResultDTO>(mapper.ConfigurationProvider)
                .OrderByDescending(mainTask => mainTask.Id)
                .ToListAsync();

            return pagedResult;
        }
        public async Task<bool> TaskHasGeofencesAsync(MainTaskInputDTO mainTask)
        {
            mainTask.Settings.RestrictGeofences = await CheckTenantRestrictGeofenceAsync();
            if (mainTask.Settings.RestrictGeofences)
            {
                var geoFences = await GetGeofencesForTaskAsync(mainTask);
                return geoFences.PickupGeoFences.Any();
            }
            return true;
        }


        private async Task<bool> CheckTenantRestrictGeofenceAsync()
        {
            var userRoleForTenant = await context.UserRoles
                  .Where(userRole => userRole.UserId == currentUser.TenantId)
                  .FirstOrDefaultAsync();
            if (userRoleForTenant != null)
            {
                return await context.RoleClaims
                 .Where(roleClaim => roleClaim.RoleId == userRoleForTenant.RoleId &&
                     roleClaim.ClaimValue == TenantPermissions.Agent.CreateAgent)
                 .AnyAsync();
            }
            return true;
        }
        private async Task<MatchingTaskGeofenceDTO> GetGeofencesForTaskAsync(MainTaskInputDTO mainTask)
        {
            var matchingTaskGeofences = new MatchingTaskGeofenceDTO();

            var firstPickupTask = mainTask.Tasks
                .FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Pickup);
            var firstDeliveryTask = mainTask.Tasks
                .FirstOrDefault(e => e.TaskTypeId == (int)TaskTypesEnum.Delivery);

            var geoFences = await context.GeoFence
                .Include(t => t.GeoFenceLocations)
                .OrderByDescending(x => x.CreatedDate)
                .ToListAsync();
            if (geoFences != null && geoFences.Any())
            {
                if (mainTask.Settings.RestrictGeofences)
                {
                    if (!firstPickupTask.Latitude.HasValue || !firstPickupTask.Longitude.HasValue ||
                        !firstDeliveryTask.Latitude.HasValue || !firstDeliveryTask.Longitude.HasValue)
                        throw new ArgumentNullException("Latitude and longitude can't be null");

                    MapPoint pickupPoint = new MapPoint
                    {
                        Latitude = firstPickupTask.Latitude.Value,
                        Longitude = firstPickupTask.Longitude.Value
                    };
                    MapPoint deliveryPoint = new MapPoint
                    {
                        Latitude = firstDeliveryTask.Latitude.Value,
                        Longitude = firstDeliveryTask.Longitude.Value
                    };
                    foreach (var geoFence in geoFences)
                    {
                        var polygon = geoFence.GeoFenceLocations
                            .Select(location => new MapPoint
                            {
                                Latitude = location.Latitude ?? 0,
                                Longitude = location.Longitude ?? 0
                            })
                            .ToArray();

                        if (MapsHelper.IsPointInPolygon(polygon, pickupPoint))
                        {
                            matchingTaskGeofences.PickupGeoFences.Add(geoFence);
                        }
                        if (MapsHelper.IsPointInPolygon(polygon, deliveryPoint))
                        {
                            matchingTaskGeofences.DeliveryGeoFences.Add(geoFence);
                        }
                    }

                }
                else
                {
                    foreach (var geoFence in geoFences)
                    {
                        var polygon = geoFence.GeoFenceLocations
                            .Select(geoFenceLocationVioewModel => new MapPoint
                            {
                                Latitude = geoFenceLocationVioewModel.Latitude ?? 0,
                                Longitude = geoFenceLocationVioewModel.Longitude ?? 0
                            })
                            .ToArray();
                        matchingTaskGeofences.PickupGeoFences.Add(geoFence);
                        matchingTaskGeofences.DeliveryGeoFences.Add(geoFence);
                    }

                }
            }

            return matchingTaskGeofences;
        }
        private IQueryable<MainTask> ApplyInternalMainTaskQueryFilter(
            TaskFilterDTO taskFilter,
            bool? isCompleted,
            IQueryable<MainTask> query)
        {
            query = query.Where(mainTask => mainTask.Tasks.Any(task => !task.IsDeleted));

            if (!string.IsNullOrEmpty(taskFilter.SearchBy))
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.Customer != null
                    && task.Customer.Name.Trim().ToLower().Contains(taskFilter.SearchBy)
                    || task.Address.Contains(taskFilter.SearchBy)));
            }
            if (taskFilter.Id > 0)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.Id == taskFilter.Id));
            }
            if (taskFilter.FromDate.HasValue)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.PickupDate.Value.Date >= taskFilter.FromDate.Value.Date
                    || task.DeliveryDate.Value.Date >= taskFilter.FromDate.Value.Date));
            }
            if (taskFilter.ToDate.HasValue)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.PickupDate.Value.Date <= taskFilter.ToDate.Value.Date
                      || task.DeliveryDate.Value.Date <= taskFilter.ToDate.Value.Date));
            }

            if (isCompleted.HasValue && isCompleted.Value)
            {
                query = query.Where(mainTask => mainTask.IsCompleted.HasValue && mainTask.IsCompleted.Value);
            }
            else if (isCompleted.HasValue && !isCompleted.Value)
            {
                query = query.Where(mainTask => mainTask.IsCompleted != true);
            }

            if (taskFilter.TaskStatusIds != null && taskFilter.TaskStatusIds.Any())
            {
                query = query.Where(mainTask => mainTask.Tasks.Where(task => taskFilter.TaskStatusIds.Contains(task.TaskStatusId)).Any());
            }
            if (taskFilter.TeamIds != null && taskFilter.TeamIds.Any())
            {
                query = query.Where(mainTask => mainTask.CreatedBy_Id == currentUser.UserName
                    || mainTask.Tasks.Where(task => taskFilter.TeamIds.Contains(task.Driver.TeamId)).Any());
            }
            if (taskFilter.BranchesIds != null && taskFilter.BranchesIds.Any())
            {
                query = query.Where(mainTask => mainTask.Tasks.Where(task => taskFilter.BranchesIds.Contains(task.BranchId ?? 0)).Any());
            }

            return query;
        }
        private IQueryable<MainTask> ApplyInternalMainTaskQueryFilter(TaskDriverFilterDTO taskDriverFilter,
            IQueryable<MainTask> query)
        {
            if (!string.IsNullOrEmpty(taskDriverFilter.SearchBy))
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.Customer != null
                    && task.Customer.Name.Trim().ToLower().Contains(taskDriverFilter.SearchBy)
                    || task.Address.Contains(taskDriverFilter.SearchBy)));
            }
            if (taskDriverFilter.Id > 0)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.Id == taskDriverFilter.Id));
            }
            if (taskDriverFilter.FromDate.HasValue)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.PickupDate.Value.Date >= taskDriverFilter.FromDate.Value.Date
                    || task.DeliveryDate.Value.Date >= taskDriverFilter.FromDate.Value.Date));
            }
            if (taskDriverFilter.ToDate.HasValue)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.PickupDate.Value.Date <= taskDriverFilter.ToDate.Value.Date
                      || task.DeliveryDate.Value.Date <= taskDriverFilter.ToDate.Value.Date));
            }
            if (taskDriverFilter.TaskStatusIds != null && taskDriverFilter.TaskStatusIds.Any())
            {
                query = query.Where(mainTask => mainTask.Tasks.Where(task => taskDriverFilter.TaskStatusIds.Contains(task.TaskStatusId)).Any());
            }
            if (taskDriverFilter.DriverId > 0)
            {
                query = query.Where(mainTask => mainTask.Tasks.Any(task => task.DriverId == taskDriverFilter.DriverId));
            }

            return query;
        }
        private IQueryable<MainTask> ApplyInternalMainTaskQueryFilter(IQueryable<MainTask> mainTasks)
        {
            return mainTasks
                .Include(mainTask => mainTask.MainTaskStatus)
                .Include(mainTask => mainTask.MainTaskType)
                .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.Customer)
                .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.TaskStatus)
                .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.TaskType)
                .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.Driver).ThenInclude(driver => driver.User)
                .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.Driver).ThenInclude(driver => driver.Team)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == currentUser.TenantId ||
                    (currentUser.DriverId.HasValue
                    && task.Tasks.Any(subTask => subTask.DriverId == currentUser.DriverId)
                    && string.IsNullOrEmpty(currentUser.TenantId)));
        }

    }
}
