﻿using Amanah.Posthub.BASE;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Tasks.WebQueries.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Tasks.WebQueries
{
    public interface ITaskQuery
    {
        Task<List<SubTaskResultDTO>> GetAllAsync();
        Task<PagedResult<SubTaskResultDTO>> GetByPaginationAsync(TaskFilterDTO filter);
        Task<SubTaskResultDTO> GetAsync(int id);
        Task<PagedResult<DriverTimelineResponseDTO>> GetDriverTimeLineAsync(TaskDriverFilterDTO filter);
        Task<PagedResult<SubTaskResultDTO>> GetTasksReportAsync(TaskReportFilterDTO filter);
        Task<List<TaskStatusCountResultDTO>> GetTaskStatusCountAsync(DateTime taskDate);
        Task<List<ExportTaskResponseDTO>> ExportTaskToExcelAsync(int id, string timeZone);
        Task<List<ExportTaskWithoutProgressResponseDTO>> ExportTasksWithoutProgressToExcelAsync(TaskReportFilterDTO filter);
        Task<List<ExportTaskResponseDTO>> ExportTasksWithProgressToExcelAsync(TaskReportFilterDTO filter);

    }
    public class TaskQuery : ITaskQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        readonly string baseUrl;
        public TaskQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser,
            IConfiguration configuration)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
            baseUrl = configuration.GetValue<string>("IdentityUrl");
        }

        public async Task<List<SubTaskResultDTO>> GetAllAsync()
        {
            return await ApplyInternalSubTaskFilter(context.Tasks
                .Where(task => task.TaskHistories.Any(history => !history.IsDeleted)))
                .ProjectTo<SubTaskResultDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
        }
        public async Task<PagedResult<SubTaskResultDTO>> GetByPaginationAsync(TaskFilterDTO filter)
        {
            PagedResult<SubTaskResultDTO> pagedResult = new PagedResult<SubTaskResultDTO>();
            filter.PageNumber = filter.PageNumber == 0 ? 1 : filter.PageNumber;
            filter.PageSize = filter.PageSize == 0 ? 20 : filter.PageSize;

            var query = ApplyInternalSubTaskFilter(context.Tasks
                .Where(task => task.TaskHistories.Any(history => !history.IsDeleted)));
            query = FilterAndSortTaskQuery(filter, query);

            pagedResult.TotalCount = await query.CountAsync();
            pagedResult.Result = await query
                .Skip((filter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ProjectTo<SubTaskResultDTO>(mapper.ConfigurationProvider)
                .ToListAsync();

            return pagedResult;
        }
        public async Task<SubTaskResultDTO> GetAsync(int id)
        {
            return await ApplyInternalSubTaskFilter(context.Tasks
                  .Where(task => task.TaskHistories.Any(history => !history.IsDeleted)))
                  .Where(task => task.Id == id)
                  .ProjectTo<SubTaskResultDTO>(mapper.ConfigurationProvider)
                  .FirstOrDefaultAsync();
        }
        public async Task<PagedResult<DriverTimelineResponseDTO>> GetDriverTimeLineAsync(TaskDriverFilterDTO filter)
        {
            var driver = await context.Driver
                .Include(driver => driver.User)
                .FirstOrDefaultAsync(driver => driver.Id == filter.DriverId);

            return driver == null || driver.User == null
                ? new PagedResult<DriverTimelineResponseDTO>()
                : await context.TaskHistory
                .Where(task => task.CreatedBy_Id == driver.User.UserName)
                .OrderByDescending(task => task.Id)
                .ToPagedResultAsync<TaskHistory, DriverTimelineResponseDTO>(filter, mapper.ConfigurationProvider);
        }

        //ToDo Refactor filter mapp to taskFilter & use pagination
        public async Task<PagedResult<SubTaskResultDTO>> GetTasksReportAsync(TaskReportFilterDTO filter)
        {
            var query = ApplyInternalSubTaskFilter(context.Tasks)
                .Where(x => (filter.StatusIds == null
                        || !filter.StatusIds.Any()
                        || filter.StatusIds.Contains(x.TaskStatusId))
                    && (filter.TaskTypeIds == null
                        || !filter.TaskTypeIds.Any()
                        || filter.TaskTypeIds.Contains(x.TaskTypeId))
                    && (filter.DriversIds == null
                        || !filter.DriversIds.Any()
                        || filter.DriversIds.Contains(x.DriverId ?? 0))
                    && (filter.ZonesIds == null
                        || !filter.ZonesIds.Any()
                        || filter.ZonesIds.Contains(x.GeoFenceId ?? 0))
                    && (filter.RestaurantIds == null
                        || !filter.RestaurantIds.Any()
                        || filter.RestaurantIds.Contains(x.Branch.RestaurantId.Value))
                    && (filter.BranchIds == null
                        || !filter.BranchIds.Any()
                        || filter.BranchIds.Contains(x.BranchId ?? 0)
                        || (x.BranchId == null && filter.GetCustomerTasks))
                    && (string.IsNullOrEmpty(filter.OrderId)
                        || x.OrderId.Contains(filter.OrderId))
                    && (string.IsNullOrEmpty(filter.Address)
                        || x.Address.Contains(filter.Address))
                    && (!filter.FromDate.HasValue
                        || (x.PickupDate.HasValue && x.PickupDate.Value.Date >= filter.FromDate.Value.Date)
                        || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date >= filter.FromDate.Value.Date))
                    && (!filter.ToDate.HasValue
                        || (x.PickupDate.HasValue && x.PickupDate.Value.Date <= filter.ToDate.Value.Date)
                        || (x.DeliveryDate.HasValue && x.DeliveryDate.Value.Date <= filter.ToDate.Value.Date)))
                .OrderByDescending(task => task.MainTaskId);

            var pagedResult = new PagedResult<SubTaskResultDTO>
            {
                TotalCount = await query.CountAsync(),
                Result = await query.Skip((filter.PageNumber - 1) * filter.PageSize)
                    .Take(filter.PageSize)
                    .ProjectTo<SubTaskResultDTO>(mapper.ConfigurationProvider)
                    .ToListAsync()
            };

            return pagedResult;
        }
        public async Task<List<TaskStatusCountResultDTO>> GetTaskStatusCountAsync(DateTime taskDate)
        {
            var tasks = context.Tasks
                .Where(task => (task.PickupDate.Value.Date == taskDate.Date || task.DeliveryDate.Value.Date == taskDate.Date)
                    && task.DriverId == currentUser.DriverId);

            return new List<TaskStatusCountResultDTO>
            {
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Accepted),
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Assigned),
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Cancelled),
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Declined),
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Failed),
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Inprogress),
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Started),
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Successful),
                await GetTaskStatusCountAsync(tasks, TaskStatusEnum.Unassigned)
            };
        }

        public async Task<List<ExportTaskResponseDTO>> ExportTaskToExcelAsync(int id, string timeZone)
        {
            var exportTask = await context.Tasks
                .Where(task => task.Id == id)
                .Include(task => task.TaskType)
                .Include(task => task.Customer)
                .Include(task => task.TaskStatus)
                .Include(task => task.Driver).ThenInclude(driver => driver.DriverRates)
                .Include(task => task.Driver).ThenInclude(driver => driver.User)
                .Include(task => task.TaskHistories)
                .FirstOrDefaultAsync();
            if (exportTask == null)
            {
                throw new NotFoundException(Keys.Validation.TaskNotFound);
            }
            var exportTasks = new List<ExportTaskResponseDTO>();
            if (exportTask.TaskHistories == null || !exportTask.TaskHistories.Any())
            {
                exportTasks.Add(new ExportTaskResponseDTO()
                {
                    Order_Id = exportTask.OrderId,
                    Order_Type = exportTask.TaskType.Name,
                    Order_Status = exportTask.TaskStatus.Name,
                    Name = exportTask.Customer.Name,
                    Address = exportTask.Address,
                    Order_DateTime = exportTask.PickupDate ?? exportTask.DeliveryDate,
                    Order_Description = exportTask.Description,
                    Reference_Image = !string.IsNullOrEmpty(exportTask.Image)
                        ? baseUrl + "/TaskImages/" + exportTask.Image + Environment.NewLine
                        : string.Empty,
                    Driver_Name = exportTask?.Driver?.User?.FullName
                });
            }
            else
            {
                exportTasks.AddRange(ExportTaskWithHistory(exportTask));
            }
            return exportTasks;
        }
        public async Task<List<ExportTaskWithoutProgressResponseDTO>> ExportTasksWithoutProgressToExcelAsync(TaskReportFilterDTO filter)
        {
            var exportTasks = await ApplyExportFilter(filter)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted && task.Tenant_Id == currentUser.TenantId)
                .OrderByDescending(task => task.MainTaskId)
                .ProjectTo<ExportTaskWithoutProgressResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();

            exportTasks.ForEach(task =>
            {
                task.Reference_Image = string.IsNullOrEmpty(task.Reference_Image)
                    ? string.Empty
                    : baseUrl + "/TaskImages/" + task.Reference_Image;
            });

            return exportTasks;
        }
        public async Task<List<ExportTaskResponseDTO>> ExportTasksWithProgressToExcelAsync(TaskReportFilterDTO filter)
        {
            var tasks = await ApplyExportFilter(filter)
                .OrderByDescending(task => task.MainTaskId)
                .ToListAsync();

            var exportTasks = new List<ExportTaskResponseDTO>();
            foreach (var task in tasks)
            {
                if (task.TaskHistories == null || !task.TaskHistories.Any())
                {
                    exportTasks.Add(new ExportTaskResponseDTO()
                    {
                        Order_Id = task.OrderId,
                        Order_Type = task.TaskType.Name,
                        Order_Status = task.TaskStatus.Name,
                        Name = task.Customer.Name,
                        Address = task.Address,
                        Order_DateTime = task.PickupDate ?? task.DeliveryDate,
                        Driver_Name = task?.Driver?.User?.FullName
                    });
                    continue;
                }
                exportTasks.AddRange(ExportTaskWithHistory(task));
            }

            return exportTasks;
        }


        private static IQueryable<SubTask> FilterAndSortTaskQuery(
            TaskFilterDTO taskFilter,
            IQueryable<SubTask> query)
        {
            if (taskFilter.TeamIds != null && taskFilter.TeamIds.Any())
            {
                query = query.Where(task => taskFilter.TeamIds.Contains(task.Driver.TeamId));
            }

            if (taskFilter.TaskStatusIds != null && taskFilter.TaskStatusIds.Any())
            {
                query = query.Where(task => taskFilter.TaskStatusIds.Contains(task.TaskStatusId));
            }

            if (taskFilter.FromDate.HasValue)
            {
                query = query.Where(task => (task.PickupDate.HasValue && task.PickupDate.Value.Date >= taskFilter.FromDate.Value.Date)
                || (task.DeliveryDate.HasValue && task.DeliveryDate.Value.Date >= taskFilter.FromDate.Value.Date));
            }

            if (taskFilter.ToDate.HasValue)
            {
                query = query.Where(task => (task.PickupDate.HasValue && task.PickupDate.Value.Date <= taskFilter.ToDate.Value.Date)
                || (task.DeliveryDate.HasValue && task.DeliveryDate.Value.Date <= taskFilter.ToDate.Value.Date));
            }
            if (taskFilter.BranchesIds != null && taskFilter.BranchesIds.Any())
            {
                query = query.Where(task => taskFilter.BranchesIds.Contains(task.BranchId ?? 0));
            }
            else
            {
                //  query = query.Where(x => x.BranchId == null && taskFilter.GetCustomerTasks);
            }
            if (!string.IsNullOrEmpty(taskFilter.SearchBy) && taskFilter.FilterColumn != null && taskFilter.FilterColumn.Any())
            {
                taskFilter.FilterColumn = taskFilter.FilterColumn.Select(x => x.ToLower()).ToList();
                int.TryParse(taskFilter.SearchBy, out var id);
                query = query.Where(task => (taskFilter.FilterColumn.Contains("id") && id > 0 && task.Id == id)
                || (taskFilter.FilterColumn.Contains("orderid") && task.OrderId.Contains(taskFilter.SearchBy))
                || (taskFilter.FilterColumn.Contains("drivername") && (task.Driver.User.FirstName + " " + task.Driver.User.LastName).Contains(taskFilter.SearchBy))
                || (taskFilter.FilterColumn.Contains("teamname") && task.Driver.Team.Name.Contains(taskFilter.SearchBy))
                || (taskFilter.FilterColumn.Contains("tasktypename") && task.TaskType.Name.Contains(taskFilter.SearchBy))
                || (taskFilter.FilterColumn.Contains("description") && task.Description.Contains(taskFilter.SearchBy))
                || (taskFilter.FilterColumn.Contains("address") && task.Address.Contains(taskFilter.SearchBy))
                || (taskFilter.FilterColumn.Contains("name") && task.Customer.Name.Contains(taskFilter.SearchBy))
                || (taskFilter.FilterColumn.Contains("taskstatusname") && task.TaskStatus.Name.Contains(taskFilter.SearchBy)));
            }
            var sortColumn = string.IsNullOrEmpty(taskFilter.SortColumn) ? " CreatedDate " : taskFilter.SortColumn;
            var sortOrder = string.IsNullOrEmpty(taskFilter.SortOrder) ? " desc " : taskFilter.SortOrder;
            switch (sortColumn.ToLower())
            {
                case "drivername":
                    sortColumn = "driver.user.firstname";
                    break;
                case "teamname":
                    sortColumn = "driver.team.name";
                    break;
                case "tasktypename":
                    sortColumn = "tasktype.name";
                    break;
                case "customername":
                    sortColumn = "customer.name";
                    break;
                case "taskstatusname":
                    sortColumn = "taskstatus.name";
                    break;
            }
            var orderby = sortColumn + " " + sortOrder;
            query = System.Linq.Dynamic.Core.DynamicQueryableExtensions.OrderBy(query, orderby);
            return query;
        }

        private IQueryable<SubTask> ApplyInternalSubTaskFilter(IQueryable<SubTask> tasks)
        {
            return tasks
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted
                    && task.Tenant_Id == currentUser.TenantId
                    || (currentUser.DriverId.HasValue && task.DriverId == currentUser.DriverId));
        }
        private async Task<TaskStatusCountResultDTO> GetTaskStatusCountAsync(IQueryable<SubTask> tasks,
            TaskStatusEnum taskStatus)
        {
            return new TaskStatusCountResultDTO()
            {
                TaskStatusId = (int)taskStatus,
                TaskStatusName = taskStatus.ToString(),
                Count = await tasks.CountAsync(x => x.TaskStatusId == (int)taskStatus)
            };
        }
        private IQueryable<SubTask> ApplyExportFilter(TaskReportFilterDTO filter)
        {
            var query = context.Tasks
                .Include(task => task.TaskType)
                .Include(task => task.Customer)
                .Include(task => task.TaskStatus)
                .Include(task => task.Driver).ThenInclude(driver => driver.User)
                .Include(task => task.MainTask)
                .Include(task => task.TaskHistories).ThenInclude(taskHistory => taskHistory.FromStatus)
                .Include(task => task.TaskHistories).ThenInclude(taskHistory => taskHistory.ToStatus)
                .Include(task => task.Branch)
                .Include(task => task.TaskGallaries)
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted && task.Tenant_Id == currentUser.TenantId)
                .AsQueryable();
            if (filter.StatusIds != null && filter.StatusIds.Any())
            {
                query = query.Where(task => filter.StatusIds.Contains(task.TaskStatusId));
            }
            if (filter.TaskTypeIds != null && filter.TaskTypeIds.Any())
            {
                query = query.Where(task => filter.TaskTypeIds.Contains(task.TaskTypeId));
            }
            if (filter.DriversIds != null && filter.DriversIds.Any())
            {
                query = query.Where(task => task.DriverId.HasValue && filter.DriversIds.Contains(task.DriverId.Value));
            }
            if (filter.ZonesIds != null && filter.ZonesIds.Any())
            {
                query = query.Where(task => task.GeoFenceId.HasValue && filter.ZonesIds.Contains(task.GeoFenceId.Value));
            }
            if (filter.RestaurantIds != null && filter.RestaurantIds.Any())
            {
                query = query.Where(task => task.Branch != null && filter.RestaurantIds.Contains(task.Branch.RestaurantId.Value));
            }
            if (filter.BranchIds != null && filter.BranchIds.Any())
            {
                query = query.Where(task => task.BranchId.HasValue && filter.BranchIds.Contains(task.BranchId.Value));
            }
            if (!string.IsNullOrEmpty(filter.OrderId))
            {
                query = query.Where(task => task.OrderId.Contains(filter.OrderId));
            }
            if (!string.IsNullOrEmpty(filter.Address))
            {
                query = query.Where(task => task.Address.Contains(filter.Address));
            }
            if (filter.FromDate.HasValue)
            {
                query = query.Where(task => (task.PickupDate.HasValue && task.PickupDate.Value.Date >= filter.FromDate.Value.Date)
                || (task.DeliveryDate.HasValue && task.DeliveryDate.Value.Date >= filter.FromDate.Value.Date));
            }
            if (filter.ToDate.HasValue)
            {
                query = query.Where(task => (task.PickupDate.HasValue && task.PickupDate.Value.Date <= filter.ToDate.Value.Date)
                         || (task.DeliveryDate.HasValue && task.DeliveryDate.Value.Date <= filter.ToDate.Value.Date));
            }

            return query;
        }
        private List<ExportTaskResponseDTO> ExportTaskWithHistory(SubTask task, TimeZoneInfo currentTimeZone = null)
        {
            var result = new List<ExportTaskResponseDTO>();
            foreach (var history in task.TaskHistories)
            {
                string attachment = string.Empty;
                if (history.ActionName == "ADDED A SIGNATURE " && !string.IsNullOrEmpty(task.SignatureURL))
                {
                    attachment += baseUrl + "/TaskSignatures/" + task.SignatureURL + Environment.NewLine;
                }
                if (history.ActionName == "ADDED THIS IMAGE ")
                {
                    foreach (var gallery in task.TaskGallaries)
                    {
                        attachment += baseUrl + "/TasksGallary/" + gallery.FileName + Environment.NewLine;
                    }
                }
                if (history.ActionName.Trim() == "ADDED THIS NOTE")
                {
                    attachment = task.Notes;
                }
                result.Add(new ExportTaskResponseDTO()
                {
                    Order_Id = task.OrderId,
                    Order_Type = task.TaskType.Name,
                    Order_Status = task.TaskStatus.Name,
                    Name = task.Customer.Name,
                    Address = task.Address,
                    Order_DateTime = task.PickupDate ?? task.DeliveryDate,
                    Progress_Date = history.CreatedDate,
                    Progress_Time = history.CreatedDate.ToShortTimeString(),
                    Reason = history.ActionName,
                    Attachments = attachment,
                    Order_Description = task.Description,
                    Reference_Image = !string.IsNullOrEmpty(task.Image)
                        ? baseUrl + "/TaskImages/" + task.Image + Environment.NewLine
                        : string.Empty,
                    Driver_Name = task?.Driver?.User?.FullName
                });
            }
            return result;
        }
    }
}
