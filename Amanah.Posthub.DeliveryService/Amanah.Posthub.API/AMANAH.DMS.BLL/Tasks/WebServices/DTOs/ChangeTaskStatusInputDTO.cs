﻿namespace Amanah.Posthub.BLL.Tasks.WebServices.DTOs
{
    public class ChangeTaskStatusInputDTO
    {
        public int Id { set; get; }
        public int StatusId { set; get; }
        public string Reason { set; get; }
        public bool IsChangeConnectedTasks { set; get; }
    }
}
