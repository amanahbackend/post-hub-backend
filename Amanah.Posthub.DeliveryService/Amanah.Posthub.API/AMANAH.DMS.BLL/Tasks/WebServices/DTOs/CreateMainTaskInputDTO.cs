﻿using Amanah.Posthub.BLL.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Amanah.Posthub.BLL.Tasks.WebServices.DTOs
{
    public class CreatedMainTaskInputDTO
    {
        public CreateMainTaskInputDTO MainTask { get; set; }
    }

    public class CreateMainTaskInputDTO
    {
        public int Id { set; get; }
        public int MainTaskTypeId { set; get; }
        //public string MainTaskTypeName { set; get; }
        //public bool? IsCompleted { set; get; }
        //public bool? IsDelayed { set; get; }
        public int AssignmentType { get; set; }
        //public int? RemainingTimeInSeconds { set; get; }
        public DateTime? ExpirationDate { get; set; }
        public bool IsNewlyAssigned { get; set; }
        //public bool IsFailToAutoAssignDriver { get; set; }
        //public int OrderWeight => 1;
        public int? MainTaskStatusId { set; get; }
        //public string MainTaskStatusName { set; get; }

        public List<CreateSubTaskInputDTO> Tasks { set; get; }
        public TaskSettingInputDTO Settings { get; set; }
        public class MainTaskInputDTOValidator : AbstractValidator<CreateMainTaskInputDTO>
        {
            public MainTaskInputDTOValidator()
            {
                RuleFor(x => x.MainTaskTypeId)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Tasks)
                    .NotNull()
                    .NotEmpty();

            }
        }
    }

    public class CreateSubTaskInputDTO : TasksBaseInfoViewModel
    {
        public int MainTaskId { set; get; }
        public int TaskTypeId { set; get; }
        // public string TaskTypeName { set; get; }
        public int? DriverId { set; get; }
        //public string DriverName { set; get; }
        //public string DriverPhoneNumber { set; get; }
        //public string DriverImageUrl { set; get; }
        //public string TeamName { set; get; }
        public string Description { set; get; } = string.Empty;
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? BranchId { get; set; }
        //public string BranchName { get; set; }
        public int? GeoFenceId { get; set; }
        //public bool? IsTaskReached { get; set; }
        //public DateTime? ReachedTime { get; set; }
        //public TimeInputDTO TotalWaitingTime { get; set; }
        //public TimeInputDTO TotalEstimationTime { get; set; }

        public CreateCustomerInputDTO Customer { set; get; }


        //public string Notes { set; get; }
        //public string SignatureFileName { set; get; }
        //public string SignatureURL { set; get; }


        public List<CreateTaskGallaryInputDTO> TaskGallaries { set; get; }

        public class CreateSubTaskInputDTOValidator : AbstractValidator<CreateSubTaskInputDTO>
        {
            public CreateSubTaskInputDTOValidator()
            {
                RuleFor(x => x.MainTaskId)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.TaskTypeId)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Address)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Customer)
                    .NotNull();

            }
        }

    }

    public class TaskSettingInputDTO
    {
        public List<int> DriverIds { get; set; }
        public List<int> TeamIds { get; set; }
        public List<string> Tags { get; set; }
        public List<int> PickupGeoFenceIds { get; set; }
        public List<int> DeliveryGeoFenceIds { get; set; }

        public bool Auto { get; set; }
        public int DriversCount { get; set; }
        public bool RestrictGeofences { get; set; } = true;
        public int RemainingDrivers { get; set; }
        public int RetriesCount { get; set; }
        public int MaxRetriesCount { get; set; }
        public int IntervalInSeconds { get; set; }

        public bool IsTenantAllowedToUsePlatformDrivers { get; set; }
    }

    public class TimeInputDTO
    {
        public TimeSpan Time { get; set; }
        public int Day { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }
    }

    public class CreateCustomerInputDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        [Required]
        public double? Latitude { set; get; }
        [Required]
        public double? Longitude { set; get; }
        public string Tags { set; get; }
        public int? CountryId { set; get; }
        //public CountryInputDTO Country { get; set; }
        public int? BranchId { set; get; }

        public CreateAddressInputDTO Location { set; get; }
        public class CustomerInputDTOValidator : AbstractValidator<CreateCustomerInputDTO>
        {
            public CustomerInputDTOValidator()
            {
                RuleFor(x => x.Name)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(100);
                RuleFor(x => x.Email)
                    .EmailAddress()
                    .MaximumLength(250);
                RuleFor(x => x.Phone)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(50);
                RuleFor(x => x.Address)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Longitude)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Latitude)
                    .NotEmpty()
                    .NotNull();
            }
        }
    }

    public class CreateTaskGallaryInputDTO
    {
        //public int Id { set; get; }
        public int TaskId { set; get; }
        public string FileName { set; get; }
        public string FileURL { set; get; }
    }

    public class CountryInputDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Code { set; get; }
        public string Flag { set; get; }
        public string FlagUrl { set; get; }
        public string TopLevel { set; get; }
    }

    public class CreateAddressInputDTO
    {
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }
    }


}
