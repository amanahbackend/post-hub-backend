﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Tasks.WebServices.DTOs
{
    public class ReassignDriverInputDTO
    {
        public int MainTaskId { get; set; }
        public List<int> DriverIds { get; set; }
        public int TaskStatusId { get; set; }
    }
}
