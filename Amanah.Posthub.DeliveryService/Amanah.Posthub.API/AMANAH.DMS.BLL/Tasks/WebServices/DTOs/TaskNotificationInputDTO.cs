﻿namespace Amanah.Posthub.BLL.Tasks.WebServices.DTOs
{
    public class TaskNotificationInputDTO
    {
        public int Id { get; set; }
        public int? DriverId { get; set; }
    }
}
