﻿using Amanah.Posthub.BLL.ViewModels;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Amanah.Posthub.BLL.Tasks.WebServices.DTOs
{
    public class UpdateSubTaskInputDTO : TasksBaseInfoViewModel
    {
        public int MainTaskId { set; get; }
        public int TaskTypeId { set; get; }
        public int? DriverId { set; get; }
        public string Description { set; get; } = string.Empty;
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? BranchId { get; set; }
        public int? GeoFenceId { get; set; }

        public UpdateCustomerInputDTO Customer { set; get; }


        //public string Notes { set; get; }
        //public string SignatureFileName { set; get; }
        //public string SignatureURL { set; get; }


        public List<UpdateTaskGallaryInputDTO> TaskGallaries { set; get; }

        public class UpdateSubTaskInputDTOValidator : AbstractValidator<UpdateSubTaskInputDTO>
        {
            public UpdateSubTaskInputDTOValidator()
            {
                RuleFor(x => x.Id)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.MainTaskId)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.TaskTypeId)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Address)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Customer)
                    .NotNull();

            }
        }

    }

    public class UpdateCustomerInputDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        [Required]
        public double? Latitude { set; get; }
        [Required]
        public double? Longitude { set; get; }
        public string Tags { set; get; }
        public int? CountryId { set; get; }
        public int? BranchId { set; get; }
        public UpdateAddressInputDTO Location { set; get; }

        public class UpdateCustomerInputDTOValidator : AbstractValidator<UpdateCustomerInputDTO>
        {
            public UpdateCustomerInputDTOValidator()
            {
                RuleFor(x => x.Name)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(100);
                RuleFor(x => x.Email)
                    .EmailAddress()
                    .MaximumLength(250);
                RuleFor(x => x.Phone)
                    .NotEmpty()
                    .NotNull()
                    .MaximumLength(50);
                RuleFor(x => x.Address)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Longitude)
                    .NotEmpty()
                    .NotNull();
                RuleFor(x => x.Latitude)
                    .NotEmpty()
                    .NotNull();
            }
        }
    }

    public class UpdateTaskGallaryInputDTO
    {
        public int Id { set; get; }
        public int TaskId { set; get; }
        public string FileName { set; get; }
        public string FileURL { set; get; }
    }

    public class UpdateAddressInputDTO
    {
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }
    }

}
