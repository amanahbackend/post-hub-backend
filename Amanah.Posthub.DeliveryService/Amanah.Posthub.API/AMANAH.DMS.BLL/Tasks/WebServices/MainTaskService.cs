﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Managers.TaskAssignment;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.Tasks.WebServices.DTOs;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.GeoFences.Repositories;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MapsUtilities;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Tasks.WebServices
{
    public interface IMainTaskService
    {
        Task<int?> CreateAsync(CreatedMainTaskInputDTO createMainTask);
        Task ReassignDriverAsync(ReassignDriverInputDTO reassignDriver);
        Task DeleteAsync(int id);
        Task<bool> TryAutoAssignmentAgainAsync(int id);

    }

    public class MainTaskService : IMainTaskService
    {
        private readonly ITaskCommonService taskCommonService;
        private readonly IMainTaskRepository mainTaskRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IRepository<TaskHistory> taskHistoryRepository;
        private readonly IUserRepository userRepository;
        private readonly IDriverRepository driverRepository;
        private readonly IGeofenceRepository geofenceRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unityOfWork;
        private readonly ICurrentUser currentUser;
        private readonly IUploadFormFileService uploadFormFileService;
        private readonly ILocalizer localizer;
        private readonly IBackGroundNotificationSercvice notificationService;
        private readonly ITaskAutoAssignmentFactory taskAutoAssignmentFactory;

        private string CurrentNotificationSenderName { get; set; }

        public MainTaskService(
            ITaskCommonService taskCommonService,
            IMainTaskRepository mainTaskRepository,
            IRepository<AccountLogs> accountLogRepository,
            IRepository<TaskHistory> taskHistoryRepository,
            IUserRepository userRepository,
            IDriverRepository driverRepository,
            IGeofenceRepository geofenceRepository,
            IUnitOfWork unityOfWork,
            IMapper mapper,
            ICurrentUser currentUser,
            IUploadFormFileService uploadFormFileService,
            ILocalizer localizer,
            IBackGroundNotificationSercvice notificationService,
            // IBackgroundJobManager backgroundJobManager,      //ToDo add all notifications in backgroundjob
            ITaskAutoAssignmentFactory taskAutoAssignmentFactory)
        {
            this.taskCommonService = taskCommonService;
            this.mainTaskRepository = mainTaskRepository;
            this.accountLogRepository = accountLogRepository;
            this.taskHistoryRepository = taskHistoryRepository;
            this.userRepository = userRepository;
            this.driverRepository = driverRepository;
            this.geofenceRepository = geofenceRepository;
            this.mapper = mapper;
            this.unityOfWork = unityOfWork;
            this.currentUser = currentUser;
            this.uploadFormFileService = uploadFormFileService;
            this.localizer = localizer;
            this.notificationService = notificationService;
            // this.backgroundJobManager = backgroundJobManager;
            this.taskAutoAssignmentFactory = taskAutoAssignmentFactory;
            CurrentNotificationSenderName = "D-Hub";//TODO: replace with Config
        }

        public async Task<int?> CreateAsync(CreatedMainTaskInputDTO createMainTask)
        {
            MainTask createdMainTask = null;
            await unityOfWork.RunTransaction(async () =>
            {
                createdMainTask = await CreateAsync(createMainTask.MainTask);
            });
            await AutoAssignAsync(createMainTask, createdMainTask);

            return createdMainTask?.Id;
        }
        public async Task ReassignDriverAsync(ReassignDriverInputDTO reassignDriver)
        {
            var mainTask = await mainTaskRepository.GetWithTasksAsync(mainTask => mainTask.Id == reassignDriver.MainTaskId
                && (!mainTask.IsCompleted.HasValue || !mainTask.IsCompleted.Value));
            if (mainTask == null)
            {
                return;
            }
            var tasks = mainTask.Tasks
                .Where(task => task.TaskStatusId != (int)TaskStatusEnum.Successful)
                .ToList();
            if (!tasks.Any())
            {
                return;
            }
            var oldDriver = tasks?.FirstOrDefault().Driver;
            var newDriverId = reassignDriver.DriverIds.FirstOrDefault();
            if (oldDriver != null && oldDriver.Id == newDriverId)
            {
                return;
            }
            var newDriver = await driverRepository.GetFirstOrDefaultAsync(driver => driver.Id == newDriverId);
            await unityOfWork.RunTransaction(async () =>
            {
                foreach (var task in tasks)
                {
                    task.TaskStatusId = (int)TaskStatusEnum.Accepted;
                    task.DriverId = newDriverId;
                    task.ReachedTime = null;
                    task.IsTaskReached = false;

                    taskHistoryRepository.Add(new TaskHistory(currentUser.TenantId)
                    {
                        TaskId = task.Id,
                        MainTaskId = task.MainTaskId,
                        ActionName = "ASSIGNED",
                        ToStatusId = (int)TaskStatusEnum.Assigned
                    });
                    await taskCommonService.CreateDriverTaskNotificationAsync(task, localizer[Keys.Notifications.NewTasksAssignedToYou]);
                }

                if (mainTask.Tasks.Where(task => task.TaskStatusId == (int)TaskStatusEnum.Successful).Any())
                {
                    mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Inprogress;
                }
                else
                {
                    mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Accepted;
                }
                mainTaskRepository.Update(mainTask);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "MainTasks",
                    ActivityType = "Reasign Driver",
                    Description = $"Main Task ({mainTask.Id}) has been reasign {(oldDriver != null ? $"from driver({oldDriver.Id})" : "")} To driver ({newDriver.Id})",
                    Record_Id = mainTask.Id
                });

                await unityOfWork.SaveChangesAsync();

                if (oldDriver != null && oldDriver.AgentStatusId == (int)AgentStatusesEnum.Busy)
                {
                    await taskCommonService.ChangeDriverAvailabilityAsync(oldDriver);
                    await unityOfWork.SaveChangesAsync();
                }

            });
            if (oldDriver != null)
            {
                await ReassignOldDriverNotificationAsync(tasks, oldDriver);
            }
            await ReassignNewDriverNotificationAsync(newDriver);
        }
        public async Task DeleteAsync(int id)
        {
            var mainTask = await mainTaskRepository.GetWithTasksAsync(mainTask => mainTask.Id == id);
            await unityOfWork.RunTransaction(async () =>
            {
                mainTask.SetIsDeleted(true);
                var tasks = mainTask?.Tasks
                    .Where(task => !task.IsDeleted)
                    .ToList();
                foreach (var task in tasks)
                {
                    task.SetIsDeleted(true);
                    task?.TaskHistories
                    .Where(history => !history.IsDeleted)
                    .ToList()
                    .ForEach(history =>
                    {
                        history.SetIsDeleted(true);
                    });

                    if (task.DriverId.HasValue)
                    {
                        await taskCommonService.CreateDriverTaskNotificationAsync(task, localizer.Format(Keys.Notifications.TaskNoHasBeenDeletedFromYou, task.Id));
                    }
                };
                mainTaskRepository.Update(mainTask);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "MainTasks",
                    ActivityType = "Delete",
                    Description = $"Main Task ({id}) has been deleted ",
                    Record_Id = id
                });

                //TODO: Need check Driver availability 
                // await _tasksManager.MakeDriverAvailable(taskToDelete.DriverId ?? 0);

                await unityOfWork.SaveChangesAsync();
                await DeleteTaskNotificationAsync(mainTask);

            });
        }

        public async Task<bool> TryAutoAssignmentAgainAsync(int id)
        {
            var autoAssignMainTask = await mainTaskRepository.GetAllIQueryable().ProjectTo<MainTaskAutoAssignInputDTO>(mapper.ConfigurationProvider).FirstOrDefaultAsync(x => x.Id == id);

            autoAssignMainTask.Settings = new TaskSettingInputDTO
            {
                DriversCount = 1,
                RetriesCount = 1,
                Auto = true
            };


            autoAssignMainTask.Settings.IsTenantAllowedToUsePlatformDrivers =
            await userRepository.CheckIfTenantHaveThePermissionAsync(TenantPermissions.Agent.CreateAgent);

            var taskAutoAssignment = await taskAutoAssignmentFactory.CreateTaskAutoAssignmentAsync();
            //ToDo need to be handled
            await taskAutoAssignment.AssignDriverToTaskAsync(autoAssignMainTask);

            return false;
        }


        private async Task<MainTask> CreateAsync(CreateMainTaskInputDTO createMainTask)
        {
            if (createMainTask.Settings == null || !createMainTask.Settings.Auto)
            {
                createMainTask.AssignmentType = (int)TaskAssignmentType.Manual;
            }

            var driverId = createMainTask.Settings?.DriverIds
                ?.FirstOrDefault();
            var branchId = createMainTask.Tasks
               .FirstOrDefault(task => task.TaskTypeId == (int)TaskTypesEnum.Pickup)
               ?.BranchId;

            var tasksWithUploadedFiles = createMainTask.Tasks
                .Where(task => task.FormImage != null);
            foreach (var task in tasksWithUploadedFiles)
            {
                var processResult = await uploadFormFileService.UploadFileAsync(task.FormImage, "TasksImages");
                if (processResult.IsSucceeded)
                {
                    task.Image = processResult.ReturnData;
                }
            }

            var mainTask = mapper.Map<MainTask>(createMainTask);
            int deliveryNo = 1;
            foreach (var task in mainTask.Tasks)
            {
                task.TaskStatusId = driverId.HasValue
                   ? (int)TaskStatusEnum.Accepted
                   : (int)TaskStatusEnum.Unassigned;
                task.DriverId = driverId;
                task.CustomerId = await taskCommonService.CreateCustomerAsync(task);
                if (task.TaskTypeId == (int)TaskTypesEnum.Delivery)
                {
                    if (deliveryNo == 1)
                    {
                        task.GeoFenceId = (await GetMatchingGeoFenceAsync(new MapPoint
                        {
                            Latitude = task.Latitude.Value,
                            Longitude = task.Longitude.Value
                        }))?.Id;
                        deliveryNo++;
                    }
                }
                task.Customer = null;
            };
            if (driverId.HasValue)
            {
                mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Accepted;
            }

            mainTaskRepository.Add(mainTask);
            await unityOfWork.SaveChangesAsync();

            foreach (var task in mainTask.Tasks)
            {
                taskHistoryRepository.Add(new TaskHistory(currentUser.TenantId)
                {
                    TaskId = task.Id,
                    MainTaskId = mainTask.Id,
                    ActionName = "CREATED",
                });
                if (task.DriverId.HasValue)
                {
                    taskHistoryRepository.Add(new TaskHistory(currentUser.TenantId)
                    {
                        TaskId = task.Id,
                        MainTaskId = mainTask.Id,
                        ActionName = "ASSIGNED",
                        ToStatusId = (int)TaskStatusEnum.Assigned,
                    });
                    await taskCommonService.CreateDriverTaskNotificationAsync(task, localizer[Keys.Notifications.NewTasksAssignedToYou]);
                }
            }
            accountLogRepository.Add(new AccountLogs()
            {
                TableName = "MainTask",
                ActivityType = "Create",
                Description = "Main Task ( " + mainTask.Id + " )  has been created ",
                Record_Id = mainTask.Id
            });
            await unityOfWork.SaveChangesAsync();

            if (driverId.HasValue)
            {
                var driver = await driverRepository.GetFirstOrDefaultAsync(driver => driver.Id == driverId);
                await CreateTaskNotificationAsync(driver.UserId);
            }

            return mainTask;
        }
        private async Task AutoAssignAsync(CreatedMainTaskInputDTO createMainTask, MainTask createdMainTask)
        {
            if (createMainTask.MainTask.Settings != null
                            && createMainTask.MainTask.Settings.Auto)
            {
                createMainTask.MainTask.Settings ??= new TaskSettingInputDTO();
                if (createMainTask.MainTask.Settings.DriversCount == 0)
                {
                    createMainTask.MainTask.Settings.DriversCount = 1;
                }
                createMainTask.MainTask.Settings.IsTenantAllowedToUsePlatformDrivers =
                    await userRepository.CheckIfTenantHaveThePermissionAsync(TenantPermissions.Agent.CreateAgent);

                var taskAutoAssignment = await taskAutoAssignmentFactory.CreateTaskAutoAssignmentAsync();
                notificationService.SendToUserAndTenantAdmin(
                   currentUser.Id, "AutoAllocationStarted",
                   localizer[Keys.Notifications.TaskCreateAndAutoAllocationStartedTitle],
                   localizer[Keys.Notifications.TaskCreateAndAutoAllocationStartedBody],
                   createdMainTask.Id);

                var mainTaskAutoAssign = mapper.Map<MainTaskAutoAssignInputDTO>(createdMainTask);
                mainTaskAutoAssign.Settings = createMainTask.MainTask.Settings;
                await taskAutoAssignment.AssignDriverToTaskAsync(mainTaskAutoAssign);
            }
        }

        private async Task<GeoFence> GetMatchingGeoFenceAsync(MapPoint driverCurrentPoint)
        {
            var geofences = await geofenceRepository.GetWithGeofencLocationAsync(geofence => true);
            foreach (var geoFence in geofences)
            {
                var zone = geoFence.GeoFenceLocations
                    .Select(geo => new MapPoint { Latitude = geo.Latitude.Value, Longitude = geo.Longitude.Value })
                    .Select(geo => new MapPoint { Latitude = geo.Latitude, Longitude = geo.Longitude })
                    .ToArray();
                if (MapsHelper.IsPointInPolygon(zone, driverCurrentPoint))
                {
                    return geoFence;
                }
            }

            return null;
        }


        private async Task ReassignOldDriverNotificationAsync(List<SubTask> tasks, Driver oldDriver)
        {
            var message = string.Format(localizer[Keys.Notifications.TaskNoHasBeenDeletedFromYou], tasks.FirstOrDefault().Id);
            notificationService.SendToUser(
               oldDriver.UserId,
               message,
               CurrentNotificationSenderName,
               message,
               new { notificationType = (int)NotificationTypeEnum.ReassignTask });
        }
        private async Task ReassignNewDriverNotificationAsync(Driver newDriver)
        {
            notificationService.SendToUser(
                   newDriver.UserId,
                   localizer[Keys.Notifications.NewTasksAssignedToYou],
                   CurrentNotificationSenderName,
                   localizer[Keys.Notifications.NewTasksAssignedToYou],
                   new { notificationType = (int)NotificationTypeEnum.NewTask });
        }
        private async Task DeleteTaskNotificationAsync(MainTask mainTask)
        {
            var completedTaskList = new int[]
                  {
                    (int)TaskStatusEnum.Successful ,
                    (int)TaskStatusEnum.Failed,
                    (int)TaskStatusEnum.Declined,
                    (int)TaskStatusEnum.Cancelled
                  };
            var deletedTasks = mainTask?.Tasks
                .Where(task => !task.IsDeleted
                    && !completedTaskList.Contains(task.TaskStatusId)
                    && task.DriverId.HasValue)
                .ToList();

            foreach (var deletedTask in deletedTasks)
            {
                var message = localizer.Format(Keys.Notifications.TaskNoHasBeenDeletedFromYou, deletedTask.Id);
                notificationService.SendToUser(
                      deletedTask.Driver.UserId,
                      message,
                      CurrentNotificationSenderName,
                      message,
                      new { notificationType = NotificationTypeEnum.NewTask });
            }
        }
        private async Task CreateTaskNotificationAsync(string userId)
        {
            notificationService.SendToUser(
               userId,
               localizer[Keys.Notifications.NewTasksAssignedToYou],
               CurrentNotificationSenderName,
               localizer[Keys.Notifications.NewTasksAssignedToYou],
              new { notificationType = (int)NotificationTypeEnum.NewTask });
        }
    }
}
