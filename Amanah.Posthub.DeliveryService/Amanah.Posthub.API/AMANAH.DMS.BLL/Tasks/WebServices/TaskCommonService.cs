﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Domain.SubTasks.Repositories;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Amanah.Posthub.SharedKernel.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Tasks.WebServices
{
    public interface ITaskCommonService
    {
        Task<int> CreateCustomerAsync(SubTask task);
        Task ChangeDriverAvailabilityAsync(Driver driver);
        Task ChangeMainTaskStatusAsync(int mainTaskId);
        Task CreateDriverTaskNotificationAsync(SubTask subTask, string message);
        Task<bool> CheckIfDriverHasOnGoingTasksAsync(int driverId);
        Task<int> CountDriverNewMainTasksAsync(int driverId);


        Task<MainTask> UpdateMainTaskStatusAsync(int mainTaskId, TaskStatusEnum? currentSubtaskNewSatatus = null, int? currentSubtaskId = null);
        MainTask UpdateMainTaskStatus(MainTask dbMainTask, TaskStatusEnum? currentSubtaskNewSatatus = null, int? currentSubtaskId = null);
    }

    public class TaskCommonService : ITaskCommonService
    {
        private readonly ISubTaskRepository subTaskRepository;
        private readonly IMainTaskRepository mainTaskRepository;
        private readonly IRepository<Customer> customerRepository;
        private readonly IRepository<Branch> branchRepository;
        private readonly IDriverRepository driverRepository;
        private readonly IRepository<Service.Domain.Tasks.Entities.TaskStatus> taskStatusRepository;
        private readonly IRepository<DriverTaskNotification> driverTaskNotificationRepository;
        private readonly IUnitOfWork unityOfWork;
        private readonly ICurrentUser currentUser;

        readonly List<int> completedSubTaskStatusIds = new List<int>()
        {
            (int)TaskStatusEnum.Successful ,
            (int)TaskStatusEnum.Failed ,
            (int)TaskStatusEnum.Cancelled
        };
        readonly List<int> ongoingTaskStatues = new List<int>
        {
            (int)TaskStatusEnum.Inprogress,
            (int)TaskStatusEnum.Started
        };


        readonly List<int> notFinishedStatus = new List<int>()
            {(int)TaskStatusEnum.Unassigned,
                (int)TaskStatusEnum.Assigned,
                (int)TaskStatusEnum.Accepted,
                (int)TaskStatusEnum.Started,
                (int)TaskStatusEnum.Inprogress,
                (int)TaskStatusEnum.Declined
            };




        public TaskCommonService(
                ICurrentUser currentUser,
                ISubTaskRepository subTaskRepository,
                IMainTaskRepository mainTaskRepository,
                IRepository<Customer> customerRepository,
                IRepository<Branch> branchRepository,
                IDriverRepository driverRepository,
                IRepository<Service.Domain.Tasks.Entities.TaskStatus> taskStatusRepository,
                IRepository<DriverTaskNotification> driverTaskNotificationRepository,
                IUnitOfWork unityOfWork)
        {
            this.subTaskRepository = subTaskRepository;
            this.mainTaskRepository = mainTaskRepository;
            this.customerRepository = customerRepository;
            this.branchRepository = branchRepository;
            this.driverRepository = driverRepository;
            this.taskStatusRepository = taskStatusRepository;
            this.driverTaskNotificationRepository = driverTaskNotificationRepository;
            this.unityOfWork = unityOfWork;
            this.currentUser = currentUser;
        }

        public async Task<int> CreateCustomerAsync(SubTask task)
        {
            Customer customer;

            if (task.BranchId.HasValue)
            {
                customer = await customerRepository.GetFirstOrDefaultAsync(customer => customer.BranchId == task.BranchId);
            }
            else
            {
                customer = await customerRepository.GetFirstOrDefaultAsync(customerEntity => customerEntity.Phone == task.Customer.Phone
                    || customerEntity.Id == task.Customer.Id);
            }

            if (customer == null)
            {
                if (task.BranchId.HasValue)
                {
                    Branch branch = await branchRepository.GetFirstOrDefaultAsync(branch => branch.Id == task.BranchId);
                    customer = new Customer()
                    {
                        Name = branch.Name,
                        Address = branch.Address,
                        CountryId = branch.CountryId,
                        Latitude = Convert.ToDouble(branch.Latitude),
                        Longitude = Convert.ToDouble(branch.Longitude),
                        Email = branch.Name.Trim().ToLower().RemoveSpecialCharacters() + "_" + branch.Id + "@dhub.com",
                        BranchId = task.BranchId,
                        Phone = branch.Phone
                    };
                    customerRepository.Add(customer);
                }
                else
                {
                    customer = task.Customer;
                    customerRepository.Add(customer);
                }
            }
            else
            {
                customer.Address = task.Customer.Address;
                customer.Latitude = task.Customer.Latitude;
                customer.Longitude = task.Customer.Longitude;
                customer.Name = task.Customer.Name;
                customerRepository.Update(customer);
            }

            await unityOfWork.SaveChangesAsync();
            return customer.Id;
        }
        public async Task ChangeDriverAvailabilityAsync(Driver driver)
        {
            bool hasOngoingTask = await CheckIfDriverHasOnGoingTasksAsync(driver.Id);

            if (driver.AgentStatusId == (int)AgentStatusesEnum.Available && hasOngoingTask)
            {
                driver.AgentStatusId = (int)AgentStatusesEnum.Busy;
            }
            else if (driver.AgentStatusId == (int)AgentStatusesEnum.Busy && !hasOngoingTask)
            {
                driver.AgentStatusId = (int)AgentStatusesEnum.Available;
            }
            driverRepository.Update(driver);
        }
        public async Task ChangeMainTaskStatusAsync(int mainTaskId)
        {
            var mainTask = await mainTaskRepository.GetWithTasksAsync(mainTask => mainTask.Id == mainTaskId);
            if (mainTask != null)
            {
                var isAllSubTasksCompleted = mainTask.Tasks
                    .All(subTask => completedSubTaskStatusIds.Contains(subTask.TaskStatusId) && !subTask.IsDeleted);
                if (isAllSubTasksCompleted)
                {
                    mainTask.IsCompleted = true;

                    var containCancelledSubTasks = mainTask.Tasks
                        .Any(subTask => subTask.TaskStatusId == (int)TaskStatusEnum.Cancelled && !subTask.IsDeleted);
                    if (containCancelledSubTasks)
                    {
                        mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Canceled;
                    }
                    else
                    {
                        var containFailedSubTasks = mainTask.Tasks
                        .Any(subTask => subTask.TaskStatusId == (int)TaskStatusEnum.Failed && !subTask.IsDeleted);
                        if (containFailedSubTasks)
                        {
                            mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Failed;
                        }
                        else
                        {
                            mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Successful;
                        }
                    }
                }
                else
                {
                    mainTask.IsCompleted = false;

                    var isAllSubTasksAccepted = mainTask.Tasks
                        .All(subTask => subTask.TaskStatusId == (int)TaskStatusEnum.Accepted && !subTask.IsDeleted);
                    if (isAllSubTasksAccepted)
                    {
                        mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Accepted;
                    }
                    else
                    {
                        var isAllSubTasksAssigned = mainTask.Tasks
                            .All(subTask => subTask.TaskStatusId == (int)TaskStatusEnum.Assigned && !subTask.IsDeleted);
                        if (isAllSubTasksAssigned)
                        {
                            mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Assigned;
                        }
                        else
                        {
                            mainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Inprogress;
                        }
                    }
                }
                mainTaskRepository.Update(mainTask);
            }
        }
        public async Task<bool> CheckIfDriverHasOnGoingTasksAsync(int driverId)
        {
            return await subTaskRepository.CheckTaskAsync(subTask => subTask.DriverId == driverId
               && ongoingTaskStatues.Contains(subTask.TaskStatusId), true);
        }
        public async Task<int> CountDriverNewMainTasksAsync(int driverId)
        {
            var bufferTime = TimeSpan.FromSeconds(5);
            var now = DateTime.UtcNow;
            var afterBufferTime = now + bufferTime;
            return await mainTaskRepository.GetAllIQueryable(mainTask => mainTask.Tasks.Any(task => task.DriverId == driverId &&
                    task.TaskStatusId == (int)TaskStatusEnum.Assigned) &&
                    (!mainTask.ExpirationDate.HasValue || mainTask.ExpirationDate.Value >= afterBufferTime))
                .Include(mainTask => mainTask.Tasks).CountAsync();
        }

        //TODo refactor: depending on literals (stringly typed code) need to be strongly typed instead
        public async Task CreateDriverTaskNotificationAsync(SubTask subTask, string message)
        {
            if (subTask.DriverId != null && subTask.DriverId > 0)
            {
                var driverTaskNotification = new DriverTaskNotification
                {
                    TaskId = subTask.Id,
                    DriverId = subTask.DriverId ?? 0,
                    Description = "Task number " + subTask.Id + " " + message,
                };

                if (message.ToLower().Contains("update"))
                {
                    driverTaskNotification.ActionStatus = "Updated";
                }
                else if (message.ToLower().Contains("delete"))
                {
                    driverTaskNotification.ActionStatus = "Deleted";
                }
                else if (message.ToLower().Contains("new task") || message.ToLower().Contains("assign"))
                {
                    driverTaskNotification.ActionStatus = "Assigned";
                }
                else
                {
                    var TaskStatus = await taskStatusRepository.GetFirstOrDefaultAsync(taskStatus => taskStatus.Id == subTask.TaskStatusId);
                    if (TaskStatus != null)
                    {
                        driverTaskNotification.ActionStatus = TaskStatus.Name;
                    }
                }
                driverTaskNotificationRepository.Add(driverTaskNotification);
            }
        }


        public async Task<MainTask> UpdateMainTaskStatusAsync(int mainTaskId,
           TaskStatusEnum? currentSubtaskNewSatatus = null,
           int? currentSubtaskId = null)
        {
            var dbMainTask = await mainTaskRepository.GetAllIQueryable()
                .IgnoreQueryFilters()
                .Where(task => !task.IsDeleted)
                .Where(task => task.Tenant_Id == currentUser.TenantId ||
                    (currentUser.DriverId.HasValue && task.Tasks.Any(subTask => subTask.DriverId == currentUser.DriverId) &&
                    string.IsNullOrEmpty(currentUser.TenantId)))
                .FirstOrDefaultAsync(x => x.Id == mainTaskId);
            return UpdateMainTaskStatus(dbMainTask, currentSubtaskNewSatatus, currentSubtaskId);
        }

        public MainTask UpdateMainTaskStatus(MainTask dbMainTask,
            TaskStatusEnum? currentSubtaskNewSatatus,
            int? currentSubtaskId = null)
        {
            var allTasksStatus = subTaskRepository.GetAllIQueryable().Where(x => x.MainTaskId == dbMainTask.Id && x.Id != currentSubtaskId).Select(x => x.TaskStatusId).ToList();
            if (currentSubtaskNewSatatus != null)
                allTasksStatus.Add((int)currentSubtaskNewSatatus);
            allTasksStatus = allTasksStatus.Distinct().ToList();

            TaskStatusEnum? allstatusValue = allTasksStatus.Count == 1 ? ((TaskStatusEnum?)allTasksStatus[0]) : null;
            if (allstatusValue != null && allstatusValue == TaskStatusEnum.Assigned)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Assigned;
                dbMainTask.IsCompleted = false;
            }
            var IsInprogress = allTasksStatus.Any(x => x == (int)TaskStatusEnum.Started || x == (int)TaskStatusEnum.Accepted);
            if (IsInprogress)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Inprogress;
                dbMainTask.IsCompleted = false;
            }
            if (allstatusValue != null && allstatusValue == TaskStatusEnum.Accepted)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Accepted;
                dbMainTask.IsCompleted = false;
            }
            if (allstatusValue != null && allstatusValue == TaskStatusEnum.Successful)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Successful;
                dbMainTask.IsCompleted = true;
            }
            if (allstatusValue != null && allstatusValue == TaskStatusEnum.Cancelled)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Canceled;
                dbMainTask.IsCompleted = true;
            }
            if (allstatusValue != null && allstatusValue == TaskStatusEnum.Declined)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Declined;
                dbMainTask.IsCompleted = true;
            }
            var IsFailed = allTasksStatus.Any(x => x == (int)TaskStatusEnum.Failed);
            if (IsFailed)
            {
                dbMainTask.MainTaskStatusId = (int)Enums.MainTaskStatus.Failed;
            }

            if (allstatusValue != null && completedSubTaskStatusIds.Contains((int)allstatusValue))
                dbMainTask.IsCompleted = true;
            if (dbMainTask.IsCompleted != null && !(bool)dbMainTask.IsCompleted)
            {
                bool ismainTaskHasAWorkingTask = false;
                foreach (var staticWorkingStatus in notFinishedStatus)
                {
                    ismainTaskHasAWorkingTask = allTasksStatus.Contains(staticWorkingStatus);
                    if (ismainTaskHasAWorkingTask)
                        break;
                }
                dbMainTask.IsCompleted = !ismainTaskHasAWorkingTask;
            }
            mainTaskRepository.Update(dbMainTask);
            return dbMainTask;
        }
    }
}
