﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Managers;
using Amanah.Posthub.BLL.Notifications.BackGroundJobs;
using Amanah.Posthub.BLL.Tasks.WebServices.DTOs;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Domain.SubTasks.Repositories;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MapsUtilities;
using MapsUtilities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Tasks.WebServices
{
    public interface ITaskService
    {
        Task<int?> CreateAsync(CreateSubTaskInputDTO task);
        Task<int?> UpdateAsync(UpdateSubTaskInputDTO updateSubTask);
        Task DeleteAsync(int id);
        Task ChangeStatusAsync(ChangeTaskStatusInputDTO taskStatusInputDTO);
        Task ReassignDriverAsync(ReassignDriverToSubTaskInputDTO reassignDriverToSubTask);
    }
    public class TaskService : ITaskService
    {
        private readonly ITaskCommonService taskCommonService;
        private readonly ISubTaskRepository subTaskRepository;
        private readonly IMainTaskRepository mainTaskRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IRepository<TaskHistory> taskHistoryRepository;
        private readonly IRepository<GeoFence> geoFenceRepository;
        private readonly IDriverRepository driverRepository;
        private readonly IMapper mapper;
        private readonly IUnitOfWork unityOfWork;
        private readonly ICurrentUser currentUser;
        private readonly IUploadFormFileService uploadFormFileService;
        private readonly ILocalizer localizer;
        private readonly IBackGroundNotificationSercvice notificationService;
        private readonly IBackgroundJobManager backgroundJobManager;

        private string CurrentNotificationSenderName { get; set; }

        public TaskService(
            ITaskCommonService taskCommonService,
            ISubTaskRepository subTaskRepository,
            IMainTaskRepository mainTaskRepository,
            IRepository<AccountLogs> accountLogRepository,
            IRepository<TaskHistory> taskHistoryRepository,
            IRepository<GeoFence> geoFenceRepository,
            IDriverRepository driverRepository,
            IUnitOfWork unityOfWork,
            IMapper mapper,
            ICurrentUser currentUser,
            IUploadFormFileService uploadFormFileService,
            ILocalizer localizer,
            IBackGroundNotificationSercvice notificationService,
            IBackgroundJobManager backgroundJobManager)
        {
            this.taskCommonService = taskCommonService;
            this.subTaskRepository = subTaskRepository;
            this.mainTaskRepository = mainTaskRepository;
            this.accountLogRepository = accountLogRepository;
            this.taskHistoryRepository = taskHistoryRepository;
            this.geoFenceRepository = geoFenceRepository;
            this.driverRepository = driverRepository;
            this.mapper = mapper;
            this.unityOfWork = unityOfWork;
            this.currentUser = currentUser;
            this.uploadFormFileService = uploadFormFileService;
            this.localizer = localizer;
            this.notificationService = notificationService;
            this.backgroundJobManager = backgroundJobManager;
            CurrentNotificationSenderName = "D-Hub";//TODO: replace with Config
        }

        public async Task<int?> CreateAsync(CreateSubTaskInputDTO createSubTask)
        {
            int? subTaskId = null;
            //validate mainTask not completed
            var driver = await mainTaskRepository.GetMainTaskCurrentDriverAsync(createSubTask.MainTaskId);
            var branchId = createSubTask.TaskTypeId == (int)TaskTypesEnum.Pickup
                ? createSubTask.BranchId
                : null;
            await unityOfWork.RunTransaction(async () =>
            {
                createSubTask.TaskStatusId = driver != null
                   ? (int)TaskStatusEnum.Accepted
                   : (int)TaskStatusEnum.Unassigned;
                createSubTask.DriverId = driver.Id;
                if (createSubTask.FormImage != null)
                {
                    var processResult = await uploadFormFileService.UploadFileAsync(createSubTask.FormImage, "TasksImages");
                    if (processResult.IsSucceeded)
                    {
                        createSubTask.Image = processResult.ReturnData;
                    }
                }

                var subTask = mapper.Map<SubTask>(createSubTask);
                subTask.CustomerId = await taskCommonService.CreateCustomerAsync(subTask);
                subTask.BranchId = branchId;
                subTaskRepository.Add(subTask);

                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Tasks",
                    ActivityType = "Create",
                    Description = $"Task ({subTask.Id} ) has been created ",
                    Record_Id = subTask.Id
                });

                taskHistoryRepository.Add(new TaskHistory(currentUser.TenantId)
                {
                    TaskId = subTask.Id,
                    MainTaskId = subTask.MainTaskId,
                    ActionName = "CREATED",
                });
                if (subTask.DriverId.HasValue)
                {
                    taskHistoryRepository.Add(new TaskHistory(currentUser.TenantId)
                    {
                        TaskId = subTask.Id,
                        MainTaskId = subTask.MainTaskId,
                        ActionName = "ASSIGNED",
                        ToStatusId = (int)TaskStatusEnum.Assigned,
                    });
                }
                await unityOfWork.SaveChangesAsync();

                subTaskId = subTask.Id;

                if (subTask.DriverId.HasValue)
                {
                    backgroundJobManager.Enqueue(new NotifyDriverOfAssignedTasksJobModel
                    {
                        Tasks = new List<ViewModels.TasksViewModel>
                    {
                        new ViewModels.TasksViewModel
                        {
                            Id = createSubTask.Id,
                            DriverId = createSubTask.DriverId
                        }
                    },
                        Message = localizer[Keys.Notifications.NewTasksAssignedToYou],
                        NotificationType = (int)NotificationTypeEnum.NewTask
                    });
                }
            });

            return subTaskId;
        }
        public async Task<int?> UpdateAsync(UpdateSubTaskInputDTO updateSubTask)
        {
            int? subTaskId = null;
            //TODO: validate status if success return 

            var subTask = await subTaskRepository.GetFirstOrDefaultAsync(task => task.Id == updateSubTask.Id);
            if (subTask == null)
            {
                throw new DomainException(Keys.Validation.TaskNotFound);
            }
            var subTaskDriver = await driverRepository.GetFirstOrDefaultAsync(driver => driver.Id == updateSubTask.DriverId);
            bool isMainTaskStatusChanged = (!subTask.DriverId.HasValue && updateSubTask.DriverId.HasValue)
                    || (subTask.DriverId.HasValue && !updateSubTask.DriverId.HasValue);

            await unityOfWork.RunTransaction(async () =>
            {
                if (updateSubTask.FormImage != null)
                {
                    var processResult = await uploadFormFileService.UploadFileAsync(updateSubTask.FormImage, "TasksImages");
                    if (processResult.IsSucceeded)
                    {
                        updateSubTask.Image = processResult.ReturnData;
                    }
                }

                subTask = mapper.Map(updateSubTask, subTask);
                subTask.CustomerId = await taskCommonService.CreateCustomerAsync(subTask);
                subTask.GeoFenceId = await GetTaskMatchingGeoFenceAsync(subTask);
                subTaskRepository.Update(subTask);
                taskHistoryRepository.Add(new TaskHistory(currentUser.TenantId)
                {
                    TaskId = subTask.Id,
                    MainTaskId = subTask.MainTaskId,
                    ActionName = "UPDATED",
                });
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Tasks",
                    ActivityType = "Update",
                    Description = $"Task ({subTask.Id}) has been updated ",
                    Record_Id = subTask.Id
                });
                if (subTask.DriverId.HasValue)
                {
                    await taskCommonService.CreateDriverTaskNotificationAsync(subTask, localizer.Format(Keys.Notifications.TaskNoHasBeenUpdated, subTask.Id));
                }
                await unityOfWork.SaveChangesAsync();

                if (isMainTaskStatusChanged)
                {
                    await taskCommonService.ChangeMainTaskStatusAsync(subTask.MainTaskId);
                    await unityOfWork.SaveChangesAsync();
                }

                subTaskId = subTask.Id;
            });

            if (subTask.DriverId.HasValue)
            {
                UpdateTaskNotification(subTask, subTaskDriver);
            }

            return subTaskId;
        }
        public async Task DeleteAsync(int id)
        {
            //need to validate not deleting first pickup if have deliveries not deleted
            var task = await subTaskRepository.GetFirstOrDefaultAsync(subTask => subTask.Id == id);
            if (task == null)
            {
                return;
            }
            await unityOfWork.RunTransaction(async () =>
            {
                task.SetIsDeleted(true);

                task?.TaskHistories
                .Where(history => !history.IsDeleted)
                .ToList()
                .ForEach(history =>
                {
                    history.SetIsDeleted(true);
                });

                if (task.DriverId.HasValue && task.Driver != null)
                {
                    await taskCommonService.CreateDriverTaskNotificationAsync(task, localizer.Format(Keys.Notifications.TaskNoHasBeenDeletedFromYou, task.Id));
                    await taskCommonService.ChangeDriverAvailabilityAsync(task.Driver);
                    DeleteTaskNotification(task);
                }

                subTaskRepository.Update(task);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Tasks",
                    ActivityType = "Delete",
                    Description = $"Task ({id}) has been deleted ",
                    Record_Id = id
                });

                //need to check if all subtasks deleted then delete main task
                var mainTask = await mainTaskRepository.GetWithTasksAsync(maintask => maintask.Id == task.MainTaskId);
                if (mainTask != null && (mainTask.Tasks.All(task => task.IsDeleted) || !mainTask.Tasks.Any()))
                {
                    mainTask.SetIsDeleted(true);
                    mainTaskRepository.Update(mainTask);
                }

                await unityOfWork.SaveChangesAsync();
            });
        }
        public async Task ChangeStatusAsync(ChangeTaskStatusInputDTO taskStatusInputDTO)
        {
            string message = string.Empty;
            var tasks = new List<SubTask>();
            if (taskStatusInputDTO.IsChangeConnectedTasks)
            {
                var mainTask = await mainTaskRepository.GetWithTasksAsync(maintask =>
                     maintask.Tasks.Where(task => task.Id == taskStatusInputDTO.Id
                     && task.TaskStatusId != (int)TaskStatusEnum.Successful).Any());
                tasks.AddRange(mainTask?.Tasks);
            }
            else
            {
                var task = await subTaskRepository.GetAllIQueryable().Include(x => x.MainTask).FirstOrDefaultAsync(task => task.Id == taskStatusInputDTO.Id);
                tasks.Add(task);
            }
            await unityOfWork.RunTransaction(async () =>
            {
                for (var index = 0; index < tasks.Count; index++)
                {
                    var task = tasks[index];
                    var oldTaskStatus = task.TaskStatusId;
                    task.TaskStatusId = taskStatusInputDTO.StatusId;
                    switch (task.TaskStatusId)
                    {
                        case (int)TaskStatusEnum.Started:
                            {
                                task = UpdateTaskDelay(task);
                            }
                            break;
                        case (int)TaskStatusEnum.Successful:
                            {
                                task.SuccessfulDate = DateTime.UtcNow;
                                task.TotalTime = await CalculateTaskTotalTimeAsync(task);
                            }
                            break;
                    }

                    if (task.DriverId.HasValue && task.Driver != null)
                    {
                        await taskCommonService.ChangeDriverAvailabilityAsync(task.Driver);
                        message = localizer.Format(Keys.Notifications.TaskNoActionByUserName, task.Id, ((TaskStatusEnum)task.TaskStatusId).ToString(), currentUser.UserName);
                        await taskCommonService.CreateDriverTaskNotificationAsync(task, message);
                    }
                    subTaskRepository.Update(task);
                    taskHistoryRepository.Add(new TaskHistory()
                    {
                        TaskId = task.Id,
                        MainTaskId = task.MainTaskId,
                        ActionName = "CHANGE TASK STATUS",
                        FromStatusId = oldTaskStatus,
                        ToStatusId = taskStatusInputDTO.StatusId,
                        Reason = taskStatusInputDTO.Reason
                    });
                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Tasks",
                        ActivityType = "Change Status",
                        Description = $"Task ({task.Id}) status has been changed from ({(TaskStatusEnum)oldTaskStatus }) To ({(TaskStatusEnum)taskStatusInputDTO.StatusId})",
                        Record_Id = task.Id
                    });
                }
                await unityOfWork.SaveChangesAsync();

                var firstTask = tasks?.FirstOrDefault();
                if (firstTask != null)
                {
                    await taskCommonService.ChangeMainTaskStatusAsync(firstTask.MainTaskId);
                    await unityOfWork.SaveChangesAsync();
                }
            });

            var firstTask = tasks?.FirstOrDefault();
            if (firstTask != null && firstTask.DriverId.HasValue)
            {
                message = localizer.Format(Keys.Notifications.TaskNoActionByUserName, firstTask.Id, ((TaskStatusEnum)firstTask.TaskStatusId).ToString(), currentUser.UserName);
                await ChangeTaskStatusNotificationAsync(firstTask, message);  //TODO: need to run in background 
            }
        }
        public async Task ReassignDriverAsync(ReassignDriverToSubTaskInputDTO reassignDriverToSubTask)
        {
            if (reassignDriverToSubTask == null || reassignDriverToSubTask.OldDriverId == reassignDriverToSubTask.NewDriverId)
            {
                return;
            }
            var tasks = await subTaskRepository.GetAllAsync(task => reassignDriverToSubTask.TaskIds.Contains(task.Id)
                && task.TaskStatusId != (int)TaskStatusEnum.Successful);
            var oldDriver = await driverRepository.GetFirstOrDefaultAsync(driver => driver.Id == reassignDriverToSubTask.OldDriverId);
            await unityOfWork.RunTransaction(async () =>
            {
                foreach (var subTask in tasks)
                {
                    var oldDriverId = subTask.DriverId;
                    subTask.DriverId = reassignDriverToSubTask.NewDriverId;
                    subTaskRepository.Update(subTask);
                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "Tasks",
                        ActivityType = "Reassign",
                        Description = $"Task ({subTask.Id}) has been reasign from driver ({oldDriverId}) To driver ({reassignDriverToSubTask.NewDriverId})",
                        Record_Id = subTask.Id
                    });
                    taskHistoryRepository.Add(new TaskHistory(currentUser.TenantId)
                    {
                        TaskId = subTask.Id,
                        MainTaskId = subTask.MainTaskId,
                        ActionName = "REASSIGNED",
                    });

                    if (subTask.DriverId.HasValue)
                    {
                        await taskCommonService.CreateDriverTaskNotificationAsync(subTask, localizer.Format(Keys.Notifications.TaskNoHasBeenUpdated, subTask.Id));
                    }
                }
                await unityOfWork.SaveChangesAsync();

                if (oldDriver != null)
                {
                    await taskCommonService.ChangeDriverAvailabilityAsync(oldDriver);
                    await unityOfWork.SaveChangesAsync();
                }

                var firstTask = tasks?.FirstOrDefault();
                if (firstTask != null)
                {
                    await taskCommonService.ChangeMainTaskStatusAsync(firstTask.MainTaskId);
                    await unityOfWork.SaveChangesAsync();
                }
            });

            if (oldDriver != null)
            {
                ReassignOldDriverNotification(tasks.ToList(), oldDriver);
            }

            var newDriver = await driverRepository.GetFirstOrDefaultAsync(driver => driver.Id == reassignDriverToSubTask.NewDriverId);
            ReassignNewDriverNotification(newDriver);
        }


        private async Task<int?> GetTaskMatchingGeoFenceAsync(SubTask task)
        {
            int? geofenceId = null;
            var geoFences = (await geoFenceRepository.GetAllAsync(geofence => !geofence.IsDeleted))
                ?.ToList();
            if (geoFences != null && geoFences.Any())
            {
                if (!(task.Latitude.HasValue && task.Longitude.HasValue))
                {
                    return null;
                }

                var taskPoint = new MapPoint
                {
                    Latitude = task.Latitude.Value,
                    Longitude = task.Longitude.Value
                };
                foreach (var geoFence in geoFences)
                {
                    var polygon = geoFence.GeoFenceLocations.Select(location =>
                        new MapPoint
                        {
                            Latitude = location.Latitude ?? 0,
                            Longitude = location.Longitude ?? 0
                        })
                        .ToArray();
                    if (MapsHelper.IsPointInPolygon(polygon, taskPoint))
                    {
                        geofenceId = geoFence.Id;
                        break;
                    }
                }
            }

            return geofenceId;
        }

        private SubTask UpdateTaskDelay(SubTask task)
        {
            task.StartDate = DateTime.UtcNow;
            var taskDate = task.PickupDate.HasValue
                ? task.PickupDate
                : task.DeliveryDate;

            if (taskDate.HasValue && task.StartDate > taskDate)
            {
                task.DelayTime = task.StartDate.Value.Subtract(taskDate.Value).TotalMilliseconds;
                task.MainTask.IsDelayed = true;
            }

            return task;
        }
        private async Task<double> CalculateTaskTotalTimeAsync(SubTask task)
        {
            var startTaskHistory =
               await taskHistoryRepository
                .GetFirstOrDefaultAsync(history => history.TaskId == task.Id
                    && history.ToStatusId == (int)TaskStatusEnum.Started, true);


            if (startTaskHistory != null)
            {
                return DateTime.UtcNow.Subtract(startTaskHistory.CreatedDate).TotalSeconds;
            }
            return 0;
        }

        private void UpdateTaskNotification(SubTask subTask, Driver subTaskDriver)
        {
            if (subTask.DriverId.HasValue)
            {
                notificationService.SendToUser(
                   subTaskDriver.UserId,
                   localizer.Format(Keys.Notifications.TaskNoHasBeenUpdated, subTask.Id),
                   CurrentNotificationSenderName,
                   localizer.Format(Keys.Notifications.TaskNoHasBeenUpdated, subTask.Id),
                   new { notificationType = NotificationTypeEnum.NewTask });
            }
        }
        private void DeleteTaskNotification(SubTask task)
        {
            var message = localizer.Format(Keys.Notifications.TaskNoHasBeenDeletedFromYou, task.Id);
            notificationService.SendToUser(
                  task.Driver.UserId,
                  message,
                  CurrentNotificationSenderName,
                  message,
                  new { notificationType = NotificationTypeEnum.NewTask });
        }
        private void ReassignOldDriverNotification(List<SubTask> tasks, Driver oldDriver)
        {
            var message = string.Format(localizer[Keys.Notifications.TaskNoHasBeenDeletedFromYou], tasks.FirstOrDefault().Id);
            notificationService.SendToUser(
               oldDriver.UserId,
               message,
               CurrentNotificationSenderName,
               message,
               new { notificationType = NotificationTypeEnum.ReassignTask });
        }
        private void ReassignNewDriverNotification(Driver newDriver)
        {
            notificationService.SendToUser(
                   newDriver.UserId,
                   localizer[Keys.Notifications.NewTasksAssignedToYou],
                   CurrentNotificationSenderName,
                   localizer[Keys.Notifications.NewTasksAssignedToYou],
                  new { notificationType = (int)NotificationTypeEnum.NewTask });
        }
        private async Task ChangeTaskStatusNotificationAsync(SubTask task, string message)
        {
            if (task.DriverId.HasValue)
            {
                var driver = task.Driver;
                if (driver == null)
                    driver = await driverRepository.GetFirstOrDefaultAsync(x => x.Id == task.DriverId, true);

                notificationService.SendToUser(
                   driver.UserId,
                   message,
                   CurrentNotificationSenderName,
                   message,
                   new { notificationType = NotificationTypeEnum.NewTask });
            }
        }

    }
}
