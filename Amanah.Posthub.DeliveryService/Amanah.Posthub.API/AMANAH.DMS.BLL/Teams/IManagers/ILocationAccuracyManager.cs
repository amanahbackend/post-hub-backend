﻿using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.ViewModels;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface ILocationAccuracyManager : IBaseManager<LocationAccuracyViewModel, LocationAccuracy>
    {
        Task<LocationAccuracyViewModel> GetFirstOrDefaultAsync(Expression<Func<LocationAccuracy, bool>> expression);
    }
}
