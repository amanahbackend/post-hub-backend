﻿using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.ViewModels;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface ITeamsManager : IBaseManager<TeamViewModel, Team>
    {
        Task<TeamViewModel> Get(int id);
        Task<List<LkpResultDto>> GetByKeywordAsync(string SearchKey);
        Task<List<TeamViewModel>> GetAllAsync(List<int> managerTeamIds);
        Task<PagedResult<TeamViewModel>> GetAllByPaginationAsync(PaginatedItemsViewModel pagingparametermodel, List<int> managerTeamIds);
        Task<List<int>> GetManagerTeamsAsync();
        Task<TeamViewModel> AddTenantTeamAsync(TeamViewModel teamViewModel);
    }
}
