﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.ViewModels;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class LocationAccuracyManager : BaseManager<LocationAccuracyViewModel, LocationAccuracy>, ILocationAccuracyManager
    {
        private readonly IMapper _mapper;

        public LocationAccuracyManager(
            ApplicationDbContext context,
            IMapper mapper,
            IRepositry<LocationAccuracy> repository)
            : base(context, repository, mapper)
        {
            _mapper = mapper;
        }

        public async Task<LocationAccuracyViewModel> GetFirstOrDefaultAsync(Expression<Func<LocationAccuracy, bool>> expression)
        {
            return await context.LocationAccuracy
                .Where(expression)
                .AsNoTracking()
                .IgnoreQueryFilters()
                .ProjectTo<LocationAccuracyViewModel>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
    }
}
