﻿namespace Amanah.Posthub.BLL.Teams.Queries.DTOs
{
    public class TeamLookupResponseDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }
}
