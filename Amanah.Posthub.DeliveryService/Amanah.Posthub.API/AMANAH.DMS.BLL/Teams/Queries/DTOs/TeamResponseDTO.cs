﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Teams.Queries.DTOs
{
    public class TeamResponseDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Tags { set; get; }
        public string Address { set; get; }
        public int LocationAccuracyId { set; get; }
        public string LocationAccuracyName { set; get; }

        public List<TeamDriverResponseDTO> Drivers { get; set; }
        public List<TeamManagerResponseDTO> TeamManagers { get; set; }

    }

    public class TeamDriverResponseDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string ImageUrl { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class TeamManagerResponseDTO
    {
        public int Id { set; get; }
        public int TeamId { set; get; }
        public string TeamName { set; get; }
        public int ManagerId { set; get; }
    }
}
