﻿using Amanah.Posthub.BLL.Teams.Queries.DTOs;
using Amanah.Posthub.Context;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.Teams.Queries
{
    public interface ITeamQuery
    {
        Task<List<TeamResponseDTO>> GetAllAsync(List<int> managerTeamIds = null);
        Task<PagedResult<TeamResponseDTO>> GetAllByPaginationAsync(TeamFilterDTO filter, List<int> managerTeamIds = null);
        Task<List<TeamLookupResponseDTO>> GetByKeywordAsync(string searchKey);
        Task<TeamResponseDTO> GetAsync(int id);

    }
    internal class TeamQuery : ITeamQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;

        public TeamQuery(
            ApplicationDbContext context,
            IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<List<TeamResponseDTO>> GetAllAsync(List<int> managerTeamIds = null)
        {
            var query = context.Teams.AsQueryable().IgnoreQueryFilters();
            if (managerTeamIds != null && managerTeamIds.Any())
            {
                query = query.Where(team => managerTeamIds.Contains(team.Id));
            }
            return await query
                 .ProjectTo<TeamResponseDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();
        }

        public async Task<PagedResult<TeamResponseDTO>> GetAllByPaginationAsync(
            TeamFilterDTO filter,
            List<int> managerTeamIds = null)
        {
            filter.PageNumber = (filter.PageNumber == 0) ? 1 : filter.PageNumber;
            filter.PageSize = (filter.PageSize == 0) ? 20 : filter.PageSize;

            var query = context.Teams.AsQueryable().IgnoreQueryFilters();
            if (managerTeamIds != null && managerTeamIds.Any())
            {
                query = query.Where(team => managerTeamIds.Contains(team.Id));
            }

            var pagedResult = new PagedResult<TeamResponseDTO>
            {
                TotalCount = await query.CountAsync()
            };

            var teams = await query
                .IgnoreQueryFilters()
                .Skip((filter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ProjectTo<TeamResponseDTO>(mapper.ConfigurationProvider)
                .ToListAsync();
            teams.ForEach(team =>
                team.Drivers = team.Drivers.Where(driver => !driver.IsDeleted).ToList());
            pagedResult.Result = teams;

            return pagedResult;
        }

        public async Task<List<TeamLookupResponseDTO>> GetByKeywordAsync(string searchKey)
        {
            var query = context.Teams.AsQueryable().IgnoreQueryFilters();
            if (!string.IsNullOrEmpty(searchKey))
            {
                query = query.Where(team => team.Name.ToLower().Trim().Contains(searchKey.ToLower().Trim()));
            }
            return await query
                 .Take(10)
                 .ProjectTo<TeamLookupResponseDTO>(mapper.ConfigurationProvider)
                 .ToListAsync();
        }

        public async Task<TeamResponseDTO> GetAsync(int id)
        {
            return await context.Teams
                .IgnoreQueryFilters()
                .Where(team => team.Id == id)
                .ProjectTo<TeamResponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }


    }
}
