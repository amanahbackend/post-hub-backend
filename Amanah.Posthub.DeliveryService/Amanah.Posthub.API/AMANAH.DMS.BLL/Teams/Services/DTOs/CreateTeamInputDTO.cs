﻿using FluentValidation;

namespace Amanah.Posthub.BLL.Teams.Services.DTOs
{
    public class CreateTeamInputDTO
    {
        public string Name { set; get; }
        public string Tags { set; get; }
        public string Address { set; get; }
        public int LocationAccuracyId { set; get; }

        public class CreateTeamInputDTOValidator : AbstractValidator<CreateTeamInputDTO>
        {
            public CreateTeamInputDTOValidator()
            {
                RuleFor(team => team.Name).NotEmpty().NotNull().MaximumLength(100);
                RuleFor(team => team.LocationAccuracyId).NotEmpty().NotNull();
            }
        }
    }
}
