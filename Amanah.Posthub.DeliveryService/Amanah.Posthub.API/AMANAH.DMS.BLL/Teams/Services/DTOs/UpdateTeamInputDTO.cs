﻿using FluentValidation;

namespace Amanah.Posthub.BLL.Teams.Services.DTOs
{
    public class UpdateTeamInputDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Tags { set; get; }
        public string Address { set; get; }
        public int LocationAccuracyId { set; get; }


        public class UpdateTeamInputDTOValidator : AbstractValidator<UpdateTeamInputDTO>
        {
            public UpdateTeamInputDTOValidator()
            {
                RuleFor(team => team.Id).NotEmpty().NotNull();
                RuleFor(team => team.Name).NotEmpty().NotNull().MaximumLength(500);
                RuleFor(team => team.LocationAccuracyId).NotEmpty().NotNull();
            }
        }
    }
}
