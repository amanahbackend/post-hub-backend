﻿using Amanah.Posthub.BLL.Teams.Queries.DTOs;
using Amanah.Posthub.BLL.Teams.Services.DTOs;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.Teams.Services
{
    public interface ITeamService
    {
        Task<TeamResponseDTO> CreateAsync(CreateTeamInputDTO createTeam);
        Task<bool> UpdateAsync(UpdateTeamInputDTO updateTeam);
        Task<(bool IsSuccess, string Error)> DeleteAsync(int id);
    }
    internal class TeamService : ITeamService
    {
        private readonly IRepository<Team> teamRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly ILocalizer localizer;

        public TeamService(
            IRepository<Team> teamRepository,
            IRepository<AccountLogs> accountLogRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            ILocalizer localizer)
        {
            this.teamRepository = teamRepository;
            this.accountLogRepository = accountLogRepository;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.localizer = localizer;
        }

        public async Task<TeamResponseDTO> CreateAsync(CreateTeamInputDTO createTeam)
        {
            var team = mapper.Map<Team>(createTeam);

            await unitOfWork.RunTransaction(async () =>
            {
                teamRepository.Add(team);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Team",
                    ActivityType = "Create",
                    Description = $"Team {team.Name} has been created",
                    Record_Id = team.Id
                });

                await unitOfWork.SaveChangesAsync();
            });
            return mapper.Map<TeamResponseDTO>(team);
        }

        public async Task<bool> UpdateAsync(UpdateTeamInputDTO updateTeam)
        {
            var existedTeam = await teamRepository.GetFirstOrDefaultAsync(team => team.Id == updateTeam.Id);
            if (existedTeam == null)
            {
                return false;
            }

            existedTeam = mapper.Map(updateTeam, existedTeam);
            await unitOfWork.RunTransaction(async () =>
            {
                teamRepository.Update(existedTeam);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Team",
                    ActivityType = "Update",
                    Description = $"Team { existedTeam.Name } has been updated",
                    Record_Id = existedTeam.Id
                });

                await unitOfWork.SaveChangesAsync();
            });

            return true;
        }

        public async Task<(bool IsSuccess, string Error)> DeleteAsync(int id)
        {
            var existedTeam = await teamRepository.GetFirstOrDefaultAsync(team => team.Id == id,
                team => team.Drivers, team => team.TeamManagers);
            if (existedTeam == null)
            {
                return (false, localizer[Keys.Validation.TeamNotFound]);
            }
            if (existedTeam.Drivers?.Count > 0)
            {
                return (false, localizer[Keys.Validation.CanNotDeleteTeamItHasDrivers]);
            }
            if (existedTeam.TeamManagers?.Count > 0)
            {
                return (false, localizer[Keys.Validation.CanNotDeleteTeamIHasManager]);
            }

            await unitOfWork.RunTransaction(async () =>
            {
                existedTeam.SetIsDeleted(true);
                teamRepository.Update(existedTeam);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "Team",
                    ActivityType = "Delete",
                    Description = $"Team { existedTeam.Name } has been deleted",
                    Record_Id = existedTeam.Id
                });

                await unitOfWork.SaveChangesAsync();
            });

            return (true, string.Empty);
        }

    }
}
