﻿using FluentValidation;

namespace Amanah.Posthub.ViewModels
{
    public class LocationAccuracyViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public int? Duration { set; get; }
    }

    public class LocationAccuracyViewModelValidator : AbstractValidator<LocationAccuracyViewModel>
    {
        public LocationAccuracyViewModelValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(50);
        }
    }
}