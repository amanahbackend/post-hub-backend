﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class TeamManagerViewModel
    {
        public int Id { set; get; }
        public int TeamId { set; get; }
        public string TeamName { set; get; }
        public int ManagerId { set; get; }
    }
}
