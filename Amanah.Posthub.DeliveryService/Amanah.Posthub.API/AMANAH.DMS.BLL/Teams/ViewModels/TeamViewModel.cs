﻿using Amanah.Posthub.BLL.ViewModels;
using FluentValidation;
using System.Collections.Generic;

namespace Amanah.Posthub.ViewModels
{
    public class TeamViewModel
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Tags { set; get; }
        public string Address { set; get; }
        public int LocationAccuracyId { set; get; }
        public string LocationAccuracyName { set; get; }
        public List<DriverTeamViewModel> Drivers { get; set; }

        public List<TeamManagerViewModel> TeamManagers { get; set; }

        public string TenantId { get; set; }
    }

    public class TeamViewModelValidator : AbstractValidator<TeamViewModel>
    {
        public TeamViewModelValidator()
        {
            RuleFor(x => x.Name).NotEmpty().NotNull().MaximumLength(100);
            RuleFor(x => x.LocationAccuracyId).NotEmpty().NotNull();
        }
    }
}
