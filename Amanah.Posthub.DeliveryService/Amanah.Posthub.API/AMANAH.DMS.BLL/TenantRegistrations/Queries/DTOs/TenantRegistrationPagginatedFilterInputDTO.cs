﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.TenantRegistrations.Queries.DTOs
{
    public class TenantRegistrationPagginatedFilterInputDTO : PaginatedItemsViewModel
    {

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string UserID { get; set; }
        public List<UserRegistrationStatus> UserRegistrationStatuses { get; set; }
        public BusinessType? BusinessType { get; set; }
    }
}
