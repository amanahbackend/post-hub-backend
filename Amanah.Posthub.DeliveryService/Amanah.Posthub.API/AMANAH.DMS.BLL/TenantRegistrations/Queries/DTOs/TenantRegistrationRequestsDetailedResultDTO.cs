﻿using Amanah.Posthub.BLL.TenantRegistrations.Services.DTO;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.TenantRegistrations.Queries.DTOs
{
    public class TenantRegistrationRequestsDetailedResultDTO : CreateTenantRegistrationDTO
    {
        public int Id { get; set; }
        public UserRegistrationStatus UserRegistrationStatus { get; set; }
        public string RejectReason { get; set; }
        public string Comment { get; set; }
        public List<TenantRegistrationAttachmentDTO> Attachments { get; set; } =
            new List<TenantRegistrationAttachmentDTO>();

        public string BusinessTypeName { get; set; }

        public ApproveTenantRegistrationDTO ApproveSettings { get; set; }

    }
}
