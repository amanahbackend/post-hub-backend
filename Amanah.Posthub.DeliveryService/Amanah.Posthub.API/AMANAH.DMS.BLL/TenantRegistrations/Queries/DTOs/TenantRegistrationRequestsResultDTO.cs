﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using System;

namespace Amanah.Posthub.BLL.TenantRegistrations.Queries.DTOs
{
    public class TenantRegistrationRequestsResultDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string BusinessCID { get; set; }
        public string BusinessTypeName { get; set; }
        public string BusinessAddressSummary { get; set; }
        public string Comment { get; set; }
        public UserRegistrationStatus UserRegistrationStatus { get; set; }
        public string ApprovedOrRejectedByUserFullName { get; set; }
        public int BusinessType { get; set; }

    }
}
