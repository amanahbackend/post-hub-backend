﻿using Amanah.Posthub.BLL.TenantRegistrations.Queries.DTOs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.BLL.TenantRegistrations.Queries
{
    public interface ITenantRegistrationRequestQuery
    {
        Task<PagedResult<TenantRegistrationRequestsResultDTO>> GetAllByPaginationAsync(TenantRegistrationPagginatedFilterInputDTO tenantSearchViewModel);
        Task<TenantRegistrationRequestsDetailedResultDTO> GetByIdAsync(int id);
        Task<PagedResult<HistoricalAuditorDTO>> GetHistoricalAuditorsAsync(PaginatedItemsViewModel paginatedItems);
    }

    public class TenantRegistrationRequestQuery : ITenantRegistrationRequestQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly IRepository<TenantRegistration> repository;
        private readonly ILocalizer _localizer;


        public TenantRegistrationRequestQuery
            (
            IRepository<TenantRegistration> repository,
            IMapper mapper,
            ApplicationDbContext context,
            ILocalizer localizer)
        {
            this.repository = repository;
            this.mapper = mapper;
            this.context = context;
            this._localizer = localizer;
        }

        public async Task<PagedResult<TenantRegistrationRequestsResultDTO>> GetAllByPaginationAsync(TenantRegistrationPagginatedFilterInputDTO filter)
        {
            var source = repository.GetAllIQueryable();
            if (filter.StartDate.HasValue)
            {
                source = source.Where(registration => filter.StartDate.Value.Date <= registration.CreatedDate.Date);
            }
            if (filter.EndDate.HasValue)
            {
                source = source.Where(registration => filter.EndDate.Value.Date >= registration.CreatedDate.Date);
            }
            if (filter.UserRegistrationStatuses != null && filter.UserRegistrationStatuses.Any())
            {
                source = source.Where(registration => filter.UserRegistrationStatuses.Contains(registration.UserRegistrationStatus));
            }
            if (!string.IsNullOrWhiteSpace(filter.UserID))
            {
                source = source.Where(registration => registration.ApprovedORejectedByUserId == filter.UserID);
            }
            if (filter.BusinessType.HasValue)
            {
                source = source.Where(registration => registration.BusinessType == filter.BusinessType);
            }
            var pagedResult = await source
                .OrderByDescending(registration => registration.CreatedDate)
                .ToPagedResultAsync<TenantRegistration, TenantRegistrationRequestsResultDTO>(
                    filter, mapper.ConfigurationProvider);
            var governarates = await context.Governrates
                    .IgnoreQueryFilters()
                    .ToListAsync();

            foreach (var request in pagedResult.Result)
            {
                request.BusinessTypeName = _localizer[((BusinessType)request.BusinessType).ToString()];
            }

            return pagedResult;
        }

        public async Task<TenantRegistrationRequestsDetailedResultDTO> GetByIdAsync(int id)
        {
            var request = await context.TenantRegistrations
                .Where(registration => registration.Id == id)
                .AsNoTracking()
                .ProjectTo<TenantRegistrationRequestsDetailedResultDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
            request.BusinessTypeName = _localizer[((BusinessType)request.BusinessType).ToString()];
            return request;
        }

        public async Task<PagedResult<HistoricalAuditorDTO>> GetHistoricalAuditorsAsync(PaginatedItemsViewModel paginatedItems)
        {
            var statuses = new[] { UserRegistrationStatus.Approved, UserRegistrationStatus.Rejected };
            var result = await context.TenantRegistrations
                .Where(registration => statuses.Contains(registration.UserRegistrationStatus))
                .Select(registration => registration.ApprovedORejectedByUser)
                .Distinct()
                .Where(user =>
                    string.IsNullOrWhiteSpace(paginatedItems.SearchBy) ||
                    user.FirstName.Contains(paginatedItems.SearchBy) ||
                    user.LastName.Contains(paginatedItems.SearchBy) ||
                    user.Email.Contains(paginatedItems.SearchBy) ||
                    user.UserName.Contains(paginatedItems.SearchBy))
                .Select(user =>
                    new HistoricalAuditorDTO
                    {
                        UserId = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName
                    })
                .ToPagedResultAsync(paginatedItems);
            return result;
        }
    }
}
