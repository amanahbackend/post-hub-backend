﻿using FluentValidation;
using System.Collections.Generic;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.TenantRegistrations.Services.DTO
{
    public class ApproveTenantRegistrationDTO
    {
        public int TenantRegistrationId { get; set; }
        public bool IsFullVersion { get; set; }
        public string AutoAllocationMethod { get; set; }
        public List<int> SelectedModules { get; set; }

        public class ApproveTenantRegistrationDTOValidator : AbstractValidator<ApproveTenantRegistrationDTO>
        {
            public ApproveTenantRegistrationDTOValidator(ILocalizer localizer)
            {
                RuleFor(approvedTanent => new { approvedTanent.AutoAllocationMethod, approvedTanent.IsFullVersion })
                    .Custom((obj, context) =>
                {
                    if (!obj.IsFullVersion && string.IsNullOrEmpty(obj.AutoAllocationMethod))
                    {
                        context.AddFailure(localizer[Keys.Validation.AutoAllocationMethodIsMissing]);
                    }
                });
            }
        }
    }

}
