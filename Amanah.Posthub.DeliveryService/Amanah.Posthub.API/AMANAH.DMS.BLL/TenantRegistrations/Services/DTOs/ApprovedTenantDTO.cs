﻿using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.TenantRegistrations.Entities;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.TenantRegistrations.Services.DTO
{
    public class ApprovedTenantDTO : ApproveTenantRegistrationDTO
    {
        public TenantRegistration TenantRegistration { get; set; }
    }

    public class ApprovedTenantViewModelValidator
            : AbstractValidator<ApprovedTenantDTO>
    {
        private readonly ApplicationDbContext _context;

        public ApprovedTenantViewModelValidator(ILocalizer localizer, ApplicationDbContext context)
        {
            _context = context;
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(apprrove => apprrove.AutoAllocationMethod)
                .MustAsync(async (autoAllocationMethod, cancellation) =>
                    await HasMethodWithPlatfoermSettingAsync(autoAllocationMethod))
                .WithMessage(localizer[Keys.Validation.PlatformSettingMissing]);
        }

        private async Task<bool> HasMethodWithPlatfoermSettingAsync(string autoAllocationMethod)
        {
            if (string.IsNullOrEmpty(autoAllocationMethod))
            {
                return true;
            }

            List<string> autoAllocationSettingKeys = null;
            switch (autoAllocationMethod)
            {
                case SettingKeyConstant.NearestAvailableMethod:
                    {
                        autoAllocationSettingKeys = new List<string>
                        {
                            SettingKeyConstant.NearestAvailableMethodOrderCapacity,
                            SettingKeyConstant.NearestAvailableMethodRadiusInKM ,
                            SettingKeyConstant.NearestAvailableMethodNumberOfRetries,
                            SettingKeyConstant.DriverLoginRequestExpiration,
                            SettingKeyConstant.ReachedDistanceRestriction
                        };
                    }
                    break;
                case SettingKeyConstant.OneByOneMethod:
                    {
                        autoAllocationSettingKeys = new List<string>
                        {
                            SettingKeyConstant.DriverLoginRequestExpiration,
                            SettingKeyConstant.ReachedDistanceRestriction
                        };
                    }
                    break;
                default:
                    autoAllocationSettingKeys = new List<string>();
                    break;
            };

            var generalSettings = await _context.Settings
                 .Where(setting => autoAllocationSettingKeys.Contains(setting.SettingKey))
                 .ToListAsync();

            foreach (var key in autoAllocationSettingKeys)
            {
                if (!generalSettings.Where(setting => setting.SettingKey == key).Any())
                {
                    return false;
                }
            };

            return true;
        }
    }

}
