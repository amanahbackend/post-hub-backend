﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.TenantRegistrations.Services.DTO
{
    public class CreateTenantRegistrationDTO
    {
        public string Name { get; set; }
        public string Email { get; set; }

        //public string PhoneNumber { get; set; }
        public int BusinessType { get; set; }
        public string OtherBusinessType { get; set; }

        public string BusinessCID { get; set; }
        public int CountryId { get; set; }
        public Address BusinessAddress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public List<string> SocialMediaAccounts { get; set; } = new List<string>();

        public string OwnerName { get; set; }
        public string OwnerCID { get; set; }
        public string OwnerPhoneNumber { get; set; }

        public bool IsSameBusinessAddress { get; set; }
        public Address OwnerAddress { get; set; }

        public bool HasOwnDrivers { get; set; }
        public bool HasMultiBranches { get; set; }
        public int BranchCount { get; set; }

        public bool IsDeliverToAllZones { get; set; }
        public double? ServingRadiusInKM { get; set; }
        public int NumberOfOrdersByDriverPerOrder { get; set; }

        public class CreateTenantRegistrationDTOValidator : AbstractValidator<CreateTenantRegistrationDTO>
        {
            private readonly ApplicationDbContext context;

            public CreateTenantRegistrationDTOValidator(ILocalizer localizer,
                ApplicationDbContext context)
            {
                this.context = context;
                RuleFor(model => model.Name).NotEmpty();
                RuleFor(registration => registration.Email)
                    .NotEmpty()
                    .MustAsync(IsEmailExistAsync)
                    .WithMessage(localizer[Keys.Validation.TenantEmailIsAlreadyExist]);
                RuleFor(registration => registration.OwnerPhoneNumber)
                    .NotEmpty()
                    .MustAsync(IsPhoneNumberExistAsync)
                    .WithMessage(localizer[Keys.Validation.TenantPhoneIsAlreadyExist]);
                RuleFor(registration => registration.BusinessCID)
                    .NotEmpty()
                    .MustAsync(IsBusinessCIDExistAsync)
                    .WithMessage(localizer[Keys.Validation.TenantCIDIsAlreadyExist]);
                RuleFor(model => model.BusinessAddress).NotNull();
                RuleFor(model => model.OwnerAddress)
                    .NotNull().When(model => !model.IsSameBusinessAddress);
            }

            private async Task<bool> IsEmailExistAsync(string email, CancellationToken cancellationToken)
            {
                var userEmailExist = await context.Users
                    .Where(user => !string.IsNullOrEmpty(email) && user.Email.ToLower().Trim() == email)
                    .AnyAsync();
                if (userEmailExist)
                {
                    return false;
                }

                return !await context.TenantRegistrations
                    .Where(tenantRegistration =>
                        tenantRegistration.UserRegistrationStatus != UserRegistrationStatus.Rejected)
                    .AnyAsync(tenantRegistration =>
                        !string.IsNullOrEmpty(email) &&
                        tenantRegistration.Email.Trim().ToLower() == email.Trim().ToLower());
            }
            private async Task<bool> IsPhoneNumberExistAsync(string phoneNumber, CancellationToken cancellationToken)
            {
                var userPhoneNumberExist = await context.Users
                    .Where(user => !string.IsNullOrEmpty(phoneNumber) && user.PhoneNumber.ToLower().Trim() == phoneNumber)
                    .AnyAsync();
                if (userPhoneNumberExist)
                {
                    return false;
                }

                return !await context.TenantRegistrations
                    .Where(tenantRegistration =>
                        tenantRegistration.UserRegistrationStatus != UserRegistrationStatus.Rejected)
                    .Where(tenantRegistration =>
                        !string.IsNullOrEmpty(phoneNumber) &&
                        tenantRegistration.OwnerPhoneNumber.Trim().ToLower() == phoneNumber.Trim().ToLower())
                    .AnyAsync();
            }
            private async Task<bool> IsBusinessCIDExistAsync(string businessCID, CancellationToken cancellationToken)
            {
                return !await context.TenantRegistrations
                    .Where(tenantRegistration =>
                        tenantRegistration.UserRegistrationStatus != UserRegistrationStatus.Rejected)
                    .Where(tenantRegistration =>
                        !string.IsNullOrEmpty(businessCID) &&
                        tenantRegistration.BusinessCID.Trim().ToLower() == businessCID.Trim().ToLower())
                    .AnyAsync();
            }
        }
    }

}
