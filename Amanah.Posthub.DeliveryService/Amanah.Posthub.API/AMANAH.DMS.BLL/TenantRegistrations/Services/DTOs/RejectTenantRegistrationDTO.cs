﻿namespace Amanah.Posthub.BLL.TenantRegistrations
{
    public class RejectTenantRegistrationDTO
    {
        public int TenantId { get; set; }

        public string Reason { get; set; }
    }

}
