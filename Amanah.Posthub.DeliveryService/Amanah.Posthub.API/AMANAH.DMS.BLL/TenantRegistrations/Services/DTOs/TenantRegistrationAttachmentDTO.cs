﻿using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.BLL.TenantRegistrations
{
    public class TenantRegistrationAttachmentDTO
    {
        public int Id { get; set; }
        public string File { get; set; }
        public IFormFile FormFile { get; set; }
    }

}
