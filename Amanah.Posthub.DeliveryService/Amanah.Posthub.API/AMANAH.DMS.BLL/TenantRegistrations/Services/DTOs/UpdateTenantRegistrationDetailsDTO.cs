﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using FluentValidation;
using System.Collections.Generic;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.TenantRegistrations.Services.DTO
{
    public class UpdateTenantRegistrationDetailsDTO : CreateTenantRegistrationDTO
    {
        public int Id { get; set; }
        public UserRegistrationStatus UserRegistrationStatus { get; set; }
        public string RejectReason { get; set; }
        public string Comment { get; set; }
        public List<TenantRegistrationAttachmentDTO> Attachments { get; set; } =
            new List<TenantRegistrationAttachmentDTO>();

        public string BusinessTypeName { get; set; }

        public ApproveTenantRegistrationDTO ApproveSettings { get; set; }

        public class UpdateTenantRegistrationDetailsDTOValidator
            : AbstractValidator<UpdateTenantRegistrationDetailsDTO>
        {
            public UpdateTenantRegistrationDetailsDTOValidator(ILocalizer localizer)
            {
                RuleFor(model => model.Name).NotEmpty();
                RuleFor(model => model.Email).NotEmpty();
                RuleFor(model => model.BusinessCID).NotEmpty();
                RuleFor(model => model.OwnerName).NotEmpty();
                RuleFor(model => model.OwnerCID).NotEmpty();
                RuleFor(model => model.OwnerPhoneNumber).NotEmpty();
                RuleFor(model => model.BusinessAddress).NotNull();
                RuleFor(model => model.OwnerAddress)
                    .NotNull().When(model => !model.IsSameBusinessAddress);
                RuleFor(model => model.SocialMediaAccounts)
                    .NotNull().WithMessage(localizer[Keys.Validation.SocialMediaNotNull]);
                RuleFor(registration => registration.Attachments)
                    .NotNull().WithMessage(localizer[Keys.Validation.AttachmentNotNull]);
            }
        }
    }
}
