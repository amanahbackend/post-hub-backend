﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Addresses.Services;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.BLL.TenantRegistrations.Services.DTO;
using Amanah.Posthub.BLL.TenantRegistrations.ViewModels;
using Amanah.Posthub.DeliverService.BLL.Settings.Resources;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Infrastructure.ExternalServices.Settings;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Admins.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Utilites.UploadFile;
using Utilities.Extensions;
using Utilities.Utilites;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.BLL.TenantRegistrations.Services
{
    public interface ITenantRegistrationRequestService
    {
        public Task AddAsync(CreateTenantRegistrationDTO tenantRegistrationDTO);
        public Task UpdateAsync(UpdateTenantRegistrationDetailsDTO model);
        public Task RejectAsync(int id, string rejectReason);
        public Task ApproveAsync(ApproveTenantRegistrationDTO approveTenantRegistrationViewModel);
    }
    public class TenantRegistrationRequestService : ITenantRegistrationRequestService
    {
        private readonly IRepository<TenantRegistration> _tenantRegistrationRepository;
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly ISettingRepository _settingRepository;
        private readonly IRepository<Branch> _branchRepository;
        private readonly IRepository<Admin, string> _adminRepository;
        private readonly IRepository<LocationAccuracy> _locationAccuracyRepository;
        private readonly IRepository<Team> _teamRepository;

        private readonly IUnitOfWork _unityOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        private readonly IPACIService _paciService;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IUserDomainService _userDomainService;
        private readonly IRoleDomainService _roleDomainService;
        private readonly EmailSettings _emailSettings;
        private readonly IEmailSenderservice _emailsender;



        public TenantRegistrationRequestService(
           IRepository<TenantRegistration> repository,
           IRepository<AccountLogs> accountLogRepository,
           IRepository<Branch> branchRepository,
           IRepository<Admin, string> adminRepository,
           IRepository<Team> teamRepository,
           ISettingRepository settingRepository,
           IRepository<LocationAccuracy> locationAccuracyRepository,
           IUserDomainService userDomainService,
           IRoleDomainService roleDomainService,
           IUploadFormFileService uploadFormFileService,
           IPACIService paciService,
           ICurrentUser currentUser,
           IUnitOfWork unityOfWork,
           IOptions<EmailSettings> emailSettings,
           IEmailSenderservice emailsender,
           IMapper mapper)
        {
            _mapper = mapper;
            _currentUser = currentUser;
            _paciService = paciService;
            _tenantRegistrationRepository = repository;
            this.accountLogRepository = accountLogRepository;
            _unityOfWork = unityOfWork;
            _uploadFormFileService = uploadFormFileService;
            _userDomainService = userDomainService;
            _roleDomainService = roleDomainService;
            _emailSettings = emailSettings.Value;
            _emailsender = emailsender;
            _branchRepository = branchRepository;
            _adminRepository = adminRepository;
            _teamRepository = teamRepository;
            _settingRepository = settingRepository;
            _locationAccuracyRepository = locationAccuracyRepository;
        }

        public async Task AddAsync(CreateTenantRegistrationDTO tenantRegistrationDTO)
        {
            var hasPendingOrApprovedRequest = await _tenantRegistrationRepository.GetAllIQueryable()
                           .Where(tenantRegistration => tenantRegistration.UserRegistrationStatus != UserRegistrationStatus.Rejected)
                           .Where(tenantRegistration =>
                               (!string.IsNullOrEmpty(tenantRegistrationDTO.Email) &&
                               tenantRegistration.Email.Trim().ToLower() == tenantRegistrationDTO.Email.Trim().ToLower()) ||
                               (!string.IsNullOrEmpty(tenantRegistrationDTO.BusinessCID) &&
                               tenantRegistration.BusinessCID.Trim().ToLower() == tenantRegistrationDTO.BusinessCID.Trim().ToLower()) ||
                               (!string.IsNullOrEmpty(tenantRegistrationDTO.OwnerPhoneNumber) &&
                               tenantRegistration.PhoneNumber.Trim().ToLower() == tenantRegistrationDTO.OwnerPhoneNumber.Trim().ToLower()))
                           .AnyAsync();
            if (hasPendingOrApprovedRequest)
            {
                throw new DomainException(Validation.RegisteredBefore);
            }

            var tenantRegistration = _mapper.Map<TenantRegistration>(tenantRegistrationDTO);
            tenantRegistration.UserRegistrationStatus = UserRegistrationStatus.New;
            if (tenantRegistrationDTO.IsSameBusinessAddress)
            {
                tenantRegistration.OwnerAddress = tenantRegistration.BusinessAddress.Clone();
            }
            await _unityOfWork.RunTransaction(async () =>
            {
                await SetAddressSummaryAsync(tenantRegistration);
                _tenantRegistrationRepository.Add(tenantRegistration);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "TenantRegistration",
                    ActivityType = "Create",
                    Description = $"{tenantRegistration.Name}  ) has been registered ",
                    Record_Id = 0
                });

                await _unityOfWork.SaveChangesAsync();
            });

        }
        public async Task UpdateAsync(UpdateTenantRegistrationDetailsDTO model)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                var existingRegistration = await InternalGetByIdAsync(model.Id);
                if (model.CountryId == default)
                {
                    model.CountryId = 121;
                }
                _mapper.Map(model, existingRegistration);
                existingRegistration.UserRegistrationStatus = UserRegistrationStatus.Pending;

                if ((model.Attachments != null && model.Attachments.Any()) || existingRegistration.Attachments.Any())
                {
                    var oldAttachments = model.Attachments?.Where(attachment => attachment.Id != default)
                      .Select(attachment => attachment.Id)
                      .ToArray();
                    var addedAttatchments = model.Attachments?.Where(attachment => attachment.Id == default)
                       .Select(attachment => new TenantRegistrationAttachmentDTO
                       {
                           FormFile = attachment.FormFile,
                       })
                       .ToArray();
                    var deletedAttachments = existingRegistration.Attachments
                        .Where(attachment => !oldAttachments.Contains(attachment.Id))
                        .ToArray();
                    existingRegistration.Attachments.RemoveRange(deletedAttachments);
                    foreach (var file in deletedAttachments)
                    {
                        try
                        {
                            File.Delete(file.File);
                        }
                        catch (IOException)
                        {
                        }
                    }
                    var addedAttachments = (await UploadDriverAttachmentAsync(addedAttatchments))
                        .Select(attacmentModel => new TenantRegistrationAttachment
                        {
                            File = attacmentModel.File,
                        })
                        .ToArray();
                    existingRegistration.Attachments.AddRange(addedAttachments);
                }
                if ((model.SocialMediaAccounts != null && model.SocialMediaAccounts.Any())
                || existingRegistration.SocialMediaAccounts.Any())
                {
                    var modelLinks = model.SocialMediaAccounts?
                    .Select(link => link.ToLower().Trim())
                    .ToArray();
                    var existedLinks = existingRegistration.SocialMediaAccounts
                        .Select(link => link.AccountLink.ToLower().Trim())
                        .ToArray();

                    var addedLinks = modelLinks.Where(link => !existedLinks.Contains(link))
                      .Select(link => new TenantSocialMediaAccount
                      {
                          AccountLink = link
                      }).ToArray();
                    existingRegistration.SocialMediaAccounts.AddRange(addedLinks);
                    var deletedLinks = existingRegistration.SocialMediaAccounts
                       .Where(link => modelLinks.Contains(link.AccountLink.ToLower().Trim()))
                       .ToArray();
                    existingRegistration.SocialMediaAccounts.RemoveRange(deletedLinks);
                }
                if (model.IsSameBusinessAddress)
                {
                    existingRegistration.OwnerAddress = existingRegistration.BusinessAddress;
                }
                await SetAddressSummaryAsync(existingRegistration);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "TenantRegistration",
                    ActivityType = "Update",
                    Description = $"{existingRegistration.Name}  has been updated ",
                    Record_Id = existingRegistration.Id
                });

                await _unityOfWork.SaveChangesAsync();
            });
        }

        public async Task ApproveAsync(ApproveTenantRegistrationDTO approveTenantRegistration)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                _unityOfWork.IsTenantFilterEnabled = false;

                TenantRegistration tenantRegistration = await InternalGetByIdAsync(approveTenantRegistration.TenantRegistrationId);
                if (tenantRegistration == null)
                {
                    throw new DomainException(Validation.TenantNotExist);
                }
                tenantRegistration.UserRegistrationStatus = UserRegistrationStatus.Approved;
                tenantRegistration.ApprovedORejectedByUserId = _currentUser.Id;
                tenantRegistration.ApproveSettings = _mapper.Map<ApproveSettings>(approveTenantRegistration);
                _tenantRegistrationRepository.Update(tenantRegistration);
                var approvedTenant = _mapper.Map<ApprovedTenantDTO>(approveTenantRegistration);
                approvedTenant.TenantRegistration = tenantRegistration;
                await CreateTenantAsync(approvedTenant);
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "TenantRegistration",
                    ActivityType = "Approve",
                    Description = $"{tenantRegistration.Name} ( {tenantRegistration.Id} ) has been approved ",
                    Record_Id = tenantRegistration.Id
                });

                await _unityOfWork.SaveChangesAsync();
            });
        }
        public async Task RejectAsync(int id, string rejectReason)
        {
            await _unityOfWork.RunTransaction(async () =>
            {
                TenantRegistration tenantRegistration = await InternalGetByIdAsync(id);
                if (tenantRegistration == null)
                {
                    throw new DomainException(Keys.Validation.TenantNotExist);
                }
                tenantRegistration.UserRegistrationStatus = UserRegistrationStatus.Rejected;
                tenantRegistration.ApprovedORejectedByUserId = _currentUser.Id;
                tenantRegistration.RejectReason = rejectReason;
                _tenantRegistrationRepository.Update(tenantRegistration);

                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "TenantRegistration",
                    ActivityType = "Reject",
                    Description = $"{tenantRegistration.Name} ( {tenantRegistration.Id} ) has been rejected ",
                    Record_Id = tenantRegistration.Id
                });

                await _unityOfWork.SaveChangesAsync();

                NotifyTenantAboutRejection(tenantRegistration);
            });
        }

        private async Task<string> CreateTenantAsync(ApprovedTenantDTO approveTenant)
        {
            List<string> permissions = GetTenantPermissions(approveTenant);
            var user = _mapper.Map<ApplicationUser>(approveTenant.TenantRegistration);
            user.Id = null; // the fake mapper
            user.IsTenantUser = true;
            var password = PasswordGenerationHelper.Generate(8, 2);
            var res = await _userDomainService.CreateAsync(user, password, null, isTenantUser: true);
            user.Id = res.Id;
            var role = await _roleDomainService.CreateAsync(
                new ApplicationRole($"{approveTenant.TenantRegistration.Name} Role {user.Id}"
                , user.Id),
                permissions);


            user.RoleNames = new List<string> { role.Name };
            await _userDomainService.AddUserToRolesAsync(user);

            var resourceSet = SettingsKeys.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);

            if (approveTenant.IsFullVersion)
            {
                await _settingRepository.AddUserDefaultSettingsAsync(user.Id,
                    new DefaultSettingValue
                    {
                        IsEnableAutoAllocation = false,
                        AutoAllocationMethod = string.Empty,
                    },
                    resourceSet);
            }
            else
            {
                await _settingRepository.AddUserDefaultSettingsAsync(user.Id,
                    await GetPlatformAutoAllocationSettingAsync(approveTenant.AutoAllocationMethod),
                    resourceSet);
            }

            await SetDefaultBusinessSeedingAsync(approveTenant, user.Id);
            try
            {
                await SendResetPasswordEmailAsync(user);
            }
            catch { }

            return user.Id;
        }
        private async Task<TenantRegistration> InternalGetByIdAsync(int id)
        {
            return await _tenantRegistrationRepository.GetAllIQueryable()
                         .Include(tenantRegistration => tenantRegistration.Attachments)
                         .Include(tenantRegistration => tenantRegistration.SocialMediaAccounts)
                         .FirstOrDefaultAsync(e => e.Id == id);
        }
        private async Task<DefaultSettingValue> GetPlatformAutoAllocationSettingAsync(string methodName)
        {
            List<string> autoAllocationSettingKeys = null;
            switch (methodName)
            {
                case SettingKeyConstant.NearestAvailableMethod:
                    {
                        autoAllocationSettingKeys = new List<string>
                        {
                            SettingKeyConstant.NearestAvailableMethodOrderCapacity,
                            SettingKeyConstant.NearestAvailableMethodRadiusInKM,
                            SettingKeyConstant.NearestAvailableMethodNumberOfRetries,
                            SettingKeyConstant.DriverLoginRequestExpiration,
                            SettingKeyConstant.ReachedDistanceRestriction
                        };
                    }
                    break;
                case SettingKeyConstant.OneByOneMethod:
                    {
                        autoAllocationSettingKeys = new List<string>
                        {
                            SettingKeyConstant.DriverLoginRequestExpiration,
                            SettingKeyConstant.ReachedDistanceRestriction
                        };
                    }
                    break;
            };

            var generalSettings = await _settingRepository
                .GetAllIQueryable()
                .IgnoreQueryFilters()
                .Where(setting => autoAllocationSettingKeys.Contains(setting.SettingKey))
                .Where(setting => !setting.IsDeleted && string.IsNullOrEmpty(setting.Tenant_Id))
                .ToListAsync().ConfigureAwait(false);

            if (generalSettings == null || !generalSettings.Any())
            {
                throw new DomainException(Validation.PlatformSettingMissing);
            }

            var capacitySetting = generalSettings.Where(setting =>
                setting.SettingKey == SettingKeyConstant.NearestAvailableMethodOrderCapacity)
                .FirstOrDefault();
            if (capacitySetting == null)
            {
                throw new DomainException(Validation.PlatformSettingMissing);
            }
            int.TryParse(capacitySetting.Value, out int capacity);

            var radiusSetting = generalSettings.Where(setting =>
                setting.SettingKey == SettingKeyConstant.NearestAvailableMethodRadiusInKM)
                .FirstOrDefault();
            if (radiusSetting == null)
            {
                throw new DomainException(Validation.PlatformSettingMissing);
            }
            double.TryParse(radiusSetting.Value, out double radius);

            var retriesSetting = generalSettings.Where(setting =>
             setting.SettingKey == SettingKeyConstant.NearestAvailableMethodNumberOfRetries)
             .FirstOrDefault();
            if (retriesSetting == null)
            {
                throw new DomainException(Validation.PlatformSettingMissing);
            }
            int.TryParse(retriesSetting.Value, out int retries);

            var settings = new DefaultSettingValue
            {
                IsEnableAutoAllocation = true,
                AutoAllocationMethod = methodName,
                NearestAvailableMethodOrderCapacity = capacity,
                NearestAvailableMethodRadiusInKM = radius,
                NearestAvailableMethodNumberOfRetries = retries,
            };

            return settings;
        }
        private async Task SetDefaultBusinessSeedingAsync(ApprovedTenantDTO approveTenant, string tenantId)
        {
            var defaultBranch = SetDefaultResturant(approveTenant, tenantId);
            var addedAdmin = new Admin(tenantId)
            {
                Id = tenantId,
                CustomerCode = GenerateCustomCode(),
                CompanyAddress = approveTenant.TenantRegistration.OwnerAddressSummary,
                CompanyName = approveTenant.TenantRegistration.Name,
                ResidentCountryId = approveTenant.TenantRegistration.CountryId,
                Branch = defaultBranch
            };
            _adminRepository.Add(addedAdmin);

            await SetDefaultTeamAsync(approveTenant, tenantId);
        }
        private async Task SetDefaultTeamAsync(ApprovedTenantDTO approveTenant, string tenantId)
        {
            if (!approveTenant.IsFullVersion &&
                !approveTenant.SelectedModules.Contains((int)DefaultTenantModule.DriverModule))
            {
                var firstLocationAccuracy = await _locationAccuracyRepository.
                    GetAllIQueryable().
                    IgnoreQueryFilters().
                    Where(locationAccuracy =>
                    locationAccuracy.Name.ToLower().Trim() == "tracking")
                    .FirstOrDefaultAsync();

                if (firstLocationAccuracy == null)
                {
                    firstLocationAccuracy = new LocationAccuracy
                    {
                        Duration = 5000,
                        Name = "Tracking"
                    };
                    _teamRepository.Add(new Team(tenantId)
                    {
                        Name = $"{approveTenant.TenantRegistration.Name} Team",
                        LocationAccuracy = firstLocationAccuracy,
                    });

                }
                else
                {
                    _teamRepository.Add(new Team(tenantId)
                    {
                        Name = $"{approveTenant.TenantRegistration.Name} Team",
                        LocationAccuracyId = firstLocationAccuracy.Id,
                    });

                }

            }
        }
        private Branch SetDefaultResturant(ApprovedTenantDTO approveTenant, string tenantId)
        {
            Branch addedBranch = null;
            if (!approveTenant.IsFullVersion &&
                !approveTenant.SelectedModules.Contains((int)DefaultTenantModule.ResturantModule))
            {
                var addedGeofence = new GeoFence(tenantId)
                {
                    Name = $"{approveTenant.TenantRegistration.Name} Geofence",
                    Description = "",
                    GeoFenceLocations = new List<GeoFenceLocation>
                    {
                        new GeoFenceLocation
                        {
                            Latitude = approveTenant.TenantRegistration.Latitude,
                            Longitude = approveTenant.TenantRegistration.Longitude,
                            PointIndex = 1
                        }
                    }

                };

                var addedRestaurant = new Restaurant(tenantId)
                {
                    Name = $"{approveTenant.TenantRegistration.Name} Restaurant",
                    IsActive = true
                };

                addedBranch = new Branch(tenantId)
                {
                    Address = approveTenant.TenantRegistration.BusinessAddressSummary,
                    Location = approveTenant.TenantRegistration.BusinessAddress,
                    CountryId = approveTenant.TenantRegistration.CountryId,
                    Name = $"{approveTenant.TenantRegistration.Name} Branch",
                    IsActive = true,
                    Phone = string.IsNullOrEmpty(approveTenant.TenantRegistration.OwnerPhoneNumber) ? string.Empty :
                    approveTenant.TenantRegistration.OwnerPhoneNumber,
                    Latitude = approveTenant.TenantRegistration.Latitude,
                    Longitude = approveTenant.TenantRegistration.Longitude,
                };
                addedBranch.Restaurant = addedRestaurant;
                addedBranch.GeoFence = addedGeofence;
                _branchRepository.Add(addedBranch);
            }

            return addedBranch;
        }
        private string GenerateCustomCode()
        {
            return Guid.NewGuid().ToString();
        }

        private async Task SendResetPasswordEmailAsync(ApplicationUser user)
        {
            if (user != null)
            {
                var token = await _userDomainService.GeneratePasswordResetTokenAsync(user);
                token = WebUtility.UrlEncode(token);
                var resetUrl = string.Format(_emailSettings.ResetCallBackUrl, token, user.UserName);

                var body = string.Format(MailTemplates.ApproveTenantRegistration, user.FirstName, resetUrl, _emailSettings.SenderName);
                await _emailsender.SendEmailAsync($"{_emailSettings.SenderName} Approve Registration", body, user.Email);
            }
        }
        private void NotifyTenantAboutRejection(TenantRegistration tenantRegistration)
        {
            //todo: will be by email or SMS 
        }
        private async Task SetAddressSummaryAsync(TenantRegistration tenantRegistration)
        {
            (tenantRegistration.BusinessAddressSummaryEnglish, tenantRegistration.BusinessAddressSummaryArabic) =
                await _paciService.GetAddressSummaryAsync(tenantRegistration.BusinessAddress);
            (tenantRegistration.OwnerAddressSummaryEnglish, tenantRegistration.OwnerAddressSummaryArabic) =
                await _paciService.GetAddressSummaryAsync(tenantRegistration.OwnerAddress);
        }
        private async Task<List<TenantRegistrationAttachmentDTO>> UploadDriverAttachmentAsync(
        IEnumerable<TenantRegistrationAttachmentDTO> attachments)
        {
            foreach (var attachment in attachments)
            {
                var path = "DriverImages";
                var processResult = await _uploadFormFileService.UploadFileAsync(attachment.FormFile, path);
                if (!processResult.IsSucceeded)
                {
                    continue;
                }
                var fileName = $"{path}/{processResult.ReturnData}";
                attachment.File = fileName;
            }
            return attachments.ToList();
        }
        private List<string> GetTenantPermissions(ApprovedTenantDTO approveTenant)
        {
            List<string> permissions;
            if (approveTenant.IsFullVersion)
            {
                permissions = GetAllTenantPermissions();
            }
            else
            {
                permissions = GetSelectedTenantPermissions(approveTenant.SelectedModules);
            }

            return permissions;
        }
        private List<string> GetSelectedTenantPermissions(List<int> selectedModules)
        {
            List<string> features = new List<string>
            {
                PermissionFeatures.Task,
                PermissionFeatures.Customer,
                PermissionFeatures.Reports,
                PermissionFeatures.AccountLogs
            };
            var featurePermissions = TenantPermissions.FeaturePermissions.ToList();
            foreach (var module in selectedModules)
            {
                switch (module)
                {
                    case (int)DefaultTenantModule.DriverModule:
                        features.AddRange(new List<string> {
                            PermissionFeatures.Agent,
                            PermissionFeatures.Settings,
                            PermissionFeatures.Geofences,
                            PermissionFeatures.Roles,
                            PermissionFeatures.Teams
                        });
                        break;
                    case (int)DefaultTenantModule.ResturantModule:
                        features.AddRange(new List<string> {
                            PermissionFeatures.Restaurants,
                            PermissionFeatures.Branches,
                            PermissionFeatures.Geofences
                        });
                        break;
                    case (int)DefaultTenantModule.ManagerModule:
                        features.AddRange(new List<string> {
                            PermissionFeatures.Managers,
                            PermissionFeatures.Roles
                        });
                        break;
                }
            }
            if (selectedModules.Any(module => module == (int)DefaultTenantModule.ResturantModule) &&
                selectedModules.Any(module => module == (int)DefaultTenantModule.ManagerModule))
            {
                features.Add(PermissionFeatures.DispatchingManager);
            }
            if (!selectedModules.Any(module => module == (int)DefaultTenantModule.DriverModule))
            {
                features.Add(PermissionFeatures.PlatformAgent);
            }

            List<string> permissions = new List<string>();
            featurePermissions
                .Where(featurePermission => features.Any(builtInFeature => featurePermission.Name.ToLower().Trim().Contains(builtInFeature.ToLower().Trim())))
                .ToList()
                .ForEach(featurePermission =>
                {
                    permissions.AddRange(
                         featurePermission.Permissions.Select(permission => permission).ToList());
                });
            return permissions
                .Distinct()
                .ToList();
        }
        private List<string> GetAllTenantPermissions()
        {
            List<string> permissions = new List<string>();
            TenantPermissions.FeaturePermissions
                .Where(featurePermission => featurePermission.Name != PermissionFeatures.PlatformAgent)
                .ToList()
                .ForEach(featurePermission =>
                {
                    permissions.AddRange(
                         featurePermission.Permissions.Select(permission => permission).ToList());
                });
            return permissions
                .Distinct()
                .ToList();
        }

    }


}
