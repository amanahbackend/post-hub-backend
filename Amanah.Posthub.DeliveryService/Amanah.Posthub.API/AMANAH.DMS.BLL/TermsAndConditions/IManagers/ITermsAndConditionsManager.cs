﻿using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.TermsAndConditions.Entities;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface ITermsAndConditionsManager : IBaseManager<TermsAndConditionsViewModel, TermsAndConditions>
    {
        Task<TermsAndConditionsViewModel> GetDriverTermsAndConditionsAsync();
        Task<TermsAndConditionsViewModel> UpdateDriverTermsAndConditionsAsync(TermsAndConditionsViewModel termsAndConditionsViewModel);
        Task<byte[]> DowenloadDriverTermsAndConditionsAsPDFAsync();
        Task<string> GetDriverTermsAndConditionsContentAsync();


    }
}
