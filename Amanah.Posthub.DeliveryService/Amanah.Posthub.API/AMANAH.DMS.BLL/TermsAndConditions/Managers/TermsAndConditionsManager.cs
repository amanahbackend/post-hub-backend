﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.Service.Domain.TermsAndConditions.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Wkhtmltopdf.NetCore;

namespace Amanah.Posthub.BLL.Managers
{
    public class TermsAndConditionsManager : BaseManager<TermsAndConditionsViewModel, TermsAndConditions>, ITermsAndConditionsManager
    {
        private readonly IRepositry<TermsAndConditions> _repository;
        private readonly IMapper _mapper;
        private readonly IGeneratePdf _generatePdf;

        public TermsAndConditionsManager(
               ApplicationDbContext context,
           IMapper mapper,
           IRepositry<TermsAndConditions> repository, IGeneratePdf generatePdf)
           : base(context, repository, mapper)
        {
            _repository = repository;
            _mapper = mapper;
            _generatePdf = generatePdf;

        }


        public async Task<TermsAndConditionsViewModel> GetDriverTermsAndConditionsAsync()
        {
            var driverTermsAndConditions = await (this.context as ApplicationDbContext).TermsAndConditions
                 .Where(x => x.Type == TermsAndConditionsType.Driver)
                 .ProjectTo<TermsAndConditionsViewModel>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync();
            if (driverTermsAndConditions == null)
            {
                driverTermsAndConditions = new TermsAndConditionsViewModel();
            }
            return driverTermsAndConditions;
        }

        public async Task<TermsAndConditionsViewModel> UpdateDriverTermsAndConditionsAsync(TermsAndConditionsViewModel termsAndConditionsViewModel)
        {
            var driverTermsAndConditions = await GetDriverTermsAndConditionsAsync();
            TermsAndConditionsViewModel commandResult = new TermsAndConditionsViewModel();
            termsAndConditionsViewModel.Type = TermsAndConditionsType.Driver;
            if (driverTermsAndConditions == null)
            {
                commandResult = await base.AddAsync(termsAndConditionsViewModel);
            }
            else
            {
                await base.UpdateAsync(termsAndConditionsViewModel);
                commandResult = termsAndConditionsViewModel;
            }
            return commandResult;
        }

        public async Task<byte[]> DowenloadDriverTermsAndConditionsAsPDFAsync()
        {
            var driverTermsAndConditions = await GetDriverTermsAndConditionsAsync();
            string driverTermsAndConditionsInHtml = CultureInfo.CurrentCulture.Name == "en"
                ? driverTermsAndConditions.TermsAndConditionsInEnglish
                : driverTermsAndConditions.TermsAndConditionsInArabic;
            byte[] termsInPDF = _generatePdf.GetPDF(driverTermsAndConditionsInHtml);
            return termsInPDF;
        }

        public async Task<string> GetDriverTermsAndConditionsContentAsync()
        {
            var driverTermsAndConditions = await GetDriverTermsAndConditionsAsync();
            string driverTermsAndConditionsContent = CultureInfo.CurrentCulture.Name == "en"
                ? driverTermsAndConditions.TermsAndConditionsInEnglish
                : driverTermsAndConditions.TermsAndConditionsInArabic;
            return driverTermsAndConditionsContent;
        }
    }
}
