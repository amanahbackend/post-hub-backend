﻿using Amanah.Posthub.Service.Domain.TermsAndConditions.Entities;

namespace Amanah.Posthub.BLL
{
    public class TermsAndConditionsViewModel
    {
        public int Id { get; set; }
        public string TermsAndConditionsInEnglish { get; set; }
        public string TermsAndConditionsInArabic { get; set; }
        public TermsAndConditionsType Type { set; get; }
    }

}
