﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Service.Domain.Users.Entities;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IApplicationRoleManager
    {
        Task<List<string>> GetCurrentUserPermissionsAsync();
        Task<ApplicationRole> GetRoleAsyncByName(string roleName, RoleType type = RoleType.Manager);
        Task<ApplicationRole> GetRoleAsync(ApplicationRole role);
        Task<string> AddRoleAsync(ApplicationRole applicationRole);
        Task AddRolesAsync(List<ApplicationRole> applicationRoles);
        Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole);
        Task<string> AddRoleAsync(string roleName, RoleType type = RoleType.Manager);
        Task<bool> IsRoleExistAsync(string roleName);
        Task<bool> IsRoleExistAsync(string roleName, RoleType type = RoleType.Manager);

        Task<List<ApplicationRole>> GetAllRolesAsync(RoleType type = RoleType.Manager);
        Task<bool> DeleteRoleAsync(ApplicationRole applicationRole);
        Task<bool> UpdateRoleAsync(ApplicationRole applicationRole);
        Task<bool> AddClaimAsync(ApplicationRole role, Claim claim);
        Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName);
        Task<IList<Claim>> GetClaimsAsync(ApplicationRole role);
        List<string> GetAllRolesByUserId(string userId);
    }
}