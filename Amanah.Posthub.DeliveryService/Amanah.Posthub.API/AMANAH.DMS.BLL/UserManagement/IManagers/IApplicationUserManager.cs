﻿using Amanah.Posthub.BLL.ViewModels.Account;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IApplicationUserManager
    {
        Task<UserManagerResult> AddUserWithoutRolesAsync(ApplicationUser user, string password);
        Task AddUserToRolesAsync(ApplicationUser user);
        Task SendResetPasswordEmailAsync(string email);
        Task AddUsersAsync(List<Tuple<ApplicationUser, string>> users);
        Task<UserManagerResult> AddUserAsync(ApplicationUser user, string password);
        Task<UserManagerResult> AddUserAsync(ApplicationUser user);
        Task<UserManagerResult> AddTenantAsync(RegistrationViewModel model);
        Task<UserManagerResult> AddUserAsync(string email, string password);
        Task<LoginResult> LoginAsync(LoginViewModel model);
        Task LogoutAsync();
        Task<AgentLoginResult> DriverLoginAsync(AgentLoginViewModel model);
        Task<bool> UpdateUserAsync(ApplicationUser user);
        Task<IdentityResult> UpdateAsync(ApplicationUser user);
        Task<ApplicationUser> GetByEmailAsync(string email);
        Task<ApplicationUser> GetByUserNameAsync(string userName);
        Task<ApplicationUser> GetByUserIdAsync(string userId);
        Task<IList<Claim>> GetClaimsAsync(ApplicationUser user);
        Task<IList<string>> GetRolesAsync(string userName);
        Task<IList<string>> GetRolesAsync(ApplicationUser user);
        Task<bool> AddUserToRoleAsync(string userName, string roleName);
        Task<bool> AddUserToRoleAsync(ApplicationUser applicationUser, ApplicationRole applicationRole);
        Task<bool> IsUserNameExistAsync(string userName);
        Task<bool> IsUserNameExistAsync(string userName, string excludedUserId);
        Task<bool> IsEmailExistAsync(string email);
        Task<bool> DeleteAsync(ApplicationUser user);
        Task<bool> SoftDeleteAsync(ApplicationUser user);
        Task<ApplicationUser> GetAsync(ApplicationUser user);
        Task<List<ApplicationUser>> GetAll();
        List<ApplicationUser> GetByUserIds(List<string> userIds);
        List<ApplicationUser> GetAllExcept(List<string> userIds); Task<List<ApplicationUser>> GetAdmins(List<string> userIdsExclude = null);
        Task<List<ApplicationUser>> GetByRole(string roleName, List<string> userIdsExclude = null);
        Task<bool> ChangeUserAvailabiltyAsync(string userId, bool isAvailable);
        string HashPassword(ApplicationUser applicationUser, string newPassword);
        Task<bool> IsAllowToDeleteAdmin();
        Task<bool> IsPhoneExist(string Phone);
        Task<UserManagerResult> ForgotPassword(string email);
        Task<UserManagerResult> ResetPasswordAsync(string email, string token, string newPassword);
        Task<UserManagerResult> CheckResetToken(string email, string token);
        Task<ApplicationUser> GetByUserNameOrEmailAsync(string usernameOrEmail);
        Task<string> GeneratePasswordResetTokenAsync(string email);
        Task<bool> ChangePasswordAsync(ApplicationUser user, string newPassword);
        Task<bool> ChangePasswordAsync(ApplicationUser user, string oldPassword, string newPassword);
        Task<bool> ValidateUserByEmailAsync(string email);
        Task<bool> ValidateUserByUserNameAsync(string userName);
    }
}
