﻿using Amanah.Posthub.Service.Domain.Users.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.IManagers
{
    public interface IUserDeviceManager
    {
        Task AddIfNotExistAsync(UserDevice userDevice);
        bool SetUserDeviceLoggedIn(UserDevice userDevice);
        List<UserDevice> GetByUserId(string userId);
        bool DeleteDevice(string token);
    }
}
