﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class ApplicationRoleManager : IApplicationRoleManager
    {
        private readonly RoleManager<ApplicationRole> identityRoleManager;
        private readonly ApplicationDbContext _dbContext;
        private readonly ICurrentUser _currentUser;

        public ApplicationRoleManager(
            RoleManager<ApplicationRole> _identityRoleManager,
            ApplicationDbContext dbContext,
            ICurrentUser currentUser)
        {
            identityRoleManager = _identityRoleManager;
            _dbContext = dbContext;
            _currentUser = currentUser;
        }

        public async Task<ApplicationRole> GetRoleAsyncByName(string roleName, RoleType type = (RoleType)1)
        {
            return await _dbContext.Roles.SingleOrDefaultAsync(t => t.Name == roleName && t.Type == (int)type);
        }

        public async Task<ApplicationRole> GetRoleAsyncById(string Id)
        {
            return await _dbContext.Roles.SingleOrDefaultAsync(t => t.Id == Id);
        }
        public async Task<ApplicationRole> GetRoleAsync(ApplicationRole role)
        {
            ApplicationRole result = null;
            if (!string.IsNullOrEmpty(role.Name))
            {
                result = await GetRoleAsyncByName(role.Name);
            }
            if (result == null && !string.IsNullOrEmpty(role.Id))
            {
                result = await GetRoleAsyncById(role.Id);
            }
            return result;
        }
        public async Task<List<ApplicationRole>> GetAllRolesAsync(RoleType type = RoleType.Manager)
        {
            List<ApplicationRole> roles = await _dbContext.Roles
                    .Where(role => role.Type == (int)type)
                    .Where(role => role.Name.ToLower().Trim() != "SuperAdmin".ToLower())
                    .Include(x => x.RoleClaims)
                    .AsNoTracking()
                    .ToListAsync();
            List<string> notAllowedToAccessRoles = await _dbContext.UserRoles.Where(userRole => userRole.UserId == _currentUser.Id ||
                     userRole.UserId == _currentUser.TenantId).Select(userRole => userRole.RoleId)
                     .AsNoTracking()
                     .ToListAsync();

            return roles.Where(role => !notAllowedToAccessRoles.Contains(role.Id)).ToList();
        }

        public async Task<string> AddRoleAsync(ApplicationRole applicationRole)
        {
            IdentityResult result = await identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole.Name;
            }
            else
            {
                throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));
            }
        }
        public async Task AddRolesAsync(List<ApplicationRole> applicationRoles)
        {
            foreach (var role in applicationRoles)
            {
                await AddRoleAsync(role);
            }
        }
        public async Task<ApplicationRole> AddRoleAsyncronous(ApplicationRole applicationRole)
        {
            IdentityResult result = await identityRoleManager.CreateAsync(applicationRole);
            if (result.Succeeded)
            {
                return applicationRole;
            }
            else
            {
                throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));
            }
        }

        public async Task<bool> UpdateRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
            var oldRole = await GetRoleAsync(applicationRole);
            if (applicationRole != null)
            {
                oldRole.Name = applicationRole.Name;
                IdentityResult callBack = await identityRoleManager.UpdateAsync(oldRole);
                if (callBack.Succeeded)
                {
                    result = true;
                }
                else
                {
                    if (callBack.Errors.Count() > 0)
                        throw new Exception(string.Join(',', callBack.Errors.Select(t => t.Description)));
                    return false;
                }
            }
            return result;
        }

        public async Task<bool> DeleteRoleAsync(ApplicationRole applicationRole)
        {
            var result = false;
            var role = await GetRoleAsync(applicationRole);
            IdentityResult callBack = await identityRoleManager.DeleteAsync(role);
            if (callBack.Succeeded)
            {
                result = true;
            }
            else
            {
                if (callBack.Errors.Count() > 0)
                    throw new Exception(string.Join(',', callBack.Errors.Select(t => t.Description)));

                return false;
            }
            return result;
        }

        public async Task<string> AddRoleAsync(string roleName, RoleType type = RoleType.Manager)
        {
            ApplicationRole applicationRole = new ApplicationRole(roleName);
            applicationRole.Type = (int)type;
            return await AddRoleAsync(applicationRole);
        }

        public async Task<bool> IsRoleExistAsync(string roleName)
        {
            return await _dbContext.Roles.AnyAsync(t => t.Name == roleName);
        }

        public async Task<bool> IsRoleExistAsync(string roleName, RoleType type = RoleType.Manager)
        {
            return await _dbContext.Roles.AnyAsync(t => t.Name == roleName && t.Type == (int)type);
        }

        public async Task<bool> AddClaimAsync(ApplicationRole role, Claim claim)
        {
            var result = await identityRoleManager.AddClaimAsync(role, claim);
            if (result.Succeeded)
            {
                return true;
            }
            else
            {
                if (result.Errors.Count() > 0)
                    throw new Exception(string.Join(',', result.Errors.Select(t => t.Description)));

                return false;
            }
        }
        public async Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName)
        {
            var roleClaim = await _dbContext.RoleClaims
                .FirstOrDefaultAsync(t => t.ClaimValue == claimName && t.RoleId == role.Id);
            _dbContext.Remove(roleClaim);
            return true;
        }

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationRole role)
        {
            var claims = await _dbContext.RoleClaims.Where(t => t.RoleId == role.Id).Select(t => t.ToClaim()).ToListAsync();
            return claims;
        }

        public List<string> GetAllRolesByUserId(string userId)
        {
#warning cleanup - to be async, no need for 2 trips to db, use join instead
            var roleIds = _dbContext.UserRoles.Where(r => r.UserId == userId).Select(r => r.RoleId).ToList();
            return _dbContext.Roles.Where(r => roleIds.Contains(r.Id)).Select(r => r.Name).ToList();
        }

        public async Task<List<string>> GetCurrentUserPermissionsAsync()
        {
            var roleId = await _dbContext.UserRoles
                .Where(userRole => userRole.UserId == _currentUser.Id)
                .Select(userRole => userRole.RoleId)
                .FirstOrDefaultAsync();
            return await _dbContext.RoleClaims
                .Where(roleClaim => roleClaim.RoleId == roleId && roleClaim.ClaimType.Trim() == "permission")
                .Select(roleClaim => roleClaim.ClaimValue)
                .ToListAsync();
        }
    }
}
