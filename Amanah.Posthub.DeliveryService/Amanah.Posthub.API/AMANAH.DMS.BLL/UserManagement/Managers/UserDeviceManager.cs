﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.Managers
{
    public class UserDeviceManager : IUserDeviceManager
    {
        private readonly ApplicationDbContext _dbContext;
        public UserDeviceManager(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddIfNotExistAsync(UserDevice userDevice)
        {
            var isUserDeviceExist = await _dbContext.UserDevice
                .AnyAsync(userDeviceDB => userDeviceDB.UserId == userDeviceDB.UserId
                    && userDeviceDB.FCMDeviceId == userDeviceDB.FCMDeviceId);
            if (isUserDeviceExist)
            {
                _dbContext.UserDevice.Add(userDevice);
                await _dbContext.SaveChangesAsync();
            }
        }

        public bool SetUserDeviceLoggedIn(UserDevice userDevice)
        {
            var userDeviceFromDatabase = _dbContext.UserDevice
                .Where(x => x.UserId == userDevice.UserId && x.FCMDeviceId == userDevice.FCMDeviceId)
                .FirstOrDefault();
            if (userDeviceFromDatabase != null)
            {
                userDeviceFromDatabase.IsLoggedIn = userDevice.IsLoggedIn;
                _dbContext.SaveChanges();
                return true;
            }
            return false;
        }

        public List<UserDevice> GetByUserId(string userId)
        {
            return _dbContext.UserDevice
                 .Where(x => x.UserId == userId && !x.IsDeleted)
                 .ToList();
        }

        public bool DeleteDevice(string token)
        {
            var userDevice = _dbContext.UserDevice.FirstOrDefault(x => x.FCMDeviceId == token);
            if (userDevice != null)
            {
                _dbContext.Remove(userDevice);
                return _dbContext.SaveChanges() > 0;
            }
            return false;
        }
    }
}
