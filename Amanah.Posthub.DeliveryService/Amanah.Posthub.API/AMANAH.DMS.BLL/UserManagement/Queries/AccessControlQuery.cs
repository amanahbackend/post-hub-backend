﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.Queries.DTOs;
using Amanah.Posthub.BLL.UserManagement.Permissions;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Users.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.UserManagement.Queries
{
    public interface IAccessControlQuery
    {
        Task<List<AccessControlResultDTO>> GetAllAsync(RoleType type = RoleType.Manager);
        Task<List<FeaturePermissionResultDTO>> GetAllManagerPermissionsAsync();
        Task<List<FeaturePermissionResultDTO>> GetAllDriverPermissionsAsync();
        Task<AccessControlResultDTO> GetAsync(string roleName);
        Task<bool> IsRoleExistAsync(string roleName, RoleType type = 0);

    }
    internal class AccessControlQuery : IAccessControlQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;
        private readonly ILocalizer localizer;
        private readonly IRoleRepository roleRepository;

        public AccessControlQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser,
            ILocalizer localizer,
            IRoleRepository roleRepository)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
            this.localizer = localizer;
            this.roleRepository = roleRepository;
        }

        public Task<List<FeaturePermissionResultDTO>> GetAllManagerPermissionsAsync()
        {
            var UserPermissions = currentUser.UserPermissions;
            var allPermissions = currentUser.Scope == Scope.Platform
                ? PlatformPermissions.FeaturePermissions
                : TenantPermissions.FeaturePermissions;
            var result = allPermissions
                .Select(feature =>
                    new FeaturePermissionResultDTO
                    {
                        Name = localizer[feature.Name],
                        Permissions = feature.Permissions
                            .Intersect(UserPermissions)
                            .Select(permission =>
                                new PermissionResultDTO
                                {
                                    Value = permission,
                                    Name = localizer[permission]
                                })
                            .ToList()
                    })
                .Where(feature => feature.Permissions.Any())
                .ToList();
            return Task.FromResult(result);
        }
        public Task<List<FeaturePermissionResultDTO>> GetAllDriverPermissionsAsync()
        {
            var result = AgentPermissions.FeaturePermissions
              .Select(feature =>
                  new FeaturePermissionResultDTO
                  {
                      Name = localizer[feature.Name],
                      Permissions = feature.Permissions
                          .Select(permission =>
                              new PermissionResultDTO
                              {
                                  Value = permission,
                                  Name = localizer[permission]
                              })
                          .ToList()
                  })
              .ToList();
            return Task.FromResult(result);

        }
        public async Task<List<AccessControlResultDTO>> GetAllAsync(RoleType type = RoleType.Manager)
        {
            var query = await this.roleRepository.GetAllRolesWithClaimsAsQueryableAsync(type);
            var result = await query.ProjectTo<AccessControlResultDTO>(this.mapper.ConfigurationProvider).ToListAsync();
            return result;
        }
        public async Task<AccessControlResultDTO> GetAsync(string roleName)
        {
            var role = await this.roleRepository.GetAllIQueryable()
                                .Include(x => x.RoleClaims)
                                .Where(x => x.Name.ToLower() == roleName.ToLower())
                                .ProjectTo<AccessControlResultDTO>(this.mapper.ConfigurationProvider)
                                .FirstOrDefaultAsync();
            return role;
        }
        public async Task<bool> IsRoleExistAsync(string roleName, RoleType type = 0)
        {
            return await roleRepository.IsRoleExistAsync(roleName);
        }
    }
}
