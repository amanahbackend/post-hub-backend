﻿using System;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Queries.DTOs
{
    public class AccessControlResultDTO
    {
        public string Id { set; get; }
        public string RoleName { get; set; }
        public List<string> Permissions { get; set; }
        public DateTime? CreationDate { set; get; }

    }
}
