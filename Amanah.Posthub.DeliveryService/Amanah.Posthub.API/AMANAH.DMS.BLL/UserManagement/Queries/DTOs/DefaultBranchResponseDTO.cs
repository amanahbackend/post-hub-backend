﻿namespace Amanah.Posthub.BLL.UserManagement.Queries.DTOs
{
    public class DefaultBranchResponseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public string Phone { get; set; }
        public int RestaurantId { get; set; }
        public int GeoFenceId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? ManagerId { get; set; }
        public bool IsActive { get; set; }

        public DefaultCustomerResponseDTO Customer { set; get; }
    }

    public class DefaultCustomerResponseDTO
    {
        public int Id { set; get; }
        public int? BranchId { set; get; }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public string Address { set; get; }
        public double Latitude { set; get; }
        public double Longitude { set; get; }
        public string Tags { set; get; }
        public int? CountryId { set; get; }

        //public CountryViewModel Country { get; set; }
        //public AddressViewModel Location { set; get; }

    }

}
