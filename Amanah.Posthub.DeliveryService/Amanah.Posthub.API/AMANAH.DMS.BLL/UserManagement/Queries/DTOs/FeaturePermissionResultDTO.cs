﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.Queries.DTOs
{
    public class FeaturePermissionResultDTO
    {
        public string Name { get; set; }
        public List<PermissionResultDTO> Permissions { get; set; }
    }
}
