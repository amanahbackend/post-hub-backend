﻿namespace Amanah.Posthub.BLL.Queries.DTOs
{
    public class PermissionResultDTO
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
