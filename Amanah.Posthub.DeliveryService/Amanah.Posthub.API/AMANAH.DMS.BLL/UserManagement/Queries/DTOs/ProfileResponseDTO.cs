﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System;

namespace Amanah.Posthub.BLL.UserManagement.Queries.DTOs
{
    public class ProfileResponseDTO
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int NationalityId { get; set; }
        public string Nationality { get; set; }
        public string NationalId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public int CountryId { set; get; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public Address Address { set; get; }
        public string FullName { get; set; }
        public int DisplayImage { get; set; }
        public int ResidentCountryId { set; get; }
        public bool IsManager { set; get; }
        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        public int? BusinessCustomerId { get; set; }
        public int? BusinessConactAccountId { get; set; }
        public int? BusinessCustomerBranchId { set; get; }
        public string CompanyAddress { get; set; }
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; }
        public int? BranchId { set; get; }
        public string ProfilePhotoURL { get; set; }
        public string BusinessCID { set; get; }
        public bool IsBusinessCustomer { set; get; } = false;
    }
}
