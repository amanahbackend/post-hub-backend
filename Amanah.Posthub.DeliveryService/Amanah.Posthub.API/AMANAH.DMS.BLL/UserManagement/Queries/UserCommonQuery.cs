﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.UserManagement.Queries
{
    public interface IUserCommonQuery
    {
        Task<List<int>> GetManagerTeamsAsync();
        Task<List<int>> GetManagerRestaurantsAsync();
        Task<List<int>> GetManagerBranchesAsync();
        Task<IEnumerable<object>> GetBranchesAsync();
        Task<List<int>> GetManagerDriversAsync(IEnumerable<int> teamIds = null);

    }
    internal class UserCommonQuery : IUserCommonQuery
    {
        private readonly ApplicationDbContext context;
        private readonly ICurrentUser currentUser;

        public UserCommonQuery(
            ApplicationDbContext context,
            ICurrentUser currentUser)
        {
            this.context = context;
            this.currentUser = currentUser;
        }

        public async Task<List<int>> GetManagerTeamsAsync()
        {
            throw new System.NotImplementedException();
            if (string.IsNullOrEmpty(currentUser.Id))
            {
                return new List<int>();
            }
            var query = context.Teams.AsQueryable();
            if (!currentUser.HasPermission(TenantPermissions.Team.ReadTeam))
            {
                query = query.Where(team =>
                    team.CreatedBy_Id == currentUser.UserName ||
                    team.TeamManagers.Where(manager => manager.Manager.UserId == currentUser.Id).Count() > 0);
            }
            return await query.Select(team => team.Id).ToListAsync();
        }

        public async Task<List<int>> GetManagerRestaurantsAsync()
        {
            if (string.IsNullOrEmpty(currentUser.Id))
            {
                return new List<int>();
            }
            var restaurants = await context.ManagerDispatching
                .Include(dispatchingManager => dispatchingManager.Manager)
                .Where(dispatchingManager => dispatchingManager.Manager.UserId == currentUser.Id)
                .Select(dispatchingManager => dispatchingManager.Restaurants)
                .Distinct()
                .ToListAsync();

            var restaurantIds = await context.Restaurant
                .Where(restaurant => restaurant.CreatedBy_Id == currentUser.UserName
                 || restaurants.Contains("All"))
                .Select(resturant => resturant.Id).ToListAsync();

            if (restaurants != null && restaurants.Any())
            {
                if (restaurantIds == null)
                {
                    restaurantIds = new List<int>();
                }
                foreach (var restaurant in restaurants)
                {
                    if (restaurant.Trim() != "All")
                    {
                        restaurantIds.AddRange(restaurant.Split(',').Select(dispatchingRestaurant => int.Parse(dispatchingRestaurant)));
                    }
                }
            }



            return restaurantIds?.Distinct().ToList();
        }

        public async Task<List<int>> GetManagerBranchesAsync()
        {
            if (string.IsNullOrEmpty(currentUser.Id))
            {
                return new List<int>();
            }
            var branchesAndZones = await context.ManagerDispatching
                .Include(dispatchingManager => dispatchingManager.Manager)
                .Where(dispatchingManager => dispatchingManager.Manager.UserId == currentUser.Id || dispatchingManager.CreatedBy_Id == currentUser.UserName)
                .Select(dispatchingManager => new { dispatchingManager.Branches, dispatchingManager.Zones })
                .ToListAsync();
            var branches = branchesAndZones.Select(brancheAndZone => brancheAndZone.Branches);
            var zones = branchesAndZones.Select(brancheAndZone => brancheAndZone.Zones);

            var brancheIds = await context.Branch
                .Where(branch => branch.CreatedBy_Id == currentUser.UserName
                    || branches.Contains("All")
                    || zones.Contains(branch.GeoFenceId.ToString()))
                .Select(branch => branch.Id)
                .ToListAsync();

            if (branches != null && branches.Any())
            {
                if (brancheIds == null || !brancheIds.Any())
                {
                    brancheIds = new List<int>();
                }
                foreach (var branch in branches)
                {
                    if (branch.Trim() != "All")
                    {
                        brancheIds.AddRange(branch.Split(',').Select(dispatchingManagerBranch => int.Parse(dispatchingManagerBranch)));
                    }
                }
            }

            return brancheIds.Distinct().ToList();
        }

        public async Task<IEnumerable<object>> GetBranchesAsync()
        {
            var brancheIds = await context.Branch
                .IgnoreQueryFilters()
                .Distinct()
                .Select(b => new
                {
                    Id = b.Id,
                    Name = b.Name
                })
                .ToListAsync();

            return brancheIds;
        }

        public async Task<List<int>> GetManagerDriversAsync(IEnumerable<int> teamIds = null)
        {
            if (teamIds == null)
            {
                return null;
            }
            return await context.Driver
                .Where(x => teamIds.Contains(x.TeamId))
                .Select(x => x.Id)
                .ToListAsync();
        }

    }
}
