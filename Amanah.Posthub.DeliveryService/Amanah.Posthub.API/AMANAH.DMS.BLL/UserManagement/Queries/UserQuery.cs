﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.UserManagement.Queries.DTOs;
using Amanah.Posthub.Context;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.UserManagement.Queries
{
    public interface IUserQuery
    {
        Task<ProfileResponseDTO> GetUserProfileAsync();
        Task<DefaultBranchResponseDTO> GetUserDefaultBranchAsync();
    }
    public class UserQuery : IUserQuery
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ICurrentUser currentUser;

        public UserQuery(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser)
        {
            this.context = context;
            this.mapper = mapper;
            this.currentUser = currentUser;
        }

        public async Task<ProfileResponseDTO> GetUserProfileAsync()
        {
            ProfileResponseDTO userProfile = null;
            if (currentUser.IsDriver())
            {
                return null;
            }
            if (currentUser.IsManager())
            {


                userProfile = await context.Manager
                    .IgnoreQueryFilters()
                    .Include(c=>c.User)
                    .Where(manager => manager.UserId == currentUser.Id)
                    .ProjectTo<ProfileResponseDTO>(mapper.ConfigurationProvider)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();

                var admin = await context.Manager
                    .IgnoreQueryFilters()
                    .Where(admin => admin.Tenant_Id == currentUser.TenantId)
                    .FirstOrDefaultAsync();
                userProfile.CustomerCode = admin.UserId;// admin.CustomerCode;
                userProfile.ResidentCountryId = admin.NationalityId;
                userProfile.CompanyAddress = "";
                userProfile.CompanyName = admin.DepartmentId.ToString();
                //userProfile.DisplayImage = ;
                userProfile.DashBoardLanguage = "";
                userProfile.TrackingPanelLanguage = "";
                userProfile.BranchId = admin.BranchId;
                userProfile.FirstName = admin.User.FirstName;
                userProfile.LastName = admin.User.LastName;
                userProfile.MiddleName = admin.User.MiddleName;
                userProfile.IsManager = true;
            }
            else if (currentUser.IsBusinessCustomer())
            {
                ProfileResponseDTO bcustomerProfile = new ProfileResponseDTO();
                var bcustomercontact = await context
                    .BusinessConactAccounts
                    .Include(x => x.BusinessCustomer)
                    .ThenInclude(y=>y.HQBranch)
                    .Include(x => x.User)
                    .IgnoreQueryFilters()
                    .Where(x => x.UserID == currentUser.Id)
                    .FirstOrDefaultAsync();

                bcustomerProfile.Id = currentUser.Id;
                bcustomerProfile.FullName = bcustomercontact.User.FullName;
                bcustomerProfile.CompanyName = bcustomercontact.BusinessCustomer.BusinessName;
                bcustomerProfile.BusinessCustomerId = bcustomercontact.BusinessCustomerId;
                bcustomerProfile.BusinessConactAccountId = bcustomercontact.Id;
                bcustomerProfile.BusinessCustomerBranchId = bcustomercontact.BusinessCustomer.HQBranchId;
                bcustomerProfile.CountryId = bcustomercontact.User.CountryId;
                bcustomerProfile.Email = bcustomercontact.User.Email;
                bcustomerProfile.FirstName = bcustomercontact.User.FirstName;
                bcustomerProfile.MiddleName = bcustomercontact.User.MiddleName;
                bcustomerProfile.LastName = bcustomercontact.User.LastName;
                bcustomerProfile.MiddleName = bcustomercontact.User.MiddleName;
                bcustomerProfile.PhoneNumber = bcustomercontact.User.PhoneNumber;
                bcustomerProfile.MobileNumber = bcustomercontact.MobileNumber;
                bcustomerProfile.ProfilePhotoURL = $"BusinessContactProfile/" + (string.IsNullOrEmpty(bcustomercontact.PersonalPhotoURL) ? "avtar.jpeg" : bcustomercontact.PersonalPhotoURL);
                bcustomerProfile.Address = bcustomercontact.BusinessCustomer.HQBranch != null ? bcustomercontact.BusinessCustomer.HQBranch.Location : null;
                bcustomerProfile.IsManager = true;
                bcustomerProfile.BusinessCID = bcustomercontact.BusinessCustomer.BusinessCID;
                bcustomerProfile.IsBusinessCustomer = true;
                return bcustomerProfile;
            }
            else
            {
                return await (from admin in context.Admins.IgnoreQueryFilters()
                              join user in context.Users on admin.Id equals user.Id
                              join nationality in context.Country on admin.NationalityId equals nationality.Id

                              where admin.Id == currentUser.Id
                              select new ProfileResponseDTO
                              {
                                  Id = user.Id,
                                  Email = user.Email,
                                  FirstName = user.FirstName,
                                  MiddleName = user.MiddleName,
                                  LastName = user.LastName,
                                  FullName = $"{user.FirstName} {user.LastName}",
                                  PhoneNumber = user.PhoneNumber,
                                  CountryId = user.CountryId,
                                  NationalityId = admin.NationalityId,
                                  Nationality = nationality != null ? nationality.Name : null,
                                  NationalId = admin.NationalId,
                                  DateOfBirth = admin.DateOfBirth,
                                  MobileNumber = admin.MobileNumber,
                                  Address = admin.Address,
                                  CustomerCode = admin.CustomerCode,
                                  ResidentCountryId = admin.ResidentCountryId,
                                  CompanyAddress = admin.CompanyAddress,
                                  CompanyName = admin.CompanyName,
                                  DisplayImage = admin.DisplayImage,
                                  DashBoardLanguage = admin.DashBoardLanguage,
                                  TrackingPanelLanguage = admin.TrackingPanelLanguage,
                                  BranchId = admin.BranchId,
                                  ProfilePhotoURL = $"StaffImages/" + (string.IsNullOrEmpty(admin.ProfilePhotoURL) ? "avtar.jpeg" : admin.ProfilePhotoURL),

                              }).AsNoTracking().IgnoreQueryFilters()
                   .FirstOrDefaultAsync();



            }

            return userProfile;
        }

        public async Task<DefaultBranchResponseDTO> GetUserDefaultBranchAsync()
        {
            return await context.Admins
                .Where(admin => admin.Tenant_Id == currentUser.TenantId)
                .ProjectTo<DefaultBranchResponseDTO>(mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }
    }
}
