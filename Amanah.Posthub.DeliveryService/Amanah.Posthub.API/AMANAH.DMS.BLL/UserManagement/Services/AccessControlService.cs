﻿using Amanah.Posthub.BLL.UserManagement.Services.DTOs;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Service.Domain.Users.Entities;
using AutoMapper;
using System.Threading.Tasks;

namespace Amanah.Posthub.BLL.UserManagement.Services
{
    public interface IAccessControlService
    {
        public Task<bool> CreateAsync(CreateAccessControlInputDTO input, BASE.Authentication.RoleType type = 0);
        public Task<bool> DeleteAsync(string roleName);
        public Task<bool> UpdateAsync(UpdateAccessControlInputDTO input);
    }
    public class AccessControlService : IAccessControlService
    {
        private readonly IMapper mapper;
        private readonly IRoleDomainService roleDomainService;

        public AccessControlService(
            IMapper mapper,
            IRoleDomainService roleDomainService)
        {
            this.mapper = mapper;
            this.roleDomainService = roleDomainService;
        }

        public async Task<bool> CreateAsync(CreateAccessControlInputDTO input, BASE.Authentication.RoleType type = 0)
        {
            var applicationRole = mapper.Map<ApplicationRole>(input);
            var createdRole = await roleDomainService.CreateAsync(applicationRole, input.Permissions, type);
            if (createdRole == null) return false;

            return true;
        }

        public async Task<bool> DeleteAsync(string roleName)
        {
            return await roleDomainService.DeleteAsync(roleName);
        }

        public async Task<bool> UpdateAsync(UpdateAccessControlInputDTO input)
        {
            var applicationRole = mapper.Map<ApplicationRole>(input);
            var createdRole = await roleDomainService.UpdateAsync(applicationRole, input.Permissions);
            if (createdRole == null) return false;

            return true;
        }
    }

}
