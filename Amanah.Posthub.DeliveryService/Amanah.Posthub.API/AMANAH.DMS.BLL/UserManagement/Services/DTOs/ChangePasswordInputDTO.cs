﻿using Amanah.Posthub.VALIDATOR.Extensions;
using FluentValidation;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.UserManagement.Services.DTOs
{
    public class ChangePasswordInputDTO
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }

        public class ChangePasswordInputDTOValidator : AbstractValidator<ChangePasswordInputDTO>
        {
            public ChangePasswordInputDTOValidator(ILocalizer localizer)
            {
                RuleFor(x => x.NewPassword)
                    .Password(localizer);
                RuleFor(x => x.ConfirmPassword).Equal(x => x.NewPassword)
                    .When(x => !string.IsNullOrWhiteSpace(x.NewPassword))
                    .WithMessage(localizer[Keys.Validation.RetypePasswordMismatch]);

                RuleFor(x => x.NewPassword).Equal(x => x.CurrentPassword)
                    .WithMessage(localizer[Keys.Validation.NewPasswordIsSameAsOld]);
            }
        }
    }
}
