﻿using Amanah.Posthub.BASE.Authentication;
using System.Collections.Generic;

namespace Amanah.Posthub.BLL.UserManagement.Services.DTOs
{
    public class UpdateAccessControlInputDTO
    {
        public string RoleName { get; set; }
        public List<string> Permissions { get; set; }
        public RoleType Type { get; set; }

    }
}
