﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.UserManagement.Services.DTOs
{
    public class UpdateProfileInputDTO
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int NationalityId { get; set; }
        public string Nationality { get; set; }
        public string NationalId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public int CountryId { set; get; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public Address Address { set; get; }
        public string FullName { get; set; }
        public IFormFile ProfilePhotoFile { get; set; }
        public string ProfilePhotoURL { get; set; }

        public int DisplayImage { get; set; }
        public int ResidentCountryId { set; get; }
        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }

        public class UpdateProfileInputDTOValidator : AbstractValidator<UpdateProfileInputDTO>
        {
            private readonly IUserRepository userRepository;
            private readonly ICurrentUser currentUser;

            public UpdateProfileInputDTOValidator(IUserRepository userRepository,
                ICurrentUser currentUser,
                ILocalizer localizer)
            {
                this.userRepository = userRepository;
                this.currentUser = currentUser;
                RuleFor(profile => profile.Email)
                    .EmailAddress()
                    .NotEmpty()
                    .MustAsync(IsEmailExistAsync)
                    .WithMessage(localizer[Keys.Validation.EmailAlreadyExists]);
                RuleFor(profile => profile.Id)
                    .NotEmpty()
                    .NotNull();
                RuleFor(profile => profile.CountryId)
                    .NotEmpty()
                    .NotNull();
            }
            public async Task<bool> IsEmailExistAsync(string email, CancellationToken cancellationToken)
            {
                var user = await userRepository.GetAsync(user => currentUser.Id != user.Id
                    && user.NormalizedEmail == email.ToUpper().Trim());
                if (user == null)
                {
                    return true;
                }
                return false;
            }
        }
    }

    public class UpdateDefaultBranchInputDTO
    {
        public int DefaultBranchId { get; set; }
    }

    public class UpdateLanguageInputDTO
    {
        public string DashBoardLanguage { get; set; }
        // public string TrackingPanelLanguage { get; set; }

        public class UpdateLanguageInputDTOValidator : AbstractValidator<UpdateLanguageInputDTO>
        {
            public UpdateLanguageInputDTOValidator()
            {
                RuleFor(language => language.DashBoardLanguage)
                    .NotEmpty()
                    .NotNull();
                // RuleFor(x => x.TrackingPanelLanguage).NotEmpty().NotNull();
            }
        }
    }
}
