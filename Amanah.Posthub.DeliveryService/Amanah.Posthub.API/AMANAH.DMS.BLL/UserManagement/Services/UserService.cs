﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.UserManagement.Services.DTOs;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Service.Domain.Admins.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Utilites.UploadFile;

namespace Amanah.Posthub.BLL.UserManagement.Services
{
    public interface IUserService
    {
        Task UpdateMyProfileAsync(UpdateProfileInputDTO updateProfileInput);
        Task<bool> ChangeMyPasswordAsync(ChangePasswordInputDTO changePasswordInput);
        Task<bool> UpdateTenantDefaultBranchAsync(int branchId);
        Task<bool> UpdateTenantLanguageAsync(string language);
    }
    public class UserService : IUserService
    {
        private readonly IRepository<Admin, string> adminRepository;
        private readonly IRepository<Manager> managerRepository;
        private readonly IUserRepository userRepository;
        private readonly IUserDomainService userDomainService;
        private readonly IUnitOfWork unityOfWork;
        private readonly ICurrentUser currentUser;
        private readonly IUploadFormFileService uploadFormFileService;
        private readonly ApplicationDbContext context;

        public UserService(
            IRepository<Admin, string> adminRepository,
            IUserRepository userRepository,
            IUserDomainService userDomainService,
            IUnitOfWork unityOfWork,
            ICurrentUser currentUser,
            IUploadFormFileService uploadFormFileService,
            IRepository<Manager> managerRepository,
            ApplicationDbContext context)
        {
            this.adminRepository = adminRepository;
            this.userRepository = userRepository;
            this.userDomainService = userDomainService;
            this.unityOfWork = unityOfWork;
            this.currentUser = currentUser;
            this.managerRepository = managerRepository;
            this.uploadFormFileService = uploadFormFileService;
            this.context = context;
        }

        public async Task UpdateMyProfileAsync(UpdateProfileInputDTO updateProfileInput)
        {
            if (currentUser.IsDriver())
            {
                return;
            }

            await unityOfWork.RunTransaction(async () =>
            {
                if (currentUser.TenantId == currentUser.Id || string.IsNullOrEmpty(currentUser.TenantId))
                {
                    var existingAdmin = await adminRepository.GetFirstOrDefaultAsync(admin => admin.Id == currentUser.Id);

                    if (existingAdmin != null)
                    {
                        if (updateProfileInput.ProfilePhotoFile != null)
                        {
                            existingAdmin.ProfilePhotoURL = await SetManagerPhotoAsync(updateProfileInput.ProfilePhotoFile);
                        }

                        existingAdmin.Address = updateProfileInput.Address;
                        existingAdmin.DateOfBirth = updateProfileInput.DateOfBirth;
                        existingAdmin.MobileNumber = updateProfileInput.MobileNumber;
                        existingAdmin.NationalityId = updateProfileInput.NationalityId;
                        existingAdmin.NationalId = updateProfileInput.NationalId;

                        existingAdmin.CompanyName = updateProfileInput.CompanyName;
                        existingAdmin.CompanyAddress = updateProfileInput.CompanyAddress;
                        existingAdmin.ResidentCountryId = updateProfileInput.ResidentCountryId;
                        adminRepository.Update(existingAdmin);
                    }
                }
                else
                {
                    var existingManager = await managerRepository.GetFirstOrDefaultAsync(manager => manager.UserId == currentUser.Id);
                    if (existingManager != null)
                    {
                        if (updateProfileInput.ProfilePhotoFile != null)
                        {
                            existingManager.ProfilePicURL = await SetManagerPhotoAsync(updateProfileInput.ProfilePhotoFile);
                        }

                        existingManager.Address = updateProfileInput.Address;
                        existingManager.DateOfBirth = updateProfileInput.DateOfBirth;
                        existingManager.MobileNumber = updateProfileInput.MobileNumber;
                        existingManager.NationalityId = updateProfileInput.NationalityId;
                        existingManager.NationalId = updateProfileInput.NationalId;

                        managerRepository.Update(existingManager);
                    }
                }
                var existingUser = await userRepository.GetFirstOrDefaultAsync(user => user.Id == currentUser.Id);
                if (existingUser != null)
                {
                   
                    existingUser.FirstName = updateProfileInput.FirstName;
                    existingUser.MiddleName = updateProfileInput.MiddleName;
                    existingUser.LastName = updateProfileInput.LastName;
                    existingUser.PhoneNumber = updateProfileInput.PhoneNumber;
                    existingUser.CountryId = updateProfileInput.CountryId;
                    existingUser.Email = updateProfileInput.Email.Trim();
                    existingUser.NormalizedEmail = updateProfileInput.Email.ToUpper().Trim();
                    existingUser.UserName = updateProfileInput.Email.Trim();
                    existingUser.NormalizedUserName = updateProfileInput.Email.ToUpper().Trim();
                    userRepository.Update(existingUser);

                   
                }

                if (currentUser.IsBusinessCustomer())
                {
                    var bcustomercontact = await context
                        .BusinessConactAccounts
                        .Include(x => x.BusinessCustomer)
                        .ThenInclude(y => y.HQBranch)
                        .Include(x => x.User)
                        .IgnoreQueryFilters()
                        .Where(x => x.UserID == currentUser.Id)
                        .FirstOrDefaultAsync();

                    existingUser.FirstName = updateProfileInput.FirstName;
                    existingUser.MiddleName = updateProfileInput.MiddleName;
                    bcustomercontact.BusinessCustomer.HQBranch.Location = updateProfileInput.Address;

                    if (updateProfileInput.ProfilePhotoFile != null)
                        bcustomercontact.PersonalPhotoURL = await SetBusinessContactProfileAsync(updateProfileInput.ProfilePhotoFile);

                    if (!string.IsNullOrWhiteSpace(updateProfileInput.CompanyName.ToString()))
                        bcustomercontact.BusinessCustomer.BusinessName = updateProfileInput.CompanyName;

                    context.BusinessConactAccounts.Update(bcustomercontact);

                }
                await unityOfWork.SaveChangesAsync();
            });

        }

        public async Task<bool> ChangeMyPasswordAsync(ChangePasswordInputDTO changePasswordInput)
        {
            return await userDomainService
                  .ChangePasswordAsync(currentUser.Id, changePasswordInput.CurrentPassword, changePasswordInput.NewPassword);

        }
        private async Task<string> SetManagerPhotoAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "StaffImages";
                var processResult = await uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }

        private async Task<string> SetBusinessContactProfileAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "BusinessContactProfile";
                var processResult = await uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }


        public async Task<bool> UpdateTenantDefaultBranchAsync(int branchId)
        {
            if (currentUser.IsDriver() || currentUser.IsManager())
            {
                return false;
            }
            var existingAdmin = await adminRepository.GetFirstOrDefaultAsync(admin => admin.Id == currentUser.Id);
            if (existingAdmin != null)
            {
                existingAdmin.BranchId = branchId;
                adminRepository.Update(existingAdmin);
                await unityOfWork.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> UpdateTenantLanguageAsync(string language)
        {
            if (currentUser.IsDriver() || currentUser.IsManager())
            {
                return false;
            }
            var existingAdmin = await adminRepository.GetFirstOrDefaultAsync(admin => admin.Id == currentUser.Id, true);
            if (existingAdmin != null)
            {
                existingAdmin.DashBoardLanguage = language;
                adminRepository.Update(existingAdmin);
                await unityOfWork.SaveChangesAsync();

                return true;
            }
            return false;
        }
    }
}
