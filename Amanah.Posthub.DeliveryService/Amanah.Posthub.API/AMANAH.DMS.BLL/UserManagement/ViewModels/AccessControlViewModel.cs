﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class AccessControlViewModel
    {
        public string RoleName { get; set; }
        public List<string> Permissions { get; set; }
    }
}
