﻿namespace Amanah.Posthub.BLL.ViewModel
{
    public class AssignRoleViewModel
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }
}
