﻿using Amanah.Posthub.VALIDATOR.Extensions;
using FluentValidation;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class ChangePasswordViewModel
    {
        public string userId { get; set; }
        public string currentPassword { get; set; }
        public string newPassword { get; set; }
        public string confirmPassword { get; set; }
    }

    public class ChangePasswordViewModelValidator : AbstractValidator<ChangePasswordViewModel>
    {
        public ChangePasswordViewModelValidator(ILocalizer localizer)
        {
            RuleFor(x => x.newPassword).Password(localizer);
            RuleFor(x => x.confirmPassword).Equal(x => x.newPassword)
                .When(x => !string.IsNullOrWhiteSpace(x.newPassword))
                .WithMessage(localizer[Keys.Validation.RetypePasswordMismatch]);

        }
    }
}
