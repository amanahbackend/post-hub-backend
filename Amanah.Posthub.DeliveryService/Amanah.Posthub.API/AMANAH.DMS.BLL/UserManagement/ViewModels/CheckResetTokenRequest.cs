﻿namespace Amanah.Posthub.BLL.ViewModels.Account
{
    public class CheckResetTokenRequest
    {
        public string Email { get; set; }
        public string Token { get; set; }
    }
}
