﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.ViewModel
{
    public class EditApplicationUserViewModel
    {
        public string PhoneNumber { get; set; }
        public string Id { get; set; }
        public bool PhoneNumberConfirmed { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool IsAvailable { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FK_CreatedBy_Id { get; set; }
        public string UpdatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsLocked { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public DateTime DeletedDate { get; set; }
        [NotMapped]
        public List<string> RoleNames { get; set; }
    }
}
