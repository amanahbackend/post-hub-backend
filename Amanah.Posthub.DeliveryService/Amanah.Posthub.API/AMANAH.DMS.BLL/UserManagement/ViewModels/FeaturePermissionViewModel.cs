﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModel
{
    public class FeaturePermissionViewModel
    {
        public string Name { get; set; }
        public List<PermissionViewModel> Permissions { get; set; }
    }
}
