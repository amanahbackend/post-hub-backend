﻿namespace Amanah.Posthub.BLL.ViewModels.Account
{
    public class ForgotPasswordRequest
    {
        public string Email { get; set; }
    }
}
