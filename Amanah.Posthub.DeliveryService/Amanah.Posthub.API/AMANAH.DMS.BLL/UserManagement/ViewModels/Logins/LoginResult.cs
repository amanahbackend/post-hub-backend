﻿namespace Amanah.Posthub.BLL.ViewModels.Account
{
    public class LoginResult : UserManagerResult
    {
        public PosthubTokenResponse TokenResponse { get; set; }
    }

    public class PosthubTokenResponse
    {
        public string AccessToken { get; set; }
        public string TokenType { get; } = "Bearer";
    }
}
