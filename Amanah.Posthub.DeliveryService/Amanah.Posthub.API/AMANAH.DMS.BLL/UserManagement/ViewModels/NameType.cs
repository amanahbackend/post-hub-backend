﻿namespace Amanah.Posthub.BLL.UserManagement.ViewModels
{
    public enum NameType
    {
        First = 1,
        Middle = 2,
        Last = 3
    }
}
