﻿namespace Amanah.Posthub.BLL.ViewModel
{
    public class PermissionViewModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
