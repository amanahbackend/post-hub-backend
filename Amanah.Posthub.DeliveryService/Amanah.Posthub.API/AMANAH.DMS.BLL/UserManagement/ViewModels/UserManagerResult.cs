﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;

namespace Amanah.Posthub.BLL.ViewModels.Account
{
    public class UserManagerResult
    {
        public UserManagerResult()
        {
            Errors = new List<IdentityError>();
            Succeeded = true;
        }
        public string Error => string.Join("-", Errors.Select(x => x.Description));
        public ICollection<IdentityError> Errors { get; set; }

        private bool succeeded;
        public bool Succeeded
        {
            get => succeeded && !Errors.Any();
            set => succeeded = value;
        }
        public UserViewModel User { get; set; }


    }
}
