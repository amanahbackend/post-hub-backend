﻿using System.Collections.Generic;

namespace Amanah.Posthub.BLL.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int CountryId { set; get; }
        public int? UserType { get; set; }
        public List<string> RoleNames { get; set; }
        public List<string> Permissions { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }
}
