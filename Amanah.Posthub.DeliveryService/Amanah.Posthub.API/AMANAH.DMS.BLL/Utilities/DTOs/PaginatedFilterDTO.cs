﻿using System;

namespace Amanah.Posthub.BLL.Utilities.DTOs
{
    public class PaginatedFilterDTO
    {
        public int TotalNumbers { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string SearchBy { get; set; }

        public int Id { get; set; }
        public string UserID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
