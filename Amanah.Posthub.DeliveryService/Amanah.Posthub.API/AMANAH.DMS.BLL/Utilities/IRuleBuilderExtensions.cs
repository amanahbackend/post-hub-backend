﻿using FluentValidation;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.VALIDATOR.Extensions
{
    public static class RuleBuilderExtensions
    {
        public static IRuleBuilder<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder, ILocalizer localizer, int minimumLength = 8)
        {
            var options = ruleBuilder.NotEmpty()
                .MinimumLength(minimumLength).WithMessage(localizer[Keys.Validation.ShortPassowrd])
                .Matches("[A-Z]").WithMessage(localizer[Keys.Validation.PasswordHasNoUppercaseLetter])
                .Matches("[a-z]").WithMessage(localizer[Keys.Validation.PasswordHasNoLowercaseLetter])
                .Matches("[0-9]").WithMessage(localizer[Keys.Validation.PasswordHasNoDigit])
                .Matches("[!@#$%^&*]").WithMessage(localizer[Keys.Validation.PasswordHasNoSpecialCharacter]);

            return options;
        }
    }
}
