﻿using System;

namespace Amanah.Posthub.BLL.ViewModel
{
    public class BaseViewModel
    {
        public string Tenant_Id { get; set; }

        public string CreatedBy_Id { get; set; }

        public string UpdatedBy_Id { get; set; }

        public string DeletedBy_Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime DeletedDate { get; set; }


    }
}