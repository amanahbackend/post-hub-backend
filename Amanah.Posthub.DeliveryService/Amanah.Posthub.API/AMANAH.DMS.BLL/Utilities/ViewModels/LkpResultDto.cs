﻿namespace Amanah.Posthub.BLL.ViewModels
{
    public class LkpResultDto
    {
        public int Id { set; get; }
        public string Name { set; get; }
    }

    public class TaskStatusResultDto : LkpResultDto
    {
        public string Color { set; get; }
    }
}
