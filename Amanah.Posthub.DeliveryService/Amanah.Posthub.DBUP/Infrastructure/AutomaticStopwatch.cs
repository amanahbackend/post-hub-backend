﻿using System;
using System.Diagnostics;

namespace Posthub.DBUP.Infrastructure
{
    class AutomaticStopwatch : IDisposable
    {
        private readonly Stopwatch _stopwatch;

        public AutomaticStopwatch()
        {
            _stopwatch = new Stopwatch();
            _stopwatch.Start();
        }

        public void Dispose()
        {
            _stopwatch.Stop();
            Console.WriteLine($"Operation completed in {_stopwatch.Elapsed}");
        }
    }
}
