﻿using Amanah.Posthub.BASE.Authentication;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Posthub.DBUP.Infrastructure.CurentUser
{
    internal static class Extensions
    {
        public static IServiceCollection AddMigratorCurrentUser(this IServiceCollection services)
        {
            services.AddSingleton<ICurrentUser, MigratorCurrentUser>();
            return services;
        }

        private class MigratorCurrentUser : ICurrentUser
        {
            public bool? IsAuthenticated => false;
            public string Id => null;
            public int? DriverId => null;
            public int? BusinessCustomerId => null;
            public string UserName => "DbMigrator";
            public RoleType? UserType => null;
            public string Scope => string.Empty;
            public IReadOnlyCollection<string> UserPermissions => Array.Empty<string>();
            public string TenantId => null;

        }
    }
}
