﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Posthub.DBUP.Infrastructure
{
    public static class Extensions
    {
        public static string GetDatabaseName(this DbContext dbContext)
        {
            return dbContext.Database.GetDbConnection().Database;
        }

        public static string GenerateBackupFileName(this DbContext dbContext)
        {
            var formatedDateTime = DateTime.UtcNow.ToString("yyyyMMdd_HH-mm-ss_ffff");
            var backupFilePath = $@"d:\Posthub\automated-migration-backups\{dbContext.GetDatabaseName()}_{formatedDateTime}.bak";
            return backupFilePath;
        }
    }
}
