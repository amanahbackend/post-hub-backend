﻿/*
 *          During the Migration, we created a new db from scratch that is respecting the ef model including
 *              seeds for lookups(types, statuses), Area, Country
 *          Then old data is migrated to the new database excluding seeded data before, some data was out of integrity and
 *              some foreign keys was not respected the following scripts used to clean those invalid data
 *
 */
-- ---
-- update all notifications FromUserId, ToUserId to contain user id instead of username
-- ---
update  Notifications set FromUserId = u.Id from Notifications n join AspNetUsers u on n.FromUserId = u.UserName;
GO
update Notifications set ToUserId = u.Id from Notifications n join AspNetUsers u on n.ToUserId = u.UserName;
GO
-- ---
-- remove all notifications that has FromUserId or ToUserId null
-- ---
delete n from Notifications n left join AspNetUsers u on n.FromUserId = u.Id where u.Id is null;
GO
delete n from Notifications n left join AspNetUsers u on n.ToUserId = u.Id where u.Id is null;
GO

-- ---
-- delete account logs that has tenant_Id null
-- ---
delete al from AccountLogs al left join AspNetUsers u on al.Tenant_Id = u.Id where u.Id is null;

-- ---
-- users that has not valid country set to kuwait
-- ---
update AspNetUsers set CountryId = 121 from AspNetUsers u left join Country c on u.CountryId = c.Id where c.Id is null;

-- ---
-- delete any role that has not existing tenant
-- ---

delete r from AspNetRoles r left join AspNetUsers u on r.Tenant_Id = u.Id where u.Id is null and r.Tenant_Id is not null;

-- ---
-- delete any user role that has null user or role
-- ---
delete ur from AspNetUserRoles ur left join AspNetRoles r on ur.RoleId = r.Id left join AspNetUsers u on ur.UserId = u.Id where r.Id is null or u.Id is null;

-- ---
-- delete any role claim that has null role
-- ---
delete rc from AspNetRoleClaims rc left join AspNetRoles r on rc.RoleId = r.Id where r.Id is null;

-- ---
-- delete any user claim that has null user
-- ---
delete uc from AspNetUserClaims uc left join AspNetUsers u on uc.UserId = u.Id where u.Id is null;

-- ---
-- delete any setting that has null or whitespace key
-- ---
delete from Settings where ltrim(rtrim(settingKey)) = '' or settingKey is null;


/*
---------------------
--Important Scripts used during migration--
---------------------
-- remove foreign key constraints check , useful before using SSIEW
EXEC sp_msforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all'

-- restror foreign key constraints check
EXEC sp_msforeachtable 'ALTER TABLE ? CHECK CONSTRAINT all'
-- ---

-- Setting database online and offline
alter database [YourDBName] 
set offline with rollback immediate

alter database [YourDBName] 
set online
-- ---

-- another way to force disconneting active connections, NOTE: you will need to call SET MULTI_USER later
-- to know if db is single or multi user use => SELECT user_access_desc FROM sys.databases WHERE name = 'YourDBName'
SET SINGLE_USER
--This rolls back all uncommitted transactions in the db.
WITH ROLLBACK IMMEDIATE
GO

-----------------------------------------------
-- Best way to kill all connections database -- https://stackoverflow.com/questions/7197574/script-to-kill-all-connections-to-a-database-more-than-restricted-user-rollback
-----------------------------------------------


--list all tables with count of rows in each table
CREATE TABLE #counts
(
    table_name varchar(255),
    row_count int
)
EXEC sp_MSForEachTable @command1='INSERT #counts (table_name, row_count) SELECT ''?'', COUNT(*) FROM ?'
SELECT table_name, row_count FROM #counts ORDER BY table_name, row_count DESC
DROP TABLE #counts


*/