﻿using Amanah.Posthub.Context;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Posthub.DBUP.Infrastructure
{
    // Logger should not show all information from entity framework unless there is a parameter passed for that (verbose)
    // Logger should show our steps, show error details if there is any error
    //backup, restore should be done using sqlconnection (ado.net) to close connection to db
    // add a parameter to allow generating script instead of direct applying migration(low priority)
    internal class Migrator
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<Migrator> _logger;

        public Migrator(ApplicationDbContext dbContext, ILogger<Migrator> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<bool> MigrateAsync()
        {
            if (await DatabaseExists())
            {
                var pendingMigrations = await _dbContext.Database.GetPendingMigrationsAsync();
                if (!pendingMigrations.Any())
                {
                    _logger.LogInformation("No pending migrations.");
                    return true;
                }
                var backupFilePath = _dbContext.GenerateBackupFileName();
                await BackupDatabaseAsync(backupFilePath);
                try
                {
                    await ApplyPendingMigrations(pendingMigrations);
                    return true;
                }
                catch (DbException exception)
                {
                    _logger.LogError(exception, "Failed to apply migrations.");
                    await RestoreDatabaseAsync(backupFilePath);
                }
            }
            else
            {
                await CreateDatabase();
                return true;
            }
            return false;
        }

        private async Task<bool> DatabaseExists()
        {
            _logger.LogTrace($"Connecting to DB {_dbContext.GetDatabaseName()} ...");
            var databaseExists = await _dbContext.Database.CanConnectAsync();
            if (databaseExists)
            {
                _logger.LogInformation($"Connected to DB {_dbContext.GetDatabaseName()} successfully.");
            }
            return databaseExists;
        }

        private async Task CreateDatabase()
        {
            _logger.LogTrace("Creating new DB from scratch ...");
            await _dbContext.Database.MigrateAsync();
            _logger.LogInformation("Database Created Successfully");
        }

        private async Task ApplyPendingMigrations(IEnumerable<string> pendingMigrations)
        {
            using (new AutomaticStopwatch())
            {
                var pendingMigrationsString = pendingMigrations.Prepend("Pending Migrations are")
                    .Join(Environment.NewLine);
                _logger.LogInformation(pendingMigrationsString);
                var migrator = _dbContext.Database.GetService<IMigrator>();
                string previousMigration = (await _dbContext.Database.GetAppliedMigrationsAsync())
                    .LastOrDefault(); ;
                foreach (var pendingMiration in pendingMigrations)
                {
                    using (new AutomaticStopwatch())
                    {
                        _logger.LogDebug($"Applying Migration {pendingMiration}");
#if DEBUG
                        var script = migrator.GenerateScript(
                            fromMigration: previousMigration,
                            toMigration: pendingMiration);
#endif

                        await migrator.MigrateAsync(pendingMiration);
                        previousMigration = pendingMiration;
                    }
                }
                _logger.LogInformation($"All Migrations applied Successfully.");
            }
        }

        private async Task BackupDatabaseAsync(string backupFilePath)
        {
            using (new AutomaticStopwatch())
            {
                _logger.LogTrace("Starting creating a new DB Backup.");
                using var conneciton = new SqlConnection(GetMasterConnectionString());
                await conneciton.OpenAsync();
                using var command = new SqlCommand(GetBackupDatabaseSql(backupFilePath), conneciton);
                var result = await command.ExecuteNonQueryAsync();
                _logger.LogTrace($"DB Backup was created Successfully.");
            }
        }

        private async Task RestoreDatabaseAsync(string backupFilePath)
        {
            using (new AutomaticStopwatch())
            {
                _logger.LogTrace("Starting restoring database backup.");

                using var connection = new SqlConnection(GetMasterConnectionString());
                await connection.OpenAsync();
                var scripts = new[]
                {
                    GetKillConnectionsToDatabaseSql(),
                    GetRestoreDatabaseSql(backupFilePath)
                };
                using var command = new SqlCommand()
                {
                    Connection = connection
                };
                foreach (var script in scripts)
                {
                    command.CommandText = script;
                    var result = await command.ExecuteNonQueryAsync();
                }
                _logger.LogTrace($"Backup restored successfully");
            }
        }

        private string GetMasterConnectionString()
        {
            var connectionStringBuilder = new SqlConnectionStringBuilder(
                _dbContext.Database.GetDbConnection().ConnectionString);
            connectionStringBuilder.InitialCatalog = "Master";
            return connectionStringBuilder.ToString();
        }

        private string GetKillConnectionsToDatabaseSql()
        {
            var sql = @$"
-- kill all active connecitons to db
USE [master];

DECLARE @kill varchar(8000) = '';  
SELECT @kill = @kill + 'kill ' + CONVERT(varchar(5), session_id) + ';'  
FROM sys.dm_exec_sessions
WHERE database_id  = db_id('{_dbContext.GetDatabaseName()}')

EXEC(@kill);
";
            return sql;

        }

        private string GetBackupDatabaseSql(string backupFilePath)
        {
            return @$"
BACKUP DATABASE {_dbContext.GetDatabaseName()}
TO DISK = '{backupFilePath}';
";
        }

        private string GetRestoreDatabaseSql(string backupFilePath)
        {
            var sql = @$"
RESTORE DATABASE {_dbContext.GetDatabaseName()}
    FROM DISK = '{backupFilePath}'
    WITH FILE = 1 ;
";
            return sql;
        }
    }
}
