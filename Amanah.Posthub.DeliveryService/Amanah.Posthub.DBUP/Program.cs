﻿using Amanah.Posthub.Context;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Posthub.DBUP.Infrastructure;
using Posthub.DBUP.Infrastructure.CurentUser;
using System;
using System.Configuration;
using System.Linq;

namespace LIMS.DBup
{
    class Program
    {

        static int Main(string[] args)
        {
            var connectionString = args.FirstOrDefault()
                ?? ConfigurationManager.ConnectionStrings["PosthubDBConnection"].ConnectionString;
            using var serviceProvider = CreateServiceProvider(connectionString);
            var migrator = serviceProvider.GetRequiredService<Migrator>();
            var logger = serviceProvider.GetRequiredService<ILogger<Program>>();
            var result = false;
            try
            {
                result = migrator.MigrateAsync().GetAwaiter().GetResult();
            }
            catch (Exception exception)
            {
                logger.LogCritical(exception, "Unhandled Exception");
            }
            return result
                ? 0
                : -1;
        }

        private static ServiceProvider CreateServiceProvider(string connectionString)
        {
            var services = new ServiceCollection();
            services
                .AddDbContext<ApplicationDbContext>(options =>
                {
                    var timeoutSeconds = (int)TimeSpan.FromMinutes(1).TotalSeconds;
                    var connectionStringbBuilder = new SqlConnectionStringBuilder(connectionString);
                    connectionStringbBuilder.ConnectTimeout = timeoutSeconds;
                    var customizedConnectionString = connectionStringbBuilder.ToString();
                    options.UseSqlServer(
                        connectionString,
                        sqlOptions =>
                        {
                            sqlOptions.CommandTimeout(timeoutSeconds)
                                .EnableRetryOnFailure(10);
                        });
                })
                .AddLogging(builder =>
                {
                    builder
                        .AddFilter("Microsoft", LogLevel.None)
                        .AddFilter("System", LogLevel.None)
                        .AddFilter("Posthub", LogLevel.Trace)
                        .AddConsole();
                })
                .AddSingleton<Migrator>()
                .AddMigratorCurrentUser();
            return services.BuildServiceProvider();
        }
    }
}
