
CREATE TABLE dbo.Country
	(
	Id int IDENTITY (1, 1) NOT NULL,
	Name nvarchar(100) NOT NULL,
	Code nvarchar(50)  NULL,
	Flag tinyint NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Country ADD CONSTRAINT
	PK_Country PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.Country SET (LOCK_ESCALATION = TABLE)
GO
