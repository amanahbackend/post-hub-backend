
CREATE TABLE dbo.Driver
	(
	Id int IDENTITY (1, 1) NOT NULL,
	ImageUrl nvarchar(max) NULL,
	Tags nvarchar(max) NULL,
	TransportDescription nvarchar(max) NULL,
	LicensePlate nvarchar(max) NULL,
	Color nvarchar(max) NULL,
	Role nvarchar(max) NULL,
	TeamId int NOT NULL,
	AgentTypeId int  NULL,
	TransportTypeId int  NULL,
	CountryId int  NULL,
	UserId [nvarchar](450)  NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Driver ADD CONSTRAINT
	PK_Driver PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


GO
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_AgentType FOREIGN KEY
	(AgentTypeId) REFERENCES dbo.AgentType (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO	
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_Team FOREIGN KEY
	(TeamId) REFERENCES dbo.Teams (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 

GO	
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_TransportType FOREIGN KEY
	(TransportTypeId) REFERENCES dbo.TransportType (Id)
	ON UPDATE  NO ACTION 
	ON DELETE  NO ACTION 

GO	
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_Country FOREIGN KEY
	(CountryId) REFERENCES dbo.Country (Id)
	ON UPDATE  NO ACTION 
	ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Driver ADD CONSTRAINT
	FK_Driver_User FOREIGN KEY
	(UserId) REFERENCES dbo.AspNetUsers (Id)
	ON UPDATE  NO ACTION 
	ON DELETE  NO ACTION 
GO

ALTER TABLE dbo.Driver SET (LOCK_ESCALATION = TABLE)
GO

