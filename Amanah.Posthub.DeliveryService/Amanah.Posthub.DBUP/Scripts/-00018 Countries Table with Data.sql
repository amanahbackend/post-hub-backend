
ALTER TABLE [dbo].[Driver] DROP CONSTRAINT [FK_Driver_Country]
GO



/****** Object:  Table [dbo].[Country]    Script Date: 3/10/2020 11:56:33 AM ******/
DROP TABLE [dbo].[Country]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 3/10/2020 11:56:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](50) NULL,
	[Flag] [nvarchar](450) NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	[TopLevel] [nvarchar](50) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Country] ON 
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (10, N'Afghanistan', N'93', N'af.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'af')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (11, N'Albania', N'355', N'al.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'al')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (12, N'Algeria', N'213', N'dz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'dz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (14, N'Andorra', N'376', N'ad.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ad')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (15, N'Angola', N'244', N'ao.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ao')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (17, N'Antarctica', N'672', N'aq.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'aq')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (19, N'Argentina', N'54', N'ar.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ar')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (20, N'Armenia', N'374', N'am.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'am')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (21, N'Aruba', N'297', N'aw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'aw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (22, N'Australia', N'61', N'au.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'au')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (23, N'Austria', N'43', N'at.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'at')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (24, N'Azerbaijan', N'994', N'az.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'az')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (26, N'Bahrain', N'973', N'bh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (27, N'Bangladesh', N'880', N'bd.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bd')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (29, N'Belarus', N'375', N'by.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'by')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (30, N'Belgium', N'32', N'be.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'be')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (31, N'Belize', N'501', N'bz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (32, N'Benin', N'229', N'bj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (34, N'Bhutan', N'975', N'bt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (35, N'Bolivia', N'591', N'bo.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bo')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (36, N'Bosnia and Herzegovina', N'387', N'ba.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ba')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (37, N'Botswana', N'267', N'bw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (38, N'Brazil', N'55', N'br.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'br')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (39, N'British Indian Ocean Territory', N'246', N'io.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'io')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (41, N'Brunei', N'673', N'bn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (42, N'Bulgaria', N'359', N'bg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (43, N'Burkina Faso', N'226', N'bf.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bf')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (44, N'Burundi', N'257', N'bi.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'bi')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (45, N'Cambodia', N'855', N'kh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'kh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (46, N'Cameroon', N'237', N'cm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (47, N'Canada', N'1', N'ca.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ca')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (48, N'Cape Verde', N'238', N'cv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (50, N'Central African Republic', N'236', N'cf.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cf')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (51, N'Chad', N'235', N'td.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'td')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (52, N'Chile', N'56', N'cl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (53, N'China', N'86', N'cn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (54, N'Christmas Island', N'61', N'cx.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cx')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (55, N'Cocos Islands', N'61', N'cc.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cc')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (56, N'Colombia', N'57', N'co.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'co')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (57, N'Comoros', N'269', N'km.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'km')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (58, N'Cook Islands', N'682', N'ck.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ck')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (59, N'Costa Rica', N'506', N'cr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (60, N'Croatia', N'385', N'hr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'hr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (61, N'Cuba', N'53', N'cu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (62, N'Curacao', N'599', N'cw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (63, N'Cyprus', N'357', N'cy.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cy')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (64, N'Czech Republic', N'420', N'cz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (65, N'Democratic Republic of the Congo', N'243', N'cd.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cd')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (66, N'Denmark', N'45', N'dk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'dk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (67, N'Djibouti', N'253', N'dj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'dj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (70, N'East Timor', N'670', N'tl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (71, N'Ecuador', N'593', N'ec.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ec')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (72, N'Egypt', N'20', N'eg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'eg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (73, N'El Salvador', N'503', N'sv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (74, N'Equatorial Guinea', N'240', N'gq.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gq')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (75, N'Eritrea', N'291', N'er.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'er')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (76, N'Estonia', N'372', N'ee.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ee')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (77, N'Ethiopia', N'251', N'et.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'et')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (78, N'Falkland Islands', N'500', N'fk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'fk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (79, N'Faroe Islands', N'298', N'fo.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'fo')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (80, N'Fiji', N'679', N'fj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'fj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (81, N'Finland', N'358', N'fi.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'fi')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (82, N'France', N'33', N'fr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'fr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (83, N'French Polynesia', N'689', N'pf.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pf')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (84, N'Gabon', N'241', N'ga.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ga')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (85, N'Gambia', N'220', N'gm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (86, N'Georgia', N'995', N'ge.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ge')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (87, N'Germany', N'49', N'de.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'de')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (88, N'Ghana', N'233', N'gh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (89, N'Gibraltar', N'350', N'gi.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gi')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (90, N'Greece', N'30', N'gr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (91, N'Greenland', N'299', N'gl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (94, N'Guatemala', N'502', N'gt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (96, N'Guinea', N'224', N'gn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (97, N'Guinea-Bissau', N'245', N'gw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (98, N'Guyana', N'592', N'gy.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gy')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (99, N'Haiti', N'509', N'ht.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ht')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (100, N'Honduras', N'504', N'hn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'hn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (101, N'Hong Kong', N'852', N'hk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'hk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (102, N'Hungary', N'36', N'hu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'hu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (103, N'Iceland', N'354', N'is.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'is')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (104, N'India', N'91', N'in.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'in')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (105, N'Indonesia', N'62', N'id.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'id')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (106, N'Iran', N'98', N'ir.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ir')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (107, N'Iraq', N'964', N'iq.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'iq')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (108, N'Ireland', N'353', N'ie.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ie')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (110, N'Israel', N'972', N'il.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'il')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (111, N'Italy', N'39', N'it.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'it')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (112, N'Ivory Coast', N'225', N'ci.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ci')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (114, N'Japan', N'81', N'jp.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'jp')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (116, N'Jordan', N'962', N'jo.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'jo')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (117, N'Kazakhstan', N'7', N'kz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'kz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (118, N'Kenya', N'254', N'ke.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ke')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (119, N'Kiribati', N'686', N'ki.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ki')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (121, N'Kuwait', N'965', N'kw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'kw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (122, N'Kyrgyzstan', N'996', N'kg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'kg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (123, N'Laos', N'856', N'la.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'la')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (124, N'Latvia', N'371', N'lv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'lv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (125, N'Lebanon', N'961', N'lb.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'lb')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (126, N'Lesotho', N'266', N'ls.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ls')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (127, N'Liberia', N'231', N'lr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'lr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (128, N'Libya', N'218', N'ly.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ly')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (129, N'Liechtenstein', N'423', N'li.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'li')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (130, N'Lithuania', N'370', N'lt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'lt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (131, N'Luxembourg', N'352', N'lu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'lu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (132, N'Macau', N'853', N'mo.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mo')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (133, N'Macedonia', N'389', N'mk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (134, N'Madagascar', N'261', N'mg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (135, N'Malawi', N'265', N'mw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (136, N'Malaysia', N'60', N'my.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'my')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (137, N'Maldives', N'960', N'mv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (138, N'Mali', N'223', N'ml.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ml')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (139, N'Malta', N'356', N'mt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (140, N'Marshall Islands', N'692', N'mh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (141, N'Mauritania', N'222', N'mr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (142, N'Mauritius', N'230', N'mu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (143, N'Mayotte', N'262', N'yt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'yt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (144, N'Mexico', N'52', N'mx.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mx')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (145, N'Micronesia', N'691', N'fm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'fm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (146, N'Moldova', N'373', N'md.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'md')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (147, N'Monaco', N'377', N'mc.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mc')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (148, N'Mongolia', N'976', N'mn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (149, N'Montenegro', N'382', N'me.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'me')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (151, N'Morocco', N'212', N'ma.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ma')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (152, N'Mozambique', N'258', N'mz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (153, N'Myanmar', N'95', N'mm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'mm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (154, N'Namibia', N'264', N'na.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'na')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (155, N'Nauru', N'674', N'nr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'nr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (156, N'Nepal', N'977', N'np.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'np')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (157, N'Netherlands', N'31', N'nl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'nl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (158, N'Netherlands Antilles', N'599', N'an.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'an')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (159, N'New Caledonia', N'687', N'nc.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'nc')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (160, N'New Zealand', N'64', N'nz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'nz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (161, N'Nicaragua', N'505', N'ni.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ni')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (162, N'Niger', N'227', N'ne.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ne')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (163, N'Nigeria', N'234', N'ng.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ng')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (164, N'Niue', N'683', N'nu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'nu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (165, N'North Korea', N'850', N'kp.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'kp')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (167, N'Norway', N'47', N'no.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'no')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (168, N'Oman', N'968', N'om.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'om')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (169, N'Pakistan', N'92', N'pk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (170, N'Palau', N'680', N'pw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (171, N'Palestine', N'970', N'ps.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ps')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (172, N'Panama', N'507', N'pa.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pa')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (173, N'Papua New Guinea', N'675', N'pg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (174, N'Paraguay', N'595', N'py.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'py')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (175, N'Peru', N'51', N'pe.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pe')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (176, N'Philippines', N'63', N'ph.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ph')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (177, N'Pitcairn', N'64', N'pn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (178, N'Poland', N'48', N'pl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (179, N'Portugal', N'351', N'pt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (181, N'Qatar', N'974', N'qa.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'qa')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (182, N'Republic of the Congo', N'242', N'cg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'cg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (183, N'Reunion', N'262', N're.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N're')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (184, N'Romania', N'40', N'ro.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ro')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (185, N'Russia', N'7', N'ru.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ru')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (186, N'Rwanda', N'250', N'rw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'rw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (187, N'Saint Barthelemy', N'590', N'gp.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gp')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (188, N'Saint Helena', N'290', N'sh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (191, N'Saint Martin', N'590', N'gp.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'gp')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (192, N'Saint Pierre and Miquelon', N'508', N'pm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'pm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (194, N'Samoa', N'685', N'ws.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ws')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (195, N'San Marino', N'378', N'sm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (196, N'Sao Tome and Principe', N'239', N'st.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'st')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (197, N'Saudi Arabia', N'966', N'sa.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sa')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (198, N'Senegal', N'221', N'sn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (199, N'Serbia', N'381', N'rs.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'rs')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (200, N'Seychelles', N'248', N'sc.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sc')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (201, N'Sierra Leone', N'232', N'sl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (202, N'Singapore', N'65', N'sg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (204, N'Slovakia', N'421', N'sk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (205, N'Slovenia', N'386', N'si.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'si')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (206, N'Solomon Islands', N'677', N'sb.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sb')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (207, N'Somalia', N'252', N'so.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'so')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (208, N'South Africa', N'27', N'za.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'za')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (209, N'South Korea', N'82', N'kr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'kr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (210, N'South Sudan', N'211', N'ss.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ss')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (211, N'Spain', N'34', N'es.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'es')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (212, N'Sri Lanka', N'94', N'lk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'lk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (213, N'Sudan', N'249', N'sd.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sd')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (214, N'Suriname', N'597', N'sr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (215, N'Svalbard and Jan Mayen', N'47', N'sj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (216, N'Swaziland', N'268', N'sz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (217, N'Sweden', N'46', N'se.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'se')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (218, N'Switzerland', N'41', N'ch.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ch')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (219, N'Syria', N'963', N'sy.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'sy')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (220, N'Taiwan', N'886', N'tw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (221, N'Tajikistan', N'992', N'tj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (222, N'Tanzania', N'255', N'tz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (223, N'Thailand', N'66', N'th.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'th')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (224, N'Togo', N'228', N'tg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (225, N'Tokelau', N'690', N'tk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (226, N'Tonga', N'676', N'to.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'to')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (228, N'Tunisia', N'216', N'tn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (229, N'Turkey', N'90', N'tr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (230, N'Turkmenistan', N'993', N'tm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (232, N'Tuvalu', N'688', N'tv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'tv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (234, N'Uganda', N'256', N'ug.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ug')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (235, N'Ukraine', N'380', N'ua.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ua')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (236, N'United Arab Emirates', N'971', N'ae.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ae')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (237, N'United Kingdom', N'44', N'uk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'uk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (238, N'United States', N'1', N'us.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'us')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (239, N'Uruguay', N'598', N'uy.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'uy')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (240, N'Uzbekistan', N'998', N'uz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'uz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (241, N'Vanuatu', N'678', N'vu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'vu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (242, N'Vatican', N'379', N'va.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'va')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (243, N'Venezuela', N'58', N've.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N've')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (244, N'Vietnam', N'84', N'vn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'vn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (245, N'Wallis and Futuna', N'681', N'wf.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'wf')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (246, N'Western Sahara', N'212', N'eh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'eh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (247, N'Yemen', N'967', N'ye.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'ye')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (248, N'Zambia', N'260', N'zm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'zm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Tenant_Id], [TopLevel]) VALUES (249, N'Zimbabwe', N'263', N'zw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10T11:27:12.8100000' AS DateTime2), NULL, N'zw')
GO
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

