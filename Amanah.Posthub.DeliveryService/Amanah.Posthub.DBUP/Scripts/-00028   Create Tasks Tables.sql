BEGIN TRANSACTION
GO

CREATE TABLE dbo.MainTaskType
	(
	Id int IDENTITY (1, 1) NOT NULL,
	Name [nvarchar](max) NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE dbo.MainTaskType ADD CONSTRAINT
	PK_MainTaskType PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO





ALTER TABLE dbo.MainTaskType SET (LOCK_ESCALATION = TABLE)

CREATE TABLE dbo.MainTask
	(
	Id int IDENTITY (1, 1) NOT NULL,
	MainTaskTypeId int NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.MainTask ADD CONSTRAINT
	PK_MainTask PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE dbo.MainTask ADD CONSTRAINT
	FK_MainTaskType_MainTask FOREIGN KEY
	(MainTaskTypeId) REFERENCES dbo.MainTaskType (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO

ALTER TABLE dbo.MainTask SET (LOCK_ESCALATION = TABLE)
GO

CREATE TABLE dbo.TaskType
	(
	Id int IDENTITY (1, 1) NOT NULL,
	Name [nvarchar](max) NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO


ALTER TABLE dbo.TaskType ADD CONSTRAINT
	PK_TaskType PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


ALTER TABLE dbo.TaskType SET (LOCK_ESCALATION = TABLE)
GO

CREATE TABLE dbo.TaskStatus
	(
	Id int IDENTITY (1, 1) NOT NULL,
	Name [nvarchar](max) NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO


ALTER TABLE dbo.TaskStatus ADD CONSTRAINT
	PK_TaskStatus PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE dbo.TaskStatus SET (LOCK_ESCALATION = TABLE)
GO

CREATE TABLE dbo.Tasks
	(
	Id int IDENTITY (1, 1) NOT NULL,
	MainTaskId int NOT NULL,
	TaskTypeId int NOT NULL,
	CustomerId int NOT NULL,
	TaskStatusId int NOT NULL,
	OrderId [nvarchar](max) NULL,
	Address [ntext] NOT NULL,
	Latitude [nvarchar](max) NULL,
	Longitude [nvarchar](max) NULL,
	Description [ntext] NOT NULL,
	Image [nvarchar](max) NULL,
	PickupDate [datetime2](7) Null,
	DeliveryDate [datetime2](7) Null,
	DriverId int NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tasks ADD CONSTRAINT
	PK_Tasks PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Tasks ADD CONSTRAINT
	FK_Tasks_MainTask FOREIGN KEY
	(MainTaskId) REFERENCES dbo.MainTask (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Tasks ADD CONSTRAINT
	FK_Tasks_TaskType FOREIGN KEY
	(TaskTypeId) REFERENCES dbo.TaskType (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Tasks ADD CONSTRAINT
	FK_Tasks_Customer FOREIGN KEY
	(CustomerId) REFERENCES dbo.Customers (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Tasks WITH NOCHECK ADD CONSTRAINT
	FK_Tasks_Driver FOREIGN KEY
	(DriverId) REFERENCES dbo.Driver (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Tasks ADD CONSTRAINT
	FK_Tasks_TaskStatus FOREIGN KEY
	(TaskStatusId) REFERENCES dbo.TaskStatus (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.Tasks SET (LOCK_ESCALATION = TABLE)
GO

CREATE TABLE dbo.TaskHistory
	(
	Id int IDENTITY (1, 1) NOT NULL,
	TaskId int NOT NULL,
	FromStatusId int  NULL,
	ToStatusId int  NULL,
	Description [ntext] NOT NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[DeletedDate] [datetime2](7) NOT NULL,
	[Tenant_Id] [nvarchar](450) NULL,
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.TaskHistory ADD CONSTRAINT
	PK_TaskHistory PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.TaskHistory ADD CONSTRAINT
	FK_Task_TaskHistory FOREIGN KEY
	(TaskId) REFERENCES dbo.Tasks (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.TaskHistory WITH NOCHECK ADD CONSTRAINT
	FK_FromStatus_TaskHistory FOREIGN KEY
	(FromStatusId) REFERENCES dbo.TaskStatus (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.TaskHistory WITH NOCHECK ADD CONSTRAINT
	FK_ToStatus_TaskHistory FOREIGN KEY
	(ToStatusId) REFERENCES dbo.TaskStatus (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.TaskHistory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
