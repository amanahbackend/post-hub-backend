BEGIN TRANSACTION
GO

ALTER TABLE TaskStatus
ADD Color [nvarchar](max) NULL;
GO

ALTER TABLE TaskHistory
ADD Reason [ntext] NULL;
GO

ALTER TABLE TaskHistory
ADD MainTaskId int NOT NULL;
GO

ALTER TABLE dbo.TaskHistory ADD CONSTRAINT
	FK_TaskHistory_MainTask FOREIGN KEY
	(MainTaskId) REFERENCES dbo.MainTask (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO

COMMIT
