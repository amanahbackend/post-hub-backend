BEGIN TRANSACTION
GO

ALTER TABLE dbo.Tasks 
ADD 
Notes [nvarchar](max) NULL,
SignatureFileName [nvarchar](max) NULL,
SignatureURL [nvarchar](max) NULL;
GO


COMMIT
