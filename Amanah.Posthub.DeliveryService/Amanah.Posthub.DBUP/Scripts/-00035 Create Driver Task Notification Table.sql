CREATE TABLE [dbo].[DriverTaskNotification] (
    [Id]           INT IDENTITY (1, 1) NOT NULL,
    [DriverId]     INT NOT NULL,
    [TaskId]     INT NOT NULL,
    [Description]  NTEXT NULL,
    [IsRead]  bit NULL default 0,
    [CreatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [DeletedBy_Id] NVARCHAR (MAX) NULL,
    [Tenant_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]    BIT            NOT NULL,
    [CreatedDate]  DATETIME2 (7)  NULL,
    [UpdatedDate]  DATETIME2 (7)  NULL,
    [DeletedDate]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_DriverTaskNotification] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

ALTER TABLE dbo.DriverTaskNotification ADD CONSTRAINT
	FK_Driver_DriverTaskNotification FOREIGN KEY
	(DriverId) REFERENCES dbo.Driver (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO

ALTER TABLE dbo.DriverTaskNotification ADD CONSTRAINT
	FK_Task_DriverTaskNotification FOREIGN KEY
	(TaskId) REFERENCES dbo.Tasks (Id)
	ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO



