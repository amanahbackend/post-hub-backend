BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DriverCurrentLocation
	DROP CONSTRAINT FK_DriverCurrentLocation_DriverCurrentLocation
GO
ALTER TABLE dbo.Driver SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_DriverCurrentLocation
	(
	DriverId int NOT NULL,
	Latitude float(53) NOT NULL,
	Longitude float(53) NOT NULL,
	CreatedBy_Id nvarchar(MAX) NULL,
	DeletedBy_Id nvarchar(MAX) NULL,
	UpdatedBy_Id nvarchar(MAX) NULL,
	IsDeleted bit NOT NULL,
	UpdatedDate datetime2(7) NOT NULL,
	CreatedDate datetime2(7) NOT NULL,
	DeletedDate datetime2(7) NOT NULL,
	Tenant_Id nvarchar(450) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_DriverCurrentLocation SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.DriverCurrentLocation)
	 EXEC('INSERT INTO dbo.Tmp_DriverCurrentLocation (DriverId, Latitude, Longitude, CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, IsDeleted, UpdatedDate, CreatedDate, DeletedDate, Tenant_Id)
		SELECT DriverId, CONVERT(float(53), Latitude), CONVERT(float(53), Longitude), CreatedBy_Id, DeletedBy_Id, UpdatedBy_Id, IsDeleted, UpdatedDate, CreatedDate, DeletedDate, Tenant_Id FROM dbo.DriverCurrentLocation WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.DriverCurrentLocation
GO
EXECUTE sp_rename N'dbo.Tmp_DriverCurrentLocation', N'DriverCurrentLocation', 'OBJECT' 
GO
ALTER TABLE dbo.DriverCurrentLocation ADD CONSTRAINT
	PK_DriverCurrentLocation PRIMARY KEY CLUSTERED 
	(
	DriverId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.DriverCurrentLocation ADD CONSTRAINT
	FK_DriverCurrentLocation_DriverCurrentLocation FOREIGN KEY
	(
	DriverId
	) REFERENCES dbo.Driver
	(
	Id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
