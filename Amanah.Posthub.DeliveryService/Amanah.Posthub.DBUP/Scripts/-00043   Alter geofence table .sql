
ALTER TABLE [dbo].[GeoFenceLocation] DROP CONSTRAINT [FK_GeoFenceLocation_GeoFence]
GO

/****** Object:  Table [dbo].[GeoFenceLocation]    Script Date: 4/12/2020 4:46:35 PM ******/
DROP TABLE [dbo].[GeoFenceLocation]
GO

/****** Object:  Table [dbo].[GeoFenceLocation]    Script Date: 4/12/2020 4:46:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GeoFenceLocation](
	[GeoFenceId] [int] NOT NULL,
	[Longitude] [nvarchar](100) NOT NULL,
	[Latitude] [nvarchar](100) NOT NULL,
	[Id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_GeoFenceLocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[GeoFenceLocation]  WITH CHECK ADD  CONSTRAINT [FK_GeoFenceLocation_GeoFence] FOREIGN KEY([GeoFenceId])
REFERENCES [dbo].[GeoFence] ([Id])
GO

ALTER TABLE [dbo].[GeoFenceLocation] CHECK CONSTRAINT [FK_GeoFenceLocation_GeoFence]
GO
