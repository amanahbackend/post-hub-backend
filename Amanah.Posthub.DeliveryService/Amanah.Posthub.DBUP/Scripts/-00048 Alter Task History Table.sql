BEGIN TRANSACTION
GO

ALTER TABLE [dbo].[TaskHistory]
DROP COLUMN [Description];
GO

ALTER TABLE [dbo].[TaskHistory]
ADD [ActionName] [nvarchar](50) NULL;
GO

ALTER TABLE [dbo].[TaskHistory]
ADD [Latitude]  float NULL;
GO

ALTER TABLE [dbo].[TaskHistory]
ADD [Longitude] float NULL;
GO

ALTER TABLE [dbo].[Tasks]
ADD [SuccessfulDate] datetime2(7) NULL;
GO

ALTER TABLE [dbo].[Tasks]
ADD [TotalTime] float NULL;
GO

COMMIT