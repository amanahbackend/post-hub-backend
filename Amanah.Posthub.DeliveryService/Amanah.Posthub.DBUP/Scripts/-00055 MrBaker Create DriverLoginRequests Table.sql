BEGIN TRANSACTION
GO

CREATE TABLE [dbo].[DriverLoginRequests] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Status]   TINYINT        NOT NULL,
    [Token]    NVARCHAR (MAX) NULL,
    [DriverId] INT            NOT NULL,
    [CreatedBy_Id] NVARCHAR (MAX) NULL,
    [UpdatedBy_Id] NVARCHAR (MAX) NULL,
    [DeletedBy_Id] NVARCHAR (MAX) NULL,
    [Tenant_Id]    NVARCHAR (MAX) NULL,
    [IsDeleted]    BIT            NOT NULL,
    [CreatedDate]  DATETIME2 (7)  NULL,
    [UpdatedDate]  DATETIME2 (7)  NULL,
    [DeletedDate]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_DriverLoginRequests] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DriverLoginRequests_Driver] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[Driver] ([Id])
);
GO



GO

COMMIT
