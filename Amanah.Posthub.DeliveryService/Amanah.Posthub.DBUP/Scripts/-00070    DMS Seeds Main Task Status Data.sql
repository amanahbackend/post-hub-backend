INSERT [dbo].[MainTaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Assigned',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[MainTaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Inprogress',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[MainTaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Successful',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[MainTaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Failed',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO

INSERT [dbo].[MainTaskStatus] 
([Name],  [CreatedDate], [DeletedDate], [IsDeleted],  [UpdatedDate], [CreatedBy_Id], [DeletedBy_Id], [Tenant_Id], [UpdatedBy_Id]) VALUES 
(N'Accepted',  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0,  CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, NULL)
GO
