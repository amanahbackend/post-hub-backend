BEGIN TRANSACTION
GO


CREATE TABLE [dbo].[DriverloginTracking] (
    [Id]                INT IDENTITY (1, 1) NOT NULL,
    [DriverId]          INT NOT NULL,
    [LoginDate]         DATETIME2 (7)   NULL,
    [LoginLatitude]     float  NULL,
	[LoginLongitude]    float  NULL,
    [LogoutDate]        DATETIME2 (7)   NULL,
    [LogoutLatitude]    float  NULL,
	[LogoutLongitude]   float  NULL,
    [LogoutActionBy]    NVARCHAR (MAX)  NULL,
    [CreatedBy_Id]      NVARCHAR (MAX) NULL,
    [UpdatedBy_Id]      NVARCHAR (MAX) NULL,
    [DeletedBy_Id]      NVARCHAR (MAX) NULL,
    [Tenant_Id]         NVARCHAR (MAX) NULL,
    [IsDeleted]         BIT            NOT NULL,
    [CreatedDate]       DATETIME2 (7)  NULL,
    [UpdatedDate]       DATETIME2 (7)  NULL,
    [DeletedDate]       DATETIME2 (7)  NULL,
    CONSTRAINT [PK_DriverloginTracking] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DriverloginTracking_Driver] FOREIGN KEY ([DriverId]) REFERENCES [dbo].[Driver] ([Id])
);
GO




COMMIT