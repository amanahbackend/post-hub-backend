/****** Object:  Table [dbo].[MapRequestLog]    Script Date: 9/9/2020 3:52:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MapRequestLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Provider] [nvarchar](100) NULL,
	[StackTrace] [nvarchar](max) NULL,
	[ApiKey] [nvarchar](100) NULL,
	[Request] [nvarchar](max) NULL,
	[Response] [nvarchar](max) NULL,
	[Success] [bit] NULL,
	[Error] [nvarchar](50) NULL,
	[ErrorMessage] [nvarchar](max) NULL,
    [CreatedBy_Id]      NVARCHAR (MAX) NULL,
    [UpdatedBy_Id]      NVARCHAR (MAX) NULL,
    [DeletedBy_Id]      NVARCHAR (MAX) NULL,
    [Tenant_Id]         NVARCHAR (MAX) NULL,
    [IsDeleted]         BIT            NOT NULL,
    [CreatedDate]       DATETIME2 (7)  NULL,
    [UpdatedDate]       DATETIME2 (7)  NULL,
    [DeletedDate]       DATETIME2 (7)  NULL,
 CONSTRAINT [PK_MapRequestLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

