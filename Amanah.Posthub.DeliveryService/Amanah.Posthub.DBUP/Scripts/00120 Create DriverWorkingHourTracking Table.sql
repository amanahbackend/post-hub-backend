Begin Transaction

/****** Object:  Table [dbo].[DriverWorkingHourTracking]    Script Date: 2/14/2021 3:19:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DriverWorkingHourTracking](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DriverId] [int] NOT NULL,
	[Date] [datetime2](7) NULL,
	[ActionName] [nvarchar](max) NULL,
	[ActionBy] [nvarchar](max) NULL,
	[Notes] nvarchar(max) NULL,
	[CreatedBy_Id] [nvarchar](max) NULL,
	[UpdatedBy_Id] [nvarchar](max) NULL,
	[DeletedBy_Id] [nvarchar](max) NULL,
	[Tenant_Id] [varchar](36) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[DeletedDate] [datetime2](7) NULL,
 CONSTRAINT [PK_DriverWorkingHourTracking] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[DriverWorkingHourTracking]  WITH CHECK ADD  CONSTRAINT [FK_DriverWorkingHourTracking_Driver] FOREIGN KEY([DriverId])
REFERENCES [dbo].[Driver] ([Id])
GO

ALTER TABLE [dbo].[DriverWorkingHourTracking] CHECK CONSTRAINT [FK_DriverWorkingHourTracking_Driver]
GO

commit