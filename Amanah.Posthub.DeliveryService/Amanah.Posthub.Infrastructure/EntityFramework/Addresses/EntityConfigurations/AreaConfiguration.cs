﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.Addresses.EntityConfigurations
{
    internal class AreaConfiguration : IEntityTypeConfiguration<Area>
    {
        public void Configure(EntityTypeBuilder<Area> builder)
        {
            builder.HasOne(x => x.Governrate)
                .WithMany(x => x.Areas)
                .HasForeignKey(x => x.FK_Governrate_Id);
        }
    }
}
