﻿using Amanah.Posthub.DATA.DbContext;
using Amanah.Posthub.Service.Domain.Admins.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.Admins.EntityConfigurations
{
    public class AdminConfiguration : IEntityTypeConfiguration<Admin>
    {
        public void Configure(EntityTypeBuilder<Admin> builder)
        {
            builder.Property(_ => _.Id)
                .HasMaxLength(StringLengths.StringId)
                .IsUnicode(false);

            builder.OwnsOne(x => x.Address);

            builder.HasOne(admin => admin.Branch)
                .WithMany()
                .HasForeignKey(admin => admin.BranchId)
                .OnDelete(DeleteBehavior.Restrict);


        }
    }
}
