﻿using Amanah.Posthub.DATA.DbContext;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class BranchConfiguration : IEntityTypeConfiguration<Branch>
    {


        public void Configure(EntityTypeBuilder<Branch> builder)
        {
            builder.Property(o => o.Name)
                .HasMaxLength(StringLengths.Default)
                .IsRequired();

            builder.Property(o => o.Phone)
                .HasMaxLength(StringLengths.Phone)
                .IsRequired();

            builder.OwnsOne(x => x.Location);

            builder.Property(o => o.Address)
                .HasMaxLength(StringLengths.Default)
                .IsRequired();

            builder.Property(o => o.Latitude)
                .IsRequired();

            builder.Property(o => o.Longitude)
                .IsRequired();

            builder.HasOne(branch => branch.Country)
                .WithMany()
                .HasForeignKey(branch => branch.CountryId)
                .OnDelete(DeleteBehavior.Restrict);

            //builder.HasOne(branch => branch.GeoFence)
            //    .WithMany()
            //    .HasForeignKey(branch => branch.GeoFenceId)
            //    .OnDelete(DeleteBehavior.Restrict);

            //builder.HasOne(branch => branch.Restaurant)
            //    .WithMany(r => r.Branchs)
            //    .HasForeignKey(branch => branch.RestaurantId)
            //    .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(branch => branch.Company)
                .WithMany(x => x.Branches)
                .HasForeignKey(branch => branch.CompanyId)
                .OnDelete(DeleteBehavior.Restrict);


        }
    }
}
