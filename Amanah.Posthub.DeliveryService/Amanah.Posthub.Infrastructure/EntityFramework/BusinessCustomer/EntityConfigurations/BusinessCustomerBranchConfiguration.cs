﻿using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class BusinessCustomerBranchConfiguration : IEntityTypeConfiguration<BusinessCustomerBranch>
    {


        public void Configure(EntityTypeBuilder<BusinessCustomerBranch> builder)
        {
            builder.OwnsOne(x => x.Location);
        }
    }
}
