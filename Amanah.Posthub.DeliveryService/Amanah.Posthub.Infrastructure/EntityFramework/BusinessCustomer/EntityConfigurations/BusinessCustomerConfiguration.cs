﻿using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class BusinessCustomerConfiguration : IEntityTypeConfiguration<BusinessCustomer>
    {


        public void Configure(EntityTypeBuilder<BusinessCustomer> builder)
        {
            builder.HasMany(x => x.BusinessCustomerContacts)
                .WithOne(x => x.BusinessCustomer)
                .HasForeignKey(x => x.BusinessCustomerId);

            builder.HasMany(x => x.BusinessCustomerBranches)
                .WithOne(x => x.BusinessCustomer)
                .HasForeignKey(x => x.BusinessCustomerId);

            builder.HasOne(x => x.BusinessType);

        }
    }
}
