﻿using Amanah.Posthub.DATA.CompanyProfile;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> configuration)
        {
            configuration.HasOne(o => o.Country)
                .WithMany()
                .HasForeignKey(o => o.CountryId)
                .OnDelete(DeleteBehavior.Restrict);

            configuration.HasMany(o => o.Branches)
            .WithOne(x => x.Company)
            .HasForeignKey(o => o.CompanyId)
            .OnDelete(DeleteBehavior.Restrict);

            configuration.HasMany(o => o.WorkingHours)
             .WithOne(x => x.Company)
             .HasForeignKey(o => o.CompanyId)
             .OnDelete(DeleteBehavior.Restrict);

            configuration.OwnsOne(x => x.CompanyAddress);

            configuration.HasMany(x => x.SocialLinks);

        }
    }
}
