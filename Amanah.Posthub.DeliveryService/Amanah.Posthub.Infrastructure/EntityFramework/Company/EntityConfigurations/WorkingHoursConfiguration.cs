﻿using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class WorkingHoursConfiguration : IEntityTypeConfiguration<WorkingHours>
    {
        public void Configure(EntityTypeBuilder<WorkingHours> configuration)
        {

            configuration.HasOne(o => o.Company)
                .WithMany(x => x.WorkingHours)
                .HasForeignKey(o => o.CompanyId)
                .OnDelete(DeleteBehavior.Restrict);


        }
    }
}
