﻿using Amanah.Posthub.DATA.DbContext;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class CustomerConfiguration
        : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.Property(o => o.Name)
                .HasMaxLength(StringLengths.Default)
                .IsRequired();

            builder.Property(o => o.Email).HasMaxLength(StringLengths.UserName);

            builder.Property(o => o.Phone)
                .HasMaxLength(StringLengths.Phone)
                .IsRequired();

            builder.Property(o => o.Address)
                .IsRequired();

            builder.Property(o => o.Latitude);

            builder.Property(o => o.Longitude);

            builder.OwnsOne(x => x.Location);

            builder.HasOne(customer => customer.Branch)
                .WithOne(branch => branch.Customer)
                .HasForeignKey<Customer>(customer => customer.BranchId);
        }
    }
}
