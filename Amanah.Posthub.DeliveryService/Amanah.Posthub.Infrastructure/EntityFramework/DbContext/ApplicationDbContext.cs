﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.CompanyProfile;
using Amanah.Posthub.DATA.Extensions;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Infrastructure.EntityFramework.Orders;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Admins.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.Service.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.Service.Domain.Notifications.Entities;
using Amanah.Posthub.Service.Domain.PriceList.Entities;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.Service.Domain.TermsAndConditions.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Utilites;
using Utilities.Extensions;

namespace Amanah.Posthub.Context
{
    /*
     * Migrations
     * Add-Migration Initial -o "DbContext/Migrations"
     */
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        private readonly ICurrentUser _currentUser;
#if false
        private static readonly object _lock = new object();
        private static int _referenceCount = 0;
#endif
        public DbSet<MapRequestLog> MapRequestLogs { get; set; }
        public DbSet<UserDevice> UserDevice { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<LocationAccuracy> LocationAccuracy { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<RFQ> RFQ { get; set; }
        public DbSet<RFQItems> RFQItems { get; set; }
        public DbSet<PriceList> PriceLists { get; set; }
        public DbSet<PriceListItems> PriceListItems { get; set; }
        public DbSet<PriceListStatus> PriceListStatuses { get; set; }
        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<AgentType> AgentType { get; set; }
        public DbSet<TransportType> TransportType { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<Driver> Driver { get; set; }
        public DbSet<DriverExternalBranch> DriverExternalBranchs { get; set; }
        public DbSet<DriverCurrentLocation> DriverCurrentLocation { get; set; }
        public DbSet<GeoFence> GeoFence { get; set; }
        public DbSet<GeoFenceLocation> GeoFenceLocation { get; set; }
        public DbSet<Manager> Manager { get; set; }
        public DbSet<AgentStatus> AgentStatus { get; set; }
        public DbSet<MainTaskStatus> MainTaskStatus { get; set; }
        public DbSet<MainTaskType> MainTaskType { get; set; }
        public DbSet<MainTask> MainTask { get; set; }
        public DbSet<TaskType> TaskType { get; set; }
        public DbSet<Service.Domain.Tasks.Entities.TaskStatus> TaskStatus { get; set; }
        public DbSet<SubTask> Tasks { get; set; }
        public DbSet<TaskHistory> TaskHistory { get; set; }
        public DbSet<TaskGallary> TaskGallary { get; set; }
        public DbSet<DriverTaskNotification> DriverTaskNotification { get; set; }
        public DbSet<TeamManager> TeamManager { get; set; }
        public DbSet<TaskRoute> TaskRoute { get; set; }
        public DbSet<TaskDriverRequests> TaskDriverRequests { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Restaurant> Restaurant { get; set; }
        public DbSet<Branch> Branch { get; set; }
        public DbSet<ManagerDispatching> ManagerDispatching { get; set; }
        public DbSet<DriverPickUpGeoFence> DriverPickUpGeoFence { get; set; }
        public DbSet<DriverDeliveryGeoFence> DriverDeliveryGeoFence { get; set; }
        public DbSet<DriverLoginRequest> DriverLoginRequests { get; set; }
        public DbSet<DriverRate> DriverRate { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Area> Area { get; set; }
        public DbSet<AccountLogs> AccountLogs { get; set; }
        public DbSet<DriverloginTracking> DriverloginTracking { get; set; }
        public DbSet<DriverRegistration> DriverRegistrations { get; set; }
        public DbSet<DriverAttachment> DriverAttachments { get; set; }
        public DbSet<TenantRegistration> TenantRegistrations { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<WorkingHours> WorkingHours { get; set; }
        public DbSet<CompanySocialLinks> CompanySocialLinks { get; set; }
        public DbSet<TenantSocialMediaAccount> TenantSocialMediaAccounts { get; set; }
        public DbSet<TenantRegistrationAttachment> TenantRegistrationAttachments { get; set; }
        public DbSet<TermsAndConditions> TermsAndConditions { get; set; }
        public DbSet<PACI> PACI { get; set; }
        public DbSet<DriverWorkingHourTracking> DriverWorkingHourTracking { get; set; }
        public DbSet<Governrate> Governrates { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<ServiceSector> ServiceSectors { get; set; }
        public DbSet<Service.Domain.CompanyProfile.Entities.BusinessType> BusinessTypes { get; set; }
        public DbSet<AccountStatus> AccountStatus { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<DriverFloor> DriverFloors { get; set; }

        ///         
        public DbSet<BusinessContactAccount> BusinessConactAccounts { get; set; }
        public DbSet<BusinessCustomer> BusinessCustomers { get; set; }
        public DbSet<BusinessCustomerBranch> BusinessCustomerBranchs { get; set; }
        public DbSet<CollectionMethod> CollectionMethods { get; set; }
        public DbSet<ContactFunction> ContactFunctions { get; set; }
        public DbSet<Courtesy> Courtesys { get; set; }
        public DbSet<ContractType> ContractType { get; set; }
        public DbSet<GeoFenceBlocks> GeoFenceBlocks { get; set; }
        public DbSet<Remark> Remarks { get; set; }
        public DbSet<DriverRole> DriverRoles { get; set; }
        public bool IsTenantFilterEnabled { get; set; } = true;

        #region OrderServiceEntities


        public DbSet<Order> Orders { get; set; }
        public DbSet<DirectOrder> DirectOrders { get; set; }
        public DbSet<BusinessOrder> BusinessOrders { get; set; }
        public DbSet<OrdersStatusLog> OrdersStatusLogs { get; set; }
        public DbSet<OrderStatus> OrderStatuss { get; set; }
        public DbSet<PriceQuotationStatus> PriceQuotationStatuses { get; set; }
        public DbSet<ServiceType> ServiceTypes { get; set; }
        public DbSet<MailItem> MailItems { get; set; }
        public DbSet<MailItemsStatusLog> MailItemsStatusLogs { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<WorkOrder> Workorders { get; set; }
        public DbSet<WeightUOM> WeightUOMs { get; set; }
        public DbSet<AttachmentFile> AttachmentFiles { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        //public DbSet<Customer> Customers{ get; set; }
        //public DbSet<Address> Addresses{ get; set; }
        //lookups
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<PaymentTransaction> PaymentTransactions { get; set; }
        //public DbSet<AccountStatus> AccountStatus{ get; set; }
        public DbSet<MailItemStatus> MailItemStatuss { get; set; }
        public DbSet<AddressTag> AddressTags { get; set; }
        public DbSet<ContractStatus> ContractStatus { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<ExtraTerm> ExtraTerms { get; set; }
        public DbSet<CreditStatus> CreditStatus { get; set; }
        public DbSet<Currency> PaymentCurrencies { get; set; }
        //public DbSet<DriverDutyStatus> DriverDutyStatus { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<LengthUOM> LengthUOMs { get; set; }
        public DbSet<MailItemType> MailItemTypes { get; set; }
        public DbSet<OrderRecurency> OrderRecurencies { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<OrderType> OrderTypes { get; set; }

        ///warning why this table
        //public DbSet<Role> Roles { get; set; }
        public DbSet<ShipServiceType> ShipServiceTypes { get; set; }
        //public DbSet<UserAccessType> UserAccessTypes { get; set; }
        //public DbSet<UserAccountStatus> UserAccountStatus { get; set; }
        public DbSet<WorkOrderStatus> WorkOrderStatus { get; set; }
        //public DbSet<Industry> Industries { get; set; }
        //public DbSet<State> States { get; set; }
        public DbSet<Courier> Couriers { get; set; }
        public DbSet<CourierZone> CourierZones { get; set; }
        public DbSet<CourierShipmentService> CourierShipmentServices { get; set; }
        public DbSet<ItemReturnReason> ItemReturnReasons { get; set; }
        public DbSet<Block> Blocks { get; set; }
        public DbSet<Amanah.Posthub.Service.Domain.Entities.Service> Services { get; set; }
        public DbSet<PackingPackageCountry> PackingPackageCountries { get; set; }
        public DbSet<PackingPackage> PackingPackages { get; set; }
        public DbSet<BusinessOrderService> BusinessOrderServices { get; set; }

        public DbSet<TermsAndConditionsLookup> TermsAndConditionsLookups { get; set; }




        #endregion

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, ICurrentUser currentUser)
            : base(options)

        {
            _currentUser = currentUser;
        }

        public override void Dispose()
        {
            base.Dispose();
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {

            builder.Entity<PackingPackageCountry>().HasIndex(e => new { e.CountryId, e.IsDeleted, e.CreatedDate }).IsUnique();


            var configuration = new ConfigurationBuilder()
                    .SetBasePath(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location))
                    .AddJsonFile("appsettings.json").Build();

            var isExpress = configuration.GetValue<bool>("FeatureManagement:Express");

            if (isExpress)
            {
                builder.Entity<Branch>().Ignore(c => c.FloorsCount);
                builder.Entity<Driver>().Ignore(c => c.DriverExternalBranchs);
                builder.Entity<Driver>().Ignore(c => c.DriverFloors);
            }


            base.OnModelCreating(builder);

            builder.Entity<Country>().HasIndex(e => new { e.Name, e.IsDeleted, e.CreatedDate }).IsUnique();
            builder.Entity<Branch>().HasIndex(e => new { e.Name, e.IsDeleted, e.CreatedDate }).IsUnique();
            builder.Entity<ApplicationRole>().HasIndex(e => new { e.Name, e.IsDeleted, e.CreatedDate }).IsUnique();
            //builder.Entity<MailItem>()
            //        .HasMany(u => u.SignatureImages).WithOne(tr => tr.MailItemSignatureImage).HasForeignKey(c=>c.SignatureImageId);


            //builder.Entity<WeightUOM>().HasIndex(e => new { e.Name_ar, e.Name_en, e.Code, e.IsDeleted }).IsUnique();
            //builder.Entity<Department>().HasIndex(e => new { e.Name_ar, e.Name_en, e.IsDeleted }).IsUnique();
            //builder.Entity<Area>().HasIndex(e => new { e.NameAR, e.NameEN, e.IsDeleted }).IsUnique();
            //builder.Entity<Governrate>().HasIndex(e => new { e.NameAR, e.NameEN, e.IsDeleted }).IsUnique();
            //builder.Entity<MailItemType>().HasIndex(e => new { e.Name_ar, e.Name_en, e.IsDeleted }).IsUnique();
            //builder.Entity<LengthUOM>().HasIndex(e => new { e.Name, e.Code, e.IsDeleted }).IsUnique();
            //builder.Entity<ItemReturnReason>().HasIndex(e => new { e.Name, e.IsDeleted }).IsUnique();
            builder.Entity<DriverRole>().HasIndex(e => new { e.Name_ar, e.Name_en, e.IsDeleted, e.CreatedDate }).IsUnique();

            builder.Entity<MailItem>()
                    .HasMany(u => u.ProofOfDeliveryImages).WithOne(tr => tr.MailItemProofOfDeliveryImage)
                    .HasForeignKey(c => c.ProofOfDeliveryImageId);


            //builder.Entity<Contact>()
            //        .HasMany(u => u.ConsigneeInfos).WithOne(tr => tr.ConsigneeInfo)
            //        .HasForeignKey(c => c.ConsigneeInfoId);

            //builder.Entity<Contact>()
            //      .HasMany(u => u.ReciverInfos).WithOne(tr => tr.ReciverInfo)
            //      .HasForeignKey(c => c.ReciverInfoId);

            //builder.Entity<MailItemsStatusLog>()
            //  .HasOne(d => d.MailItem)
            //  .WithMany(s => s.MailItemsStatusLog)
            //    .HasForeignKey(d => d.MailItemId);

            //builder.Entity<MailItemsStatusLog>()
            // .HasOne(d => d.MailItemStatus)
            // .WithMany(s => s.MailItemsStatusLogs)
            //   .HasForeignKey(d => d.MailItemStatusId);

            //builder.Entity<MailItem>()
            //       .HasMany(u => u.MailItemsStatusLog).WithOne(tr => tr.MailItem)
            //       .HasForeignKey(c => c.MailItemId);

            //builder.Entity<MailItemStatus>()
            //       .HasMany(u => u.MailItemsStatusLogs).WithOne(tr => tr.MailItemStatus)
            //       .HasForeignKey(c => c.MailItemStatusId);


            EntityConfigurationHelper.ApplyConfigurations(builder);
            OrdersModelConfigurationHelper.ApplyOrderModelConfigurations(builder);



            AddGlobalFilters(builder);
            TenantRelationshipHelper.Configure(builder);



        }
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            HandleAuditing();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            HandleAuditing();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        #region Automatic Auditing & Global Query Filter
        private void HandleAuditing()
        {
            // TenantID for created
            var now = DateTime.UtcNow;
            var haveTenantAddedEntries = ChangeTracker.Entries<IHaveTenant>()
                .Where(entry => entry.State == EntityState.Added)
                .ToArray();
            foreach (var addedEntry in haveTenantAddedEntries)
            {
                addedEntry.SetPropertyIfNotSet(nameof(IHaveTenant.Tenant_Id), _currentUser.TenantId);
            }
            var haveTenantModifiedEntries = ChangeTracker.Entries<IHaveTenant>()
              .Where(entry => entry.State == EntityState.Modified)
              .ToArray();
            foreach (var modifiedEntry in haveTenantModifiedEntries)
            {
                if (modifiedEntry.Property(nameof(IHaveTenant.Tenant_Id)) != null)    //to avoid automapper setting tenant id to null   && setting tenant if has value
                {
                    if (string.IsNullOrEmpty(modifiedEntry.Entity.Tenant_Id))
                    {
                        modifiedEntry.Property(nameof(IHaveTenant.Tenant_Id)).IsModified = false;
                    }
                }
            }

            var auditedCreatedEntries = ChangeTracker.Entries<IHaveAuditing>()
            .Where(entry => entry.State == EntityState.Added)
            .ToArray();
            foreach (var addedEntry in auditedCreatedEntries)
            {
                addedEntry.SetPropertyIfNotSet(nameof(IHaveAuditing.CreatedBy_Id), _currentUser.UserName);
                addedEntry.SetPropertyIfNotSet(nameof(IHaveAuditing.CreatedDate), now);
            }
            var auditedModifiedEntries = ChangeTracker.Entries<IHaveAuditing>()
                .Where(entry => entry.State == EntityState.Modified)
                .ToArray();
            foreach (var modifiedEntry in auditedModifiedEntries)
            {
                modifiedEntry.SetPropertyIfNotSet(nameof(IHaveAuditing.UpdatedBy_Id), _currentUser.UserName);
                modifiedEntry.SetPropertyIfNotSet(nameof(IHaveAuditing.UpdatedDate), now);
                modifiedEntry.Property(nameof(IHaveAuditing.CreatedBy_Id)).IsModified = false;
                modifiedEntry.Property(nameof(IHaveAuditing.CreatedDate)).IsModified = false;
            }
            var auditedDeletedEntries = ChangeTracker.Entries<IHaveAuditing>()
                .Where(entry => entry.State == EntityState.Deleted)
                .ToArray();
            foreach (var deletedEntry in auditedDeletedEntries)
            {
                deletedEntry.SetPropertyIfNotSet(nameof(IHaveAuditing.IsDeleted), true);
                deletedEntry.SetPropertyIfNotSet(nameof(IHaveAuditing.DeletedBy_Id), _currentUser.UserName);
                deletedEntry.SetPropertyIfNotSet(nameof(IHaveAuditing.DeletedDate), now);
                deletedEntry.State = EntityState.Modified;
                deletedEntry.Property(nameof(IHaveTenant.Tenant_Id)).IsModified = false;
                deletedEntry.Property(nameof(IHaveAuditing.CreatedBy_Id)).IsModified = false;
                deletedEntry.Property(nameof(IHaveAuditing.CreatedDate)).IsModified = false;
            }
        }

        private void AddGlobalFilters(ModelBuilder builder)
        {
            // Note: adding multiple global query filters is not supported,
            //  a single filter is allowed however you can add multiple conditions to it.
            var tenantFilteredEntityTypes = builder.GetEntitiesOfType<IHaveTenant>();
            var softDeleteFilteredEntityTypes = builder.GetEntitiesOfType<IHaveAuditing>();
            foreach (var tenantEnityType in tenantFilteredEntityTypes)
            {
                tenantEnityType.AddQueryFilter(QueryFilterHelper.GetTenantQueryFilter(tenantEnityType.ClrType, this));
            }
            foreach (var softDeletedEntity in softDeleteFilteredEntityTypes)
            {
                softDeletedEntity.AddQueryFilter(QueryFilterHelper.GetSoftDeleteQueryFilter(softDeletedEntity.ClrType));
            }
        }

        private static class QueryFilterHelper
        {
            private static readonly MethodInfo _inernalGetTenantQueryFilterMethodInfo =
                ReflectionHelper.GetStaticMethod(typeof(QueryFilterHelper), nameof(InternalGetTenantQueryFilter));
            private static readonly MethodInfo _internalGetSoftDeleteQueryFilterMethodInfo =
                ReflectionHelper.GetStaticMethod(typeof(QueryFilterHelper), nameof(InternalGetSoftDeleteQueryFilter));

            public static LambdaExpression GetSoftDeleteQueryFilter(Type entityType)
            {
                return _internalGetSoftDeleteQueryFilterMethodInfo.MakeGenericMethod(entityType)
                    .Invoke(null, Array.Empty<object>()) as LambdaExpression;
            }

            public static LambdaExpression GetTenantQueryFilter(Type entityType, ApplicationDbContext dbContext)
            {
                return _inernalGetTenantQueryFilterMethodInfo.MakeGenericMethod(entityType)
                    .Invoke(null, new object[] { dbContext }) as LambdaExpression;
            }

            private static Expression<Func<TEntity, bool>> InternalGetTenantQueryFilter<TEntity>(
                ApplicationDbContext dbContext)
                where TEntity : IHaveTenant
            {
                // Note: it is ment to use dbcontext instead of directly referencing currentuser.Tenant_Id to avoid tenant caching
                return entity =>
                    !dbContext.IsTenantFilterEnabled ||
                    dbContext._currentUser.IsAuthenticated != true ||
                    entity.Tenant_Id == dbContext._currentUser.TenantId;
            }

            private static Expression<Func<TEntity, bool>> InternalGetSoftDeleteQueryFilter<TEntity>()
                where TEntity : IHaveAuditing
            {
                return entity => !entity.IsDeleted;
            }
        }
        #endregion


        #region Automatic loading Entity Configuration
        private static class EntityConfigurationHelper
        {
            private static readonly MethodInfo _applyEntityConfigurationGenericMethod = GetApplyMethod();

            public static void ApplyConfigurations(ModelBuilder builder)
            {
                //Note: IQueryTypeConfiguration is obselete and renamed to keyless entity, we can use IEntityTypeConfiguration,
                // set HasNoKey()
                // https://docs.microsoft.com/en-us/ef/core/modeling/keyless-entity-types
                var configurationTypes = Assembly.GetExecutingAssembly()
                    .GetTypes()
                    .Where(IsConfigurationType)
                    .ToArray();
                var entityTypes = configurationTypes
                    .Select(type => type.GetInterfaces().Single(IsEntityTypeConfigurationInterface).GenericTypeArguments[0])
                    .ToArray();
                ValidateNoConfigurationDuplication(entityTypes);
                foreach (var configurationType in configurationTypes)
                {
                    var entityType = configurationType.GetInterfaces()
                        .Single(IsEntityTypeConfigurationInterface)
                        .GenericTypeArguments[0];
                    _applyEntityConfigurationGenericMethod
                        .MakeGenericMethod(entityType)
                        .Invoke(builder, new[] { Activator.CreateInstance(configurationType) });

                }
            }

            private static MethodInfo GetApplyMethod()
            {
                return typeof(ModelBuilder)
                    .GetMethods()
                    .Where(method => method.Name == nameof(ModelBuilder.ApplyConfiguration))
                    .Single(method =>
                        method.GetParameters()
                            .Any(parameter =>
                                parameter.ParameterType.IsGenericType &&
                                parameter.ParameterType.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>)));
            }

            private static bool IsConfigurationType(Type type)
            {
                if (type is null)
                {
                    throw new ArgumentNullException(nameof(type));
                }

                return type.IsConcrete() && type.GetInterfaces().Any(IsEntityTypeConfigurationInterface);
            }

            private static bool IsEntityTypeConfigurationInterface(Type interfaceType)
            {
                return interfaceType.IsConstructedGenericType &&
                    interfaceType.GetGenericTypeDefinition() == typeof(IEntityTypeConfiguration<>);
            }

            private static void ValidateNoConfigurationDuplication(Type[] types)
            {
                var duplicates = types.GetDuplicates();
                if (duplicates.Any())
                {
                    var duplicateNames = duplicates.Select(type => type.Name).Join(", ");
                    string message = $"Duplicated entity configuration found for the types [{duplicateNames}]";
                    throw new InvalidOperationException(message);
                }
            }
        }
        #endregion

        #region Tenan Relationship Configuration
        private static class TenantRelationshipHelper
        {
            private static readonly MethodInfo _setTenantIdForeignKey =
                ReflectionHelper.GetStaticMethod(typeof(TenantRelationshipHelper), nameof(SetTenantIdForeignKey));

            public static void Configure(ModelBuilder modelBuilder)
            {
                modelBuilder.GetEntitiesOfType<IHaveTenant>()
                    .ToList()
                    .ForEach(entityType =>
                    {
                        _setTenantIdForeignKey.MakeGenericMethod(entityType.ClrType)
                            .Invoke(null, new object[] { modelBuilder });
                    });
            }

            private static void SetTenantIdForeignKey<T>(ModelBuilder modelBuilder) where T : class, IHaveTenant
            {
                modelBuilder.Entity<T>()
                    .HasOne<ApplicationUser>()
                    .WithMany()
                    .HasForeignKey(nameof(IHaveTenant.Tenant_Id))
                    .OnDelete(DeleteBehavior.Restrict)
                    .IsRequired(false);
            }
        }
        #endregion
    }
}
