﻿using Amanah.Posthub.BASE.Domain.Repositories;
using Amanah.Posthub.Context;
using Amanah.Posthub.Repoistry;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Amanah.Posthub.Infrastructure.EntityFramework.DbContext
{
    public static class Extensions
    {
        public static IServiceCollection AddEntityFramework(
            this IServiceCollection services,
            IConfiguration configuration,
            IHostEnvironment environment)
        {
            services.AddScoped(typeof(IRepositry<>), typeof(Repositry<>));
            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(
                    configuration["ConnectionString"],
                    sqlOptions =>
                    {
                        sqlOptions.EnableRetryOnFailure(5);
                    })
                    .EnableSensitiveDataLogging(environment.IsDevelopment());
            });

            services.AddScoped<Microsoft.EntityFrameworkCore.DbContext>(
                serviceProvider => serviceProvider.GetRequiredService<ApplicationDbContext>());
            return services;
        }
    }
}
