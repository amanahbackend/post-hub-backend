﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AgentStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AgentType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Country",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Flag = table.Column<string>(nullable: true),
                    TopLevel = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Country", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LocationAccuracy",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Duration = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationAccuracy", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MainTaskStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainTaskStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MainTaskType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainTaskType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TaskStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Color = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TaskType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TransportType",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransportType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(unicode: false, maxLength: 36, nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    IsAvailable = table.Column<bool>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUsers_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountLogs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    ActivityType = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    TableName = table.Column<string>(nullable: true),
                    Record_Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountLogs_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Admins",
                columns: table => new
                {
                    Id = table.Column<string>(unicode: false, maxLength: 36, nullable: false),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    CompanyAddress = table.Column<string>(nullable: true),
                    DisplayImage = table.Column<int>(nullable: false),
                    DashBoardLanguage = table.Column<string>(nullable: true),
                    TrackingPanelLanguage = table.Column<string>(nullable: true),
                    DeactivationReason = table.Column<string>(nullable: true),
                    ResidentCountryId = table.Column<int>(nullable: false),
                    BranchId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Admins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Admins_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Area",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    FK_Governrate_Id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Area", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Area_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(unicode: false, maxLength: 36, nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoles_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GeoFence",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeoFence", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GeoFence_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MainTask",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    MainTaskTypeId = table.Column<int>(nullable: false),
                    IsCompleted = table.Column<bool>(nullable: true),
                    IsDelayed = table.Column<bool>(nullable: true),
                    AssignmentType = table.Column<int>(nullable: false),
                    IsFailToAutoAssignDriver = table.Column<bool>(nullable: false),
                    ExpirationDate = table.Column<DateTime>(nullable: true),
                    MainTaskStatusId = table.Column<int>(nullable: true),
                    EstimatedTime = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainTask", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MainTask_MainTaskStatus_MainTaskStatusId",
                        column: x => x.MainTaskStatusId,
                        principalTable: "MainTaskStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MainTask_MainTaskType_MainTaskTypeId",
                        column: x => x.MainTaskTypeId,
                        principalTable: "MainTaskType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MainTask_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Manager",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manager", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Manager_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Manager_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MapRequestLogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Provider = table.Column<string>(nullable: true),
                    StackTrace = table.Column<string>(nullable: true),
                    ApiKey = table.Column<string>(nullable: true),
                    Request = table.Column<string>(nullable: true),
                    Response = table.Column<string>(nullable: true),
                    Success = table.Column<bool>(nullable: false),
                    Error = table.Column<string>(nullable: true),
                    ErrorMessage = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MapRequestLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MapRequestLogs_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Notifications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    IsSeen = table.Column<bool>(nullable: false),
                    Body = table.Column<string>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    ToUserId = table.Column<string>(nullable: false),
                    FromUserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Notifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Notifications_AspNetUsers_FromUserId",
                        column: x => x.FromUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Notifications_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Notifications_AspNetUsers_ToUserId",
                        column: x => x.ToUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PACI",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    PACINumber = table.Column<string>(nullable: true),
                    AreaName = table.Column<string>(nullable: true),
                    AreaId = table.Column<int>(nullable: false),
                    GovernorateName = table.Column<string>(nullable: true),
                    GovernorateId = table.Column<int>(nullable: false),
                    BlockName = table.Column<string>(nullable: true),
                    BlockId = table.Column<int>(nullable: false),
                    StreetName = table.Column<string>(nullable: true),
                    StreetId = table.Column<int>(nullable: false),
                    Longtiude = table.Column<decimal>(nullable: false),
                    Latitude = table.Column<decimal>(nullable: false),
                    AppartementNumber = table.Column<string>(nullable: true),
                    FloorNumber = table.Column<string>(nullable: true),
                    BuildingNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PACI", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PACI_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Restaurant",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 250, nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Restaurant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Restaurant_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    SettingKey = table.Column<string>(maxLength: 255, nullable: true),
                    Value = table.Column<string>(nullable: true),
                    SettingDataType = table.Column<string>(nullable: true),
                    GroupId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Settings_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    LocationAccuracyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Teams_LocationAccuracy_LocationAccuracyId",
                        column: x => x.LocationAccuracyId,
                        principalTable: "LocationAccuracy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Teams_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserDevice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: true),
                    DeviceType = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    FCMDeviceId = table.Column<string>(nullable: true),
                    IsLoggedIn = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDevice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserDevice_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GeoFenceLocation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GeoFenceId = table.Column<int>(nullable: false),
                    Longitude = table.Column<double>(nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    PointIndex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeoFenceLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GeoFenceLocation_GeoFence_GeoFenceId",
                        column: x => x.GeoFenceId,
                        principalTable: "GeoFence",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaskDriverRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    TaskId = table.Column<int>(nullable: false),
                    MainTaskId = table.Column<int>(nullable: false),
                    DriverId = table.Column<int>(nullable: true),
                    ResponseStatus = table.Column<int>(nullable: false),
                    RetriesCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskDriverRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskDriverRequests_MainTask_MainTaskId",
                        column: x => x.MainTaskId,
                        principalTable: "MainTask",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskDriverRequests_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ManagerDispatching",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    DesignationName = table.Column<string>(maxLength: 100, nullable: false),
                    Zones = table.Column<string>(nullable: true),
                    Restaurants = table.Column<string>(nullable: true),
                    Branches = table.Column<string>(nullable: true),
                    ManagerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManagerDispatching", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManagerDispatching_Manager_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "Manager",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ManagerDispatching_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Branch",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    CountryId = table.Column<int>(nullable: true),
                    Phone = table.Column<string>(maxLength: 50, nullable: false),
                    RestaurantId = table.Column<int>(nullable: false),
                    GeoFenceId = table.Column<int>(nullable: false),
                    Address = table.Column<string>(maxLength: 255, nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsActivePrevious = table.Column<bool>(nullable: false),
                    Location_Area = table.Column<string>(nullable: true),
                    Location_Block = table.Column<string>(nullable: true),
                    Location_Street = table.Column<string>(nullable: true),
                    Location_Building = table.Column<string>(nullable: true),
                    Location_Floor = table.Column<string>(nullable: true),
                    Location_Flat = table.Column<string>(nullable: true),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Branch", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Branch_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Branch_GeoFence_GeoFenceId",
                        column: x => x.GeoFenceId,
                        principalTable: "GeoFence",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Branch_Restaurant_RestaurantId",
                        column: x => x.RestaurantId,
                        principalTable: "Restaurant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Branch_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Driver",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    ImageUrl = table.Column<string>(nullable: true),
                    Tags = table.Column<string>(nullable: true),
                    TransportDescription = table.Column<string>(nullable: true),
                    LicensePlate = table.Column<string>(nullable: true),
                    Color = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true),
                    TeamId = table.Column<int>(nullable: false),
                    AgentTypeId = table.Column<int>(nullable: true),
                    TransportTypeId = table.Column<int>(nullable: false),
                    CountryId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    AgentStatusId = table.Column<int>(nullable: true),
                    LastDisconnectStatusId = table.Column<int>(nullable: true),
                    ReachedTime = table.Column<DateTime>(nullable: true),
                    AgentStatusUpdateDate = table.Column<DateTime>(nullable: false),
                    DeviceType = table.Column<string>(nullable: true),
                    Version = table.Column<string>(nullable: true),
                    Reason = table.Column<string>(nullable: true),
                    MaxOrdersWeightsCapacity = table.Column<int>(nullable: false),
                    MinOrdersNoWait = table.Column<int>(nullable: false),
                    IsInClubbingTime = table.Column<bool>(nullable: false),
                    ClubbingTimeExpiration = table.Column<DateTime>(nullable: true),
                    DriverAvgRate = table.Column<double>(nullable: true),
                    TokenTimeExpiration = table.Column<DateTime>(nullable: true),
                    AllPickupGeoFences = table.Column<bool>(nullable: false),
                    AllDeliveryGeoFences = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Driver", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Driver_AgentStatus_AgentStatusId",
                        column: x => x.AgentStatusId,
                        principalTable: "AgentStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Driver_AgentType_AgentTypeId",
                        column: x => x.AgentTypeId,
                        principalTable: "AgentType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Driver_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Driver_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Driver_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Driver_TransportType_TransportTypeId",
                        column: x => x.TransportTypeId,
                        principalTable: "TransportType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Driver_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TeamManager",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeamId = table.Column<int>(nullable: false),
                    ManagerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeamManager", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TeamManager_Manager_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "Manager",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeamManager_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Email = table.Column<string>(maxLength: 128, nullable: true),
                    Phone = table.Column<string>(maxLength: 50, nullable: false),
                    Address = table.Column<string>(nullable: false),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true),
                    Tags = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: true),
                    BranchId = table.Column<int>(nullable: true),
                    Location_Area = table.Column<string>(nullable: true),
                    Location_Block = table.Column<string>(nullable: true),
                    Location_Street = table.Column<string>(nullable: true),
                    Location_Building = table.Column<string>(nullable: true),
                    Location_Floor = table.Column<string>(nullable: true),
                    Location_Flat = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Branch_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Customers_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverCurrentLocation",
                columns: table => new
                {
                    DriverId = table.Column<int>(nullable: false),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true),
                    BranchId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverCurrentLocation", x => x.DriverId);
                    table.ForeignKey(
                        name: "FK_DriverCurrentLocation_Branch_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverCurrentLocation_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverCurrentLocation_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverDeliveryGeoFence",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DriverId = table.Column<int>(nullable: false),
                    GeoFenceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverDeliveryGeoFence", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverDeliveryGeoFence_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverDeliveryGeoFence_GeoFence_GeoFenceId",
                        column: x => x.GeoFenceId,
                        principalTable: "GeoFence",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DriverLoginRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    Status = table.Column<byte>(nullable: false),
                    Token = table.Column<string>(nullable: false),
                    DriverId = table.Column<int>(nullable: false),
                    ApprovedOrRejectBy = table.Column<string>(nullable: true),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverLoginRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverLoginRequests_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverLoginRequests_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverloginTracking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    DriverId = table.Column<int>(nullable: false),
                    LoginDate = table.Column<DateTime>(nullable: true),
                    LoginLatitude = table.Column<double>(nullable: true),
                    LoginLongitude = table.Column<double>(nullable: true),
                    LogoutDate = table.Column<DateTime>(nullable: true),
                    LogoutLatitude = table.Column<double>(nullable: true),
                    LogoutLongitude = table.Column<double>(nullable: true),
                    LogoutActionBy = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverloginTracking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverloginTracking_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverloginTracking_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverPickUpGeoFence",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DriverId = table.Column<int>(nullable: false),
                    GeoFenceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverPickUpGeoFence", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverPickUpGeoFence_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverPickUpGeoFence_GeoFence_GeoFenceId",
                        column: x => x.GeoFenceId,
                        principalTable: "GeoFence",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    MainTaskId = table.Column<int>(nullable: false),
                    TaskTypeId = table.Column<int>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    TaskStatusId = table.Column<int>(nullable: false),
                    DriverId = table.Column<int>(nullable: true),
                    OrderId = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: false),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    PickupDate = table.Column<DateTime>(nullable: true),
                    DeliveryDate = table.Column<DateTime>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: true),
                    DelayTime = table.Column<double>(nullable: true),
                    SuccessfulDate = table.Column<DateTime>(nullable: true),
                    TotalTime = table.Column<double>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    SignatureFileName = table.Column<string>(nullable: true),
                    SignatureURL = table.Column<string>(nullable: true),
                    BranchId = table.Column<int>(nullable: true),
                    GeoFenceId = table.Column<int>(nullable: true),
                    TotalDistance = table.Column<double>(nullable: true),
                    TotalTaskTime = table.Column<double>(nullable: true),
                    IsTaskReached = table.Column<bool>(nullable: true),
                    ReachedTime = table.Column<DateTime>(nullable: true),
                    Location_Area = table.Column<string>(nullable: true),
                    Location_Block = table.Column<string>(nullable: true),
                    Location_Street = table.Column<string>(nullable: true),
                    Location_Building = table.Column<string>(nullable: true),
                    Location_Floor = table.Column<string>(nullable: true),
                    Location_Flat = table.Column<string>(nullable: true),
                    EstimatedTime = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Branch_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_GeoFence_GeoFenceId",
                        column: x => x.GeoFenceId,
                        principalTable: "GeoFence",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_MainTask_MainTaskId",
                        column: x => x.MainTaskId,
                        principalTable: "MainTask",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_TaskStatus_TaskStatusId",
                        column: x => x.TaskStatusId,
                        principalTable: "TaskStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_TaskType_TaskTypeId",
                        column: x => x.TaskTypeId,
                        principalTable: "TaskType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tasks_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverRate",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    CustomerId = table.Column<int>(nullable: false),
                    DriverId = table.Column<int>(nullable: false),
                    TasksId = table.Column<int>(nullable: false),
                    Rate = table.Column<double>(nullable: false),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverRate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverRate_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverRate_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverRate_Tasks_TasksId",
                        column: x => x.TasksId,
                        principalTable: "Tasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverRate_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverTaskNotification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    TaskId = table.Column<int>(nullable: false),
                    DriverId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ActionStatus = table.Column<string>(nullable: true),
                    IsRead = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverTaskNotification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverTaskNotification_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverTaskNotification_Tasks_TaskId",
                        column: x => x.TaskId,
                        principalTable: "Tasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverTaskNotification_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TaskGallary",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    MainTaskId = table.Column<int>(nullable: true),
                    TaskId = table.Column<int>(nullable: false),
                    DriverId = table.Column<int>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    FileURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskGallary", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskGallary_Tasks_TaskId",
                        column: x => x.TaskId,
                        principalTable: "Tasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskGallary_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TaskHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    TaskId = table.Column<int>(nullable: false),
                    MainTaskId = table.Column<int>(nullable: false),
                    FromStatusId = table.Column<int>(nullable: true),
                    ToStatusId = table.Column<int>(nullable: true),
                    Reason = table.Column<string>(nullable: true),
                    ActionName = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskHistory_TaskStatus_FromStatusId",
                        column: x => x.FromStatusId,
                        principalTable: "TaskStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskHistory_Tasks_TaskId",
                        column: x => x.TaskId,
                        principalTable: "Tasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskHistory_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskHistory_TaskStatus_ToStatusId",
                        column: x => x.ToStatusId,
                        principalTable: "TaskStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TaskRoute",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    TaskId = table.Column<int>(nullable: false),
                    DriverId = table.Column<int>(nullable: false),
                    TaskStatusId = table.Column<int>(nullable: true),
                    Latitude = table.Column<double>(nullable: true),
                    Longitude = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskRoute", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaskRoute_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskRoute_Tasks_TaskId",
                        column: x => x.TaskId,
                        principalTable: "Tasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskRoute_TaskStatus_TaskStatusId",
                        column: x => x.TaskStatusId,
                        principalTable: "TaskStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskRoute_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountLogs_Tenant_Id",
                table: "AccountLogs",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Admins_Tenant_Id",
                table: "Admins",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Area_Tenant_Id",
                table: "Area",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoles_Tenant_Id",
                table: "AspNetRoles",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CountryId",
                table: "AspNetUsers",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_Tenant_Id",
                table: "AspNetUsers",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Branch_CountryId",
                table: "Branch",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Branch_GeoFenceId",
                table: "Branch",
                column: "GeoFenceId");

            migrationBuilder.CreateIndex(
                name: "IX_Branch_RestaurantId",
                table: "Branch",
                column: "RestaurantId");

            migrationBuilder.CreateIndex(
                name: "IX_Branch_Tenant_Id",
                table: "Branch",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_BranchId",
                table: "Customers",
                column: "BranchId",
                unique: true,
                filter: "[BranchId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_CountryId",
                table: "Customers",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_Tenant_Id",
                table: "Customers",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_AgentStatusId",
                table: "Driver",
                column: "AgentStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_AgentTypeId",
                table: "Driver",
                column: "AgentTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_CountryId",
                table: "Driver",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_TeamId",
                table: "Driver",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_Tenant_Id",
                table: "Driver",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_TransportTypeId",
                table: "Driver",
                column: "TransportTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_UserId",
                table: "Driver",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverCurrentLocation_BranchId",
                table: "DriverCurrentLocation",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverCurrentLocation_Tenant_Id",
                table: "DriverCurrentLocation",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_DriverDeliveryGeoFence_DriverId",
                table: "DriverDeliveryGeoFence",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverDeliveryGeoFence_GeoFenceId",
                table: "DriverDeliveryGeoFence",
                column: "GeoFenceId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverLoginRequests_DriverId",
                table: "DriverLoginRequests",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverLoginRequests_Tenant_Id",
                table: "DriverLoginRequests",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_DriverloginTracking_DriverId",
                table: "DriverloginTracking",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverloginTracking_Tenant_Id",
                table: "DriverloginTracking",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_DriverPickUpGeoFence_DriverId",
                table: "DriverPickUpGeoFence",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverPickUpGeoFence_GeoFenceId",
                table: "DriverPickUpGeoFence",
                column: "GeoFenceId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRate_CustomerId",
                table: "DriverRate",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRate_DriverId",
                table: "DriverRate",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRate_TasksId",
                table: "DriverRate",
                column: "TasksId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRate_Tenant_Id",
                table: "DriverRate",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_DriverTaskNotification_DriverId",
                table: "DriverTaskNotification",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverTaskNotification_TaskId",
                table: "DriverTaskNotification",
                column: "TaskId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverTaskNotification_Tenant_Id",
                table: "DriverTaskNotification",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GeoFence_Tenant_Id",
                table: "GeoFence",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_GeoFenceLocation_GeoFenceId",
                table: "GeoFenceLocation",
                column: "GeoFenceId");

            migrationBuilder.CreateIndex(
                name: "IX_MainTask_MainTaskStatusId",
                table: "MainTask",
                column: "MainTaskStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_MainTask_MainTaskTypeId",
                table: "MainTask",
                column: "MainTaskTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MainTask_Tenant_Id",
                table: "MainTask",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Manager_Tenant_Id",
                table: "Manager",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Manager_UserId",
                table: "Manager",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ManagerDispatching_ManagerId",
                table: "ManagerDispatching",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_ManagerDispatching_Tenant_Id",
                table: "ManagerDispatching",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_MapRequestLogs_Tenant_Id",
                table: "MapRequestLogs",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_FromUserId",
                table: "Notifications",
                column: "FromUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_Tenant_Id",
                table: "Notifications",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_ToUserId",
                table: "Notifications",
                column: "ToUserId");

            migrationBuilder.CreateIndex(
                name: "IX_PACI_Tenant_Id",
                table: "PACI",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Restaurant_Tenant_Id",
                table: "Restaurant",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Tenant_Id",
                table: "Settings",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_SettingKey_Tenant_Id",
                table: "Settings",
                columns: new[] { "SettingKey", "Tenant_Id" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaskDriverRequests_MainTaskId",
                table: "TaskDriverRequests",
                column: "MainTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskDriverRequests_Tenant_Id",
                table: "TaskDriverRequests",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_TaskGallary_TaskId",
                table: "TaskGallary",
                column: "TaskId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskGallary_Tenant_Id",
                table: "TaskGallary",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_TaskHistory_FromStatusId",
                table: "TaskHistory",
                column: "FromStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskHistory_TaskId",
                table: "TaskHistory",
                column: "TaskId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskHistory_Tenant_Id",
                table: "TaskHistory",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_TaskHistory_ToStatusId",
                table: "TaskHistory",
                column: "ToStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskRoute_DriverId",
                table: "TaskRoute",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskRoute_TaskId",
                table: "TaskRoute",
                column: "TaskId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskRoute_TaskStatusId",
                table: "TaskRoute",
                column: "TaskStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskRoute_Tenant_Id",
                table: "TaskRoute",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_BranchId",
                table: "Tasks",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_CustomerId",
                table: "Tasks",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_DriverId",
                table: "Tasks",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_GeoFenceId",
                table: "Tasks",
                column: "GeoFenceId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_MainTaskId",
                table: "Tasks",
                column: "MainTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_TaskStatusId",
                table: "Tasks",
                column: "TaskStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_TaskTypeId",
                table: "Tasks",
                column: "TaskTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_Tenant_Id",
                table: "Tasks",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_TeamManager_ManagerId",
                table: "TeamManager",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_TeamManager_TeamId",
                table: "TeamManager",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Teams_LocationAccuracyId",
                table: "Teams",
                column: "LocationAccuracyId");

            migrationBuilder.CreateIndex(
                name: "IX_Teams_Tenant_Id",
                table: "Teams",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_UserDevice_UserId",
                table: "UserDevice",
                column: "UserId");

            SeedLookups(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountLogs");

            migrationBuilder.DropTable(
                name: "Admins");

            migrationBuilder.DropTable(
                name: "Area");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "DriverCurrentLocation");

            migrationBuilder.DropTable(
                name: "DriverDeliveryGeoFence");

            migrationBuilder.DropTable(
                name: "DriverLoginRequests");

            migrationBuilder.DropTable(
                name: "DriverloginTracking");

            migrationBuilder.DropTable(
                name: "DriverPickUpGeoFence");

            migrationBuilder.DropTable(
                name: "DriverRate");

            migrationBuilder.DropTable(
                name: "DriverTaskNotification");

            migrationBuilder.DropTable(
                name: "GeoFenceLocation");

            migrationBuilder.DropTable(
                name: "ManagerDispatching");

            migrationBuilder.DropTable(
                name: "MapRequestLogs");

            migrationBuilder.DropTable(
                name: "Notifications");

            migrationBuilder.DropTable(
                name: "PACI");

            migrationBuilder.DropTable(
                name: "Settings");

            migrationBuilder.DropTable(
                name: "TaskDriverRequests");

            migrationBuilder.DropTable(
                name: "TaskGallary");

            migrationBuilder.DropTable(
                name: "TaskHistory");

            migrationBuilder.DropTable(
                name: "TaskRoute");

            migrationBuilder.DropTable(
                name: "TeamManager");

            migrationBuilder.DropTable(
                name: "UserDevice");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Manager");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Driver");

            migrationBuilder.DropTable(
                name: "MainTask");

            migrationBuilder.DropTable(
                name: "TaskStatus");

            migrationBuilder.DropTable(
                name: "TaskType");

            migrationBuilder.DropTable(
                name: "Branch");

            migrationBuilder.DropTable(
                name: "AgentStatus");

            migrationBuilder.DropTable(
                name: "AgentType");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "TransportType");

            migrationBuilder.DropTable(
                name: "MainTaskStatus");

            migrationBuilder.DropTable(
                name: "MainTaskType");

            migrationBuilder.DropTable(
                name: "GeoFence");

            migrationBuilder.DropTable(
                name: "Restaurant");

            migrationBuilder.DropTable(
                name: "LocationAccuracy");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Country");
        }

        private void SeedLookups(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"
BEGIN TRANSACTION
-- ---
-- AgentStatus
-- ---
GO
SET IDENTITY_INSERT [dbo].[AgentStatus] ON 

GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (1, N'Offline', N'0', N'0', N'0', 0, CAST(N'2020-03-04 00:00:00.0000000' AS DateTime2), CAST(N'2020-03-04 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (2, N'Available', N'0', N'0', N'0', 0, CAST(N'2020-03-04 00:00:00.0000000' AS DateTime2), CAST(N'2020-03-04 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (3, N'Unavailable', N'0', N'0', N'0', 0, CAST(N'2020-03-04 00:00:00.0000000' AS DateTime2), CAST(N'2020-03-04 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (4, N'Busy', N'0', N'0', N'0', 0, CAST(N'2020-03-04 00:00:00.0000000' AS DateTime2), CAST(N'2020-03-04 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[AgentStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (5, N'Blocked', NULL, NULL, NULL, 0, CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[AgentStatus] OFF
GO

-- ---
-- AgentType
-- ---
SET IDENTITY_INSERT [dbo].[AgentType] ON 

GO
INSERT [dbo].[AgentType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (1, N'Captive', NULL, NULL, NULL, 0, CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2))
GO
INSERT [dbo].[AgentType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (2, N'Freelancer', NULL, NULL, NULL, 0, CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[AgentType] OFF
GO

-- ---
-- MainTaskStatus
-- ---
SET IDENTITY_INSERT [dbo].[MainTaskStatus] ON 

GO
INSERT [dbo].[MainTaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (1, N'Assigned', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[MainTaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (2, N'Inprogress', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[MainTaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (3, N'Successful', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[MainTaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (4, N'Failed', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[MainTaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (5, N'Accepted', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[MainTaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (9, N'Canceled', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'2001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'2001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[MainTaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (10, N'Declined', NULL, NULL, NULL, 0, CAST(N'2001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'2001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'2001-01-01 00:00:00.0000000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[MainTaskStatus] OFF
GO

-- ---
-- MainTaskType
-- ---
SET IDENTITY_INSERT [dbo].[MainTaskType] ON 

GO
INSERT [dbo].[MainTaskType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (1, N'Pickup & Delivery', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[MainTaskType] OFF
GO

-- ---
-- TaskStatus
-- ---
SET IDENTITY_INSERT [dbo].[TaskStatus] ON 

GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (1, N'Unassigned', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'#2F9E2C')
GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (2, N'Assigned', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'#9B9B9B')
GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (3, N'Accepted', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'#BA68C8')
GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (4, N'Started', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'#1393FF')
GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (5, N'In progress', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'Null')
GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (6, N'Successful', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'#2F9E2C')
GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (7, N'Failed', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'#D0021B')
GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (8, N'Declined', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'#9B9B9B')
GO
INSERT [dbo].[TaskStatus] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [Color]) VALUES (9, N'Cancelled', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), N'#9B9B9B')
GO
SET IDENTITY_INSERT [dbo].[TaskStatus] OFF
GO

-- ---
-- TaskType
-- ---
SET IDENTITY_INSERT [dbo].[TaskType] ON 

GO
INSERT [dbo].[TaskType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (1, N'Pickup', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[TaskType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (2, N'Delivery', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[TaskType] OFF
GO

-- ---
-- TransportType
-- ---
SET IDENTITY_INSERT [dbo].[TransportType] ON 

GO
INSERT [dbo].[TransportType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (1, N'Truck', NULL, NULL, NULL, 0, CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2))
GO
INSERT [dbo].[TransportType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (2, N'Car', NULL, NULL, NULL, 0, CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2))
GO
INSERT [dbo].[TransportType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (4, N'Scooter', NULL, NULL, NULL, 0, CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2))
GO
INSERT [dbo].[TransportType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (5, N'Cycle', NULL, NULL, NULL, 0, CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2))
GO
INSERT [dbo].[TransportType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (6, N'Walk', NULL, NULL, NULL, 0, CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2), CAST(N'2020-02-17 14:13:32.3677566' AS DateTime2))
GO
INSERT [dbo].[TransportType] ([Id], [Name], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate]) VALUES (10, N'Drone', NULL, NULL, NULL, 0, CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
SET IDENTITY_INSERT [dbo].[TransportType] OFF
GO

-- ---
-- Area
-- ---
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'101', CAST(N'2019-05-27 11:24:41.1851657' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Dasman', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'102', CAST(N'2019-05-27 11:25:01.6710382' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Sharq', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'103', CAST(N'2019-05-27 11:24:44.2182113' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Mirqab', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'104', CAST(N'2019-05-27 11:24:54.5983837' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Al Sour Gardens', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'105', CAST(N'2019-05-27 11:25:04.4744529' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Qibla', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'106', CAST(N'2019-05-27 11:24:39.6789196' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Mansouriya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'107', CAST(N'2019-05-27 11:24:47.7787553' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Dasma', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'108', CAST(N'2019-05-27 11:25:05.8341195' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Bnaid Al-Qar', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'109', CAST(N'2019-05-27 11:24:48.5627202' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Shamiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'110', CAST(N'2019-05-27 11:24:50.1904972' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Abdulla Al-Salem', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'111', CAST(N'2019-05-27 11:24:43.4678186' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Qadsiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'112', CAST(N'2019-05-27 11:24:44.9586233' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Daiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'113', CAST(N'2019-05-27 11:25:03.7715580' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Faiha', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'114', CAST(N'2019-05-27 11:24:40.4335386' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Nuzha', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'115', CAST(N'2019-05-27 11:24:57.5926786' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Shuwaikh', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'116', CAST(N'2019-05-27 11:24:53.0753461' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Kifan', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'117', CAST(N'2019-05-27 11:24:55.3843930' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Mubarakiya Camps', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'118', CAST(N'2019-05-27 11:24:47.0224431' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Shuwaikh Industrial-2', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'119', CAST(N'2019-05-27 11:25:07.2664391' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Ghornata', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'120', CAST(N'2019-05-27 11:24:59.2502257' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Failaka Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'121', CAST(N'2019-05-27 11:26:51.0712994' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Warba Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'122', CAST(N'2019-05-27 11:26:36.2903676' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Bubyan Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'123', CAST(N'2019-05-27 11:24:38.9623174' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Mischan Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'124', CAST(N'2019-05-27 11:24:37.6012049' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Auha Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'125', CAST(N'2019-05-27 11:24:38.2741473' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Kubbar Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'126', CAST(N'2019-05-27 11:24:42.7532541' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Qaruh Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'127', CAST(N'2019-05-27 11:24:42.0705405' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Umm Al-Maradim Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'128', CAST(N'2019-05-27 11:24:51.7113241' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Umm Al-Namel Island', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'129', CAST(N'2019-05-27 11:24:58.4216495' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Shuwaikh Industrial-1', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'130', CAST(N'2019-05-27 11:24:45.6241313' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Shuwaikh Industrial-3', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'131', CAST(N'2019-05-27 11:25:08.0336930' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Health Area', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'132', CAST(N'2019-05-27 11:25:00.8635748' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'The Sea Front', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'133', CAST(N'2019-05-27 11:25:03.1043503' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Shuwaikh Port', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'134', CAST(N'2019-05-27 11:24:56.8840891' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Northwest Sulaibikhat', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'201', CAST(N'2019-05-27 11:24:50.9791273' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Khaldiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'202', CAST(N'2019-05-27 11:24:46.3361124' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Adailiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'203', CAST(N'2019-05-27 11:24:52.3945139' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Rawda', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'204', CAST(N'2019-05-27 11:25:35.6283498' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Hawalli', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'205', CAST(N'2019-05-27 11:25:38.6899548' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Shaab', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'206', CAST(N'2019-05-27 11:27:17.3240968' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Al Masayel', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'207', CAST(N'2019-05-27 11:25:30.2294516' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Salmiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'208', CAST(N'2019-05-27 11:25:37.1122554' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Rumaithiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'209', CAST(N'2019-05-27 11:25:29.5511464' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Mishrif', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'210', CAST(N'2019-05-27 11:25:37.9077509' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Salwa', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'211', CAST(N'2019-05-27 11:27:19.6533603' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Messila', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'212', CAST(N'2019-05-27 11:27:16.5708599' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Sabah Al-Salem', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'213', CAST(N'2019-05-27 11:25:31.6753912' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Bayan', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'214', CAST(N'2019-05-27 11:24:49.3558352' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Yarmouk', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'215', CAST(N'2019-05-27 11:25:06.5459985' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Qortuba', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'216', CAST(N'2019-05-27 11:24:53.7762957' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Surra', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'217', CAST(N'2019-05-27 11:25:30.8838997' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Jabriya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'220', CAST(N'2019-05-27 11:27:00.9305964' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Farwaniya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'221', CAST(N'2019-05-27 11:27:07.7632729' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Rai', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'222', CAST(N'2019-05-27 11:26:58.5807162' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Omariya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'223', CAST(N'2019-05-27 11:27:02.2840648' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Rabiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'224', CAST(N'2019-05-27 11:26:59.4176096' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Riggai', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'225', CAST(N'2019-05-27 11:27:04.4238780' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Andalus', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'226', CAST(N'2019-05-27 11:27:03.7655886' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Ardhiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'227', CAST(N'2019-05-27 11:27:09.7801694' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Ferdous', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'228', CAST(N'2019-05-27 11:27:08.4215598' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Jleeb Al-Shiyoukh', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'229', CAST(N'2019-05-27 11:25:34.8000177' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Ministries Area', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'230', CAST(N'2019-05-27 11:25:28.7926928' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Shuhada', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'231', CAST(N'2019-05-27 11:25:28.1047040' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Al Bida''a', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'232', CAST(N'2019-05-27 11:27:12.2684161' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Subhan Industrial', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'233', CAST(N'2019-05-27 11:27:10.6572868' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'International Airport', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'234', CAST(N'2019-05-27 11:25:33.4014882' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Zahra', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'235', CAST(N'2019-05-27 11:25:27.4022159' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Hitteen', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'236', CAST(N'2019-05-27 11:25:36.3845331' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Al-Siddiq', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'237', CAST(N'2019-05-27 11:25:32.7223244' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Salam', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'239', CAST(N'2019-05-27 11:25:39.5133948' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Anjafa', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'241', CAST(N'2019-05-27 11:27:01.6276774' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Sabah Al-Nasser', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'242', CAST(N'2019-05-27 11:27:00.1494447' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Ardhiya 4', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'243', CAST(N'2019-05-27 11:27:03.0650165' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Ashbeliah', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'244', CAST(N'2019-05-27 11:26:56.1691841' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Ardhiya 6', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'245', CAST(N'2019-05-27 11:27:18.8731924' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Wista', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'246', CAST(N'2019-05-27 11:26:57.7832724' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Dajeej', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'247', CAST(N'2019-05-27 11:27:09.0999617' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Rehab', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'248', CAST(N'2019-05-27 11:27:21.0300776' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Al-Adan', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'249', CAST(N'2019-05-27 11:27:15.8646403' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Al-Qusour', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'250', CAST(N'2019-05-27 11:27:15.1929833' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Al-Qurain', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'251', CAST(N'2019-05-27 11:27:13.7108846' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Mubarak Al-Kabeer', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'252', CAST(N'2019-05-27 11:27:05.0842910' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Khaitan', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'253', CAST(N'2019-05-27 11:25:34.0977803' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Mubarakyia', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'301', CAST(N'2019-05-27 11:25:47.8777244' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Egaila', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'302', CAST(N'2019-05-27 11:25:43.4699217' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Riqqa', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'303', CAST(N'2019-05-27 11:25:47.1275322' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Hadiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'305', CAST(N'2019-05-27 11:26:17.1155663' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Al-Fintas', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'306', CAST(N'2019-05-27 11:26:10.9869965' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Mahboula', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'307', CAST(N'2019-05-27 11:26:09.4648698' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Abu Halifa', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'308', CAST(N'2019-05-27 11:26:15.7814396' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Mangaf', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'309', CAST(N'2019-05-27 11:25:52.8804377' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabahiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'310', CAST(N'2019-05-27 11:26:12.3644328' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Fahaheel', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'311', CAST(N'2019-05-27 11:25:58.3757990' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shuaiba Industrial esterly', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'312', CAST(N'2019-05-27 11:25:52.0962854' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Ali Subah Al-Salem', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'314', CAST(N'2019-05-27 11:25:53.5570078' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'East Ahmadi', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'315', CAST(N'2019-05-27 11:26:01.2034176' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Wafra', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'316', CAST(N'2019-05-27 11:25:46.4265466' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Dhaher', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'317', CAST(N'2019-05-27 11:26:04.0905343' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shalehat Al-Nuwaiseeb', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'318', CAST(N'2019-05-27 11:26:04.8216824' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shalehat Al-Khiran', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'319', CAST(N'2019-05-27 11:26:08.5889395' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'New Wafra', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'320', CAST(N'2019-05-27 11:25:54.9356019' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Mina Abdulla', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'321', CAST(N'2019-05-27 11:25:57.0169448' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Ahmadi Governorate Desert', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'322', CAST(N'2019-05-27 11:26:18.4967720' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Magwa', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'324', CAST(N'2019-05-27 11:27:18.0885152' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Abu Hassaniah', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'325', CAST(N'2019-05-27 11:26:00.4366514' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Zoor', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'326', CAST(N'2019-05-27 11:27:13.0262476' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Abu Ftaira', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'329', CAST(N'2019-05-27 11:25:59.0629453' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Al-Nuwaiseeb', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'330', CAST(N'2019-05-27 11:25:50.3840542' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'South Ahmadi', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'332', CAST(N'2019-05-27 11:26:15.0657921' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Middle of Ahmadi', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'333', CAST(N'2019-05-27 11:25:49.6505638' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'North Ahmadi', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'337', CAST(N'2019-05-27 11:26:14.4044049' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Wafra Farms', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'338', CAST(N'2019-05-27 11:25:45.0832242' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'South-Sabahiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'340', CAST(N'2019-05-27 11:25:44.1857312' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Jaber Al-Ali', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'342', CAST(N'2019-05-27 11:25:41.3761874' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shalehat Zoor', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'343', CAST(N'2019-05-27 11:26:16.4712498' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shalehat Jlea''a', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'344', CAST(N'2019-05-27 11:26:13.7367576' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shalehat Bneder', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'345', CAST(N'2019-05-27 11:25:08.7312221' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Shalehat Doha', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'346', CAST(N'2019-05-27 11:26:17.7797564' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shalehat Dba''ayeh', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'347', CAST(N'2019-05-27 11:26:47.1949566' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Shalehat Kazima', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'348', CAST(N'2019-05-27 11:26:28.6432398' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Shalehat Subiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'349', CAST(N'2019-05-27 11:25:54.2399904' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shalehat Mina Abdullah', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'350', CAST(N'2019-05-27 11:25:57.6959087' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Rajim Khashman', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'351', CAST(N'2019-05-27 11:25:59.7396010' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad Al-marine', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'352', CAST(N'2019-05-27 11:26:07.7640237' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad 1', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'353', CAST(N'2019-05-27 11:26:05.5677457' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad 2', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'354', CAST(N'2019-05-27 11:26:03.4221081' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad 3', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'355', CAST(N'2019-05-27 11:26:06.2352730' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad 4', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'356', CAST(N'2019-05-27 11:25:56.3417738' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad 5', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'357', CAST(N'2019-05-27 11:26:02.0399384' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad Services', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'358', CAST(N'2019-05-27 11:26:06.9288062' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad Investment', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'359', CAST(N'2019-05-27 11:25:42.1287630' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Khiran City', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'401', CAST(N'2019-05-27 11:24:56.2304886' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Doha', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'402', CAST(N'2019-05-27 11:25:02.4357521' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Sulaibikhat', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'403', CAST(N'2019-05-27 11:26:44.3137859' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Jahra', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'404', CAST(N'2019-05-27 11:26:25.6578196' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Qasr', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'405', CAST(N'2019-05-27 11:26:21.4840496' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Naeem', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'407', CAST(N'2019-05-27 11:26:24.9378293' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Sulaibiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'408', CAST(N'2019-05-27 11:26:22.8306458' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Sulaibiya Industrial 1', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'409', CAST(N'2019-05-27 11:26:34.1987931' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Abdally', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'410', CAST(N'2019-05-27 11:26:45.6823461' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Amghara Industrial', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'412', CAST(N'2019-05-27 11:26:54.5966865' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Al Mutlaa', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'413', CAST(N'2019-05-27 11:26:43.6222520' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Kazima', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'414', CAST(N'2019-05-27 11:26:53.8620533' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Rawdatain', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'415', CAST(N'2019-05-27 11:26:53.1614612' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Umm Al-Aish', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'416', CAST(N'2019-05-27 11:26:30.7346198' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Salmy', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'417', CAST(N'2019-05-27 11:26:27.9635874' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Kabd', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'418', CAST(N'2019-05-27 11:26:37.7378057' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Bar Al-Jahra Governorate', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'419', CAST(N'2019-05-27 11:26:38.4062660' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Taima', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'420', CAST(N'2019-05-27 11:26:51.7570625' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Waha', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'421', CAST(N'2019-05-27 11:26:34.8832226' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Al Sheqaya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'422', CAST(N'2019-05-27 11:26:48.0493582' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Subiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'423', CAST(N'2019-05-27 11:26:44.9912754' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Sulaibiya Industrial 2', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'424', CAST(N'2019-05-27 11:26:37.0123432' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Sulaibiya Agricultural', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'426', CAST(N'2019-05-27 11:25:09.4179607' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Mina Doha', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'427', CAST(N'2019-05-27 11:26:42.9320407' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Oyoun', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'428', CAST(N'2019-05-27 11:26:52.4361669' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Jahra Camps', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'429', CAST(N'2019-05-27 11:26:23.5131701' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Nasseem', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'430', CAST(N'2019-05-27 11:27:20.3467641' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'Al-Fnaitees', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'431', CAST(N'2019-05-27 11:26:50.2366095' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Jahra-Industrial', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'432', CAST(N'2019-05-27 11:25:51.2408923' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Janobyia Aljawakheer', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'433', CAST(N'2019-05-27 11:25:45.7525355' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sulaibyia Industrial 3', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'434', CAST(N'2019-05-27 11:26:10.2177401' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Shuaiba Industrial Western', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'435', CAST(N'2019-05-27 11:26:22.1487972' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Nahda', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'436', CAST(N'2019-05-27 11:25:26.7320913' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'2', NULL, 0, N'Mubarak Al-Abdullah', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'437', CAST(N'2019-05-27 11:26:27.3114623' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Saad Al-Abdulla City', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'438', CAST(N'2019-05-27 11:27:06.3954838' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Abdullah Mubarak Al-Sabah', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'439', CAST(N'2019-05-27 11:25:42.8082108' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Fahad Al-Ahmad', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'440', CAST(N'2019-05-27 11:26:24.2262783' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Kaerawan', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'441', CAST(N'2019-05-27 11:26:20.8215107' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Jawakher Al Jahra', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'442', CAST(N'2019-05-27 11:26:30.0137786' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Bhaith', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'443', CAST(N'2019-05-27 11:27:14.4148436' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'6', NULL, 0, N'West Abu Ftirah Hirafyia', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'444', CAST(N'2019-05-27 11:26:26.4331311' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'North West Jahra', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'445', CAST(N'2019-05-27 11:26:57.0160672' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Ardhiya Herafiya', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'446', CAST(N'2019-05-27 11:25:05.1602926' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Jaber Al-Ahmad', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'447', CAST(N'2019-05-27 11:25:55.6374074' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'kabd Agricultural', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'448', CAST(N'2019-05-27 11:26:46.4202651' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'Al Naayem', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'450', CAST(N'2019-05-27 11:27:07.0496252' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'West Abdullah Al-Mubarak', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'451', CAST(N'2019-05-27 11:26:40.6602799' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'453', CAST(N'2019-05-27 11:26:11.6448505' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Mina Abdullah Refinery', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'454', CAST(N'2019-05-27 11:26:13.0490159' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Mina Al-Ahmadi Refinery', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'455', CAST(N'2019-05-27 11:25:00.0856053' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'1', NULL, 0, N'Sulaibikhat Cemetery', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'456', CAST(N'2019-05-27 11:26:35.5441973' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Amghara', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'457', CAST(N'2019-05-27 11:27:05.7396166' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'5', NULL, 0, N'Sabah Al-Salem University City', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'458', CAST(N'2019-05-27 11:26:02.7467318' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Sabah Al-Ahmad 6', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'459', CAST(N'2019-05-27 11:25:48.8189551' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'3', NULL, 0, N'Al shadadyia Industrial', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'901', CAST(N'2019-05-27 11:26:41.5105825' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 1', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'902', CAST(N'2019-05-27 11:26:42.2545227' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 2', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'903', CAST(N'2019-05-27 11:26:39.1226174' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 3', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'904', CAST(N'2019-05-27 11:26:29.3321192' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 4', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'905', CAST(N'2019-05-27 11:26:48.6991424' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 5', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'906', CAST(N'2019-05-27 11:26:31.3994686' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 6', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'907', CAST(N'2019-05-27 11:26:39.8764524' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 7', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'908', CAST(N'2019-05-27 11:26:32.0850317' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 8', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'909', CAST(N'2019-05-27 11:26:20.1462323' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 9', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'911', CAST(N'2019-05-27 11:26:33.5410154' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 11', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'912', CAST(N'2019-05-27 11:26:49.3847529' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 12', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[Area] ([Id], [CreatedDate], [DeletedDate], [CreatedBy_Id], [DeletedBy_Id], [FK_Governrate_Id], [UpdatedBy_Id], [IsDeleted], [Name], [UpdatedDate]) VALUES (N'920', CAST(N'2019-05-27 11:26:32.8396006' AS DateTime2), CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2), NULL, NULL, N'4', NULL, 0, N'South Al Mutlaa 10', CAST(N'0001-01-01 00:00:00.0000000' AS DateTime2))
GO

-- ---
-- Country
-- ---
SET IDENTITY_INSERT [dbo].[Country] ON 

GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (10, N'Afghanistan', N'93', N'af.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'af')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (11, N'Albania', N'355', N'al.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'al')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (12, N'Algeria', N'213', N'dz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'dz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (14, N'Andorra', N'376', N'ad.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ad')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (15, N'Angola', N'244', N'ao.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ao')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (17, N'Antarctica', N'672', N'aq.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'aq')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (19, N'Argentina', N'54', N'ar.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ar')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (20, N'Armenia', N'374', N'am.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'am')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (21, N'Aruba', N'297', N'aw.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'aw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (22, N'Australia', N'61', N'au.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'au')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (23, N'Austria', N'43', N'at.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'at')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (24, N'Azerbaijan', N'994', N'az.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'az')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (26, N'Bahrain', N'973', N'bh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (27, N'Bangladesh', N'880', N'bd.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bd')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (29, N'Belarus', N'375', N'by.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'by')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (30, N'Belgium', N'32', N'be.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'be')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (31, N'Belize', N'501', N'bz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (32, N'Benin', N'229', N'bj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (34, N'Bhutan', N'975', N'bt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (35, N'Bolivia', N'591', N'bo.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bo')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (36, N'Bosnia and Herzegovina', N'387', N'ba.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ba')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (37, N'Botswana', N'267', N'bw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (38, N'Brazil', N'55', N'br.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'br')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (39, N'British Indian Ocean Territory', N'246', N'io.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'io')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (41, N'Brunei', N'673', N'bn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (42, N'Bulgaria', N'359', N'bg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (43, N'Burkina Faso', N'226', N'bf.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bf')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (44, N'Burundi', N'257', N'bi.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'bi')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (45, N'Cambodia', N'855', N'kh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'kh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (46, N'Cameroon', N'237', N'cm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (47, N'Canada', N'1', N'ca.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ca')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (48, N'Cape Verde', N'238', N'cv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (50, N'Central African Republic', N'236', N'cf.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cf')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (51, N'Chad', N'235', N'td.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'td')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (52, N'Chile', N'56', N'cl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (53, N'China', N'86', N'cn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (54, N'Christmas Island', N'61', N'cx.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cx')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (55, N'Cocos Islands', N'61', N'cc.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cc')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (56, N'Colombia', N'57', N'co.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'co')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (57, N'Comoros', N'269', N'km.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'km')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (58, N'Cook Islands', N'682', N'ck.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ck')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (59, N'Costa Rica', N'506', N'cr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (60, N'Croatia', N'385', N'hr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'hr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (61, N'Cuba', N'53', N'cu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (62, N'Curacao', N'599', N'cw.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (63, N'Cyprus', N'357', N'cy.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cy')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (64, N'Czech Republic', N'420', N'cz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (65, N'Democratic Republic of the Congo', N'243', N'cd.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cd')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (66, N'Denmark', N'45', N'dk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'dk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (67, N'Djibouti', N'253', N'dj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'dj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (70, N'East Timor', N'670', N'tl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (71, N'Ecuador', N'593', N'ec.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ec')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (72, N'Egypt', N'20', N'eg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'eg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (73, N'El Salvador', N'503', N'sv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (74, N'Equatorial Guinea', N'240', N'gq.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gq')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (75, N'Eritrea', N'291', N'er.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'er')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (76, N'Estonia', N'372', N'ee.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ee')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (77, N'Ethiopia', N'251', N'et.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'et')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (78, N'Falkland Islands', N'500', N'fk.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'fk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (79, N'Faroe Islands', N'298', N'fo.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'fo')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (80, N'Fiji', N'679', N'fj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'fj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (81, N'Finland', N'358', N'fi.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'fi')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (82, N'France', N'33', N'fr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'fr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (83, N'French Polynesia', N'689', N'pf.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pf')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (84, N'Gabon', N'241', N'ga.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ga')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (85, N'Gambia', N'220', N'gm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (86, N'Georgia', N'995', N'ge.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ge')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (87, N'Germany', N'49', N'de.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'de')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (88, N'Ghana', N'233', N'gh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (89, N'Gibraltar', N'350', N'gi.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gi')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (90, N'Greece', N'30', N'gr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (91, N'Greenland', N'299', N'gl.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (94, N'Guatemala', N'502', N'gt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (96, N'Guinea', N'224', N'gn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (97, N'Guinea-Bissau', N'245', N'gw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (98, N'Guyana', N'592', N'gy.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gy')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (99, N'Haiti', N'509', N'ht.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ht')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (100, N'Honduras', N'504', N'hn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'hn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (101, N'Hong Kong', N'852', N'hk.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'hk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (102, N'Hungary', N'36', N'hu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'hu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (103, N'Iceland', N'354', N'is.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'is')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (104, N'India', N'91', N'in.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'in')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (105, N'Indonesia', N'62', N'id.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'id')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (106, N'Iran', N'98', N'ir.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ir')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (107, N'Iraq', N'964', N'iq.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'iq')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (108, N'Ireland', N'353', N'ie.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ie')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (110, N'Israel', N'972', N'il.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'il')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (111, N'Italy', N'39', N'it.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'it')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (112, N'Ivory Coast', N'225', N'ci.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ci')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (114, N'Japan', N'81', N'jp.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'jp')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (116, N'Jordan', N'962', N'jo.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'jo')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (117, N'Kazakhstan', N'7', N'kz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'kz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (118, N'Kenya', N'254', N'ke.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ke')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (119, N'Kiribati', N'686', N'ki.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ki')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (121, N'Kuwait', N'965', N'kw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'kw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (122, N'Kyrgyzstan', N'996', N'kg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'kg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (123, N'Laos', N'856', N'la.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'la')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (124, N'Latvia', N'371', N'lv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'lv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (125, N'Lebanon', N'961', N'lb.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'lb')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (126, N'Lesotho', N'266', N'ls.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ls')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (127, N'Liberia', N'231', N'lr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'lr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (128, N'Libya', N'218', N'ly.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ly')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (129, N'Liechtenstein', N'423', N'li.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'li')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (130, N'Lithuania', N'370', N'lt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'lt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (131, N'Luxembourg', N'352', N'lu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'lu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (132, N'Macau', N'853', N'mo.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mo')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (133, N'Macedonia', N'389', N'mk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (134, N'Madagascar', N'261', N'mg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (135, N'Malawi', N'265', N'mw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (136, N'Malaysia', N'60', N'my.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'my')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (137, N'Maldives', N'960', N'mv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (138, N'Mali', N'223', N'ml.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ml')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (139, N'Malta', N'356', N'mt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (140, N'Marshall Islands', N'692', N'mh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (141, N'Mauritania', N'222', N'mr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (142, N'Mauritius', N'230', N'mu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (143, N'Mayotte', N'262', N'yt.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'yt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (144, N'Mexico', N'52', N'mx.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mx')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (145, N'Micronesia', N'691', N'fm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'fm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (146, N'Moldova', N'373', N'md.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'md')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (147, N'Monaco', N'377', N'mc.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mc')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (148, N'Mongolia', N'976', N'mn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (149, N'Montenegro', N'382', N'me.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'me')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (151, N'Morocco', N'212', N'ma.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ma')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (152, N'Mozambique', N'258', N'mz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (153, N'Myanmar', N'95', N'mm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'mm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (154, N'Namibia', N'264', N'na.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'na')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (155, N'Nauru', N'674', N'nr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'nr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (156, N'Nepal', N'977', N'np.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'np')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (157, N'Netherlands', N'31', N'nl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'nl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (158, N'Netherlands Antilles', N'599', N'an.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'an')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (159, N'New Caledonia', N'687', N'nc.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'nc')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (160, N'New Zealand', N'64', N'nz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'nz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (161, N'Nicaragua', N'505', N'ni.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ni')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (162, N'Niger', N'227', N'ne.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ne')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (163, N'Nigeria', N'234', N'ng.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ng')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (164, N'Niue', N'683', N'nu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'nu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (165, N'North Korea', N'850', N'kp.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'kp')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (167, N'Norway', N'47', N'no.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'no')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (168, N'Oman', N'968', N'om.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'om')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (169, N'Pakistan', N'92', N'pk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (170, N'Palau', N'680', N'pw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (171, N'Palestine', N'970', N'ps.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ps')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (172, N'Panama', N'507', N'pa.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pa')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (173, N'Papua New Guinea', N'675', N'pg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (174, N'Paraguay', N'595', N'py.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'py')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (175, N'Peru', N'51', N'pe.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pe')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (176, N'Philippines', N'63', N'ph.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ph')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (177, N'Pitcairn', N'64', N'pn.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (178, N'Poland', N'48', N'pl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (179, N'Portugal', N'351', N'pt.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pt')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (181, N'Qatar', N'974', N'qa.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'qa')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (182, N'Republic of the Congo', N'242', N'cg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'cg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (183, N'Reunion', N'262', N're.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N're')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (184, N'Romania', N'40', N'ro.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ro')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (185, N'Russia', N'7', N'ru.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ru')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (186, N'Rwanda', N'250', N'rw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'rw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (187, N'Saint Barthelemy', N'590', N'gp.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gp')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (188, N'Saint Helena', N'290', N'sh.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (191, N'Saint Martin', N'590', N'gp.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'gp')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (192, N'Saint Pierre and Miquelon', N'508', N'pm.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'pm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (194, N'Samoa', N'685', N'ws.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ws')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (195, N'San Marino', N'378', N'sm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (196, N'Sao Tome and Principe', N'239', N'st.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'st')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (197, N'Saudi Arabia', N'966', N'sa.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sa')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (198, N'Senegal', N'221', N'sn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (199, N'Serbia', N'381', N'rs.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'rs')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (200, N'Seychelles', N'248', N'sc.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sc')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (201, N'Sierra Leone', N'232', N'sl.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sl')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (202, N'Singapore', N'65', N'sg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (204, N'Slovakia', N'421', N'sk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (205, N'Slovenia', N'386', N'si.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'si')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (206, N'Solomon Islands', N'677', N'sb.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sb')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (207, N'Somalia', N'252', N'so.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'so')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (208, N'South Africa', N'27', N'za.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'za')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (209, N'South Korea', N'82', N'kr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'kr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (210, N'South Sudan', N'211', N'ss.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ss')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (211, N'Spain', N'34', N'es.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'es')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (212, N'Sri Lanka', N'94', N'lk.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'lk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (213, N'Sudan', N'249', N'sd.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sd')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (214, N'Suriname', N'597', N'sr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (215, N'Svalbard and Jan Mayen', N'47', N'sj.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (216, N'Swaziland', N'268', N'sz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (217, N'Sweden', N'46', N'se.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'se')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (218, N'Switzerland', N'41', N'ch.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ch')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (219, N'Syria', N'963', N'sy.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'sy')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (220, N'Taiwan', N'886', N'tw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tw')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (221, N'Tajikistan', N'992', N'tj.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tj')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (222, N'Tanzania', N'255', N'tz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (223, N'Thailand', N'66', N'th.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'th')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (224, N'Togo', N'228', N'tg.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tg')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (225, N'Tokelau', N'690', N'tk.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (226, N'Tonga', N'676', N'to.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'to')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (228, N'Tunisia', N'216', N'tn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (229, N'Turkey', N'90', N'tr.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tr')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (230, N'Turkmenistan', N'993', N'tm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (232, N'Tuvalu', N'688', N'tv.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'tv')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (234, N'Uganda', N'256', N'ug.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ug')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (235, N'Ukraine', N'380', N'ua.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ua')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (236, N'United Arab Emirates', N'971', N'ae.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ae')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (237, N'United Kingdom', N'44', N'uk.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'uk')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (238, N'United States', N'1', N'us.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'us')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (239, N'Uruguay', N'598', N'uy.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'uy')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (240, N'Uzbekistan', N'998', N'uz.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'uz')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (241, N'Vanuatu', N'678', N'vu.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'vu')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (242, N'Vatican', N'379', N'va.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'va')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (243, N'Venezuela', N'58', N've.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N've')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (244, N'Vietnam', N'84', N'vn.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'vn')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (245, N'Wallis and Futuna', N'681', N'wf.webp', NULL, NULL, NULL, 1, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'wf')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (246, N'Western Sahara', N'212', N'eh.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'eh')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (247, N'Yemen', N'967', N'ye.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'ye')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (248, N'Zambia', N'260', N'zm.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'zm')
GO
INSERT [dbo].[Country] ([Id], [Name], [Code], [Flag], [CreatedBy_Id], [DeletedBy_Id], [UpdatedBy_Id], [IsDeleted], [UpdatedDate], [CreatedDate], [DeletedDate], [TopLevel]) VALUES (249, N'Zimbabwe', N'263', N'zw.webp', NULL, NULL, NULL, 0, CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), CAST(N'2020-03-10 11:27:12.8100000' AS DateTime2), N'zw')
GO
SET IDENTITY_INSERT [dbo].[Country] OFF
GO

COMMIT
");
        }
    }
}
