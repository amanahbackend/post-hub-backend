﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class driverRegistrationDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustomerCode",
                table: "Admins",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DriverRegistrations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    UserRegistrationStatus = table.Column<int>(nullable: false),
                    RejectReason = table.Column<string>(nullable: true),
                    CustomerCode = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: false),
                    AreaId = table.Column<string>(nullable: true),
                    JobStatusName = table.Column<string>(nullable: true),
                    TrainingRate = table.Column<int>(nullable: false),
                    TrainingLanguage = table.Column<string>(nullable: true),
                    TrainingComments = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(maxLength: 250, nullable: false),
                    FullName = table.Column<string>(maxLength: 250, nullable: false),
                    PhoneNumber = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    LicensePlate = table.Column<string>(nullable: true),
                    TransportTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverRegistrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverRegistrations_Area_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Area",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverRegistrations_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverRegistrations_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverRegistrations_TransportType_TransportTypeId",
                        column: x => x.TransportTypeId,
                        principalTable: "TransportType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TenantRegistrations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    UserRegistrationStatus = table.Column<int>(nullable: false),
                    RejectReason = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    BusinessType = table.Column<int>(nullable: false),
                    OtherBusinessType = table.Column<string>(nullable: true),
                    Latitude = table.Column<double>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    PACI = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: false),
                    GovernorateId = table.Column<string>(nullable: true),
                    AreaId = table.Column<string>(nullable: true),
                    Block = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    Building = table.Column<string>(nullable: true),
                    Floor = table.Column<string>(nullable: true),
                    Apartment = table.Column<string>(nullable: true),
                    OwnerName = table.Column<string>(nullable: true),
                    OwnerCID = table.Column<string>(nullable: true),
                    OwnerPhoneNumber = table.Column<string>(nullable: true),
                    IsSameBusinessAddress = table.Column<bool>(nullable: false),
                    OwnerAddress = table.Column<string>(nullable: true),
                    OwnerPACI = table.Column<string>(nullable: true),
                    Governorate = table.Column<string>(nullable: true),
                    OwnerAreaId = table.Column<string>(nullable: true),
                    OwnerBlock = table.Column<string>(nullable: true),
                    OwnerStreet = table.Column<string>(nullable: true),
                    OwnerBuilding = table.Column<string>(nullable: true),
                    OwnerFloor = table.Column<string>(nullable: true),
                    OwnerApartment = table.Column<string>(nullable: true),
                    HasOwnDrivers = table.Column<bool>(nullable: false),
                    HasMultiBranches = table.Column<bool>(nullable: false),
                    BranchCount = table.Column<int>(nullable: false),
                    IsDeliverToAllZones = table.Column<bool>(nullable: false),
                    ServingRadiusInKM = table.Column<double>(nullable: false),
                    NumberOfOrdersByDriverPerOrder = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantRegistrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantRegistrations_Area_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Area",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TenantRegistrations_Area_OwnerAreaId",
                        column: x => x.OwnerAreaId,
                        principalTable: "Area",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TenantRegistrations_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DriverAttachments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    DriverRegistrationId = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    File = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverAttachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverAttachments_DriverRegistrations_DriverRegistrationId",
                        column: x => x.DriverRegistrationId,
                        principalTable: "DriverRegistrations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverAttachments_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TenantRegistrationAttachments",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantRegistrationId = table.Column<int>(nullable: false),
                    File = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantRegistrationAttachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantRegistrationAttachments_TenantRegistrations_TenantRegistrationId",
                        column: x => x.TenantRegistrationId,
                        principalTable: "TenantRegistrations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TenantSocialMediaAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TenantRegistrationId = table.Column<int>(nullable: false),
                    AccountLink = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantSocialMediaAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantSocialMediaAccounts_TenantRegistrations_TenantRegistrationId",
                        column: x => x.TenantRegistrationId,
                        principalTable: "TenantRegistrations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DriverAttachments_DriverRegistrationId",
                table: "DriverAttachments",
                column: "DriverRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverAttachments_Tenant_Id",
                table: "DriverAttachments",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRegistrations_AreaId",
                table: "DriverRegistrations",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRegistrations_CountryId",
                table: "DriverRegistrations",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRegistrations_Tenant_Id",
                table: "DriverRegistrations",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRegistrations_TransportTypeId",
                table: "DriverRegistrations",
                column: "TransportTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantRegistrationAttachments_TenantRegistrationId",
                table: "TenantRegistrationAttachments",
                column: "TenantRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantRegistrations_AreaId",
                table: "TenantRegistrations",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantRegistrations_OwnerAreaId",
                table: "TenantRegistrations",
                column: "OwnerAreaId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantRegistrations_Tenant_Id",
                table: "TenantRegistrations",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_TenantSocialMediaAccounts_TenantRegistrationId",
                table: "TenantSocialMediaAccounts",
                column: "TenantRegistrationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DriverAttachments");

            migrationBuilder.DropTable(
                name: "TenantRegistrationAttachments");

            migrationBuilder.DropTable(
                name: "TenantSocialMediaAccounts");

            migrationBuilder.DropTable(
                name: "DriverRegistrations");

            migrationBuilder.DropTable(
                name: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "CustomerCode",
                table: "Admins");
        }
    }
}
