﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddingDriverWorkingHoursTracking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DriverWorkingHourTracking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    Tenant_Id = table.Column<string>(nullable: true),
                    DriverId = table.Column<int>(nullable: false),
                    ActivityDateTime = table.Column<DateTime>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    ActionBy = table.Column<string>(nullable: true),
                    Notes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverWorkingHourTracking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverWorkingHourTracking_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DriverWorkingHourTracking_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DriverWorkingHourTracking_DriverId",
                table: "DriverWorkingHourTracking",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverWorkingHourTracking_Tenant_Id",
                table: "DriverWorkingHourTracking",
                column: "Tenant_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DriverWorkingHourTracking");
        }
    }
}
