﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateDriverRegistrationDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "TenantRegistrations",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OwnerName",
                table: "TenantRegistrations",
                maxLength: 250,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TenantRegistrations",
                maxLength: 250,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "DriverRegistrations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TenantRegistrations_CountryId",
                table: "TenantRegistrations",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_TenantRegistrations_Country_CountryId",
                table: "TenantRegistrations",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TenantRegistrations_Country_CountryId",
                table: "TenantRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_TenantRegistrations_CountryId",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "Comment",
                table: "DriverRegistrations");

            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "OwnerName",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 250);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 250);
        }
    }
}
