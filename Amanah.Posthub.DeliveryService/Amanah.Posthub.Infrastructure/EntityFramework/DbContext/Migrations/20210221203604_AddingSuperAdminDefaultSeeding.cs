﻿using Amanah.Posthub.DATA.Users.EntityConfigurations;
using Amanah.Posthub.EntityConfigurations;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddingSuperAdminDefaultSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("delete from AspNetRoles where NormalizedName = 'SUPERADMIN'");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedBy_Id", "CreatedDate", "DeletedBy_Id", "DeletedDate", "IsDeleted", "Name", "NormalizedName", "Tenant_Id", "Type", "UpdatedBy_Id", "UpdatedDate" },
                values: new object[] { "EC7CFEE4-DDE1-4B37-95DE-E3458B2B9666", "4E0AE6D6-252F-4E15-B963-589B0C3F5389", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "SuperAdmin", "SUPERADMIN", null, 4, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CountryId", "CreatedBy_Id", "CreatedDate", "DeletedBy_Id", "DeletedDate", "Email", "EmailConfirmed", "FirstName", "IsAvailable", "IsDeleted", "IsLocked", "LastName", "LockoutEnabled", "LockoutEnd", "MiddleName", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Tenant_Id", "TwoFactorEnabled", "UpdatedBy_Id", "UpdatedDate", "UserName" },
                values: new object[] { "5D56A46B-72BF-4849-82D0-43851B574479", 0, "D5E28451-1E22-4DD8-A6E3-86330A71759C", 121, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "super-admin@amanah.com", true, "Super", true, false, false, "Admin", false, null, null, "SUPER-ADMIN@AMANAH.COM", "SUPER-ADMIN@AMANAH.COM", "AQAAAAEAACcQAAAAEPxqhST2073HVmQmXOTq5eC0sWJLNAj6yWL5dj3KG3pnQT/qE8GLDAazPObekZb84w==", null, false, "788F1971-0E77-49DC-BC49-EAE4A629BA66", null, false, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "super-admin@amanah.com" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "5D56A46B-72BF-4849-82D0-43851B574479", "EC7CFEE4-DDE1-4B37-95DE-E3458B2B9666" });
            AddSuperAdminClaims(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "5D56A46B-72BF-4849-82D0-43851B574479", "EC7CFEE4-DDE1-4B37-95DE-E3458B2B9666" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "EC7CFEE4-DDE1-4B37-95DE-E3458B2B9666");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "5D56A46B-72BF-4849-82D0-43851B574479");

        }

        private void AddSuperAdminClaims(MigrationBuilder migrationBuilder)
        {
            var permissions = new[]
            {
                "ManagerPermissions.Agent.ReadRequestAgent",
                "ManagerPermissions.Agent.EditRequestAgent",
                "ManagerPermissions.Agent.ApproveRequestAgent",
                "ManagerPermissions.Agent.RejectRequestAgent",
                "ManagerPermissions.Agent.ReadAgent",
                "ManagerPermissions.Agent.UpdateAgent",
                "ManagerPermissions.Agent.DeleteAgent",
                "ManagerPermissions.Agent.DeleteAllAgent",
                "ManagerPermissions.Agent.UpdateAllAgent",
                "ManagerPermissions.Agent.ChangeAgentPassword",
                "ManagerPermissions.Agent.ViewDriversLoginRequests",
                "PlatformManagerPermissions.Tenants.ReadDefaultPermissionTenants",
                "PlatformManagerPermissions.Tenants.ReadRequestTenant",
                "PlatformManagerPermissions.Tenants.ApproveRequestTenant",
                "PlatformManagerPermissions.Tenants.RejectRequestTenant",
                "PlatformManagerPermissions.Managers.ReadDefaultPermissionManagers",
                "PlatformManagerPermissions.Settings.AddManager",
                "PlatformManagerPermissions.Settings.ReadAllManagers",
                "PlatformManagerPermissions.Settings.UpdateAllManager",
                "PlatformManagerPermissions.Settings.ReadTeamManager",
                "PlatformManagerPermissions.Settings.UpdateTeamManager",
                "PlatformManagerPermissions.Settings.ChangeManagerPassword",
                "ManagerPermissions.Roles.AddRole",
                "ManagerPermissions.Roles.ReadAllRoles",
                "ManagerPermissions.Roles.UpdateAllRoles",
                "ManagerPermissions.Roles.UpdateRole",
                "ManagerPermissions.Roles.DeleteRole",
            };
            foreach (var permission in permissions)
            {
                migrationBuilder.InsertData(
                    table: "AspNetRoleClaims",
                    columns: new[] { "RoleId", "ClaimType", "ClaimValue" },
                    values: new[] { ApplicationRoleConfiguration.Constants.SuperAdminRoleId, "permission", permission });
            }

            migrationBuilder.InsertData(
                table: "AspNetUserClaims",
                columns: new[] { "UserId", "ClaimType", "ClaimValue" },
                values: new[] { ApplicationUserConfiguration.Constants.SuperAdminId, "scope", "platform" });
        }
    }
}
