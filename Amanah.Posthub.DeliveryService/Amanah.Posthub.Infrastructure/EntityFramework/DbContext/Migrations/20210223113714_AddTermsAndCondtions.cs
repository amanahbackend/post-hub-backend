﻿using Amanah.Posthub.DATA.Users.EntityConfigurations;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddTermsAndCondtions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TermsAndConditions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TermsAndConditionsInEnglish = table.Column<string>(nullable: true),
                    TermsAndConditionsInArabic = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TermsAndConditions", x => x.Id);
                });

            string permission = "ManagerPermissions.Agent.UpdateAgentTermsAndConditions";
            migrationBuilder.InsertData(
                    table: "AspNetRoleClaims",
                    columns: new[] { "RoleId", "ClaimType", "ClaimValue" },
                    values: new[] { ApplicationRoleConfiguration.Constants.SuperAdminRoleId, "permission", permission });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TermsAndConditions");

            string permission = "ManagerPermissions.Agent.UpdateAgentTermsAndConditions";

            migrationBuilder.DeleteData(
             table: "AspNetRoleClaims",
             keyColumn: "ClaimValue",
             keyValue: permission);


        }
    }
}
