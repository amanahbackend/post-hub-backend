﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.DATA.Users.EntityConfigurations;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddingSuperAdminRoleAndTeamMissingRoleClaimSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: ApplicationRoleConfiguration.Constants.SuperAdminRoleId,
                column: "Type",
                value: (int)RoleType.Manager);

            AddSuperAdminClaims(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
             table: "AspNetRoles",
             keyColumn: "Id",
             keyValue: ApplicationRoleConfiguration.Constants.SuperAdminRoleId,
             column: "Type",
             value: 4);
        }
        private void AddSuperAdminClaims(MigrationBuilder migrationBuilder)
        {
            var permissions = new[]
            {
                "ManagerPermissions.Roles.AddRole",
                "ManagerPermissions.Roles.ReadAllRoles",
                "ManagerPermissions.Roles.UpdateAllRoles",
                "ManagerPermissions.Roles.UpdateRole",
                "ManagerPermissions.Roles.DeleteRole",
                "ManagerPermissions.Team.CreateTeam",
                "ManagerPermissions.Team.DeleteTeam",
                "ManagerPermissions.Team.UpdateTeam",
                "ManagerPermissions.Team.ReadTeam",
                "ManagerPermissions.Team.DeleteAllTeam",
                "ManagerPermissions.Team.UpdateAllTeam",
                "ManagerPermissions.Team.ReadMyTeam"
        };

            foreach (var permission in permissions)
            {
                migrationBuilder.InsertData(
                    table: "AspNetRoleClaims",
                    columns: new[] { "RoleId", "ClaimType", "ClaimValue" },
                    values: new[] { ApplicationRoleConfiguration.Constants.SuperAdminRoleId, "permission", permission });
            }

        }
    }
}
