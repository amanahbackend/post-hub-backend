﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class DriverRegistrationAddingApprovalAndRejectorUserDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerCode",
                table: "DriverRegistrations");

            migrationBuilder.AddColumn<string>(
                name: "ApprovedORejectedByUserId",
                table: "DriverRegistrations",
                nullable: true);


            migrationBuilder.CreateIndex(
                name: "IX_DriverRegistrations_ApprovedORejectedByUserId",
                table: "DriverRegistrations",
                column: "ApprovedORejectedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_DriverRegistrations_AspNetUsers_ApprovedORejectedByUserId",
                table: "DriverRegistrations",
                column: "ApprovedORejectedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverRegistrations_AspNetUsers_ApprovedORejectedByUserId",
                table: "DriverRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_DriverRegistrations_ApprovedORejectedByUserId",
                table: "DriverRegistrations");

            migrationBuilder.DropColumn(
                name: "ApprovedORejectedByUserId",
                table: "DriverRegistrations");

            migrationBuilder.AddColumn<string>(
                name: "CustomerCode",
                table: "DriverRegistrations",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
