﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddMissingTransportFieldsToDriverRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TransportColor",
                table: "DriverRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransportYearAndModel",
                table: "DriverRegistrations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TransportColor",
                table: "DriverRegistrations");

            migrationBuilder.DropColumn(
                name: "TransportYearAndModel",
                table: "DriverRegistrations");
        }
    }
}
