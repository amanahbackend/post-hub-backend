﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddingApprovedOrRejectedByForTenant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApprovedORejectedByUserId",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TenantRegistrations_ApprovedORejectedByUserId",
                table: "TenantRegistrations",
                column: "ApprovedORejectedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TenantRegistrations_AspNetUsers_ApprovedORejectedByUserId",
                table: "TenantRegistrations",
                column: "ApprovedORejectedByUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TenantRegistrations_AspNetUsers_ApprovedORejectedByUserId",
                table: "TenantRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_TenantRegistrations_ApprovedORejectedByUserId",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "ApprovedORejectedByUserId",
                table: "TenantRegistrations");
        }
    }
}
