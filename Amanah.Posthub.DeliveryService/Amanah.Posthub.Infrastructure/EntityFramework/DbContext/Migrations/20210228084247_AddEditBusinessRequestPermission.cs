﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddEditBusinessRequestPermission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnerAddress",
                table: "TenantRegistrations");
            migrationBuilder.Sql(@"
                BEGIN TRANSACTION
                GO
                Insert into [dbo].[AspNetRoleClaims] (RoleId,ClaimType,ClaimValue) Values (N'EC7CFEE4-DDE1-4B37-95DE-E3458B2B9666', N'permission', N'PlatformManagerPermissions.Tenants.EditRequestTenant')
                COMMIT
                ");
        }


        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OwnerAddress",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
