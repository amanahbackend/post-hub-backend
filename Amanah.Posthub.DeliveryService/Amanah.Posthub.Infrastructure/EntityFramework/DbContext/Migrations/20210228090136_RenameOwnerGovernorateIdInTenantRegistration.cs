﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class RenameOwnerGovernorateIdInTenantRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Governorate",
                table: "TenantRegistrations");

            migrationBuilder.AddColumn<string>(
                name: "OwnerGovernorateId",
                table: "TenantRegistrations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnerGovernorateId",
                table: "TenantRegistrations");

            migrationBuilder.AddColumn<string>(
                name: "Governorate",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
