﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateDriverRegistrationWithDriverId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DriverId",
                table: "DriverRegistrations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DriverRegistrations_DriverId",
                table: "DriverRegistrations",
                column: "DriverId");

            migrationBuilder.AddForeignKey(
                name: "FK_DriverRegistrations_Driver_DriverId",
                table: "DriverRegistrations",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverRegistrations_Driver_DriverId",
                table: "DriverRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_DriverRegistrations_DriverId",
                table: "DriverRegistrations");

            migrationBuilder.DropColumn(
                name: "DriverId",
                table: "DriverRegistrations");
        }
    }
}
