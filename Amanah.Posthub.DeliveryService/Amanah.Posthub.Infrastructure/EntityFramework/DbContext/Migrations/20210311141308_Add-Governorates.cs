﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddGovernorates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Area",
                newName: "Name");


            migrationBuilder.AddColumn<string>(
                name: "NameAR",
                table: "Area",
                nullable: true);


            migrationBuilder.CreateTable(
                name: "Governrates",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CreatedBy_Id = table.Column<string>(nullable: true),
                    UpdatedBy_Id = table.Column<string>(nullable: true),
                    DeletedBy_Id = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: false),
                    NameEN = table.Column<string>(nullable: true),
                    NameAR = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Governrates", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverRegistrations_Driver_DriverId",
                table: "DriverRegistrations");

            migrationBuilder.DropTable(
                name: "Governrates");


            migrationBuilder.DropColumn(
                name: "NameAR",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "NameEN",
                table: "Area");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Area",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
