﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class RemoveRelationBetweenDriverRegistrationAndArea : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverRegistrations_Area_AreaId",
                table: "DriverRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_DriverRegistrations_AreaId",
                table: "DriverRegistrations");

            migrationBuilder.AlterColumn<string>(
                name: "AreaId",
                table: "DriverRegistrations",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.Sql(@"
update dr set AreaId = a.NameEN from DriverRegistrations dr join Area a on dr.AreaId = a.Id
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "AreaId",
                table: "DriverRegistrations",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DriverRegistrations_AreaId",
                table: "DriverRegistrations",
                column: "AreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_DriverRegistrations_Area_AreaId",
                table: "DriverRegistrations",
                column: "AreaId",
                principalTable: "Area",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
