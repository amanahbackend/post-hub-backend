﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdatingAddressAndBusinessRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TenantRegistrations_Area_AreaId",
                table: "TenantRegistrations");

            migrationBuilder.DropForeignKey(
                name: "FK_TenantRegistrations_Area_OwnerAreaId",
                table: "TenantRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_TenantRegistrations_AreaId",
                table: "TenantRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_TenantRegistrations_OwnerAreaId",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "Apartment",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "AreaId",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "GovernorateId",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "IsSameBusinessAddress",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerApartment",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAreaId",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerBlock",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerBuilding",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerFloor",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerGovernorateId",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerPACI",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerStreet",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "PACI",
                table: "TenantRegistrations");

            migrationBuilder.RenameColumn(
                name: "Street",
                table: "TenantRegistrations",
                newName: "OwnerAddress_Street");

            migrationBuilder.RenameColumn(
                name: "Floor",
                table: "TenantRegistrations",
                newName: "OwnerAddress_Floor");

            migrationBuilder.RenameColumn(
                name: "Building",
                table: "TenantRegistrations",
                newName: "OwnerAddress_Building");

            migrationBuilder.RenameColumn(
                name: "Block",
                table: "TenantRegistrations",
                newName: "OwnerAddress_Block");

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddressSummaryArabic",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddressSummaryEnglish",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAddressSummaryArabic",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAddressSummaryEnglish",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Area",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Block",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Building",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Flat",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Floor",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Governorate",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Street",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAddress_Area",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAddress_Flat",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAddress_Governorate",
                table: "TenantRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Governorate",
                table: "Tasks",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Governorate",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Governorate",
                table: "Branch",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BusinessAddressSummaryArabic",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddressSummaryEnglish",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAddressSummaryArabic",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAddressSummaryEnglish",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Area",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Block",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Building",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Flat",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Floor",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Governorate",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Street",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAddress_Area",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAddress_Flat",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAddress_Governorate",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "Location_Governorate",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "Location_Governorate",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Location_Governorate",
                table: "Branch");

            migrationBuilder.RenameColumn(
                name: "OwnerAddress_Street",
                table: "TenantRegistrations",
                newName: "Street");

            migrationBuilder.RenameColumn(
                name: "OwnerAddress_Floor",
                table: "TenantRegistrations",
                newName: "Floor");

            migrationBuilder.RenameColumn(
                name: "OwnerAddress_Building",
                table: "TenantRegistrations",
                newName: "Building");

            migrationBuilder.RenameColumn(
                name: "OwnerAddress_Block",
                table: "TenantRegistrations",
                newName: "Block");

            migrationBuilder.AddColumn<string>(
                name: "Apartment",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AreaId",
                table: "TenantRegistrations",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GovernorateId",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsSameBusinessAddress",
                table: "TenantRegistrations",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "OwnerApartment",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAreaId",
                table: "TenantRegistrations",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerBlock",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerBuilding",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerFloor",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerGovernorateId",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerPACI",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerStreet",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PACI",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TenantRegistrations_AreaId",
                table: "TenantRegistrations",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantRegistrations_OwnerAreaId",
                table: "TenantRegistrations",
                column: "OwnerAreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_TenantRegistrations_Area_AreaId",
                table: "TenantRegistrations",
                column: "AreaId",
                principalTable: "Area",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TenantRegistrations_Area_OwnerAreaId",
                table: "TenantRegistrations",
                column: "OwnerAreaId",
                principalTable: "Area",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
