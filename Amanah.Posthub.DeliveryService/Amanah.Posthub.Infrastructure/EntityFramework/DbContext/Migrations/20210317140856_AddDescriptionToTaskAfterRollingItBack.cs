﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddDescriptionToTaskAfterRollingItBack : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /* Issue the add descriptino migration was added to development on parallel to other migrations added to feature/business-and-driver-registrations*/
            /* the column already exists in dev, staging, so will rename column if exists first, then copy data and drop if exists*/
            /*adding following code to keep supporting creating database from scratch*/
            migrationBuilder.Sql(@"
IF EXISTS(SELECT 1 FROM sys.columns WHERE [name] = N'Description'
           AND [object_id] = OBJECT_ID(N'TaskHistory'))
BEGIN
    EXEC sp_RENAME 'TaskHistory.Description', '_Description' , 'COLUMN'
END;
");
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "TaskHistory",
                nullable: true);
            migrationBuilder.Sql(@"
IF EXISTS(SELECT 1 FROM sys.columns WHERE [name] = N'_Description'
           AND [object_id] = OBJECT_ID(N'TaskHistory'))
BEGIN
    UPDATE TaskHistory SET Description = _Description;
    ALTER TABLE TaskHistory DROP COLUMN _Description;
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "TaskHistory");
        }
    }
}
