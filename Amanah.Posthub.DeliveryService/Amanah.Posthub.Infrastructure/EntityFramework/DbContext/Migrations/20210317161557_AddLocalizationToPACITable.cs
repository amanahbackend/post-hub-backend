﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddLocalizationToPACITable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AreaName",
                table: "PACI",
                newName: "AreaNameEnglish");

            migrationBuilder.RenameColumn(
                name: "BlockName",
                table: "PACI",
                newName: "BlockNameEnglish");

            migrationBuilder.RenameColumn(
                name: "GovernorateName",
                table: "PACI",
                newName: "GovernorateNameEnglish");

            migrationBuilder.RenameColumn(
                name: "StreetName",
                table: "PACI",
                newName: "StreetNameEnglish");

            migrationBuilder.AddColumn<string>(
                name: "AreaNameArabic",
                table: "PACI",
                nullable: true);



            migrationBuilder.AddColumn<string>(
                name: "BlockNameArabic",
                table: "PACI",
                nullable: true);


            migrationBuilder.AddColumn<string>(
                name: "GovernorateNameArabic",
                table: "PACI",
                nullable: true);


            migrationBuilder.AddColumn<string>(
                name: "StreetNameArabic",
                table: "PACI",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AreaNameArabic",
                table: "PACI");

            migrationBuilder.DropColumn(
                name: "BlockNameArabic",
                table: "PACI");


            migrationBuilder.DropColumn(
                name: "GovernorateNameArabic",
                table: "PACI");


            migrationBuilder.DropColumn(
                name: "StreetNameArabic",
                table: "PACI");


            migrationBuilder.RenameColumn(
                name: "AreaNameEnglish",
                table: "PACI",
                newName: "AreaName");

            migrationBuilder.RenameColumn(
                name: "BlockNameEnglish",
                table: "PACI",
                newName: "BlockName");

            migrationBuilder.RenameColumn(
                name: "GovernorateNameEnglish",
                table: "PACI",
                newName: "GovernorateName");

            migrationBuilder.RenameColumn(
                name: "StreetNameEnglish",
                table: "PACI",
                newName: "StreetName");


        }
    }
}
