﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class RemoveTenantIdFromGlobalEntitiesAreasAndPACI : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Area_AspNetUsers_Tenant_Id",
                table: "Area");

            migrationBuilder.DropForeignKey(
                name: "FK_PACI_AspNetUsers_Tenant_Id",
                table: "PACI");

            migrationBuilder.DropIndex(
                name: "IX_PACI_Tenant_Id",
                table: "PACI");


            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "PACI");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "Area");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "PACI",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "Area",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PACI_Tenant_Id",
                table: "PACI",
                column: "Tenant_Id");


            migrationBuilder.AddForeignKey(
                name: "FK_Area_AspNetUsers_Tenant_Id",
                table: "Area",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PACI_AspNetUsers_Tenant_Id",
                table: "PACI",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
