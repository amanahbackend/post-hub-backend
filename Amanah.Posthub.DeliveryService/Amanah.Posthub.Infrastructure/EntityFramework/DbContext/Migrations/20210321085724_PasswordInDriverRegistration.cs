﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class PasswordInDriverRegistration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Password",
                table: "DriverRegistrations");

            migrationBuilder.AddColumn<string>(
                name: "PasswordHash",
                table: "DriverRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecurityStamp",
                table: "DriverRegistrations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PasswordHash",
                table: "DriverRegistrations");

            migrationBuilder.DropColumn(
                name: "SecurityStamp",
                table: "DriverRegistrations");

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "DriverRegistrations",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
