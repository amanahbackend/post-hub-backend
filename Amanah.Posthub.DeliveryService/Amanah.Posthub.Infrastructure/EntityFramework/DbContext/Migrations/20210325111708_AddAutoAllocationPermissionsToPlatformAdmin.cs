﻿using Amanah.Posthub.DATA.Users.EntityConfigurations;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddAutoAllocationPermissionsToPlatformAdmin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            AddSuperAdminAutoAllocationClaims(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }

        private void AddSuperAdminAutoAllocationClaims(MigrationBuilder migrationBuilder)
        {
            var permissions = new[]
            {
               "ManagerPermissions.Settings.UpdateAutoAllocation",
               "ManagerPermissions.Settings.ReadNotification",
               "ManagerPermissions.Settings.UpdateNotification"
            };
            foreach (var permission in permissions)
            {
                migrationBuilder.InsertData(
                    table: "AspNetRoleClaims",
                    columns: new[] { "RoleId", "ClaimType", "ClaimValue" },
                    values: new[] { ApplicationRoleConfiguration.Constants.SuperAdminRoleId, "permission", permission });
            }
        }

    }
}
