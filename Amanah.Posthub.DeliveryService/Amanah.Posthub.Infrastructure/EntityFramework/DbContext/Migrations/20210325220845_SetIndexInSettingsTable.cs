﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class SetIndexInSettingsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Settings_SettingKey_Tenant_Id",
                table: "Settings");

            migrationBuilder.CreateIndex(
             name: "IX_Settings_SettingKey_Tenant_Id",
             table: "Settings",
             columns: new[] { "SettingKey", "Tenant_Id" },
             unique: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Settings_SettingKey_Tenant_Id",
                table: "Settings");

            migrationBuilder.CreateIndex(
             name: "IX_Settings_SettingKey_Tenant_Id",
             table: "Settings",
             columns: new[] { "SettingKey", "Tenant_Id" },
             unique: true);
        }
    }
}
