﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateDriverCurrentLocationDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverCurrentLocation_Driver_DriverId",
                table: "DriverCurrentLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DriverCurrentLocation",
                table: "DriverCurrentLocation");

            //migrationBuilder.DropColumn(
            //    name: "DriverId",
            //    table: "DriverCurrentLocation");


            //migrationBuilder.AddColumn<int>(
            //    name: "Id",
            //    table: "DriverCurrentLocation",
            //    nullable: false,
            //    defaultValue: 0);

            migrationBuilder.RenameColumn(
                name: "DriverId",
                table: "DriverCurrentLocation",
                newName: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DriverCurrentLocation",
                table: "DriverCurrentLocation",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_DriverCurrentLocation_Driver_Id",
                table: "DriverCurrentLocation",
                column: "Id",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DriverCurrentLocation_Driver_Id",
                table: "DriverCurrentLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DriverCurrentLocation",
                table: "DriverCurrentLocation");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "DriverCurrentLocation");

            migrationBuilder.AddColumn<int>(
                name: "DriverId",
                table: "DriverCurrentLocation",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DriverCurrentLocation",
                table: "DriverCurrentLocation",
                column: "DriverId");

            migrationBuilder.AddForeignKey(
                name: "FK_DriverCurrentLocation_Driver_DriverId",
                table: "DriverCurrentLocation",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
