﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class addOtherIHaveAuditingToUserDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "UserDevice",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "UserDevice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "UserDevice",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "UserDevice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "UserDevice",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "UserDevice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "UserDevice");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "UserDevice");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "UserDevice");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "UserDevice");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "UserDevice");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "UserDevice");
        }
    }
}
