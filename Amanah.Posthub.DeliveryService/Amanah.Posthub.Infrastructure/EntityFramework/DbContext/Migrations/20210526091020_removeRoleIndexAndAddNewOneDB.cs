﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class removeRoleIndexAndAddNewOneDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
            name: "RoleNameIndex",
            table: "AspNetRoles");

            migrationBuilder.CreateIndex(
                name: "RoleTenantNameIndex",
                table: "AspNetRoles",
                columns: new[] { "Name", "NormalizedName", "Tenant_Id" },
                unique: true,
                filter: "[Name] IS NOT NULL AND [NormalizedName] IS NOT NULL AND [Tenant_Id] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "RoleTenantNameIndex",
                table: "AspNetRoles");
        }
    }
}
