﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateGeoFenceLocationDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "GeoFenceLocation",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "GeoFenceLocation",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "GeoFenceLocation",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "GeoFenceLocation",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "GeoFenceLocation",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "GeoFenceLocation",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "GeoFenceLocation",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "GeoFenceLocation");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "GeoFenceLocation");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "GeoFenceLocation");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "GeoFenceLocation");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "GeoFenceLocation");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "GeoFenceLocation");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "GeoFenceLocation");
        }
    }
}
