﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateGeoFenceLocationWithTenantIdDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "GeoFenceLocation",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_GeoFenceLocation_Tenant_Id",
                table: "GeoFenceLocation",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_GeoFenceLocation_AspNetUsers_Tenant_Id",
                table: "GeoFenceLocation",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GeoFenceLocation_AspNetUsers_Tenant_Id",
                table: "GeoFenceLocation");

            migrationBuilder.DropIndex(
                name: "IX_GeoFenceLocation_Tenant_Id",
                table: "GeoFenceLocation");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "GeoFenceLocation");
        }
    }
}
