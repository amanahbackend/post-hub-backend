﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateTeamManagerWithTenantIdDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "TeamManager",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "TeamManager",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "TeamManager",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "TeamManager",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "TeamManager",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "TeamManager",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "TeamManager",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "TeamManager",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.CreateIndex(
                name: "IX_TeamManager_Tenant_Id",
                table: "TeamManager",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TeamManager_AspNetUsers_Tenant_Id",
                table: "TeamManager",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TeamManager_AspNetUsers_Tenant_Id",
                table: "TeamManager");

            migrationBuilder.DropIndex(
                name: "IX_TeamManager_Tenant_Id",
                table: "TeamManager");

            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "TeamManager");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "TeamManager");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "TeamManager");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "TeamManager");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "TeamManager");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "TeamManager");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "TeamManager");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "TeamManager");
        }
    }
}
