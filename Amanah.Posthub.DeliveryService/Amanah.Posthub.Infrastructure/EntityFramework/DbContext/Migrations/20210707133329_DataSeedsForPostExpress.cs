﻿using Amanah.Posthub.BLL.Authorization;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class DataSeedsForPostExpress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CountryId", "CreatedBy_Id", "CreatedDate", "DeletedBy_Id", "DeletedDate", "Email", "EmailConfirmed", "FirstName", "IsAvailable", "IsDeleted", "IsLocked", "LastName", "LockoutEnabled", "LockoutEnd", "MiddleName", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "Tenant_Id", "TwoFactorEnabled", "UpdatedBy_Id", "UpdatedDate", "UserName" },
                values: new object[] { "AV59A46B-72BF-4849-82D0-43851B574590", 0, "D5E28451-1E22-4DD8-A6E3-86330A71759C", 121, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "super-admin@postexpress.com", true, "Super", true, false, false, "Admin", false, null, null, "SUPER-ADMIN@POSTEXPRESS.COM", "SUPER-ADMIN@POSTEXPRESS.COM", "AQAAAAEAACcQAAAAEPxqhST2073HVmQmXOTq5eC0sWJLNAj6yWL5dj3KG3pnQT/qE8GLDAazPObekZb84w==", null, false, "788F1971-0E77-49DC-BC49-EAE4A629BA66", "AV59A46B-72BF-4849-82D0-43851B574590", false, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "super-admin@postexpress.com" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedBy_Id", "CreatedDate", "DeletedBy_Id", "DeletedDate", "IsDeleted", "Name", "NormalizedName", "Tenant_Id", "Type", "UpdatedBy_Id", "UpdatedDate" },
                values: new object[] { "DJ7CFEE4-DDE1-4B37-95DE-C4458B2D9888", "4E0AE6D6-252F-4E15-B963-589B0C3F5389", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "PostExpress-SuperAdmin", "POSTEXPRESS-SUPERADMIN", "AV59A46B-72BF-4849-82D0-43851B574590", 1, null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "AV59A46B-72BF-4849-82D0-43851B574590", "DJ7CFEE4-DDE1-4B37-95DE-C4458B2D9888" });
            AddPostExpressSuperAdminClaims(migrationBuilder);


            migrationBuilder.InsertData(
              table: "Company",
              columns: new[] {
                    "Id",
                    "Name",
                    "NameAR",
                    "Email",
                    "CountryId",
                    "Phone",
                    "CreatedBy_Id",
                    "CreatedDate",
                    "DeletedBy_Id",
                    "DeletedDate",
                    "IsDeleted",
                    "Tenant_Id",
                    "UpdatedBy_Id",
                    "UpdatedDate" },
              values: new object[] {
                    1,
                    "Post Express",
                    "بوست اكبريس",
                    "super-admin@postexpress.com",
                    121,
                    "1097030998",
                    null,
                    new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                    null,
                    new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                    false,
                    "AV59A46B-72BF-4849-82D0-43851B574590",
                    null,
                    new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                 });

            migrationBuilder.InsertData(
    table: "Admins",
    columns: new[] {
                    "Id",
                    "CompanyName",
                    "DisplayImage",
                    "ResidentCountryId",
                    "CreatedBy_Id",
                    "CreatedDate",
                    "DeletedBy_Id",
                    "DeletedDate",
                    "IsDeleted",
                    "Tenant_Id",
                    "UpdatedBy_Id",
                    "UpdatedDate" },
    values: new object[] {
               "AV59A46B-72BF-4849-82D0-43851B574590",
               "Post Express",
               1,
                121,
                null,
                new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                null,
                new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                false,
                "AV59A46B-72BF-4849-82D0-43851B574590",
                null,
                new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
       });



        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
    table: "AspNetUserRoles",
    keyColumns: new[] { "UserId", "RoleId" },
    keyValues: new object[] { "AV59A46B-72BF-4849-82D0-43851B574590", "DJ7CFEE4-DDE1-4B37-95DE-C4458B2D9888" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "DJ7CFEE4-DDE1-4B37-95DE-C4458B2D9888");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "AV59A46B-72BF-4849-82D0-43851B574590");

            migrationBuilder.DeleteData(
                         table: "Company",
                         keyColumn: "Id",
                         keyValue: 1);

            migrationBuilder.DeleteData(
                            table: "Admins",
                            keyColumn: "Id",
                            keyValue: "AV59A46B-72BF-4849-82D0-43851B574590");


        }


        private List<string> GetAllTenantPermissions()
        {
            List<string> permissions = new List<string>();
            TenantPermissions.FeaturePermissions
                .Where(featurePermission => featurePermission.Name != PermissionFeatures.PlatformAgent)
                .ToList()
                .ForEach(featurePermission =>
                {
                    permissions.AddRange(
                         featurePermission.Permissions.Select(permission => permission).ToList());
                });
            return permissions
                .Distinct()
                .ToList();
        }


        private void AddPostExpressSuperAdminClaims(MigrationBuilder migrationBuilder)
        {
            var permissions = GetAllTenantPermissions();

            foreach (var permission in permissions)
            {
                migrationBuilder.InsertData(
                    table: "AspNetRoleClaims",
                    columns: new[] { "RoleId", "ClaimType", "ClaimValue" },
                    values: new[] { "DJ7CFEE4-DDE1-4B37-95DE-C4458B2D9888", "permission", permission });
            }

            migrationBuilder.InsertData(
                table: "AspNetUserClaims",
                columns: new[] { "UserId", "ClaimType", "ClaimValue" },
                values: new[] { "AV59A46B-72BF-4849-82D0-43851B574590", "scope", "tenant" });
        }

    }
}
