﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class fixRelationsInWorkingHours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkingHours_Branch_BranchId1",
                table: "WorkingHours");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkingHours_Company_CompanyId1",
                table: "WorkingHours");

            migrationBuilder.DropIndex(
                name: "IX_WorkingHours_BranchId1",
                table: "WorkingHours");

            migrationBuilder.DropIndex(
                name: "IX_WorkingHours_CompanyId1",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "BranchId1",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "CompanyId1",
                table: "WorkingHours");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BranchId1",
                table: "WorkingHours",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyId1",
                table: "WorkingHours",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkingHours_BranchId1",
                table: "WorkingHours",
                column: "BranchId1");

            migrationBuilder.CreateIndex(
                name: "IX_WorkingHours_CompanyId1",
                table: "WorkingHours",
                column: "CompanyId1");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkingHours_Branch_BranchId1",
                table: "WorkingHours",
                column: "BranchId1",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkingHours_Company_CompanyId1",
                table: "WorkingHours",
                column: "CompanyId1",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
