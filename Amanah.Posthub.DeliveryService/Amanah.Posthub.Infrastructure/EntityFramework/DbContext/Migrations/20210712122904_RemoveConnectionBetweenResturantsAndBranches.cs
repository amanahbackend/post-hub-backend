﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class RemoveConnectionBetweenResturantsAndBranches : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch");

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch",
                column: "GeoFenceId",
                principalTable: "GeoFence",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch",
                column: "RestaurantId",
                principalTable: "Restaurant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch");

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch",
                column: "GeoFenceId",
                principalTable: "GeoFence",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch",
                column: "RestaurantId",
                principalTable: "Restaurant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
