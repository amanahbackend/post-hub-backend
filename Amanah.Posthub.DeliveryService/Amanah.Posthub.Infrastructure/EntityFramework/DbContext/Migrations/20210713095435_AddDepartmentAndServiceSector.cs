﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddDepartmentAndServiceSector : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBirth",
                table: "Manager",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Manager",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "NationalId",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "NationalIdExpiryDate",
                table: "Manager",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "NationalIdExtractDate",
                table: "Manager",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceSectorId",
                table: "Manager",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "SupervisorId",
                table: "Manager",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Departments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NameAr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Departments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Departments_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ServiceSectors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NameAr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceSectors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceSectors_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Manager_DepartmentId",
                table: "Manager",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Manager_ServiceSectorId",
                table: "Manager",
                column: "ServiceSectorId");

            migrationBuilder.CreateIndex(
                name: "IX_Manager_SupervisorId",
                table: "Manager",
                column: "SupervisorId");

            migrationBuilder.CreateIndex(
                name: "IX_Departments_Tenant_Id",
                table: "Departments",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceSectors_Tenant_Id",
                table: "ServiceSectors",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_AspNetUsers_SupervisorId",
                table: "Manager",
                column: "SupervisorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_Departments_DepartmentId",
                table: "Manager",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_ServiceSectors_ServiceSectorId",
                table: "Manager",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.InsertData(
              table: "ServiceSectors",
              columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
              values: new object[] { "Sector1", "قطاع1", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "ServiceSectors",
                columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Sector2", "قطاع2", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                        table: "Departments",
                        columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                        values: new object[] { "Department1", "قسم1", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                    table: "Departments",
                    columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                    values: new object[] { "Department2", "قسم2", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });






        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manager_AspNetUsers_SupervisorId",
                table: "Manager");

            migrationBuilder.DropForeignKey(
                name: "FK_Manager_Departments_DepartmentId",
                table: "Manager");

            migrationBuilder.DropForeignKey(
                name: "FK_Manager_ServiceSectors_ServiceSectorId",
                table: "Manager");

            migrationBuilder.DropTable(
                name: "Departments");

            migrationBuilder.DropTable(
                name: "ServiceSectors");

            migrationBuilder.DropIndex(
                name: "IX_Manager_DepartmentId",
                table: "Manager");

            migrationBuilder.DropIndex(
                name: "IX_Manager_ServiceSectorId",
                table: "Manager");

            migrationBuilder.DropIndex(
                name: "IX_Manager_SupervisorId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "NationalId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "NationalIdExpiryDate",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "NationalIdExtractDate",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "ServiceSectorId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "SupervisorId",
                table: "Manager");
        }
    }
}
