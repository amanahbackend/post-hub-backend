﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddBranchToManager : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Manager",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Manager_BranchId",
                table: "Manager",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_Branch_BranchId",
                table: "Manager",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manager_Branch_BranchId",
                table: "Manager");

            migrationBuilder.DropIndex(
                name: "IX_Manager_BranchId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Manager");
        }
    }
}
