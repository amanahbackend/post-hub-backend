﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddBusinessType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BusinessTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NameAr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusinessTypes_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BusinessTypes_Tenant_Id",
                table: "BusinessTypes",
                column: "Tenant_Id");

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Bank", "بنك", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Court", "محكمة", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Society", "اجتماعى", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Club", "نادى", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Lawyer", "مكتب محاماه", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Union", "اتحاد", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Syndicate", "نقابه", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "NGO", "منظمه غير حكوميه", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Assosciation", "منظمه غير منظمة", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Advertisement", "اعلانات", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Publications", "المنشورات", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });


            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Other", "المنشورات", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

        }


        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BusinessTypes");
        }
    }
}
