﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddNtionalityAndAddresses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address_Area",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Block",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Building",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Flat",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Floor",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Governorate",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Street",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NationalityId",
                table: "Manager",
                type: "int",
                nullable: false,
                defaultValue: 121);

            migrationBuilder.AddColumn<string>(
                name: "ProfilePicURL",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Manager_NationalityId",
                table: "Manager",
                column: "NationalityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_Country_NationalityId",
                table: "Manager",
                column: "NationalityId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manager_Country_NationalityId",
                table: "Manager");

            migrationBuilder.DropIndex(
                name: "IX_Manager_NationalityId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Area",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Block",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Building",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Flat",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Floor",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Governorate",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Street",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "NationalityId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "ProfilePicURL",
                table: "Manager");
        }
    }
}
