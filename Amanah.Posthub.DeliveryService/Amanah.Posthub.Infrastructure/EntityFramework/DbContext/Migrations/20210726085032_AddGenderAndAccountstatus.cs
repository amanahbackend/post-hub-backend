﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddGenderAndAccountstatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccountStatusId",
                table: "Manager",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "GenderId",
                table: "Manager",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateTable(
                name: "AccountStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NameAr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountStatus_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Genders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NameAr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Genders_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });



            migrationBuilder.InsertData(
                table: "Genders",
                columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Male", "ذكر", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                    table: "Genders",
                    columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                    values: new object[] { "Female", "انثى", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
              table: "AccountStatus",
              columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
              values: new object[] { "Active", "مفعل", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
              table: "AccountStatus",
              columns: new[] { "Name", "NameAr", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
              values: new object[] { "Not Active", " غير مفعل", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });




            migrationBuilder.CreateIndex(
                name: "IX_Manager_AccountStatusId",
                table: "Manager",
                column: "AccountStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Manager_GenderId",
                table: "Manager",
                column: "GenderId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountStatus_Tenant_Id",
                table: "AccountStatus",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Genders_Tenant_Id",
                table: "Genders",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_AccountStatus_AccountStatusId",
                table: "Manager",
                column: "AccountStatusId",
                principalTable: "AccountStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_Genders_GenderId",
                table: "Manager",
                column: "GenderId",
                principalTable: "Genders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);




        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manager_AccountStatus_AccountStatusId",
                table: "Manager");

            migrationBuilder.DropForeignKey(
                name: "FK_Manager_Genders_GenderId",
                table: "Manager");

            migrationBuilder.DropTable(
                name: "AccountStatus");

            migrationBuilder.DropTable(
                name: "Genders");

            migrationBuilder.DropIndex(
                name: "IX_Manager_AccountStatusId",
                table: "Manager");

            migrationBuilder.DropIndex(
                name: "IX_Manager_GenderId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "AccountStatusId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "GenderId",
                table: "Manager");
        }
    }
}
