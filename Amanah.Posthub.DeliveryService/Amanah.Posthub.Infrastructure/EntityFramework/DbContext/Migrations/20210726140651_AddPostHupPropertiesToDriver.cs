﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddPostHupPropertiesToDriver : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MobileNumber",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AccountStatusId",
                table: "Driver",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<string>(
                name: "Address_Area",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Block",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Building",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Flat",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Floor",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Governorate",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Street",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Driver",
                type: "int",
                nullable: false,
                defaultValue: 2);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBirth",
                table: "Driver",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DepartmentId",
                table: "Driver",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "GenderId",
                table: "Driver",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<string>(
                name: "LicenseNumber",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MobileNumber",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NationalId",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "NationalityId",
                table: "Driver",
                type: "int",
                nullable: false,
                defaultValue: 121);

            migrationBuilder.AddColumn<int>(
                name: "ServiceSectorId",
                table: "Driver",
                type: "int",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<string>(
                name: "SupervisorId",
                table: "Driver",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Driver_AccountStatusId",
                table: "Driver",
                column: "AccountStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_BranchId",
                table: "Driver",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_DepartmentId",
                table: "Driver",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_GenderId",
                table: "Driver",
                column: "GenderId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_NationalityId",
                table: "Driver",
                column: "NationalityId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_ServiceSectorId",
                table: "Driver",
                column: "ServiceSectorId");

            migrationBuilder.CreateIndex(
                name: "IX_Driver_SupervisorId",
                table: "Driver",
                column: "SupervisorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_AccountStatus_AccountStatusId",
                table: "Driver",
                column: "AccountStatusId",
                principalTable: "AccountStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_AspNetUsers_SupervisorId",
                table: "Driver",
                column: "SupervisorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_Branch_BranchId",
                table: "Driver",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_Country_NationalityId",
                table: "Driver",
                column: "NationalityId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_Departments_DepartmentId",
                table: "Driver",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_Genders_GenderId",
                table: "Driver",
                column: "GenderId",
                principalTable: "Genders",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_ServiceSectors_ServiceSectorId",
                table: "Driver",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Driver_AccountStatus_AccountStatusId",
                table: "Driver");

            migrationBuilder.DropForeignKey(
                name: "FK_Driver_AspNetUsers_SupervisorId",
                table: "Driver");

            migrationBuilder.DropForeignKey(
                name: "FK_Driver_Branch_BranchId",
                table: "Driver");

            migrationBuilder.DropForeignKey(
                name: "FK_Driver_Country_NationalityId",
                table: "Driver");

            migrationBuilder.DropForeignKey(
                name: "FK_Driver_Departments_DepartmentId",
                table: "Driver");

            migrationBuilder.DropForeignKey(
                name: "FK_Driver_Genders_GenderId",
                table: "Driver");

            migrationBuilder.DropForeignKey(
                name: "FK_Driver_ServiceSectors_ServiceSectorId",
                table: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Driver_AccountStatusId",
                table: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Driver_BranchId",
                table: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Driver_DepartmentId",
                table: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Driver_GenderId",
                table: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Driver_NationalityId",
                table: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Driver_ServiceSectorId",
                table: "Driver");

            migrationBuilder.DropIndex(
                name: "IX_Driver_SupervisorId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "MobileNumber",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "AccountStatusId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Address_Area",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Address_Block",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Address_Building",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Address_Flat",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Address_Floor",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Address_Governorate",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Address_Street",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "DepartmentId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "GenderId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "LicenseNumber",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "MobileNumber",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "NationalId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "NationalityId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "ServiceSectorId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "SupervisorId",
                table: "Driver");
        }
    }
}
