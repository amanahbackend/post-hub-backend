﻿
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class RenameCoulmnsToName_enAndName_ar : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NameAr",
                table: "ServiceSectors",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "ServiceSectors",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "NameAr",
                table: "Genders",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Genders",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "NameAr",
                table: "Departments",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Departments",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "NameAr",
                table: "BusinessTypes",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "BusinessTypes",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "NameAr",
                table: "AccountStatus",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "AccountStatus",
                newName: "Name_en");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "ServiceSectors",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "Name_ar",
                table: "ServiceSectors",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "Genders",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "Name_ar",
                table: "Genders",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "Departments",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "Name_ar",
                table: "Departments",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "BusinessTypes",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "Name_ar",
                table: "BusinessTypes",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "AccountStatus",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "Name_ar",
                table: "AccountStatus",
                newName: "Name_ar");
        }
    }
}
