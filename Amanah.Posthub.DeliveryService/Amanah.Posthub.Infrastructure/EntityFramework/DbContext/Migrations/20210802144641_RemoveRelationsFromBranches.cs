﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class RemoveRelationsFromBranches : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch");

            migrationBuilder.RenameColumn(
                name: "TwitterURL",
                table: "Company",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "NameAR",
                table: "Company",
                newName: "Name_ar");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Company",
                newName: "Mobile");

            migrationBuilder.RenameColumn(
                name: "FacebookURl",
                table: "Company",
                newName: "MissionSlogan_en");

            migrationBuilder.AddColumn<string>(
                name: "BussinesDiscription_ar",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BussinesDiscription_en",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Area",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Block",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Building",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Flat",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Floor",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Governorate",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Street",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FaxNo",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MainBranchId",
                table: "Company",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MainBranchId1",
                table: "Company",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MissionSlogan_ar",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RestaurantId",
                table: "Branch",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "GeoFenceId",
                table: "Branch",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                table: "Branch",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "CompanySocialLinks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CompanyId = table.Column<int>(type: "int", nullable: false),
                    SiteName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SiteLink = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanySocialLinks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanySocialLinks_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Company_MainBranchId1",
                table: "Company",
                column: "MainBranchId1");

            migrationBuilder.CreateIndex(
                name: "IX_CompanySocialLinks_CompanyId",
                table: "CompanySocialLinks",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch",
                column: "GeoFenceId",
                principalTable: "GeoFence",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch",
                column: "RestaurantId",
                principalTable: "Restaurant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Branch_MainBranchId1",
                table: "Company",
                column: "MainBranchId1",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch");

            migrationBuilder.DropForeignKey(
                name: "FK_Company_Branch_MainBranchId1",
                table: "Company");

            migrationBuilder.DropTable(
                name: "CompanySocialLinks");

            migrationBuilder.DropIndex(
                name: "IX_Company_MainBranchId1",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "BussinesDiscription_ar",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "BussinesDiscription_en",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Area",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Block",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Building",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Flat",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Floor",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Governorate",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Street",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "FaxNo",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "MainBranchId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "MainBranchId1",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "MissionSlogan_ar",
                table: "Company");

            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "Company",
                newName: "TwitterURL");

            migrationBuilder.RenameColumn(
                name: "Name_ar",
                table: "Company",
                newName: "NameAR");

            migrationBuilder.RenameColumn(
                name: "Mobile",
                table: "Company",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "MissionSlogan_en",
                table: "Company",
                newName: "FacebookURl");

            migrationBuilder.AlterColumn<int>(
                name: "RestaurantId",
                table: "Branch",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "GeoFenceId",
                table: "Branch",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                table: "Branch",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_GeoFence_GeoFenceId",
                table: "Branch",
                column: "GeoFenceId",
                principalTable: "GeoFence",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_Restaurant_RestaurantId",
                table: "Branch",
                column: "RestaurantId",
                principalTable: "Restaurant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
