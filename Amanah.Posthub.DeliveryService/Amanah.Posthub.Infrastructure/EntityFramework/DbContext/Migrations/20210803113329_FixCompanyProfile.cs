﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class FixCompanyProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TradeNameAR",
                table: "Company",
                newName: "TradeName_en");

            migrationBuilder.RenameColumn(
                name: "TradeName",
                table: "Company",
                newName: "TradeName_ar");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TradeName_en",
                table: "Company",
                newName: "TradeNameAR");

            migrationBuilder.RenameColumn(
                name: "TradeName_ar",
                table: "Company",
                newName: "TradeName");
        }
    }
}
