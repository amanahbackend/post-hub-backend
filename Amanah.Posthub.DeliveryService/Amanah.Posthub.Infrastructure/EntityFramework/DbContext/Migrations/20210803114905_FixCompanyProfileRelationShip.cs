﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class FixCompanyProfileRelationShip : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AlterColumn<int>(
                name: "MainBranchId",
                table: "Company",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Company_Branch_MainBranchId",
            //        table: "Company",
            //        column: "MainBranchId",
            //        principalTable: "Branch",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_Branch_MainBranchId",
                table: "Company");

            migrationBuilder.AlterColumn<int>(
                name: "MainBranchId",
                table: "Company",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Company_Branch_MainBranchId",
                table: "Company",
                column: "MainBranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
