﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class FixCompanyProfileLanguages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BussinesDiscription_ar",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "BussinesDiscription_en",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "MissionSlogan_ar",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "MissionSlogan_en",
                table: "Company");

            migrationBuilder.RenameColumn(
                name: "TradeName_en",
                table: "Company",
                newName: "TradeName");

            migrationBuilder.RenameColumn(
                name: "TradeName_ar",
                table: "Company",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "Company",
                newName: "MissionSlogan");

            migrationBuilder.RenameColumn(
                name: "Name_ar",
                table: "Company",
                newName: "BussinesDiscription");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TradeName",
                table: "Company",
                newName: "TradeName_en");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Company",
                newName: "TradeName_ar");

            migrationBuilder.RenameColumn(
                name: "MissionSlogan",
                table: "Company",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
                name: "BussinesDiscription",
                table: "Company",
                newName: "Name_ar");

            migrationBuilder.AddColumn<string>(
                name: "BussinesDiscription_ar",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BussinesDiscription_en",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MissionSlogan_ar",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MissionSlogan_en",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
