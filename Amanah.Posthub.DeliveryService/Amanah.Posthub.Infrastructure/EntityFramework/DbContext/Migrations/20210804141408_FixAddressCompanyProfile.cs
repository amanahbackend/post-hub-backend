﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class FixAddressCompanyProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropIndex(
                name: "IX_Company_MainBranchId1",
                table: "Company");


            migrationBuilder.DropForeignKey(
                name: "FK_Company_Branch_MainBranchId1",
                table: "Company");

            migrationBuilder.DropColumn(
                    name: "MainBranchId1",
                    table: "Company");


        }


        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
