﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateWorkingHours : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkingHours_Branch_BranchId",
                table: "WorkingHours");

            migrationBuilder.DropIndex(
                name: "IX_WorkingHours_BranchId",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "Offset",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "TimeZone",
                table: "WorkingHours");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "To",
                table: "WorkingHours",
                type: "time",
                nullable: true,
                oldClrType: typeof(TimeSpan),
                oldType: "time");

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SecondFrom",
                table: "WorkingHours",
                type: "time",
                nullable: true);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "SecondTo",
                table: "WorkingHours",
                type: "time",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SecondFrom",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "SecondTo",
                table: "WorkingHours");

            migrationBuilder.AlterColumn<TimeSpan>(
                name: "To",
                table: "WorkingHours",
                type: "time",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0),
                oldClrType: typeof(TimeSpan),
                oldType: "time",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "WorkingHours",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "Offset",
                table: "WorkingHours",
                type: "time",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "TimeZone",
                table: "WorkingHours",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorkingHours_BranchId",
                table: "WorkingHours",
                column: "BranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkingHours_Branch_BranchId",
                table: "WorkingHours",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
