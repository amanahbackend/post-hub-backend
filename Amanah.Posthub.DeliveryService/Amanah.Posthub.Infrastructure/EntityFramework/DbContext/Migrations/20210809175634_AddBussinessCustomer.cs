﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddBussinessCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_PACINumber",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAddress_PACINumber",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_PACINumber",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_PACINumber",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_PACINumber",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_PACINumber",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);


            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_PACINumber",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_PACINumber",
                table: "Branch",
                type: "nvarchar(max)",
                nullable: true);


            migrationBuilder.CreateTable(
                name: "CollectionMethods",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CollectionMethods", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CollectionMethods_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactFunctions",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactFunctions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContactFunctions_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Courtesys",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courtesys", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Courtesys_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BusinessCustomers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BusinessTypeId = table.Column<int>(type: "int", nullable: false),
                    BusinessLogoUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BusinessCID = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BusinessCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CollectionMethodId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessCustomers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusinessCustomers_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);


                    table.ForeignKey(
                        name: "FK_BusinessCustomers_CollectionMethods_CollectionMethodId",
                        column: x => x.CollectionMethodId,
                        principalTable: "CollectionMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusinessConactAccounts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<string>(type: "varchar(36)", nullable: true),
                    ContactCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ContactFunctionId = table.Column<int>(type: "int", nullable: false),
                    CourtesyId = table.Column<int>(type: "int", nullable: false),
                    AccountStatusId = table.Column<int>(type: "int", nullable: false),
                    PersonalPhotoURL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MobileNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Fax = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BusinessCustomerId = table.Column<int>(type: "int", nullable: false),
                    BusinessCustomerBranchId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessConactAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusinessConactAccounts_AccountStatus_AccountStatusId",
                        column: x => x.AccountStatusId,
                        principalTable: "AccountStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusinessConactAccounts_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessConactAccounts_AspNetUsers_UserID",
                        column: x => x.UserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessConactAccounts_BusinessCustomers_BusinessCustomerId",
                        column: x => x.BusinessCustomerId,
                        principalTable: "BusinessCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusinessConactAccounts_ContactFunctions_ContactFunctionId",
                        column: x => x.ContactFunctionId,
                        principalTable: "ContactFunctions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusinessConactAccounts_Courtesys_CourtesyId",
                        column: x => x.CourtesyId,
                        principalTable: "Courtesys",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BusinessCustomerBranchs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Mobile = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Latitude = table.Column<double>(type: "float", nullable: true),
                    Longitude = table.Column<double>(type: "float", nullable: true),
                    Location_PACINumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location_Governorate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location_Area = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location_Block = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location_Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location_Building = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location_Floor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Location_Flat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BranchContactId = table.Column<int>(type: "int", nullable: false),
                    PickupContactId = table.Column<int>(type: "int", nullable: false),
                    CloseTime = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PreferedPickupTimeFrom = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PreferedPickupTimeTo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeliveryNotes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BusinessCustomerId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessCustomerBranchs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusinessCustomerBranchs_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessCustomerBranchs_BusinessConactAccounts_BranchContactId1",
                        column: x => x.BranchContactId,
                        principalTable: "BusinessConactAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessCustomerBranchs_BusinessConactAccounts_PickupContactId1",
                        column: x => x.PickupContactId,
                        principalTable: "BusinessConactAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessCustomerBranchs_BusinessCustomers_BusinessCustomerId",
                        column: x => x.BusinessCustomerId,
                        principalTable: "BusinessCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BusinessCustomerBranchs_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });



            migrationBuilder.CreateIndex(
                name: "IX_BusinessConactAccounts_AccountStatusId",
                table: "BusinessConactAccounts",
                column: "AccountStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessConactAccounts_BusinessCustomerId",
                table: "BusinessConactAccounts",
                column: "BusinessCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessConactAccounts_ContactFunctionId",
                table: "BusinessConactAccounts",
                column: "ContactFunctionId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessConactAccounts_CourtesyId",
                table: "BusinessConactAccounts",
                column: "CourtesyId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessConactAccounts_Tenant_Id",
                table: "BusinessConactAccounts",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessConactAccounts_UserID",
                table: "BusinessConactAccounts",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomerBranchs_BranchContactId1",
                table: "BusinessCustomerBranchs",
                column: "BranchContactId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomerBranchs_BusinessCustomerId",
                table: "BusinessCustomerBranchs",
                column: "BusinessCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomerBranchs_CountryId",
                table: "BusinessCustomerBranchs",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomerBranchs_PickupContactId1",
                table: "BusinessCustomerBranchs",
                column: "PickupContactId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomerBranchs_Tenant_Id",
                table: "BusinessCustomerBranchs",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomers_BusinessTypeId",
                table: "BusinessCustomers",
                column: "BusinessTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomers_CollectionMethodId",
                table: "BusinessCustomers",
                column: "CollectionMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomers_Tenant_Id",
                table: "BusinessCustomers",
                column: "Tenant_Id");


            migrationBuilder.CreateIndex(
                name: "IX_CollectionMethods_Tenant_Id",
                table: "CollectionMethods",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ContactFunctions_Tenant_Id",
                table: "ContactFunctions",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Courtesys_Tenant_Id",
                table: "Courtesys",
                column: "Tenant_Id");


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BusinessCustomerBranchs");

            migrationBuilder.DropTable(
                name: "BusinessConactAccounts");

            migrationBuilder.DropTable(
                name: "BusinessCustomers");

            migrationBuilder.DropTable(
                name: "ContactFunctions");

            migrationBuilder.DropTable(
                name: "Courtesys");


            migrationBuilder.DropTable(
                name: "CollectionMethods");



            migrationBuilder.DropColumn(
                name: "BusinessAddress_PACINumber",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAddress_PACINumber",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "Location_PACINumber",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "Address_PACINumber",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_PACINumber",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Location_PACINumber",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_PACINumber",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Location_PACINumber",
                table: "Branch");

        }
    }
}
