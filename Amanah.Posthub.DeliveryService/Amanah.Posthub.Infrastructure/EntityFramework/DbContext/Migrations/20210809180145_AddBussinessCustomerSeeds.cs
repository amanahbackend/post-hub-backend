﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddBussinessCustomerSeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.InsertData(
                table: "CollectionMethods",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Credit", "Credit", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "CollectionMethods",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Cash", "Cash", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "CollectionMethods",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Deposit", "Deposit", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "CollectionMethods",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "COD", "COD", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            /////////////////////////////


            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Spoken", "Spoken", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Contracts", "Contracts", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Accounting", "Accounting", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Pick-up", "Pick-up", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });


            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Accounting", "Accounting", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Branch contact", "Branch contact", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

            migrationBuilder.InsertData(
                table: "ContactFunctions",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Business orders", "Business orders", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });


            migrationBuilder.InsertData(
                        table: "Courtesys",
                        columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                        values: new object[] { "Mr", "Mr", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });


            migrationBuilder.InsertData(
                table: "Courtesys",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Mrs", "Mrs", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });


            migrationBuilder.InsertData(
                table: "Courtesys",
                columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
                values: new object[] { "Ms", "Ms", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });



        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
