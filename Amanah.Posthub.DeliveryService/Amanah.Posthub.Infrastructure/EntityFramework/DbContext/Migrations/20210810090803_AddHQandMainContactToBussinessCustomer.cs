﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddHQandMainContactToBussinessCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HQBranchId",
                table: "BusinessCustomers",
                type: "int",
                nullable: true,
                defaultValue: 0);


            migrationBuilder.AddColumn<int>(
                name: "MainContactId",
                table: "BusinessCustomers",
                type: "int",
                nullable: true,
                defaultValue: 0);



            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomers_HQBranchId",
                table: "BusinessCustomers",
                column: "HQBranchId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomers_MainContactId",
                table: "BusinessCustomers",
                column: "MainContactId");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomers_BusinessConactAccounts_MainContactId1",
                table: "BusinessCustomers",
                column: "MainContactId",
                principalTable: "BusinessConactAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomers_BusinessCustomerBranchs_HQBranchId",
                table: "BusinessCustomers",
                column: "HQBranchId",
                principalTable: "BusinessCustomerBranchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomers_BusinessConactAccounts_MainContactId1",
                table: "BusinessCustomers");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomers_BusinessCustomerBranchs_HQBranchId1",
                table: "BusinessCustomers");

            migrationBuilder.DropIndex(
                name: "IX_BusinessCustomers_HQBranchId",
                table: "BusinessCustomers");

            migrationBuilder.DropIndex(
                name: "IX_BusinessCustomers_MainContactId",
                table: "BusinessCustomers");

            migrationBuilder.DropColumn(
                name: "HQBranchId",
                table: "BusinessCustomers");

            migrationBuilder.DropColumn(
                name: "HQBranchId",
                table: "BusinessCustomers");

            migrationBuilder.DropColumn(
                name: "MainContactId",
                table: "BusinessCustomers");

            migrationBuilder.DropColumn(
                name: "MainContactId",
                table: "BusinessCustomers");
        }
    }
}
