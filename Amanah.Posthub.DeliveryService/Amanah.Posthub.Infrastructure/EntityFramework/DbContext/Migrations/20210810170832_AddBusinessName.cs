﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddBusinessName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {




            migrationBuilder.AddColumn<string>(
                name: "BusinessName",
                table: "BusinessCustomers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "BusinessCustomers",
                type: "bit",
                nullable: false,
                defaultValue: false);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.DropColumn(
                name: "BusinessName",
                table: "BusinessCustomers");

            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "BusinessCustomers");
        }
    }
}
