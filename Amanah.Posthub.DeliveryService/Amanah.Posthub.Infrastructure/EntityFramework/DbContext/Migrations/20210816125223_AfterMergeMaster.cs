﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AfterMergeMaster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "WorkingHours",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Latitude",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BusinessAddress_Longitude",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAddress_Latitude",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OwnerAddress_Longitude",
                table: "TenantRegistrations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Latitude",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Longitude",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Latitude",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Longitude",
                table: "Manager",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Latitude",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address_Longitude",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Latitude",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Longitude",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CourierZoneId",
                table: "Country",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Latitude",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddress_Longitude",
                table: "Company",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MainContactId",
                table: "BusinessCustomers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "HQBranchId",
                table: "BusinessCustomers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "Location_Latitude",
                table: "BusinessCustomerBranchs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Longitude",
                table: "BusinessCustomerBranchs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                table: "Branch",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Latitude",
                table: "Branch",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location_Longitude",
                table: "Branch",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Area",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "StateId",
                table: "Area",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Courier",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courier", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "State",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_State", x => x.Id);
                    table.ForeignKey(
                        name: "FK_State_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CourierZone",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CourierId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourierZone", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CourierZone_Courier_CourierId",
                        column: x => x.CourierId,
                        principalTable: "Courier",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkingHours_BranchId",
                table: "WorkingHours",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_Country_CourierZoneId",
                table: "Country",
                column: "CourierZoneId");



            migrationBuilder.CreateIndex(
                name: "IX_Area_StateId",
                table: "Area",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_CourierZone_CourierId",
                table: "CourierZone",
                column: "CourierId");

            migrationBuilder.CreateIndex(
                name: "IX_State_CountryId",
                table: "State",
                column: "CountryId");




            migrationBuilder.AddForeignKey(
                name: "FK_WorkingHours_Branch_BranchId",
                table: "WorkingHours",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Area_State_StateId",
            //    table: "Area");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomerBranchs_BusinessConactAccounts_BranchContactId1",
                table: "BusinessCustomerBranchs");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomerBranchs_BusinessConactAccounts_PickupContactId1",
                table: "BusinessCustomerBranchs");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomers_BusinessConactAccounts_MainContactId1",
                table: "BusinessCustomers");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomers_BusinessCustomerBranchs_HQBranchId",
                table: "BusinessCustomers");


            migrationBuilder.DropForeignKey(
                name: "FK_WorkingHours_Branch_BranchId",
                table: "WorkingHours");

            migrationBuilder.DropTable(
                name: "CourierZone");

            migrationBuilder.DropTable(
                name: "State");

            migrationBuilder.DropTable(
                name: "Courier");

            migrationBuilder.DropIndex(
                name: "IX_WorkingHours_BranchId",
                table: "WorkingHours");

            migrationBuilder.DropIndex(
                name: "IX_Country_CourierZoneId",
                table: "Country");

            migrationBuilder.DropIndex(
                name: "IX_BusinessCustomers_HQBranchId",
                table: "BusinessCustomers");

            migrationBuilder.DropIndex(
                name: "IX_BusinessCustomers_MainContactId",
                table: "BusinessCustomers");

            migrationBuilder.DropIndex(
                name: "IX_BusinessCustomerBranchs_BranchContactId1",
                table: "BusinessCustomerBranchs");

            migrationBuilder.DropIndex(
                name: "IX_BusinessCustomerBranchs_PickupContactId1",
                table: "BusinessCustomerBranchs");

            migrationBuilder.DropIndex(
                name: "IX_Area_StateId",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "WorkingHours");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Latitude",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "BusinessAddress_Longitude",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAddress_Latitude",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "OwnerAddress_Longitude",
                table: "TenantRegistrations");

            migrationBuilder.DropColumn(
                name: "Location_Latitude",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "Location_Longitude",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "Address_Latitude",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Longitude",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "Address_Latitude",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Address_Longitude",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "Location_Latitude",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Location_Longitude",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CourierZoneId",
                table: "Country");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Latitude",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyAddress_Longitude",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Location_Latitude",
                table: "BusinessCustomerBranchs");

            migrationBuilder.DropColumn(
                name: "Location_Longitude",
                table: "BusinessCustomerBranchs");

            migrationBuilder.DropColumn(
                name: "Location_Latitude",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "Location_Longitude",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "StateId",
                table: "Area");

            migrationBuilder.AlterColumn<int>(
                name: "MainContactId",
                table: "BusinessCustomers",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HQBranchId",
                table: "BusinessCustomers",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HQBranchId1",
                table: "BusinessCustomers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MainContactId1",
                table: "BusinessCustomers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BranchContactId1",
                table: "BusinessCustomerBranchs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PickupContactId1",
                table: "BusinessCustomerBranchs",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                table: "Branch",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomers_HQBranchId1",
                table: "BusinessCustomers",
                column: "HQBranchId1");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomers_MainContactId1",
                table: "BusinessCustomers",
                column: "MainContactId1");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomerBranchs_BranchContactId1",
                table: "BusinessCustomerBranchs",
                column: "BranchContactId1");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessCustomerBranchs_PickupContactId1",
                table: "BusinessCustomerBranchs",
                column: "PickupContactId1");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomerBranchs_BusinessConactAccounts_BranchContactId1",
                table: "BusinessCustomerBranchs",
                column: "BranchContactId1",
                principalTable: "BusinessConactAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomerBranchs_BusinessConactAccounts_PickupContactId1",
                table: "BusinessCustomerBranchs",
                column: "PickupContactId1",
                principalTable: "BusinessConactAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomers_BusinessConactAccounts_MainContactId1",
                table: "BusinessCustomers",
                column: "MainContactId1",
                principalTable: "BusinessConactAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomers_BusinessCustomerBranchs_HQBranchId1",
                table: "BusinessCustomers",
                column: "HQBranchId1",
                principalTable: "BusinessCustomerBranchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
