﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddOrderModules : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Area_State_StateId",
            //    table: "Area");

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Country_CourierZone_CourierZoneId",
            //    table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_CourierZone_Courier_CourierId",
                table: "CourierZone");

            migrationBuilder.DropForeignKey(
                name: "FK_State_Country_CountryId",
                table: "State");

            migrationBuilder.DropPrimaryKey(
                name: "PK_State",
                table: "State");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CourierZone",
                table: "CourierZone");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Courier",
                table: "Courier");

            migrationBuilder.RenameTable(
                name: "State",
                newName: "States");

            migrationBuilder.RenameTable(
                name: "CourierZone",
                newName: "CourierZones");

            migrationBuilder.RenameTable(
                name: "Courier",
                newName: "Couriers");

            migrationBuilder.RenameIndex(
                name: "IX_State_CountryId",
                table: "States",
                newName: "IX_States_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_CourierZone_CourierId",
                table: "CourierZones",
                newName: "IX_CourierZones_CourierId");

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "States",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "CourierZones",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Couriers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_States",
                table: "States",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CourierZones",
                table: "CourierZones",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Couriers",
                table: "Couriers",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AddressTags",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AddressTags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MidelName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Lastname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AddressId = table.Column<int>(type: "int", nullable: false),
                    Address_PACINumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Governorate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Area = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Block = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Building = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Floor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Flat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Latitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address_Longitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Contract",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contract", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ContractStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CourierShipmentServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourierShipmentServices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CreditStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreditStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Symbol = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DriverDutyStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverDutyStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FileType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FileType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Industries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Industries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ItemReturnReasons",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemReturnReasons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LengthUOMs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LengthUOMs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MailItemStatuss",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rank = table.Column<int>(type: "int", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailItemStatuss", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MailItemTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailItemTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderRecurencies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderRecurencies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rank = table.Column<int>(type: "int", nullable: false),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "OrderTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PaymentMethods",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServiceTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ShipServiceTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShipServiceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserAccessTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccessTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserAccountStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rank = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccountStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WeightUOMs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeightUOMs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkOrderStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rank = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkOrderStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkorderType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkorderType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DirectOrders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<int>(type: "int", nullable: true),
                    DropOffAddress_PACINumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Governorate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Area = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Block = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Building = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Floor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Flat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Latitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Longitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SenderInfoId = table.Column<int>(type: "int", nullable: true),
                    ConsigneeInfoId = table.Column<int>(type: "int", nullable: true),
                    IsMatchConsigneeID = table.Column<bool>(type: "bit", nullable: false),
                    ReciverInfoId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DirectOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DirectOrders_Account_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DirectOrders_Contacts_ConsigneeInfoId",
                        column: x => x.ConsigneeInfoId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DirectOrders_Contacts_ReciverInfoId",
                        column: x => x.ReciverInfoId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DirectOrders_Contacts_SenderInfoId",
                        column: x => x.SenderInfoId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BusinessOrders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerId = table.Column<int>(type: "int", nullable: true),
                    ContactId = table.Column<int>(type: "int", nullable: true),
                    ContractId = table.Column<int>(type: "int", nullable: true),
                    MailItemsTypeId = table.Column<int>(type: "int", nullable: true),
                    PostagePerPiece = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CollectionFormSerialNo = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_Contract_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_MailItemTypes_MailItemsTypeId",
                        column: x => x.MailItemsTypeId,
                        principalTable: "MailItemTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Workorders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    WorkorderTypeId = table.Column<int>(type: "int", nullable: true),
                    MainTaskId = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workorders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Workorders_MainTask_MainTaskId",
                        column: x => x.MainTaskId,
                        principalTable: "MainTask",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Workorders_WorkorderType_WorkorderTypeId",
                        column: x => x.WorkorderTypeId,
                        principalTable: "WorkorderType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AttachmentFiles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Path = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FileTypeId = table.Column<int>(type: "int", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BusinessOrderId = table.Column<int>(type: "int", nullable: true),
                    BusinessOrderId1 = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachmentFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AttachmentFiles_BusinessOrders_BusinessOrderId",
                        column: x => x.BusinessOrderId,
                        principalTable: "BusinessOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AttachmentFiles_BusinessOrders_BusinessOrderId1",
                        column: x => x.BusinessOrderId1,
                        principalTable: "BusinessOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AttachmentFiles_FileType_FileTypeId",
                        column: x => x.FileTypeId,
                        principalTable: "FileType",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: false),
                    OrderTypeId = table.Column<int>(type: "int", nullable: false),
                    ServiceSectorId = table.Column<int>(type: "int", nullable: false),
                    ServiceTypeId = table.Column<int>(type: "int", nullable: false),
                    IssueAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ReadyAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StartedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeliveryBefore = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FulfilledAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TotalWeight = table.Column<decimal>(type: "decimal(7,3)", nullable: false),
                    WeightUOMId = table.Column<int>(type: "int", nullable: false),
                    TotalPostage = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PaymentMethodId = table.Column<int>(type: "int", nullable: false),
                    CurrencyId = table.Column<int>(type: "int", nullable: false),
                    BusinessOrderId = table.Column<int>(type: "int", nullable: false),
                    DirectOrderId = table.Column<int>(type: "int", nullable: false),
                    MailItemsCount = table.Column<int>(type: "int", nullable: false),
                    PickupAddress_PACINumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Governorate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Area = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Block = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Building = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Floor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Flat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Latitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupAddress_Longitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickupNote = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffNote = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_BusinessOrders_BusinessOrderId",
                        column: x => x.BusinessOrderId,
                        principalTable: "BusinessOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Currency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_DirectOrders_DirectOrderId",
                        column: x => x.DirectOrderId,
                        principalTable: "DirectOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_OrderStatus_StatusId",
                        column: x => x.StatusId,
                        principalTable: "OrderStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_OrderTypes_OrderTypeId",
                        column: x => x.OrderTypeId,
                        principalTable: "OrderTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_PaymentMethods_PaymentMethodId",
                        column: x => x.PaymentMethodId,
                        principalTable: "PaymentMethods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_ServiceSectors_ServiceSectorId",
                        column: x => x.ServiceSectorId,
                        principalTable: "ServiceSectors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_ServiceTypes_ServiceTypeId",
                        column: x => x.ServiceTypeId,
                        principalTable: "ServiceTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_WeightUOMs_WeightUOMId",
                        column: x => x.WeightUOMId,
                        principalTable: "WeightUOMs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MailItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MailItemBarCode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeliveryBefore = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PickUpAddress_PACINumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Governorate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Area = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Block = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Building = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Floor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Flat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Latitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PickUpAddress_Longitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_PACINumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Governorate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Area = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Block = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Street = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Building = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Floor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Flat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Latitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DropOffAddress_Longitude = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConsigneeInfoId = table.Column<int>(type: "int", nullable: false),
                    ReciverInfoId = table.Column<int>(type: "int", nullable: false),
                    IsMatchConsigneeID = table.Column<bool>(type: "bit", nullable: false),
                    ProofOfDeliveryImageId = table.Column<int>(type: "int", nullable: true),
                    SignatureImageId = table.Column<int>(type: "int", nullable: true),
                    Weight = table.Column<int>(type: "int", nullable: false),
                    WeightUOM = table.Column<int>(type: "int", nullable: false),
                    Hight = table.Column<int>(type: "int", nullable: false),
                    Width = table.Column<int>(type: "int", nullable: false),
                    ReservedSpecial = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ItemTypeId = table.Column<int>(type: "int", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: false),
                    WorkorderId = table.Column<int>(type: "int", nullable: false),
                    OrderId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MailItems_AttachmentFiles_ProofOfDeliveryImageId",
                        column: x => x.ProofOfDeliveryImageId,
                        principalTable: "AttachmentFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MailItems_AttachmentFiles_SignatureImageId",
                        column: x => x.SignatureImageId,
                        principalTable: "AttachmentFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MailItems_Contacts_ConsigneeInfoId",
                        column: x => x.ConsigneeInfoId,
                        principalTable: "Contacts",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MailItems_Contacts_ReciverInfoId",
                        column: x => x.ReciverInfoId,
                        principalTable: "Contacts",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MailItems_MailItemStatuss_StatusId",
                        column: x => x.StatusId,
                        principalTable: "MailItemStatuss",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MailItems_MailItemTypes_ItemTypeId",
                        column: x => x.ItemTypeId,
                        principalTable: "MailItemTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MailItems_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MailItems_Workorders_WorkorderId",
                        column: x => x.WorkorderId,
                        principalTable: "Workorders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrdersStatusLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OldStatusId = table.Column<int>(type: "int", nullable: false),
                    NewStatusId = table.Column<int>(type: "int", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OrderId = table.Column<int>(type: "int", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrdersStatusLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrdersStatusLogs_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrdersStatusLogs_OrderStatus_NewStatusId",
                        column: x => x.NewStatusId,
                        principalTable: "OrderStatus",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_OrdersStatusLogs_OrderStatus_OldStatusId",
                        column: x => x.OldStatusId,
                        principalTable: "OrderStatus",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MailItemsStatusLogs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MailItemId = table.Column<int>(type: "int", nullable: false),
                    OldStatusId = table.Column<int>(type: "int", nullable: false),
                    NewStatusId = table.Column<int>(type: "int", nullable: false),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MailItemsStatusLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MailItemsStatusLogs_MailItems_MailItemId",
                        column: x => x.MailItemId,
                        principalTable: "MailItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MailItemsStatusLogs_MailItemStatuss_NewStatusId",
                        column: x => x.NewStatusId,
                        principalTable: "MailItemStatuss",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_MailItemsStatusLogs_MailItemStatuss_OldStatusId",
                        column: x => x.OldStatusId,
                        principalTable: "MailItemStatuss",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "AddressTags",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, true, false, false, "منزل" },
                    { 2, true, false, false, "عمل" },
                    { 3, true, false, false, "مؤقت" },
                    { 4, true, false, false, "جديد" }
                });

            migrationBuilder.InsertData(
                table: "ContractStatus",
                columns: new[] { "Id", "Description", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, "", true, false, false, "WaitingApproval" },
                    { 2, "", true, false, false, "Active" },
                    { 3, "", true, false, false, "Inactive" },
                    { 4, "", true, false, false, "OnHold" },
                    { 5, "", true, false, false, "Blocked" },
                    { 6, "", true, false, false, "Canceled" },
                    { 7, "", true, false, false, "InRenewal" },
                    { 8, "", true, false, false, "Closed" }
                });

            migrationBuilder.InsertData(
                table: "CourierShipmentServices",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 4, true, false, false, "IEE" },
                    { 3, true, false, false, "IPF" },
                    { 2, true, false, false, "IE" },
                    { 1, true, false, false, "IP" }
                });

            migrationBuilder.InsertData(
                table: "CreditStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, true, false, false, "إئتمان" },
                    { 2, true, false, false, "نقدي" },
                    { 3, true, false, false, "إيداع" },
                    { 4, true, false, false, "محظور" }
                });

            migrationBuilder.InsertData(
                table: "Currency",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "IsSystem", "Name", "Symbol" },
                values: new object[,]
                {
                    { 1, "USD", true, false, false, "", null },
                    { 2, "GBP", true, false, false, "", null },
                    { 3, "KWD", true, false, false, "", null },
                    { 4, "SAR", true, false, false, "", null },
                    { 5, "EUR", true, false, false, "", null }
                });

            migrationBuilder.InsertData(
                table: "DriverDutyStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 5, true, false, false, "OnBreak" },
                    { 4, true, false, false, "Going-off-duty" },
                    { 3, true, false, false, "OffDuty" },
                    { 2, true, false, false, "Away" },
                    { 1, true, false, false, "OnDuty" }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, true, false, false, "بنوك" },
                    { 2, true, false, false, "محاكم" },
                    { 3, true, false, false, "مجمعات" },
                    { 4, true, false, false, "نوادي" },
                    { 5, true, false, false, "محامون" },
                    { 6, true, false, false, " اتحادات" },
                    { 7, true, false, false, "نقابات" },
                    { 8, true, false, false, "كيانات غير ربحيه" },
                    { 9, true, false, false, "جمعيات" },
                    { 10, true, false, false, "دعاية" },
                    { 11, true, false, false, "مطبوعات" }
                });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[] { 2, "en-us", true, false, false, "English" });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[] { 1, "ar-eg", true, false, false, "العربيه" });

            migrationBuilder.InsertData(
                table: "LengthUOMs",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, "Cm", true, false, false, "centemeter" },
                    { 2, "Inch", true, false, false, "inches" },
                    { 3, "Feet", true, false, false, "feet" }
                });

            migrationBuilder.InsertData(
                table: "MailItemStatuss",
                columns: new[] { "Id", "Description", "IsActive", "IsDeleted", "IsSystem", "Name", "Rank" },
                values: new object[,]
                {
                    { 20, null, true, false, false, "  العنصر مرتجع", 0 },
                    { 19, null, true, false, false, "  العنصر موقوف", 0 },
                    { 18, null, true, false, false, "  تم تسليم العنصر", 0 },
                    { 16, null, true, false, false, " العنصر تحت التسليم", 0 },
                    { 15, null, true, false, false, "  تم توزيع العنصر", 0 },
                    { 14, null, true, false, false, "  العنصر تحت التوزيع", 0 },
                    { 13, null, true, false, false, "  تم عنونة العنصر", 0 },
                    { 12, null, true, false, false, "  تم فرز العنصر", 0 },
                    { 11, null, true, false, false, " العنصر تحت الفرز", 0 },
                    { 17, null, true, false, false, " جاري تسليم العنصر", 0 },
                    { 9, null, true, false, false, "  تم التقاط العنصر", 0 },
                    { 8, null, true, false, false, "  تم فرز الطلب", 0 },
                    { 7, null, true, false, false, "  طلب تحت الفرز ", 0 },
                    { 6, null, true, false, false, " تم رفع بيانات العناصر", 0 },
                    { 5, null, true, false, false, "لرفع بيانات العناصر", 0 },
                    { 4, null, true, false, false, "تم التقاط الطلب", 0 },
                    { 10, null, true, false, false, "  تم رفع العنصر", 0 },
                    { 3, null, true, false, false, "تم التوزيع لسائق", 0 },
                    { 2, null, true, false, false, "تم توزي ع الطلب", 0 },
                    { 1, null, true, false, false, "طلب جديد", 0 }
                });

            migrationBuilder.InsertData(
                table: "MailItemTypes",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 6, true, false, false, "Commodity" },
                    { 4, true, false, false, "Post Card" },
                    { 5, true, false, false, "Parcel" },
                    { 2, true, false, false, "Letter" },
                    { 1, true, false, false, "Root" },
                    { 3, true, false, false, "Flat" }
                });

            migrationBuilder.InsertData(
                table: "OrderRecurencies",
                columns: new[] { "Id", "Code", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, "", true, false, false, "Daily" },
                    { 2, "", true, false, false, "Weekly" },
                    { 3, "", true, false, false, "BiWeekly" },
                    { 4, "", true, false, false, "Monthly" },
                    { 5, "", true, false, false, "AtDayofMonth" },
                    { 6, "", true, false, false, "AtDayofWeek" }
                });

            migrationBuilder.InsertData(
                table: "OrderStatus",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "IsSystem", "Name", "Rank" },
                values: new object[,]
                {
                    { 10, "", "", true, false, false, "On-Hold", 0 },
                    { 15, "", "", true, false, false, "Canceled", 0 },
                    { 14, "", "", true, false, false, "ClosedWithReturns", 0 },
                    { 16, "", "", true, false, false, "PassedToCorrier", 0 },
                    { 12, "", "", true, false, false, "Driver at Delivery", 0 },
                    { 11, "", "", true, false, false, "Dispatched", 0 }
                });

            migrationBuilder.InsertData(
                table: "OrderStatus",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "IsSystem", "Name", "Rank" },
                values: new object[,]
                {
                    { 9, "", "", true, false, false, "Sorted", 0 },
                    { 13, "", "", true, false, false, "Delivered", 0 },
                    { 7, "", "", true, false, false, "Uploaded", 0 },
                    { 6, "", "", true, false, false, "In-upload", 0 },
                    { 5, "", "", true, false, false, "Received", 0 },
                    { 4, "", "", true, false, false, "Picked-up", 0 },
                    { 3, "", "", true, false, false, "Driver at Pickup", 0 },
                    { 2, "", "", true, false, false, "Assigned", 0 },
                    { 1, "", "", true, false, false, "Registered", 0 },
                    { 8, "", "", true, false, false, "In-Sorting", 0 }
                });

            migrationBuilder.InsertData(
                table: "OrderTypes",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 2, true, false, false, "عادي" },
                    { 1, true, false, false, "تجاري" }
                });

            migrationBuilder.InsertData(
                table: "PaymentMethods",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, true, false, false, "نقدي" },
                    { 2, true, false, false, "فيزا" },
                    { 3, true, false, false, "إيداع" },
                    { 4, true, false, false, "KNET" }
                });

            migrationBuilder.InsertData(
                table: "ShipServiceTypes",
                columns: new[] { "Id", "Description", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, null, true, false, false, " IP" },
                    { 2, null, true, false, false, " IE" },
                    { 3, null, true, false, false, "  IPF" },
                    { 4, null, true, false, false, " IEE" }
                });

            migrationBuilder.InsertData(
                table: "UserAccessTypes",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 2, true, false, false, "WebAcess" },
                    { 1, true, false, false, "MobileAcess" }
                });

            migrationBuilder.InsertData(
                table: "UserAccountStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name", "Rank" },
                values: new object[,]
                {
                    { 1, true, false, false, "WaitingApproval", 0 },
                    { 2, true, false, false, "Active", 0 },
                    { 3, true, false, false, "Inactive", 0 },
                    { 4, true, false, false, "OnHold", 0 },
                    { 5, true, false, false, "Blocked", 0 },
                    { 6, true, false, false, " Canceled", 0 }
                });

            migrationBuilder.InsertData(
                table: "WeightUOMs",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 3, "lb", "is a unit of mass used in British imperial and United States customary systems of measurement", true, false, false, "Pound" },
                    { 1, "KG", "A unit of mass equal to one thousand grams.", true, false, false, "Kilogram" },
                    { 2, "G", "", true, false, false, "Gram" }
                });

            migrationBuilder.InsertData(
                table: "WorkOrderStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name", "Rank" },
                values: new object[,]
                {
                    { 8, true, false, false, "مفتوح للعمل", 0 },
                    { 7, true, false, false, "في قائمة السائق", 0 },
                    { 6, true, false, false, " مرفوض", 0 },
                    { 5, true, false, false, "تم التوزيع", 0 },
                    { 9, true, false, false, "ملغي", 0 },
                    { 3, true, false, false, "موقوف", 0 },
                    { 2, true, false, false, "استكمال عتاصر", 0 },
                    { 1, true, false, false, "جديد", 0 },
                    { 4, true, false, false, "للتوزيع", 0 },
                    { 10, true, false, false, " مغلق", 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AttachmentFiles_BusinessOrderId",
                table: "AttachmentFiles",
                column: "BusinessOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachmentFiles_BusinessOrderId1",
                table: "AttachmentFiles",
                column: "BusinessOrderId1");

            migrationBuilder.CreateIndex(
                name: "IX_AttachmentFiles_FileTypeId",
                table: "AttachmentFiles",
                column: "FileTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_ContactId",
                table: "BusinessOrders",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_ContractId",
                table: "BusinessOrders",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_CustomerId",
                table: "BusinessOrders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_MailItemsTypeId",
                table: "BusinessOrders",
                column: "MailItemsTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_DirectOrders_ConsigneeInfoId",
                table: "DirectOrders",
                column: "ConsigneeInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_DirectOrders_CustomerId",
                table: "DirectOrders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_DirectOrders_ReciverInfoId",
                table: "DirectOrders",
                column: "ReciverInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_DirectOrders_SenderInfoId",
                table: "DirectOrders",
                column: "SenderInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ConsigneeInfoId",
                table: "MailItems",
                column: "ConsigneeInfoId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ItemTypeId",
                table: "MailItems",
                column: "ItemTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_OrderId",
                table: "MailItems",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ProofOfDeliveryImageId",
                table: "MailItems",
                column: "ProofOfDeliveryImageId");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ReciverInfoId",
                table: "MailItems",
                column: "ReciverInfoId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_SignatureImageId",
                table: "MailItems",
                column: "SignatureImageId");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_StatusId",
                table: "MailItems",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_WorkorderId",
                table: "MailItems",
                column: "WorkorderId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_MailItemsStatusLogs_MailItemId",
            //    table: "MailItemsStatusLogs",
            //    column: "MailItemId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_MailItemsStatusLogs_NewStatusId",
            //    table: "MailItemsStatusLogs",
            //    column: "NewStatusId",
            //    unique: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_MailItemsStatusLogs_OldStatusId",
            //    table: "MailItemsStatusLogs",
            //    column: "OldStatusId",
            //    unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BusinessOrderId",
                table: "Orders",
                column: "BusinessOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CurrencyId",
                table: "Orders",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DirectOrderId",
                table: "Orders",
                column: "DirectOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrderTypeId",
                table: "Orders",
                column: "OrderTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PaymentMethodId",
                table: "Orders",
                column: "PaymentMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ServiceSectorId",
                table: "Orders",
                column: "ServiceSectorId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ServiceTypeId",
                table: "Orders",
                column: "ServiceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_StatusId",
                table: "Orders",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_WeightUOMId",
                table: "Orders",
                column: "WeightUOMId");

            migrationBuilder.CreateIndex(
                name: "IX_OrdersStatusLogs_NewStatusId",
                table: "OrdersStatusLogs",
                column: "NewStatusId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrdersStatusLogs_OldStatusId",
                table: "OrdersStatusLogs",
                column: "OldStatusId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrdersStatusLogs_OrderId",
                table: "OrdersStatusLogs",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Workorders_MainTaskId",
                table: "Workorders",
                column: "MainTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_Workorders_WorkorderTypeId",
                table: "Workorders",
                column: "WorkorderTypeId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Area_States_StateId",
            //    table: "Area",
            //    column: "StateId",
            //    principalTable: "States",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Country_CourierZones_CourierZoneId",
            //    table: "Country",
            //    column: "CourierZoneId",
            //    principalTable: "CourierZones",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CourierZones_Couriers_CourierId",
                table: "CourierZones",
                column: "CourierId",
                principalTable: "Couriers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_States_Country_CountryId",
                table: "States",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Area_States_StateId",
                table: "Area");

            migrationBuilder.DropForeignKey(
                name: "FK_Country_CourierZones_CourierZoneId",
                table: "Country");

            migrationBuilder.DropForeignKey(
                name: "FK_CourierZones_Couriers_CourierId",
                table: "CourierZones");

            migrationBuilder.DropForeignKey(
                name: "FK_States_Country_CountryId",
                table: "States");

            migrationBuilder.DropTable(
                name: "AddressTags");

            migrationBuilder.DropTable(
                name: "ContractStatus");

            migrationBuilder.DropTable(
                name: "CourierShipmentServices");

            migrationBuilder.DropTable(
                name: "CreditStatus");

            migrationBuilder.DropTable(
                name: "DriverDutyStatus");

            migrationBuilder.DropTable(
                name: "Industries");

            migrationBuilder.DropTable(
                name: "ItemReturnReasons");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "LengthUOMs");

            migrationBuilder.DropTable(
                name: "MailItemsStatusLogs");

            migrationBuilder.DropTable(
                name: "OrderRecurencies");

            migrationBuilder.DropTable(
                name: "OrdersStatusLogs");

            migrationBuilder.DropTable(
                name: "ShipServiceTypes");

            migrationBuilder.DropTable(
                name: "UserAccessTypes");

            migrationBuilder.DropTable(
                name: "UserAccountStatus");

            migrationBuilder.DropTable(
                name: "WorkOrderStatus");

            migrationBuilder.DropTable(
                name: "MailItems");

            migrationBuilder.DropTable(
                name: "AttachmentFiles");

            migrationBuilder.DropTable(
                name: "MailItemStatuss");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Workorders");

            migrationBuilder.DropTable(
                name: "FileType");

            migrationBuilder.DropTable(
                name: "BusinessOrders");

            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.DropTable(
                name: "DirectOrders");

            migrationBuilder.DropTable(
                name: "OrderStatus");

            migrationBuilder.DropTable(
                name: "OrderTypes");

            migrationBuilder.DropTable(
                name: "PaymentMethods");

            migrationBuilder.DropTable(
                name: "ServiceTypes");

            migrationBuilder.DropTable(
                name: "WeightUOMs");

            migrationBuilder.DropTable(
                name: "WorkorderType");

            migrationBuilder.DropTable(
                name: "Contract");

            migrationBuilder.DropTable(
                name: "MailItemTypes");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_States",
                table: "States");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CourierZones",
                table: "CourierZones");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Couriers",
                table: "Couriers");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "States");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "CourierZones");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Couriers");

            migrationBuilder.RenameTable(
                name: "States",
                newName: "State");

            migrationBuilder.RenameTable(
                name: "CourierZones",
                newName: "CourierZone");

            migrationBuilder.RenameTable(
                name: "Couriers",
                newName: "Courier");

            migrationBuilder.RenameIndex(
                name: "IX_States_CountryId",
                table: "State",
                newName: "IX_State_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_CourierZones_CourierId",
                table: "CourierZone",
                newName: "IX_CourierZone_CourierId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_State",
                table: "State",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CourierZone",
                table: "CourierZone",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Courier",
                table: "Courier",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_State_StateId",
                table: "Area",
                column: "StateId",
                principalTable: "State",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Country_CourierZone_CourierZoneId",
                table: "Country",
                column: "CourierZoneId",
                principalTable: "CourierZone",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CourierZone_Courier_CourierId",
                table: "CourierZone",
                column: "CourierId",
                principalTable: "Courier",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_State_Country_CountryId",
                table: "State",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
