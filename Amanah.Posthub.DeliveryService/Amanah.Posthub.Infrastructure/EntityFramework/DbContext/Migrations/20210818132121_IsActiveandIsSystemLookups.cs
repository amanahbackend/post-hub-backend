﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class IsActiveandIsSystemLookups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Area_States_StateId",
            //    table: "Area");

            migrationBuilder.DropTable(
                name: "DriverDutyStatus");

            migrationBuilder.DropTable(
                name: "Industries");

            migrationBuilder.DropTable(
                name: "States");

            migrationBuilder.DropTable(
                name: "UserAccessTypes");

            migrationBuilder.DropTable(
                name: "UserAccountStatus");

            migrationBuilder.DropIndex(
                name: "IX_Area_StateId",
                table: "Area");

            migrationBuilder.RenameColumn(
                name: "StateId",
                table: "Area",
                newName: "GovernrateId");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ServiceSectors",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "ServiceSectors",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Governrates",
                type: "int",
                nullable: true,
                defaultValue: 121);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Governrates",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Genders",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Genders",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Departments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Departments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Courtesys",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Courtesys",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ContactFunctions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "ContactFunctions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "CollectionMethods",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "CollectionMethods",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "BusinessTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "BusinessTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "GovernrateId1",
                table: "Area",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "AccountStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "AccountStatus",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Governrates_CountryId",
                table: "Governrates",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Area_GovernrateId1",
                table: "Area",
                column: "GovernrateId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_Governrates_GovernrateId1",
                table: "Area",
                column: "GovernrateId1",
                principalTable: "Governrates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Governrates_Country_CountryId",
                table: "Governrates",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Area_Governrates_GovernrateId1",
                table: "Area");

            migrationBuilder.DropForeignKey(
                name: "FK_Governrates_Country_CountryId",
                table: "Governrates");

            migrationBuilder.DropIndex(
                name: "IX_Governrates_CountryId",
                table: "Governrates");

            migrationBuilder.DropIndex(
                name: "IX_Area_GovernrateId1",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ServiceSectors");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "ServiceSectors");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Governrates");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Governrates");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Genders");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Genders");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Departments");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Departments");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Courtesys");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Courtesys");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "ContactFunctions");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "CollectionMethods");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "BusinessTypes");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "BusinessTypes");

            migrationBuilder.DropColumn(
                name: "GovernrateId1",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "AccountStatus");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "AccountStatus");

            migrationBuilder.RenameColumn(
                name: "GovernrateId",
                table: "Area",
                newName: "StateId");

            migrationBuilder.CreateTable(
                name: "DriverDutyStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverDutyStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Industries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Industries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "States",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CountryId = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_States", x => x.Id);
                    table.ForeignKey(
                        name: "FK_States_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserAccessTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccessTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserAccountStatus",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rank = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccountStatus", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "DriverDutyStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, true, false, false, "OnDuty" },
                    { 2, true, false, false, "Away" },
                    { 3, true, false, false, "OffDuty" },
                    { 4, true, false, false, "Going-off-duty" },
                    { 5, true, false, false, "OnBreak" }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 11, true, false, false, "مطبوعات" },
                    { 10, true, false, false, "دعاية" },
                    { 9, true, false, false, "جمعيات" },
                    { 8, true, false, false, "كيانات غير ربحيه" },
                    { 6, true, false, false, " اتحادات" },
                    { 7, true, false, false, "نقابات" },
                    { 4, true, false, false, "نوادي" },
                    { 3, true, false, false, "مجمعات" },
                    { 2, true, false, false, "محاكم" },
                    { 1, true, false, false, "بنوك" },
                    { 5, true, false, false, "محامون" }
                });

            migrationBuilder.InsertData(
                table: "UserAccessTypes",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[,]
                {
                    { 1, true, false, false, "MobileAcess" },
                    { 2, true, false, false, "WebAcess" }
                });

            migrationBuilder.InsertData(
                table: "UserAccountStatus",
                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name", "Rank" },
                values: new object[,]
                {
                    { 5, true, false, false, "Blocked", 0 },
                    { 1, true, false, false, "WaitingApproval", 0 },
                    { 2, true, false, false, "Active", 0 },
                    { 3, true, false, false, "Inactive", 0 },
                    { 4, true, false, false, "OnHold", 0 },
                    { 6, true, false, false, " Canceled", 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Area_StateId",
                table: "Area",
                column: "StateId");

            migrationBuilder.CreateIndex(
                name: "IX_States_CountryId",
                table: "States",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_States_StateId",
                table: "Area",
                column: "StateId",
                principalTable: "States",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
