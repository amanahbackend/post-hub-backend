﻿using Amanah.Posthub.BLL.Authorization;
using Microsoft.EntityFrameworkCore.Migrations;
using System.Collections.Generic;
using System.Linq;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdatePermissionsForPostExpressSuperAdmin : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                     table: "AspNetRoleClaims",
                     keyColumns: new[] { "RoleId" },
                     keyValues: new object[] { "DJ7CFEE4-DDE1-4B37-95DE-C4458B2D9888" });


            AddPostExpressSuperAdminClaims(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            AddPostExpressSuperAdminClaims(migrationBuilder);
        }


        private List<string> GetAllTenantPermissions()
        {
            List<string> permissions = new List<string>();
            TenantPermissions.FeaturePermissions
                .Where(featurePermission => featurePermission.Name != PermissionFeatures.PlatformAgent)
                .ToList()
                .ForEach(featurePermission =>
                {
                    permissions.AddRange(
                         featurePermission.Permissions.Select(permission => permission).ToList());
                });
            return permissions
                .Distinct()
                .ToList();
        }


        private void AddPostExpressSuperAdminClaims(MigrationBuilder migrationBuilder)
        {
            var permissions = GetAllTenantPermissions();

            foreach (var permission in permissions)
            {
                migrationBuilder.InsertData(
                    table: "AspNetRoleClaims",
                    columns: new[] { "RoleId", "ClaimType", "ClaimValue" },
                    values: new[] { "DJ7CFEE4-DDE1-4B37-95DE-C4458B2D9888", "permission", permission });



            }

            migrationBuilder.InsertData(
                table: "AspNetUserClaims",
                columns: new[] { "UserId", "ClaimType", "ClaimValue" },
                values: new[] { "AV59A46B-72BF-4849-82D0-43851B574590", "scope", "tenant" });
        }

    }
}
