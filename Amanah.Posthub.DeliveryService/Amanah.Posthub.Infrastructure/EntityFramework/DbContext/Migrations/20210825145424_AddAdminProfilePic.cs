﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddAdminProfilePic : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProfilePhotoURL",
                table: "Admins",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Admins_NationalityId",
                table: "Admins",
                column: "NationalityId");

            migrationBuilder.AddForeignKey(
                name: "FK_Admins_Country_NationalityId",
                table: "Admins",
                column: "NationalityId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Admins_Country_NationalityId",
                table: "Admins");

            migrationBuilder.DropIndex(
                name: "IX_Admins_NationalityId",
                table: "Admins");

            migrationBuilder.DropColumn(
                name: "ProfilePhotoURL",
                table: "Admins");
        }
    }
}
