﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class removeBusinessOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachmentFiles_BusinessOrders_BusinessOrderId",
                table: "AttachmentFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachmentFiles_BusinessOrders_BusinessOrderId1",
                table: "AttachmentFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_BusinessOrders_BusinessOrderId",
                table: "Orders");

            migrationBuilder.DropTable(
                name: "BusinessOrders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_BusinessOrderId",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "BusinessOrderId1",
                table: "AttachmentFiles",
                newName: "OrderId1");

            migrationBuilder.RenameColumn(
                name: "BusinessOrderId",
                table: "AttachmentFiles",
                newName: "OrderId");

            migrationBuilder.RenameIndex(
                name: "IX_AttachmentFiles_BusinessOrderId1",
                table: "AttachmentFiles",
                newName: "IX_AttachmentFiles_OrderId1");

            migrationBuilder.RenameIndex(
                name: "IX_AttachmentFiles_BusinessOrderId",
                table: "AttachmentFiles",
                newName: "IX_AttachmentFiles_OrderId");

            migrationBuilder.AddColumn<int>(
                name: "CollectionFormSerialNo",
                table: "Orders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ContactId",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContractId",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CustomerId",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MailItemsTypeId",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PostagePerPiece",
                table: "Orders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Customers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ContactId",
                table: "Orders",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ContractId",
                table: "Orders",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_MailItemsTypeId",
                table: "Orders",
                column: "MailItemsTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_Orders_OrderId",
                table: "AttachmentFiles",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_Orders_OrderId1",
                table: "AttachmentFiles",
                column: "OrderId1",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Contacts_ContactId",
                table: "Orders",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Contract_ContractId",
                table: "Orders",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                table: "Orders",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_MailItemTypes_MailItemsTypeId",
                table: "Orders",
                column: "MailItemsTypeId",
                principalTable: "MailItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachmentFiles_Orders_OrderId",
                table: "AttachmentFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachmentFiles_Orders_OrderId1",
                table: "AttachmentFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Contacts_ContactId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Contract_ContractId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_MailItemTypes_MailItemsTypeId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_ContactId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_ContractId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_MailItemsTypeId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "CollectionFormSerialNo",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ContractId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "MailItemsTypeId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "PostagePerPiece",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Customers");

            migrationBuilder.RenameColumn(
                name: "OrderId1",
                table: "AttachmentFiles",
                newName: "BusinessOrderId1");

            migrationBuilder.RenameColumn(
                name: "OrderId",
                table: "AttachmentFiles",
                newName: "BusinessOrderId");

            migrationBuilder.RenameIndex(
                name: "IX_AttachmentFiles_OrderId1",
                table: "AttachmentFiles",
                newName: "IX_AttachmentFiles_BusinessOrderId1");

            migrationBuilder.RenameIndex(
                name: "IX_AttachmentFiles_OrderId",
                table: "AttachmentFiles",
                newName: "IX_AttachmentFiles_BusinessOrderId");

            migrationBuilder.CreateTable(
                name: "BusinessOrders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CollectionFormSerialNo = table.Column<int>(type: "int", nullable: false),
                    ContactId = table.Column<int>(type: "int", nullable: true),
                    ContractId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CustomerId = table.Column<int>(type: "int", nullable: true),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    MailItemsTypeId = table.Column<int>(type: "int", nullable: true),
                    PostagePerPiece = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_Contract_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_MailItemTypes_MailItemsTypeId",
                        column: x => x.MailItemsTypeId,
                        principalTable: "MailItemTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BusinessOrderId",
                table: "Orders",
                column: "BusinessOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_ContactId",
                table: "BusinessOrders",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_ContractId",
                table: "BusinessOrders",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_CustomerId",
                table: "BusinessOrders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_MailItemsTypeId",
                table: "BusinessOrders",
                column: "MailItemsTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_BusinessOrders_BusinessOrderId",
                table: "AttachmentFiles",
                column: "BusinessOrderId",
                principalTable: "BusinessOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_BusinessOrders_BusinessOrderId1",
                table: "AttachmentFiles",
                column: "BusinessOrderId1",
                principalTable: "BusinessOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_BusinessOrders_BusinessOrderId",
                table: "Orders",
                column: "BusinessOrderId",
                principalTable: "BusinessOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
