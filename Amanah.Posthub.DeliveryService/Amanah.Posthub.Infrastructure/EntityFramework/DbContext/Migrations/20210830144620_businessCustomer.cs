﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class businessCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachmentFiles_Orders_OrderId",
                table: "AttachmentFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachmentFiles_Orders_OrderId1",
                table: "AttachmentFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Contacts_ContactId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Contract_ContractId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_MailItemTypes_MailItemsTypeId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_ContactId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_MailItemsTypeId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_AttachmentFiles_OrderId",
                table: "AttachmentFiles");

            migrationBuilder.DropColumn(
                name: "CollectionFormSerialNo",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "PostagePerPiece",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrderId",
                table: "AttachmentFiles");

            migrationBuilder.RenameColumn(
                name: "MailItemsTypeId",
                table: "Orders",
                newName: "PickupRequestNotificationId");

            migrationBuilder.RenameColumn(
                name: "CustomerId",
                table: "Orders",
                newName: "PickupLocationId");

            migrationBuilder.RenameColumn(
                name: "ContractId",
                table: "Orders",
                newName: "DriverId");

            migrationBuilder.RenameIndex(
                name: "IX_Orders_ContractId",
                table: "Orders",
                newName: "IX_Orders_DriverId");

            migrationBuilder.RenameColumn(
                name: "OrderId1",
                table: "AttachmentFiles",
                newName: "BusinessOrderId");

            migrationBuilder.RenameIndex(
                name: "IX_AttachmentFiles_OrderId1",
                table: "AttachmentFiles",
                newName: "IX_AttachmentFiles_BusinessOrderId");

            migrationBuilder.AddColumn<bool>(
                name: "IsDraft",
                table: "Orders",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Driver",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BusinessOrders",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BusinessCustomerId = table.Column<int>(type: "int", nullable: true),
                    ContactId = table.Column<int>(type: "int", nullable: true),
                    ContractId = table.Column<int>(type: "int", nullable: true),
                    MailItemsTypeId = table.Column<int>(type: "int", nullable: true),
                    PostagePerPiece = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    CollectionFormSerialNo = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_BusinessCustomers_BusinessCustomerId",
                        column: x => x.BusinessCustomerId,
                        principalTable: "BusinessCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_Contract_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contract",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrders_MailItemTypes_MailItemsTypeId",
                        column: x => x.MailItemsTypeId,
                        principalTable: "MailItemTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BusinessOrderId",
                table: "Orders",
                column: "BusinessOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_BusinessCustomerId",
                table: "BusinessOrders",
                column: "BusinessCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_ContactId",
                table: "BusinessOrders",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_ContractId",
                table: "BusinessOrders",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_MailItemsTypeId",
                table: "BusinessOrders",
                column: "MailItemsTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_BusinessOrders_BusinessOrderId",
                table: "AttachmentFiles",
                column: "BusinessOrderId",
                principalTable: "BusinessOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_BusinessOrders_BusinessOrderId",
                table: "Orders",
                column: "BusinessOrderId",
                principalTable: "BusinessOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Driver_DriverId",
                table: "Orders",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachmentFiles_BusinessOrders_BusinessOrderId",
                table: "AttachmentFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_BusinessOrders_BusinessOrderId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Driver_DriverId",
                table: "Orders");

            migrationBuilder.DropTable(
                name: "BusinessOrders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_BusinessOrderId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "IsDraft",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Driver");

            migrationBuilder.RenameColumn(
                name: "PickupRequestNotificationId",
                table: "Orders",
                newName: "MailItemsTypeId");

            migrationBuilder.RenameColumn(
                name: "PickupLocationId",
                table: "Orders",
                newName: "CustomerId");

            migrationBuilder.RenameColumn(
                name: "DriverId",
                table: "Orders",
                newName: "ContractId");

            migrationBuilder.RenameIndex(
                name: "IX_Orders_DriverId",
                table: "Orders",
                newName: "IX_Orders_ContractId");

            migrationBuilder.RenameColumn(
                name: "BusinessOrderId",
                table: "AttachmentFiles",
                newName: "OrderId1");

            migrationBuilder.RenameIndex(
                name: "IX_AttachmentFiles_BusinessOrderId",
                table: "AttachmentFiles",
                newName: "IX_AttachmentFiles_OrderId1");

            migrationBuilder.AddColumn<int>(
                name: "CollectionFormSerialNo",
                table: "Orders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ContactId",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "PostagePerPiece",
                table: "Orders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "OrderId",
                table: "AttachmentFiles",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ContactId",
                table: "Orders",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_MailItemsTypeId",
                table: "Orders",
                column: "MailItemsTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachmentFiles_OrderId",
                table: "AttachmentFiles",
                column: "OrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_Orders_OrderId",
                table: "AttachmentFiles",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_Orders_OrderId1",
                table: "AttachmentFiles",
                column: "OrderId1",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Contacts_ContactId",
                table: "Orders",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Contract_ContractId",
                table: "Orders",
                column: "ContractId",
                principalTable: "Contract",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                table: "Orders",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_MailItemTypes_MailItemsTypeId",
                table: "Orders",
                column: "MailItemsTypeId",
                principalTable: "MailItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
