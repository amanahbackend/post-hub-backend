﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class orderNullables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomers_BusinessTypes_BusinessTypeId",
                table: "BusinessCustomers");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomers_CollectionMethods_CollectionMethodId",
                table: "BusinessCustomers");

            migrationBuilder.AlterColumn<decimal>(
                name: "PostagePerPiece",
                table: "BusinessOrders",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<int>(
                name: "CollectionFormSerialNo",
                table: "BusinessOrders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "CollectionMethodId",
                table: "BusinessCustomers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "BusinessTypeId",
                table: "BusinessCustomers",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomers_BusinessTypes_BusinessTypeId",
                table: "BusinessCustomers",
                column: "BusinessTypeId",
                principalTable: "BusinessTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomers_CollectionMethods_CollectionMethodId",
                table: "BusinessCustomers",
                column: "CollectionMethodId",
                principalTable: "CollectionMethods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomers_BusinessTypes_BusinessTypeId",
                table: "BusinessCustomers");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessCustomers_CollectionMethods_CollectionMethodId",
                table: "BusinessCustomers");

            migrationBuilder.AlterColumn<decimal>(
                name: "PostagePerPiece",
                table: "BusinessOrders",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CollectionFormSerialNo",
                table: "BusinessOrders",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CollectionMethodId",
                table: "BusinessCustomers",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BusinessTypeId",
                table: "BusinessCustomers",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomers_BusinessTypes_BusinessTypeId",
                table: "BusinessCustomers",
                column: "BusinessTypeId",
                principalTable: "BusinessTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessCustomers_CollectionMethods_CollectionMethodId",
                table: "BusinessCustomers",
                column: "CollectionMethodId",
                principalTable: "CollectionMethods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
