﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class mailItemOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_MailItemStatuss_StatusId",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_MailItemTypes_ItemTypeId",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_Workorders_WorkorderId",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItemsStatusLogs_MailItems_MailItemId",
                table: "MailItemsStatusLogs");

            //migrationBuilder.DropIndex(
            //    name: "IX_MailItemsStatusLogs_NewStatusId",
            //    table: "MailItemsStatusLogs");

            //migrationBuilder.DropIndex(
            //    name: "IX_MailItemsStatusLogs_OldStatusId",
            //    table: "MailItemsStatusLogs");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_ConsigneeInfoId",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_ReciverInfoId",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "WeightUOM",
                table: "MailItems");

            migrationBuilder.AddColumn<int>(
                name: "TotalItemCount",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OldStatusId",
                table: "MailItemsStatusLogs",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "NewStatusId",
                table: "MailItemsStatusLogs",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "MailItemId",
                table: "MailItemsStatusLogs",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "WorkorderId",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "Width",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "Weight",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "StatusId",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ReciverInfoId",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ItemTypeId",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "Hight",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeliveryBefore",
                table: "MailItems",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "ConsigneeInfoId",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "LengthUOMId",
                table: "MailItems",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WeightUOMId",
                table: "MailItems",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "Contacts",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_MailItemsStatusLogs_NewStatusId",
                table: "MailItemsStatusLogs",
                column: "NewStatusId",
                unique: true,
                filter: "[NewStatusId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MailItemsStatusLogs_OldStatusId",
                table: "MailItemsStatusLogs",
                column: "OldStatusId",
                unique: true,
                filter: "[OldStatusId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ConsigneeInfoId",
                table: "MailItems",
                column: "ConsigneeInfoId",
                unique: true,
                filter: "[ConsigneeInfoId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_LengthUOMId",
                table: "MailItems",
                column: "LengthUOMId");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ReciverInfoId",
                table: "MailItems",
                column: "ReciverInfoId",
                unique: true,
                filter: "[ReciverInfoId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_WeightUOMId",
                table: "MailItems",
                column: "WeightUOMId");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_LengthUOMs_LengthUOMId",
                table: "MailItems",
                column: "LengthUOMId",
                principalTable: "LengthUOMs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_MailItemStatuss_StatusId",
                table: "MailItems",
                column: "StatusId",
                principalTable: "MailItemStatuss",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_MailItemTypes_ItemTypeId",
                table: "MailItems",
                column: "ItemTypeId",
                principalTable: "MailItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_WeightUOMs_WeightUOMId",
                table: "MailItems",
                column: "WeightUOMId",
                principalTable: "WeightUOMs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_Workorders_WorkorderId",
                table: "MailItems",
                column: "WorkorderId",
                principalTable: "Workorders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItemsStatusLogs_MailItems_MailItemId",
                table: "MailItemsStatusLogs",
                column: "MailItemId",
                principalTable: "MailItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_LengthUOMs_LengthUOMId",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_MailItemStatuss_StatusId",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_MailItemTypes_ItemTypeId",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_WeightUOMs_WeightUOMId",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_Workorders_WorkorderId",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItemsStatusLogs_MailItems_MailItemId",
                table: "MailItemsStatusLogs");

            migrationBuilder.DropIndex(
                name: "IX_MailItemsStatusLogs_NewStatusId",
                table: "MailItemsStatusLogs");

            migrationBuilder.DropIndex(
                name: "IX_MailItemsStatusLogs_OldStatusId",
                table: "MailItemsStatusLogs");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_ConsigneeInfoId",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_LengthUOMId",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_ReciverInfoId",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_WeightUOMId",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "TotalItemCount",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "LengthUOMId",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "WeightUOMId",
                table: "MailItems");

            migrationBuilder.AlterColumn<int>(
                name: "OldStatusId",
                table: "MailItemsStatusLogs",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "NewStatusId",
                table: "MailItemsStatusLogs",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MailItemId",
                table: "MailItemsStatusLogs",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "WorkorderId",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Width",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Weight",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "StatusId",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ReciverInfoId",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ItemTypeId",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Hight",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeliveryBefore",
                table: "MailItems",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ConsigneeInfoId",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WeightUOM",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "Contacts",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MailItemsStatusLogs_NewStatusId",
                table: "MailItemsStatusLogs",
                column: "NewStatusId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MailItemsStatusLogs_OldStatusId",
                table: "MailItemsStatusLogs",
                column: "OldStatusId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ConsigneeInfoId",
                table: "MailItems",
                column: "ConsigneeInfoId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ReciverInfoId",
                table: "MailItems",
                column: "ReciverInfoId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_MailItemStatuss_StatusId",
                table: "MailItems",
                column: "StatusId",
                principalTable: "MailItemStatuss",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_MailItemTypes_ItemTypeId",
                table: "MailItems",
                column: "ItemTypeId",
                principalTable: "MailItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_Workorders_WorkorderId",
                table: "MailItems",
                column: "WorkorderId",
                principalTable: "Workorders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MailItemsStatusLogs_MailItems_MailItemId",
                table: "MailItemsStatusLogs",
                column: "MailItemId",
                principalTable: "MailItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
