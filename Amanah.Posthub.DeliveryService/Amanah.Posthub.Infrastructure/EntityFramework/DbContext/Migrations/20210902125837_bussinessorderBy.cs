﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class bussinessorderBy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderById",
                table: "BusinessOrders",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_OrderById",
                table: "BusinessOrders",
                column: "OrderById");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessOrders_BusinessConactAccounts_OrderById",
                table: "BusinessOrders",
                column: "OrderById",
                principalTable: "BusinessConactAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessOrders_BusinessConactAccounts_OrderById",
                table: "BusinessOrders");

            migrationBuilder.DropIndex(
                name: "IX_BusinessOrders_OrderById",
                table: "BusinessOrders");

            migrationBuilder.DropColumn(
                name: "OrderById",
                table: "BusinessOrders");
        }
    }
}
