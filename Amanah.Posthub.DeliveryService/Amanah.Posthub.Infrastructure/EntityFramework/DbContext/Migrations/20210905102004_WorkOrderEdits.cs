﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class WorkOrderEdits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_DirectOrders_DirectOrderId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Workorders_MainTask_MainTaskId",
                table: "Workorders");

            migrationBuilder.RenameColumn(
                name: "MainTaskId",
                table: "Workorders",
                newName: "WorkOrderStatusId");

            migrationBuilder.RenameIndex(
                name: "IX_Workorders_MainTaskId",
                table: "Workorders",
                newName: "IX_Workorders_WorkOrderStatusId");

            migrationBuilder.AddColumn<DateTime>(
                name: "AssignedDate",
                table: "Workorders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeliveryDate",
                table: "Workorders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "DriverId",
                table: "Workorders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "IssueAt",
                table: "Workorders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "PickupDate",
                table: "Workorders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<int>(
                name: "DirectOrderId",
                table: "Orders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Workorders_DriverId",
                table: "Workorders",
                column: "DriverId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_DirectOrders_DirectOrderId",
                table: "Orders",
                column: "DirectOrderId",
                principalTable: "DirectOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Workorders_Driver_DriverId",
                table: "Workorders",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Workorders_WorkOrderStatus_WorkOrderStatusId",
                table: "Workorders",
                column: "WorkOrderStatusId",
                principalTable: "WorkOrderStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_DirectOrders_DirectOrderId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Workorders_Driver_DriverId",
                table: "Workorders");

            migrationBuilder.DropForeignKey(
                name: "FK_Workorders_WorkOrderStatus_WorkOrderStatusId",
                table: "Workorders");

            migrationBuilder.DropIndex(
                name: "IX_Workorders_DriverId",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "AssignedDate",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "DeliveryDate",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "DriverId",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "IssueAt",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "PickupDate",
                table: "Workorders");

            migrationBuilder.RenameColumn(
                name: "WorkOrderStatusId",
                table: "Workorders",
                newName: "MainTaskId");

            migrationBuilder.RenameIndex(
                name: "IX_Workorders_WorkOrderStatusId",
                table: "Workorders",
                newName: "IX_Workorders_MainTaskId");

            migrationBuilder.AlterColumn<int>(
                name: "DirectOrderId",
                table: "Orders",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_DirectOrders_DirectOrderId",
                table: "Orders",
                column: "DirectOrderId",
                principalTable: "DirectOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Workorders_MainTask_MainTaskId",
                table: "Workorders",
                column: "MainTaskId",
                principalTable: "MainTask",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
