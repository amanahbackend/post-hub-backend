﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class WorkOrderAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Orders_Driver_DriverId",
            //        table: "Orders");

            //migrationBuilder.DropColumn(
            //    name: "DriverId",
            //    table: "Orders");

            migrationBuilder.RenameColumn(
                name: "LastModifiedBy",
                table: "Workorders",
                newName: "UpdatedBy_Id");

            migrationBuilder.RenameColumn(
                name: "LastModificationTime",
                table: "Workorders",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DeletionTime",
                table: "Workorders",
                newName: "DeletedDate");

            migrationBuilder.RenameColumn(
                name: "DeletedBy",
                table: "Workorders",
                newName: "DeletedBy_Id");

            migrationBuilder.RenameColumn(
                name: "CreationTime",
                table: "Workorders",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Workorders",
                newName: "CreatedBy_Id");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PickupDate",
                table: "Workorders",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "IssueAt",
                table: "Workorders",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeliveryDate",
                table: "Workorders",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "AssignedDate",
                table: "Workorders",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<string>(
                name: "AreasCovered",
                table: "Workorders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeliveryBefore",
                table: "Workorders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemsNo",
                table: "Workorders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastStatusDate",
                table: "Workorders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PlannedStart",
                table: "Workorders",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Priorty",
                table: "Workorders",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "Workorders",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Workorders_Tenant_Id",
                table: "Workorders",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Workorders_AspNetUsers_Tenant_Id",
                table: "Workorders",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workorders_AspNetUsers_Tenant_Id",
                table: "Workorders");

            migrationBuilder.DropIndex(
                name: "IX_Workorders_Tenant_Id",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "AreasCovered",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "DeliveryBefore",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "ItemsNo",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "LastStatusDate",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "PlannedStart",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "Priorty",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "Workorders");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "Workorders",
                newName: "LastModificationTime");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy_Id",
                table: "Workorders",
                newName: "LastModifiedBy");

            migrationBuilder.RenameColumn(
                name: "DeletedDate",
                table: "Workorders",
                newName: "DeletionTime");

            migrationBuilder.RenameColumn(
                name: "DeletedBy_Id",
                table: "Workorders",
                newName: "DeletedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "Workorders",
                newName: "CreationTime");

            migrationBuilder.RenameColumn(
                name: "CreatedBy_Id",
                table: "Workorders",
                newName: "CreatedBy");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PickupDate",
                table: "Workorders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "IssueAt",
                table: "Workorders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "DeliveryDate",
                table: "Workorders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "AssignedDate",
                table: "Workorders",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DriverId",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Driver_DriverId",
                table: "Orders",
                column: "DriverId",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
