﻿//using Microsoft.EntityFrameworkCore.Migrations;

//namespace Amanah.Posthub.DATA.DbContext.Migrations
//{
//    public partial class _20210908_001 : Migration
//    {
//        protected override void Up(MigrationBuilder migrationBuilder)
//        {
//            migrationBuilder.DropColumn(
//                name: "MailItemsCount",
//                table: "Orders");

//            //migrationBuilder.AddColumn<int>(
//            //    name: "ItemDeleveryTypeId",
//            //    table: "MailItems",
//            //    type: "int",
//            //    nullable: true,
//            //    defaultValue: 0);

//            //migrationBuilder.AddColumn<int>(
//            //    name: "Raf",
//            //    table: "MailItems",
//            //    type: "int",
//            //    nullable: true);

//            //migrationBuilder.AlterColumn<decimal>(
//            //    name: "PostagePerPiece",
//            //    table: "BusinessOrders",
//            //    type: "decimal(18,2)",
//            //    nullable: false,
//            //    defaultValue: 0m,
//            //    oldClrType: typeof(decimal),
//            //    oldType: "decimal(18,2)",
//            //    oldNullable: true);

//            migrationBuilder.AlterColumn<int>(
//                name: "CollectionFormSerialNo",
//                table: "BusinessOrders",
//                type: "int",
//                nullable: false,
//                defaultValue: 0,
//                oldClrType: typeof(int),
//                oldType: "int",
//                oldNullable: true);

//            migrationBuilder.AddColumn<int>(
//                name: "Raf",
//                table: "Area",
//                type: "int",
//                nullable: true);

//            migrationBuilder.CreateTable(
//                name: "ItemDeleveryType",
//                columns: table => new
//                {
//                    Id = table.Column<int>(type: "int", nullable: false)
//                        .Annotation("SqlServer:Identity", "1, 1"),
//                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
//                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
//                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
//                    IsActive = table.Column<bool>(type: "bit", nullable: false)
//                },
//                constraints: table =>
//                {
//                    table.PrimaryKey("PK_ItemDeleveryType", x => x.Id);
//                });

//            migrationBuilder.InsertData(
//                table: "ItemDeleveryType",
//                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
//                values: new object[] { 1, true, false, true, "Drop" });

//            migrationBuilder.InsertData(
//                table: "ItemDeleveryType",
//                columns: new[] { "Id", "IsActive", "IsDeleted", "IsSystem", "Name" },
//                values: new object[] { 2, true, false, true, "Pickup" });

//            //migrationBuilder.CreateIndex(
//            //    name: "IX_MailItems_ItemDeleveryTypeId",
//            //    table: "MailItems",
//            //    column: "ItemDeleveryTypeId");

//            //migrationBuilder.AddForeignKey(
//            //    name: "FK_MailItems_ItemDeleveryType_ItemDeleveryTypeId",
//            //    table: "MailItems",
//            //    column: "ItemDeleveryTypeId",
//            //    principalTable: "ItemDeleveryType",
//            //    principalColumn: "Id",
//            //    onDelete: ReferentialAction.SetNull);
//        }

//        protected override void Down(MigrationBuilder migrationBuilder)
//        {
//            migrationBuilder.DropForeignKey(
//                name: "FK_MailItems_ItemDeleveryType_ItemDeleveryTypeId",
//                table: "MailItems");

//            migrationBuilder.DropTable(
//                name: "ItemDeleveryType");

//            migrationBuilder.DropIndex(
//                name: "IX_MailItems_ItemDeleveryTypeId",
//                table: "MailItems");

//            //migrationBuilder.DropColumn(
//            //    name: "ItemDeleveryTypeId",
//            //    table: "MailItems");

//            //migrationBuilder.DropColumn(
//            //    name: "Raf",
//            //    table: "MailItems"
//            //    );

//            //migrationBuilder.DropColumn(
//            //    name: "Raf",
//            //    table: "Area");

//            migrationBuilder.AddColumn<int>(
//                name: "MailItemsCount",
//                table: "Orders",
//                type: "int",
//                nullable: true);

//            //migrationBuilder.AlterColumn<decimal>(
//            //    name: "PostagePerPiece",
//            //    table: "BusinessOrders",
//            //    type: "decimal(18,2)",
//            //    nullable: true,
//            //    oldClrType: typeof(decimal),
//            //    oldType: "decimal(18,2)");

//            migrationBuilder.AlterColumn<int>(
//                name: "CollectionFormSerialNo",
//                table: "BusinessOrders",
//                type: "int",
//                nullable: true,
//                oldClrType: typeof(int),
//                oldType: "int");
//        }
//    }
//}
