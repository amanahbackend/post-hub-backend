﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class MailItemBaseEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MailItemsCount",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "LastModifiedBy",
                table: "MailItems",
                newName: "UpdatedBy_Id");

            migrationBuilder.RenameColumn(
                name: "LastModificationTime",
                table: "MailItems",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DeletionTime",
                table: "MailItems",
                newName: "DeletedDate");

            migrationBuilder.RenameColumn(
                name: "DeletedBy",
                table: "MailItems",
                newName: "DeletedBy_Id");

            migrationBuilder.RenameColumn(
                name: "CreationTime",
                table: "MailItems",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "MailItems",
                newName: "CreatedBy_Id");

            migrationBuilder.AddColumn<int>(
                name: "ItemDeleveryTypeId",
                table: "MailItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Raf",
                table: "MailItems",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "MailItems",
                type: "varchar(36)",
                nullable: true);

            //migrationBuilder.AlterColumn<decimal>(
            //    name: "PostagePerPiece",
            //    table: "BusinessOrders",
            //    type: "decimal(18,2)",
            //    nullable: false,
            //    defaultValue: 0m,
            //    oldClrType: typeof(decimal),
            //    oldType: "decimal(18,2)",
            //    oldNullable: true);

            //migrationBuilder.AlterColumn<int>(
            //    name: "CollectionFormSerialNo",
            //    table: "BusinessOrders",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0,
            //    oldClrType: typeof(int),
            //    oldType: "int",
            //    oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Raf",
                table: "Area",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ItemDeleveryType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemDeleveryType", x => x.Id);
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_MailItems_ItemDeleveryTypeId",
            //    table: "MailItems",
            //    column: "ItemDeleveryTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_Tenant_Id",
                table: "MailItems",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_AspNetUsers_Tenant_Id",
                table: "MailItems",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            //migrationBuilder.AddForeignKey(
            //    name: "FK_MailItems_ItemDeleveryType_ItemDeleveryTypeId",
            //    table: "MailItems",
            //    column: "ItemDeleveryTypeId",
            //    principalTable: "ItemDeleveryType",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_AspNetUsers_Tenant_Id",
                table: "MailItems");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_ItemDeleveryType_ItemDeleveryTypeId",
                table: "MailItems");

            migrationBuilder.DropTable(
                name: "ItemDeleveryType");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_ItemDeleveryTypeId",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_Tenant_Id",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "ItemDeleveryTypeId",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "Raf",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "Raf",
                table: "Area");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "MailItems",
                newName: "LastModificationTime");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy_Id",
                table: "MailItems",
                newName: "LastModifiedBy");

            migrationBuilder.RenameColumn(
                name: "DeletedDate",
                table: "MailItems",
                newName: "DeletionTime");

            migrationBuilder.RenameColumn(
                name: "DeletedBy_Id",
                table: "MailItems",
                newName: "DeletedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "MailItems",
                newName: "CreationTime");

            migrationBuilder.RenameColumn(
                name: "CreatedBy_Id",
                table: "MailItems",
                newName: "CreatedBy");

            migrationBuilder.AddColumn<int>(
                name: "MailItemsCount",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "PostagePerPiece",
                table: "BusinessOrders",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<int>(
                name: "CollectionFormSerialNo",
                table: "BusinessOrders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");
        }
    }
}
