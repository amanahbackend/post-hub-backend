﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class mailItemReturnReason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DropOffDate",
                table: "MailItems",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemReturnReasonId",
                table: "MailItems",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherItemReturnReason",
                table: "MailItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CollectionFormSerialNo",
                table: "BusinessOrders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_ItemReturnReasonId",
                table: "MailItems",
                column: "ItemReturnReasonId");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_ItemReturnReasons_ItemReturnReasonId",
                table: "MailItems",
                column: "ItemReturnReasonId",
                principalTable: "ItemReturnReasons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_ItemReturnReasons_ItemReturnReasonId",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_ItemReturnReasonId",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "DropOffDate",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "ItemReturnReasonId",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "OtherItemReturnReason",
                table: "MailItems");

            migrationBuilder.AlterColumn<int>(
                name: "CollectionFormSerialNo",
                table: "BusinessOrders",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
