﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class workOrderStatusCha : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workorders_WorkOrderStatus_WorkOrderStatusId",
                table: "Workorders");

            migrationBuilder.AlterColumn<int>(
                name: "WorkOrderStatusId",
                table: "Workorders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ItemsNo",
                table: "Workorders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Workorders_WorkOrderStatus_WorkOrderStatusId",
                table: "Workorders",
                column: "WorkOrderStatusId",
                principalTable: "WorkOrderStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workorders_WorkOrderStatus_WorkOrderStatusId",
                table: "Workorders");

            migrationBuilder.AlterColumn<int>(
                name: "WorkOrderStatusId",
                table: "Workorders",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ItemsNo",
                table: "Workorders",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Workorders_WorkOrderStatus_WorkOrderStatusId",
                table: "Workorders",
                column: "WorkOrderStatusId",
                principalTable: "WorkOrderStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
