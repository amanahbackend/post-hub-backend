﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class mailItem_NewField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CaseNo",
                table: "MailItems",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FromSerial",
                table: "MailItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PickupDate",
                table: "MailItems",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CaseNo",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "FromSerial",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "PickupDate",
                table: "MailItems");
        }
    }
}
