﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddContractModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BusinessCustomerId",
                table: "Contracts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "ContractDate",
                table: "Contracts",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ContractEndDate",
                table: "Contracts",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ContractStartDate",
                table: "Contracts",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "ContractTypeId",
                table: "Contracts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "DistriputionAllowance",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "InstallmentsAfterEachMessage",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "InstallmentsAtContracting",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "InstallmentsPaymentNotLaterThan",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InstallmentsPaymentNotLaterThanPeriodType",
                table: "Contracts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsPrintAndFixEnabledEnabled",
                table: "Contracts",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsPrintReciptListAndCollectEnabled",
                table: "Contracts",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "ItemSupplyPeriod",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "NoOfMessages",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentNotLetterThan",
                table: "Contracts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PaymentNotLetterThanPeriodType",
                table: "Contracts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PaymentType",
                table: "Contracts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "ServiceDistriputionPeriod",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceSectorId",
                table: "Contracts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "CollectionFormSerialNo",
                table: "BusinessOrders",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "ContractType",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractType", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_BusinessCustomerId",
                table: "Contracts",
                column: "BusinessCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_ContractTypeId",
                table: "Contracts",
                column: "ContractTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_ServiceSectorId",
                table: "Contracts",
                column: "ServiceSectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_BusinessCustomers_BusinessCustomerId",
                table: "Contracts",
                column: "BusinessCustomerId",
                principalTable: "BusinessCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_ContractType_ContractTypeId",
                table: "Contracts",
                column: "ContractTypeId",
                principalTable: "ContractType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_ServiceSectors_ServiceSectorId",
                table: "Contracts",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_BusinessCustomers_BusinessCustomerId",
                table: "Contracts");

            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_ContractType_ContractTypeId",
                table: "Contracts");

            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_ServiceSectors_ServiceSectorId",
                table: "Contracts");

            migrationBuilder.DropTable(
                name: "ContractType");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_BusinessCustomerId",
                table: "Contracts");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_ContractTypeId",
                table: "Contracts");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_ServiceSectorId",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "BusinessCustomerId",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ContractDate",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ContractEndDate",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ContractStartDate",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ContractTypeId",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "DistriputionAllowance",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "InstallmentsAfterEachMessage",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "InstallmentsAtContracting",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "InstallmentsPaymentNotLaterThan",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "InstallmentsPaymentNotLaterThanPeriodType",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "IsPrintAndFixEnabledEnabled",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "IsPrintReciptListAndCollectEnabled",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ItemSupplyPeriod",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "NoOfMessages",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "PaymentNotLetterThan",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "PaymentNotLetterThanPeriodType",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "PaymentType",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ServiceDistriputionPeriod",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ServiceSectorId",
                table: "Contracts");

            migrationBuilder.AlterColumn<int>(
                name: "CollectionFormSerialNo",
                table: "BusinessOrders",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
