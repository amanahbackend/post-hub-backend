﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddContractTypeSeeds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LastModifiedBy",
                table: "Contracts",
                newName: "UpdatedBy_Id");

            migrationBuilder.RenameColumn(
                name: "LastModificationTime",
                table: "Contracts",
                newName: "UpdatedDate");

            migrationBuilder.RenameColumn(
                name: "DeletionTime",
                table: "Contracts",
                newName: "DeletedDate");

            migrationBuilder.RenameColumn(
                name: "DeletedBy",
                table: "Contracts",
                newName: "DeletedBy_Id");

            migrationBuilder.RenameColumn(
                name: "CreationTime",
                table: "Contracts",
                newName: "CreatedDate");

            migrationBuilder.RenameColumn(
                name: "CreatedBy",
                table: "Contracts",
                newName: "CreatedBy_Id");

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "Contracts",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "ContractType",
                columns: new[] { "Id", "Description", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[] { 1, null, true, false, true, "Unbounded  Local Distribution Contrct" });

            migrationBuilder.InsertData(
                table: "ContractType",
                columns: new[] { "Id", "Description", "IsActive", "IsDeleted", "IsSystem", "Name" },
                values: new object[] { 2, null, true, false, true, "Bounded  Local Distribution Contrct" });

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_Tenant_Id",
                table: "Contracts",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_AspNetUsers_Tenant_Id",
                table: "Contracts",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_AspNetUsers_Tenant_Id",
                table: "Contracts");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_Tenant_Id",
                table: "Contracts");

            migrationBuilder.DeleteData(
                table: "ContractType",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "ContractType",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "Contracts");

            migrationBuilder.RenameColumn(
                name: "UpdatedDate",
                table: "Contracts",
                newName: "LastModificationTime");

            migrationBuilder.RenameColumn(
                name: "UpdatedBy_Id",
                table: "Contracts",
                newName: "LastModifiedBy");

            migrationBuilder.RenameColumn(
                name: "DeletedDate",
                table: "Contracts",
                newName: "DeletionTime");

            migrationBuilder.RenameColumn(
                name: "DeletedBy_Id",
                table: "Contracts",
                newName: "DeletedBy");

            migrationBuilder.RenameColumn(
                name: "CreatedDate",
                table: "Contracts",
                newName: "CreationTime");

            migrationBuilder.RenameColumn(
                name: "CreatedBy_Id",
                table: "Contracts",
                newName: "CreatedBy");
        }
    }
}
