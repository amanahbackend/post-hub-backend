﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AreaConfigurationWithGovernorate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Area_Governrates_GovernrateId",
            //        table: "Area");

            //migrationBuilder.DropIndex(
            //    name: "IX_Area_GovernrateId",
            //    table: "Area");
            migrationBuilder.DropIndex(
                name: "IX_Area_GovernrateId1",
                table: "Area");


            migrationBuilder.DropColumn(
                name: "GovernrateId",
                table: "Area");


            migrationBuilder.DropColumn(
                name: "GovernrateId1",
                table: "Area");


            //migrationBuilder.CreateIndex(
            //    name: "IX_Area_FK_Governrate_Id",
            //    table: "Area",
            //    column: "FK_Governrate_Id");

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Area_Governrates_FK_Governrate_Id",
            //        table: "Area",
            //        column: "FK_Governrate_Id",
            //        principalTable: "Governrates",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Area_Governrates_FK_Governrate_Id",
                table: "Area");

            migrationBuilder.DropIndex(
                name: "IX_Area_FK_Governrate_Id",
                table: "Area");

            migrationBuilder.AddColumn<int>(
                name: "GovernrateId",
                table: "Area",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Area_GovernrateId",
                table: "Area",
                column: "GovernrateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_Governrates_GovernrateId",
                table: "Area",
                column: "GovernrateId",
                principalTable: "Governrates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
