﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class PQStatusEntityAndRFQAndPQItemsUpdate16_9_2021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RFQItems_Country_CountryId",
                table: "RFQItems");

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DropColumn(
                name: "Amount",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "HasAmount",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "RFQ");

            migrationBuilder.RenameColumn(
                name: "Notes",
                table: "RFQ",
                newName: "Services");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "RFQ",
                newName: "PQSubject");

            migrationBuilder.AlterColumn<double>(
                name: "Weight",
                table: "RFQItems",
                type: "float",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");

            migrationBuilder.AlterColumn<decimal>(
                name: "ToGoShippingPrice",
                table: "RFQItems",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "RoundTripShippingPrice",
                table: "RFQItems",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "RFQItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<decimal>(
                name: "CommingShippingPrice",
                table: "RFQItems",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AddColumn<int>(
                name: "CurrencyId",
                table: "RFQItems",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "RFQItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasAmount",
                table: "RFQItems",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ItemPrice",
                table: "RFQItems",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Notes",
                table: "RFQItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Quantity",
                table: "RFQItems",
                type: "float",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Total",
                table: "RFQ",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)");

            migrationBuilder.AddColumn<string>(
                name: "LetterSubject",
                table: "RFQ",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LetterText",
                table: "RFQ",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PQCode",
                table: "RFQ",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PQDate",
                table: "RFQ",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "PQValidToDate",
                table: "RFQ",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PriceQuotationStatusId",
                table: "RFQ",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PriceQuotationStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceQuotationStatuses", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Description", "Name" },
                values: new object[] { null, "SendToCustomer" });

            migrationBuilder.CreateIndex(
                name: "IX_RFQItems_CurrencyId",
                table: "RFQItems",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_RFQ_PriceQuotationStatusId",
                table: "RFQ",
                column: "PriceQuotationStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_RFQ_PriceQuotationStatuses_PriceQuotationStatusId",
                table: "RFQ",
                column: "PriceQuotationStatusId",
                principalTable: "PriceQuotationStatuses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RFQItems_Country_CountryId",
                table: "RFQItems",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RFQItems_Currency_CurrencyId",
                table: "RFQItems",
                column: "CurrencyId",
                principalTable: "Currency",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RFQ_PriceQuotationStatuses_PriceQuotationStatusId",
                table: "RFQ");

            migrationBuilder.DropForeignKey(
                name: "FK_RFQItems_Country_CountryId",
                table: "RFQItems");

            migrationBuilder.DropForeignKey(
                name: "FK_RFQItems_Currency_CurrencyId",
                table: "RFQItems");

            migrationBuilder.DropTable(
                name: "PriceQuotationStatuses");

            migrationBuilder.DropIndex(
                name: "IX_RFQItems_CurrencyId",
                table: "RFQItems");

            migrationBuilder.DropIndex(
                name: "IX_RFQ_PriceQuotationStatusId",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "CurrencyId",
                table: "RFQItems");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "RFQItems");

            migrationBuilder.DropColumn(
                name: "HasAmount",
                table: "RFQItems");

            migrationBuilder.DropColumn(
                name: "ItemPrice",
                table: "RFQItems");

            migrationBuilder.DropColumn(
                name: "Notes",
                table: "RFQItems");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "RFQItems");

            migrationBuilder.DropColumn(
                name: "LetterSubject",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "LetterText",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "PQCode",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "PQDate",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "PQValidToDate",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "PriceQuotationStatusId",
                table: "RFQ");

            migrationBuilder.RenameColumn(
                name: "Services",
                table: "RFQ",
                newName: "Notes");

            migrationBuilder.RenameColumn(
                name: "PQSubject",
                table: "RFQ",
                newName: "Description");

            migrationBuilder.AlterColumn<double>(
                name: "Weight",
                table: "RFQItems",
                type: "float",
                nullable: false,
                defaultValue: 0.0,
                oldClrType: typeof(double),
                oldType: "float",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "ToGoShippingPrice",
                table: "RFQItems",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RoundTripShippingPrice",
                table: "RFQItems",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CountryId",
                table: "RFQItems",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CommingShippingPrice",
                table: "RFQItems",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Total",
                table: "RFQ",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "RFQ",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "HasAmount",
                table: "RFQ",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "RFQ",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Quantity",
                table: "RFQ",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "OrderStatus",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Description", "Name" },
                values: new object[] { "", "Registered" });

            migrationBuilder.InsertData(
                table: "OrderStatus",
                columns: new[] { "Id", "Code", "Description", "IsActive", "IsDeleted", "IsSystem", "Name", "Rank" },
                values: new object[,]
                {
                    { 2, "", "", true, false, false, "Assigned", 0 },
                    { 3, "", "", true, false, false, "Driver at Pickup", 0 },
                    { 4, "", "", true, false, false, "Picked-up", 0 },
                    { 5, "", "", true, false, false, "Received", 0 },
                    { 6, "", "", true, false, false, "In-upload", 0 },
                    { 7, "", "", true, false, false, "Uploaded", 0 },
                    { 8, "", "", true, false, false, "In-Sorting", 0 },
                    { 9, "", "", true, false, false, "Sorted", 0 },
                    { 10, "", "", true, false, false, "On-Hold", 0 },
                    { 11, "", "", true, false, false, "Dispatched", 0 },
                    { 12, "", "", true, false, false, "Driver at Delivery", 0 },
                    { 13, "", "", true, false, false, "Delivered", 0 },
                    { 14, "", "", true, false, false, "ClosedWithReturns", 0 },
                    { 15, "", "", true, false, false, "Canceled", 0 },
                    { 16, "", "", true, false, false, "PassedToCorrier", 0 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_RFQItems_Country_CountryId",
                table: "RFQItems",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
