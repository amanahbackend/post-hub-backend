﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class FixSignatureImageInDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_AttachmentFiles_MailItems_SignatureImageId",
            //    table: "AttachmentFiles");

            //migrationBuilder.DropIndex(
            //    name: "IX_AttachmentFiles_SignatureImageId",
            //    table: "AttachmentFiles");

            migrationBuilder.AddColumn<string>(
                name: "SignatureImageURL",
                table: "MailItems",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MailItemSignatureImageId",
                table: "AttachmentFiles",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AttachmentFiles_MailItemSignatureImageId",
                table: "AttachmentFiles",
                column: "MailItemSignatureImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_MailItems_MailItemSignatureImageId",
                table: "AttachmentFiles",
                column: "MailItemSignatureImageId",
                principalTable: "MailItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachmentFiles_MailItems_MailItemSignatureImageId",
                table: "AttachmentFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachmentFiles_MailItemSignatureImageId",
                table: "AttachmentFiles");

            migrationBuilder.DropColumn(
                name: "SignatureImageURL",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "MailItemSignatureImageId",
                table: "AttachmentFiles");

            migrationBuilder.CreateIndex(
                name: "IX_AttachmentFiles_SignatureImageId",
                table: "AttachmentFiles",
                column: "SignatureImageId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachmentFiles_MailItems_SignatureImageId",
                table: "AttachmentFiles",
                column: "SignatureImageId",
                principalTable: "MailItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
