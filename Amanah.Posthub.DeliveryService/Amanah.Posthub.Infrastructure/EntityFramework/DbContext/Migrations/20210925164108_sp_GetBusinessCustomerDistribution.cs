﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class sp_GetBusinessCustomerDistribution : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_MailItems_DefaultAssignedDriverID",
                table: "MailItems",
                column: "DefaultAssignedDriverID");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_Driver_DefaultAssignedDriverID",
                table: "MailItems",
                column: "DefaultAssignedDriverID",
                principalTable: "Driver",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_Driver_DefaultAssignedDriverID",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_DefaultAssignedDriverID",
                table: "MailItems");
        }
    }
}
