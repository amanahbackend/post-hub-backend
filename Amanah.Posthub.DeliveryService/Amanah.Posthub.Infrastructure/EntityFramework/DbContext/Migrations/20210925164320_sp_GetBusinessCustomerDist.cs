﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class sp_GetBusinessCustomerDist : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var createProcSql = @"CREATE  PROCEDURE sp_GetBusinessCustomerDistribution
                 @Month int ,
                 @Year int ,
                 @BusinessCustomerId int,
                 @PageNumber INT=1, 
                 @PageSize   INT=10
                 as
                 begin
                 WITH ctepaging 
                     AS(
                 select  DAY([DeliveredDate]) as DeliveredDay  ,bcb.[Name],bc.[BusinessName], Row_number() OVER(ORDER BY bcb.[Name], DAY([DeliveredDate])) AS rownum 
                 ,count(*) as itemNo from [dbo].[MailItems] m
                 inner join orders o on m.[OrderId]=o.Id
                 inner join [dbo].[BusinessOrders] bo on o.[BusinessOrderId] =bo.Id
                 inner join [dbo].[BusinessCustomers] as bc on bo.[BusinessCustomerId]=bc.Id
                 inner join [dbo].[BusinessCustomerBranchs] bcb on bcb.Id=bc.[HQBranchId]
                 where bc.Id=@BusinessCustomerId and YEAR([DeliveredDate])= @Year and  MONTH([DeliveredDate])= @Month and m.IsDeleted!=1
                 group by  DAY([DeliveredDate]),bcb.[Name],bcb.[Name],bc.[BusinessName]
                 )
                SELECT * 
                FROM   ctepaging 
                WHERE  rownum BETWEEN ( @PageNumber - 1 ) * @PageSize + 1 AND
                @PageNumber * @PageSize 
                 end";
            migrationBuilder.Sql(createProcSql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var dropProcSql = "DROP PROC sp_GetBusinessCustomerDistribution";
            migrationBuilder.Sql(dropProcSql);
        }
    }
}
