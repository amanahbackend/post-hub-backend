﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class PriceListEntities_28_9_2021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PriceListStatuses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceListStatuses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PriceLists",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FromArea = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ToArea = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PriceListStatusId = table.Column<int>(type: "int", nullable: true),
                    ValidToDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    ValidFromDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    FromWeight = table.Column<double>(type: "float", nullable: true),
                    ToWeight = table.Column<double>(type: "float", nullable: true),
                    ServiceTime = table.Column<int>(type: "int", nullable: false),
                    ServiceTimeUnitId = table.Column<int>(type: "int", nullable: false),
                    ServiceTimeUnit = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    ServiceSectorId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceLists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PriceLists_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PriceLists_PriceListStatuses_PriceListStatusId",
                        column: x => x.PriceListStatusId,
                        principalTable: "PriceListStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                        column: x => x.ServiceSectorId,
                        principalTable: "ServiceSectors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PriceLists_PriceListStatusId",
                table: "PriceLists",
                column: "PriceListStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_PriceLists_ServiceSectorId",
                table: "PriceLists",
                column: "ServiceSectorId");

            migrationBuilder.CreateIndex(
                name: "IX_PriceLists_Tenant_Id",
                table: "PriceLists",
                column: "Tenant_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PriceLists");

            migrationBuilder.DropTable(
                name: "PriceListStatuses");
        }
    }
}
