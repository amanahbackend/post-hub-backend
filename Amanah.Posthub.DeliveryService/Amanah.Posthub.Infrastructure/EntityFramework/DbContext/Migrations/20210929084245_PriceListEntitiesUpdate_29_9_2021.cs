﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class PriceListEntitiesUpdate_29_9_2021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "FromArea",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "FromWeight",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "ServiceTime",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "ServiceTimeUnit",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "ServiceTimeUnitId",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "ToArea",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "ToWeight",
                table: "PriceLists");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceSectorId",
                table: "PriceLists",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "PriceListItems",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FromArea = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ToArea = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FromWeight = table.Column<double>(type: "float", nullable: true),
                    ToWeight = table.Column<double>(type: "float", nullable: true),
                    ServiceTime = table.Column<int>(type: "int", nullable: false),
                    ServiceTimeUnitId = table.Column<int>(type: "int", nullable: true),
                    ServiceTimeUnit = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PriceListId = table.Column<int>(type: "int", nullable: false),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceListItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PriceListItems_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PriceListItems_PriceLists_PriceListId",
                        column: x => x.PriceListId,
                        principalTable: "PriceLists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PriceListItems_PriceListId",
                table: "PriceListItems",
                column: "PriceListId");

            migrationBuilder.CreateIndex(
                name: "IX_PriceListItems_Tenant_Id",
                table: "PriceListItems",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                table: "PriceLists",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                table: "PriceLists");

            migrationBuilder.DropTable(
                name: "PriceListItems");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceSectorId",
                table: "PriceLists",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FromArea",
                table: "PriceLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "FromWeight",
                table: "PriceLists",
                type: "float",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "PriceLists",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "ServiceTime",
                table: "PriceLists",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ServiceTimeUnit",
                table: "PriceLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceTimeUnitId",
                table: "PriceLists",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ToArea",
                table: "PriceLists",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "ToWeight",
                table: "PriceLists",
                type: "float",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                table: "PriceLists",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
