﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class ExtraTerms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ExtraTerms",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Term = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ContractId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtraTerms", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExtraTerms_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExtraTerms_Contracts_ContractId",
                        column: x => x.ContractId,
                        principalTable: "Contracts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExtraTerms_ContractId",
                table: "ExtraTerms",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "IX_ExtraTerms_Tenant_Id",
                table: "ExtraTerms",
                column: "Tenant_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExtraTerms");
        }
    }
}
