﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddBranchManager : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BranchManagerId",
                table: "Branch",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Branch_BranchManagerId",
                table: "Branch",
                column: "BranchManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Branch_AspNetUsers_BranchManagerId",
                table: "Branch",
                column: "BranchManagerId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Branch_AspNetUsers_BranchManagerId",
                table: "Branch");

            migrationBuilder.DropIndex(
                name: "IX_Branch_BranchManagerId",
                table: "Branch");

            migrationBuilder.DropColumn(
                name: "BranchManagerId",
                table: "Branch");
        }
    }
}
