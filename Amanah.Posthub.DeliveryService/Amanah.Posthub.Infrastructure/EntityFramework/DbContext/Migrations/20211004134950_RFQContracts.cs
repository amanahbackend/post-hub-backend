﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class RFQContracts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RFQId",
                table: "Contracts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contracts_RFQId",
                table: "Contracts",
                column: "RFQId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contracts_RFQ_RFQId",
                table: "Contracts",
                column: "RFQId",
                principalTable: "RFQ",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contracts_RFQ_RFQId",
                table: "Contracts");

            migrationBuilder.DropIndex(
                name: "IX_Contracts_RFQId",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "RFQId",
                table: "Contracts");
        }
    }
}
