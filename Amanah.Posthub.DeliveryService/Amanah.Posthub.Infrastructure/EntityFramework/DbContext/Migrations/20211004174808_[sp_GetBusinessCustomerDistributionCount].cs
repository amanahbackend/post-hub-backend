﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class sp_GetBusinessCustomerDistributionCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var createProcSql = @"create   PROCEDURE [dbo].[sp_GetBusinessCustomerDistributionCount]
                                 @FromDate Date ,
                                 @ToDate Date ,
                                 @BusinessCustomerId int
                               
                                 as
                                 begin
                              
                                 select  DAY([DeliveredDate]) as DeliveredDay  ,bcb.[Name] as BranchName,bc.[BusinessName], Row_number() OVER(ORDER BY bcb.[Name], DAY([DeliveredDate])) AS rownum 
                                 ,count(*) as ItemsNo from [dbo].[MailItems] m
                                 inner join orders o on m.[OrderId]=o.Id
                                 inner join [dbo].[BusinessOrders] bo on o.[BusinessOrderId] =bo.Id
                                 inner join [dbo].[BusinessCustomers] as bc on bo.[BusinessCustomerId]=bc.Id
                                 inner join [dbo].[BusinessCustomerBranchs] bcb on bcb.Id=bc.[HQBranchId]
                                 where bc.Id=@BusinessCustomerId and CONVERT(date, [DeliveredDate])>=CONVERT(date, @FromDate) and  CONVERT(date, [DeliveredDate])<= CONVERT(date,@ToDate) and m.IsDeleted!=1 and m.statusId=13
                                 group by  DAY([DeliveredDate]),bcb.[Name],bcb.[Name],bc.[BusinessName]
                                 end ";
            migrationBuilder.Sql(createProcSql);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var dropProcSql = "DROP PROC sp_GetBusinessCustomerDistributionCount";
            migrationBuilder.Sql(dropProcSql);
        }
    }
}
