﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateBlocks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blocks_Area_AreaId1",
                table: "Blocks");

            migrationBuilder.DropIndex(
                name: "IX_Blocks_AreaId1",
                table: "Blocks");

            migrationBuilder.DropColumn(
                name: "AreaId1",
                table: "Blocks");

            migrationBuilder.AlterColumn<int>(
                name: "AreaId",
                table: "Blocks",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Blocks_AreaId",
                table: "Blocks",
                column: "AreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Blocks_Area_AreaId",
                table: "Blocks",
                column: "AreaId",
                principalTable: "Area",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blocks_Area_AreaId",
                table: "Blocks");

            migrationBuilder.DropIndex(
                name: "IX_Blocks_AreaId",
                table: "Blocks");

            migrationBuilder.AlterColumn<string>(
                name: "AreaId",
                table: "Blocks",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AreaId1",
                table: "Blocks",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Blocks_AreaId1",
                table: "Blocks",
                column: "AreaId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Blocks_Area_AreaId1",
                table: "Blocks",
                column: "AreaId1",
                principalTable: "Area",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
