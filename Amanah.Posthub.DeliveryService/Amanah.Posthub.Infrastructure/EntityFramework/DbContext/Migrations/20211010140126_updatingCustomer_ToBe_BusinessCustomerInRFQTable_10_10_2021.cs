﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class updatingCustomer_ToBe_BusinessCustomerInRFQTable_10_10_2021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RFQ_Customers_CustomerId",
                table: "RFQ");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "RFQ",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "BusinessCustomerId",
                table: "RFQ",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RFQ_BusinessCustomerId",
                table: "RFQ",
                column: "BusinessCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_RFQ_BusinessCustomers_BusinessCustomerId",
                table: "RFQ",
                column: "BusinessCustomerId",
                principalTable: "BusinessCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RFQ_Customers_CustomerId",
                table: "RFQ",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RFQ_BusinessCustomers_BusinessCustomerId",
                table: "RFQ");

            migrationBuilder.DropForeignKey(
                name: "FK_RFQ_Customers_CustomerId",
                table: "RFQ");

            migrationBuilder.DropIndex(
                name: "IX_RFQ_BusinessCustomerId",
                table: "RFQ");

            migrationBuilder.DropColumn(
                name: "BusinessCustomerId",
                table: "RFQ");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "RFQ",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_RFQ_Customers_CustomerId",
                table: "RFQ",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
