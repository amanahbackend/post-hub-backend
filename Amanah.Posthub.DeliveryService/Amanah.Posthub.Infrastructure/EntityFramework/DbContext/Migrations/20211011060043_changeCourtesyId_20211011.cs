﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class changeCourtesyId_20211011 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessConactAccounts_Courtesys_CourtesyId",
                table: "BusinessConactAccounts");

            migrationBuilder.AlterColumn<int>(
                name: "CourtesyId",
                table: "BusinessConactAccounts",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessConactAccounts_Courtesys_CourtesyId",
                table: "BusinessConactAccounts",
                column: "CourtesyId",
                principalTable: "Courtesys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessConactAccounts_Courtesys_CourtesyId",
                table: "BusinessConactAccounts");

            migrationBuilder.AlterColumn<int>(
                name: "CourtesyId",
                table: "BusinessConactAccounts",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessConactAccounts_Courtesys_CourtesyId",
                table: "BusinessConactAccounts",
                column: "CourtesyId",
                principalTable: "Courtesys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
