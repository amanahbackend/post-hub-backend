﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class UpdateMailItemsStatusLogTable_14_10_2021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItemsStatusLogs_MailItemStatuss_NewStatusId",
                table: "MailItemsStatusLogs");

            migrationBuilder.DropForeignKey(
                name: "FK_MailItemsStatusLogs_MailItemStatuss_OldStatusId",
                table: "MailItemsStatusLogs");

            //migrationBuilder.DropIndex(
            //    name: "IX_MailItemsStatusLogs_NewStatusId",
            //    table: "MailItemsStatusLogs");

            //migrationBuilder.DropIndex(
            //    name: "IX_MailItemsStatusLogs_OldStatusId",
            //    table: "MailItemsStatusLogs");

            migrationBuilder.DropColumn(
                name: "NewStatusId",
                table: "MailItemsStatusLogs");

            migrationBuilder.RenameColumn(
                name: "OldStatusId",
                table: "MailItemsStatusLogs",
                newName: "MailItemStatusId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_MailItemsStatusLogs_MailItemStatusId",
            //    table: "MailItemsStatusLogs",
            //    column: "MailItemStatusId",
            //    unique: true,
            //    filter: "[MailItemStatusId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItemsStatusLogs_MailItemStatuss_MailItemStatusId",
                table: "MailItemsStatusLogs",
                column: "MailItemStatusId",
                principalTable: "MailItemStatuss",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItemsStatusLogs_MailItemStatuss_MailItemStatusId",
                table: "MailItemsStatusLogs");

            //migrationBuilder.DropIndex(
            //    name: "IX_MailItemsStatusLogs_MailItemStatusId",
            //    table: "MailItemsStatusLogs");

            migrationBuilder.RenameColumn(
                name: "MailItemStatusId",
                table: "MailItemsStatusLogs",
                newName: "OldStatusId");

            migrationBuilder.AddColumn<int>(
                name: "NewStatusId",
                table: "MailItemsStatusLogs",
                type: "int",
                nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_MailItemsStatusLogs_NewStatusId",
            //    table: "MailItemsStatusLogs",
            //    column: "NewStatusId",
            //    unique: true,
            //    filter: "[NewStatusId] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_MailItemsStatusLogs_OldStatusId",
            //    table: "MailItemsStatusLogs",
            //    column: "OldStatusId",
            //    unique: true,
            //    filter: "[OldStatusId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItemsStatusLogs_MailItemStatuss_NewStatusId",
                table: "MailItemsStatusLogs",
                column: "NewStatusId",
                principalTable: "MailItemStatuss",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItemsStatusLogs_MailItemStatuss_OldStatusId",
                table: "MailItemsStatusLogs",
                column: "OldStatusId",
                principalTable: "MailItemStatuss",
                principalColumn: "Id");
        }
    }
}
