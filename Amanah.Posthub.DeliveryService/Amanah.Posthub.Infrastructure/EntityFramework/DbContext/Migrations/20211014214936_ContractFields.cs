﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class ContractFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ItemsSupplyAheadOf",
                table: "Contracts",
                type: "int",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "ItemsSupplyAheadOfValue",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            //migrationBuilder.AddColumn<int>(
            //    name: "PaymentNotLetterThanPeriodType",
            //    table: "Contracts",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "PaymentNotLetterThanPeriodValue",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentTermsMessageNo",
                table: "Contracts",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ServiceDistributionPeriod",
                table: "Contracts",
                type: "int",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "ServiceDistributionPeriodValue",
                table: "Contracts",
                type: "decimal(18,2)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemsSupplyAheadOf",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ItemsSupplyAheadOfValue",
                table: "Contracts");

            //migrationBuilder.DropColumn(
            //    name: "PaymentNotLetterThanPeriodType",
            //    table: "Contracts");

            migrationBuilder.DropColumn(
                name: "PaymentNotLetterThanPeriodValue",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "PaymentTermsMessageNo",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ServiceDistributionPeriod",
                table: "Contracts");

            migrationBuilder.DropColumn(
                name: "ServiceDistributionPeriodValue",
                table: "Contracts");
        }
    }
}
