﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class OrderInternationalelivery : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OrderInternationalDeliveryId",
                table: "Orders",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Quantity",
                table: "MailItems",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                table: "MailItems",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BusinessTypeId",
                table: "BusinessOrders",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HQBranchId",
                table: "BusinessOrders",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "OrderInternationalDelivery",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AirWayBillNumber = table.Column<int>(type: "int", nullable: true),
                    CourierId = table.Column<int>(type: "int", nullable: true),
                    CourierTrackingNumber = table.Column<int>(type: "int", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastModificationTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModifiedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    DeletionTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedBy = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderInternationalDelivery", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderInternationalDelivery_Couriers_CourierId",
                        column: x => x.CourierId,
                        principalTable: "Couriers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Orders_OrderInternationalDeliveryId",
            //    table: "Orders",
            //    column: "OrderInternationalDeliveryId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_BusinessOrders_BusinessTypeId",
            //    table: "BusinessOrders",
            //    column: "BusinessTypeId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_BusinessOrders_HQBranchId",
            //    table: "BusinessOrders",
            //    column: "HQBranchId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_OrderInternationalDelivery_CourierId",
            //    table: "OrderInternationalDelivery",
            //    column: "CourierId");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessOrders_BusinessCustomerBranchs_HQBranchId",
                table: "BusinessOrders",
                column: "HQBranchId",
                principalTable: "BusinessCustomerBranchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessOrders_BusinessTypes_BusinessTypeId",
                table: "BusinessOrders",
                column: "BusinessTypeId",
                principalTable: "BusinessTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_OrderInternationalDelivery_OrderInternationalDeliveryId",
                table: "Orders",
                column: "OrderInternationalDeliveryId",
                principalTable: "OrderInternationalDelivery",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessOrders_BusinessCustomerBranchs_HQBranchId",
                table: "BusinessOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_BusinessOrders_BusinessTypes_BusinessTypeId",
                table: "BusinessOrders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_OrderInternationalDelivery_OrderInternationalDeliveryId",
                table: "Orders");

            migrationBuilder.DropTable(
                name: "OrderInternationalDelivery");

            //migrationBuilder.DropIndex(
            //    name: "IX_Orders_OrderInternationalDeliveryId",
            //    table: "Orders");

            //migrationBuilder.DropIndex(
            //    name: "IX_BusinessOrders_BusinessTypeId",
            //    table: "BusinessOrders");

            //migrationBuilder.DropIndex(
            //    name: "IX_BusinessOrders_HQBranchId",
            //    table: "BusinessOrders");

            migrationBuilder.DropColumn(
                name: "OrderInternationalDeliveryId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "BusinessTypeId",
                table: "BusinessOrders");

            migrationBuilder.DropColumn(
                name: "HQBranchId",
                table: "BusinessOrders");
        }
    }
}
