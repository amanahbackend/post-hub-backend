﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class OrderInternationaleliveryUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CourierTrackingNumber",
                table: "OrderInternationalDelivery",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            //migrationBuilder.AddColumn<int>(
            //    name: "MailItemStatusId1",
            //    table: "MailItemsStatusLogs",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_MailItemsStatusLogs_MailItemStatusId1",
            //    table: "MailItemsStatusLogs",
            //    column: "MailItemStatusId1");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_MailItemsStatusLogs_MailItemStatuss_MailItemStatusId1",
            //    table: "MailItemsStatusLogs",
            //    column: "MailItemStatusId1",
            //    principalTable: "MailItemStatuss",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_MailItemsStatusLogs_MailItemStatuss_MailItemStatusId1",
            //    table: "MailItemsStatusLogs");

            //migrationBuilder.DropIndex(
            //    name: "IX_MailItemsStatusLogs_MailItemStatusId1",
            //    table: "MailItemsStatusLogs");

            //migrationBuilder.DropColumn(
            //    name: "MailItemStatusId1",
            //    table: "MailItemsStatusLogs");

            migrationBuilder.AlterColumn<int>(
                name: "CourierTrackingNumber",
                table: "OrderInternationalDelivery",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }
    }
}
