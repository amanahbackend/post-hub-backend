﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class updateMailItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "MailItems",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_CountryId",
                table: "MailItems",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_Country_CountryId",
                table: "MailItems",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_Country_CountryId",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_CountryId",
                table: "MailItems");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "MailItems");
        }
    }
}
