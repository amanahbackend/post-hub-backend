﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddBranchToBusinessOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BusinessCustomerBranchId",
                table: "BusinessOrders",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_BusinessCustomerBranchId",
                table: "BusinessOrders",
                column: "BusinessCustomerBranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessOrders_BusinessCustomerBranchs_BusinessCustomerBranchId",
                table: "BusinessOrders",
                column: "BusinessCustomerBranchId",
                principalTable: "BusinessCustomerBranchs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessOrders_BusinessCustomerBranchs_BusinessCustomerBranchId",
                table: "BusinessOrders");

            migrationBuilder.DropIndex(
                name: "IX_BusinessOrders_BusinessCustomerBranchId",
                table: "BusinessOrders");

            migrationBuilder.DropColumn(
                name: "BusinessCustomerBranchId",
                table: "BusinessOrders");
        }
    }
}
