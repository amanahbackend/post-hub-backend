﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class TrackOrderStatus_24_10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrdersStatusLogs_OrderStatus_NewStatusId",
                table: "OrdersStatusLogs");

            migrationBuilder.DropForeignKey(
                name: "FK_OrdersStatusLogs_OrderStatus_OldStatusId",
                table: "OrdersStatusLogs");

            migrationBuilder.DropIndex(
                name: "IX_OrdersStatusLogs_NewStatusId",
                table: "OrdersStatusLogs");

            migrationBuilder.DropIndex(
                name: "IX_OrdersStatusLogs_OldStatusId",
                table: "OrdersStatusLogs");

            migrationBuilder.DropColumn(
                name: "NewStatusId",
                table: "OrdersStatusLogs");

            migrationBuilder.RenameColumn(
                name: "OldStatusId",
                table: "OrdersStatusLogs",
                newName: "OrderStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_OrdersStatusLogs_OrderStatusId",
                table: "OrdersStatusLogs",
                column: "OrderStatusId",
                unique: false,
                filter: "[OrderStatusId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersStatusLogs_OrderStatus_OrderStatusId",
                table: "OrdersStatusLogs",
                column: "OrderStatusId",
                principalTable: "OrderStatus",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrdersStatusLogs_OrderStatus_OrderStatusId",
                table: "OrdersStatusLogs");

            migrationBuilder.DropIndex(
                name: "IX_OrdersStatusLogs_OrderStatusId",
                table: "OrdersStatusLogs");

            migrationBuilder.RenameColumn(
                name: "OrderStatusId",
                table: "OrdersStatusLogs",
                newName: "OldStatusId");

            migrationBuilder.AddColumn<int>(
                name: "NewStatusId",
                table: "OrdersStatusLogs",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrdersStatusLogs_NewStatusId",
                table: "OrdersStatusLogs",
                column: "NewStatusId",
                unique: true,
                filter: "[NewStatusId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_OrdersStatusLogs_OldStatusId",
                table: "OrdersStatusLogs",
                column: "OldStatusId",
                unique: true,
                filter: "[OldStatusId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersStatusLogs_OrderStatus_NewStatusId",
                table: "OrdersStatusLogs",
                column: "NewStatusId",
                principalTable: "OrderStatus",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OrdersStatusLogs_OrderStatus_OldStatusId",
                table: "OrdersStatusLogs",
                column: "OldStatusId",
                principalTable: "OrderStatus",
                principalColumn: "Id");
        }
    }
}
