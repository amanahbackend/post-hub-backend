﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class BusinessOrderUpdateEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_BusinessOrders_BusinessCustomerBranchs_HQBranchId",
            //    table: "BusinessOrders");

            //migrationBuilder.DropIndex(
            //    name: "IX_BusinessOrders_HQBranchId",
            //    table: "BusinessOrders");

            //migrationBuilder.DropColumn(
            //    name: "HQBranchId",
            //    table: "BusinessOrders");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.AddColumn<int>(
            //    name: "HQBranchId",
            //    table: "BusinessOrders",
            //    type: "int",
            //    nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_BusinessOrders_HQBranchId",
            //    table: "BusinessOrders",
            //    column: "HQBranchId");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_BusinessOrders_BusinessCustomerBranchs_HQBranchId",
            //    table: "BusinessOrders",
            //    column: "HQBranchId",
            //    principalTable: "BusinessCustomerBranchs",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
