﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class BusinessTypesSeedsForIndividual : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.InsertData(
             table: "BusinessTypes",
             columns: new[] { "Name_en", "Name_ar", "IsDeleted", "CreatedDate", "UpdatedDate", "DeletedDate", "Tenant_Id" },
             values: new object[] { "Individual", "فردى", false, DateTime.Now, DateTime.Now, DateTime.Now, "AV59A46B-72BF-4849-82D0-43851B574590" });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
