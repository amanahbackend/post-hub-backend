﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class Addlocaliztionforsomelookups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
        


            migrationBuilder.RenameColumn(
                name: "Name",
                table: "MailItemTypes",
                newName: "Name_en");

            migrationBuilder.RenameColumn(
             name: "Name",
             table: "WeightUOMs",
             newName: "Name_en");


            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "WeightUOMs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "WeightUOMs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "WeightUOMs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "WeightUOMs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Name_ar",
                table: "WeightUOMs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "WeightUOMs",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "WeightUOMs",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "WeightUOMs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy_Id",
                table: "MailItemTypes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "MailItemTypes",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy_Id",
                table: "MailItemTypes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedDate",
                table: "MailItemTypes",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Name_ar",
                table: "MailItemTypes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdatedBy_Id",
                table: "MailItemTypes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tenant_Id",
                table: "MailItemTypes",
                type: "varchar(36)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedDate",
                table: "MailItemTypes",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 903, DateTimeKind.Utc).AddTicks(9791), "Root", null });

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1224), "Letter", null });

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1274), "Flat", null });

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1303), "Post Card", null });

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1330), "Parcel", null });

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1363), "Commodity", null });

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 897, DateTimeKind.Utc).AddTicks(7840), "Kilogram", null });

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 898, DateTimeKind.Utc).AddTicks(671), "Gram", null });

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedDate", "Name_en", "UpdatedBy_Id" },
                values: new object[] { new DateTime(2021, 11, 22, 14, 33, 21, 898, DateTimeKind.Utc).AddTicks(764), "Pound", null });

            migrationBuilder.CreateIndex(
                name: "IX_WeightUOMs_Tenant_Id",
                table: "WeightUOMs",
                column: "Tenant_Id");

            migrationBuilder.CreateIndex(
                name: "IX_MailItemTypes_Tenant_Id",
                table: "MailItemTypes",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItemTypes_AspNetUsers_Tenant_Id",
                table: "MailItemTypes",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WeightUOMs_AspNetUsers_Tenant_Id",
                table: "WeightUOMs",
                column: "Tenant_Id",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItemTypes_AspNetUsers_Tenant_Id",
                table: "MailItemTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_WeightUOMs_AspNetUsers_Tenant_Id",
                table: "WeightUOMs");

            migrationBuilder.DropIndex(
                name: "IX_WeightUOMs_Tenant_Id",
                table: "WeightUOMs");

            migrationBuilder.DropIndex(
                name: "IX_MailItemTypes_Tenant_Id",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "Name_ar",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "WeightUOMs");

            migrationBuilder.DropColumn(
                name: "CreatedBy_Id",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "DeletedBy_Id",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "DeletedDate",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "Name_ar",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "UpdatedBy_Id",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "Tenant_Id",
                table: "MailItemTypes");

            migrationBuilder.DropColumn(
                name: "UpdatedDate",
                table: "MailItemTypes");

            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "WeightUOMs",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Name_en",
                table: "MailItemTypes",
                newName: "Name");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "Root");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "Letter");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "Name",
                value: "Flat");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "Name",
                value: "Post Card");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "Name",
                value: "Parcel");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "Name",
                value: "Commodity");

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "Kilogram");

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "Gram");

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Name",
                value: "Pound");
        }
    }
}
