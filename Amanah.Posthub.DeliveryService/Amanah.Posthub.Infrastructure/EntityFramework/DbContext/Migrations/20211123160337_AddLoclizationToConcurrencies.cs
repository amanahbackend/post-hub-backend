﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddLoclizationToConcurrencies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Currency",
                newName: "Code_ar");

            migrationBuilder.RenameColumn(
                name: "Code",
                table: "Currency",
                newName: "Code_en");

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Code_ar", "Code_en" },
                values: new object[] { "دولار امريكى", "USD" });

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Code_ar", "Code_en" },
                values: new object[] { "جنيه استرلينى", "GBP" });

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Code_ar", "Code_en" },
                values: new object[] { "دينار كويتى", "KWD" });

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Code_ar", "Code_en" },
                values: new object[] { "ريا سعودى", "SAR" });

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Code_ar", "Code_en" },
                values: new object[] { "يورو", "EUR" });

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 82, DateTimeKind.Utc).AddTicks(121));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 82, DateTimeKind.Utc).AddTicks(1344));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 82, DateTimeKind.Utc).AddTicks(1388));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 82, DateTimeKind.Utc).AddTicks(1505));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 82, DateTimeKind.Utc).AddTicks(1536));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 82, DateTimeKind.Utc).AddTicks(1563));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 75, DateTimeKind.Utc).AddTicks(980));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 75, DateTimeKind.Utc).AddTicks(3489));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 23, 16, 3, 33, 75, DateTimeKind.Utc).AddTicks(3544));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Code_en",
                table: "Currency",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Code_ar",
                table: "Currency",
                newName: "Code");

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Code", "Name" },
                values: new object[] { "USD", "" });

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Code", "Name" },
                values: new object[] { "GBP", "" });

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Code", "Name" },
                values: new object[] { "KWD", "" });

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Code", "Name" },
                values: new object[] { "SAR", "" });

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Code", "Name" },
                values: new object[] { "EUR", "" });

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 903, DateTimeKind.Utc).AddTicks(9791));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1224));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1274));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1303));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1330));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 904, DateTimeKind.Utc).AddTicks(1363));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 897, DateTimeKind.Utc).AddTicks(7840));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 898, DateTimeKind.Utc).AddTicks(671));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 11, 22, 14, 33, 21, 898, DateTimeKind.Utc).AddTicks(764));
        }
    }
}
