﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class removingServiceSectorIdFromPriceList_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                table: "PriceLists");

            migrationBuilder.DropIndex(
                name: "IX_PriceLists_ServiceSectorId",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "ServiceSectorId",
                table: "PriceLists");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(3055));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4243));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4277));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4297));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4313));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4332));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 19, 996, DateTimeKind.Utc).AddTicks(1335));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 19, 996, DateTimeKind.Utc).AddTicks(3393));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 19, 996, DateTimeKind.Utc).AddTicks(3430));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServiceSectorId",
                table: "PriceLists",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 119, DateTimeKind.Utc).AddTicks(2956));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 119, DateTimeKind.Utc).AddTicks(5724));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 119, DateTimeKind.Utc).AddTicks(5813));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 119, DateTimeKind.Utc).AddTicks(5847));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 119, DateTimeKind.Utc).AddTicks(5878));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 119, DateTimeKind.Utc).AddTicks(5918));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 113, DateTimeKind.Utc).AddTicks(5567));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 113, DateTimeKind.Utc).AddTicks(8800));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 13, 16, 11, 59, 113, DateTimeKind.Utc).AddTicks(8853));

            migrationBuilder.CreateIndex(
                name: "IX_PriceLists_ServiceSectorId",
                table: "PriceLists",
                column: "ServiceSectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                table: "PriceLists",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
