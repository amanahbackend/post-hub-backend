﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class addingNewFKServiceSectorToPriceList_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServiceSectorId",
                table: "PriceLists",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(5897));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7101));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7131));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7148));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7164));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7181));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(4397));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(6493));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(6526));

            migrationBuilder.CreateIndex(
                name: "IX_PriceLists_ServiceSectorId",
                table: "PriceLists",
                column: "ServiceSectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                table: "PriceLists",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PriceLists_ServiceSectors_ServiceSectorId",
                table: "PriceLists");

            migrationBuilder.DropIndex(
                name: "IX_PriceLists_ServiceSectorId",
                table: "PriceLists");

            migrationBuilder.DropColumn(
                name: "ServiceSectorId",
                table: "PriceLists");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(3055));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4243));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4277));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4297));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4313));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 20, 0, DateTimeKind.Utc).AddTicks(4332));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 19, 996, DateTimeKind.Utc).AddTicks(1335));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 19, 996, DateTimeKind.Utc).AddTicks(3393));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 11, 19, 996, DateTimeKind.Utc).AddTicks(3430));
        }
    }
}
