﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class addingWorkOrderIdIntoMailItemStatusLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WorkOrderId",
                table: "MailItemsStatusLogs",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 172, DateTimeKind.Utc).AddTicks(4948));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 172, DateTimeKind.Utc).AddTicks(6164));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 172, DateTimeKind.Utc).AddTicks(6195));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 172, DateTimeKind.Utc).AddTicks(6212));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 172, DateTimeKind.Utc).AddTicks(6226));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 172, DateTimeKind.Utc).AddTicks(6244));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 167, DateTimeKind.Utc).AddTicks(8017));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 168, DateTimeKind.Utc).AddTicks(204));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 29, 17, 36, 22, 168, DateTimeKind.Utc).AddTicks(300));

            migrationBuilder.CreateIndex(
                name: "IX_MailItemsStatusLogs_WorkOrderId",
                table: "MailItemsStatusLogs",
                column: "WorkOrderId");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItemsStatusLogs_Workorders_WorkOrderId",
                table: "MailItemsStatusLogs",
                column: "WorkOrderId",
                principalTable: "Workorders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItemsStatusLogs_Workorders_WorkOrderId",
                table: "MailItemsStatusLogs");

            migrationBuilder.DropIndex(
                name: "IX_MailItemsStatusLogs_WorkOrderId",
                table: "MailItemsStatusLogs");

            migrationBuilder.DropColumn(
                name: "WorkOrderId",
                table: "MailItemsStatusLogs");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(5897));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7101));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7131));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7148));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7164));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7181));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(4397));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(6493));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(6526));
        }
    }
}
