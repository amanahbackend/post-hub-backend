﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class changeWeightDataType_20211229 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Weight",
                table: "MailItems",
                type: "decimal(18,2)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 306, DateTimeKind.Utc).AddTicks(6065));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 306, DateTimeKind.Utc).AddTicks(7153));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 306, DateTimeKind.Utc).AddTicks(7183));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 306, DateTimeKind.Utc).AddTicks(7198));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 306, DateTimeKind.Utc).AddTicks(7212));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 306, DateTimeKind.Utc).AddTicks(7228));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 302, DateTimeKind.Utc).AddTicks(5041));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 302, DateTimeKind.Utc).AddTicks(7227));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 30, 7, 6, 49, 302, DateTimeKind.Utc).AddTicks(7263));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Weight",
                table: "MailItems",
                type: "int",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,2)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(5897));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7101));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7131));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7148));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7164));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 758, DateTimeKind.Utc).AddTicks(7181));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(4397));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(6493));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2021, 12, 26, 18, 17, 3, 754, DateTimeKind.Utc).AddTicks(6526));
        }
    }
}
