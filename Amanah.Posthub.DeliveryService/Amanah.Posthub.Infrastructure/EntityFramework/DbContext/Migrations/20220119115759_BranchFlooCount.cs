﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class BranchFlooCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "DriverBranchs");
            migrationBuilder.AddColumn<int>(
              name: "FloorsCount",
              table: "Branch",
              type: "int",
              nullable: false,
              defaultValue: 0);

            migrationBuilder.CreateTable(
              name: "DriverFloors",
              columns: table => new
              {
                  Id = table.Column<int>(type: "int", nullable: false)
                      .Annotation("SqlServer:Identity", "1, 1"),
                  Floor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                  BranchId = table.Column<int>(type: "int", nullable: false),
                  DriverId = table.Column<int>(type: "int", nullable: false),
                  CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                  UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                  DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                  IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                  CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                  UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                  DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                  Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
              },
              constraints: table =>
              {
                  table.PrimaryKey("PK_DriverFloors", x => x.Id);
                  table.ForeignKey(
                      name: "FK_DriverFloors_AspNetUsers_Tenant_Id",
                      column: x => x.Tenant_Id,
                      principalTable: "AspNetUsers",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Restrict);
                  table.ForeignKey(
                      name: "FK_DriverFloors_Driver_DriverId",
                      column: x => x.DriverId,
                      principalTable: "Driver",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
              });


            migrationBuilder.AddColumn<int>(
                name: "BranchId",
                table: "Driver",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "DriverExternalBranchs",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DriverId = table.Column<int>(type: "int", nullable: false),
                    BranchId = table.Column<int>(type: "int", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverExternalBranchs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverExternalBranchs_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverExternalBranchs_Branch_BranchId",
                        column: x => x.BranchId,
                        principalTable: "Branch",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DriverExternalBranchs_Driver_DriverId",
                        column: x => x.DriverId,
                        principalTable: "Driver",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });





            migrationBuilder.CreateIndex(
                name: "IX_Driver_BranchId",
                table: "Driver",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverExternalBranchs_BranchId",
                table: "DriverExternalBranchs",
                column: "BranchId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverExternalBranchs_DriverId",
                table: "DriverExternalBranchs",
                column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverExternalBranchs_Tenant_Id",
                table: "DriverExternalBranchs",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_Branch_BranchId",
                table: "Driver",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.CreateIndex(
            name: "IX_DriverFloors_DriverId",
            table: "DriverFloors",
            column: "DriverId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverFloors_Tenant_Id",
                table: "DriverFloors",
                column: "Tenant_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
               name: "FloorsCount",
               table: "Branch");

            migrationBuilder.DropTable(
              name: "DriverFloors");

            migrationBuilder.DropForeignKey(
                name: "FK_Driver_Branch_BranchId",
                table: "Driver");

            migrationBuilder.DropTable(
                name: "DriverExternalBranchs");

            migrationBuilder.DropIndex(
                name: "IX_Driver_BranchId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "BranchId",
                table: "Driver");

          
        }
    }
}
