﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class addStatementCode_20220124 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Driver_Branch_BranchId1",
            //    table: "Driver");

            //migrationBuilder.RenameColumn(
            //    name: "BranchId1",
            //    table: "Driver",
            //    newName: "BranchId");

            //migrationBuilder.RenameIndex(
            //    name: "IX_Driver_BranchId1",
            //    table: "Driver",
            //    newName: "IX_Driver_BranchId");

            migrationBuilder.AddColumn<string>(
                name: "StatementCode",
                table: "Orders",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(7198));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8472));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8509));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8530));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8548));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8567));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 602, DateTimeKind.Utc).AddTicks(5118));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 602, DateTimeKind.Utc).AddTicks(7240));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 602, DateTimeKind.Utc).AddTicks(7283));

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_Branch_BranchId",
                table: "Driver",
                column: "BranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Driver_Branch_BranchId",
            //    table: "Driver");

            migrationBuilder.DropColumn(
                name: "StatementCode",
                table: "Orders");

            //migrationBuilder.RenameColumn(
            //    name: "BranchId",
            //    table: "Driver",
            //    newName: "BranchId1");

            //migrationBuilder.RenameIndex(
            //    name: "IX_Driver_BranchId",
            //    table: "Driver",
            //    newName: "IX_Driver_BranchId1");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 831, DateTimeKind.Utc).AddTicks(6750));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 831, DateTimeKind.Utc).AddTicks(7994));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 831, DateTimeKind.Utc).AddTicks(8030));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 831, DateTimeKind.Utc).AddTicks(8046));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 831, DateTimeKind.Utc).AddTicks(8061));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 831, DateTimeKind.Utc).AddTicks(8079));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 827, DateTimeKind.Utc).AddTicks(3867));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 827, DateTimeKind.Utc).AddTicks(5888));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 1, 14, 14, 827, DateTimeKind.Utc).AddTicks(5923));

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Driver_Branch_BranchId1",
            //    table: "Driver",
            //    column: "BranchId1",
            //    principalTable: "Branch",
            //    principalColumn: "Id",
            //    onDelete: ReferentialAction.Cascade);
        }
    }
}
