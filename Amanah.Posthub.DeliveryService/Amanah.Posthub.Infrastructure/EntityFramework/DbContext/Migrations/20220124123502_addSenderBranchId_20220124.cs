﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class addSenderBranchId_20220124 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(7789));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9084));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9123));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9143));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9161));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9189));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 423, DateTimeKind.Utc).AddTicks(2321));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 423, DateTimeKind.Utc).AddTicks(4625));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 423, DateTimeKind.Utc).AddTicks(4668));

            migrationBuilder.CreateIndex(
                name: "IX_Orders_SenderBranchId",
                table: "Orders",
                column: "SenderBranchId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Branch_SenderBranchId",
                table: "Orders",
                column: "SenderBranchId",
                principalTable: "Branch",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Branch_SenderBranchId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_SenderBranchId",
                table: "Orders");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(7198));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8472));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8509));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8530));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8548));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 606, DateTimeKind.Utc).AddTicks(8567));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 602, DateTimeKind.Utc).AddTicks(5118));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 602, DateTimeKind.Utc).AddTicks(7240));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 21, 10, 602, DateTimeKind.Utc).AddTicks(7283));
        }
    }
}
