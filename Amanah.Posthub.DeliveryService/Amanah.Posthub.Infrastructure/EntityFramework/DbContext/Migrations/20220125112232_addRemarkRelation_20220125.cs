﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class addRemarkRelation_20220125 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(2401));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3321));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3359));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3380));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3399));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3420));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 494, DateTimeKind.Utc).AddTicks(886));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 494, DateTimeKind.Utc).AddTicks(2849));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 494, DateTimeKind.Utc).AddTicks(2896));

            migrationBuilder.CreateIndex(
                name: "IX_MailItems_RemarkId",
                table: "MailItems",
                column: "RemarkId");

            migrationBuilder.AddForeignKey(
                name: "FK_MailItems_Remarks_RemarkId",
                table: "MailItems",
                column: "RemarkId",
                principalTable: "Remarks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MailItems_Remarks_RemarkId",
                table: "MailItems");

            migrationBuilder.DropIndex(
                name: "IX_MailItems_RemarkId",
                table: "MailItems");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(7789));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9084));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9123));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9143));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9161));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 427, DateTimeKind.Utc).AddTicks(9189));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 423, DateTimeKind.Utc).AddTicks(2321));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 423, DateTimeKind.Utc).AddTicks(4625));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 24, 12, 34, 58, 423, DateTimeKind.Utc).AddTicks(4668));
        }
    }
}
