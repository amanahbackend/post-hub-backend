﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class updateManagerRelation_20220125 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manager_ServiceSectors_ServiceSectorId",
                table: "Manager");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceSectorId",
                table: "Manager",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 436, DateTimeKind.Utc).AddTicks(3888));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 436, DateTimeKind.Utc).AddTicks(4639));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 436, DateTimeKind.Utc).AddTicks(4664));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 436, DateTimeKind.Utc).AddTicks(4678));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 436, DateTimeKind.Utc).AddTicks(4691));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 436, DateTimeKind.Utc).AddTicks(4705));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 433, DateTimeKind.Utc).AddTicks(2730));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 433, DateTimeKind.Utc).AddTicks(4975));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 12, 53, 57, 433, DateTimeKind.Utc).AddTicks(5011));

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_ServiceSectors_ServiceSectorId",
                table: "Manager",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manager_ServiceSectors_ServiceSectorId",
                table: "Manager");

            migrationBuilder.AlterColumn<int>(
                name: "ServiceSectorId",
                table: "Manager",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(2401));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3321));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3359));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3380));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3399));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 499, DateTimeKind.Utc).AddTicks(3420));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 494, DateTimeKind.Utc).AddTicks(886));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 494, DateTimeKind.Utc).AddTicks(2849));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 25, 11, 22, 29, 494, DateTimeKind.Utc).AddTicks(2896));

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_ServiceSectors_ServiceSectorId",
                table: "Manager",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
