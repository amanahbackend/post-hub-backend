﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class changSenderName_20220126 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 739, DateTimeKind.Utc).AddTicks(5633));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 739, DateTimeKind.Utc).AddTicks(7693));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 739, DateTimeKind.Utc).AddTicks(7850));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 739, DateTimeKind.Utc).AddTicks(7887));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 739, DateTimeKind.Utc).AddTicks(7913));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 739, DateTimeKind.Utc).AddTicks(7944));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 732, DateTimeKind.Utc).AddTicks(2090));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 732, DateTimeKind.Utc).AddTicks(7186));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 11, 1, 37, 732, DateTimeKind.Utc).AddTicks(7310));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 857, DateTimeKind.Utc).AddTicks(4690));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 857, DateTimeKind.Utc).AddTicks(5739));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 857, DateTimeKind.Utc).AddTicks(5767));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 857, DateTimeKind.Utc).AddTicks(5781));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 857, DateTimeKind.Utc).AddTicks(5793));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 857, DateTimeKind.Utc).AddTicks(5808));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 853, DateTimeKind.Utc).AddTicks(4531));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 853, DateTimeKind.Utc).AddTicks(6298));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 26, 10, 51, 15, 853, DateTimeKind.Utc).AddTicks(6326));
        }
    }
}
