﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class changInOrder_20220130 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 835, DateTimeKind.Utc).AddTicks(8984));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 836, DateTimeKind.Utc).AddTicks(536));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 836, DateTimeKind.Utc).AddTicks(577));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 836, DateTimeKind.Utc).AddTicks(599));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 836, DateTimeKind.Utc).AddTicks(620));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 836, DateTimeKind.Utc).AddTicks(644));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 831, DateTimeKind.Utc).AddTicks(2595));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 831, DateTimeKind.Utc).AddTicks(4805));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 8, 14, 50, 831, DateTimeKind.Utc).AddTicks(4855));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 633, DateTimeKind.Utc).AddTicks(9514));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 634, DateTimeKind.Utc).AddTicks(300));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 634, DateTimeKind.Utc).AddTicks(329));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 634, DateTimeKind.Utc).AddTicks(344));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 634, DateTimeKind.Utc).AddTicks(359));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 634, DateTimeKind.Utc).AddTicks(375));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 630, DateTimeKind.Utc).AddTicks(9226));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 631, DateTimeKind.Utc).AddTicks(813));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 1, 30, 7, 28, 43, 631, DateTimeKind.Utc).AddTicks(850));
        }
    }
}
