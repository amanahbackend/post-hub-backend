﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class workOrderServiceSector : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ServiceSectorId",
                table: "Workorders",
                type: "int",
                nullable: true);


           
            migrationBuilder.CreateIndex(
                name: "IX_Workorders_ServiceSectorId",
                table: "Workorders",
                column: "ServiceSectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Workorders_ServiceSectors_ServiceSectorId",
                table: "Workorders",
                column: "ServiceSectorId",
                principalTable: "ServiceSectors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workorders_ServiceSectors_ServiceSectorId",
                table: "Workorders");

            migrationBuilder.DropIndex(
                name: "IX_Workorders_ServiceSectorId",
                table: "Workorders");

            migrationBuilder.DropColumn(
                name: "ServiceSectorId",
                table: "Workorders");

           
        }
    }
}
