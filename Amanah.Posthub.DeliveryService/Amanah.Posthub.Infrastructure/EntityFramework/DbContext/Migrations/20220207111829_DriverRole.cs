﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class DriverRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DriverRoleId",
                table: "Driver",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DriverRoles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DriverRoles_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

           
            migrationBuilder.CreateIndex(
                name: "IX_Driver_DriverRoleId",
                table: "Driver",
                column: "DriverRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverRoles_Tenant_Id",
                table: "DriverRoles",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Driver_DriverRoles_DriverRoleId",
                table: "Driver",
                column: "DriverRoleId",
                principalTable: "DriverRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Driver_DriverRoles_DriverRoleId",
                table: "Driver");

            migrationBuilder.DropTable(
                name: "DriverRoles");

            migrationBuilder.DropIndex(
                name: "IX_Driver_DriverRoleId",
                table: "Driver");

            migrationBuilder.DropColumn(
                name: "DriverRoleId",
                table: "Driver");

           
        }
    }
}
