﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class gov_countryId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Area",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Area_CountryId",
                table: "Area",
                column: "CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Area_Country_CountryId",
                table: "Area",
                column: "CountryId",
                principalTable: "Country",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Area_Country_CountryId",
                table: "Area");

            migrationBuilder.DropIndex(
                name: "IX_Area_CountryId",
                table: "Area");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Area");

        }
    }
}
