﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class BusinessOrderPackageServicesRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "OrderPackagePrice",
                table: "BusinessOrders",
                type: "decimal(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PackingPackageId",
                table: "BusinessOrders",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BusinessOrderServices",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BusinessOrderId = table.Column<int>(type: "int", nullable: true),
                    ServiceId = table.Column<int>(type: "int", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BusinessOrderServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BusinessOrderServices_AspNetUsers_Tenant_Id",
                        column: x => x.Tenant_Id,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrderServices_BusinessOrders_BusinessOrderId",
                        column: x => x.BusinessOrderId,
                        principalTable: "BusinessOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BusinessOrderServices_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 166, DateTimeKind.Utc).AddTicks(3383));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 166, DateTimeKind.Utc).AddTicks(5629));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 166, DateTimeKind.Utc).AddTicks(5786));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 166, DateTimeKind.Utc).AddTicks(5833));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 166, DateTimeKind.Utc).AddTicks(5865));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 166, DateTimeKind.Utc).AddTicks(5904));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 157, DateTimeKind.Utc).AddTicks(7718));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 158, DateTimeKind.Utc).AddTicks(1940));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 13, 12, 29, 55, 158, DateTimeKind.Utc).AddTicks(2012));

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrders_PackingPackageId",
                table: "BusinessOrders",
                column: "PackingPackageId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrderServices_BusinessOrderId",
                table: "BusinessOrderServices",
                column: "BusinessOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrderServices_ServiceId",
                table: "BusinessOrderServices",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_BusinessOrderServices_Tenant_Id",
                table: "BusinessOrderServices",
                column: "Tenant_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BusinessOrders_PackingPackages_PackingPackageId",
                table: "BusinessOrders",
                column: "PackingPackageId",
                principalTable: "PackingPackages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BusinessOrders_PackingPackages_PackingPackageId",
                table: "BusinessOrders");

            migrationBuilder.DropTable(
                name: "BusinessOrderServices");

            migrationBuilder.DropIndex(
                name: "IX_BusinessOrders_PackingPackageId",
                table: "BusinessOrders");

            migrationBuilder.DropColumn(
                name: "OrderPackagePrice",
                table: "BusinessOrders");

            migrationBuilder.DropColumn(
                name: "PackingPackageId",
                table: "BusinessOrders");

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 88, DateTimeKind.Utc).AddTicks(5198));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 88, DateTimeKind.Utc).AddTicks(9026));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 88, DateTimeKind.Utc).AddTicks(9107));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 88, DateTimeKind.Utc).AddTicks(9153));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 88, DateTimeKind.Utc).AddTicks(9193));

            migrationBuilder.UpdateData(
                table: "MailItemTypes",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 88, DateTimeKind.Utc).AddTicks(9239));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 77, DateTimeKind.Utc).AddTicks(2468));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 77, DateTimeKind.Utc).AddTicks(6556));

            migrationBuilder.UpdateData(
                table: "WeightUOMs",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedDate",
                value: new DateTime(2022, 2, 3, 9, 47, 25, 77, DateTimeKind.Utc).AddTicks(6603));
        }
    }
}
