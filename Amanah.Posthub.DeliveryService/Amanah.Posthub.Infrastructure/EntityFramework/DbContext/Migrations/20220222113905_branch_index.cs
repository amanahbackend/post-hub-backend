﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class branch_index : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.CreateIndex(
                name: "IX_Branch_Name_IsDeleted_CreatedDate",
                table: "Branch",
                columns: new[] { "Name", "IsDeleted", "CreatedDate" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Branch_Name_IsDeleted_CreatedDate",
                table: "Branch");

        }
    }
}
