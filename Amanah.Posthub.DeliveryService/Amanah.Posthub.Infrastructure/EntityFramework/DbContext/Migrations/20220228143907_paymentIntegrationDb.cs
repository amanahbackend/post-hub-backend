﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class paymentIntegrationDb : Migration
    {
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			var paymentDB = @"SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PaymentTransactions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [nvarchar](max) NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[ReferenceId] [nvarchar](max) NULL,
	[TotalPrice] [float] NOT NULL,
	[PaymentGatewayId] [int] NOT NULL,
	[PaymentRequestDetails] [nvarchar](max) NULL,
	[PaymentResponseDetails] [nvarchar](max) NULL,
	[TransactionResponseDetails] [nvarchar](max) NULL,
	[TransactionResponseDate] [datetime2](7) NULL,
	[IsSuccessTransaction] [bit] NULL,
	[TransactionStatusId] [int] NOT NULL,
	[OrderId] [nvarchar](max) NULL,
 CONSTRAINT [PK_PaymentTransactions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
";
			migrationBuilder.Sql(paymentDB);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.Sql(@"Drop table [dbo].[PaymentTransactions]");
		}
	}
}
