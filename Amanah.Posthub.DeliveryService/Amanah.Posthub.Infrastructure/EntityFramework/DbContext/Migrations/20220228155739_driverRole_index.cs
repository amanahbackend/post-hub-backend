﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class driverRole_index : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "IX_Branch_Name_Phone_Mobile_IsDeleted_CreatedDate",
            //    table: "Branch");

            migrationBuilder.AlterColumn<string>(
                name: "Name_en",
                table: "DriverRoles",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name_ar",
                table: "DriverRoles",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Mobile",
                table: "Branch",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);



            migrationBuilder.CreateIndex(
                name: "IX_DriverRoles_Name_ar_Name_en_IsDeleted_CreatedDate",
                table: "DriverRoles",
                columns: new[] { "Name_ar", "Name_en", "IsDeleted", "CreatedDate" },
                unique: true,
                filter: "[Name_ar] IS NOT NULL AND [Name_en] IS NOT NULL");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Branch_Name_IsDeleted_CreatedDate",
            //    table: "Branch",
            //    columns: new[] { "Name", "IsDeleted", "CreatedDate" },
            //    unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_DriverRoles_Name_ar_Name_en_IsDeleted_CreatedDate",
                table: "DriverRoles");

            //migrationBuilder.DropIndex(
            //    name: "IX_Branch_Name_IsDeleted_CreatedDate",
            //    table: "Branch");

            migrationBuilder.AlterColumn<string>(
                name: "Name_en",
                table: "DriverRoles",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name_ar",
                table: "DriverRoles",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Mobile",
                table: "Branch",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Branch_Name_Phone_Mobile_IsDeleted_CreatedDate",
            //    table: "Branch",
            //    columns: new[] { "Name", "Phone", "Mobile", "IsDeleted", "CreatedDate" },
            //    unique: true,
            //    filter: "[Name] IS NOT NULL AND [Phone] IS NOT NULL AND [Mobile] IS NOT NULL");
        }
    }
}
