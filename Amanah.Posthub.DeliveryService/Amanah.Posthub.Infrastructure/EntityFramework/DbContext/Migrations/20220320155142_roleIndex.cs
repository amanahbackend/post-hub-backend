﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class roleIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
           ;

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoles_Name_IsDeleted_CreatedDate",
                table: "AspNetRoles",
                columns: new[] { "Name", "IsDeleted", "CreatedDate" },
                unique: true,
                filter: "[Name] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AspNetRoles_Name_IsDeleted_CreatedDate",
                table: "AspNetRoles");

        }
    }
}
