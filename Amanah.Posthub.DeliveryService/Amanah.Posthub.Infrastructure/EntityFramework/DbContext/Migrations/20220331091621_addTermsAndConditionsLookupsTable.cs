﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class addTermsAndConditionsLookupsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Manager_ServiceSectors_ServiceSectorId",
            //        table: "Manager");

            //    migrationBuilder.AddColumn<int>(
            //        name: "SecurityCaseNumber",
            //        table: "Workorders",
            //        type: "int",
            //        nullable: true);

            //    migrationBuilder.AddColumn<int>(
            //        name: "ServiceSectorId",
            //        table: "Workorders",
            //        type: "int",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "WorkOrderImageURL",
            //        table: "Workorders",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<bool>(
            //        name: "IsInternal",
            //        table: "Orders",
            //        type: "bit",
            //        nullable: false,
            //        defaultValue: false);

            //    migrationBuilder.AddColumn<int>(
            //        name: "SenderBranchId",
            //        table: "Orders",
            //        type: "int",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "SenderBranchName",
            //        table: "Orders",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "SenderEmail",
            //        table: "Orders",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "SenderFloorNo",
            //        table: "Orders",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "SenderName",
            //        table: "Orders",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "StatementCode",
            //        table: "Orders",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AlterColumn<int>(
            //        name: "ServiceSectorId",
            //        table: "Manager",
            //        type: "int",
            //        nullable: true,
            //        oldClrType: typeof(int),
            //        oldType: "int");

            //    migrationBuilder.AddColumn<string>(
            //        name: "Description",
            //        table: "MailItems",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<int>(
            //        name: "RemarkId",
            //        table: "MailItems",
            //        type: "int",
            //        nullable: true);

            //    migrationBuilder.AddColumn<int>(
            //        name: "DriverRoleId",
            //        table: "Driver",
            //        type: "int",
            //        nullable: true);

            //    migrationBuilder.AddColumn<int>(
            //        name: "FloorNo",
            //        table: "Departments",
            //        type: "int",
            //        nullable: false,
            //        defaultValue: 0);

            //    migrationBuilder.AddColumn<int>(
            //        name: "BranchId",
            //        table: "Contacts",
            //        type: "int",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "BranchName",
            //        table: "Contacts",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<int>(
            //        name: "DepartmentId",
            //        table: "Contacts",
            //        type: "int",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "DepartmentName",
            //        table: "Contacts",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "Email",
            //        table: "Contacts",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<string>(
            //        name: "FloorNo",
            //        table: "Contacts",
            //        type: "nvarchar(max)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<decimal>(
            //        name: "OrderPackagePrice",
            //        table: "BusinessOrders",
            //        type: "decimal(18,2)",
            //        nullable: true);

            //    migrationBuilder.AddColumn<int>(
            //        name: "PackingPackageId",
            //        table: "BusinessOrders",
            //        type: "int",
            //        nullable: true);

            //    migrationBuilder.AddColumn<int>(
            //        name: "FloorsCount",
            //        table: "Branch",
            //        type: "int",
            //        nullable: false,
            //        defaultValue: 0);

            //    migrationBuilder.CreateTable(
            //        name: "DriverExternalBranchs",
            //        columns: table => new
            //        {
            //            Id = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            DriverId = table.Column<int>(type: "int", nullable: false),
            //            BranchId = table.Column<int>(type: "int", nullable: true),
            //            CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_DriverExternalBranchs", x => x.Id);
            //            table.ForeignKey(
            //                name: "FK_DriverExternalBranchs_AspNetUsers_Tenant_Id",
            //                column: x => x.Tenant_Id,
            //                principalTable: "AspNetUsers",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_DriverExternalBranchs_Branch_BranchId",
            //                column: x => x.BranchId,
            //                principalTable: "Branch",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_DriverExternalBranchs_Driver_DriverId",
            //                column: x => x.DriverId,
            //                principalTable: "Driver",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Cascade);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "DriverFloors",
            //        columns: table => new
            //        {
            //            Id = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            Floor = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            BranchId = table.Column<int>(type: "int", nullable: false),
            //            DriverId = table.Column<int>(type: "int", nullable: false),
            //            CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_DriverFloors", x => x.Id);
            //            table.ForeignKey(
            //                name: "FK_DriverFloors_AspNetUsers_Tenant_Id",
            //                column: x => x.Tenant_Id,
            //                principalTable: "AspNetUsers",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_DriverFloors_Branch_BranchId",
            //                column: x => x.BranchId,
            //                principalTable: "Branch",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Cascade);
            //            table.ForeignKey(
            //                name: "FK_DriverFloors_Driver_DriverId",
            //                column: x => x.DriverId,
            //                principalTable: "Driver",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Cascade);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "DriverRoles",
            //        columns: table => new
            //        {
            //            Id = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            Name_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            Name_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_DriverRoles", x => x.Id);
            //            table.ForeignKey(
            //                name: "FK_DriverRoles_AspNetUsers_Tenant_Id",
            //                column: x => x.Tenant_Id,
            //                principalTable: "AspNetUsers",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "PackingPackages",
            //        columns: table => new
            //        {
            //            Id = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            Name_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            Name_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            Description_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            Description_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DimensionWidth = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //            DimensionLength = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //            DimensionHight = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //            MaxWeight = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //            CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_PackingPackages", x => x.Id);
            //            table.ForeignKey(
            //                name: "FK_PackingPackages_AspNetUsers_Tenant_Id",
            //                column: x => x.Tenant_Id,
            //                principalTable: "AspNetUsers",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "PaymentTransactions",
            //        columns: table => new
            //        {
            //            Id = table.Column<long>(type: "bigint", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            CreatedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            OrderId = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            TransactionStatusId = table.Column<int>(type: "int", nullable: false),
            //            PaymentGatewayId = table.Column<int>(type: "int", nullable: false),
            //            ReferenceId = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            TotalPrice = table.Column<double>(type: "float", nullable: false),
            //            IsSuccessTransaction = table.Column<bool>(type: "bit", nullable: true),
            //            PaymentResponseDetails = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            PaymentRequestDetails = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            TransactionResponseDetails = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            TransactionResponseDate = table.Column<DateTime>(type: "datetime2", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_PaymentTransactions", x => x.Id);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "Remarks",
            //        columns: table => new
            //        {
            //            Id = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            Name_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            Name_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsActive = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_Remarks", x => x.Id);
            //            table.ForeignKey(
            //                name: "FK_Remarks_AspNetUsers_Tenant_Id",
            //                column: x => x.Tenant_Id,
            //                principalTable: "AspNetUsers",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "Services",
            //        columns: table => new
            //        {
            //            Id = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            Name_en = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            Name_ar = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //            PhotoUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_Services", x => x.Id);
            //            table.ForeignKey(
            //                name: "FK_Services_AspNetUsers_Tenant_Id",
            //                column: x => x.Tenant_Id,
            //                principalTable: "AspNetUsers",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            migrationBuilder.CreateTable(
                name: "TermsAndConditionsLookups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TermsAndConditions_En = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    TermsAndConditions_Ar = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TermsAndConditionsLookups", x => x.Id);
                });

            //    migrationBuilder.CreateTable(
            //        name: "PackingPackageCountries",
            //        columns: table => new
            //        {
            //            Id = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            //            CountryId = table.Column<int>(type: "int", nullable: false),
            //            PackingPackageId = table.Column<int>(type: "int", nullable: false),
            //            CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_PackingPackageCountries", x => x.Id);
            //            table.ForeignKey(
            //                name: "FK_PackingPackageCountries_AspNetUsers_Tenant_Id",
            //                column: x => x.Tenant_Id,
            //                principalTable: "AspNetUsers",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_PackingPackageCountries_Country_CountryId",
            //                column: x => x.CountryId,
            //                principalTable: "Country",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Cascade);
            //            table.ForeignKey(
            //                name: "FK_PackingPackageCountries_PackingPackages_PackingPackageId",
            //                column: x => x.PackingPackageId,
            //                principalTable: "PackingPackages",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Cascade);
            //        });

            //    migrationBuilder.CreateTable(
            //        name: "BusinessOrderServices",
            //        columns: table => new
            //        {
            //            Id = table.Column<int>(type: "int", nullable: false)
            //                .Annotation("SqlServer:Identity", "1, 1"),
            //            BusinessOrderId = table.Column<int>(type: "int", nullable: true),
            //            ServiceId = table.Column<int>(type: "int", nullable: true),
            //            Price = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
            //            CreatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            UpdatedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            DeletedBy_Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
            //            IsDeleted = table.Column<bool>(type: "bit", nullable: false),
            //            CreatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            UpdatedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            //            Tenant_Id = table.Column<string>(type: "varchar(36)", nullable: true)
            //        },
            //        constraints: table =>
            //        {
            //            table.PrimaryKey("PK_BusinessOrderServices", x => x.Id);
            //            table.ForeignKey(
            //                name: "FK_BusinessOrderServices_AspNetUsers_Tenant_Id",
            //                column: x => x.Tenant_Id,
            //                principalTable: "AspNetUsers",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_BusinessOrderServices_BusinessOrders_BusinessOrderId",
            //                column: x => x.BusinessOrderId,
            //                principalTable: "BusinessOrders",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //            table.ForeignKey(
            //                name: "FK_BusinessOrderServices_Services_ServiceId",
            //                column: x => x.ServiceId,
            //                principalTable: "Services",
            //                principalColumn: "Id",
            //                onDelete: ReferentialAction.Restrict);
            //        });

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 1,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 514, DateTimeKind.Utc).AddTicks(2754));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 2,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 514, DateTimeKind.Utc).AddTicks(6144));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 3,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 514, DateTimeKind.Utc).AddTicks(6258));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 4,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 514, DateTimeKind.Utc).AddTicks(6324));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 5,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 514, DateTimeKind.Utc).AddTicks(6389));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 6,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 514, DateTimeKind.Utc).AddTicks(6459));

            //    migrationBuilder.UpdateData(
            //        table: "WeightUOMs",
            //        keyColumn: "Id",
            //        keyValue: 1,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 504, DateTimeKind.Utc).AddTicks(6515));

            //    migrationBuilder.UpdateData(
            //        table: "WeightUOMs",
            //        keyColumn: "Id",
            //        keyValue: 2,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 505, DateTimeKind.Utc).AddTicks(1119));

            //    migrationBuilder.UpdateData(
            //        table: "WeightUOMs",
            //        keyColumn: "Id",
            //        keyValue: 3,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 31, 9, 16, 14, 505, DateTimeKind.Utc).AddTicks(1207));

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Workorders_ServiceSectorId",
            //        table: "Workorders",
            //        column: "ServiceSectorId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Orders_SenderBranchId",
            //        table: "Orders",
            //        column: "SenderBranchId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_MailItems_RemarkId",
            //        table: "MailItems",
            //        column: "RemarkId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Driver_DriverRoleId",
            //        table: "Driver",
            //        column: "DriverRoleId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Contacts_BranchId",
            //        table: "Contacts",
            //        column: "BranchId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Contacts_DepartmentId",
            //        table: "Contacts",
            //        column: "DepartmentId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_BusinessOrders_PackingPackageId",
            //        table: "BusinessOrders",
            //        column: "PackingPackageId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Branch_Name_IsDeleted_CreatedDate",
            //        table: "Branch",
            //        columns: new[] { "Name", "IsDeleted", "CreatedDate" },
            //        unique: true);

            //    migrationBuilder.CreateIndex(
            //        name: "IX_BusinessOrderServices_BusinessOrderId",
            //        table: "BusinessOrderServices",
            //        column: "BusinessOrderId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_BusinessOrderServices_ServiceId",
            //        table: "BusinessOrderServices",
            //        column: "ServiceId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_BusinessOrderServices_Tenant_Id",
            //        table: "BusinessOrderServices",
            //        column: "Tenant_Id");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_DriverExternalBranchs_BranchId",
            //        table: "DriverExternalBranchs",
            //        column: "BranchId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_DriverExternalBranchs_DriverId",
            //        table: "DriverExternalBranchs",
            //        column: "DriverId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_DriverExternalBranchs_Tenant_Id",
            //        table: "DriverExternalBranchs",
            //        column: "Tenant_Id");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_DriverFloors_BranchId",
            //        table: "DriverFloors",
            //        column: "BranchId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_DriverFloors_DriverId",
            //        table: "DriverFloors",
            //        column: "DriverId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_DriverFloors_Tenant_Id",
            //        table: "DriverFloors",
            //        column: "Tenant_Id");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_DriverRoles_Tenant_Id",
            //        table: "DriverRoles",
            //        column: "Tenant_Id");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_PackingPackageCountries_CountryId_IsDeleted_CreatedDate",
            //        table: "PackingPackageCountries",
            //        columns: new[] { "CountryId", "IsDeleted", "CreatedDate" },
            //        unique: true);

            //    migrationBuilder.CreateIndex(
            //        name: "IX_PackingPackageCountries_PackingPackageId",
            //        table: "PackingPackageCountries",
            //        column: "PackingPackageId");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_PackingPackageCountries_Tenant_Id",
            //        table: "PackingPackageCountries",
            //        column: "Tenant_Id");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_PackingPackages_Tenant_Id",
            //        table: "PackingPackages",
            //        column: "Tenant_Id");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Remarks_Tenant_Id",
            //        table: "Remarks",
            //        column: "Tenant_Id");

            //    migrationBuilder.CreateIndex(
            //        name: "IX_Services_Tenant_Id",
            //        table: "Services",
            //        column: "Tenant_Id");

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_BusinessOrders_PackingPackages_PackingPackageId",
            //        table: "BusinessOrders",
            //        column: "PackingPackageId",
            //        principalTable: "PackingPackages",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Contacts_Branch_BranchId",
            //        table: "Contacts",
            //        column: "BranchId",
            //        principalTable: "Branch",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Contacts_Departments_DepartmentId",
            //        table: "Contacts",
            //        column: "DepartmentId",
            //        principalTable: "Departments",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Driver_DriverRoles_DriverRoleId",
            //        table: "Driver",
            //        column: "DriverRoleId",
            //        principalTable: "DriverRoles",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_MailItems_Remarks_RemarkId",
            //        table: "MailItems",
            //        column: "RemarkId",
            //        principalTable: "Remarks",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Manager_ServiceSectors_ServiceSectorId",
            //        table: "Manager",
            //        column: "ServiceSectorId",
            //        principalTable: "ServiceSectors",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Orders_Branch_SenderBranchId",
            //        table: "Orders",
            //        column: "SenderBranchId",
            //        principalTable: "Branch",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Workorders_ServiceSectors_ServiceSectorId",
            //        table: "Workorders",
            //        column: "ServiceSectorId",
            //        principalTable: "ServiceSectors",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //    migrationBuilder.DropForeignKey(
            //        name: "FK_BusinessOrders_PackingPackages_PackingPackageId",
            //        table: "BusinessOrders");

            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Contacts_Branch_BranchId",
            //        table: "Contacts");

            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Contacts_Departments_DepartmentId",
            //        table: "Contacts");

            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Driver_DriverRoles_DriverRoleId",
            //        table: "Driver");

            //    migrationBuilder.DropForeignKey(
            //        name: "FK_MailItems_Remarks_RemarkId",
            //        table: "MailItems");

            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Manager_ServiceSectors_ServiceSectorId",
            //        table: "Manager");

            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Orders_Branch_SenderBranchId",
            //        table: "Orders");

            //    migrationBuilder.DropForeignKey(
            //        name: "FK_Workorders_ServiceSectors_ServiceSectorId",
            //        table: "Workorders");

            //    migrationBuilder.DropTable(
            //        name: "BusinessOrderServices");

            //    migrationBuilder.DropTable(
            //        name: "DriverExternalBranchs");

            //    migrationBuilder.DropTable(
            //        name: "DriverFloors");

            //    migrationBuilder.DropTable(
            //        name: "DriverRoles");

            //    migrationBuilder.DropTable(
            //        name: "PackingPackageCountries");

            //    migrationBuilder.DropTable(
            //        name: "PaymentTransactions");

            //    migrationBuilder.DropTable(
            //        name: "Remarks");

            migrationBuilder.DropTable(
                name: "TermsAndConditionsLookups");

            //    migrationBuilder.DropTable(
            //        name: "Services");

            //    migrationBuilder.DropTable(
            //        name: "PackingPackages");

            //    migrationBuilder.DropIndex(
            //        name: "IX_Workorders_ServiceSectorId",
            //        table: "Workorders");

            //    migrationBuilder.DropIndex(
            //        name: "IX_Orders_SenderBranchId",
            //        table: "Orders");

            //    migrationBuilder.DropIndex(
            //        name: "IX_MailItems_RemarkId",
            //        table: "MailItems");

            //    migrationBuilder.DropIndex(
            //        name: "IX_Driver_DriverRoleId",
            //        table: "Driver");

            //    migrationBuilder.DropIndex(
            //        name: "IX_Contacts_BranchId",
            //        table: "Contacts");

            //    migrationBuilder.DropIndex(
            //        name: "IX_Contacts_DepartmentId",
            //        table: "Contacts");

            //    migrationBuilder.DropIndex(
            //        name: "IX_BusinessOrders_PackingPackageId",
            //        table: "BusinessOrders");

            //    migrationBuilder.DropIndex(
            //        name: "IX_Branch_Name_IsDeleted_CreatedDate",
            //        table: "Branch");

            //    migrationBuilder.DropColumn(
            //        name: "SecurityCaseNumber",
            //        table: "Workorders");

            //    migrationBuilder.DropColumn(
            //        name: "ServiceSectorId",
            //        table: "Workorders");

            //    migrationBuilder.DropColumn(
            //        name: "WorkOrderImageURL",
            //        table: "Workorders");

            //    migrationBuilder.DropColumn(
            //        name: "IsInternal",
            //        table: "Orders");

            //    migrationBuilder.DropColumn(
            //        name: "SenderBranchId",
            //        table: "Orders");

            //    migrationBuilder.DropColumn(
            //        name: "SenderBranchName",
            //        table: "Orders");

            //    migrationBuilder.DropColumn(
            //        name: "SenderEmail",
            //        table: "Orders");

            //    migrationBuilder.DropColumn(
            //        name: "SenderFloorNo",
            //        table: "Orders");

            //    migrationBuilder.DropColumn(
            //        name: "SenderName",
            //        table: "Orders");

            //    migrationBuilder.DropColumn(
            //        name: "StatementCode",
            //        table: "Orders");

            //    migrationBuilder.DropColumn(
            //        name: "Description",
            //        table: "MailItems");

            //    migrationBuilder.DropColumn(
            //        name: "RemarkId",
            //        table: "MailItems");

            //    migrationBuilder.DropColumn(
            //        name: "DriverRoleId",
            //        table: "Driver");

            //    migrationBuilder.DropColumn(
            //        name: "FloorNo",
            //        table: "Departments");

            //    migrationBuilder.DropColumn(
            //        name: "BranchId",
            //        table: "Contacts");

            //    migrationBuilder.DropColumn(
            //        name: "BranchName",
            //        table: "Contacts");

            //    migrationBuilder.DropColumn(
            //        name: "DepartmentId",
            //        table: "Contacts");

            //    migrationBuilder.DropColumn(
            //        name: "DepartmentName",
            //        table: "Contacts");

            //    migrationBuilder.DropColumn(
            //        name: "Email",
            //        table: "Contacts");

            //    migrationBuilder.DropColumn(
            //        name: "FloorNo",
            //        table: "Contacts");

            //    migrationBuilder.DropColumn(
            //        name: "OrderPackagePrice",
            //        table: "BusinessOrders");

            //    migrationBuilder.DropColumn(
            //        name: "PackingPackageId",
            //        table: "BusinessOrders");

            //    migrationBuilder.DropColumn(
            //        name: "FloorsCount",
            //        table: "Branch");

            //    migrationBuilder.AlterColumn<int>(
            //        name: "ServiceSectorId",
            //        table: "Manager",
            //        type: "int",
            //        nullable: false,
            //        defaultValue: 0,
            //        oldClrType: typeof(int),
            //        oldType: "int",
            //        oldNullable: true);

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 1,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 713, DateTimeKind.Utc).AddTicks(7751));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 2,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 714, DateTimeKind.Utc).AddTicks(308));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 3,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 714, DateTimeKind.Utc).AddTicks(378));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 4,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 714, DateTimeKind.Utc).AddTicks(412));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 5,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 714, DateTimeKind.Utc).AddTicks(438));

            //    migrationBuilder.UpdateData(
            //        table: "MailItemTypes",
            //        keyColumn: "Id",
            //        keyValue: 6,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 714, DateTimeKind.Utc).AddTicks(469));

            //    migrationBuilder.UpdateData(
            //        table: "WeightUOMs",
            //        keyColumn: "Id",
            //        keyValue: 1,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 704, DateTimeKind.Utc).AddTicks(9633));

            //    migrationBuilder.UpdateData(
            //        table: "WeightUOMs",
            //        keyColumn: "Id",
            //        keyValue: 2,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 705, DateTimeKind.Utc).AddTicks(2307));

            //    migrationBuilder.UpdateData(
            //        table: "WeightUOMs",
            //        keyColumn: "Id",
            //        keyValue: 3,
            //        column: "CreatedDate",
            //        value: new DateTime(2022, 3, 21, 12, 0, 16, 705, DateTimeKind.Utc).AddTicks(2353));

            //    migrationBuilder.AddForeignKey(
            //        name: "FK_Manager_ServiceSectors_ServiceSectorId",
            //        table: "Manager",
            //        column: "ServiceSectorId",
            //        principalTable: "ServiceSectors",
            //        principalColumn: "Id",
            //        onDelete: ReferentialAction.Cascade);
        }
    }
}
