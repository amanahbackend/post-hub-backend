﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Amanah.Posthub.DATA.DbContext.Migrations
{
    public partial class AddTitleColumsInTremsAndConditionsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Title_Ar",
                table: "TermsAndConditionsLookups",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Title_En",
                table: "TermsAndConditionsLookups",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 1,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 468, DateTimeKind.Utc).AddTicks(7024));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 2,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 468, DateTimeKind.Utc).AddTicks(9551));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 3,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 468, DateTimeKind.Utc).AddTicks(9609));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 4,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 468, DateTimeKind.Utc).AddTicks(9642));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 5,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 468, DateTimeKind.Utc).AddTicks(9670));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 6,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 468, DateTimeKind.Utc).AddTicks(9705));

            //migrationBuilder.UpdateData(
            //    table: "WeightUOMs",
            //    keyColumn: "Id",
            //    keyValue: 1,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 459, DateTimeKind.Utc).AddTicks(7085));

            //migrationBuilder.UpdateData(
            //    table: "WeightUOMs",
            //    keyColumn: "Id",
            //    keyValue: 2,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 460, DateTimeKind.Utc).AddTicks(1355));

            //migrationBuilder.UpdateData(
            //    table: "WeightUOMs",
            //    keyColumn: "Id",
            //    keyValue: 3,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 14, 10, 58, 57, 460, DateTimeKind.Utc).AddTicks(1416));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Title_Ar",
                table: "TermsAndConditionsLookups");

            migrationBuilder.DropColumn(
                name: "Title_En",
                table: "TermsAndConditionsLookups");

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 1,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 336, DateTimeKind.Utc).AddTicks(7844));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 2,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 336, DateTimeKind.Utc).AddTicks(9431));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 3,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 336, DateTimeKind.Utc).AddTicks(9466));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 4,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 336, DateTimeKind.Utc).AddTicks(9484));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 5,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 336, DateTimeKind.Utc).AddTicks(9501));

            //migrationBuilder.UpdateData(
            //    table: "MailItemTypes",
            //    keyColumn: "Id",
            //    keyValue: 6,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 336, DateTimeKind.Utc).AddTicks(9525));

            //migrationBuilder.UpdateData(
            //    table: "WeightUOMs",
            //    keyColumn: "Id",
            //    keyValue: 1,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 329, DateTimeKind.Utc).AddTicks(9705));

            //migrationBuilder.UpdateData(
            //    table: "WeightUOMs",
            //    keyColumn: "Id",
            //    keyValue: 2,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 330, DateTimeKind.Utc).AddTicks(2294));

            //migrationBuilder.UpdateData(
            //    table: "WeightUOMs",
            //    keyColumn: "Id",
            //    keyValue: 3,
            //    column: "CreatedDate",
            //    value: new DateTime(2022, 4, 10, 11, 47, 11, 330, DateTimeKind.Utc).AddTicks(2332));
        }
    }
}
