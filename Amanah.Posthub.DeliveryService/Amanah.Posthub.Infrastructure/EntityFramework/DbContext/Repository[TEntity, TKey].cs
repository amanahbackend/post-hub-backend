﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Amanah.Posthub.SharedKernel.Utilites.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;


namespace Amanah.Posthub.Infrastructure.EntityFramework.DbContext
{
    public class Repository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        protected readonly ApplicationDbContext _dbContext;

        protected virtual DbSet<TEntity> DbSet => _dbContext.Set<TEntity>();

        public Repository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual Task<TEntity> GetByIdAsync(TKey id)
        {
            return DbSet.AsNoTracking().IgnoreQueryFilters().SingleOrDefaultAsync(entity => entity.Id.Equals(id));
        }

        public virtual async Task<IReadOnlyCollection<TEntity>> GetAllAsync(
        [NotNull] Expression<Func<TEntity, bool>> predicate,
        params SortingField<TEntity>[] sortingFields)
        {
            Check.NotNull(predicate, nameof(predicate));
            Check.NotNull(sortingFields, nameof(sortingFields));
            return await DbSet.Where(predicate)
                .AsNoTracking()
                .AddSorting(sortingFields)
                .ToArrayAsync();
            //.ContinueWith(array => (IReadOnlyCollection<TEntity>)array,
            //    TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        public virtual IQueryable<TEntity> GetAllIQueryable(
           [NotNull] Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            var query = DbSet.IgnoreQueryFilters().Where(predicate).AsNoTracking();

            return query;
        }

        public virtual IQueryable<TEntity> GetAllIQueryable(
          [NotNull] Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] include)
        {
            var query = GetAllIQueryable(predicate);
            Include(ref query, include);

            return query;
        }
        public virtual IQueryable<TEntity> GetAllIQueryable() => DbSet.AsQueryable();

        public virtual async Task<IReadOnlyCollection<TEntity>> ToIReadOnlyCollectionAsync(IQueryable<TEntity> query)
        {
            var res = await query.IgnoreQueryFilters().ToArrayAsync().ContinueWith(array => (IReadOnlyCollection<TEntity>)array);
            return res;
        }

        public virtual Task<_PagedResult<TEntity>> GetAllPagedResult(
            [NotNull] Expression<Func<TEntity, bool>> predicate,
            [NotNull] PagingOptions pagingOptions,
            params SortingField<TEntity>[] sortingFields)
        {
            Check.NotNull(predicate, nameof(predicate));
            Check.NotNull(pagingOptions, nameof(pagingOptions));
            Check.NotNull(sortingFields, nameof(sortingFields));
            return DbSet.IgnoreQueryFilters().Where(predicate)
                .AddSorting(sortingFields)
                .ToPagedResultAsync(pagingOptions);
        }

        public virtual Task<TEntity> GetFirstOrDefaultAsync(
            [NotNull] Expression<Func<TEntity, bool>> predicate,
            params SortingField<TEntity>[] sortingFields)
        {
            Check.NotNull(predicate, nameof(predicate));
            Check.NotNull(sortingFields, nameof(sortingFields));
            return DbSet.IgnoreQueryFilters().AddSorting(sortingFields)
                .AsNoTracking()
                .FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<TEntity> GetFirstOrDefaultAsync(
         [NotNull] Expression<Func<TEntity, bool>> predicate,
         params Expression<Func<TEntity, object>>[] include)
        {
            Check.NotNull(predicate, nameof(predicate));
            var query = DbSet.IgnoreQueryFilters().AsQueryable();
            Include(ref query, include);

            return await query.IgnoreQueryFilters().AsNoTracking().FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<TEntity> GetFirstOrDefaultAsync(
         [NotNull] Expression<Func<TEntity, bool>> predicate, bool IgnoreQueryFilters = false)
        {
            Check.NotNull(predicate, nameof(predicate));
            var query = DbSet.AsQueryable();
            if (IgnoreQueryFilters)
                query = query.IgnoreQueryFilters();
            return await query.AsNoTracking().FirstOrDefaultAsync(predicate);
        }

        public virtual async Task<TEntity> GetFirstOrDefaultAsync(
        [NotNull] Expression<Func<TEntity, bool>> predicate) => await GetFirstOrDefaultAsync(predicate, false);

        protected void Include(ref IQueryable<TEntity> query, params Expression<Func<TEntity, object>>[] include)
        {
            foreach (Expression<Func<TEntity, object>> navigationProperty in include)
                query = query.IgnoreQueryFilters().Include<TEntity, object>(navigationProperty);
        }

        public virtual Task<TEntity> GetSingleOrDefaultAsync([NotNull] Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return DbSet.IgnoreQueryFilters().AsNoTracking().SingleOrDefaultAsync(predicate);
        }

        public virtual Task<bool> AnyAsync([NotNull] Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return DbSet.IgnoreQueryFilters().AnyAsync(predicate);
        }

        public virtual Task<int> CountAsync([NotNull] Expression<Func<TEntity, bool>> predicate)
        {
            Check.NotNull(predicate, nameof(predicate));
            return DbSet.IgnoreQueryFilters().CountAsync(predicate);
        }

        public virtual void Add([NotNull] TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));
            DbSet.Add(entity);
        }

        public virtual void AddRange([NotNull] IEnumerable<TEntity> entities)
        {
            Check.NotNull(entities, nameof(entities));
            DbSet.AddRange(entities);
        }

        public virtual void Update([NotNull] TEntity entity, bool setTrackedEntity = true)
        {
            Check.NotNull(entity, nameof(entity));
            DbSet.Update(entity);
            if (setTrackedEntity)
            {
                var trackedEntity = DbSet.Local.FirstOrDefault(tracked => tracked.Id.Equals(entity.Id));
                if (trackedEntity != null)
                {
                    _dbContext.Entry(trackedEntity).CurrentValues.SetValues(entity);
                    // return trackedEntity;
                }
                else
                {
                    DbSet.Update(entity);
                    // return entity
                }
            }
        }


        public virtual void UpdateRange([NotNull] IEnumerable<TEntity> entities)
        {
            Check.NotNull(entities, nameof(entities));
            foreach (var entity in entities)
            {
                Update(entity);
            }
        }

        public virtual void Delete([NotNull] TEntity entity)
        {
            Check.NotNull(entity, nameof(entity));
            DbSet.Remove(entity);
        }

        public virtual void DeleteRange([NotNull] IEnumerable<TEntity> entities)
        {
            Check.NotNull(entities, nameof(entities));
            DbSet.RemoveRange(entities);
        }
    }

    public class Repository<TEntity> : Repository<TEntity, int>, IRepository<TEntity>
        where TEntity : class, IEntity<int>
    {
        public Repository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<UnitOfWork> _logger;

        public bool IsTenantFilterEnabled
        {
            get => _dbContext.IsTenantFilterEnabled;
            set => _dbContext.IsTenantFilterEnabled = value;
        }

        public UnitOfWork(ApplicationDbContext dbContext, ILogger<UnitOfWork> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }


        public Task<bool> RunTransaction(
            Func<Task> sequence,
            IsolationLevel isolationLevel = IsolationLevel.Serializable)
        {
            return _dbContext.Database
                .CreateExecutionStrategy()
                .ExecuteAsync(async () =>
                {
                    var result = true;
                    using var transaction = await _dbContext.Database.BeginTransactionAsync(isolationLevel);
                    try
                    {
                        await sequence();
                        await transaction.CommitAsync();
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, "Failed to run transaction");
                        result = false;
                        if (exception is DomainException)
                        {
                            throw;
                        }
                        else
                        {
                            throw new DomainException(Keys.Validation.GeneralError);
                        }
                    }

                    return result;
                });
        }

        public Task<int> SaveChangesAsync()
        {
            //try
            //{
            return _dbContext.SaveChangesAsync();
            //}
            //catch (Exception e)
            //{

            //    throw;
            //}

        }
    }
}
