﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.BASE.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.Repoistry
{
    public class Repositry<T> : IRepositry<T> where T : class, IHaveAuditing
    {
        private DbContext _context;
        private DbSet<T> _set;

        public Repositry(DbContext context)
        {
            _context = context;
            _set = _context.Set<T>();
        }

        public virtual IQueryable<T> GetAll()
        {
            return _set.IgnoreQueryFilters().OrderByDescending(x => x.CreatedDate)
                .AsNoTracking();
        }

        public virtual IQueryable<T> AsQueryable()
        {
            return _set.IgnoreQueryFilters().AsQueryable();
        }

        public virtual T Get(params object[] id)
        {
            return _set.Find(id);
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            T item = null;
            item = _set.IgnoreQueryFilters().FirstOrDefault(predicate);
            return item;
        }
        public async Task<T> GetAsync(Expression<Func<T, bool>> predicate)
        {
            T item = null;
            item = await _set.IgnoreQueryFilters().FirstOrDefaultAsync(predicate);
            return item;
        }

        public virtual T AddAsync(T entity)
        {
            T result = null;
            if (Validator.IsValid(entity))
            {
                entity = _set.Add(entity).Entity;
#warning cleanup - it is not responsibility of repo to save at all, it is unit of work who should save, this is not saving just entity but any modified entity tracked by dbcontext
                if (SaveChanges() > 0)
                {
                    result = entity;
                }
            }
            else
            {
#warning cleanup- efcore will do this instead of us
                StringBuilder exceptionMsgs = new StringBuilder();
                List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                foreach (var errmsg in errorMsgs)
                {
                    exceptionMsgs.Append(errmsg);
                    exceptionMsgs.Append("/n");
                }
                throw new Exception(exceptionMsgs.ToString());
            }
            return result;
        }

        public virtual bool Update(T entity)
        {
            InternalUpdate(entity);
            return SaveChanges() > 0;
        }

        public virtual bool Update(IEnumerable<T> entityLst)
        {
            foreach (var entity in entityLst)
            {
                InternalUpdate(entity);
            }
            return SaveChanges() > 0;
        }

        public virtual bool Delete(T entity)
        {
            return Delete(new[] { entity }); ;
        }

        public virtual bool DeleteById(params object[] id)
        {
            T entity = _set.Find(id);
            return Delete(entity);
        }

        public virtual bool Delete(IEnumerable<T> entitylst)
        {
            bool result = false;
            if (entitylst != null && entitylst.Any())
            {
                foreach (var entity in entitylst)
                {
                    // if tracked get and delete
                    var trackedEntity = GetTrackedEntity(entity);
                    if (trackedEntity != null)
                    {
                        trackedEntity.State = EntityState.Deleted;
                    }
                    else
                    {
                        _set.Remove(entity);
                    }
                }
                SaveChanges();
                result = true;
            }
            return result;
        }

        public virtual int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public virtual List<T> Add(IEnumerable<T> entityLst)
        {
            List<T> objList = new List<T>();
            foreach (T entity in entityLst)
                objList.Add(AddAsync(entity));
            return objList;
        }

        public bool SoftDelete(T entity)
        {
            return SoftDelete(new[] { entity });
        }

        public bool SoftDelete(IEnumerable<T> entityLst)
        {
            return Delete(entityLst);
        }

        public IQueryable<T> GetAllByPagination(IQueryable<T> listQuery, PaginatedItemsViewModel pagingparametermodel, out int totalNumbers)
        {
            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            var source = listQuery.AsQueryable().AsNoTracking();

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            totalNumbers = source.Count();
            var items = source.Skip((CurrentPage - 1) * PageSize).Take(PageSize);

            return items;
        }

        public async Task<PagedResult<T>> GetAllByPaginationAsync(IQueryable<T> listQuery, PaginatedItemsViewModel pagingparametermodel)
        {
            var pagedResult = new PagedResult<T>();

            pagingparametermodel.PageNumber = (pagingparametermodel.PageNumber == 0) ? 1 : pagingparametermodel.PageNumber;
            pagingparametermodel.PageSize = (pagingparametermodel.PageSize == 0) ? 20 : pagingparametermodel.PageSize;

            var source = listQuery.AsQueryable().AsNoTracking();

            // Parameter is passed from Query string if it is null then it default Value will be pageNumber:1  
            int CurrentPage = pagingparametermodel.PageNumber;

            // Parameter is passed from Query string if it is null then it default Value will be pageSize:20  
            int PageSize = pagingparametermodel.PageSize;
            pagedResult.TotalCount = await source.CountAsync();//
            pagedResult.Result = await source.Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToListAsync();

            return pagedResult;
        }

        private string TenantId;
        private string UserName;



        /// <summary>
        /// Get All For Lookups and
        /// </summary>
        /// <param name="IgnoreTenant"></param>
        /// <returns></returns>
        public IQueryable<T> GetAll(bool IgnoreTenant)
        {
            if (IgnoreTenant)
            {
                return _set.Where(x => x.IsDeleted == false).OrderByDescending(x => x.CreatedDate).AsNoTracking();
            }
            else
            {
                return _set.OrderByDescending(x => x.CreatedDate).AsNoTracking();

            }
        }

        private void InternalUpdate(T entity)
        {
            if (Validator.IsValid(entity))
            {
                EntityEntry<T> trackedEntity = GetTrackedEntity(entity);
                if (trackedEntity != null)
                {
                    trackedEntity.CurrentValues.SetValues(entity);
                    entity = trackedEntity.Entity;
                }
                else
                {
                    _context.Update(entity);
                }
            }
            else
            {
                StringBuilder exceptionMsgs = new StringBuilder();
                List<string> errorMsgs = Validator.GetInvalidMessages(entity).ToList();
                foreach (var errmsg in errorMsgs)
                {
                    exceptionMsgs.Append(errmsg);
                    exceptionMsgs.Append("/n");
                }
                throw new Exception(exceptionMsgs.ToString());
            }
        }

        private EntityEntry<T> GetTrackedEntity(T entity)
        {
            var idProperty = entity.GetType().GetProperty("Id");
            var idValue = idProperty.GetValue(entity);
            var trackedEntity = _context.ChangeTracker
                .Entries<T>()
                .FirstOrDefault(tracked => tracked.Property("Id").CurrentValue.Equals(idValue));
            return trackedEntity;
        }

        private class Validator
        {
            #region public Methods

            static void SetValidatableObjectErrors<TEntity>(TEntity item, List<string> errors) where TEntity : class
            {
                if (typeof(IValidatableObject).IsAssignableFrom(typeof(TEntity)))
                {
                    var validationContext = new ValidationContext(item, null, null);

                    var validationResults = ((IValidatableObject)item).Validate(validationContext);

                    errors.AddRange(validationResults.Select(vr => vr.ErrorMessage));
                }
            }

            //static void SetValidationAttributeErrors<TEntity>(TEntity item, List<string> errors) where TEntity : class
            //{
            //    TypeDescriptor.AddProvider(new AssociatedMetadataTypeTypeDescriptionProvider(typeof(TEntity)),
            //                   typeof(TEntity));                 

            //    var result = from property in TypeDescriptor.GetProperties(item).Cast<PropertyDescriptor>()
            //                 from attribute in property.Attributes.OfType<ValidationAttribute>()
            //                 where !attribute.IsValid(property.GetValue(item))
            //                 select attribute.FormatErrorMessage(property.DisplayName);

            //    if (result != null
            //        &&
            //        result.Any())
            //    {
            //        errors.AddRange(result);
            //    }
            //}

            #endregion

            #region IEntityValidator Members

            public static bool IsValid<TEntity>(TEntity item) where TEntity : class
            {

                if (item == null)
                    return false;

                List<string> validationErrors = new List<string>();

                SetValidatableObjectErrors(item, validationErrors);
                //SetValidationAttributeErrors(item, validationErrors);

                return !validationErrors.Any();
            }

            public static IEnumerable<string> GetInvalidMessages<TEntity>(TEntity item) where TEntity : class
            {
                if (item == null)
                    return null;

                List<string> validationErrors = new List<string>();

                SetValidatableObjectErrors(item, validationErrors);
                //SetValidationAttributeErrors(item, validationErrors);

                return validationErrors;
            }

            #endregion
        }
    }

}
