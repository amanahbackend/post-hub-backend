﻿using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class DriverAttachmentConfiguration : IEntityTypeConfiguration<DriverAttachment>
    {
        public void Configure(EntityTypeBuilder<DriverAttachment> configuration)
        {
        }
    }
}
