﻿using Amanah.Posthub.DATA.Extensions;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class DriverRegistrationConfiguration : IEntityTypeConfiguration<DriverRegistration>
    {
        public void Configure(EntityTypeBuilder<DriverRegistration> configuration)
        {
            configuration.Property(o => o.UserName)
                .IsRequired();

            configuration.HasOne(o => o.ApprovedORejectedByUser)
                .WithMany()
                .HasForeignKey(o => o.ApprovedORejectedByUserId)
                .OnDelete(DeleteBehavior.Restrict);
            configuration.HasOne(o => o.Driver)
                .WithMany()
                .HasForeignKey(o => o.DriverId)
                .OnDelete(DeleteBehavior.Restrict);
            configuration.HasOne(o => o.Country)
                .WithMany()
                .HasForeignKey(o => o.CountryId)
                .OnDelete(DeleteBehavior.Restrict);
            configuration.HasOne(o => o.TransportType)
                .WithMany()
                .HasForeignKey(o => o.TransportTypeId)
                .OnDelete(DeleteBehavior.Restrict);
            configuration.HasMany(o => o.DriverAttachments)
                .WithOne()
                .HasForeignKey(o => o.DriverRegistrationId)
                .OnDelete(DeleteBehavior.Cascade);


            configuration.Property(x => x.PlatFormZones).HasJsonConversion();
        }

    }
}
