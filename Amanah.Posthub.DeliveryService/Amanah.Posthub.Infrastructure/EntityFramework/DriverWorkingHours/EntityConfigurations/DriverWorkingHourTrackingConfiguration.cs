﻿
using Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class DriverWorkingHourTrackingConfiguration
        : IEntityTypeConfiguration<DriverWorkingHourTracking>
    {
        public void Configure(EntityTypeBuilder<DriverWorkingHourTracking> DriverWorkingHourTrackingConfiguration)
        {
            DriverWorkingHourTrackingConfiguration.HasKey(x => x.Id);

        }
    }
}
