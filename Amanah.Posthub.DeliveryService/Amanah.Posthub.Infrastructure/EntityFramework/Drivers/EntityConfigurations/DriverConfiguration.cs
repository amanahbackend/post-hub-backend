﻿using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class DriverConfiguration
        : IEntityTypeConfiguration<Driver>
    {
        public void Configure(EntityTypeBuilder<Driver> builder)
        {
            builder.HasMany(driver => driver.DriverLoginRequests)
                .WithOne(driver => driver.Driver)
                .HasForeignKey(driver => driver.DriverId);

            builder.HasOne(driver => driver.User)
                .WithMany()
                .HasForeignKey(driver => driver.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<DriverCurrentLocation>(driver => driver.CurrentLocation)
                .WithOne(driverCurrentLocation => driverCurrentLocation.Driver)
                .HasForeignKey<DriverCurrentLocation>(driverCurrentLocation => driverCurrentLocation.Id);

            builder.OwnsOne(x => x.Address);


        }
    }
}
