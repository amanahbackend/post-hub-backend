﻿using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.Drivers.EntityConfigurations
{
    internal class DriverRateConfiguration : IEntityTypeConfiguration<DriverRate>
    {
        public void Configure(EntityTypeBuilder<DriverRate> builder)
        {
            builder.HasOne(rate => rate.Driver)
                .WithMany(driver => driver.DriverRates)
                .IsRequired()
                .HasForeignKey(rate => rate.DriverId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(rate => rate.Customer)
                .WithMany()
                .HasForeignKey(DriverRate => DriverRate.CustomerId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(rate => rate.Tasks)
                .WithMany(task => task.DriverRates)// this is strange
                .HasForeignKey(rate => rate.TasksId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
