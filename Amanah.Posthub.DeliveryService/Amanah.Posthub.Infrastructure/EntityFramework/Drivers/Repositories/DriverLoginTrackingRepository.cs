﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.Drivers.Repositories
{
    public class DriverLoginTrackingRepository : Repository<DriverloginTracking>, IDriverLoginTrackingRepository
    {
        private readonly ICurrentUser currentUser;

        public DriverLoginTrackingRepository(ApplicationDbContext dbContext,
            ICurrentUser currentUser) : base(dbContext)
        {
            this.currentUser = currentUser;
        }

        public async Task<DriverloginTracking> GetLastOrDefaultAsync(Expression<Func<DriverloginTracking, bool>> expression)
        {
            return await _dbContext.DriverloginTracking
                .Where(expression)
                .LastOrDefaultAsync();
        }

        public async Task UpdateAsync(List<int> driverIds)
        {
            var driverLoginTrackings = await _dbContext.DriverloginTracking
                .Where(driver => driverIds.Contains(driver.DriverId))
                .ToListAsync();
            foreach (var driverId in driverIds)
            {
                var driverLoginTracking = driverLoginTrackings.LastOrDefault(driver => driver.DriverId == driverId);
                if (driverLoginTracking != null)
                {
                    driverLoginTracking.LogoutDate = DateTime.UtcNow;
                    driverLoginTracking.LogoutActionBy = currentUser.UserName;
                    base.Update(driverLoginTracking);
                }
            }
        }

        public async Task UpdateAsync(int driverId, double latitude, double longitude)
        {
            var driverLoginTracking = await _dbContext.DriverloginTracking
               .Where(driver => driver.DriverId == driverId)
               .OrderBy(driver => driver.CreatedDate)
               .LastOrDefaultAsync();
            if (driverLoginTracking != null)
            {
                driverLoginTracking.LogoutLatitude = latitude;
                driverLoginTracking.LogoutLongitude = longitude;
                driverLoginTracking.LogoutDate = DateTime.UtcNow;
                driverLoginTracking.LogoutActionBy = currentUser.UserName;
                base.Update(driverLoginTracking);
            }
        }

    }
}
