﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.Drivers.Repositories
{
    public class DriverRepository : Repository<Driver>, IDriverRepository
    {
        public DriverRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async override Task<IReadOnlyCollection<Driver>> GetAllAsync(
            [NotNull] Expression<Func<Driver, bool>> predicate,
            params SortingField<Driver>[] sortingFields)
        {
            return await _dbContext.Driver
                .Where(predicate)
                .AddSorting(sortingFields)
                .ToListAsync();
        }

        public async Task<Driver> GetAsync(int id)
        {
            return await _dbContext.Driver
                 .Where(driver => driver.Id == id && !driver.IsDeleted)
                 .IgnoreQueryFilters()
                 .AsNoTracking()
                 .FirstOrDefaultAsync();
        }

        public async Task<Driver> GetWithGeofencesAsync(int id)
        {
            return await _dbContext.Driver
                 .Include(driver => driver.DriverDeliveryGeoFences)
                 .Include(driver => driver.DriverPickUpGeoFences)
                 .Include(driver => driver.DriverFloors)
                 .Include(driver => driver.DriverExternalBranchs)
                 .Where(driver => driver.Id == id && !driver.IsDeleted)
                 .IgnoreQueryFilters()
                 .AsNoTracking()
                 .FirstOrDefaultAsync();
        }

        public async Task<Driver> GetWithGeofencesExpressAsync(int id)
        {
            return await _dbContext.Driver
                 .Include(driver => driver.DriverDeliveryGeoFences)
                 .Include(driver => driver.DriverPickUpGeoFences)
                 .Where(driver => driver.Id == id && !driver.IsDeleted)
                 .IgnoreQueryFilters()
                 .AsNoTracking()
                 .FirstOrDefaultAsync();
        }




        public async Task<Driver> GetWithCurrentLocationAsync(int id)
        {
            return await _dbContext.Driver
                 .Include(driver => driver.CurrentLocation)
                 .Include(driver => driver.DriverPickUpGeoFences)
                 .Where(driver => driver.Id == id && !driver.IsDeleted)
                 .IgnoreQueryFilters()
                 .AsNoTracking()
                 .FirstOrDefaultAsync();
        }

        public async Task<Driver> GetByUserIdAsync(string userId)
        {
            return await _dbContext.Driver
                 .Where(driver => driver.UserId == userId)
                 .AsNoTracking()
                 .FirstOrDefaultAsync();
        }

        public void Update(
            Driver driver,
            List<DriverPickUpGeoFence> driverPickUpGeoFences = null,
            List<DriverDeliveryGeoFence> driverDeliveryGeoFences = null,
            List<DriverFloor> driverFloors=null,
            List<DriverExternalBranch> driverBranches=null)
        {
            if (driver.DriverPickUpGeoFences != null && driver.DriverPickUpGeoFences.Any())
            {
                _dbContext.RemoveRange(driver.DriverPickUpGeoFences);
            }
            if (driver.DriverDeliveryGeoFences != null && driver.DriverDeliveryGeoFences.Any())
            {
                _dbContext.RemoveRange(driver.DriverDeliveryGeoFences);
            }
            if (driver.DriverFloors != null && driver.DriverFloors.Any())
            {
                _dbContext.RemoveRange(driver.DriverFloors);
            }
            if (driver.DriverExternalBranchs != null && driver.DriverExternalBranchs.Any())
            {
                var driverExternalBranchs = _dbContext.DriverExternalBranchs.Where(b => b.DriverId == driver.Id);
                _dbContext.DriverExternalBranchs.RemoveRange(driverExternalBranchs);
            }
            driver.DriverPickUpGeoFences = driverPickUpGeoFences;
            driver.DriverDeliveryGeoFences = driverDeliveryGeoFences;
            driver.DriverFloors = driverFloors;
            driver.DriverExternalBranchs = driverBranches;

            base.Update(driver);
        }

        public async Task Remove(List<int> driverIds)
        {
            var drivers = await GetAllAsync(driver => driverIds.Contains(driver.Id));
            if (drivers != null && drivers.Any())
            {
                drivers.ToList()
                    .ForEach(driver => driver.SetIsDeleted(true));
                UpdateRange(drivers);
            }
        }
        public async Task Remove(int driverId)
        {
            var driver = await GetByIdAsync(driverId);
            if (driver != null)
            {
                driver.SetIsDeleted(true);
                Update(driver);
            }
        }


        public async Task<bool> ChangeDriverStatusByUserIdAsync(string userId, int statusId)
        {
            var driver = await GetByUserIdAsync(userId);
            return ChangeDriverStatusByUserIdAsync(ref driver, statusId);
        }
        public async Task<bool> ChangeDriverStatusByIdAsync(int id, int statusId)
        {
            var driver = await GetAsync(id);
            return ChangeDriverStatusByUserIdAsync(ref driver, statusId);
        }
        public bool ChangeDriverStatusByUserIdAsync(ref Driver driver, int statusId)
        {
            driver.AgentStatusId = statusId;
            if (statusId != (int)AgentStatusesEnum.Available)
            {
                driver.ReachedTime = null;
                driver.ClubbingTimeExpiration = null;
                driver.IsInClubbingTime = false;
                driver.MaxOrdersWeightsCapacity = 1;
                driver.MinOrdersNoWait = 1;
            }
            Update(driver);
            return true;
        }


        public async Task<IQueryable<Manager>> GetDriverManagersByTeamIdAsync(int teamId)
        {
            var managerIds = await _dbContext.TeamManager
                .IgnoreQueryFilters()
                .Where(x => x.TeamId == teamId)
                .Select(x => x.ManagerId)
                .ToListAsync();

            return _dbContext.Manager
                .IgnoreQueryFilters()
                .Where(manager => !manager.IsDeleted && managerIds.Contains(manager.Id));
        }

        public async Task<bool> IsDriverAsync(string UserId)
        {
            return await GetAllIQueryable()
                .IgnoreQueryFilters()
                .AnyAsync(user => user.UserId == UserId && !user.User.IsDeleted);
        }

    }
}
