﻿using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class GeoFenceConfiguration
        : IEntityTypeConfiguration<GeoFence>
    {
        public void Configure(EntityTypeBuilder<GeoFence> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(o => o.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(o => o.Description)
               .HasMaxLength(100)
               .IsRequired();

            builder.HasMany(x => x.GeoFenceLocations)
                .WithOne(x => x.GeoFence)
                .HasForeignKey(x => x.GeoFenceId);

            builder.HasMany(x => x.GeoFenceBlocks)
                .WithOne(x => x.GeoFence)
                .HasForeignKey(x => x.GeoFenceId);

        }
    }
}
