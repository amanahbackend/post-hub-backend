﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.GeoFences.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.GeoFences.Repositories
{
    public class GeofenceRepository : Repository<GeoFence>, IGeofenceRepository
    {
        public GeofenceRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<List<GeoFence>> GetWithGeofencLocationAsync(Expression<Func<GeoFence, bool>> expression)
        {
            return await _dbContext.GeoFence
                 .Include(geofence => geofence.GeoFenceLocations)
                 .Where(expression)
                  .ToListAsync();
        }
    }
}
