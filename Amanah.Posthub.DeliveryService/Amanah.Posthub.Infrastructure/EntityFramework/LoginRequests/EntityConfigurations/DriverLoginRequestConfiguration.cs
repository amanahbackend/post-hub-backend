﻿
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class DriverLoginRequestConfiguration
        : IEntityTypeConfiguration<DriverLoginRequest>
    {
        public void Configure(EntityTypeBuilder<DriverLoginRequest> DriverLoginRequestConfiguration)
        {
            DriverLoginRequestConfiguration.HasKey(x => x.Id);

            DriverLoginRequestConfiguration.Property(o => o.Status)
                .IsRequired();

            DriverLoginRequestConfiguration.Property(o => o.DriverId)
               .IsRequired();
            DriverLoginRequestConfiguration.Property(o => o.Token)
              .IsRequired();




        }
    }
}
