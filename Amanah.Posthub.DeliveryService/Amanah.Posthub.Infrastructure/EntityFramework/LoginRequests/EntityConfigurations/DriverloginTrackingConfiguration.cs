﻿
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class DriverloginTrackingConfiguration
        : IEntityTypeConfiguration<DriverloginTracking>
    {
        public void Configure(EntityTypeBuilder<DriverloginTracking> driverloginTrackingConfiguration)
        {
            driverloginTrackingConfiguration.HasKey(x => x.Id);

        }
    }
}
