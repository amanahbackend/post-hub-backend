﻿
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class ManagerConfiguration
        : IEntityTypeConfiguration<Manager>
    {
        public void Configure(EntityTypeBuilder<Manager> ManagerConfiguration)
        {


            ManagerConfiguration.HasKey(x => x.Id);

            ManagerConfiguration
             .HasOne(x => x.Branch);

            ManagerConfiguration.OwnsOne(x => x.Address);


            ManagerConfiguration
             .HasOne(x => x.Nationality);

            ManagerConfiguration
               .HasOne(x => x.ServiceSector)
               .WithMany(x => x.Managers)
               .HasForeignKey(x => x.ServiceSectorId);

            ManagerConfiguration
              .HasOne(x => x.Department)
              .WithMany(x => x.Managers)
              .HasForeignKey(x => x.DepartmentId);

        }
    }
}
