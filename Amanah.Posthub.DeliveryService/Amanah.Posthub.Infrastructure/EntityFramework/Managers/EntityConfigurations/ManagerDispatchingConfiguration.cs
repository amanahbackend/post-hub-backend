﻿
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class ManagerDispatchingConfiguration
        : IEntityTypeConfiguration<ManagerDispatching>
    {
        public void Configure(EntityTypeBuilder<ManagerDispatching> ManagerDispatchingConfiguration)
        {
            ManagerDispatchingConfiguration.HasKey(x => x.Id);

            ManagerDispatchingConfiguration.Property(o => o.DesignationName)
                .HasMaxLength(100)
                .IsRequired();
        }
    }
}
