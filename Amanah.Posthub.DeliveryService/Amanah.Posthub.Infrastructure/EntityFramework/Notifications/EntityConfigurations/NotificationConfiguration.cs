﻿
using Amanah.Posthub.Service.Domain.Notifications.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class NotificationConfiguration : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.Property(o => o.Body).IsRequired();

            builder.HasOne<ApplicationUser>()
                .WithMany()
                .HasForeignKey(Notification => Notification.FromUserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<ApplicationUser>()
                .WithMany()
                .HasForeignKey(Notification => Notification.ToUserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
