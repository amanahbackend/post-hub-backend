﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Notifications.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Notifications.Entities;

namespace Amanah.Posthub.Infrastructure.EntityFramework.SubTasks.Repositories
{
    public class NotificationRepository : Repository<Notification>, INotificationRepository
    {

        public NotificationRepository(
            ApplicationDbContext dbContext) : base(dbContext)
        {
        }

    }
}
