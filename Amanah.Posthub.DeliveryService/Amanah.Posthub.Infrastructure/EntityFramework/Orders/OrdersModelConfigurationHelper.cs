﻿using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Microsoft.EntityFrameworkCore;

namespace Amanah.Posthub.Infrastructure.EntityFramework.Orders
{
    public static class OrdersModelConfigurationHelper
    {
        public static void ApplyOrderModelConfigurations(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Order>().Property(p => p.TotalWeight).HasColumnType("decimal(7, 3)");

            modelBuilder.Entity<MailItem>().HasOne(c => c.ConsigneeInfo).WithOne().OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<MailItem>().HasOne(c => c.ReciverInfo).WithOne().OnDelete(DeleteBehavior.NoAction);

            //modelBuilder.Entity<OrdersStatusLog>().HasOne(c => c.OldStatus).WithOne().OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<OrdersStatusLog>().HasOne(c => c.OrderStatus).WithOne().OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<MailItemsStatusLog>().HasOne(c => c.MailItemStatus).WithOne().OnDelete(DeleteBehavior.NoAction);
            // modelBuilder.Entity<MailItemsStatusLog>().HasOne(c => c.OldStatus).WithOne().OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Order>().OwnsOne(c => c.PickupAddress);
            modelBuilder.Entity<Contact>().OwnsOne(c => c.Address);
            modelBuilder.Entity<DirectOrder>().OwnsOne(c => c.DropOffAddress);
            modelBuilder.Entity<MailItem>().OwnsOne(c => c.PickUpAddress);
            modelBuilder.Entity<MailItem>().OwnsOne(c => c.DropOffAddress);

            //#region Seed Default data
            SeedWeightUOMs(modelBuilder);
            SeedMailItemStatus(modelBuilder);
            SeedPaymentMethods(modelBuilder);
            ////this.SeedAccountStatus(modelBuilder);
            SeedAddressTag(modelBuilder);
            //this.SeedCollectionMethod(modelBuilder);
            //this.SeedContactFunction(modelBuilder);
            SeedContractStatus(modelBuilder);
            //this.SeedCountry(modelBuilder);
            SeedCreditStatus(modelBuilder);
            SeedCurrency(modelBuilder);
            //SeedDriverDutyStatus(modelBuilder);
            //this.SeedGender(modelBuilder);
            SeedLanguage(modelBuilder);
            SeedLengthUOM(modelBuilder);
            SeedMailItemType(modelBuilder);
            SeedOrderRecurency(modelBuilder);
            //SeedOrderStatus(modelBuilder);
            SeedOrderType(modelBuilder);
            ///warning no need for this seeds 
            //SeedRole(modelBuilder);
            //this.SeedServicSector(modelBuilder);
            SeedShipServiceType(modelBuilder);
            //SeedUserAccessType(modelBuilder);
            //SeedUserAccountStatus(modelBuilder);
            SeedWorkOrderStatus(modelBuilder);
            //SeedIndustry(modelBuilder);
            SeedCourierShipmentService(modelBuilder);
            //#endregion
            SeedItemDeleveryType(modelBuilder);
            SeedPriceQuotationStatus(modelBuilder);

            SeedContractType(modelBuilder);

        }


        #region Seed Default Data
        private static void SeedAccountStatus(ModelBuilder builder)
        {
            var accountStatusActive = new AccountStatus() { Id = 1, Name_en = "active", Name_ar = "مفعل" };
            builder.Entity<AccountStatus>().HasData(accountStatusActive);

            var accountStatusInactive = new AccountStatus() { Id = 2, Name_en = "inactive", Name_ar = "غير مفعل" };
            builder.Entity<AccountStatus>().HasData(accountStatusInactive);

            var accountStatusBlocked = new AccountStatus() { Id = 3, Name_en = "blocked", Name_ar = "موقوف" };
            builder.Entity<AccountStatus>().HasData(accountStatusBlocked);

            var accountStatusBlockListed = new AccountStatus() { Id = 4, Name_en = "blockListed", Name_ar = "محظور" };
            builder.Entity<AccountStatus>().HasData(accountStatusBlockListed);

        }
        private static void SeedAddressTag(ModelBuilder builder)
        {
            builder.Entity<AddressTag>().HasData(
                new AddressTag() { Id = 1, Name = "منزل", IsDeleted = false, IsActive = true },
                new AddressTag() { Id = 2, Name = "عمل", IsDeleted = false, IsActive = true },
                new AddressTag() { Id = 3, Name = "مؤقت", IsDeleted = false, IsActive = true },
                new AddressTag() { Id = 4, Name = "جديد", IsDeleted = false, IsActive = true }
            );
        }
        //private static void SeedCollectionMethod(ModelBuilder builder)
        //{
        //    var collectionMethodCash = new CollectionMethod(){ Id =1,Name = "Cash", IsDeleted = false, IsActive = true };
        //    builder.Entity<CollectionMethod>().HasData(collectionMethodCash);

        //    var collectionMethodPrepaid = new CollectionMethod(){ Id =2,Name = "Prepaid Stock", IsDeleted = false, IsActive = true };
        //    builder.Entity<CollectionMethod>().HasData(collectionMethodPrepaid);

        //    var collectionMethodAccount = new CollectionMethod(){ Id =3,Name = "Account", IsDeleted = false, IsActive = true };
        //    builder.Entity<CollectionMethod>().HasData(collectionMethodAccount);
        //}
        //private static void SeedContactFunction(ModelBuilder builder)
        //{
        //    var contactFunctionSPOk = new ContactFunction(){ Id =1,Name = "SPOk", IsDeleted = false, IsActive = true };
        //    builder.Entity<ContactFunction>().HasData(contactFunctionSPOk);

        //    var contactFunctionContracts = new ContactFunction(){ Id =2,Name = "contracts", IsDeleted = false, IsActive = true };
        //    builder.Entity<ContactFunction>().HasData(contactFunctionContracts);

        //    var contactFunctionInvoices = new ContactFunction(){ Id =3,Name = "invoices", IsDeleted = false, IsActive = true };
        //    builder.Entity<ContactFunction>().HasData(contactFunctionInvoices);

        //    var contactFunctionBranch = new ContactFunction(){ Id =4,Name = "branch", IsDeleted = false, IsActive = true };
        //    builder.Entity<ContactFunction>().HasData(contactFunctionBranch);

        //    var contactFunctionPickup = new ContactFunction(){ Id =5,Name = "pickup", IsDeleted = false, IsActive = true };
        //    builder.Entity<ContactFunction>().HasData(contactFunctionPickup);



        //}
        private static void SeedContractStatus(ModelBuilder builder)
        {
            var contractStatusWaitingApproval = new ContractStatus() { Id = 1, Name = "WaitingApproval", IsDeleted = false, Description = "", IsActive = true };
            builder.Entity<ContractStatus>().HasData(contractStatusWaitingApproval);

            var contractStatusActive = new ContractStatus() { Id = 2, Name = "Active", IsDeleted = false, Description = "", IsActive = true };
            builder.Entity<ContractStatus>().HasData(contractStatusActive);

            var contractStatusInactive = new ContractStatus() { Id = 3, Name = "Inactive", IsDeleted = false, Description = "", IsActive = true };
            builder.Entity<ContractStatus>().HasData(contractStatusInactive);

            var contractStatusOnHold = new ContractStatus() { Id = 4, Name = "OnHold", IsDeleted = false, Description = "", IsActive = true };
            builder.Entity<ContractStatus>().HasData(contractStatusOnHold);

            var contractStatusBlocked = new ContractStatus() { Id = 5, Name = "Blocked", IsDeleted = false, Description = "", IsActive = true };
            builder.Entity<ContractStatus>().HasData(contractStatusBlocked);

            var contractStatusCanceled = new ContractStatus() { Id = 6, Name = "Canceled", IsDeleted = false, Description = "", IsActive = true };
            builder.Entity<ContractStatus>().HasData(contractStatusCanceled);

            var contractStatusInRenewal = new ContractStatus() { Id = 7, Name = "InRenewal", IsDeleted = false, Description = "", IsActive = true };
            builder.Entity<ContractStatus>().HasData(contractStatusInRenewal);

            var contractStatusClosed = new ContractStatus() { Id = 8, Name = "Closed", IsDeleted = false, Description = "", IsActive = true };
            builder.Entity<ContractStatus>().HasData(contractStatusClosed);
        }
        //private static void SeedCountry(ModelBuilder builder)
        //{
        //    var countryKWT = new Country(){ Id =1,Name = "الكويت", Code = "KWT"};
        //    builder.Entity<Country>().HasData(countryKWT);

        //    var countryEGY = new Country(){ Id =2,Name = "Egypt", Code = "EGY" };
        //    builder.Entity<Country>().HasData(countryEGY);

        //    var countrySAR = new Country(){ Id =3,Name = "saudi Arabia", Code = "SAR" };
        //    builder.Entity<Country>().HasData(countrySAR);

        //    var countryUAE = new Country(){ Id =4,Name = "United Arab Emarate", Code = "UAE"};
        //    builder.Entity<Country>().HasData(countryUAE);
        //}
        private static void SeedCreditStatus(ModelBuilder builder)
        {
            var creditStatusCredit = new CreditStatus() { Id = 1, Name = "إئتمان", IsDeleted = false, IsActive = true };
            builder.Entity<CreditStatus>().HasData(creditStatusCredit);

            var creditStatusCash = new CreditStatus() { Id = 2, Name = "نقدي", IsDeleted = false, IsActive = true };
            builder.Entity<CreditStatus>().HasData(creditStatusCash);

            var creditStatusDepodit = new CreditStatus() { Id = 3, Name = "إيداع", IsDeleted = false, IsActive = true };
            builder.Entity<CreditStatus>().HasData(creditStatusDepodit);

            var creditStatusBlackListed = new CreditStatus() { Id = 4, Name = "محظور", IsDeleted = false, IsActive = true };
            builder.Entity<CreditStatus>().HasData(creditStatusBlackListed);


        }
        private static void SeedCurrency(ModelBuilder builder)
        {
            var currencyUSD = new Currency() { Id = 1, Code_ar = "دولار امريكى", IsDeleted = false, Code_en = "USD", IsActive = true };
            builder.Entity<Currency>().HasData(currencyUSD);

            var currencyGBP = new Currency() { Id = 2, Code_ar = "جنيه استرلينى", IsDeleted = false, Code_en = "GBP", IsActive = true };
            builder.Entity<Currency>().HasData(currencyGBP);

            var currencyKWD = new Currency() { Id = 3, Code_ar = "دينار كويتى", IsDeleted = false, Code_en = "KWD", IsActive = true };
            builder.Entity<Currency>().HasData(currencyKWD);

            var currencySAR = new Currency() { Id = 4, Code_ar = "ريا سعودى", IsDeleted = false, Code_en = "SAR", IsActive = true };
            builder.Entity<Currency>().HasData(currencySAR);

            var currencyEUR = new Currency() { Id = 5, Code_ar = "يورو", IsDeleted = false, Code_en = "EUR", IsActive = true };
            builder.Entity<Currency>().HasData(currencyEUR);
        }
        //private static void SeedDriverDutyStatus(ModelBuilder builder)
        //{
        //    var driverOnDuty = new DriverDutyStatus() { Id = 1, Name = "OnDuty", IsDeleted = false, IsActive = true };
        //    builder.Entity<DriverDutyStatus>().HasData(driverOnDuty);

        //    var driverAway = new DriverDutyStatus() { Id = 2, Name = "Away", IsDeleted = false, IsActive = true };
        //    builder.Entity<DriverDutyStatus>().HasData(driverAway);

        //    var driverOffDuty = new DriverDutyStatus() { Id = 3, Name = "OffDuty", IsDeleted = false, IsActive = true };
        //    builder.Entity<DriverDutyStatus>().HasData(driverOffDuty);

        //    var driverGoing = new DriverDutyStatus() { Id = 4, Name = "Going-off-duty", IsDeleted = false, IsActive = true };
        //    builder.Entity<DriverDutyStatus>().HasData(driverGoing);

        //    var driverOnBreak = new DriverDutyStatus() { Id = 5, Name = "OnBreak", IsDeleted = false, IsActive = true };
        //    builder.Entity<DriverDutyStatus>().HasData(driverOnBreak);

        //}
        //private static void SeedGender(ModelBuilder builder)
        //{

        //    var genderMale = new Gender(){ Id =1,Name_en = "Male"};
        //    builder.Entity<Gender>().HasData(genderMale);

        //    var genderFemale = new Gender(){ Id =2,Name_en = "Female"};
        //    builder.Entity<Gender>().HasData(genderFemale);
        //}
        private static void SeedLanguage(ModelBuilder builder)
        {
            var langAr = new Language() { Id = 1, Name = "العربيه", IsDeleted = false, Code = "ar-eg", IsActive = true };
            builder.Entity<Language>().HasData(langAr);

            var langEn = new Language() { Id = 2, Name = "English", IsDeleted = false, Code = "en-us", IsActive = true };
            builder.Entity<Language>().HasData(langEn);
        }
        private static void SeedLengthUOM(ModelBuilder builder)
        {
            var lengthUOMCm = new LengthUOM() { Id = 1, Name = "centemeter", IsDeleted = false, Code = "Cm", IsActive = true };
            builder.Entity<LengthUOM>().HasData(lengthUOMCm);

            var lengthUOMInch = new LengthUOM() { Id = 2, Name = "inches", IsDeleted = false, Code = "Inch", IsActive = true };
            builder.Entity<LengthUOM>().HasData(lengthUOMInch);

            var lengthUOMFeet = new LengthUOM() { Id = 3, Name = "feet", IsDeleted = false, Code = "Feet", IsActive = true };
            builder.Entity<LengthUOM>().HasData(lengthUOMFeet);
        }
        private static void SeedMailItemType(ModelBuilder builder)
        {
            var mailItemTypeRoot = new MailItemType() { Id = 1, Name_en = "Root", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemType>().HasData(mailItemTypeRoot);

            var mailItemTypeLetter = new MailItemType() { Id = 2, Name_en = "Letter", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemType>().HasData(mailItemTypeLetter);

            var mailItemTypeFlat = new MailItemType() { Id = 3, Name_en = "Flat", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemType>().HasData(mailItemTypeFlat);

            var mailItemTypeCard = new MailItemType() { Id = 4, Name_en = "Post Card", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemType>().HasData(mailItemTypeCard);

            var mailItemTypeParcel = new MailItemType() { Id = 5, Name_en = "Parcel", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemType>().HasData(mailItemTypeParcel);

            var mailItemTypeCommodity = new MailItemType() { Id = 6, Name_en = "Commodity", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemType>().HasData(mailItemTypeCommodity);
        }
        private static void SeedOrderRecurency(ModelBuilder builder)
        {
            var orderRecurencyDaily = new OrderRecurency() { Id = 1, Name = "Daily", IsDeleted = false, Code = "", IsActive = true };
            builder.Entity<OrderRecurency>().HasData(orderRecurencyDaily);

            var orderRecurencyWeekly = new OrderRecurency() { Id = 2, Name = "Weekly", IsDeleted = false, Code = "", IsActive = true };
            builder.Entity<OrderRecurency>().HasData(orderRecurencyWeekly);

            var orderRecurencyBiWeekly = new OrderRecurency() { Id = 3, Name = "BiWeekly", IsDeleted = false, Code = "", IsActive = true };
            builder.Entity<OrderRecurency>().HasData(orderRecurencyBiWeekly);

            var orderRecurencyMonthly = new OrderRecurency() { Id = 4, Name = "Monthly", IsDeleted = false, Code = "", IsActive = true };
            builder.Entity<OrderRecurency>().HasData(orderRecurencyMonthly);

            var orderRecurencyAtDayofMonth = new OrderRecurency() { Id = 5, Name = "AtDayofMonth", IsDeleted = false, Code = "", IsActive = true };
            builder.Entity<OrderRecurency>().HasData(orderRecurencyAtDayofMonth);

            var orderRecurencyAtDayofWeek = new OrderRecurency() { Id = 6, Name = "AtDayofWeek", IsDeleted = false, Code = "", IsActive = true };
            builder.Entity<OrderRecurency>().HasData(orderRecurencyAtDayofWeek);


        }
        private static void SeedOrderStatus(ModelBuilder builder)
        {
            var orderStatusRegistered = new OrderStatus() { Id = 1, Name = "Registered", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusRegistered);

            var orderStatusAssigned = new OrderStatus() { Id = 2, Name = "Assigned", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusAssigned);

            var orderStatusDriver = new OrderStatus() { Id = 3, Name = "Driver at Pickup", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusDriver);

            var orderStatusPicked = new OrderStatus() { Id = 4, Name = "Picked-up", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusPicked);

            var orderStatusReceived = new OrderStatus() { Id = 5, Name = "Received", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusReceived);

            var orderStatusUpload = new OrderStatus() { Id = 6, Name = "In-upload", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusUpload);

            var orderStatusUploaded = new OrderStatus() { Id = 7, Name = "Uploaded", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusUploaded);

            var orderStatusSorting = new OrderStatus() { Id = 8, Name = "In-Sorting", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusSorting);

            var orderStatusSorted = new OrderStatus() { Id = 9, Name = "Sorted", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusSorted);

            var orderStatusHold = new OrderStatus() { Id = 10, Name = "On-Hold", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusHold);

            var orderStatusDispatched = new OrderStatus() { Id = 11, Name = "Dispatched", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusDispatched);

            var orderStatusDriverAtDel = new OrderStatus() { Id = 12, Name = "Driver at Delivery", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusDriverAtDel);

            var orderStatusDelivered = new OrderStatus() { Id = 13, Name = "Delivered", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusDelivered);

            var orderStatusClosedWithReturns = new OrderStatus() { Id = 14, Name = "ClosedWithReturns", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusClosedWithReturns);

            var orderStatusCanceled = new OrderStatus() { Id = 15, Name = "Canceled", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusCanceled);

            var orderStatusPassedToCorrier = new OrderStatus() { Id = 16, Name = "PassedToCorrier", IsDeleted = false, Code = "", Description = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(orderStatusPassedToCorrier);
        }
        private static void SeedPriceQuotationStatus(ModelBuilder builder)
        {

            var pQStatusSendToCust = new OrderStatus() { Id = 1, Name = "SendToCustomer", IsDeleted = false, Code = "", IsActive = true };
            builder.Entity<OrderStatus>().HasData(pQStatusSendToCust);
        }

        private static void SeedOrderType(ModelBuilder builder)
        {
            var orderTypeBusinessOrder = new OrderType() { Id = 1, Name = "تجاري", IsDeleted = false, IsActive = true };
            builder.Entity<OrderType>().HasData(orderTypeBusinessOrder);

            var orderTypeRegularOrder = new OrderType() { Id = 2, Name = "عادي", IsDeleted = false, IsActive = true };
            builder.Entity<OrderType>().HasData(orderTypeRegularOrder);
        }
        //private static void SeedRole(ModelBuilder builder)
        //{
        //    var roleSupAdmin = new Role() { Id = 1, Name = "مدير النظام", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleSupAdmin);

        //    var roleSysAdmin = new Role() { Id = 2, Name = "مشغل النظام", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleSysAdmin);

        //    var roleCSR = new Role() { Id = 3, Name = "خدمات عملاء", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleCSR);

        //    var roleSortClerk = new Role() { Id = 4, Name = "فراز", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleSortClerk);

        //    var roleDispatcher = new Role() { Id = 5, Name = "موزع", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleDispatcher);

        //    var roleAdministor = new Role() { Id = 6, Name = "إداري", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleAdministor);

        //    var roleAccountant = new Role() { Id = 7, Name = "محاسب", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleAccountant);

        //    var roleDriver = new Role() { Id = 8, Name = "سائق", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleDriver);

        //    var roleDelegate = new Role() { Id = 9, Name = "مندوب", IsDeleted = false, Code = "", Description = "", IsActive = true };
        //    builder.Entity<Role>().HasData(roleDelegate);
        //}
        // private static void SeedServicSector(ModelBuilder builder)
        //{
        //    var servicSectorGeneral = new ServiceSector(){ Id =1,Name_ar = " عام", Name_en = "general"};
        //    builder.Entity<ServiceSector>().HasData(servicSectorGeneral);

        //    var servicSectorPostLocal = new ServiceSector(){ Id =2,Name_ar = " بريد محلي", Name_en = "PostLocal" };
        //    builder.Entity<ServiceSector>().HasData(servicSectorPostLocal);

        //    var servicSectorPostIntl = new ServiceSector(){ Id =3,Name_ar = " بريد دولي", Name_en = "PostIntl"};
        //    builder.Entity<ServiceSector>().HasData(servicSectorPostIntl);

        //    var servicSectorShipLocal = new ServiceSector(){ Id =4,Name_ar = "شحن محلي", Name_en = "ShipLocal"};
        //    builder.Entity<ServiceSector>().HasData(servicSectorShipLocal);

        //    var servicSectorShipIntl = new ServiceSector(){ Id =5,Name_ar = "شحن دولي", Name_en = "ShipIntl" };
        //    builder.Entity<ServiceSector>().HasData(servicSectorShipIntl);

        //    var servicSectorD2D = new ServiceSector(){ Id =6,Name_ar = "شحن للباب", Name_en = "D2D"};
        //    builder.Entity<ServiceSector>().HasData(servicSectorD2D);

        //    var servicSectorTriggeredDirectMail = new ServiceSector(){ Id =7,Name_ar = " بريد تحت الطلب", Name_en = "triggered direct mail"};
        //    builder.Entity<ServiceSector>().HasData(servicSectorTriggeredDirectMail);

        //    var servicSectorVariableDataPrinting = new ServiceSector(){ Id =8,Name_ar = "طباعة وتوزيع", Name_en = "Variable Data Printing"};
        //    builder.Entity<ServiceSector>().HasData(servicSectorVariableDataPrinting);

        //    var servicSectorPackage = new ServiceSector(){ Id =9,Name_ar = "تغليف وتوزيع", Name_en = "Package Assembly & Kitting"};
        //    builder.Entity<ServiceSector>().HasData(servicSectorPackage);


        //    var servicSectorSelfMailer = new ServiceSector(){ Id =10,Name_ar = " بدون مظروف", Name_en = "Self Mailer" };
        //    builder.Entity<ServiceSector>().HasData(servicSectorSelfMailer);

        //    var servicSectorStatements = new ServiceSector(){ Id =11,Name_ar = " كشوف", Name_en = "Statements"};
        //    builder.Entity<ServiceSector>().HasData(servicSectorStatements);
        //}
        private static void SeedShipServiceType(ModelBuilder builder)
        {
            var shipServiceTypeIP = new ShipServiceType() { Id = 1, Name = " IP", IsDeleted = false, IsActive = true };
            builder.Entity<ShipServiceType>().HasData(shipServiceTypeIP);

            var shipServiceTypeIE = new ShipServiceType() { Id = 2, Name = " IE", IsDeleted = false, IsActive = true };
            builder.Entity<ShipServiceType>().HasData(shipServiceTypeIE);

            var shipServiceTypeIPF = new ShipServiceType() { Id = 3, Name = "  IPF", IsDeleted = false, IsActive = true };
            builder.Entity<ShipServiceType>().HasData(shipServiceTypeIPF);

            var shipServiceTypeIEE = new ShipServiceType() { Id = 4, Name = " IEE", IsDeleted = false, IsActive = true };
            builder.Entity<ShipServiceType>().HasData(shipServiceTypeIEE);
        }
        //private static void SeedUserAccessType(ModelBuilder builder)
        //{
        //    var userAccessTypeMobileAcess = new UserAccessType() { Id = 1, Name = "MobileAcess", IsDeleted = false, IsActive = true };
        //    builder.Entity<UserAccessType>().HasData(userAccessTypeMobileAcess);

        //    var userAccessTypeWebAcess = new UserAccessType() { Id = 2, Name = "WebAcess", IsDeleted = false, IsActive = true };
        //    builder.Entity<UserAccessType>().HasData(userAccessTypeWebAcess);
        //}
        //private static void SeedUserAccountStatus(ModelBuilder builder)
        //{
        //    var userAccountStatusWaitingApproval = new UserAccountStatus() { Id = 1, Name = "WaitingApproval", IsDeleted = false, IsActive = true };
        //    builder.Entity<UserAccountStatus>().HasData(userAccountStatusWaitingApproval);

        //    var userAccountStatusActive = new UserAccountStatus() { Id = 2, Name = "Active", IsDeleted = false, IsActive = true };
        //    builder.Entity<UserAccountStatus>().HasData(userAccountStatusActive);

        //    var userAccountStatusInactive = new UserAccountStatus() { Id = 3, Name = "Inactive", IsDeleted = false, IsActive = true };
        //    builder.Entity<UserAccountStatus>().HasData(userAccountStatusInactive);

        //    var userAccountStatusOnHold = new UserAccountStatus() { Id = 4, Name = "OnHold", IsDeleted = false, IsActive = true };
        //    builder.Entity<UserAccountStatus>().HasData(userAccountStatusOnHold);

        //    var userAccountStatusBlocked = new UserAccountStatus() { Id = 5, Name = "Blocked", IsDeleted = false, IsActive = true };
        //    builder.Entity<UserAccountStatus>().HasData(userAccountStatusBlocked);

        //    var userAccountStatusCanceled = new UserAccountStatus() { Id = 6, Name = " Canceled", IsDeleted = false, IsActive = true };
        //    builder.Entity<UserAccountStatus>().HasData(userAccountStatusCanceled);
        //}
        private static void SeedWorkOrderStatus(ModelBuilder builder)
        {
            var workOrderStatusNew = new WorkOrderStatus() { Id = 1, Name = "جديد", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusNew);

            var workOrderStatusOnFill = new WorkOrderStatus() { Id = 2, Name = "استكمال عتاصر", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusOnFill);

            var workOrderStatusOnHold = new WorkOrderStatus() { Id = 3, Name = "موقوف", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusOnHold);

            var workOrderStatusToDispatch = new WorkOrderStatus() { Id = 4, Name = "للتوزيع", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusToDispatch);

            var workOrderStatusDispatched = new WorkOrderStatus() { Id = 5, Name = "تم التوزيع", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusDispatched);

            var workOrderStatusRejected = new WorkOrderStatus() { Id = 6, Name = " مرفوض", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusRejected);

            var workOrderStatusOnList = new WorkOrderStatus() { Id = 7, Name = "في قائمة السائق", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusOnList);

            var workOrderStatusInProgress = new WorkOrderStatus() { Id = 8, Name = "مفتوح للعمل", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusInProgress);

            var workOrderStatusCanceled = new WorkOrderStatus() { Id = 9, Name = "ملغي", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusCanceled);

            var workOrderStatusClosed = new WorkOrderStatus() { Id = 10, Name = " مغلق", IsDeleted = false, IsActive = true };
            builder.Entity<WorkOrderStatus>().HasData(workOrderStatusClosed);
        }
        //private static void SeedIndustry(ModelBuilder builder)
        //{

        //    var industryBank = new Industry() { Id = 1, Name = "بنوك", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryBank);

        //    var industryCourt = new Industry() { Id = 2, Name = "محاكم", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryCourt);

        //    var industrySociety = new Industry() { Id = 3, Name = "مجمعات", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industrySociety);

        //    var industryClub = new Industry() { Id = 4, Name = "نوادي", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryClub);

        //    var industryLawyer = new Industry() { Id = 5, Name = "محامون", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryLawyer);

        //    var industryUnion = new Industry() { Id = 6, Name = " اتحادات", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryUnion);

        //    var industrySyndicate = new Industry() { Id = 7, Name = "نقابات", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industrySyndicate);

        //    var industryNGO = new Industry() { Id = 8, Name = "كيانات غير ربحيه", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryNGO);

        //    var industryAssosciation = new Industry() { Id = 9, Name = "جمعيات", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryAssosciation);

        //    var industryAdvertisement = new Industry() { Id = 10, Name = "دعاية", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryAdvertisement);

        //    var industryPublications = new Industry() { Id = 11, Name = "مطبوعات", IsDeleted = false, IsActive = true };
        //    builder.Entity<Industry>().HasData(industryPublications);


        //}
        private static void SeedMailItemStatus(ModelBuilder builder)
        {
            var mailItemStatusNew = new MailItemStatus() { Id = 1, Name = "طلب جديد", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusNew);

            var mailItemStatusoDispatched = new MailItemStatus() { Id = 2, Name = "تم توزي ع الطلب", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusoDispatched);

            var mailItemStatusoAssigned = new MailItemStatus() { Id = 3, Name = "تم التوزيع لسائق", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusoAssigned);

            var mailItemStatusoPicked = new MailItemStatus() { Id = 4, Name = "تم التقاط الطلب", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusoPicked);

            var mailItemStatusoToFill = new MailItemStatus() { Id = 5, Name = "لرفع بيانات العناصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusoToFill);

            var mailItemStatuoFilled = new MailItemStatus() { Id = 6, Name = " تم رفع بيانات العناصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatuoFilled);

            var mailItemStatusoToSort = new MailItemStatus() { Id = 7, Name = "  طلب تحت الفرز ", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusoToSort);

            var mailItemStatusoSorted = new MailItemStatus() { Id = 8, Name = "  تم فرز الطلب", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusoSorted);

            var mailItemStatusitemPickedUp = new MailItemStatus() { Id = 9, Name = "  تم التقاط العنصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusitemPickedUp);

            var mailItemStatusitemFilled = new MailItemStatus() { Id = 10, Name = "  تم رفع العنصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusitemFilled);

            var mailItemStatuitemToSort = new MailItemStatus() { Id = 11, Name = " العنصر تحت الفرز", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatuitemToSort);

            var mailItemStatusitemSorted = new MailItemStatus() { Id = 12, Name = "  تم فرز العنصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusitemSorted);

            var mailItemStatusitemLabeled = new MailItemStatus() { Id = 13, Name = "  تم عنونة العنصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusitemLabeled);

            var mailItemStatusitemPreDispatched = new MailItemStatus() { Id = 14, Name = "  العنصر تحت التوزيع", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusitemPreDispatched);

            var mailItemStatusitemDispatched = new MailItemStatus() { Id = 15, Name = "  تم توزيع العنصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusitemDispatched);

            var mailItemStatuitemToDelivery = new MailItemStatus() { Id = 16, Name = " العنصر تحت التسليم", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatuitemToDelivery);

            var mailItemStatusitemOnDelivery = new MailItemStatus() { Id = 17, Name = " جاري تسليم العنصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusitemOnDelivery);

            var mailItemStatusitemDelivered = new MailItemStatus() { Id = 18, Name = "  تم تسليم العنصر", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatusitemDelivered);

            var mailItemStatuitemOnHold = new MailItemStatus() { Id = 19, Name = "  العنصر موقوف", IsDeleted = false, IsActive = true };
            builder.Entity<MailItemStatus>().HasData(mailItemStatuitemOnHold);

            var mailItemStatuitemReturned = new MailItemStatus() { Id = 20, Name = "  العنصر مرتجع", IsDeleted = false, IsActive = true };

            builder.Entity<MailItemStatus>().HasData(mailItemStatuitemReturned);
        }
        private static void SeedWeightUOMs(ModelBuilder builder)
        {
            var uomKilogram = new WeightUOM() { Id = 1, Name_en = "Kilogram", Code = "KG", Description = "A unit of mass equal to one thousand grams.", IsActive = true };
            builder.Entity<WeightUOM>().HasData(uomKilogram);

            var uomGram = new WeightUOM() { Id = 2, Name_en = "Gram", Code = "G", Description = "", IsActive = true };
            builder.Entity<WeightUOM>().HasData(uomGram);

            var uomLb = new WeightUOM() { Id = 3, Name_en = "Pound", Code = "lb", Description = "is a unit of mass used in British imperial and United States customary systems of measurement", IsActive = true };
            builder.Entity<WeightUOM>().HasData(uomLb);

        }
        private static void SeedPaymentMethods(ModelBuilder builder)
        {
            var paymentMethodCash = new PaymentMethod() { Id = 1, Name = "نقدي", IsDeleted = false, IsActive = true };
            builder.Entity<PaymentMethod>().HasData(paymentMethodCash);

            var paymentMethodVisa = new PaymentMethod() { Id = 2, Name = "فيزا", IsDeleted = false, IsActive = true };
            builder.Entity<PaymentMethod>().HasData(paymentMethodVisa);

            var paymentMethodDeposit = new PaymentMethod() { Id = 3, Name = "إيداع", IsDeleted = false, IsActive = true };
            builder.Entity<PaymentMethod>().HasData(paymentMethodDeposit);

            var paymentMethodKNET = new PaymentMethod() { Id = 4, Name = "KNET", IsDeleted = false, IsActive = true };
            builder.Entity<PaymentMethod>().HasData(paymentMethodKNET);
        }

        private static void SeedCourierShipmentService(ModelBuilder builder)
        {
            var paymentMethodCash = new CourierShipmentService() { Id = 1, Name = "IP", IsDeleted = false, IsActive = true };
            builder.Entity<CourierShipmentService>().HasData(paymentMethodCash);

            var paymentMethodVisa = new CourierShipmentService() { Id = 2, Name = "IE", IsDeleted = false, IsActive = true };
            builder.Entity<CourierShipmentService>().HasData(paymentMethodVisa);

            var paymentMethodDeposit = new CourierShipmentService() { Id = 3, Name = "IPF", IsDeleted = false, IsActive = true };
            builder.Entity<CourierShipmentService>().HasData(paymentMethodDeposit);

            var paymentMethodKNET = new CourierShipmentService() { Id = 4, Name = "IEE", IsDeleted = false, IsActive = true };
            builder.Entity<CourierShipmentService>().HasData(paymentMethodKNET);
        }
        private static void SeedItemDeleveryType(ModelBuilder builder)
        {
            var drop = new ItemDeleveryType() { Id = 1, Name = "Drop", IsDeleted = false, IsActive = true, IsSystem = true };
            builder.Entity<ItemDeleveryType>().HasData(drop);
            var pickup = new ItemDeleveryType() { Id = 2, Name = "Pickup", IsDeleted = false, IsActive = true, IsSystem = true };
            builder.Entity<ItemDeleveryType>().HasData(pickup);
        }


        private static void SeedContractType(ModelBuilder builder)
        {
            var Unbounded = new ContractType() { Id = 1, Name = "Unbounded  Local Distribution Contrct", IsDeleted = false, IsActive = true, IsSystem = true };
            builder.Entity<ContractType>().HasData(Unbounded);
            var bounded = new ContractType() { Id = 2, Name = "Bounded  Local Distribution Contrct", IsDeleted = false, IsActive = true, IsSystem = true };
            builder.Entity<ContractType>().HasData(bounded);
        }

        #endregion


    }
}
