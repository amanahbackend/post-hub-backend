﻿using Amanah.Posthub.Service.Domain.PriceList.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class RFQConfiguration
        : IEntityTypeConfiguration<RFQ>
    {
        public void Configure(EntityTypeBuilder<RFQ> ManagerConfiguration)
        {

            ManagerConfiguration.Property(o => o.ServiceSectorId)
               .IsRequired();

            //ManagerConfiguration.Property(o => o.BusinessCustomerId)               
            //    .IsRequired();

            ManagerConfiguration.HasKey(x => x.Id);

            //ManagerConfiguration
            //  .HasOne(x => x.BusinessCustomer)
            //  .WithMany(x => x.Prices)
            //  .HasForeignKey(x => x.CustomerId);

        }
    }
}
