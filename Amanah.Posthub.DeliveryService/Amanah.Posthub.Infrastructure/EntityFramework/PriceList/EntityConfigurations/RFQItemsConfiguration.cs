﻿using Amanah.Posthub.Service.Domain.PriceList.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class RFQItemsConfiguration
        : IEntityTypeConfiguration<RFQItems>
    {
        public void Configure(EntityTypeBuilder<RFQItems> ManagerConfiguration)
        {
            ManagerConfiguration.HasKey(x => x.Id);

            ManagerConfiguration.Property(o => o.RFQId)
               .IsRequired();


            ManagerConfiguration
              .HasOne(x => x.RFQ)
              .WithMany(x => x.RFQItemPrices)
              .HasForeignKey(x => x.RFQId);

        }
    }
}
