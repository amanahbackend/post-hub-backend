﻿using Amanah.Posthub.Service.Domain.Resturants.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class RestaurantConfiguration : IEntityTypeConfiguration<Restaurant>
    {
        public void Configure(EntityTypeBuilder<Restaurant> RestaurantConfiguration)
        {
            RestaurantConfiguration.HasKey(x => x.Id);

            RestaurantConfiguration.Property(o => o.Name)
                .HasMaxLength(250)
                .IsRequired();
            //RestaurantConfiguration.HasMany(x => x.Branchs).WithOne(x => x.Restaurant).HasForeignKey(x => x.RestaurantId);

        }
    }
}
