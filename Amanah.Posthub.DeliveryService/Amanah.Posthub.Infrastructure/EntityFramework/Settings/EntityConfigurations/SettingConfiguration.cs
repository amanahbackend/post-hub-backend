﻿using Amanah.Posthub.DATA.DbContext;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class SettingConfiguration
        : IEntityTypeConfiguration<Setting>
    {
        public void Configure(EntityTypeBuilder<Setting> builder)
        {
            builder.Property(setting => setting.SettingKey)
                .HasMaxLength(StringLengths.Default);

            builder.HasIndex(Setting => new { Setting.SettingKey, Setting.Tenant_Id })
                .IsUnique()
                .HasFilter(null);
        }
    }
}
