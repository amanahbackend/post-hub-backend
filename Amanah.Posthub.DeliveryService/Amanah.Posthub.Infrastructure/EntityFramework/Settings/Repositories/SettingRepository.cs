﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.MainTasks.Repositories
{
    public class SettingRepository : Repository<Setting>, ISettingRepository
    {
        public SettingRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
        public async Task<List<Setting>> GetSettingByGroupIdAsync(string GroupId)
        {
            ///Return Setting by Key for specific tenant 
            return await _dbContext.Settings
                .Where(x => x.GroupId == GroupId)
                .ToListAsync();
        }
        /// <summary>
        /// Get Setting by key for current Tenant that come from token 
        /// </summary>
        /// <param name="settingkey">Setting Key </param>
        /// <returns>Setting Object </returns>
        public async Task<Setting> GetSettingByKeyAsync(string settingkey)
        {
            return await _dbContext.Settings
                .Where(x => x.SettingKey.Trim().ToLower() == settingkey.Trim().ToLower())
                .FirstOrDefaultAsync();
        }
        public IQueryable<Setting> GetSettingByKeyAsync(List<string> settingkey)
        {
            settingkey = settingkey.Select(x => x.Trim().ToLower()).ToList();
            return _dbContext.Settings
              .Where(x => settingkey.Contains(x.SettingKey.Trim().ToLower()));
        }
        public async Task<Setting> GetAsync(int id)
        {
            return await (_dbContext as ApplicationDbContext).Settings
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<int> GetAutoAllocationTypeidAsync()
        {
            var autoallocationSetting = await GetSettingByKeyAsync(SettingKeyConstant.SelectedAutoAllocationMethodName);
            var enableAutoallocationSetting = await GetSettingByKeyAsync(SettingKeyConstant.IsEnableAutoAllocation);
            int autoAllocationType = enableAutoallocationSetting == null
                ? (int)AutoAllocationTypeEnum.Manual
                : enableAutoallocationSetting.Value.ToLower() == "false"
                    ? (int)AutoAllocationTypeEnum.Manual
                    : autoallocationSetting == null
                        ? (int)AutoAllocationTypeEnum.Manual
                        : autoallocationSetting.Value == SettingKeyConstant.FirstInFirstOutMethod
                            ? (int)AutoAllocationTypeEnum.Fifo
                            : autoallocationSetting.Value == SettingKeyConstant.NearestAvailableMethod
                                ? (int)AutoAllocationTypeEnum.Nearest
                                : autoallocationSetting.Value == SettingKeyConstant.OneByOneMethod
                                    ? (int)AutoAllocationTypeEnum.OneByOne
                                    : (int)AutoAllocationTypeEnum.Manual;

            return autoAllocationType;
        }



        public async Task<bool> AddUserDefaultSettingsAsync(string tenantId, DefaultSettingValue defaultSetting, ResourceSet resourceSet)
        {
            //var resourceSet = BLL.Settings.Resources.SettingsKeys.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            if (resourceSet == null)
            {
                return false;
            }
            var settings = await CreateSettingListAsync(tenantId, resourceSet, defaultSetting);
            if (settings == null || !settings.Any())
            {
                return false;
            }
            await (_dbContext as ApplicationDbContext).Settings.AddRangeAsync(settings);
            return true;
        }

        private async Task<List<Setting>> CreateSettingListAsync(string tenantId, ResourceSet resourceSet, DefaultSettingValue defaultSetting)
        {
            var oldSettings = await (_dbContext as ApplicationDbContext).Settings
                .IgnoreQueryFilters()
                .Where(setting => setting.Tenant_Id == tenantId && !setting.IsDeleted)
                .ToListAsync();
            var settings = new List<Setting>();
            foreach (DictionaryEntry entry in resourceSet)
            {
                var isExist = oldSettings.Any(e => e.SettingKey.ToLower().Trim() == entry.Key.ToString().ToLower().Trim());
                if (isExist)
                {
                    continue;
                }
                settings.Add(CreateSetting(tenantId, entry.Key.ToString(), defaultSetting));
            }
            return settings;
        }

        private Setting CreateSetting(string tenantId, string settingKey, DefaultSettingValue defaultSettingValue)
        {
            string dataType = "string";
            string value = string.Empty;
            string group = ((int)SettingGroupEnum.AutoAllocation).ToString();
            switch (settingKey)
            {
                case SettingKeyConstant.IsEnableAutoAllocation:
                    {
                        value = defaultSettingValue.IsEnableAutoAllocation.ToString().Trim().ToLower();
                        dataType = "boolean";
                    }
                    break;
                case SettingKeyConstant.SelectedAutoAllocationMethodName:
                    {
                        value = defaultSettingValue.AutoAllocationMethod;
                    }
                    break;
                case SettingKeyConstant.NearestAvailableMethodOrderCapacity:
                    {
                        value = defaultSettingValue.NearestAvailableMethodOrderCapacity.ToString();
                    }
                    break;
                case SettingKeyConstant.NearestAvailableMethodRadiusInKM:
                    {
                        value = defaultSettingValue.NearestAvailableMethodRadiusInKM.ToString();
                    }
                    break;
                case SettingKeyConstant.NearestAvailableMethodNumberOfRetries:
                    {
                        value = defaultSettingValue.NearestAvailableMethodNumberOfRetries.ToString();
                    }
                    break;

                case SettingKeyConstant.ReachedDistanceRestriction:
                    {
                        value = defaultSettingValue.ReachedDistanceRestrictionInMeter.ToString();
                        group = ((int)SettingGroupEnum.General).ToString();
                    }
                    break;
                case SettingKeyConstant.DriverLoginRequestExpiration:
                    {
                        value = defaultSettingValue.DriverLoginRequestExpirationInHour.ToString();
                        group = ((int)SettingGroupEnum.General).ToString();
                    }
                    break;
            }

            return new Setting(tenantId: tenantId)
            {
                GroupId = group,
                SettingDataType = dataType,
                SettingKey = settingKey,
                Value = value,
            };
        }


    }
}
