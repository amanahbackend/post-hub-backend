﻿
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class TasksConfiguration
        : IEntityTypeConfiguration<SubTask>
    {
        public void Configure(EntityTypeBuilder<SubTask> TasksConfiguration)
        {
            TasksConfiguration.HasKey(x => x.Id);
            TasksConfiguration.Property(o => o.MainTaskId).IsRequired();
            TasksConfiguration.Property(o => o.TaskTypeId).IsRequired();
            TasksConfiguration.Property(o => o.CustomerId).IsRequired();
            TasksConfiguration.Property(o => o.TaskStatusId).IsRequired();
            TasksConfiguration.Property(o => o.Address).IsRequired();
            TasksConfiguration.OwnsOne(o => o.Location);
        }
    }
}
