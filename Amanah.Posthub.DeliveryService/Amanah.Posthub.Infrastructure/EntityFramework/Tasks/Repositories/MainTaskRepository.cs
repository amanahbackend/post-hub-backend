﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
namespace Amanah.Posthub.Infrastructure.EntityFramework.MainTasks.Repositories
{
    public class MainTaskRepository : Repository<MainTask>, IMainTaskRepository
    {
        public MainTaskRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }

        public async Task<MainTask> GetWithTasksAsync(Expression<Func<MainTask, bool>> expression)
        {
            return await _dbContext.MainTask
                .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.Driver)
                .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.TaskHistories)
                .Where(expression)
                .FirstOrDefaultAsync();
        }

        public async Task<Driver> GetMainTaskCurrentDriverAsync(int id)
        {
            var mainTask = await _dbContext.MainTask
               .Include(mainTask => mainTask.Tasks).ThenInclude(task => task.Driver)
               .Where(mainTask => mainTask.Id == id)
               .FirstOrDefaultAsync();
            return mainTask.Tasks
                  .Where(task => task.DriverId.HasValue)
                  .LastOrDefault()
                  ?.Driver;
        }

    }
}
