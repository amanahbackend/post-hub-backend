﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.SubTasks.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.SubTasks.Repositories
{
    public class SubTaskRepository : Repository<SubTask>, ISubTaskRepository
    {
        private readonly ICurrentUser _currentUser;

        public SubTaskRepository(
            ApplicationDbContext dbContext,
            ICurrentUser currentUser) : base(dbContext)
        {
            _currentUser = currentUser;
        }

        public async override Task<SubTask> GetFirstOrDefaultAsync(
            [NotNull] Expression<Func<SubTask, bool>> predicate)
        {
            return await _dbContext.Tasks
                .Include(task => task.Customer)
                .Include(task => task.Branch).ThenInclude(branch => branch.Customer)
                .Include(task => task.Driver).ThenInclude(driver => driver.User)
                .Include(task => task.TaskHistories)
                .Where(predicate)
                .FirstOrDefaultAsync();
        }
        public async Task<bool> CheckTaskAsync(Expression<Func<SubTask, bool>> expression, bool? ignoreFilter = false)
        {
            if (ignoreFilter.HasValue && ignoreFilter.Value)
            {
                return await _dbContext.Tasks
                    .IgnoreQueryFilters()
                    .Where(task => !task.IsDeleted)
                    .AnyAsync(expression);
            }
            else
            {
                return await _dbContext.Tasks
                      .AnyAsync(expression);
            }
        }

        public async Task<SubTask> GetSingleForStartAsync(int id)
        {
            return await _dbContext.Tasks
                 .Include(task => task.MainTask)
                 .Include(task => task.TaskStatus)
                 .Include(task => task.TaskHistories)
                 .Include(task => task.Driver).ThenInclude(driver => driver.CurrentLocation)
                 .Include(task => task.Driver).ThenInclude(driver => driver.User)
                 .Where(task => task.Id == id)
                 .Where(task => !task.IsDeleted && _currentUser.DriverId.HasValue && _currentUser.DriverId == task.DriverId)
                 .IgnoreQueryFilters()
                 .AsNoTracking()
                 .FirstOrDefaultAsync();
        }


    }
}
