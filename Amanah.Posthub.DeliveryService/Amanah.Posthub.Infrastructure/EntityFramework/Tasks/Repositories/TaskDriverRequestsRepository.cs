﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.MainTasks.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.TaskDriverRequestss.Repositories
{
    public class TaskDriverRequestRepository : Repository<TaskDriverRequests>, ITaskDriverRequestRepository
    {
        public TaskDriverRequestRepository(ApplicationDbContext dbContext) : base(dbContext)
        {



        }
        public IQueryable<TaskDriverRequests> GetTaskDriverRequests(int mainTaskId, int? driverId)
        {
            var query = _dbContext.TaskDriverRequests
            .IgnoreQueryFilters()
                .Where(x => x.MainTaskId == mainTaskId && x.DriverId == driverId.Value);
            //   var asdasd = query.ToSql();
            return query;
        }
        public async Task<TaskDriverRequests> GetTaskDriverRequestsLastAsync(int mainTaskId, int? driverId)
        {
            return await _dbContext.TaskDriverRequests
           .IgnoreQueryFilters()
               .FirstOrDefaultAsync(x => x.MainTaskId == mainTaskId && x.DriverId == driverId.Value);
        }

    }
}
