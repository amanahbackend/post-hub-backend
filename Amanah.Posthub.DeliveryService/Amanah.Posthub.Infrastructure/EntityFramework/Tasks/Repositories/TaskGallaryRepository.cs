﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.SubTasks.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Tasks.Entities;

namespace Amanah.Posthub.Infrastructure.EntityFramework.SubTasks.Repositories
{
    public class TaskGallaryRepository : Repository<TaskGallary>, ITaskGallaryRepository
    {
        public TaskGallaryRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }


    }
}
