﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.TaskHistorys.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Tasks.Entities;

namespace Amanah.Posthub.Infrastructure.EntityFramework.TaskHistorys.Repositories
{
    public class TaskHistoryRepository : Repository<TaskHistory>, ITaskHistoryRepository
    {
        public TaskHistoryRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }




    }
}
