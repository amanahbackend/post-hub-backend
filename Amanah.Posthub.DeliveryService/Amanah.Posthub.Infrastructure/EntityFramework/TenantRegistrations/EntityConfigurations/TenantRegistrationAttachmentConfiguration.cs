﻿using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class TenantRegistrationAttachmentConfiguration : IEntityTypeConfiguration<TenantRegistrationAttachment>
    {
        public void Configure(EntityTypeBuilder<TenantRegistrationAttachment> configuration)
        {
        }
    }
}
