﻿using Amanah.Posthub.DATA.DbContext;
using Amanah.Posthub.DATA.Extensions;
using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.EntityConfigurations
{
    public class TenantRegistrationConfiguration : IEntityTypeConfiguration<TenantRegistration>
    {
        public void Configure(EntityTypeBuilder<TenantRegistration> configuration)
        {
            configuration.Property(o => o.Name)
                .HasMaxLength(StringLengths.Default)
                .IsRequired();

            configuration.Property(o => o.OwnerName)
                .HasMaxLength(StringLengths.Default)
                .IsRequired();

            configuration.Property(o => o.ApproveSettings)
                .HasJsonConversion();

            configuration.HasOne(o => o.ApprovedORejectedByUser)
                .WithMany()
                .HasForeignKey(o => o.ApprovedORejectedByUserId)
                .OnDelete(DeleteBehavior.Restrict);
            configuration.HasOne(o => o.Country)
                .WithMany()
                .HasForeignKey(o => o.CountryId)
                .OnDelete(DeleteBehavior.Restrict);
            configuration.HasMany(o => o.SocialMediaAccounts)
                .WithOne()
                .HasForeignKey(o => o.TenantRegistrationId)
                .OnDelete(DeleteBehavior.Cascade);
            configuration.OwnsOne(registration => registration.BusinessAddress);
            configuration.OwnsOne(registration => registration.OwnerAddress);
            configuration.HasMany(o => o.Attachments)
                .WithOne()
                .HasForeignKey(o => o.TenantRegistrationId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
