﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.DATA.DbContext;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.Users.EntityConfigurations
{
    internal class ApplicationRoleConfiguration : IEntityTypeConfiguration<ApplicationRole>
    {
        public void Configure(EntityTypeBuilder<ApplicationRole> builder)
        {
            builder.HasIndex(role => new { role.Name, role.NormalizedName, role.Tenant_Id })
                .IsUnique()
                .HasName("RoleTenantNameIndex");
            builder.Property(role => role.Id)
                .HasMaxLength(StringLengths.StringId)
                .IsUnicode(false)
                .ValueGeneratedOnAdd();

            builder.HasMany(role => role.RoleClaims)
              .WithOne()
              .HasForeignKey(role => role.RoleId)
              .OnDelete(DeleteBehavior.Cascade);

            builder.HasData(CreateSuperAdminRole());
        }

        private ApplicationRole CreateSuperAdminRole()
        {
            return new ApplicationRole(Constants.SuperAdminRoleName)
            {
                Id = Constants.SuperAdminRoleId,
                NormalizedName = Constants.SuperAdminRoleName.ToUpper(),
                ConcurrencyStamp = Constants.initialConcurrencyStamp,
                Type = (int)RoleType.Manager
            };
        }

        public class Constants
        {
            public const string SuperAdminRoleId = "EC7CFEE4-DDE1-4B37-95DE-E3458B2B9666";
            public const string SuperAdminRoleName = "SuperAdmin";
            public const string initialConcurrencyStamp = "4E0AE6D6-252F-4E15-B963-589B0C3F5389";
        }
    }
}
