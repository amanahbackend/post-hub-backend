﻿using Amanah.Posthub.DATA.DbContext;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Amanah.Posthub.EntityConfigurations
{
    public class ApplicationUserConfiguration
        : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            builder.Property(user => user.Id)
                .HasMaxLength(StringLengths.StringId)
                .IsUnicode(false)
                .ValueGeneratedOnAdd();

            builder.HasOne(user => user.Country)
                .WithMany()
                .HasForeignKey(user => user.CountryId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(user => user.UserRoles)
                .WithOne()
                .HasForeignKey(user => user.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasMany(user => user.UserClaims)
                .WithOne()
                .HasForeignKey(user => user.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasData(CreateSuperAdmin());
        }

        private ApplicationUser CreateSuperAdmin()
        {
            return new ApplicationUser
            {
                Id = Constants.SuperAdminId,
                Email = Constants.SuperAdminEmail,
                NormalizedEmail = Constants.SuperAdminEmail.ToUpper(),
                UserName = Constants.SuperAdminEmail,
                NormalizedUserName = Constants.SuperAdminEmail.ToUpper(),
                CountryId = Constants.KuwaitCountryId,
                ConcurrencyStamp = Constants.InitialConcurrencyStamp,
                SecurityStamp = Constants.InitialSecurityStamp,
                IsAvailable = true,
                EmailConfirmed = true,
                PasswordHash = Constants.DefaultPasswordHash,
                FirstName = "Super",
                LastName = "Admin"
            };
        }

        public class Constants
        {
            public const string SuperAdminId = "5D56A46B-72BF-4849-82D0-43851B574479";
            public const string SuperAdminEmail = "super-admin@amanah.com";
            public const string SupserAdminDefaultPassword = "P@ssw0rd";
            public const int KuwaitCountryId = 121;
            public const string InitialConcurrencyStamp = "D5E28451-1E22-4DD8-A6E3-86330A71759C";
            public const string InitialSecurityStamp = "788F1971-0E77-49DC-BC49-EAE4A629BA66";
            public const string DefaultPasswordHash = "AQAAAAEAACcQAAAAEPxqhST2073HVmQmXOTq5eC0sWJLNAj6yWL5dj3KG3pnQT/qE8GLDAazPObekZb84w==";
        }
    }
}
