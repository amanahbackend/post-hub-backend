﻿using Amanah.Posthub.EntityConfigurations;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.DATA.Users.EntityConfigurations
{
    class IdentityUserRoleConfiguraiton : IEntityTypeConfiguration<IdentityUserRole<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserRole<string>> builder)
        {
            builder.HasData(
                new IdentityUserRole<string>
                {
                    RoleId = ApplicationRoleConfiguration.Constants.SuperAdminRoleId,
                    UserId = ApplicationUserConfiguration.Constants.SuperAdminId
                });
        }
    }
}
