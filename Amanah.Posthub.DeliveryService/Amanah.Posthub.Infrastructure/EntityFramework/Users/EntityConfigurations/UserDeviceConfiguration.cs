﻿using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Amanah.Posthub.Infrastructure.EntityFramework.Users.EntityConfigurations
{
    public class UserDeviceConfiguration
        : IEntityTypeConfiguration<UserDevice>
    {
        public void Configure(EntityTypeBuilder<UserDevice> builder)
        {
            //builder.Ignore(property => property.IsDeleted);
            //builder.Ignore(property => property.CreatedBy_Id);
            //builder.Ignore(property => property.UpdatedBy_Id);
            //builder.Ignore(property => property.DeletedBy_Id);
            //builder.Ignore(property => property.CreatedDate);
            //builder.Ignore(property => property.UpdatedDate);
            //builder.Ignore(property => property.DeletedDate);
        }
    }
}
