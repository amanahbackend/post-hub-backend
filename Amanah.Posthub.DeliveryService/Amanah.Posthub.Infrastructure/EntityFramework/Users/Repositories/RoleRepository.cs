﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.Users.Repositories
{
    public class RoleRepository : Repository<ApplicationRole, string>, IRoleRepository
    {
        private readonly ICurrentUser currentUser;

        public RoleRepository(
            ApplicationDbContext dbContext,
            ICurrentUser currentUser) : base(dbContext)
        {
            this.currentUser = currentUser;
        }

        public async Task<IQueryable<ApplicationRole>> GetAllRolesWithClaimsAsQueryableAsync(RoleType type = RoleType.Manager)
        {

            List<string> notAllowedToAccessRoles = await _dbContext.UserRoles.Where(userRole => userRole.UserId == currentUser.Id ||
                     userRole.UserId == currentUser.TenantId).Select(userRole => userRole.RoleId)
                     .AsNoTracking()
                     .ToListAsync();

            IQueryable<ApplicationRole> roles = _dbContext.Roles
                    .Where(role => role.Type == (int)type && !role.IsDeleted)
                    .Where(role => role.Name.ToLower().Trim() != "SuperAdmin".ToLower())
                    .Where(role => !notAllowedToAccessRoles.Contains(role.Id))
                    .Include(x => x.RoleClaims)
                    .AsNoTracking()
                    .AsQueryable();

            return roles;
        }

        public async Task<ApplicationRole> GetAsync(Expression<Func<ApplicationRole, bool>> roleExpression)
        {
            return await _dbContext.Roles
                  .IgnoreQueryFilters()
                  .Where(roleExpression)
                  .Where(c => !c.IsDeleted)
                  .FirstOrDefaultAsync();
        }

        public async Task<bool> IsRoleAssignedToUsersAsync(ApplicationRole role)
        {
            return await _dbContext.UserRoles.AnyAsync(x => x.RoleId == role.Id);
        }

        public async Task<bool> IsRoleExistAsync(string roleName, RoleType type = 0)
        {
            //&& (type == 0 || t.Type == (int)type)
            return await _dbContext.Roles.IgnoreQueryFilters().AnyAsync(t => t.Name == roleName && !t.IsDeleted);
        }

        public async Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName)
        {
            var roleClaim = await _dbContext.RoleClaims
                .FirstOrDefaultAsync(t => t.ClaimValue == claimName && t.RoleId == role.Id);
            _dbContext.Remove(roleClaim);
            return true;
        }
        public async Task<bool> RemoveRoleClaimsAsync(ApplicationRole role)
        {
            var roleClaim = await _dbContext.RoleClaims.Where(t => t.RoleId == role.Id)
               .ToListAsync();
            _dbContext.RemoveRange(roleClaim);
            return true;
        }







    }
}
