﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.Users.Repositories
{
    public class UserDeviceRepository : Repository<UserDevice>, IUserDeviceRepository
    {
        public UserDeviceRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public async Task AddOrUpdateAsync(UserDevice userDevice)
        {
            var existedUserDevice = await _dbContext.UserDevice
                .Where(userdevice => userdevice.UserId == userDevice.UserId
                && userdevice.FCMDeviceId == userDevice.FCMDeviceId
                && !userdevice.IsDeleted)
                .FirstOrDefaultAsync();
            if (existedUserDevice == null)
            {
                _dbContext.Add(userDevice);
            }
            else
            {
                existedUserDevice.IsLoggedIn = userDevice.IsLoggedIn;
                existedUserDevice.DeviceType = userDevice.DeviceType;
                existedUserDevice.Version = userDevice.Version;
                _dbContext.Update(existedUserDevice);
            }
        }

        public async Task<List<UserDevice>> SoftDeleteAsync(Expression<Func<UserDevice, bool>> expression)
        {
            var userDevices = await _dbContext.UserDevice
                .Where(expression)
                .IgnoreQueryFilters()
                .ToListAsync();
            userDevices.ForEach(userDevice => userDevice.SetIsDeleted(true));
            _dbContext.UpdateRange(userDevices);
            return userDevices;
        }
        public async Task<List<UserDevice>> HardDeleteAsync(Expression<Func<UserDevice, bool>> expression)
        {
            //var query =  _dbContext.UserDevice
            //    .Where(expression)
            //    .IgnoreQueryFilters().ToSql();


            var userDevices = await _dbContext.UserDevice
                .IgnoreQueryFilters()
                .Where(expression)
                .ToListAsync();
            _dbContext.RemoveRange(userDevices);
            return userDevices;
        }


    }
}
