﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Infrastructure.EntityFramework.Utilities.Constants;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.EntityFramework.Users.Repositories
{
    public class UserRepository : Repository<ApplicationUser, string>, IUserRepository
    {
        private readonly ICurrentUser currentUser;

        public UserRepository(ApplicationDbContext dbContext,
            ICurrentUser currentUser) : base(dbContext)
        {
            this.currentUser = currentUser;
        }

        public async Task<ApplicationUser> GetAsync(Expression<Func<ApplicationUser, bool>> userExpression)
        {
            return await _dbContext.Users
                .Where(userExpression)
                .IgnoreQueryFilters()
                .FirstOrDefaultAsync();
        }

        public async Task<ApplicationUser> GetLoginAsync(string username = null, string email = null)
        {
            var applicationUser = await _dbContext.Users
                .Include(user => user.UserRoles)
                .Include(user => user.UserClaims)
                .Where(user => ((!string.IsNullOrEmpty(username) && user.UserName.Trim().ToLower() == username.Trim().ToLower())
                   || (!string.IsNullOrEmpty(email) && user.Email.Trim().ToLower() == email.Trim().ToLower()))
                   && !user.IsDeleted)
                .FirstOrDefaultAsync();
            if (applicationUser != null)
            {
                var appRole = await _dbContext.Roles
                  .Include(role => role.RoleClaims)
                  .Where(role => role.Id == applicationUser.UserRoles.Select(u => u.RoleId).FirstOrDefault())
                  .FirstOrDefaultAsync();
                if (appRole != null)
                {

                    applicationUser.RoleNames = new List<string> { appRole.Name };
                    applicationUser.Permissions = appRole.RoleClaims.Where(r => r.ClaimType == "permission")
                        .Select(r => r.ClaimValue)
                        .Distinct()
                        .ToList();
                    applicationUser.UserType = appRole.Type;
                }

            }
            return applicationUser;
        }

        public async Task<bool> CheckIfTenantHaveThePermissionAsync(string permissionName)
        {
            var userRoleForTenant = await _dbContext.UserRoles
                  .Where(userRole => userRole.UserId == currentUser.TenantId)
                  .FirstOrDefaultAsync();
            if (userRoleForTenant != null)
            {
                return !await _dbContext.RoleClaims
                 .Where(roleClaim => roleClaim.RoleId == userRoleForTenant.RoleId &&
                 roleClaim.ClaimValue == permissionName)
                 .AnyAsync();
            }
            return false;
        }

        public void AddUserClaim(string userId, bool? isTenantUser = null)
        {
            _dbContext.UserClaims.Add(new IdentityUserClaim<string>
            {
                UserId = userId,
                ClaimType = "scope",
                ClaimValue = isTenantUser.HasValue && isTenantUser.Value ? Scope.Tenant : currentUser.Scope
            });
        }

    }
}
