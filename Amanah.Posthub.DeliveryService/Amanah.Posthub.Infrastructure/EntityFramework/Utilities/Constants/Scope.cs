﻿namespace Amanah.Posthub.Infrastructure.EntityFramework.Utilities.Constants
{
    public class Scope
    {
        public const string Platform = "platform";
        public const string Tenant = "tenant";
    }
}
