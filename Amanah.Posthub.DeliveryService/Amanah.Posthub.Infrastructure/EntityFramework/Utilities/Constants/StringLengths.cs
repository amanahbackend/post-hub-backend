﻿namespace Amanah.Posthub.DATA.DbContext
{
    internal static class StringLengths
    {
        public const int Default = 255;
        public const int UserName = 128;
        public const int StringId = 36;
        public const int Phone = 50;
    }
}
