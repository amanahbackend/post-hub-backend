﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Utilites;

namespace Amanah.Posthub.DATA.Extensions
{
    internal static class EntityFrameworkExtensions
    {
        public static IEnumerable<IMutableEntityType> GetEntitiesOfType<T>(this ModelBuilder modelBuilder)
        {
            return modelBuilder.Model
                .GetEntityTypes()
                .Where(entityType =>
                    typeof(T).IsAssignableFrom(entityType.ClrType) &&
                    (entityType.BaseType == null || !typeof(T).IsAssignableFrom(entityType.BaseType.ClrType)));
        }

        public static void SetPropertyIfNotSet(this EntityEntry entityEntry, string propertyName, object value)
        {
            var property = entityEntry.Property(propertyName);
            if (ReflectionHelper.IsDefaultValue(property.CurrentValue))
            {
                property.CurrentValue = value;
            }
        }

        public static void AddQueryFilter(this IMutableEntityType target, LambdaExpression filter)
        {
            if (target.GetQueryFilter() == default)
            {
                target.SetQueryFilter(filter);
            }
            else
            {
                var parameter = target.GetQueryFilter().Parameters[0];
                var left = target.GetQueryFilter().Body;
                var right = filter.Body.ReplaceParameter(filter.Parameters[0], parameter);
                var body = Expression.AndAlso(left, right);
                target.SetQueryFilter(Expression.Lambda(body, parameter));
            }
        }

        public static PropertyBuilder<TProperty> HasJsonConversion<TProperty>(this PropertyBuilder<TProperty> propertyBuilder)
        {
            var jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
            return propertyBuilder.HasConversion(
                value => JsonConvert.SerializeObject(value, jsonSettings),
                json => JsonConvert.DeserializeObject<TProperty>(json, jsonSettings));
        }

        private static Expression ReplaceParameter(this Expression expression, ParameterExpression source, Expression target)
        {
            return new ParameterReplacer { Source = source, Target = target }.Visit(expression);
        }

        private class ParameterReplacer : ExpressionVisitor
        {
            public ParameterExpression Source;

            public Expression Target;

            protected override Expression VisitParameter(ParameterExpression node)
            {
                return node == Source ? Target : base.VisitParameter(node);
            }
        }
    }
}
