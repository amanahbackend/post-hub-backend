﻿using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;


namespace Amanah.Posthub.DATA.Helpers
{
    public static class IQueryableExtensions
    {
        public static async Task<PagedResult<T>> ToPagedResultAsync<T>(
            this IQueryable<T> source,
            PaginatedItemsViewModel pagingOptions)
        {
            var pagedResult = new PagedResult<T>
            {
                TotalCount = await source.CountAsync(),
                Result = await source.ApplyPaging(pagingOptions).ToListAsync()
            };
            return pagedResult;
        }

        public static async Task<_PagedResult<T>> ToPagedResultAsync<T>(
            this IQueryable<T> source,
            PagingOptions pagingOptions)
        {
            var skip = (pagingOptions.PageNumber - 1) * pagingOptions.PageSize;
            var totalCount = await source.CountAsync();
            var pageItems = await source.Skip(skip)
                .Take(pagingOptions.PageSize)
                .ToArrayAsync();
            var totalPages = (int)Math.Ceiling((double)totalCount / pagingOptions.PageSize);
            return new _PagedResult<T>(
                totalCount,
                totalPages,
                pagingOptions.PageNumber,
                pagingOptions.PageSize,
                pageItems);
        }

        public static async Task<PagedResult<TResult>> ToPagedResultAsync<TEntity, TResult>(
            this IQueryable<TEntity> source,
            PaginatedItemsViewModel pagingOptions,
            IConfigurationProvider config)
        {
            var totalCount = await source.CountAsync();
            var items = await source
                .ApplyPaging(pagingOptions)
                .ProjectTo<TResult>(config)
                .ToListAsync();
            //if(items.Count < pagingOptions.PageSize && totalCount >= pagingOptions.PageSize)
            //{
            //    Debugger.Break();
            //}
            var pagedResult = new PagedResult<TResult>
            {
                TotalCount = totalCount,
                Result = items
            };
            return pagedResult;
        }

        public static IQueryable<T> ApplyPaging<T>(this IQueryable<T> source, PaginatedItemsViewModel pagingOptions)
        {
            var pageNumber = Math.Max(pagingOptions.PageNumber, 1);
            var pageSize = pagingOptions.PageSize == 0
                ? 20
                : pagingOptions.PageSize;
            return source.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        public static IQueryable<T> ApplyPaging<T>(this IQueryable<T> source, PagingOptions pagingOptions)
        {
            var pageNumber = pagingOptions.PageNumber;
            var pageSize = pagingOptions.PageSize;
            return source.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var knownKeys = new HashSet<TKey>();
            return source.Where(element => knownKeys.Add(keySelector(element)));
        }
        public static IQueryable<TEntity> AddSorting<TEntity>(
            this IQueryable<TEntity> source,
            params SortingField<TEntity>[] sortings)
        {
            if (!sortings.Any())
            {
                return source;
            }
            var firstSorting = sortings.First();
            var result = firstSorting.Direction == SortDirection.Ascending
                ? source.OrderBy(firstSorting.Field)
                : source.OrderByDescending(firstSorting.Field);
            foreach (var sorting in sortings.Skip(1))
            {
                result = sorting.Direction == SortDirection.Ascending
                    ? result.ThenBy(sorting.Field)
                    : result.ThenByDescending(sorting.Field);
            }
            return result;
        }

#if DEBUG
        /// <summary>
        /// Translate IQueryable to sql if relational database provider is configured Compatible with EF core 3.1
        /// To be used for testing and debugging purposes only
        /// </summary>
        public static string ToSql<TEntity>(this IQueryable<TEntity> query)
        {
            using var enumerator = query.Provider.Execute<IEnumerable<TEntity>>(query.Expression).GetEnumerator();
            var relationalCommandCache = enumerator.GetPrivateField("_relationalCommandCache");
            var selectExpression = relationalCommandCache.GetPrivateField<SelectExpression>("_selectExpression");
            var factory = relationalCommandCache.GetPrivateField<IQuerySqlGeneratorFactory>("_querySqlGeneratorFactory");
            var sqlGenerator = factory.Create();
            var command = sqlGenerator.GetCommand(selectExpression);
            string sql = command.CommandText;
            return sql;
        }

        private static object GetPrivateField(this object instance, string privateField) =>
            instance?.GetType()
                .GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)
                ?.GetValue(instance);

        private static T GetPrivateField<T>(this object instance, string privateField) =>
            (T)instance?.GetType()
                .GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)
                ?.GetValue(instance);
#endif
    }
}