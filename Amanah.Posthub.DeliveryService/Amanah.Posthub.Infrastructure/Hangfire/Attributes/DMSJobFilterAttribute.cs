﻿using Amanah.Posthub.BASE.Authentication;
using Hangfire.Client;
using Hangfire.Common;
using Hangfire.Server;

namespace Amanah.Posthub.Infrastructure.Hangfire.Attributes
{
    public sealed class StoreCurrentUserJobFilterAttribute : JobFilterAttribute, IClientFilter, IServerFilter
    {
        private const string CurrentUserDetailsParamter = "CURRENT_USER_DETAILS";
        private readonly ICurrentUserDetailsStore _currentUserDetailsStore;

        public StoreCurrentUserJobFilterAttribute(ICurrentUserDetailsStore currentUserDetailsStore)
        {
            _currentUserDetailsStore = currentUserDetailsStore;
        }

        void IClientFilter.OnCreating(CreatingContext filterContext)
        {
            var currentUserDetails = _currentUserDetailsStore.Serialize();
            if (currentUserDetails != default)
            {
                filterContext.SetJobParameter(CurrentUserDetailsParamter, currentUserDetails);
            }
        }

        void IClientFilter.OnCreated(CreatedContext filterContext)
        {
        }

        void IServerFilter.OnPerforming(PerformingContext filterContext)
        {
            var details = filterContext.GetJobParameter<string>(CurrentUserDetailsParamter);
            if (!string.IsNullOrWhiteSpace(details))
            {
                _currentUserDetailsStore.Deserialize(details);
            }
        }

        void IServerFilter.OnPerformed(PerformedContext filterContext)
        {
            _currentUserDetailsStore.Clear();
        }
    }


}