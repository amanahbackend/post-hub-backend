﻿namespace Amanah.Posthub.Infrastructure.Hangfire.Attributes
{
#if false
    /*
To support the priorities use the following
*/
    // 1- Add the priority enum in shredkernl/backgournJobs
    public enum JobPriority
    {
        Critical = 1,
        High,
        Normal,
        Medium,
        Low
    }

    // 2- Add JobPriorityAttribute that carry jobPriority
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class JobPriorityAttrubute : Attribute
    {
        private static readonly JobPriorityAttrubute _defaultInstance = new JobPriorityAttrubute();

        public JobPriority Priority { get; set; } = JobPriority.Normal;

        public static JobPriorityAttrubute Default => _defaultInstance;
    }
    //3- add the following attribute in infrastructure
    internal class HandlePriorityJobFilterAttribute : JobFilterAttribute, IElectStateFilter
    {
        public void OnStateElection(ElectStateContext context)
        {
            var enqueuedState = context.CandidateState as EnqueuedState;
            if (enqueuedState != null)
            {
                enqueuedState.Queue = GetQueueFor(context.BackgroundJob.Job);
            }
        }

        private string GetQueueFor(Job job)
        {
            var attribute = JobPriorityAttrubute.Default;
            if (job.Args.Count == 1)
            {
                attribute = job.Args
                    .First()
                    .GetType()
                    .GetCustomAttribute<JobPriorityAttrubute>();
            }
            return JobPriorityHelper.GetSortablePriorityName(attribute.Priority);
        }
    } 
#endif
}