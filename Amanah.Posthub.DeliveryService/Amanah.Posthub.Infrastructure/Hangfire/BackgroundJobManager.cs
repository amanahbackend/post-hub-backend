﻿using Amanah.Posthub.SharedKernel.BackgroundJobs;
using Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes;
using Amanah.Posthub.SharedKernel.Utilites.Helpers;
using Hangfire;
using Microsoft.Extensions.Logging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.Hangfire
{
    public class BackgroundJobManager : IBackgroundJobManager
    {
        private readonly IServiceProvider _serviceProvider;

        public BackgroundJobManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void Enqueue<TJobModel>(TJobModel jobModel, TimeSpan delay)
        {
            Check.NotNegative(delay, nameof(delay));
            if (delay == default)
            {
                BackgroundJob.Enqueue(GetJobExpression(jobModel));
            }
            else
            {
                BackgroundJob.Schedule(GetJobExpression(jobModel), delay);
            }
        }

        public void Schedule<TJobModel>(TJobModel jobModel, DateTime enqueeAtUtc)
        {
            BackgroundJob.Schedule(GetJobExpression(jobModel), enqueeAtUtc);
        }

        private Expression<Func<IBackgroundJob<TJobModel>, Task>> GetJobExpression<TJobModel>(
            TJobModel jobModel) => job => job.ExecuteAsync(jobModel);
    }

#if true // Example
    [MinutelyJob]
    internal class TestPeriodicJob : IPeriodicBackgroundJob
    {
        private readonly ILogger<TestPeriodicJob> _logger;
        private readonly IBackgroundJobManager _jobManager;

        public TestPeriodicJob(ILogger<TestPeriodicJob> logger, IBackgroundJobManager jobManager)
        {
            _logger = logger;
            _jobManager = jobManager;
        }

        public Task ExecuteAsync()
        {
            _logger.LogInformation($"Run {DateTime.Now}");
            _jobManager.Enqueue(new TestJobModel { Data = "Dummy" });
            return Task.CompletedTask;
        }
    }

    class TestJobModel
    {
        public string Data { get; set; }
    }

    class TestJobHandler : IBackgroundJob<TestJobModel>
    {
        private readonly ILogger<TestJobHandler> _logger;

        public TestJobHandler(ILogger<TestJobHandler> logger)
        {
            _logger = logger;
        }

        public Task ExecuteAsync(TestJobModel jobModel)
        {
            _logger.LogInformation($"Enqueued job run at {DateTime.Now}");
            return Task.CompletedTask;
        }
    }
#endif
}
