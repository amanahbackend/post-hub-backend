﻿using Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes;
using System;

namespace Amanah.Posthub.Infrastructure.Hangfire
{
    public static class CronHelper
    {
        public static string GenerateCron(PeriodicJobAttribute jobAttribute)
        {
            switch (jobAttribute)
            {
                case MinutelyJobAttribute minutely:
                    return $"*/{minutely.EveryMinutes} * * * *";
                case HourlyJobAttribute hourly:
                    return $"{hourly.AtMinute} */{hourly.EveryHours} * * *";
                case DailyJobAttribute daily:
                    return $"{daily.AtMinute} {daily.AtHour} */{daily.EveryDays} * *";
                case WeeklyJobAttribute weekly:
                    return $"{weekly.AtMinute} {weekly.AtHour} * * {weekly.AtDayOfWeek}";
                case MonthlyJobAttribute monthly:
                    return $"{monthly.AtMinute} {monthly.AtHour} {monthly.AtDayOfMonth} */{monthly.EveryMonths} *";
                case YearlyJobAttribute yearly:
                    return $"{yearly.AtMinute} {yearly.AtHour} {yearly.AtDayOfMonth} {yearly.AtMonth} *";
                default:
                    throw new NotSupportedException($"{jobAttribute.GetType().Name} is not supported.");
            }
        }
    }
}
