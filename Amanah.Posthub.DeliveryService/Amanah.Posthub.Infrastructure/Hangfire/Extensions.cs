﻿using Amanah.Posthub.Infrastructure.Hangfire.Attributes;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using Amanah.Posthub.SharedKernel.BackgroundJobs.Attributes;
using Amanah.Posthub.SharedKernel.Utilites.Helpers;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Utilites;
using Utilities.Extensions;

namespace Amanah.Posthub.Infrastructure.Hangfire
{
    public static class Extensions
    {
        private readonly static MethodInfo _addPeriodicJobMethodInfo =
            ReflectionHelper.GetStaticMethod(typeof(Extensions), nameof(AddPeriodicJob));

        public static IServiceCollection AddBackgroundJobs(
            this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IBackgroundJobManager, BackgroundJobManager>();
            RegisterBackGroundJobs(services);
            return services.AddHangfire((provider, hangfireConfig) => hangfireConfig
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseFilter(ActivatorUtilities.CreateInstance<StoreCurrentUserJobFilterAttribute>(provider))
                .UseSqlServerStorage(configuration["ConnectionString"], new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }))
                .AddHangfireServer(options =>
                {
#if false
                    options.Queues = GetQueues(); 
#endif
                });
        }

        public static IApplicationBuilder UseBackgroundJobs(this IApplicationBuilder app)
        {
            RegisterPeriodicBackgroundJobs();
            return app.UseHangfireDashboard();
        }

        private static void RegisterBackGroundJobs(IServiceCollection services)
        {
            static bool IsBackgroundJob(Type @interface) => @interface.IsOfGenericType(typeof(IBackgroundJob<>));

            static void ValidateNoDuplicates(IEnumerable<Type> types)
            {
                var duplicates = types.GetDuplicates().ToArray();
                if (duplicates.Any())
                {
                    var joinedDuplicates = duplicates.Select(type => type.Name).Join(", ");
                    var message = $"Single job handler is allowed the following duplicates found \"{joinedDuplicates}\"";
                    throw new InvalidOperationException(message);
                }
            }

            var backgroundJobs = TypeFinder.FindTypes(type => type.IsConcrete())
                .Select(type =>
                    (Job: type, Interfaces: type.GetInterfaces().Where(IsBackgroundJob)))
                .Where(type => type.Interfaces.Any())
                .ToArray();
            ValidateNoDuplicates(backgroundJobs.SelectMany(type => type.Interfaces));
            foreach (var (job, interfaces) in backgroundJobs)
            {
                foreach (var @interface in interfaces)
                {
                    services.AddScoped(@interface, job);
                }
            }
        }

        private static void RegisterPeriodicBackgroundJobs()
        {
            static bool IsPeriodicJob(Type type)
            {
                return type.IsConcrete() && type.IsOfType<IPeriodicBackgroundJob>();
            }

            static PeriodicJobAttribute GetPeriodicJobAttribute(Type jobType)
            {
                var jobAttribute = jobType.GetCustomAttribute<PeriodicJobAttribute>();
                if (jobAttribute == null)
                {
                    throw new InvalidOperationException(
                        $"The job must has {nameof(PeriodicJobAttribute)} with scheduling details.");
                }
                return jobAttribute;
            }

            TypeFinder.FindTypes(IsPeriodicJob)
                .ToList()
                .ForEach(type =>
                {
                    var cronExpression = CronHelper.GenerateCron(GetPeriodicJobAttribute(type));
                    _addPeriodicJobMethodInfo.MakeGenericMethod(type).Invoke(null, new object[] { cronExpression });
                });
        }

        private static void AddPeriodicJob<TPeriodicJob>(string cronExpression)
            where TPeriodicJob : IPeriodicBackgroundJob
        {
            RecurringJob.AddOrUpdate<TPeriodicJob>(job => job.ExecuteAsync(), cronExpression);
        }

#if false
        private static string[] GetQueues()
        {
            return Enum.GetValues(typeof(JobPriority))
                .Cast<JobPriority>()
                .Select(JobPriorityHelper.GetSortablePriorityName)
                .OrderBy(priority => priority)
                .ToArray();
        } 
#endif
    }
}
