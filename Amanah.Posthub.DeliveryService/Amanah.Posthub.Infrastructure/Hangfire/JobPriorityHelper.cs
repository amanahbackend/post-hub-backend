﻿#if false
using Amanah.Posthub.Infrastructure.Hangfire.Attributes;

namespace Amanah.Posthub.Infrastructure.Hangfire
{
    internal class JobPriorityHelper
    {
        public static string GetSortablePriorityName(JobPriority priority)
        {
            return $"{(int)priority}-{priority}";
        }
    }
}

#endif