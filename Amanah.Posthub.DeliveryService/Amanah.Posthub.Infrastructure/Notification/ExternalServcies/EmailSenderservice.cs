﻿using Amanah.Posthub.Infrastructure.ExternalServices.Settings;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.Infrastructure.ExternalServices
{
    public class EmailSenderService : IEmailSenderservice
    {
        private readonly EmailSettings _emailSettings;

        public EmailSenderService(IOptions<EmailSettings> emailSettings)
        {
            _emailSettings = emailSettings.Value;
        }

        public async Task SendEmailAsync(string subject, string body, string toEmail)
        {
            await SendEmailAsync(subject, body, new List<string> { toEmail });
        }

        public async Task SendEmailAsync(string subject, string body, IList<string> toEmails, List<byte[]> pdfAttachments = null)
        {
            try
            {
                var mimeMessage = new MimeMessage();

                mimeMessage.From.Add(new MailboxAddress(_emailSettings.SenderName, _emailSettings.Sender));
                if (toEmails != null)
                {
                    foreach (var email in toEmails)
                    {
                        mimeMessage.To.Add(new MailboxAddress("", email));
                    }
                }
                //mimeMessage.To.Add(new MailboxAddress("",));

                mimeMessage.Subject = subject;
                if (pdfAttachments != null)
                {
                    var builder = new BodyBuilder();
                    builder.HtmlBody = body;
                    int i = 1;
                    foreach (var fileBytes in pdfAttachments)
                    {
                        builder.Attachments.Add("Attachment" + i + ".pdf", fileBytes, MimeKit.ContentType.Parse(MediaTypeNames.Application.Pdf));
                        i++;
                    }
                    mimeMessage.Body = builder.ToMessageBody();
                }
                else
                {
                    mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                    {
                        Text = body
                    };
                }


                using var client = new MailKit.Net.Smtp.SmtpClient
                {
                    ServerCertificateValidationCallback = (s, c, h, e) => true
                };
                await client.ConnectAsync(_emailSettings.MailServer, _emailSettings.MailPort, SecureSocketOptions.SslOnConnect);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync(new NetworkCredential(_emailSettings.Sender, _emailSettings.Password));
                await client.SendAsync(mimeMessage);
                await client.DisconnectAsync(true);

            }
            catch (Exception ex)
            {
                throw new DomainException(Keys.Validation.FailSendEmail);
            }

        }


    }
}
