﻿using Amanah.Posthub.Context;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.ExternalServices
{
    // TODO: this is a clear infrastructure service and its service should go to the shared kernel
    public class FirebasePushNotificationService : IFirebasePushNotificationService
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;
        private readonly IBackgroundJobManager _jobManager;

        public FirebasePushNotificationService(
            ApplicationDbContext context,
            IConfiguration configuration,
            IBackgroundJobManager jobManager)
        {
            _context = context;
            _configuration = configuration;
            _jobManager = jobManager;

        }

        public async Task SendAsync(
            string toUserId,
            string notificationTitle,
            string notificationText,
            object notificationData)
        {
            var applicationId = _configuration.GetValue<string>("FcmSettings:ApiKey");

            //get last deviceId That driver loged in from it
            var userDevices = await _context.UserDevice
               .Where(userDevice => userDevice.UserId == toUserId && !userDevice.IsDeleted)
               .IgnoreQueryFilters()
               .AsNoTracking()
               .ToListAsync();

            foreach (var device in userDevices)
            {
                _jobManager.Enqueue(new SendFireBaseNotificationJobModel
                {
                    FCMDeviceId = device.FCMDeviceId,
                    NotificationTitle = notificationTitle,
                    NotificationText = notificationText,
                    Payload = notificationData,
                    ApplicationId = applicationId
                });
            }

        }
    }

    public class SendFireBaseNotificationJobModel
    {
        public string FCMDeviceId { get; set; }

        public string NotificationTitle { get; set; }

        public string NotificationText { get; set; }

        public object Payload { get; set; }

        public string ApplicationId { get; set; }
    }

    public class SendFireBaseNotificationJob : IBackgroundJob<SendFireBaseNotificationJobModel>
    {
        public Task ExecuteAsync(SendFireBaseNotificationJobModel jobModel)
        {
            return FireBase.SendPushNotificationAsync(
                jobModel.FCMDeviceId,
                jobModel.NotificationTitle,
                jobModel.NotificationText,
                jobModel.Payload,
                jobModel.ApplicationId);
        }
    }
}
