﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.ExternalServices
{
    public interface IEmailSenderservice
    {
        Task SendEmailAsync(string subject, string body, string toEmail);
        Task SendEmailAsync(string subject, string body, IList<string> toEmails, List<byte[]> Attachments = null);


    }
}
