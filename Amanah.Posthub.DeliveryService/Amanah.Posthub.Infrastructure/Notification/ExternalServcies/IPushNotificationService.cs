﻿using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.ExternalServices
{
    public interface IFirebasePushNotificationService
    {
        Task SendAsync(string toUserId, string notificationTitle, string notificationText, object notificationData);
    }
}
