﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.Infrastructure.ExternalServices
{
    public static class FireBase
    {
        //static readonly string ApplicationId = ConfigurationManager.AppSettings["FireBase.ApplicationId"];
        //static readonly string SenderId = ConfigurationManager.AppSettings["FireBase.SenderId"];

        public const string SendToAll = "/topics/all";

        public static async Task<string> SendPushNotificationAsync(string deviceId, string title, string body,
            object dataObj, string ApplicationId)
        {
            string str;
            try
            {
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";

                var data = new
                {
                    to = deviceId,
                    data = new
                    {
                        body = body,
                        title = title,
                        MessageData = dataObj
                    },

                };
                var json = JsonConvert.SerializeObject(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", ApplicationId));
                tRequest.ContentLength = byteArray.Length;
                tRequest.Timeout = 8000;

                using (Stream dataStream = await tRequest.GetRequestStreamAsync())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = await tRequest.GetResponseAsync())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                str = ex.Message;
            }
            return str;
        }




    }
}
