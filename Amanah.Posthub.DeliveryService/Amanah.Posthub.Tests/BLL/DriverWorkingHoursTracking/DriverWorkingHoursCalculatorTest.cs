﻿using System;
using System.Collections.Generic;
using System.Linq;
using Amanah.Posthub.BLL.DriverWorkingHour.Managers;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.Entities;
using NUnit.Framework;
using Shouldly;
using Utilities.Extensions;

namespace DMS.Tests.BLL.DriverWorkingHoursTracking
{
    public class DriverWorkingHoursCalculatorTest
    {
        // add multiple drivers
        // add missing entries at end & begining
        // add entries that has start in day and close in another day
        [Test]
        public void Calculte_Empty_ShouldReturnEmptyResult()
        {
            // Arrange,Act,Assert
            DriverWorkingHoursCalculator.CalculateOld(Enumerable.Empty<DriverWorkingHourTracking>()).ShouldBeEmpty();
        }

        [Test]
        public void Calculte_NormalEntries_ShouldReturnCorrectData()
        {
            // Arrange
            var onTime = TimeSpan.FromMinutes(45);
            var offTime = TimeSpan.FromMinutes(15);
            var day = DateTime.Now.BeginingOfDay();
            var driverA = CreateDriver(1, "FirstA", "LastA");
            var driverB = CreateDriver(2, "FirstB", "LastB");
            var driverAEntries = driverA.StartWithLogin(day)
                .SetOffDuty(onTime)
                .SetOnDuty(offTime)
                .Logout(onTime)
                .Login(offTime)
                .Logout(onTime)
                .Login(offTime)
                .SetOffDuty(onTime)
                .Logout(onTime)
                .ToArray();
            var driverBEntries = driverB.StartWithLogin(day)
                .SetOffDuty(onTime)
                .SetOnDuty(offTime)
                .Logout(onTime)
                .ToArray();
            var expectedResult = new Dictionary<int, (DateTime, TimeSpan, string)>
            {
                {1, (day, TimeSpan.FromMinutes(4 * onTime.TotalMinutes), "FirstA LastA")},
                {2, (day, TimeSpan.FromMinutes(2 * onTime.TotalMinutes), "FirstB LastB")},
            };

            // Act
            var result = DriverWorkingHoursCalculator.CalculateOld(driverAEntries.Concat(driverBEntries));

            // Assert
            result.Count.ShouldBe(expectedResult.Count);

            foreach (var item in result)
            {
                expectedResult.ShouldContainKey(item.DriverId);
                expectedResult[item.DriverId].ShouldBe((item.DayOfMonth, item.InternalTotalHours, item.DriverName));
            }
        }

        [Test]
        public void Calculte_MissingStartEndEntries_ShouldAddMissingEntries()
        {
            // Arrange
            var onTime = TimeSpan.FromMinutes(45);
            var offTime = TimeSpan.FromMinutes(15);
            var day = DateTime.Now.AddDays(-1).BeginingOfDay();
            var driverA = CreateDriver(1, "FirstA", "LastA");
            var driverB = CreateDriver(2, "FirstB", "LastB");
            var driverAEntries = Enumerable.Repeat(driverA.SetOffDuty(day + onTime), 1)
                .SetOnDuty(offTime)
                .Logout(onTime)
                .Login(offTime)
                .Logout(onTime)
                .Login(offTime)
                .ToArray();
            var driverBEntries = driverB.StartWithLogin(day)
                .SetOffDuty(onTime)
                .SetOnDuty(offTime)
                .ToArray();
            var addedTimeToDriverA = (driverAEntries.First().ActivityDateTime - day) +
                (day.EndOfDay() - driverAEntries.Last().ActivityDateTime);
            var addedTimeToDriverB = day.EndOfDay() - driverBEntries.Last().ActivityDateTime;
            var expectedResult = new Dictionary<int, (DateTime, TimeSpan, string)>
            {
                {1, (day, TimeSpan.FromMinutes(2 * onTime.TotalMinutes) + addedTimeToDriverA.FixFraction(), "FirstA LastA")},
                {2, (day, TimeSpan.FromMinutes(1 * onTime.TotalMinutes)+addedTimeToDriverB.FixFraction(), "FirstB LastB")},
            };

            // Act
            var result = DriverWorkingHoursCalculator.CalculateOld(driverAEntries.Concat(driverBEntries));

            // Assert
            result.Count.ShouldBe(expectedResult.Count);

            foreach (var item in result)
            {
                expectedResult.ShouldContainKey(item.DriverId);
                expectedResult[item.DriverId].ShouldBe((item.DayOfMonth, item.InternalTotalHours, item.DriverName));
            }
        }

        [Test]
        public void Calculte_StartAndEndDistributedOnMultipleDays_ShouldSplitDaysAndAddMissingEntries()
        {
            // Arrange
            var driverA = CreateDriver(1, "FirstA", "LastA");
            var driverB = CreateDriver(2, "FirstB", "LastB");
            var driverAEntries = new[]
            {
                driverA.Login(new DateTime(2021, 2, 14, 22, 0, 0)),
                driverA.Logout(new DateTime(2021, 2, 15, 2, 0, 0)),
            };
            var driverBEntries = new[]
            {
                driverB.Login(new DateTime(2021, 2, 15, 9, 0, 0)),
                driverB.Logout(new DateTime(2021, 2, 15, 10, 0, 0)),
                driverB.Login(new DateTime(2021, 2, 15, 20, 0, 0)),
                driverB.Logout(new DateTime(2021, 2, 16, 2, 0, 0)),
            };
            var expectedResult = new (int Id, DateTime Day, TimeSpan TotalHours, string DriverName)[]
            {
                (1, new DateTime(2021, 2, 14),TimeSpan.FromHours(2), "FirstA LastA"),
                (1, new DateTime(2021, 2, 15),TimeSpan.FromHours(2), "FirstA LastA"),
                (2, new DateTime(2021, 2, 15), TimeSpan.FromHours(5), "FirstB LastB"),
                (2, new DateTime(2021, 2, 16), TimeSpan.FromHours(2), "FirstB LastB"),
            }
                .GroupBy(item => item.Id)
                .ToDictionary(
                    group => group.Key,
                    group => group.Select(item => (item.Day, item.TotalHours, item.DriverName)));

            // Act
            var result = DriverWorkingHoursCalculator.CalculateOld(driverAEntries.Concat(driverBEntries));

            // Assert
            var groupedResult = result.GroupBy(item => item.DriverId)
                .ToDictionary(
                    group => group.Key,
                    group => group.Select(item => (item.DayOfMonth, item.InternalTotalHours, item.DriverName)));
            groupedResult.Count().ShouldBe(expectedResult.Count);
            foreach (var (userId, userEntries) in groupedResult)
            {
                expectedResult.ShouldContainKey(userId);
                userEntries.ShouldBe(expectedResult[userId]);
            }
        }

        [Test]
        public void Calculte_DuplicatesOrMissingMiddleEntries_ShouldBeAdjusted()
        {
            // Arrange
            var driverA = CreateDriver(1, "FirstA", "LastA");
            var driverB = CreateDriver(2, "FirstB", "LastB");
            var driverAEntries = new[]
            {
                driverA.Login(new DateTime(2021, 2, 14, 22, 0, 0)),
                driverA.Login(new DateTime(2021, 2, 14, 22, 10, 0)),
                driverA.Login(new DateTime(2021, 2, 14, 22, 20, 0)),
                driverA.Logout(new DateTime(2021, 2, 15, 2, 0, 0)),
                driverA.Logout(new DateTime(2021, 2, 15, 2, 10, 0)),
                driverA.Logout(new DateTime(2021, 2, 15, 2, 20, 0)),
            };
            var driverBEntries = new[]
            {
                driverB.Login(new DateTime(2021, 2, 15, 9, 0, 0)),
                driverB.Login(new DateTime(2021, 2, 15, 9, 30, 0)),
                driverB.Logout(new DateTime(2021, 2, 15, 10, 0, 0)),
                driverB.Logout(new DateTime(2021, 2, 15, 10, 30, 0)),
                driverB.Login(new DateTime(2021, 2, 15, 20, 0, 0)),
                driverB.Login(new DateTime(2021, 2, 15, 20, 30, 0)),
                driverB.Logout(new DateTime(2021, 2, 16, 2, 0, 0)),
                driverB.Logout(new DateTime(2021, 2, 16, 2, 50, 0)),
            };
            var expectedResult = new (int Id, DateTime Day, TimeSpan TotalHours, string DriverName)[]
            {
                (1, new DateTime(2021, 2, 14),TimeSpan.FromHours(2), "FirstA LastA"),
                (1, new DateTime(2021, 2, 15),TimeSpan.FromHours(2), "FirstA LastA"),
                (2, new DateTime(2021, 2, 15), TimeSpan.FromHours(5), "FirstB LastB"),
                (2, new DateTime(2021, 2, 16), TimeSpan.FromHours(2), "FirstB LastB"),
            }
                .GroupBy(item => item.Id)
                .ToDictionary(
                    group => group.Key,
                    group => group.Select(item => (item.Day, item.TotalHours, item.DriverName)));

            // Act
            var result = DriverWorkingHoursCalculator.CalculateOld(driverAEntries.Concat(driverBEntries));

            // Assert
            var groupedResult = result.GroupBy(item => item.DriverId)
                .ToDictionary(
                    group => group.Key,
                    group => group.Select(item => (item.DayOfMonth, item.InternalTotalHours, item.DriverName)));
            groupedResult.Count().ShouldBe(expectedResult.Count);
            foreach (var (userId, userEntries) in groupedResult)
            {
                expectedResult.ShouldContainKey(userId);
                userEntries.ShouldBe(expectedResult[userId]);
            }
        }

        private Driver CreateDriver(int id, string firstName, string lastName)
        {
            return new Driver()
                .SetProperty(nameof(Driver.Id), id)
                .SetProperty(nameof(Driver.User), new ApplicationUser { FirstName = firstName, LastName = lastName });
        }
    }

    internal static class Extensions
    {
        public static DriverWorkingHourTracking Login(this Driver driver, DateTime dateTime)
        {
            return DriverWorkingHourTracking.CreateLogin(string.Empty, driver.Id)
                .SetDriver(driver)
                .SetActivityDateTime(dateTime);
        }

        public static IEnumerable<DriverWorkingHourTracking> Login(
            this IEnumerable<DriverWorkingHourTracking> entries, TimeSpan afterTime)
        {
            var lastEntry = entries.Last();
            return entries.Append(lastEntry.Driver.Login(lastEntry.ActivityDateTime + afterTime));
        }

        public static IEnumerable<DriverWorkingHourTracking> StartWithLogin(this Driver driver, DateTime dateTime)
        {
            return Enumerable.Repeat(driver.Login(dateTime), 1);
        }

        public static DriverWorkingHourTracking Logout(this Driver driver, DateTime dateTime)
        {
            return new DriverWorkingHourTracking(driver.Id, AgentStatusesEnum.Offline)
                .SetDriver(driver)
                .SetActivityDateTime(dateTime);
        }

        public static IEnumerable<DriverWorkingHourTracking> Logout(
            this IEnumerable<DriverWorkingHourTracking> entries, TimeSpan afterTime)
        {
            var lastEntry = entries.Last();
            return entries.Append(lastEntry.Driver.Logout(lastEntry.ActivityDateTime + afterTime));
        }

        public static DriverWorkingHourTracking SetOnDuty(this Driver driver, DateTime dateTime)
        {
            return new DriverWorkingHourTracking(driver.Id, AgentStatusesEnum.Available)
                .SetDriver(driver)
                .SetActivityDateTime(dateTime);
        }

        public static IEnumerable<DriverWorkingHourTracking> SetOnDuty(
            this IEnumerable<DriverWorkingHourTracking> entries, TimeSpan afterTime)
        {
            var lastEntry = entries.Last();
            return entries.Append(lastEntry.Driver.SetOnDuty(lastEntry.ActivityDateTime + afterTime));
        }

        public static DriverWorkingHourTracking SetOffDuty(this Driver driver, DateTime dateTime)
        {
            return new DriverWorkingHourTracking(driver.Id, AgentStatusesEnum.Unavailable)
                .SetDriver(driver)
                .SetActivityDateTime(dateTime);
        }

        public static IEnumerable<DriverWorkingHourTracking> SetOffDuty(
            this IEnumerable<DriverWorkingHourTracking> entries, TimeSpan afterTime)
        {
            var lastEntry = entries.Last();
            return entries.Append(lastEntry.Driver.SetOffDuty(lastEntry.ActivityDateTime + afterTime));
        }

        public static T SetProperty<T>(this T item, string propertyName, object value)
        {
            item.GetType().GetProperty(propertyName).SetValue(item, value);
            return item;
        }

        private static DriverWorkingHourTracking SetDriver(this DriverWorkingHourTracking entry, Driver driver)
        {
            entry.SetProperty(nameof(DriverWorkingHourTracking.Driver), driver);
            return entry;
        }

        private static DriverWorkingHourTracking SetActivityDateTime(this DriverWorkingHourTracking entry, DateTime dateTime)
        {
            entry.SetProperty(nameof(DriverWorkingHourTracking.ActivityDateTime), dateTime);
            return entry;
        }
    }

}
