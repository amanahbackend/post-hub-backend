using Amanah.Posthub.ViewModels;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DMS.Tests
{
    public class LocationAccuracyControllerTests : BaseControllerTests
    {

        [SetUp]
        public void Setup()
        {
            //Arrange
        }

        [Test]
        public async Task GetAllAsync()
        {

            // Act
            var response = await _client.GetAsync("/api/LocationAccuracy/GetAll");

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async Task GetAllByPaginationAsync()
        {

            // Act
            var response = await _client.GetAsync("/api/LocationAccuracy/GetAllByPagination?PageNumber=1&PageSize=1");

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async Task DetailsAsync()
        {

            // Act
            var response = await _client.GetAsync("/api/LocationAccuracy/Details/1");


            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async Task CreateAsync()
        {

            // Act
            var model = new LocationAccuracyViewModel
            {
                Name = "Unit Test Location"
            };
            var json = JsonConvert.SerializeObject(model);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("/api/LocationAccuracy/Create",data);


            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async Task UpdateAsync()
        {

            // Act
            var model = new LocationAccuracyViewModel
            {
                Id = 1,
                Name = "Unit Test Location"
            };
            var json = JsonConvert.SerializeObject(model);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/LocationAccuracy/Update", data);


            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        [Test]
        public async Task DeleteAsync()
        {

            // Act
            var response = await _client.DeleteAsync("/api/LocationAccuracy/Delete/1");

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

    }
}