using NUnit.Framework;

namespace DMS.Tests
{
    public class SettingsControllerTests : BaseControllerTests
    {

        [Test]
        public async System.Threading.Tasks.Task Test1Async()
        {

            // Act
            var response = await _client.GetAsync("/api/values/1");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            // Assert
            Assert.AreEqual("value", responseString);
            //Assert.Pass();
        }
    }
}