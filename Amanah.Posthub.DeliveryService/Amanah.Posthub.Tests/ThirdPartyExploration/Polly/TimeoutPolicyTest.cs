﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using NUnit.Framework;
using Polly;
using Polly.Timeout;
using Shouldly;

namespace DMS.Tests.ThirdPartyExploration.Polly
{
    [Ignore("This test is just for expoloration and mastering purposes of third party tools")]
    public class TimeoutPolicyTest
    {
        private int _retryCount = 0;

        [Test]
        public async Task TimeoutPolicy_ExecuteAsyncTimeoutPessimisticStrategy_ShouldThrow()
        {
            await Should.ThrowAsync<TimeoutRejectedException>(
                Policy
                    .TimeoutAsync(TimeSpan.FromSeconds(1), TimeoutStrategy.Pessimistic)
                    .ExecuteAsync(() => GetStringAsync()));
        }

        [Test]
        public async Task TimeoutPolicy_ExecuteAndCaptureAsyncTimeoutPessimisticStrategy_ReturnFailureResult()
        {
            var result = await Policy
                   .TimeoutAsync(TimeSpan.FromSeconds(1), TimeoutStrategy.Pessimistic)
                   .ExecuteAndCaptureAsync(() => GetStringAsync());
            result.Outcome.ShouldBe(OutcomeType.Failure);
            result.Result.ShouldBeNull();
        }

        [Test]
        public async Task FallBackRetryTimeoutPolicy_ShouldTimeoutAndRetryAndFallback()
        {
            _retryCount = 0;
            var handlePolicy = Policy
                .Handle<TimeoutRejectedException>()
                .Or<HttpRequestException>()
                .Or<WebException>()
                .OrResult((string)null);
            var fallbackPolicy = handlePolicy
                .FallbackAsync((ct) => Task.FromResult(string.Empty));
            var waitAndRetryPloicy = handlePolicy.WaitAndRetryAsync(3, retry => TimeSpan.FromSeconds(retry));
            var timeoutPolicy = Policy.TimeoutAsync(TimeSpan.FromSeconds(1), TimeoutStrategy.Pessimistic)
                .AsAsyncPolicy<string>();
            var finalPolicy = Policy
                .WrapAsync(fallbackPolicy, waitAndRetryPloicy, timeoutPolicy);
            var result = await finalPolicy.WrapAsync(timeoutPolicy)
                .ExecuteAsync(() => GetStringAsync());
            _retryCount.ShouldBe(4);
            result.ShouldBe(string.Empty);
        }

        [Test]
        public async Task TimeoutRetries_ShouldTiemoutOverallRetries()
        {
            var sleeps = Enumerable.Range(1, 15).Select(number => TimeSpan.FromSeconds(0.5));
            var retryPolicy = Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(sleeps);
            var overallTimeoutPolicy = Policy.TimeoutAsync(TimeSpan.FromSeconds(5), TimeoutStrategy.Pessimistic);
            var finalPolicy = Policy.WrapAsync(overallTimeoutPolicy, retryPolicy);
            await Should.ThrowAsync<TimeoutRejectedException>(
                () => finalPolicy.ExecuteAsync(() => GetStringAsync(delay: TimeSpan.FromSeconds(1), throws: true)));
        }

        private async Task<string> GetStringAsync(TimeSpan delay = default, bool throws = false)
        {
            _retryCount++;
            if (delay == default)
            {
                delay = TimeSpan.FromMinutes(1);
            }
            await Task.Delay(delay);
            if (throws)
            {
                throw new Exception();
            }
            return "Result";
        }
    }
}
