﻿using System.Collections.Generic;

namespace DMS.Tests
{
    public class TokenResultViewModel
    {
        public List<string> Roles { set; get; }
        public string Id { set; get; }
        public string UserName { set; get; }
        public string FullName { set; get; }
        public dynamic token { set; get; }
    }
}
