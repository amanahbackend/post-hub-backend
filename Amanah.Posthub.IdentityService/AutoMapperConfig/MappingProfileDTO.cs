﻿using Amanah.Posthub.BLL.ActionLogs.Queries.DTOs;
using Amanah.Posthub.BLL.Addresses.ViewModels;
using Amanah.Posthub.BLL.Branches.Queries.DTOs;
using Amanah.Posthub.BLL.Branches.Services.DTOs;
using Amanah.Posthub.BLL.DriverRegistrations.Queries.DTOs;
using Amanah.Posthub.BLL.Drivers.Queries.DTOs;
using Amanah.Posthub.BLL.Drivers.Services.DTOs;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.BLL.GeoFences.Queries.DTOs;
using Amanah.Posthub.BLL.GeoFences.Services.DTOs;
using Amanah.Posthub.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.BLL.Managers.Services.DTOs;
using Amanah.Posthub.BLL.Queries.DTOs;
using Amanah.Posthub.BLL.Restaurants.Queries.DTOs;
using Amanah.Posthub.BLL.Restaurants.Services.DTOs;
using Amanah.Posthub.BLL.Tasks.AutoAssignServices.DTOs;
using Amanah.Posthub.BLL.Tasks.TaskAssignment.DTOs;
using Amanah.Posthub.BLL.Tasks.WebQueries.DTOs;
using Amanah.Posthub.BLL.Tasks.WebServices.DTOs;
using Amanah.Posthub.BLL.Teams.Queries.DTOs;
using Amanah.Posthub.BLL.Teams.Services.DTOs;
using Amanah.Posthub.BLL.TenantRegistrations;
using Amanah.Posthub.BLL.TenantRegistrations.Queries.DTOs;
using Amanah.Posthub.BLL.TenantRegistrations.Services.DTO;
using Amanah.Posthub.BLL.UserManagement.Queries.DTOs;
using Amanah.Posthub.BLL.UserManagement.Services.DTOs;
using Amanah.Posthub.BLL.ViewModels;
using Amanah.Posthub.DATA.CompanyProfile;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.BusinessCustomers.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.Company.Queries.DTOs;
using Amanah.Posthub.DeliverService.BLL.Company.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.CompanyProfile.Services.DTOs;
using Amanah.Posthub.DeliverService.BLL.Managers.Queries.DTOs;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Admins.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.Service.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Amanah.Posthub.Service.Domain.DriverRegistrations.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.Service.TenantRegistrations.Entities;
using Amanah.Posthub.SharedKernel.Extensions;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Utilities.Extensions;

namespace Amanah.Posthub.IdentityService.AutoMapperConfig
{
    public class MappingProfileDTO : Profile
    {
        /// <summary>
        /// todo Refactor : part of refactor will be added to MappingProfile after finishing  
        /// </summary>
        public MappingProfileDTO()
        {
            CreateMap<DriverResultDTO, Driver>(MemberList.None);
            CreateMap<Driver, DriverResultDTO>(MemberList.None)
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
              .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName))
              .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
              .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
              .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
              .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.User.MiddleName))
              .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.User.LastName) ? string.Empty : src.User.LastName))
              .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => $"DriverImages/" + (string.IsNullOrEmpty(src.ImageUrl) ? "avtar.jpeg" : src.ImageUrl)))
              .ForMember(dest => dest.AgentTypeName, opt => opt.MapFrom(src => src.AgentType.Name))
              .ForMember(dest => dest.AgentStatusName, opt => opt.MapFrom(src => src.AgentStatus != null ? src.AgentStatusId == 6 ? "Available" : src.AgentStatus.Name : string.Empty))
              .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name))
              .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.Name))
              .ForMember(dest => dest.CountryCode, opt => opt.MapFrom(src => src.Country.Code))
              .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name))
              .ForMember(dest => dest.DeviceType, opt => opt.MapFrom(src => src.DeviceType))
              .ForMember(dest => dest.Version, opt => opt.MapFrom(src => src.Version))
              .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.Latitude))
              .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.Longitude))
              .ForMember(dest => dest.LocationAccuracyName, opt => opt.MapFrom(src => src.Team.LocationAccuracy.Name))
              .ForMember(dest => dest.LocationAccuracyDuration, opt => opt.MapFrom(src => src.Team.LocationAccuracy.Duration))
              .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Department.Name_ar : src.Department.Name_en))
              .ForMember(dest => dest.ServiceSectorName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.ServiceSector.Name_ar : src.ServiceSector.Name_en))
              .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Supervisor.FirstName + " " + src.Supervisor.LastName))
             // .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch.Name))
              .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality.Name))
              .ForMember(dest => dest.GenderName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Gender.Name_ar : src.Gender.Name_en))
              .ForMember(dest => dest.AccountStatusName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.AccountStatus.Name_ar : src.AccountStatus.Name_en));
             // .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch.Name))
              //.ForMember(dest => dest.ExistInBranchId, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.BranchId))
            //  .ForMember(dest => dest.BranchId, opt => opt.MapFrom(src => src.BranchId));

            CreateMap<DriverGeoFenceResultDTO, DriverDeliveryGeoFence>(MemberList.None);
            CreateMap<DriverDeliveryGeoFence, DriverGeoFenceResultDTO>(MemberList.None)
                .ForMember(dest => dest.GeoFenceName, opt => opt.MapFrom(src => src.GeoFence.Name));

            CreateMap<DriverGeoFenceResultDTO, DriverPickUpGeoFence>(MemberList.None);
            CreateMap<DriverPickUpGeoFence, DriverGeoFenceResultDTO>(MemberList.None)
                .ForMember(dest => dest.GeoFenceName, opt => opt.MapFrom(src => src.GeoFence.Name));

            CreateMap<Driver, DriverValidForAssigningResultDTO>(MemberList.None)
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FirstName + " " + src.User.LastName))
              .ForMember(dest => dest.AgentStatusName, opt => opt.MapFrom(src => src.AgentStatus.Name));

            CreateMap<Driver, DriverTransportionTypeResultDTO>(MemberList.None)
             .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name));

            CreateMap<CreateDriverInputDTO, ApplicationUser>(MemberList.None)
                 .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                 .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                 .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                 .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                 .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                 .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                 .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());
            CreateMap<CreateDriverInputDTO, Driver>(MemberList.None)
                .ForMember(dest => dest.AgentStatusId, opt => opt.MapFrom(src => (int)AgentStatusesEnum.Offline));
            CreateMap<CreateDriverGeofenceInputDTO, DriverPickUpGeoFence>(MemberList.None);
            CreateMap<CreateDriverGeofenceInputDTO, DriverDeliveryGeoFence>(MemberList.None);

            CreateMap<Driver, AutoAssignDriverDTO>(MemberList.None)
             .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName))
             .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
             .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.User.LastName) ? string.Empty : src.User.LastName))
             .ForMember(dest => dest.BranchId, opt => opt.MapFrom(src => src.CurrentLocation == null ? null : src.CurrentLocation.BranchId));
            CreateMap<DriverDeliveryGeoFence, AutoAssignDriverDeliveryGeofenceDTO>(MemberList.None);


            CreateMap<UpdateDriverInputDTO, ApplicationUser>(MemberList.None)
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());
            CreateMap<UpdateDriverInputDTO, Driver>(MemberList.None)
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.ImageUrl, opt => opt.Ignore())
                .ForMember(dest => dest.DriverPickUpGeoFences, opt => opt.Ignore())
                .ForMember(dest => dest.DriverDeliveryGeoFences, opt => opt.Ignore())
                .ForMember(dest => dest.AgentStatusId, opt => opt.Ignore());
            CreateMap<UpdateDriverGeofenceInputDTO, DriverPickUpGeoFence>(MemberList.None);
            CreateMap<UpdateDriverGeofenceInputDTO, DriverDeliveryGeoFence>(MemberList.None);

            CreateMap<Driver, DriverLoginResponseDTO>(MemberList.None)
              .ForMember(dest => dest.DriverID, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.ProfilePictureURL, opt => opt.MapFrom(src => $"DriverImages/" + (string.IsNullOrEmpty(src.ImageUrl) ? "avtar.jpeg" : src.ImageUrl)))
              .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name))
              .ForMember(dest => dest.LocationAccuracyName, opt => opt.MapFrom(src => src.Team.LocationAccuracy.Name))
              .ForMember(dest => dest.LocationAccuracyDuration, opt => opt.MapFrom(src => src.Team.LocationAccuracy.Duration));
            CreateMap<ApplicationUser, UserResponseDTO>(MemberList.None);

            CreateMap<DriverLoginRequest, DriverLoginRequestResponseDTO>(MemberList.None)
               .ForMember(dest => dest.UpdatedDate, opt => opt.MapFrom(src => src.UpdatedDate.Date == DateTime.MinValue ? (DateTime?)null : src.UpdatedDate))
               .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.Driver.User.UserName))
               .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.Driver.TeamId))
               .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Driver.Team == null ? string.Empty : src.Driver.Team.Name))
               .ForMember(dest => dest.AgentTypeId, opt => opt.MapFrom(src => src.Driver.AgentTypeId))
               .ForMember(dest => dest.AgentTypeName, opt => opt.MapFrom(src => src.Driver.AgentType == null ? string.Empty : src.Driver.AgentType.Name));


            CreateMap<DriverloginTracking, DriverloginTrackingResultDTO>(MemberList.None)
               .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => $"{src.Driver.User.FirstName} {src.Driver.User.LastName}"))
               .ForMember(dest => dest.LogoutDate, opt => opt.MapFrom(src => src.LogoutDate.HasValue && src.LogoutDate.Value.Date < new DateTime(2000, 01, 01).Date
                   ? null
                   : src.LogoutDate));


            CreateMap<MainTask, BLL.Tasks.WebQueries.DTOs.MainTaskResultDTO>(MemberList.None)
                .ForMember(dest => dest.MainTaskStatusName, opt => opt.MapFrom(src => src.MainTaskStatus != null ? src.MainTaskStatus.Name : null))
                .ForMember(dest => dest.MainTaskTypeName, opt => opt.MapFrom(src => src.MainTaskType != null ? src.MainTaskType.Name : null))
                .ForMember(dest => dest.Tasks, opt => opt.MapFrom(src => src.Tasks.Where(x => !x.IsDeleted)))
                .ForMember(dest => dest.Settings, opt => new SettingResultDTO());

            CreateMap<SubTask, SubTaskResultDTO>(MemberList.None)
             .ForMember(dest => dest.DelayTime, opt => opt.MapFrom(src => src.DelayTime))
             .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch != null
                 ? src.Branch.Name
                 : src.Address))
             .ForMember(dest => dest.IsTaskReached, opt => opt.MapFrom(src => src.IsTaskReached ?? false))
             .ForMember(dest => dest.TotalWaitingTime, opt => opt.MapFrom(src => HandleTotalWaitingTime(src)))
             .ForMember(dest => dest.TotalEstimationTime, opt => opt.MapFrom(src => HandleTotalEstimationTime(src.EstimatedTime)))
             .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
             .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
             .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.DriverId.HasValue
                && src.Driver != null
                && src.Driver.User != null
                ? src.Driver.User.FullName
                : string.Empty))
             .ForMember(dest => dest.DriverPhoneNumber, opt => opt.MapFrom(src => src.DriverId.HasValue
                && src.Driver != null
                ? src.Driver.User.PhoneNumber
                : string.Empty))
             .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
             .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
             .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.DriverId.HasValue && src.Driver != null
                ? src.Driver.Team.Name : string.Empty))
             .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.DriverId.HasValue && src.Driver != null
                ? src.Driver.TeamId : (int?)null))
             .ForMember(dest => dest.SignatureFileName, opt => opt.MapFrom(src => src.SignatureFileName))
             .ForMember(dest => dest.SignatureURL, opt => opt.MapFrom(src => $"TaskSignatures/{src.SignatureURL}"))
             .ForMember(dest => dest.DriverRates, opt => opt.MapFrom(src => src.DriverRates))
             .ForMember(dest => dest.FormImage, opt => opt.Ignore());

            CreateMap<MainTask, DashboardMainTaskResultDTO>(MemberList.None)
                .ForMember(dest => dest.MainTaskStatusName, opt => opt.MapFrom(src => src.MainTaskStatus != null
                    ? src.MainTaskStatus.Name : string.Empty))
                .ForMember(dest => dest.MainTaskStatus, opt => opt.MapFrom(src => src.MainTaskStatusId))
                .ForMember(dest => dest.MainTaskTypeName, opt => opt.MapFrom(src => src.MainTaskType != null
                    ? src.MainTaskType.Name : string.Empty))

                .ForMember(dest => dest.DriverId, opt => opt.MapFrom(src => GetMainTaskCurrentDriver(src).Id))
                .ForMember(dest => dest.DriverImageUrl, opt => opt.MapFrom(src => GetMainTaskCurrentDriver(src).ImageUrl))
                .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => GetMainTaskCurrentDriver(src).User.FullName))
                .ForMember(dest => dest.DriverPhoneNumber, opt => opt.MapFrom(src => GetMainTaskCurrentDriver(src).User.PhoneNumber))

                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => GetMainTaskCurrentDriver(src).Team.Name))
                .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => GetMainTaskCurrentDriver(src).TeamId))

                .ForMember(dest => dest.NoOfTasks, opt => opt.MapFrom(src => src.Tasks != null
                    ? src.Tasks.Where(task => !task.IsDeleted).Count() : 0))
                .ForMember(dest => dest.NoOfCompletedTasks, opt => opt.MapFrom(src => src.Tasks != null
                    ? src.Tasks.Where(task => !task.IsDeleted && task.TaskStatusId == (int)TaskStatusEnum.Successful).Count() : 0))
                .ForMember(dest => dest.TotalEstimationTime, opt => opt.MapFrom(src => HandleTotalEstimationTime(src.EstimatedTime)));

            CreateMap<SubTask, DashboardSubTaskResultDTO>(MemberList.None)
                .ForMember(dest => dest.TaskTypeName, opt => opt.MapFrom(src => src.TaskType.Name))
                .ForMember(dest => dest.TaskStatusName, opt => opt.MapFrom(src => src.TaskStatus.Name))
                .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.DriverId.HasValue
                    ? src.Driver.User.FullName : string.Empty))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
                .ForMember(dest => dest.TeamId, opt => opt.MapFrom(src => src.DriverId.HasValue && src.Driver != null
                    ? src.Driver.TeamId : (int?)null));

            CreateMap<Customer, DashboardCustomerResultDTO>(MemberList.None);

            CreateMap<TaskHistory, TaskHistoryResponseDTO>(MemberList.None)
                .ForMember(dest => dest.TaskTypeId, opt => opt.MapFrom(src => src.Task != null ? src.Task.TaskTypeId : 0))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Task != null ? src.Task.Address : string.Empty))
                .ForMember(dest => dest.FromStatusName, opt => opt.MapFrom(src => src.FromStatus.Name))
                .ForMember(dest => dest.ToStatusName, opt => opt.MapFrom(src => src.ToStatus.Name))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy_Id));


            CreateMap<TaskHistory, DriverTimelineResponseDTO>(MemberList.None)
           .ForMember(dest => dest.TaskTypeId, opt => opt.MapFrom(src => src.Task != null ? src.Task.TaskTypeId : 0))
           .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Task != null ? src.Task.Address : string.Empty))
           .ForMember(dest => dest.FromStatusName, opt => opt.MapFrom(src => src.FromStatus.Name))
           .ForMember(dest => dest.ToStatusName, opt => opt.MapFrom(src => src.ToStatus.Name))
           .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Latitude))
           .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Longitude))
           .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.ActionName == "ADDED A SIGNATURE " && !string.IsNullOrEmpty(src.Description)
            ? $"TaskSignatures/{src.Description}"
            : src.ActionName == "ADDED THIS IMAGE " && !string.IsNullOrEmpty(src.Reason)
            ? $"TasksGallary/{src.Reason}"
            : src.Description))
           .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy_Id));


            CreateMap<CreateMainTaskInputDTO, MainTask>(MemberList.None);
            CreateMap<CreateSubTaskInputDTO, SubTask>(MemberList.None)
                .ForMember(dest => dest.DelayTime, opt => opt.Ignore())
                .ForMember(dest => dest.TaskHistories, opt => opt.Ignore())
                .ForMember(dest => dest.StartDate, opt => opt.Ignore())
                .ForMember(dest => dest.SuccessfulDate, opt => opt.Ignore())
                .ForMember(dest => dest.TotalTime, opt => opt.Ignore())
                .ForMember(dest => dest.TaskGallaries, opt => opt.Ignore());
            CreateMap<CreateTaskGallaryInputDTO, TaskGallary>(MemberList.None);
            CreateMap<CreateCustomerInputDTO, Customer>(MemberList.None);
            CreateMap<CreateAddressInputDTO, Address>(MemberList.None);

            CreateMap<MainTask, MainTaskAutoAssignInputDTO>(MemberList.None)
               .ForMember(dest => dest.BranchId, opt => opt.MapFrom(src => src.Tasks.FirstOrDefault(t => t.TaskTypeId == (int)TaskTypesEnum.Pickup).BranchId))
               .ForMember(dest => dest.GeofenceId, opt => opt.MapFrom(src => src.Tasks.FirstOrDefault(t => t.TaskTypeId == (int)TaskTypesEnum.Delivery).GeoFenceId));

            CreateMap<UpdateSubTaskInputDTO, SubTask>(MemberList.None)
               .ForMember(dest => dest.DelayTime, opt => opt.Ignore())
               .ForMember(dest => dest.TaskHistories, opt => opt.Ignore())
               .ForMember(dest => dest.StartDate, opt => opt.Ignore())
               .ForMember(dest => dest.SuccessfulDate, opt => opt.Ignore())
               .ForMember(dest => dest.TotalTime, opt => opt.Ignore())
               .ForMember(dest => dest.TaskGallaries, opt => opt.Ignore());
            CreateMap<UpdateTaskGallaryInputDTO, TaskGallary>(MemberList.None);
            CreateMap<UpdateCustomerInputDTO, Customer>(MemberList.None);
            CreateMap<UpdateAddressInputDTO, Address>(MemberList.None);


            CreateMap<Team, GeofenceTeamResponseDTO>(MemberList.None);
            CreateMap<GeoFence, GeofenceResponseDTO>(MemberList.None)
                .ForMember(dest => dest.Locations, opt => opt.MapFrom(src => src.GeoFenceLocations != null || src.GeoFenceLocations.Count > 0
                    ? src.GeoFenceLocations.Select(t => new GeoFenceLocationResponseDTO
                    {
                        Latitude = t.Latitude,
                        Longitude = t.Longitude
                    }).ToList()
                    : new List<GeoFenceLocationResponseDTO>()))
                .ForMember(dest => dest.GeoFenceBlocks, opt => opt.MapFrom(src => src.GeoFenceBlocks != null || src.GeoFenceBlocks.Count > 0
                    ? src.GeoFenceBlocks.Where(c => !c.IsDeleted).Select(t => new GeoFenceBlocksResponseDTO
                    {
                        Area = !String.IsNullOrEmpty(t.Area) ? t.Area : "",
                        Block = !String.IsNullOrEmpty(t.Block) ? t.Block : "",
                        BlockId = t.Id > 0 ? t.Id : 0
                    }).ToList()
                    : new List<GeoFenceBlocksResponseDTO>()));

           // CreateMap<GeoFenceBlocks, GeoFenceBlocksResponseDTO>(MemberList.None);


            CreateMap<CreateGeofenceInputDTO, GeoFence>(MemberList.None)
               .ForMember(dest => dest.GeoFenceBlocks, opt => opt.MapFrom(src => src.GeoFenceBlocks))
                .ForMember(dest => dest.GeoFenceLocations, opt => opt.MapFrom(src => src.Locations.Select(location => new GeoFenceLocation
                {
                    Latitude = location.Latitude,
                    Longitude = location.Longitude,
                    PointIndex = location.PointIndex
                }))

               );


            CreateMap<CreateGeoFenceBlockInputDTO, GeoFenceBlocks>(MemberList.None);
            CreateMap<UpdateGeoFenceBlocksInputDTO, GeoFenceBlocks>(MemberList.None);



            CreateMap<UpdateGeofenceInputDTO, GeoFence>(MemberList.None)
               .ForMember(dest => dest.GeoFenceBlocks, opt => opt.MapFrom(src => src.GeoFenceBlocks))
                .ForMember(dest => dest.GeoFenceLocations, opt => opt.MapFrom(src => src.Locations.Select(location => new GeoFenceLocation
                {
                    Id = location.Id,
                    GeoFenceId = src.Id,
                    Latitude = location.Latitude,
                    Longitude = location.Longitude,
                    PointIndex = location.PointIndex
                })));


            CreateMap<Manager, ProfileResponseDTO>(MemberList.None)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FullName))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
                .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.User.MiddleName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.User.CountryId))
                .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality.Name))
                .ForMember(dest => dest.ResidentCountryId, opt => opt.MapFrom(src => src.User.CountryId))
                .ForMember(dest => dest.ProfilePhotoURL, opt => opt.MapFrom(src => $"StaffImages/" + (string.IsNullOrEmpty(src.ProfilePicURL) ? "avtar.jpeg" : src.ProfilePicURL)))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber));

            CreateMap<Admin, DefaultBranchResponseDTO>(MemberList.None)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.BranchId ?? 0))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Address : string.Empty))
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.CountryId : null))
                .ForMember(dest => dest.GeoFenceId, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.GeoFenceId : 0))
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.Branch != null && src.Branch.IsActive))
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Latitude : null))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Longitude : null))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Name : string.Empty))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.Phone : string.Empty))
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Branch != null && src.Branch.Customer != null ? GetDefaultCustomerResponseDTO(src.Branch.Customer) : new DefaultCustomerResponseDTO()))
                .ForMember(dest => dest.RestaurantId, opt => opt.MapFrom(src => src.Branch != null ? src.Branch.RestaurantId : 0));

            CreateMap<Country, CountryResposeDTO>(MemberList.None)
                .ForMember(dest => dest.FlagUrl, opt => opt.MapFrom((src) => "CountryFlags/" + src.Flag));
            CreateMap<TeamManager, ManagerTeamResposeDTO>(MemberList.None)
                .ForMember(dest => dest.IsDeleted, opt => opt.MapFrom(src => src.Team.IsDeleted))
                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name));
            CreateMap<Manager, ManagerResponseDTO>(MemberList.None)
                .BeforeMap((src, dest) =>
                {
                    src.TeamManagers = src.TeamManagers.Where(manager => manager.Team != null && !manager.Team.IsDeleted).ToList();
                })
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
                .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.User.MiddleName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.Id))
                .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.User.CountryId))
                .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Department.Name_ar : src.Department.Name_en))
                .ForMember(dest => dest.ServiceSectorName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.ServiceSector.Name_ar : src.ServiceSector.Name_en))
                .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Supervisor.FirstName + " " + src.Supervisor.LastName))
                .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch.Name))
                .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality.Name))
                .ForMember(dest => dest.GenderName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Gender.Name_ar : src.Gender.Name_en))
                .ForMember(dest => dest.AccountStatusName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.AccountStatus.Name_ar : src.AccountStatus.Name_en))
                .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch.Name))
                .ForMember(dest => dest.ProfilePicURL, opt => opt.MapFrom(src => $"StaffImages/" + (string.IsNullOrEmpty(src.ProfilePicURL) ? "avtar.jpeg" : src.ProfilePicURL)))
                .ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.User.Country));

            CreateMap<Manager, ExportManagerResponseDTO>(MemberList.None)
            .BeforeMap((src, dest) =>
            {
                src.TeamManagers = src.TeamManagers.Where(manager => manager.Team != null && !manager.Team.IsDeleted).ToList();
            })
            .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
            .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
            .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))
            .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.TeamManagers.FirstOrDefault() != null ? src.TeamManagers.FirstOrDefault().Team.Name : null))
            .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Department.Name_ar : src.Department.Name_en))
            .ForMember(dest => dest.ServiceSectorName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.ServiceSector.Name_ar : src.ServiceSector.Name_en))
            .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Supervisor.FirstName + " " + src.Supervisor.LastName))
            .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch.Name))
            .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality.Name))
            .ForMember(dest => dest.GenderName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Gender.Name_ar : src.Gender.Name_en))
            .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch.Name));


            CreateMap<CreateManagerInputDTO, ApplicationUser>(MemberList.None)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId));

            CreateMap<CreateManagerTeamInputDTO, TeamManager>(MemberList.None);
            CreateMap<CreateManagerInputDTO, Manager>(MemberList.None)
                .ForMember(dest => dest.TeamManagers, opt => opt.MapFrom(src => src.TeamManagers));


            CreateMap<UpdateManagerInputDTO, ApplicationUser>(MemberList.None)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId));

            CreateMap<UpdateManagerTeamInputDTO, TeamManager>(MemberList.None);
            CreateMap<UpdateManagerInputDTO, Manager>(MemberList.None)
                                .ForMember(dest => dest.TeamManagers, opt => opt.MapFrom(src => src.TeamManagers));

            CreateMap<CreateRestaurantInputDTO, Restaurant>(MemberList.None);
            CreateMap<UpdateRestaurantInputDTO, Restaurant>(MemberList.None);
            CreateMap<Restaurant, RestaurantResponseDTO>(MemberList.None);



            CreateMap<ManagerDispatching, ManagerDispatchingResultDTO>(MemberList.None)
                .ForMember(dest => dest.ManagerName, opt => opt.MapFrom(src => src.Manager.User.FullName));

            CreateMap<CreateManagerDispatchingInputDTO, ManagerDispatching>(MemberList.None);
            CreateMap<UpdateManagerDispatchingInputDTO, ManagerDispatching>(MemberList.None);

            CreateMap<ApplicationRole, AccessControlResultDTO>(MemberList.None)
              .ForMember(dest => dest.RoleName, opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.CreationDate, opt => opt.MapFrom(src => src.CreatedDate))
              .ForMember(dest => dest.Permissions, opt => opt.MapFrom(src => src.RoleClaims.Select(x => x.ClaimValue).ToList()));

            CreateMap<CreateAccessControlInputDTO, ApplicationRole>(MemberList.None)
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.RoleName))
              .ForMember(dest => dest.Type, opt => opt.MapFrom(src => (int)src.Type))
              .ForMember(dest => dest.RoleClaims, opt => opt.Ignore());

            CreateMap<UpdateAccessControlInputDTO, ApplicationRole>(MemberList.None)
              .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.RoleName))
              .ForMember(dest => dest.Type, opt => opt.MapFrom(src => (int)src.Type))
              .ForMember(dest => dest.RoleClaims, opt => opt.Ignore());





            CreateMap<TenantRegistration, TenantRegistrationRequestsDetailedResultDTO>(MemberList.None)
              .ForMember(dest => dest.SocialMediaAccounts, opt => opt.MapFrom(src => src.SocialMediaAccounts != null ?
                 src.SocialMediaAccounts.Select(account => account.AccountLink).ToList() : null));

            CreateMap<TenantRegistration, TenantRegistrationRequestsResultDTO>(MemberList.None)
           .ForMember(
               dest => dest.ApprovedOrRejectedByUserFullName,
               opt => opt.MapFrom(src =>
                   src.ApprovedORejectedByUser != null
                       ? src.ApprovedORejectedByUser.FirstName + " " + src.ApprovedORejectedByUser.LastName
                       : string.Empty))
           .ForMember(dest => dest.RegistrationDate, opt => opt.MapFrom(src => src.CreatedDate))
           .ForMember(
               viewModel => viewModel.BusinessAddressSummary,
               options => options.MapFrom(registration =>
                   Thread.CurrentThread.CurrentCulture.IsArabic()
                       ? registration.BusinessAddressSummaryArabic
                       : registration.BusinessAddressSummaryEnglish));


            CreateMap<CreateTenantRegistrationDTO, TenantRegistration>(MemberList.None)
                 .ForMember(dest => dest.SocialMediaAccounts, opt => opt.MapFrom(src => src.SocialMediaAccounts != null
                     ? src.SocialMediaAccounts.Select(account => new TenantSocialMediaAccount
                     {
                         AccountLink = account,
                     }).ToList()
                     : new List<TenantSocialMediaAccount>()))
                 .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.CountryId > 0 ? src.CountryId : 121))
                 .ForMember(registration => registration.Id, options => options.Ignore())
                 .ForMember(registration => registration.Attachments, options => options.Ignore())
                 .ForMember(registration => registration.UserRegistrationStatus, options => options.Ignore())
                 .ForMember(registration => registration.ApprovedORejectedByUserId, options => options.Ignore());


            CreateMap<UpdateTenantRegistrationDetailsDTO, TenantRegistration>(MemberList.None)
               .ForMember(dest => dest.SocialMediaAccounts, opt => opt.MapFrom(src => src.SocialMediaAccounts != null
                   ? src.SocialMediaAccounts.Select(account => new TenantSocialMediaAccount
                   {
                       AccountLink = account,
                       TenantRegistrationId = src.Id
                   }).ToList()
                   : new List<TenantSocialMediaAccount>()))
               .ForMember(registration => registration.Id, options => options.Ignore())
               .ForMember(registration => registration.Attachments, options => options.Ignore())
               .ForMember(registration => registration.UserRegistrationStatus, options => options.Ignore())
               .ForMember(registration => registration.ApprovedORejectedByUserId, options => options.Ignore());
            CreateMap<TenantRegistration, UpdateTenantRegistrationDetailsDTO>(MemberList.None)
               .ForMember(dest => dest.SocialMediaAccounts, opt => opt.MapFrom(src => src.SocialMediaAccounts != null
                ? src.SocialMediaAccounts.Select(account => account.AccountLink).ToList() : null));

            CreateMap<TenantRegistrationAttachment, TenantRegistrationAttachmentDTO>(MemberList.None).ReverseMap();

            CreateMap<TenantRegistrationAttachment, DriverRegistrationRequestsResultDTO>(MemberList.None).ReverseMap();

            ///Driver registration DTOs
            CreateMap<DriverRegistration, DriverRegistrationRequestsResultDTO>(MemberList.None)
              .ForMember(dest => dest.ApprovedOrRejectedByUserFullName, opt => opt.MapFrom(src => src.ApprovedORejectedByUser != null ?
                  src.ApprovedORejectedByUser.FirstName + " " + src.ApprovedORejectedByUser.LastName : string.Empty))
              .ForMember(dest => dest.RegistrationDate, opt => opt.MapFrom(src => src.CreatedDate))
              .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name));
            CreateMap<DriverAttachmentDTO, DriverAttachment>(MemberList.None).ReverseMap();

            CreateMap<DriverRegistration, DriverRegistrationRquestDetailsResultDTO>().ReverseMap()
               .ForMember(registration => registration.PasswordHash, options => options.Ignore())
               .ForMember(registration => registration.SecurityStamp, options => options.Ignore())
               .ForMember(registration => registration.Id, options => options.Ignore())
               .ForMember(registration => registration.DriverAttachments, options => options.Ignore())
               .ForMember(registration => registration.UserRegistrationStatus, options => options.Ignore())
               .ForMember(registration => registration.ApprovedORejectedByUserId, options => options.Ignore());

            CreateMap<CreateDriverRegistrationInputDTO, DriverRegistration>(MemberList.None)
                .ConstructUsing(driverRegistrationVM => new DriverRegistration(driverRegistrationVM.Tenant_Id));
            CreateMap<DriverRegistration, CreateDriverRegistrationInputDTO>(MemberList.None)
                .ForMember(dest => dest.ApprovedOrRejectedByUserFullName, opt => opt.MapFrom(src => src.ApprovedORejectedByUser != null
                    ? $"{src.ApprovedORejectedByUser.FirstName} {src.ApprovedORejectedByUser.LastName}"
                    : string.Empty))
                .ForMember(dest => dest.RegistrationDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.TransportTypeName, opt => opt.MapFrom(src => src.TransportType.Name))
                .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.Name))
                .ForMember(dest => dest.CountryCode, opt => opt.MapFrom(src => src.Country.Code));
            CreateMap<DriverRegistration, UpdateDriverRegistrationDetailsInputDTO>().ReverseMap()
                .ForMember(registration => registration.PasswordHash, options => options.Ignore())
                .ForMember(registration => registration.SecurityStamp, options => options.Ignore())
                .ForMember(registration => registration.Id, options => options.Ignore())
                .ForMember(registration => registration.DriverAttachments, options => options.Ignore())
                .ForMember(registration => registration.UserRegistrationStatus, options => options.Ignore())
                .ForMember(registration => registration.ApprovedORejectedByUserId, options => options.Ignore());

            CreateMap<Branch, BranchResponseDTO>(MemberList.None)
                     .ForMember(dest => dest.BranchManagerId, opt => opt.MapFrom(src => src.BranchManagerId))
                     .ForMember(dest => dest.BranchManagerName, opt => opt.MapFrom(src => src.BranchManager.FullName));


            CreateMap<Branch, ExportBranchResponseDTO>(MemberList.None)
                     .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                     .ForMember(dest => dest.Governorate, opt => opt.MapFrom(src => src.Location.Governorate))
                     .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.Location.Area))
                     .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.Location.Block))
                     .ForMember(dest => dest.Flat, opt => opt.MapFrom(src => src.Location.Flat))
                     .ForMember(dest => dest.Floor, opt => opt.MapFrom(src => src.Location.Floor))
                     .ForMember(dest => dest.Building, opt => opt.MapFrom(src => src.Location.Building)).ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            CreateMap<Restaurant, BranchRestaurantResponseDTO>(MemberList.None);
            CreateMap<Country, BranchCustomerCountryResponseDTO>(MemberList.None);
            CreateMap<Address, BranchAddressResponseDTO>(MemberList.None);
            CreateMap<Customer, BranchCustomerResponseDTO>(MemberList.None);
            CreateMap<CreateBranchInputDTO, Customer>(MemberList.None)
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => $"{src.Name.Trim().ToLower().RemoveSpecialCharacters()}_{Guid.NewGuid()}@dhub.com"));
            CreateMap<UpdateBranchInputDTO, Customer>(MemberList.None)
                .ForMember(dest => dest.Id, src => src.Ignore())
                .ForMember(dest => dest.CreatedBy_Id, src => src.Ignore())
                .ForMember(dest => dest.CreatedDate, src => src.Ignore());

            CreateMap<Branch, CompanyBranchResponseDTO>(MemberList.None);


            CreateMap<CreateBranchAddressInputDTO, Address>(MemberList.None);
            CreateMap<UpdateBranchAddressInputDTO, Address>(MemberList.None);

            CreateMap<CreateBranchInputDTO, Branch>(MemberList.None)
                .ForMember(dest => dest.Restaurant, src => src.Ignore());
            CreateMap<UpdateBranchInputDTO, Branch>(MemberList.None)
                .ForMember(dest => dest.Restaurant, src => src.Ignore())
                .ForMember(dest => dest.Customer, opt => opt.Ignore());

            CreateMap<AccountLogs, AccountLogResponseDTO>(MemberList.None);

            CreateMap<Team, TeamResponseDTO>(MemberList.None)
               .ForMember(dest => dest.LocationAccuracyName, opt => opt.MapFrom(src => src.LocationAccuracy.Name));
            CreateMap<CreateTeamInputDTO, Team>(MemberList.None)
                 .ForMember(dest => dest.Drivers, opt => opt.Ignore());
            CreateMap<UpdateTeamInputDTO, Team>(MemberList.None)
                 .ForMember(dest => dest.Drivers, opt => opt.Ignore());
            CreateMap<Driver, TeamDriverResponseDTO>(MemberList.None)
                 .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FullName));
            CreateMap<TeamManager, TeamManagerResponseDTO>(MemberList.None);

            CreateMap<SubTask, ExportTaskWithoutProgressResponseDTO>(MemberList.None)
               .ForMember(dest => dest.Order_Type, opt => opt.MapFrom(src => src.TaskType.Name))
               .ForMember(dest => dest.Order_Status, opt => opt.MapFrom(src => src.TaskStatus.Name))
               .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Customer.Name))
               .ForMember(dest => dest.Order_DateTime, opt => opt.MapFrom(src => src.PickupDate ?? src.DeliveryDate))
               .ForMember(dest => dest.Driver_Name, opt => opt.MapFrom(src => src.Driver.User.FullName))
               .ForMember(dest => dest.Reference_Image, opt => opt.MapFrom(src => src.Image))
               .ForMember(dest => dest.Order_Description, opt => opt.MapFrom(src => src.Description));

            CreateMap<Driver, ExportDriverResponseDTO>(MemberList.None)
             .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
             .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FullName))
             .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))

             .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.User.PhoneNumber))
             .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Department.Name_ar : src.Department.Name_en))
             .ForMember(dest => dest.ServiceSectorName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.ServiceSector.Name_ar : src.ServiceSector.Name_en))
             .ForMember(dest => dest.SupervisorName, opt => opt.MapFrom(src => src.Supervisor.FirstName + " " + src.Supervisor.LastName))
             //.ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.Branch.Name))
             .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality.Name));


            CreateMap<Company, CompanyProfileDetailsDTO>(MemberList.None)
                 .ForMember(dest => dest.LogoUrl, opt => opt.MapFrom(src => $"CompanyImages/" + (string.IsNullOrEmpty(src.LogoUrl) ? "avtar.jpeg" : src.LogoUrl)))
                 .ForMember(dest => dest.MainBranch, opt => opt.MapFrom(src => src.MainBranch))
                 .ForMember(dest => dest.Branches, opt => opt.MapFrom(src => src.Branches))
                 .ForMember(dest => dest.WorkingHours, opt => opt.MapFrom(src => src.WorkingHours));

            CreateMap<UpdateCompanyProfileDTO, Company>(MemberList.None)
                                 .ForMember(dest => dest.LogoUrl, opt => opt.Ignore());

            CreateMap<UpdateWorkinghoursDTO, WorkingHours>(MemberList.None);
            CreateMap<CreateWorkinghoursInputDTO, WorkingHours>(MemberList.None);
            CreateMap<WorkingHours, CompanyWorkingHoursResultDTO>(MemberList.None);
            CreateMap<CompanySocialLinksInputDTO, CompanySocialLinks>(MemberList.None);
            CreateMap<CompanySocialLinks, SocialLinksResponseDTO>(MemberList.None);


            CreateMap<Department, DepartmentResponseDTO>(MemberList.None);
            CreateMap<ServiceSector, ServiceSectorResponseDTO>(MemberList.None);
            CreateMap<AccountStatus, AccountStatusResponseDTO>(MemberList.None);
            CreateMap<Gender, GenderResponseDTO>(MemberList.None);

            ///Business Customer
            CreateMap<ContactFunction, ContactFunctionResponseDTO>(MemberList.None);
            CreateMap<CollectionMethod, CollectionMethodResponseDTO>(MemberList.None);
            CreateMap<Courtesy, CourtesyResponseDTO>(MemberList.None);
            ///Create
            CreateMap<CreateBusinessCustomerInputDTO, BusinessCustomer>(MemberList.None);
            CreateMap<CreateBusinessCustomerContactInputDTO, BusinessContactAccount>(MemberList.None);
            CreateMap<CreateBusinessCustomerContactInputDTO, ApplicationUser>(MemberList.None)
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(x => x.FullName))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(x => x.Phone))
                .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore()); ;
            CreateMap<CreateBusinessCustomerBranchInputDTO, BusinessCustomerBranch>(MemberList.None);

            CreateMap<BusinessCustomer, BusinessCustomerListResponseDTO>(MemberList.None)
                .ForMember(dest => dest.BusinessTypeName, opt =>
                        opt.MapFrom(src => src.BusinessType != null ?
                        Thread.CurrentThread.CurrentCulture.IsArabic()
                          ? src.BusinessType.Name_ar : src.BusinessType.Name_en : ""))
                .ForMember(dest => dest.CollectionMethodName, opt => opt.MapFrom(src => src.CollectionMethod != null ?
                        Thread.CurrentThread.CurrentCulture.IsArabic() ? src.CollectionMethod.Name_ar : src.CollectionMethod.Name_en : ""))
                .ForMember(dest => dest.BusinessLogoUrl, opt => opt.MapFrom(src => $"BusinessCustomerLogos/" + (string.IsNullOrEmpty(src.BusinessLogoUrl) ? "avtar.jpeg" : src.BusinessLogoUrl)))
                .ForMember(dest => dest.JoiningDate, opt => opt.MapFrom(x => x.CreatedDate))
                .ForMember(dest => dest.HQBranchAddress, opt => opt.MapFrom(x => x.HQBranch != null ? x.HQBranch.Address : ""))
                .ForMember(dest => dest.HQBranchLocation, opt => opt.MapFrom(x => x.HQBranch != null ?
                         x.HQBranch.Location : null))
                .ForMember(dest => dest.AccountType, opt => opt.MapFrom(x => x.AccountType != null ? x.AccountType : 1))
                .ForMember(dest => dest.AccountStatusId, opt => opt.MapFrom(x => x.MainContact != null ? x.MainContact.AccountStatusId : 1))
                .ForMember(dest => dest.JoiningDate, opt => opt.MapFrom(x => x.CreatedDate));

            CreateMap<BusinessCustomer, BusinessCustomerListBusinessIndustryResponseDTO>(MemberList.None);


            CreateMap<BusinessCustomer, ExportBusinessCustomerListResponseDTO>(MemberList.None)
                .ForMember(dest => dest.BusinessTypeName, opt =>
                        opt.MapFrom(src => src.BusinessType != null ?
                        Thread.CurrentThread.CurrentCulture.IsArabic()
                          ? src.BusinessType.Name_ar : src.BusinessType.Name_en : ""))
                .ForMember(dest => dest.CollectionMethodName, opt => opt.MapFrom(src => src.CollectionMethod != null ?
                        Thread.CurrentThread.CurrentCulture.IsArabic() ? src.CollectionMethod.Name_ar : src.CollectionMethod.Name_en : ""))
                .ForMember(dest => dest.JoiningDate, opt => opt.MapFrom(x => x.CreatedDate))
                .ForMember(dest => dest.HQBranchAddress, opt => opt.MapFrom(x => x.HQBranch != null ? x.HQBranch.Address : ""))
                .ForMember(dest => dest.HQBranchLocation, opt => opt.MapFrom(x => x.HQBranch != null ?
                         x.HQBranch.Location : null))
                .ForMember(dest => dest.JoiningDate, opt => opt.MapFrom(x => x.CreatedDate));

            CreateMap<BusinessCustomer, BusinessCustomerDetailsResponseDTO>(MemberList.None)
                .ForMember(dest => dest.BusinessTypeName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.BusinessType != null ? src.BusinessType.Name_ar : "" : src.BusinessType != null ? src.BusinessType.Name_en : ""))
                .ForMember(dest => dest.CollectionMethodName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.CollectionMethod != null ? src.CollectionMethod.Name_ar : "" : src.CollectionMethod != null ? src.CollectionMethod.Name_en : ""))
                .ForMember(dest => dest.BusinessLogoUrl, opt => opt.MapFrom(src => $"BusinessCustomerLogos/" + (string.IsNullOrEmpty(src.BusinessLogoUrl) ? "avtar.jpeg" : src.BusinessLogoUrl)))
                .ForMember(dest => dest.JoiningDate, opt => opt.MapFrom(x => x.CreatedDate))
                .ForMember(dest => dest.HQBranchAddress, opt => opt.MapFrom(x => x.HQBranch != null ? x.HQBranch.Address : ""))
                .ForMember(dest => dest.HQBranchLocation, opt => opt.MapFrom(x => x.HQBranch != null ? x.HQBranch.Location : null))
                .ForMember(dest => dest.BusinessCID, opt => opt.MapFrom(x => x.BusinessCID))
                .ForMember(dest => dest.BusinessCode, opt => opt.MapFrom(x => x.BusinessCode))
                .ForMember(dest => dest.BusinessName, opt => opt.MapFrom(x => x.BusinessName))
                .ForMember(dest => dest.BusinessTypeId, opt => opt.MapFrom(x => x.BusinessTypeId))
                .ForMember(dest => dest.BusinessTypeName, opt => opt.MapFrom(x => x.BusinessType != null ? x.BusinessType.Name_en : ""))
                .ForMember(dest => dest.BusinessCustomerCode, opt => opt.MapFrom(x => x.BusinessCode))
                .ForMember(dest => dest.HQBranchName, opt => opt.MapFrom(x => x.HQBranch != null ? x.HQBranch.Name : ""))
                .ForMember(dest => dest.HQBranchId, opt => opt.MapFrom(x => x.HQBranch.Id))
                .ForMember(dest => dest.BusinessCustomerId, opt => opt.MapFrom(x => x.Id))
                .ForMember(dest => dest.MainContactId, opt => opt.MapFrom(x => x.MainContactId));

            //.ForMember(dest => dest.ContractCode, opt => opt.MapFrom(x => x.BusinessOrders != null ? x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id) != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id).Contract != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id).Contract.Code : "" : "" : ""))
            //.ForMember(dest => dest.ContractStatus, opt => opt.MapFrom(x => x.BusinessOrders != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id) != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id).Contract != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id).Contract.ContractStatus : null : null : null))
            //.ForMember(dest => dest.ContractId, opt => opt.MapFrom(x => x.BusinessOrders != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id) != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id).ContractId : null : null))
            //.ForMember(dest => dest.OrderById, opt => opt.MapFrom(x => x.BusinessOrders != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id) != null ?
            //x.BusinessOrders.FirstOrDefault(c => c.BusinessCustomerId == x.Id).OrderById : null : null))
            //.ForMember(dest => dest.JoiningDate, opt => opt.MapFrom(x => x.CreatedDate));


            CreateMap<BusinessContactAccount, BusinessContactAccountListResponseDTO>(MemberList.None)
            .ForMember(dest => dest.ContactFunctionName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.ContactFunction.Name_ar : src.ContactFunction.Name_en))
            .ForMember(dest => dest.AccountStatusName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.AccountStatus.Name_ar : src.AccountStatus.Name_en))
            .ForMember(dest => dest.CourtesyName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Courtesy.Name_ar : src.Courtesy.Name_en))
            .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.User.Email))
            .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.User.CountryId))
            .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.User.PhoneNumber))
            .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FirstName))
            .ForMember(dest => dest.PersonalPhotoURL, opt => opt.MapFrom(src => $"BusinessContactProfile/" + (string.IsNullOrEmpty(src.PersonalPhotoURL) ? "avtar.jpeg" : src.PersonalPhotoURL)));

            CreateMap<BusinessCustomerBranch, BusinessCustomerBranchReponseDTO>(MemberList.None);

            ///
            CreateMap<UpdateBusinessCustomerInputDTO, BusinessCustomer>(MemberList.None);
            CreateMap<UpdateBusinessCustomerBranchInputDTO, BusinessCustomerBranch>(MemberList.None);
            CreateMap<UpdateBusinessCustomerContactInputDTO, BusinessContactAccount>(MemberList.None);
            CreateMap<UpdateBusinessCustomerContactInputDTO, ApplicationUser>(MemberList.None)
                          .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.UserId))
                          .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(x => x.Phone))
                          .ForMember(dest => dest.FirstName, opt => opt.MapFrom(x => x.FullName))
                          .ForMember(dest => dest.NormalizedUserName, opt => opt.Ignore())
                          .ForMember(dest => dest.NormalizedEmail, opt => opt.Ignore())
                          .ForMember(dest => dest.PasswordHash, opt => opt.Ignore())
                          .ForMember(dest => dest.SecurityStamp, opt => opt.Ignore())
                          .ForMember(dest => dest.ConcurrencyStamp, opt => opt.Ignore())
                          .ForMember(dest => dest.TwoFactorEnabled, opt => opt.Ignore())
                          .ForMember(dest => dest.LockoutEnd, opt => opt.Ignore())
                          .ForMember(dest => dest.LockoutEnabled, opt => opt.Ignore())
                          .ForMember(dest => dest.AccessFailedCount, opt => opt.Ignore());


            CreateMap<Block, BlockViewModel>(MemberList.None)
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.BlockNo))
                 .ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.BlockNo));




        }

        private static Driver GetMainTaskCurrentDriver(MainTask mainTask)
        {
            var driver = mainTask.Tasks?.Where(task => task.DriverId.HasValue && task.Driver != null).LastOrDefault()
                    ?.Driver;
            if (driver == null)
            {
                driver = new Driver
                {
                    User = new ApplicationUser(),
                    Team = new Team()
                };
            }

            return driver;
        }
        private static DashboardTimeResultDTO HandleTotalWaitingTime(SubTask src)
        {
            DashboardTimeResultDTO waitingTime = null;
            if (src.IsTaskReached.HasValue && src.ReachedTime.HasValue && src.StartDate.HasValue)
            {
                TimeSpan differenceInTime = (src.StartDate.Value - src.ReachedTime.Value);
                waitingTime = new DashboardTimeResultDTO
                {
                    Day = differenceInTime.Days,
                    Hour = differenceInTime.Hours,
                    Minute = differenceInTime.Minutes,
                    Second = differenceInTime.Seconds
                };
            }
            return waitingTime;
        }
        private static DashboardTimeResultDTO HandleTotalEstimationTime(double? estimationTime)
        {
            DashboardTimeResultDTO EstimationTime = null;
            if (estimationTime.HasValue)
            {
                TimeSpan EstimationTimeSpan = TimeSpan.FromSeconds((double)estimationTime);
                EstimationTime = new DashboardTimeResultDTO
                {
                    Day = EstimationTimeSpan.Days,
                    Hour = EstimationTimeSpan.Hours,
                    Minute = EstimationTimeSpan.Minutes,
                    Second = EstimationTimeSpan.Seconds
                };
            }
            return EstimationTime;
        }
        private static DefaultCustomerResponseDTO GetDefaultCustomerResponseDTO(Customer customer)
        {
            return new DefaultCustomerResponseDTO
            {
                Id = customer.Id,
                Name = customer.Name,
                Address = customer.Address,
                BranchId = customer.BranchId,
                CountryId = customer.CountryId,
                Email = customer.Email,
                Latitude = customer.Latitude ?? 0,
                Longitude = customer.Longitude ?? 0,
                Phone = customer.Phone,
                Tags = customer.Tags
            };
        }

    }
}
