﻿using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.ViewModels.Account;
using Amanah.Posthub.SharedKernel.Common.Reporting;
using Amananh.AppController.Validator.ResourceFilter;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.FeatureManagement.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Utilities.Utilites.Localization.Keys;

namespace Amanah.Posthub.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    //[FeatureGate(FeatureManagementFlags.Concierge)]
    public class AccountController : BaseController
    {
        protected readonly IApplicationUserManager _userManager;
        protected readonly ISettingsManager _settingsManager;
        protected readonly IWebHostEnvironment _webHostEnvirnoment;
        protected readonly IRDLCrenderService _RDLCrenderService;



        public AccountController(
            IApplicationUserManager userManager,
            ISettingsManager settingsManager,
            IWebHostEnvironment webHostEnvirnoment,
            IRDLCrenderService RDLCrenderService)
        {
            _userManager = userManager;
            _settingsManager = settingsManager;
            _webHostEnvirnoment = webHostEnvirnoment;
            _RDLCrenderService = RDLCrenderService;
        }



        [HttpGet("print")]
        public IActionResult Print()
        {
            var path = $"{this._webHostEnvirnoment.ContentRootPath}\\Report1.rdlc";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("ReportParameter1", "ASP.NET CORE RDLC Report");

            var result = _RDLCrenderService.RenderRdlc(path, ExportReportType.Pdf, parameters, null);
            return File(result, "application/pdf");

            //    string mimtype = "";
            //    int extension = 1;
            //    
            //    //get products from product table 
            //    LocalReport localReport = new LocalReport(path);
            //    var result = localReport.Execute(RenderType.Pdf, extension, parameters, mimtype);
            //    return File(result.MainStream, "application/pdf");

        }


        [HttpGet("ResendResetPasswordEmail")]
        public async Task<IActionResult> ResendResetPasswordEmailAsync(string email)
        {
            await _userManager.SendResetPasswordEmailAsync(email);
            return Ok();
        }


        //[AmanahCientApplicationsFilter]
        [HttpPost("Login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginViewModel model)
        {
            var res = await _userManager.LoginAsync(model);
            if (res.Errors.Any())
                return BadRequest(res.Errors.FirstOrDefault()?.Description);

            return Ok(res);
        }

        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("Logout")]
        public async Task<IActionResult> LogoutAsync()
        {
            await _userManager.LogoutAsync();
            return Ok();
        }

        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPasswordAsync([FromBody] ForgotPasswordRequest model)
        {
            var res = await _userManager.ForgotPassword(model.Email);
            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }

        [HttpPost("ResetPassword")]
        public async Task<IActionResult> ResetPasswordAsync([FromBody] ResetPasswordRequest model)
        {
            var res = await _userManager.ResetPasswordAsync(model.Email, model.Token, model.NewPassword);
            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }

        [HttpPost("CheckResetToken")]
        public async Task<IActionResult> CheckResetTokenAsync([FromBody] CheckResetTokenRequest model)
        {
            var res = await _userManager.CheckResetToken(model.Email, model.Token);

            if (res.Errors.Count > 0)
            {
                return BadRequest(res.Errors.FirstOrDefault()?.Description);
            }
            return Ok(res);
        }
    }
}