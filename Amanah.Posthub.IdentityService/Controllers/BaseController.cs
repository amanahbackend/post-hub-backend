﻿using Microsoft.AspNetCore.Mvc;

namespace Amanah.Posthub.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected IActionResult Success(string message = null)
        {
            return Ok(GetResult(true, message));
        }

        protected IActionResult Fail(string message)
        {
            return Ok(GetResult(false, message));
        }

        private object GetResult(bool isSucceeded, string message)
        {
            return
                new
                {
                    IsSucceeded = isSucceeded,
                    Message = message
                };
        }
    }
}