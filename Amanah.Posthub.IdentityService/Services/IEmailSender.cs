﻿using System.Threading.Tasks;

namespace Amanah.Posthub.IdentityService.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
