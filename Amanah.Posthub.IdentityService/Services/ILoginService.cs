﻿using Amanah.Posthub.Service.Domain.Users.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.IdentityService.Services
{
    public interface ILoginService<T>
    {
        Task<bool> ValidateCredentials(T user, string password);
        Task<T> FindByUsername(string user);
        Task SignIn(T user);
        List<ApplicationUser> GetAll();
    }
}
