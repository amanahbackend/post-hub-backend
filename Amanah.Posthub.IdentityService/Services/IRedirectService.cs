﻿namespace Amanah.Posthub.IdentityService.Services
{
    public interface IRedirectService
    {
        string ExtractRedirectUriFromReturnUrl(string url);
    }
}
