﻿using System.Threading.Tasks;

namespace Amanah.Posthub.IdentityService.Services
{
    public interface ISmsSender
    {
        Task SendSmsAsync(string number, string message);
    }
}
