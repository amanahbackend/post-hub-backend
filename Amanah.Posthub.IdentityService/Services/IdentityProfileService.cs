﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Users.Entities;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityAuthority.Configs
{
    public class IdentityProfileService : IProfileService
    {

        private readonly IUserClaimsPrincipalFactory<ApplicationUser> _claimsFactory;
        private readonly UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationRole> _identityRoleManager;
        private readonly ApplicationDbContext _context;

        public IdentityProfileService(
            IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> identityRoleManager,
            ApplicationDbContext context)
        {
            _claimsFactory = claimsFactory;
            _userManager = userManager;
            _identityRoleManager = identityRoleManager;
            _context = context;

        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            if (user == null)
            {
                throw new ArgumentException("");
            }

            var principal = await _claimsFactory.CreateAsync(user);
            var claims = principal.Claims.ToList();
            claims.Add(new Claim(CustomClaimTypes.TenantId, user.Tenant_Id));
            //Add more claims like this


            int _userType = 0;
            var UserRoles = await _userManager.GetRolesAsync(user);
            if (UserRoles != null && UserRoles.Any())
            {
                if (UserRoles.Contains("Tenant") || UserRoles.Contains("Tenant") || UserRoles.Contains("Tenant"))
                {
                    _userType = 0;
                }
                else
                {
                    var userRoleName = UserRoles.FirstOrDefault();

                    var UserRole = await _identityRoleManager.FindByNameAsync(userRoleName);

                    _userType = UserRole.Type;

                    claims.Add(new Claim(CustomClaimTypes.UserId, user.Id.ToString()));

                    if (UserRole.Type == (int)RoleType.Manager)
                    {
                        _userType = (int)RoleType.Manager;
                    }
                    else if (UserRole.Type == (int)RoleType.Agent)
                    {

                        var driver = _context.Driver.Where(x => x.UserId == user.Id).FirstOrDefault();
                        if (driver != null)
                        {
                            claims.Add(new Claim("DriverId", driver.Id.ToString()));
                        }
                        _userType = (int)RoleType.Agent;
                    }
                    else if (UserRole.Type == (int)RoleType.BusinessCustomer)
                    {
                        var bcustomerContact = _context.BusinessConactAccounts.Where(x => x.UserID == user.Id).FirstOrDefault();
                        if (bcustomerContact != null)
                        {
                            claims.Add(new Claim("BusinessCustomerId", bcustomerContact.BusinessCustomerId.ToString()));
                            claims.Add(new Claim("BusinessCustomerContactId", bcustomerContact.Id.ToString()));
                        }
                        _userType = (int)RoleType.BusinessCustomer;
                    }
                }
            }

            claims.Add(new Claim("UserType", _userType.ToString()));
            principal.AddIdentity(new ClaimsIdentity(claims));



            //claims.Add(new System.Security.Claims.Claim("MyProfileID", user.Id));

            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await _userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }
}