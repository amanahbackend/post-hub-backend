﻿using Amanah.Posthub.API.Authorization;
using Amanah.Posthub.BLL.Addresses;
using Amanah.Posthub.BLL.Addresses.Services;
using Amanah.Posthub.BLL.Authorization;
using Amanah.Posthub.BLL.IManagers;
using Amanah.Posthub.BLL.Managers;
using Amanah.Posthub.BLL.UserManagement.Permissions;
using Amanah.Posthub.BLL.UserManagement.Services;
using Amanah.Posthub.Context;
using Amanah.Posthub.DeliverService.API.Localization;
using Amanah.Posthub.Domain.Drivers.DomainServices;
using Amanah.Posthub.Domain.Drivers.Repositories;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.Infrastructure.EntityFramework.Drivers.Repositories;
using Amanah.Posthub.Infrastructure.EntityFramework.Users.Repositories;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Infrastructure.ExternalServices.Settings;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.Settings;
using Amanah.Posthub.SharedKernel.Infrastructure.Authentication;
using Amanah.Posthub.IdentityService.Services;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using NSwag;
using NSwag.Generation.Processors.Security;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Utilites.UploadFile;
using Utilities.Utilites.Localization;
using OpenApiSecurityScheme = NSwag.OpenApiSecurityScheme;
using Amanah.Posthub.IdentityService.Configuration;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.DbContexts;
using Amanah.Posthub.IdentityService.Certificates;
using IdentityServer4.Services;
using IdentityAuthority.Configs;
using Microsoft.IdentityModel.Tokens;
using Amanah.Posthub.BLL.UserManagement.Queries;
using Amanah.Posthub.SharedKernel.Common.Reporting;
using Amananh.AppController.Validator;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Amanah.Posthub.BASE;
using System.Net;
using Utilities.Utilites.Exceptions;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Amanah.Posthub.IdentityService.API;
using Amanah.Posthub.IdentityService.API.Localization;
using ElmahCore.Mvc;
using Prometheus;
using Microsoft.FeatureManagement;
using Amanah.Posthub.SharedKernel.Exceptions;
using System.IO;
using NLog;

namespace Amanah.Posthub.IdentityService
{
    public class Startup
    {
        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                    "CorsPolicy",
                    builder => builder
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials()
                        .SetIsOriginAllowed(host => true));
            });


            // Add framework services.
            services.AddEntityFramework(_configuration, _environment);

            ConfigureIdentityServer(services);

            services.AddHttpClient();

            services.Configure<AppSettings>(_configuration);
            services.Configure<EmailSettings>(_configuration.GetSection("EmailSettings"));

            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            services.AddHttpContextAccessor();
            services.AddCurrentUser();
            services.AddSignalR();

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<Services.ILoginService<ApplicationUser>, EFLoginService>();
            services.AddTransient<IRedirectService, RedirectService>();
            services.AddScoped<IUserQuery, UserQuery>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAdminManager, AdminManager>();
            services.AddScoped<IAdminManager, AdminManager>();

            services.AddScoped(typeof(IGeoFenceManager), typeof(GeoFenceManager));
            services.Configure<PACISettings>(_configuration.GetSection("Address").GetSection("PACIAddressProvider").GetSection("PACIServiceSettings"));
            services.AddScoped(typeof(IUploadFormFileService), typeof(UploadFormFileService));
            services.AddScoped(typeof(IUploadFileManager), typeof(UploadFileManager));

            services.AddAuthorization(options =>
            {
                var allPermissions = TenantPermissions.FlatPermissions
                    .Concat(PlatformPermissions.FlatPermissions)
                    .Concat(AgentPermissions.FlatPermissions)
                    .ToArray();
                foreach (var permission in allPermissions)
                {
                    options.AddPolicy(permission, builder =>
                    {
                        builder.RequireAuthenticatedUser();
                        builder.RequireScope("profile");
                        builder.AddRequirements(new PermissionRequirement(permission));
                    });
                }
            });

            services.AddAmanahApplicationValidator(options =>
            {
                options.AmanahValidatorURL = "http://52.224.240.66:8085";
                options.ClientId = "PostExpress";
                options.ApplicationId = "PostHub";
            });

            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            ConfigureAuthService(services);

            services.AddAutoMapper(typeof(Startup));

            services.AddElmah();

            RegisterConventions(services);
            services.AddTransient<ILocalizer, Localizer>();

            services.AddControllers();
            services.AddMvcCore()
                .AddAuthorization()
               .AddDataAnnotationsLocalization(options => options.DataAnnotationLocalizerProvider =
                    (type, factory) => factory.Create(typeof(Translations)));


            services.AddSwaggerGen(config =>
            {
                config.DescribeAllEnumsAsStrings();
                //config.OperationFilter<SupportMultipleEnumValues>();
            });
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Posthub API";
                    document.Info.Description = "";
                    document.Info.TermsOfService = "None";
                };

                config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                    new OpenApiSecurityScheme()
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = "Authorization",
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Copy this into the value field: Bearer {token}"
                    }
                );
            });

            services.AddFeatureManagement();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
         
            app.UseRequestLocalization(options =>
            {
                var arabicCulture = new CultureInfo("ar");
                arabicCulture.DateTimeFormat.Calendar = new GregorianCalendar();
                var englishCulture = new CultureInfo("en");
                var supportedCultures = new[] { englishCulture, arabicCulture };
                options.DefaultRequestCulture = new RequestCulture(supportedCultures.First());
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });

            app.UseCors("CorsPolicy");

            if (env.IsDevelopment())
            {
                app.UseExceptionHandler(errorAppBuilder =>
                {
                    errorAppBuilder.Run(async context =>
                    {
                        var exception = context.Features.Get<IExceptionHandlerFeature>()?.Error;
                        var localizer = context.RequestServices.GetRequiredService<ILocalizer>();
                        var logger = context.RequestServices.GetRequiredService<ILogger<ExceptionHandlerMiddleware>>();
                        var (content, status) = exception switch
                        {
                            NotFoundException notFoundException => (notFoundException.Message, HttpStatusCode.NotFound),
                            DomainException domainException => (domainException.Message, HttpStatusCode.BadRequest),
                            UnauthorizedAccessException unauthorizedAccessException =>
                                (Keys.Validation.UnAuthorizedAccess, HttpStatusCode.Unauthorized),
                            _ => (Keys.Validation.GeneralError, HttpStatusCode.InternalServerError),
                        };
                        logger.LogCritical(exception, content);
                        if (!context.Response.HasStarted)
                        {
                            context.Response.StatusCode = (int)status;
                            var result =
                                new
                                {
                                    error = localizer[content]
                                };
                            // TODO: move to an extension method WriteJsonAsync
                            context.Response.ContentType = "application/json; charset=UTF-8";
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                            await context.Response.Body.FlushAsync();
                        }
                    });
                });
            }
            else
            {
                app.UseExceptionHandler(errorAppBuilder =>
                {
                    errorAppBuilder.Run(async context =>
                    {
                        var exception = context.Features.Get<IExceptionHandlerFeature>()?.Error;
                        var localizer = context.RequestServices.GetRequiredService<ILocalizer>();
                        var logger = context.RequestServices.GetRequiredService<ILogger<ExceptionHandlerMiddleware>>();
                        var (content, status) = exception switch
                        {
                            NotFoundException notFoundException => (notFoundException.Message, HttpStatusCode.NotFound),
                            DomainException domainException => (domainException.Message, HttpStatusCode.BadRequest),
                            UnauthorizedAccessException unauthorizedAccessException =>
                                (Keys.Validation.UnAuthorizedAccess, HttpStatusCode.Unauthorized),
                            _ => (Keys.Validation.GeneralError, HttpStatusCode.InternalServerError),
                        };
                        logger.LogCritical(exception, content);
                        if (!context.Response.HasStarted)
                        {
                            context.Response.StatusCode = (int)status;
                            var result =
                                new
                                {
                                    error = localizer[content]
                                };
                            // TODO: move to an extension method WriteJsonAsync
                            context.Response.ContentType = "application/json; charset=UTF-8";
                            await context.Response.WriteAsync(JsonConvert.SerializeObject(result));
                            await context.Response.Body.FlushAsync();
                        }
                    });
                });
            }
            InitializeDatabase(app);


            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseRouting();
            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseElmah();
            //initializing health monitoring (using prometheus)
            // Custom Metrics to count requests for each endpoint and the method
            var counter = Metrics.CreateCounter("identity_path_counter", "Counts requests to the identity API endpoints", new CounterConfiguration
            {
                LabelNames = new[] { "method", "endpoint" }
            });
            app.UseMetricServer();
            app.UseHttpMetrics(options =>
            {
                options.AddCustomLabel("host", context => context.Request.Host.Host);
            });

            app.UseMiddleware<ErrorHandlerMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireAuthorization();
            });
        }

        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            var identityUrl = _configuration.GetValue<string>("IdentityUrl");
            services.AddAuthentication("Bearer")
             .AddJwtBearer("Bearer", options =>
             {
                 options.Authority = identityUrl;
                 options.TokenValidationParameters = new TokenValidationParameters
                 {
                     ValidateAudience = false
                 };
             });
        }

        private void RegisterConventions(IServiceCollection services)
        {
            services.AddScoped<IDriverRepository, DriverRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserDeviceRepository, UserDeviceRepository>();
            services.AddScoped<IDriverLoginTrackingRepository, DriverLoginTrackingRepository>();
            services.AddScoped<IDriverDomainService, DriverDomainService>();
            services.AddScoped<IUserDomainService, UserDomainService>();
            services.AddScoped<IRoleDomainService, RoleDomainService>();
            services.AddScoped<IApplicationUserManager, ApplicationUserManager>();
            services.AddScoped<IApplicationRoleManager, ApplicationRoleManager>();
            services.AddScoped<IEmailSenderservice, EmailSenderService>();
            services.AddScoped<IUserDeviceManager, UserDeviceManager>();
            services.AddScoped<ISettingsManager, SettingsManager>();
            services.AddScoped<IRDLCrenderService, RDLCrenderService>();


        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                var context = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                context.Database.Migrate();
                if (!context.Clients.Any())
                {
                    foreach (var client in Config.Clients)
                    {
                        context.Clients.Add(client.ToEntity());
                    }
                    context.SaveChanges();
                }

                if (!context.IdentityResources.Any())
                {
                    foreach (var resource in Config.GetResources())
                    {
                        context.IdentityResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }

                if (!context.ApiScopes.Any())
                {
                    foreach (var resource in Config.ApiScopes)
                    {
                        context.ApiScopes.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }
            }
        }


        private void ConfigureIdentityServer(IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders()
                .AddErrorDescriber<LocalizedIdentityErrorDescriber>();


            string IdentityServerDBConnectionString = _configuration["IdentityServerDBConnectionString"];
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            services.AddIdentityServer()
                .AddJwtBearerClientAuthentication()
                .AddAspNetIdentity<ApplicationUser>()
                .AddSigningCredential(Certificate.Get())
                .AddConfigurationStore(options =>
                {
                    options.ConfigureDbContext = db =>
                        db.UseSqlServer(IdentityServerDBConnectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));
                })
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = db =>
                        db.UseSqlServer(IdentityServerDBConnectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));
                    options.EnableTokenCleanup = true;
                });


            var IdentityUrl = _configuration.GetValue<string>("IdentityUrl");
            var Secret = _configuration.GetValue<string>("Secret");
            var ClientId = _configuration.GetValue<string>("ClientId");

            services.Configure<TokenClientOptions>(options =>
            {
                options.Address = IdentityUrl;
                options.ClientId = ClientId;
                options.ClientSecret = Secret;
            });

            services.AddTransient(sp => sp.GetRequiredService<IOptions<TokenClientOptions>>().Value);


            services.AddHttpClient<TokenClient>();
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 6;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+��������������������������������������������";
            });

            services.Configure<DataProtectionTokenProviderOptions>(option =>
                option.TokenLifespan = TimeSpan.FromDays(1));

            services.AddTransient<IProfileService, IdentityProfileService>();
            //export services health check to prometheus 
            services.AddHealthChecks().ForwardToPrometheus();
        }

    }
}
