using Xunit;
using Moq;
using Amanah.Posthub.OrderService.Application.Features.MailItems;
using MediatR;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders;
using System.Threading.Tasks;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.OrderService.API.Controllers;

namespace Amanah.Posthub.OrderService.API.Tests
{
    public class OrderServiceAPITests 
    {
        [Fact]
        public void PickupMailItem_AssertIsNotNull()
        {
            
            //var dbContext = new Mock<Context.ApplicationDbContext>();
            PickupMailItemDTO command = new PickupMailItemDTO();
            var mediator = new Mock<IMediator>(command);
            //var mapper = new Mock<AutoMapper.IMapper>(command);
            //var unitofWork = new Mock<IUnitOfWork>(command);
            //var workItemRepository = new Mock<IRepository<MailItem>>();
            var result = mediator.Setup(m => m.Send(command, new System.Threading.CancellationToken()));

            Assert.NotNull(result);
        }
    }
}