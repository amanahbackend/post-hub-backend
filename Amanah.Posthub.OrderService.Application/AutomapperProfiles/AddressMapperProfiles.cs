﻿using Amanah.Posthub.OrderService.Application.Features.Areas.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using AutoMapper;
using System.Globalization;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles
{
    public class AddressMapperProfiles : Profile
    {
        public AddressMapperProfiles()
        {
            CreateMap<AreaDTO, Area>(MemberList.None);

            CreateMap<Area, AreaDTO>(MemberList.None)
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.FK_Governrate_Id, opt => opt.MapFrom(src => src.FK_Governrate_Id))
            .ForMember(dest => dest.Raf, opt => opt.MapFrom(src => src.Raf))
            .ForMember(dest => dest.NameAR, opt => opt.MapFrom(src => src.NameAR))
            .ForMember(dest => dest.NameEN, opt => opt.MapFrom(src => src.NameEN))
            .ForMember(dest => dest.GovernateName, opt => opt.MapFrom(src => CultureInfo.CurrentCulture.Name == "en" ? src.Governrate.NameEN : src.Governrate.NameAR))
            .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
            .ForMember(dest => dest.FK_Governrate_Id, opt => opt.MapFrom(src => src.FK_Governrate_Id))
            .ForMember(dest => dest.FK_Country_Id, opt => opt.MapFrom(src => src.Governrate.CountryId))
            .ForMember(dest=>dest.CountryName,opt=>opt.MapFrom(src=>src.Governrate.Country.Name))
            .ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem));

            CreateMap<Block, BlockDTO>(MemberList.None)
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.BlockNo, opt => opt.MapFrom(src => src.BlockNo))
            .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
            .ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem));



        }
    }
}
