﻿using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using AutoMapper;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles
{
    public class AreaFroDispatchMapperProfile : Profile
    {
        public AreaFroDispatchMapperProfile()
        {
            CreateMap<MailItem, AreaForDispatch>(MemberList.None)
              .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.DropOffAddress.Area))
              .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.DropOffAddress.Area));
        }
    }
}
