﻿using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Queries.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using AutoMapper;
using System;
using System.Threading;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles
{
    public class ContractMapperProfile : Profile
    {
        public ContractMapperProfile()
        {
            CreateMap<CreateContractInputDTO, Contract>(MemberList.None);

            CreateMap<UpdateContractInputDTO, Contract>(MemberList.None);

            CreateMap<Contract, ContractListResponseDto>(MemberList.None)
                            .ForMember(dest => dest.BusinessIndustry, opt =>
                                  opt.MapFrom(src => src.BusinessCustomer != null ?
                                                       src.BusinessCustomer.BusinessType != null ? src.BusinessCustomer.BusinessType.Name_en : ""
                                                          : ""))
                             .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                             .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                             .ForMember(dest => dest.ContractDate, opt => opt.MapFrom(src => src.ContractDate != null ? src.ContractDate : (DateTime?)null))
                             .ForMember(dest => dest.ContractEndDate, opt => opt.MapFrom(src => src.ContractEndDate != null ? src.ContractEndDate : (DateTime?)null))
                             .ForMember(dest => dest.ContractStartDate, opt => opt.MapFrom(src => src.ContractStartDate != null ? src.ContractStartDate : (DateTime?)null))
                             .ForMember(dest => dest.ContractStatusId, opt => opt.MapFrom(src => src.ContractStatusId))
                             .ForMember(dest => dest.ContractStatusName, opt => opt.MapFrom(src => src.ContractStatus != null ? src.ContractStatus.Name : ""))
                             .ForMember(dest => dest.ContractTypeId, opt => opt.MapFrom(src => src.ContractType != null ? src.ContractType.Id : 0))
                             .ForMember(dest => dest.ContractTypeName, opt => opt.MapFrom(src => src.ContractType != null ? src.ContractType.Name : ""))
                             .ForMember(dest => dest.CustomerName,
                                      opt => opt.MapFrom(src => src.BusinessCustomer != null ? src.BusinessCustomer.BusinessName : ""))
                             .ForMember(dest => dest.PaymentTypeId, opt => opt.MapFrom(src => src.PaymentType))
                             .ForMember(dest => dest.PaymentTypeName, opt => opt.MapFrom(src => src.PaymentType != null ? src.PaymentType.ToString() : ""));

            CreateMap<Contract, ExportContractListResponseDto>(MemberList.None)
                           .ForMember(dest => dest.BusinessIndustry, opt =>
                                 opt.MapFrom(src => src.BusinessCustomer != null ?
                                                      src.BusinessCustomer.BusinessType != null ? src.BusinessCustomer.BusinessType.Name_en : ""
                                                         : ""))
                            .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                            .ForMember(dest => dest.ContractDate, opt => opt.MapFrom(src => src.ContractDate))
                            .ForMember(dest => dest.ContractEndDate, opt => opt.MapFrom(src => src.ContractEndDate))
                            .ForMember(dest => dest.ContractStartDate, opt => opt.MapFrom(src => src.ContractStartDate))
                            //.ForMember(dest => dest.ContractStatusId, opt => opt.MapFrom(src => src.ContractStatusId))
                            .ForMember(dest => dest.ContractStatusName, opt => opt.MapFrom(src => src.ContractStatus.Name))
                            //.ForMember(dest => dest.ContractTypeId, opt => opt.MapFrom(src => src.ContractType.Id))
                            .ForMember(dest => dest.ContractTypeName, opt => opt.MapFrom(src => src.ContractType.Name))
                            .ForMember(dest => dest.CustomerName,
                                     opt => opt.MapFrom(src => src.BusinessCustomer != null ? src.BusinessCustomer.BusinessName : ""))
                            .ForMember(dest => dest.PaymentTypeId, opt => opt.MapFrom(src => src.PaymentType))
                            .ForMember(dest => dest.PaymentTypeName, opt => opt.MapFrom(src => src.PaymentType != null ? src.PaymentType.ToString() : ""));
            //for get by id
            CreateMap<Contract, ContractDTO>(MemberList.None)
           .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
           .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
           .ForMember(dest => dest.ContractDate, opt => opt.MapFrom(src => src.ContractDate))
           .ForMember(dest => dest.ContractStartDate, opt => opt.MapFrom(src => src.ContractStartDate))
           .ForMember(dest => dest.ContractEndDate, opt => opt.MapFrom(src => src.ContractEndDate))
           .ForMember(dest => dest.ContractStatusId, opt => opt.MapFrom(src => src.ContractStatusId))
           .ForMember(dest => dest.ContractTypeId, opt => opt.MapFrom(src => src.ContractTypeId))
           .ForMember(dest => dest.CurrencyId, opt => opt.MapFrom(src => src.CurrencyId))
           .ForMember(dest => dest.DistriputionAllowance, opt => opt.MapFrom(src => src.DistriputionAllowance))
           .ForMember(dest => dest.InstallmentsAfterEachMessage, opt => opt.MapFrom(src => src.InstallmentsAfterEachMessage))
           .ForMember(dest => dest.InstallmentsAtContracting, opt => opt.MapFrom(src => src.InstallmentsAtContracting))
           .ForMember(dest => dest.InstallmentsPaymentNotLaterThan, opt => opt.MapFrom(src => src.InstallmentsPaymentNotLaterThan))
           .ForMember(dest => dest.InstallmentsPaymentNotLaterThanPeriodType, opt => opt.MapFrom(src => src.InstallmentsPaymentNotLaterThanPeriodType))
           .ForMember(dest => dest.IsPrintAndFixEnabledEnabled, opt => opt.MapFrom(src => src.IsPrintAndFixEnabledEnabled))
           .ForMember(dest => dest.IsPrintReciptListAndCollectEnabled, opt => opt.MapFrom(src => src.IsPrintReciptListAndCollectEnabled))
           .ForMember(dest => dest.ItemSupplyPeriod, opt => opt.MapFrom(src => src.ItemSupplyPeriod))
           .ForMember(dest => dest.NoOfMessages, opt => opt.MapFrom(src => src.NoOfMessages))
           .ForMember(dest => dest.NumberOfUnits, opt => opt.MapFrom(src => src.NumberOfUnits))
           .ForMember(dest => dest.PaymentNotLetterThan, opt => opt.MapFrom(src => src.PaymentNotLetterThan))
           .ForMember(dest => dest.PaymentNotLetterThanPeriodType, opt => opt.MapFrom(src => src.PaymentNotLetterThanPeriodType))
           .ForMember(dest => dest.PaymentType, opt => opt.MapFrom(src => src.PaymentType))
           .ForMember(dest => dest.PricePerUnit, opt => opt.MapFrom(src => src.PricePerUnit))
           .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.ServiceSectorId))
           .ForMember(dest => dest.Currency, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Currency.Code_ar : src.Currency.Code_en))
           .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.BusinessCustomer.HQBranch.Address))
           .ForMember(dest => dest.Mobile, opt => opt.MapFrom(src => src.BusinessCustomer.HQBranch.Mobile))
           .ForMember(dest => dest.MainContactName, opt => opt.MapFrom(src => src.BusinessCustomer.MainContact.User.UserName))
           .ForMember(dest => dest.BusinessCustomerName, opt => opt.MapFrom(src => src.BusinessCustomer.BusinessName))
           .ForMember(dest => dest.ContractDays, opt => opt.MapFrom(src => (src.ContractEndDate - src.ContractStartDate).Value.TotalDays))
           .ForMember(dest => dest.RFQId, opt => opt.MapFrom(src => src.RFQId))
           .ForMember(dest => dest.PaymentNotLetterThanPeriodValue, opt => opt.MapFrom(src => src.PaymentNotLetterThanPeriodValue))
           .ForMember(dest => dest.ItemsSupplyAheadOf, opt => opt.MapFrom(src => src.ItemsSupplyAheadOf))
           .ForMember(dest => dest.ItemsSupplyAheadOfValue, opt => opt.MapFrom(src => src.ItemsSupplyAheadOfValue))
           .ForMember(dest => dest.ServiceDistributionPeriod, opt => opt.MapFrom(src => src.ServiceDistributionPeriod))
           .ForMember(dest => dest.ServiceDistributionPeriodValue, opt => opt.MapFrom(src => src.ServiceDistributionPeriodValue))
           .ForMember(dest => dest.PaymentTermsMessageNo, opt => opt.MapFrom(src => src.PaymentTermsMessageNo))
           .ForMember(dest => dest.BusinessCustomerId, opt => opt.MapFrom(src => src.BusinessCustomerId));


            CreateMap<ExtraTermDTO, ExtraTerm>(MemberList.None);

            CreateMap<ExtraTerm, ExtraTermDTO>(MemberList.None)
           .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
           .ForMember(dest => dest.Term, opt => opt.MapFrom(src => src.Term));





        }
    }
}
