﻿using Amanah.Posthub.OrderService.Application.Features.Invoices.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.Invoices.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using AutoMapper;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles

{
    public class InvoicesMapperProfile : Profile
    {
        public InvoicesMapperProfile()
        {


            CreateMap<GenerateInvoiceInputDTO, Invoice>(MemberList.None)
                   .ForMember(dest => dest.InvoicePeriodFrom, opt => opt.MapFrom(src => src.ValidFromDate))
              .ForMember(dest => dest.InvoicePeriodTo, opt => opt.MapFrom(src => src.ValidToDate))
              ;
            CreateMap<Invoice, GenerateInvoiceInputDTO>(MemberList.None)
               .ForMember(dest => dest.ValidFromDate, opt => opt.MapFrom(src => src.InvoicePeriodFrom))
              .ForMember(dest => dest.ValidToDate, opt => opt.MapFrom(src => src.InvoicePeriodTo))
             ;

            CreateMap<Invoice, InvoiceListResponseDTO>(MemberList.None)
                .ForMember(dest => dest.BusinessCustomerName, opt => opt.MapFrom(src => src.Contract.BusinessCustomer.BusinessName))
                .ForMember(dest => dest.BusinessCustomerId, opt => opt.MapFrom(src => src.BusinessCustomerId))


               .ForMember(dest => dest.TotalValue, opt => opt.MapFrom(src => src.TotalUnitsPrice))
            ;

            CreateMap<UpdateInvoiceInputDTO, Invoice>(MemberList.None)
                  .ForMember(dest => dest.InvoicePeriodFrom, opt => opt.MapFrom(src => src.ValidFromDate))
              .ForMember(dest => dest.InvoicePeriodTo, opt => opt.MapFrom(src => src.ValidToDate))
                ;
            CreateMap<Invoice, UpdateInvoiceInputDTO>(MemberList.None)
               .ForMember(dest => dest.ValidFromDate, opt => opt.MapFrom(src => src.InvoicePeriodFrom))
              .ForMember(dest => dest.ValidToDate, opt => opt.MapFrom(src => src.InvoicePeriodTo))
              ;




            CreateMap<Invoice, InvoiceDetailsResponseDTO>(MemberList.None)
                 .ForMember(dest => dest.ValidFromDate, opt => opt.MapFrom(src => src.InvoicePeriodFrom))
              .ForMember(dest => dest.ValidToDate, opt => opt.MapFrom(src => src.InvoicePeriodTo))
               .ForMember(dest => dest.BusinessCustomerName, opt => opt.MapFrom(src => src.Contract.BusinessCustomer.BusinessName))
              .ForMember(dest => dest.InvoiceStatusName, opt => opt.MapFrom(src => src.InvoiceStatus))

                  ;
            CreateMap<InvoiceDetailsResponseDTO, Invoice>(MemberList.None)
                 .ForMember(dest => dest.InvoicePeriodFrom, opt => opt.MapFrom(src => src.ValidFromDate))
              .ForMember(dest => dest.InvoicePeriodTo, opt => opt.MapFrom(src => src.ValidToDate))
                ;
        }
    }
}