﻿using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using AutoMapper;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles

{
    public class OrderMapperProfile : Profile
    {
        public OrderMapperProfile()
        {
            // _payContext = payContext;

            CreateMap<Order, OrderListExportToExcelResponseDTO>(MemberList.None)
                .ForMember(dest => dest.BusinessOrders_Branch, opt => opt.MapFrom(src => src.BusinessOrder.BusinessCustomer.HQBranch.Name))
                .ForMember(dest => dest.BusinessOrders_Business, opt => opt.MapFrom(src => src.BusinessOrder.BusinessCustomer.BusinessType.Name_en))
                .ForMember(dest => dest.BusinessOrders_BusinessCode, opt => opt.MapFrom(src => src.BusinessOrder.BusinessCustomer.BusinessCode))
                .ForMember(dest => dest.BusinessOrders_Customer, opt => opt.MapFrom(src => src.BusinessOrder.BusinessCustomer.BusinessName))
                .ForMember(dest => dest.DeliveryBefore, opt => opt.MapFrom(src => src.DeliveryBefore))
                .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.IssueAt))
                .ForMember(dest => dest.ReadyAt, opt => opt.MapFrom(src => src.ReadyAt))
                .ForMember(dest => dest.StartedAt, opt => opt.MapFrom(src => src.StartedAt))
                .ForMember(dest => dest.DeliveryDate, opt => opt.MapFrom(src => src.DeliveryDate != null ? src.DeliveryDate : (DateTime?)null))
                .ForMember(dest => dest.Envelop, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.BusinessOrder.MailItemsType.Name_ar : src.BusinessOrder.MailItemsType.Name_en))
                .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.CreationTime))
                .ForMember(dest => dest.TotalItemCount, opt => opt.MapFrom(src => (int)src.MailItems.Where(c => !c.IsDeleted).Count()))
                .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.OrderCode))
                .ForMember(dest => dest.OrderTypeId, opt => opt.MapFrom(src => src.OrderTypeId))
                .ForMember(dest => dest.OrderTypeName, opt => opt.MapFrom(src => src.OrderType.Name))
                .ForMember(dest => dest.PersonalOrders_Code, opt => opt.MapFrom(src => src.DirectOrder.Customer.Id))
                .ForMember(dest => dest.PersonalOrders_Company, opt => opt.MapFrom(src => src.DirectOrder.Customer.Company))
                .ForMember(dest => dest.PersonalOrders_Name, opt => opt.MapFrom(src => src.DirectOrder.Customer.Name))
                .ForMember(dest => dest.PickupDate, opt => opt.MapFrom(src => src.PickupDate != null ? src.PickupDate : (DateTime?)null))
                .ForMember(dest => dest.StartedAt, opt => opt.MapFrom(src => src.StartedAt != null ? src.PickupDate : (DateTime?)null))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.Reciver_PersonalOrders_Address, opt => opt.MapFrom(src => src.DirectOrder.ReciverInfo.Address))
                .ForMember(dest => dest.Reciver_PersonalOrders_Mobile, opt => opt.MapFrom(src => src.DirectOrder.ReciverInfo.Mobile))
                .ForMember(dest => dest.Reciver_PersonalOrders_Name, opt => opt.MapFrom(src => src.DirectOrder.ReciverInfo.FirstName))
                .ForMember(dest => dest.Sender_BusinessOrders_ContactAddress, opt => opt.Ignore())
                .ForMember(dest => dest.Sender_BusinessOrders_ContactBranch, opt => opt.Ignore())
                .ForMember(dest => dest.Sender_BusinessOrders_ContactMobile, opt => opt.MapFrom(src => src.BusinessOrder.BusinessContactAccount != null ? src.BusinessOrder.BusinessContactAccount.MobileNumber : ""))
                .ForMember(dest => dest.Sender_BusinessOrders_ContactName, opt => opt.MapFrom(src => src.BusinessOrder.BusinessContactAccount != null ? src.BusinessOrder.BusinessContactAccount.User.FirstName : ""))
                .ForMember(dest => dest.Sender_PersonalOrders_Address, opt => opt.MapFrom(src => src.DirectOrder.SenderInfo.Address))
                .ForMember(dest => dest.Sender_PersonalOrders_Name, opt => opt.MapFrom(src => src.DirectOrder.SenderInfo.FirstName))
                .ForMember(dest => dest.Sender_PersonalOrders_Mobile, opt => opt.MapFrom(src => src.DirectOrder.SenderInfo.Mobile))
                .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.StatusId))
                .ForMember(dest => dest.StatusName, opt => opt.MapFrom(src => src.Status.Name))
                .ForMember(dest => dest.TotalWeight, opt => opt.MapFrom(src => src.TotalWeight));

            CreateMap<Order, OrderListResponseDTO>(MemberList.None)
               .ForMember(dest => dest.BusinessOrders_Branch, opt => opt.MapFrom(src => src.BusinessOrder.BusinessCustomer.HQBranch.Name))
               .ForMember(dest => dest.BusinessOrders_Business, opt => opt.MapFrom(src => src.BusinessOrder.BusinessCustomer.BusinessType.Name_en))
               .ForMember(dest => dest.BusinessOrders_BusinessCode, opt => opt.MapFrom(src => src.BusinessOrder.BusinessCustomer.BusinessCode))
               .ForMember(dest => dest.BusinessOrders_Customer, opt => opt.MapFrom(src => src.BusinessOrder.BusinessCustomer.BusinessName))
               .ForMember(dest => dest.DeliveryBefore, opt => opt.MapFrom(src => src.DeliveryBefore))
               .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.IssueAt))
               .ForMember(dest => dest.ReadyAt, opt => opt.MapFrom(src => src.ReadyAt))
               .ForMember(dest => dest.StartedAt, opt => opt.MapFrom(src => src.StartedAt))
               .ForMember(dest => dest.DeliveryDate, opt => opt.MapFrom(src => src.DeliveryDate != null ? src.DeliveryDate : (DateTime?)null))
               .ForMember(dest => dest.Envelop, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.BusinessOrder.MailItemsType.Name_ar : src.BusinessOrder.MailItemsType.Name_en))
               .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.CreationTime))
               .ForMember(dest => dest.TotalItemCount, opt => opt.MapFrom(src => (int)src.MailItems.Where(c => !c.IsDeleted).Count()))
               .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.OrderCode))
               .ForMember(dest => dest.OrderTypeId, opt => opt.MapFrom(src => src.OrderTypeId))
               .ForMember(dest => dest.OrderTypeName, opt => opt.MapFrom(src => src.OrderType.Name))
               .ForMember(dest => dest.PersonalOrders_Code, opt => opt.MapFrom(src => src.DirectOrder.Customer.Id))
               .ForMember(dest => dest.PersonalOrders_Company, opt => opt.MapFrom(src => src.DirectOrder.Customer.Company))
               .ForMember(dest => dest.PersonalOrders_Name, opt => opt.MapFrom(src => src.DirectOrder.Customer.Name))
               .ForMember(dest => dest.PickupDate, opt => opt.MapFrom(src => src.PickupDate != null ? src.PickupDate : (DateTime?)null))
               .ForMember(dest => dest.StartedAt, opt => opt.MapFrom(src => src.StartedAt != null ? src.PickupDate : (DateTime?)null))
               .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
               .ForMember(dest => dest.Reciver_PersonalOrders_Address, opt => opt.MapFrom(src => src.DirectOrder.ReciverInfo.Address))
               .ForMember(dest => dest.Reciver_PersonalOrders_Mobile, opt => opt.MapFrom(src => src.DirectOrder.ReciverInfo.Mobile))
               .ForMember(dest => dest.Reciver_PersonalOrders_Name, opt => opt.MapFrom(src => src.DirectOrder.ReciverInfo.FirstName))
               .ForMember(dest => dest.Sender_BusinessOrders_ContactAddress, opt => opt.Ignore())
               .ForMember(dest => dest.Sender_BusinessOrders_ContactBranch, opt => opt.Ignore())
               .ForMember(dest => dest.Sender_BusinessOrders_ContactMobile, opt => opt.MapFrom(src => src.BusinessOrder.BusinessContactAccount != null ? src.BusinessOrder.BusinessContactAccount.MobileNumber : ""))
               .ForMember(dest => dest.Sender_BusinessOrders_ContactName, opt => opt.MapFrom(src => src.BusinessOrder.BusinessContactAccount != null ? src.BusinessOrder.BusinessContactAccount.User.FirstName : ""))
               .ForMember(dest => dest.Sender_PersonalOrders_Address, opt => opt.MapFrom(src => src.DirectOrder.SenderInfo.Address))
               .ForMember(dest => dest.Sender_PersonalOrders_Name, opt => opt.MapFrom(src => src.DirectOrder.SenderInfo.FirstName))
               .ForMember(dest => dest.Sender_PersonalOrders_Mobile, opt => opt.MapFrom(src => src.DirectOrder.SenderInfo.Mobile))
               .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.StatusId))
               .ForMember(dest => dest.StatusName, opt => opt.MapFrom(src => src.Status.Name))
               .ForMember(dest => dest.TotalWeight, opt => opt.MapFrom(src => src.TotalWeight))
               // .ForMember(dest => dest.OrderPaymentStatus, opt => opt.MapFrom(src =>_payContext.PaymentTransactions.FirstOrDefault(x=>x.OrderId==src.Id.ToString()).IsSuccessTransaction ==true?"Success":"Fail"))

               .ForMember(dest => dest.CourierTrackingNumber, opt => opt.MapFrom(src => src.OrderInternationalDelivery.CourierTrackingNumber));



            CreateMap<MailItem, BusinessCustomerAndBranchesDTO>(MemberList.None)
            .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.Order.MailItems.Where(x => x.Status.Id == (int)MailItemStatuses.Delivered && !x.IsDeleted)))
            // .GroupBy(a => src.DeliveredDate.Value.Day).Count()))
            .ForMember(dest => dest.DeliveryDate, opt => opt.MapFrom(src => src.DeliveredDate))
            .ForMember(dest => dest.Day, opt => opt.MapFrom(src => src.DeliveredDate.Value.Day))
            .ForMember(dest => dest.BusinessBranchName, opt => opt.MapFrom(src => src.Order.BusinessOrder.BusinessCustomer.HQBranch.Name))
            .AfterMap((src, dest) =>
            {

                //dest.ItemsNo = src.Order.MailItems.Where(x => x.Status.Id == 13 && !x.IsDeleted)
                //.GroupBy(a => a.DeliveredDate.Value.Day).Count();


            });

            CreateMap<OrdersStatusLog, TrackOrderStatusLogDto>(MemberList.None)
           .ForMember(dest => dest.OrderStatusId, opt => opt.MapFrom(src => src.OrderStatusId))
           .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreationTime))
           .ForMember(dest => dest.TimeOfDate, opt => opt.MapFrom(src => src.CreationTime.ToString("hh:mm tt")))
           .ForMember(dest => dest.OrderStatusName, opt => opt.MapFrom(src => src.OrderStatus != null ? src.OrderStatus.Name : ""));

            CreateMap<WorkOrder, StatementListResponseDTO>(MemberList.None)
         .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
         .ForMember(dest => dest.SecurityCaseNumber, opt => opt.MapFrom(src => src.SecurityCaseNumber))
         .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.WorkOrderStatusId))
         .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.MailItems.FirstOrDefault() != null ? src.MailItems.FirstOrDefault().ConsigneeInfo != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch.Name : src.MailItems.FirstOrDefault().ConsigneeInfo.BranchName : "" : ""))
         .ForMember(dest => dest.BranchId, opt => opt.MapFrom(src => src.MailItems.FirstOrDefault() != null ? src.MailItems.FirstOrDefault().ConsigneeInfo != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch.Id : 0 : 0 : 0))
         .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
         .ForMember(dest => dest.NumberOfItems, opt => opt.MapFrom(src => src.MailItems != null ? src.MailItems.Count() : 0))
         .ForMember(dest => dest.CreatedDatetime, opt => opt.MapFrom(src => src.CreatedDate))
         .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.WorkOrderStatus != null ? src.WorkOrderStatus.Name : ""));

            CreateMap<Order, ReceiveOrderListResponseDTO>(MemberList.None)
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.StatementCode, opt => opt.MapFrom(src => src.StatementCode))
            .ForMember(dest => dest.SenderBranchId, opt => opt.MapFrom(src => src.SenderBranchId))
            .ForMember(dest => dest.SenderBranch, opt => opt.MapFrom(src => src.SenderBranch != null ? src.SenderBranch.Name : ""))
            .ForMember(dest => dest.ReciverBranch, opt => opt.MapFrom(src => src.MailItems.FirstOrDefault() != null ? src.MailItems.FirstOrDefault().ConsigneeInfo != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch.Name : "" : "" : ""))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.MailItems.FirstOrDefault() != null ? src.MailItems.FirstOrDefault().Remark != null ? System.Threading.Thread.CurrentThread.CurrentCulture.IsArabic() ? src.MailItems.FirstOrDefault().Remark.Name_ar : src.MailItems.FirstOrDefault().Remark.Name_en : src.MailItems.FirstOrDefault().Description : ""))
            .ForMember(dest => dest.SenderName, opt => opt.MapFrom(src => src.SenderName))
            .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.OrderCode));

            CreateMap<WorkOrder, PrintPostBagStatementDTO>(MemberList.None)
           .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
           .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.MailItems.FirstOrDefault() != null ? src.MailItems.FirstOrDefault().ConsigneeInfo != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch.Name : "" : "" : ""))
           .ForMember(dest => dest.BranchCode, opt => opt.MapFrom(src => src.MailItems.FirstOrDefault() != null ? src.MailItems.FirstOrDefault().ConsigneeInfo != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch.Code : "" : "" : ""))
           .ForMember(dest => dest.BranchId, opt => opt.MapFrom(src => src.MailItems.FirstOrDefault() != null ? src.MailItems.FirstOrDefault().ConsigneeInfo != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch != null ? src.MailItems.FirstOrDefault().ConsigneeInfo.Branch.Id : 0 : 0 : 0))
           .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
           .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy_Id))
           .ForMember(dest => dest.RecieveAt, opt => opt.MapFrom(src => src.DeliveryDate != null ? src.DeliveryDate.Value.Date : (DateTime?)null))
           .ForMember(dest => dest.RecieveOn, opt => opt.MapFrom(src => src.DeliveryDate != null ? src.DeliveryDate.Value.ToLongTimeString() : ""))
           .ForMember(dest => dest.SecurityCaseNumber, opt => opt.MapFrom(src => src.SecurityCaseNumber))
           .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.MailItems != null ? src.MailItems.Count() : 0))
           .ForMember(dest => dest.WorkOrderImageURL, opt => opt.MapFrom(src => $"UploadFile/WorkOrderFile/" + (string.IsNullOrEmpty(src.WorkOrderImageURL) ? "avtar.jpeg" : src.WorkOrderImageURL)))
           .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(src => src.CreatedDate));

            CreateMap<MailItem, MailItemResponseDTO>(MemberList.None)
         .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
         .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy_Id))
         .ForMember(dest => dest.MailItemBarCode, opt => opt.MapFrom(src => src.MailItemBarCode))
         .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Notes))
         .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Remark != null ? src.Remark.Name_en : src.Description))
         .ForMember(dest => dest.DeliveryAt, opt => opt.MapFrom(src => src.DeliveredDate != null ? src.DeliveredDate.Value.Date : (DateTime?)null))
         .ForMember(dest => dest.DeliveryOn, opt => opt.MapFrom(src => src.DeliveredDate != null ? src.DeliveredDate.Value.ToLongTimeString() : ""))
         .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.ConsigneeInfo != null ? src.ConsigneeInfo.Branch != null ? src.ConsigneeInfo.Branch.Name : "" : ""))
         .ForMember(dest => dest.BranchCode, opt => opt.MapFrom(src => src.ConsigneeInfo != null ? src.ConsigneeInfo.Branch != null ? src.ConsigneeInfo.Branch.Code : "" : ""))
         .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(src => src.CreatedDate));

        }
    }
}