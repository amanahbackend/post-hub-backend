﻿using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using Amanah.Posthub.Service.Domain.PriceList.Entities;
using AutoMapper;
using System;
using System.Linq;
using System.Threading;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles

{
    public class PLMapperProfile : Profile
    {
        public PLMapperProfile()
        {


            CreateMap<CreatePLInputDTO, PriceList>(MemberList.None)
              .ForMember(dest => dest.PriceListStatusId, opt => opt.MapFrom(src => src.StatusId))
              .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.SectorTypeId))
              ;
            CreateMap<PriceList, CreatePLInputDTO>(MemberList.None)
             .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.PriceListStatusId))
             .ForMember(dest => dest.SectorTypeId, opt => opt.MapFrom(src => src.ServiceSectorId))
             ;

            CreateMap<PriceListItems, PLListResponseDTO>(MemberList.None)
               .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.PriceListStatus, opt => opt.MapFrom(src => src.PriceList.PriceListStatus.Name))
               .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.PriceList.Code))
               .ForMember(dest => dest.ValidToDate, opt => opt.MapFrom(src => src.PriceList.ValidToDate != null ? src.PriceList.ValidToDate : (DateTime?)null))
               //Ramzy: we use only hours now also we need to check why the ServiceTimeUnit not saved with 'hours'
               .ForMember(dest => dest.ServiceTime, opt => opt.MapFrom(src => src.ServiceTimeUnit == "hours" ? src.ServiceTime: src.ServiceTime * 24 ))
               .ForMember(dest => dest.ServiceTimeUnit, opt => opt.MapFrom(src => src.ServiceTimeUnit))
               .ForMember(dest => dest.ServiceSectorName, opt => opt.MapFrom(src => src.PriceList.ServiceSector.Name_en))
            //.ForMember(dest => dest.Price, opt => opt.MapFrom(src =>src.Price))
            ;

            CreateMap<PriceList, PLListResponseDTO>(MemberList.None)
               .ForMember(dest => dest.PriceListId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.PriceListStatus, opt => opt.MapFrom(src => src.PriceListStatus.Name))
               .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
               .ForMember(dest => dest.ItemsCount, opt => opt.MapFrom(src => src.PriceListItems != null ? src.PriceListItems.Where(c=>!c.IsDeleted).Count() : 0))
               .ForMember(dest => dest.ServiceSectorName, opt => opt.MapFrom(src => src.ServiceSector.Name_en));

            CreateMap<PriceListItems, CreatePLItemsInputDTO>(MemberList.None);
            CreateMap<CreatePLItemsInputDTO, PriceListItems>(MemberList.None);



            CreateMap<PriceList, UpdatePLInputDTO>(MemberList.None)
                 .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                 .ForMember(dest => dest.PriceListStatusId, opt => opt.MapFrom(src => src.PriceListStatusId))
              .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.ServiceSectorId))
                ;
            CreateMap<UpdatePLInputDTO, PriceList>(MemberList.None)
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.PriceListStatusId, opt => opt.MapFrom(src => src.PriceListStatusId))
              .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.ServiceSectorId))
              ;


            CreateMap<RFQItems, CreatePQItemsInputDTO>(MemberList.None)
                  .ForMember(dest => dest.ItemDescription, opt => opt.MapFrom(src => src.Description))
                   .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Quantity))
                   .ForMember(dest => dest.CurrencyName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Currency.Code_ar : src.Currency.Code_en))

                ;
            CreateMap<CreatePQItemsInputDTO, RFQItems>(MemberList.None)
                  .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.ItemDescription))
                   .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Amount))
                  ;

            CreateMap<PriceList, PLDetailsResponseDTO>(MemberList.None)
                  .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.PriceListStatusId))
                   // .ForMember(dest => dest.ValidToDate, opt => opt.MapFrom(src => src.PQValidToDate))
                   // .ForMember(dest => dest.LetterSubHtml, opt => opt.MapFrom(src => src.LetterSubject))
                   // .ForMember(dest => dest.LetterTexHtml, opt => opt.MapFrom(src => src.LetterText))
                   // .ForMember(dest => dest.PqSubHtml, opt => opt.MapFrom(src => src.PQSubject))
                   // .ForMember(dest => dest.PQCode, opt => opt.MapFrom(src => src.PQCode))
                   // .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                   .ForMember(dest => dest.SectorTypeId, opt => opt.MapFrom(src => src.ServiceSectorId));
            CreateMap<PLDetailsResponseDTO, PriceList>(MemberList.None);
        }
    }
}