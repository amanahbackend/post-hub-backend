﻿using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using Amanah.Posthub.Service.Domain.PriceList.Entities;
using AutoMapper;
using System;
using System.Linq;
using System.Threading;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles

{
    public class RFQMapperProfile : Profile
    {
        public RFQMapperProfile()
        {

            CreateMap<CreateRFQInputDTO, RFQ>(MemberList.None)
                   .ForMember(dest => dest.BusinessCustomerId, opt => opt.MapFrom(src => src.CustomerId))
                   .ForMember(dest => dest.PQValidToDate, opt => opt.MapFrom(src => src.ValidToDate))
                   .ForMember(dest => dest.PQDate, opt => opt.MapFrom(src => src.PQDate))
                   .ForMember(dest => dest.LetterSubject, opt => opt.MapFrom(src => src.LetterSubHtml))
                   .ForMember(dest => dest.LetterText, opt => opt.MapFrom(src => src.LetterTexHtml))
                   .ForMember(dest => dest.PQSubject, opt => opt.MapFrom(src => src.PqSubHtml))
                   .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.SectorTypeId))

                ;
            CreateMap<CreateRFQInputDTO, RFQItems>(MemberList.None)
              .ForMember(dest => dest.CurrencyId, opt => opt.MapFrom(src => src.CurrencyId));

            CreateMap<CreateRFQWithGeneralOfferInputDTO, RFQ>(MemberList.None)
                  .ForMember(dest => dest.IsGeneralOffer, opt => opt.MapFrom(src => src.IsGeneralOffer))
                  .ForMember(dest => dest.PQValidToDate, opt => opt.MapFrom(src => src.ValidToDate))
                  .ForMember(dest => dest.PQDate, opt => opt.MapFrom(src => src.PQDate))
                  .ForMember(dest => dest.LetterSubject, opt => opt.MapFrom(src => src.LetterSubHtml))
                  .ForMember(dest => dest.LetterText, opt => opt.MapFrom(src => src.LetterTexHtml))
                  .ForMember(dest => dest.PQSubject, opt => opt.MapFrom(src => src.PqSubHtml))
                  .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.SectorTypeId))

               ;
            CreateMap<CreateRFQWithGeneralOfferInputDTO, RFQItems>(MemberList.None)
              .ForMember(dest => dest.CurrencyId, opt => opt.MapFrom(src => src.CurrencyId));

            CreateMap<RFQ, RFQListResponseDTO>(MemberList.None)
               .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.SectorTypeName, opt => opt.MapFrom(src => src.ServiceSector != null ? src.ServiceSector.Name_en : ""))
               .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.BusinessCustomer != null ? src.BusinessCustomer.BusinessName : ""))
               .ForMember(dest => dest.PQStatusName, opt => opt.MapFrom(src => src.PriceQuotationStatus != null ? src.PriceQuotationStatus.Name : ""))
               .ForMember(dest => dest.PqDate, opt => opt.MapFrom(src => src.PQDate != null ? src.PQDate : (DateTime?)null))
               .ForMember(dest => dest.PqValidToDate, opt => opt.MapFrom(src => src.PQValidToDate != null ? src.PQValidToDate : (DateTime?)null))
               .ForMember(dest => dest.PqSubject, opt => opt.MapFrom(src => src.PQSubject))
              .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.PQCode))
               .ForMember(dest => dest.SectorTypeId, opt => opt.MapFrom(src => src.ServiceSectorId));

            CreateMap<RFQItems, CreateRFQItemsInputDTO>(MemberList.None);
            CreateMap<CreateRFQItemsInputDTO, RFQItems>(MemberList.None);

            CreateMap<RFQItems, RFQItemsDTO>(MemberList.None);


            CreateMap<RFQItems, InternationalRFQDetails>(MemberList.None)
                .ForMember(dest => dest.CountryName, opt => opt.MapFrom(src => src.Country.Name))
                ;
            CreateMap<InternationalRFQDetails, RFQItems>(MemberList.None);

            CreateMap<RFQ, UpdateRFQDTO>(MemberList.None);
            CreateMap<UpdateRFQDTO, RFQ>(MemberList.None)
                 .ForMember(dest => dest.BusinessCustomerId, opt => opt.MapFrom(src => src.CustomerId))
                 .ForMember(dest => dest.PQValidToDate, opt => opt.MapFrom(src => src.ValidToDate))
                 .ForMember(dest => dest.LetterSubject, opt => opt.MapFrom(src => src.LetterSubHtml))
                 .ForMember(dest => dest.LetterText, opt => opt.MapFrom(src => src.LetterTexHtml))
                 .ForMember(dest => dest.PQSubject, opt => opt.MapFrom(src => src.PqSubHtml))
                 .ForMember(dest => dest.PQCode, opt => opt.MapFrom(src => src.PQCode))
                 .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.SectorTypeId))

              ;
            CreateMap<RFQ, UpdateRFQWithGeneralOfferDTO>(MemberList.None);
            CreateMap<UpdateRFQWithGeneralOfferDTO, RFQ>(MemberList.None)
                 .ForMember(dest => dest.IsGeneralOffer, opt => opt.MapFrom(src => src.IsGeneralOffer))
                 .ForMember(dest => dest.PQValidToDate, opt => opt.MapFrom(src => src.ValidToDate))
                 .ForMember(dest => dest.LetterSubject, opt => opt.MapFrom(src => src.LetterSubHtml))
                 .ForMember(dest => dest.LetterText, opt => opt.MapFrom(src => src.LetterTexHtml))
                 .ForMember(dest => dest.PQSubject, opt => opt.MapFrom(src => src.PqSubHtml))
                 // .ForMember(dest => dest.PQCode, opt => opt.MapFrom(src => src.PQCode))
                 .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.SectorTypeId))

              ;
            

            CreateMap<RFQItems, CreatePQItemsInputDTO>(MemberList.None)
                  .ForMember(dest => dest.ItemDescription, opt => opt.MapFrom(src => src.Description))
                   .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Quantity))
                   .ForMember(dest => dest.HasAmount, opt => opt.MapFrom(src => src.HasAmount))
                   .ForMember(dest => dest.ItemPrice, opt => opt.MapFrom(src => src.ItemPrice))
                   .ForMember(dest => dest.CurrencyName, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Currency.Code_ar : src.Currency.Code_en))

                ;
            CreateMap<CreatePQItemsInputDTO, RFQItems>(MemberList.None)
                  .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.ItemDescription))
                   .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Amount))
                   .ForMember(dest => dest.HasAmount, opt => opt.MapFrom(src => src.HasAmount))
                   .ForMember(dest => dest.ItemPrice, opt => opt.MapFrom(src => src.ItemPrice))
                  ;

            CreateMap<RFQ, RFQDetailsResponseDTO>(MemberList.None)
                .ForMember(dest => dest.CurrencyId, opt => opt.MapFrom(src => src.RFQItemPrices.FirstOrDefault().CurrencyId))
                .ForMember(dest => dest.ValidToDate, opt => opt.MapFrom(src => src.PQValidToDate))
                .ForMember(dest => dest.LetterSubHtml, opt => opt.MapFrom(src => src.LetterSubject))
                .ForMember(dest => dest.LetterTexHtml, opt => opt.MapFrom(src => src.LetterText))
                .ForMember(dest => dest.PqSubHtml, opt => opt.MapFrom(src => src.PQSubject))
                .ForMember(dest => dest.PQCode, opt => opt.MapFrom(src => src.PQCode))
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.BusinessCustomer.BusinessName))
                .ForMember(dest => dest.SectorTypeId, opt => opt.MapFrom(src => src.ServiceSectorId))
                .ForMember(dest => dest.IsGeneralOffer, opt => opt.MapFrom(src => src.IsGeneralOffer !=null ? src.IsGeneralOffer:false))
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.BusinessCustomerId !=null ? src.BusinessCustomerId :null));

            CreateMap<RFQDetailsResponseDTO, RFQ>(MemberList.None);

            CreateMap<RFQItems, RFQItemsByRFQ>(MemberList.None)
                .ForMember(dest => dest.ItemPrice, opt => opt.MapFrom(src => src.ItemPrice))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.RFQId, opt => opt.MapFrom(src => src.RFQId))
                .ForMember(dest => dest.RoundTripShippingPrice, opt => opt.MapFrom(src => src.RoundTripShippingPrice))
                .ForMember(dest => dest.HasAmount, opt => opt.MapFrom(src => src.HasAmount))
                .ForMember(dest => dest.CurrencyCode, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Currency.Code_ar : src.Currency.Code_en))
                .ForMember(dest => dest.CurrencyId, opt => opt.MapFrom(src => src.CurrencyId))
                .ForMember(dest => dest.BusinessCustomerId, opt => opt.MapFrom(src => src.RFQ.BusinessCustomerId))
                .ForMember(dest => dest.BusinessCustomerName, opt => opt.MapFrom(src => src.RFQ.BusinessCustomer.BusinessName))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id));

            CreateMap<RFQ, RFQDTOList>(MemberList.None)
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.PQSubject, opt => opt.MapFrom(src => src.PQSubject))
            .ForMember(dest => dest.PQCode, opt => opt.MapFrom(src => src.PQCode));

            CreateMap<RFQ,GeneralOffersWithSectorsResponseDTO>(MemberList.None)
                 .ForMember(dest => dest.IsGeneralOffer, opt => opt.MapFrom(src => src.IsGeneralOffer))                
                 .ForMember(dest => dest.SectorTypeId, opt => opt.MapFrom(src => src.ServiceSectorId))
                 .ForMember(dest => dest.SectorTypeName, opt => opt.MapFrom(src => src.ServiceSector.Name_en))              ;

        }
    }
}