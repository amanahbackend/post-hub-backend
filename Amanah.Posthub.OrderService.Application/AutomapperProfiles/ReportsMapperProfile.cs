﻿using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using AutoMapper;
using System.Linq;


namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles
{
    public class ReportsMapperProfile : Profile
    {
        public ReportsMapperProfile()
        {
            CreateMap<MailItem, LocalMessageDistributionReportDTO>(MemberList.None)

           .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
           .ForMember(dest => dest.DeliveryDate, opt => opt.MapFrom(src => src.DeliveredDate))
           .ForMember(dest => dest.WorkOrderCode, opt => opt.MapFrom(src => src.Workorder.Code))
           .ForMember(dest => dest.DriverName, opt => opt.MapFrom(src => src.Workorder.Driver.User.UserName))
           // .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.DropOffAddress.Area))
           .ForMember(dest => dest.FromSerial, opt => opt.MapFrom(src => src.FromSerial))
           .ForMember(dest => dest.PickupDate, opt => opt.MapFrom(src => src.PickupDate))
           .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.Order.MailItems.Count()))
           .ForMember(dest => dest.ReturnItemsNo, opt => opt.MapFrom(src => src.Order.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Returned).Count()))
           .ForMember(dest => dest.DeliveredItemsNo, opt => opt.MapFrom(src => src.Order.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Delivered).Count()))
           .ForMember(dest => dest.RemainingItemsNo, opt => opt.MapFrom(src => (src.Order.MailItems.Count()
           - (src.Order.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Returned).Count() + src.Order.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Delivered).Count()))))
           .ForMember(dest => dest.CaseNo, opt => opt.MapFrom(src => src.CaseNo));

        }
    }
}
