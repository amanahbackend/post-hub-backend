﻿using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles
{
    public class ServicesMapperProfile : Profile
    {
        public ServicesMapperProfile()
        {
            CreateMap<Service.Domain.Entities.Service, ServiceResponseDTO>() 
                .ForMember(des => des.Name, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Name_ar : src.Name_en))
                .ForMember(des => des.PhotoUrl, opt => opt.MapFrom(src => $"UploadFile/ServiceFile/{(string.IsNullOrEmpty(src.PhotoUrl) ? "avtar.jpeg" : src.PhotoUrl) }"));


        }
    }
}
