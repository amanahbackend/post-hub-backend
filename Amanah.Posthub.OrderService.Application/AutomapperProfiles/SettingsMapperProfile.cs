﻿using Amanah.Posthub.OrderService.Application.Features.Department.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.DriverRoles.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Remarks.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Services.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using AutoMapper;
using System.Threading;
using Utilities.Extensions;
using Amanah.Posthub.OrderService.Application.Features.Remarks.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Queries.DTOs;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles
{
    public class SettingsMapperProfile : Profile
    {
        public SettingsMapperProfile()
        {

            // CreateMap<DepartmentResponseDTO, Department>(MemberList.None);

            // CreateMap<Department, DepartmentResponseDTO>(MemberList.None)
            //.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //.ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en!=null? src.Name_en:""))
            //.ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
            //.ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
            //.ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar!=null? src.Name_ar:""));

            CreateMap<CreateDepartmentInputDTO, Department>(MemberList.None)
           .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
           .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
           .ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
           .ForMember(dest => dest.FloorNo, opt => opt.MapFrom(src => src.FloorNo))
           .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));



            CreateMap<CreateDriverRoleInputDTO, DriverRole>(MemberList.None)
           .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
           .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<UpdateDriverRolesInputDTO, DriverRole>(MemberList.None)
            .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
            .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<CreateDriverRoleInputDTO, DriverRole>(MemberList.None)
           .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
           .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<UpdateDriverRolesInputDTO, DriverRole>(MemberList.None)
            .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
            .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<UpdateDepartmentInputDTO, Department>(MemberList.None)
           .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
           .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
           .ForMember(dest => dest.IsSystem, opt => opt.MapFrom(src => src.IsSystem))
           .ForMember(dest => dest.FloorNo, opt => opt.MapFrom(src => src.FloorNo))
           .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<CreateServiceInputDTO, Amanah.Posthub.Service.Domain.Entities.Service>(MemberList.None)
           .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
           .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
           .ForMember(dest => dest.PhotoUrl, opt => opt.MapFrom(src => src.PhotoUrl))
           .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<UpdateServcieInputDTO, Amanah.Posthub.Service.Domain.Entities.Service>(MemberList.None)
           .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
           .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
           .ForMember(dest => dest.PhotoUrl, opt => opt.MapFrom(src => src.PhotoUrl))
           .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<CreatePackingPackageInputDTO, PackingPackage>(MemberList.None)
          .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
          .ForMember(dest => dest.MaxWeight, opt => opt.MapFrom(src => src.MaxWeight))
          .ForMember(dest => dest.DimensionHight, opt => opt.MapFrom(src => src.DimensionHight))
          .ForMember(dest => dest.DimensionLength, opt => opt.MapFrom(src => src.DimensionLength))
          .ForMember(dest => dest.DimensionWidth, opt => opt.MapFrom(src => src.DimensionWidth))
          .ForMember(dest => dest.Description_ar, opt => opt.MapFrom(src => src.Description_ar))
          .ForMember(dest => dest.Description_en, opt => opt.MapFrom(src => src.Description_en))
          .ForMember(dest => dest.MaxWeight, opt => opt.MapFrom(src => src.MaxWeight))
          .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<UpdatePackingPackageInputDTO, PackingPackage>(MemberList.None)
        .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
        .ForMember(dest => dest.DimensionHight, opt => opt.MapFrom(src => src.DimensionHight))
        .ForMember(dest => dest.DimensionLength, opt => opt.MapFrom(src => src.DimensionLength))
        .ForMember(dest => dest.DimensionWidth, opt => opt.MapFrom(src => src.DimensionWidth))
        .ForMember(dest => dest.Description_ar, opt => opt.MapFrom(src => src.Description_ar))
        .ForMember(dest => dest.Description_en, opt => opt.MapFrom(src => src.Description_en))
        .ForMember(dest => dest.MaxWeight, opt => opt.MapFrom(src => src.MaxWeight))
        .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<PackingPackageCountryDTO, PackingPackageCountry>(MemberList.None)
        .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
        .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.CountryId));

            CreateMap<PackingPackage, PackingPackageDTO>(MemberList.None)
        .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
        .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en))
        .ForMember(dest => dest.MaxWeight, opt => opt.MapFrom(src => src.MaxWeight))
        .ForMember(dest => dest.DimensionHight, opt => opt.MapFrom(src => src.DimensionHight))
        .ForMember(dest => dest.DimensionLength, opt => opt.MapFrom(src => src.DimensionLength))
        .ForMember(dest => dest.DimensionWidth, opt => opt.MapFrom(src => src.DimensionWidth))
        .ForMember(dest => dest.Description_ar, opt => opt.MapFrom(src => src.Description_ar))
        .ForMember(dest => dest.Description_en, opt => opt.MapFrom(src => src.Description_en))
        .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar));

            CreateMap<PackingPackageCountry, PackingPackageCountryResponseDTO>(MemberList.None)
         .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
         .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
         .ForMember(dest => dest.CountryId, opt => opt.MapFrom(src => src.CountryId));

            CreateMap<PackingPackageCountry, PackingPackageForMobileDTO>(MemberList.None)
        .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PackingPackage.Id))
        .ForMember(dest => dest.Name, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.PackingPackage != null ? src.PackingPackage.Name_ar : "" : src.PackingPackage != null ? src.PackingPackage.Name_en : ""))
        .ForMember(dest => dest.Description, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.PackingPackage != null ? src.PackingPackage.Description_ar : "" : src.PackingPackage != null ? src.PackingPackage.Description_en : ""))
        .ForMember(dest => dest.MaxWeight, opt => opt.MapFrom(src => src.PackingPackage != null ? src.PackingPackage.MaxWeight : 0))
        .ForMember(dest => dest.DimensionHight, opt => opt.MapFrom(src => src.PackingPackage != null ? src.PackingPackage.DimensionHight : 0))
        .ForMember(dest => dest.DimensionLength, opt => opt.MapFrom(src => src.PackingPackage != null ? src.PackingPackage.DimensionLength : 0))
        .ForMember(dest => dest.DimensionWidth, opt => opt.MapFrom(src => src.PackingPackage != null ? src.PackingPackage.DimensionWidth : 0))
        .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price));

            CreateMap<Amanah.Posthub.Service.Domain.Entities.Service, ServiceResponseForMobileDTO>(MemberList.None)
         .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
        .ForMember(dest => dest.PhotoUrl, opt => opt.MapFrom(src => $"UploadFile/ServiceFile/" + (string.IsNullOrEmpty(src.PhotoUrl) ? "avtar.jpeg" : src.PhotoUrl)))
        .ForMember(dest => dest.Name, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Name_ar : src.Name_en))
        .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price));

            CreateMap<CreateRemarkInputDTO, Remark>(MemberList.None)
           .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar))
            .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en));

            CreateMap<UpdateRemarkInputDTO, Remark>(MemberList.None)
           .ForMember(dest => dest.Name_ar, opt => opt.MapFrom(src => src.Name_ar))
           .ForMember(dest => dest.Name_en, opt => opt.MapFrom(src => src.Name_en));

            CreateMap<CreateTermsAndConditionsCommand, TermsAndConditionsLookup>(MemberList.None);
            CreateMap<UpdateTermsAndConditionsCommand, TermsAndConditionsLookup>(MemberList.None);

            CreateMap<TermsAndConditionsLookup, TermsAndConditionsDto>(MemberList.None)
                .ForMember(des => des.Title, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.Title_Ar : src.Title_En))
                .ForMember(des => des.TermsAndConditions, opt => opt.MapFrom(src => Thread.CurrentThread.CurrentCulture.IsArabic() ? src.TermsAndConditions_Ar : src.TermsAndConditions_En));



            CreateMap<PackingPackageCountry, CountriesWithPackagesForMobileDTO>(MemberList.None)
           .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Country != null ? src.Country.Id : 0))
           .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Country != null ? src.Country.Code : ""))
           .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Country != null ? src.Country.Name : ""));


        }
    }
}
