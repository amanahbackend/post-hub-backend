﻿using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Features.MailItemStatusLogs.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.Express;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.AutomapperProfiles

{

    public class WorkOrderMapperProfile : Profile
    {
        public WorkOrderMapperProfile()
        {
            CreateMap<WorkOrder, WorkOrderListResponseDTO>(MemberList.None)
               //.ForMember(dest => dest.AreaRefCode, opt => opt.Ignore())
               //.ForMember(dest => dest.Areas, opt => opt.Ignore())
               .ForMember(dest => dest.AssignedDate, opt => opt.MapFrom(src => src.AssignedDate != null ? src.AssignedDate : (DateTime?)null))
               .ForMember(dest => dest.AssignedToCode, opt => opt.MapFrom(src => src.Driver.Code))
               .ForMember(dest => dest.AssignedToMobile, opt => opt.MapFrom(src => src.Driver.MobileNumber))
               .ForMember(dest => dest.AssignedToName, opt => opt.MapFrom(src => src.Driver.User.FirstName))
               .ForMember(dest => dest.AssignedToStatus, opt => opt.MapFrom(src => src.Driver.AgentStatus.Name))
               //.ForMember(dest => dest.DeliveryBefore, opt => opt.Ignore())
               .ForMember(dest => dest.DeliveryDate, opt => opt.MapFrom(src => src.DeliveryDate != null ? src.DeliveryDate : (DateTime?)null))
               //.ForMember(dest => dest.DispatchedDate, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToCode, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToMobile, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToName, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToStatus,opt=> opt.Ignore())
               .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.IssueAt))
               .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.MailItems.Count()))
               .ForMember(dest => dest.PickupDate, opt => opt.MapFrom(src => src.PickupDate))
               //.ForMember(dest => dest.SectorTypeName, opt => opt.Ignore())
               .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.WorkOrderStatus.Id))
               .ForMember(dest => dest.StatusName, opt => opt.MapFrom(src => src.WorkOrderStatus.Name))
               .ForMember(dest => dest.WOrderNo, opt => opt.MapFrom(src => src.Code));


            CreateMap<MailItemsStatusLog, TrackMailItemStatusLogDto>(MemberList.None)
            .ForMember(dest => dest.MailItemStatusId, opt => opt.MapFrom(src => src.MailItemStatusId))
            .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreationTime))
            .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.MailItem != null ? src.MailItemStatusId == (int)MailItemStatuses.Returned || src.MailItemStatusId == (int)MailItemStatuses.Delivered ?
              src.MailItem.DropOffAddress.Latitude : src.MailItemStatusId == (int)MailItemStatuses.New || src.MailItemStatusId == (int)MailItemStatuses.Dispatched
              || src.MailItemStatusId == (int)MailItemStatuses.OnDelivery ? src.MailItem.PickUpAddress.Latitude : "" : ""))

              .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.MailItem != null ? src.MailItemStatusId == (int)MailItemStatuses.Returned || src.MailItemStatusId == (int)MailItemStatuses.Delivered ?
              src.MailItem.DropOffAddress.Longitude : src.MailItemStatusId == (int)MailItemStatuses.New || src.MailItemStatusId == (int)MailItemStatuses.Dispatched
              || src.MailItemStatusId == (int)MailItemStatuses.OnDelivery ? src.MailItem.PickUpAddress.Longitude : "" : ""))

            // .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.MailItem != null ? src.MailItem.DropOffAddress != null ? src.MailItem.DropOffAddress.Longitude : "" : ""))
            .ForMember(dest => dest.MailItemStatusName, opt => opt.MapFrom(src => src.MailItemStatus != null ? src.MailItemStatus.Name : ""));

               

            CreateMap<WorkOrder, WorkOrderExportResponseDTO>(MemberList.None)
               //.ForMember(dest => dest.AreaRefCode, opt => opt.Ignore())
               //.ForMember(dest => dest.Areas, opt => opt.Ignore())
               .ForMember(dest => dest.AssignedDate, opt => opt.MapFrom(src => src.AssignedDate))
               .ForMember(dest => dest.AssignedToCode, opt => opt.MapFrom(src => src.Driver.Code))
               .ForMember(dest => dest.AssignedToMobile, opt => opt.MapFrom(src => src.Driver.MobileNumber))
               .ForMember(dest => dest.AssignedToName, opt => opt.MapFrom(src => src.Driver.User.FirstName))
               .ForMember(dest => dest.AssignedToStatus, opt => opt.MapFrom(src => src.Driver.AgentStatus.Name))
               //.ForMember(dest => dest.DeliveryBefore, opt => opt.Ignore())
               .ForMember(dest => dest.DeliveryDate, opt => opt.MapFrom(src => src.DeliveryDate))
               //.ForMember(dest => dest.DispatchedDate, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToCode, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToMobile, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToName, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToStatus,opt=> opt.Ignore())
               .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.IssueAt))
               .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.MailItems.Count()))
               .ForMember(dest => dest.PickupDate, opt => opt.MapFrom(src => src.PickupDate))
               //.ForMember(dest => dest.SectorTypeName, opt => opt.Ignore())
               .ForMember(dest => dest.StatusName, opt => opt.MapFrom(src => src.WorkOrderStatus.Name))
               .ForMember(dest => dest.WOrderNo, opt => opt.MapFrom(src => src.Code));

            CreateMap<CreateWorkOrderInputDTO, WorkOrder>(MemberList.None);
            CreateMap<CreateWorkOrderForExpressCommand, WorkOrder>(MemberList.None);
            CreateMap<UpdateWorkOrderDTO, WorkOrder>(MemberList.None);

            CreateMap<ActivateWorkOrderInputDTO, WorkOrder>(MemberList.None);

            CreateMap<WorkOrder, WorkOrderDetailsResponseDTO>(MemberList.None)
               //.ForMember(dest => dest.AreaRefCode, opt => opt.Ignore())
               //.ForMember(dest => dest.Areas, opt => opt.Ignore())
               .ForMember(dest => dest.AssignedDate, opt => opt.MapFrom(src => src.AssignedDate))
               .ForMember(dest => dest.AssignedToCode, opt => opt.MapFrom(src => src.Driver.Code))
               .ForMember(dest => dest.AssignedToMobile, opt => opt.MapFrom(src => src.Driver.MobileNumber))
               .ForMember(dest => dest.AssignedToName, opt => opt.MapFrom(src => src.Driver.User.FirstName))
               .ForMember(dest => dest.AssignedToStatus, opt => opt.MapFrom(src => src.Driver.AgentStatus.Name))
               //.ForMember(dest => dest.DeliveryBefore, opt => opt.Ignore())
               .ForMember(dest => dest.DeliveryDate, opt => opt.MapFrom(src => src.DeliveryDate))
               //.ForMember(dest => dest.DispatchedDate, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToCode, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToMobile, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToName, opt => opt.Ignore())
               //.ForMember(dest => dest.DispatchedToStatus,opt=> opt.Ignore())
               .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.IssueAt))
               .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.MailItems.Count()))
               .ForMember(dest => dest.PickupDate, opt => opt.MapFrom(src => src.PickupDate))
               //.ForMember(dest => dest.SectorTypeName, opt => opt.Ignore())
               .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => src.WorkOrderStatus.Id))
               .ForMember(dest => dest.StatusName, opt => opt.MapFrom(src => src.WorkOrderStatus.Name))
               .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.WorkOrderStatusId))
               .ForMember(dest => dest.Priorty, opt => opt.MapFrom(src => src.Priorty))
               .ForMember(dest => dest.WOrderNo, opt => opt.MapFrom(src => src.Code));

            CreateMap<WorkOrder, WorkOrderForMobileResponseDTO>(MemberList.None)

            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //  date
            .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.IssueAt))
             .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.AssignedDate))
            .ForMember(dest => dest.DueDate, opt => opt.MapFrom(src => src.DeliveryDate))

            .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.MailItems.Count()))
            .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.WorkOrderStatus.Id))
            .ForMember(dest => dest.WorkOrderStatusName, opt => opt.MapFrom(src => src.WorkOrderStatus.Name))
            .ForMember(dest => dest.RemainingNo, opt => opt.MapFrom(src => (src.MailItems.Count()) - (src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Returned || c.StatusId == (int)MailItemStatuses.New).Count())))
            .ForMember(dest => dest.ReturnedNo, opt => opt.MapFrom(src => src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Returned).Count()))
            .ForMember(dest => dest.ItemsInformationNo, opt => opt.MapFrom(src => (src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Delivered).Count())))
            .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.WorkOrderStatusId));

            CreateMap<WorkOrder, ClosedWorkOrderForMobileResponseDTO>(MemberList.None)

            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //  date
            .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.IssueAt))
             .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.AssignedDate))
            .ForMember(dest => dest.DueDate, opt => opt.MapFrom(src => src.DeliveryDate))

            .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.MailItems.Count()))
            .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.WorkOrderStatus.Id))
            .ForMember(dest => dest.WorkOrderStatusName, opt => opt.MapFrom(src => src.WorkOrderStatus.Name))
            .ForMember(dest => dest.RemainingNo, opt => opt.MapFrom(src => (src.MailItems.Count()) - (src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Delivered || c.StatusId == (int)MailItemStatuses.Returned).Count())))
            .ForMember(dest => dest.ReturnedNo, opt => opt.MapFrom(src => src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Returned).Count()))
            .ForMember(dest => dest.ItemsInformationNo, opt => opt.MapFrom(src => (src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Delivered).Count())))
            .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.WorkOrderStatusId))
            .ForMember(dest => dest.CloseDate, opt => opt.MapFrom(src => src.DeliveryDate));

            /////mailItem
            ///
            // CreateMap<UpdateMailItemInputDto, MailItem>(MemberList.None);

            CreateMap<UpdateMailItemInputDto, MailItem>().AfterMap((src, dest) =>
            {
                //dest.Id = src.Id;
                //dest.MailItemBarCode = src.MailItemBarCode;
                //dest.PickUpAddress = src.DropOffAddress;
                //dest.ReciverInfo = new Contact
                //{
                //    FirstName = src.RecieverName,
                //    Mobile = src.RecieverMobile
                //};
                // dest.WorkorderId = src.WorkOrderId;
                //  dest.StatusId = src.StatusId;
                // dest.DeliveryBefore = src.DeliveryBefore;
                //dest.SignatureImage =new AttachmentFile { Path = src.SignatureImagePath };
                //   dest.ProofOfDeliveryImage=new AttachmentFile { Path = src.ProofOfDeliveryImagePath };
                // dest.ItemReturnReasonId = src.ItemReturnReasonId;
                // dest.OtherItemReturnReason = src.OtherItemReturnReason;


                //if (src.ProofOfDeliveryImageURLs != null)
                //{
                //    dest.ProofOfDeliveryImages = new List<AttachmentFile>();
                //    foreach (var item in src.ProofOfDeliveryImageURLs)
                //    {
                //        dest.ProofOfDeliveryImages.Add(new AttachmentFile
                //        {
                //            Path = item
                //        });
                //    }
                //}

                //if (src.SignatureImageURL != null)
                //{
                //    dest.SignatureImages = new List<AttachmentFile>();
                //    dest.SignatureImages.Add(new AttachmentFile
                //    {
                //        Path = src.SignatureImageURL
                //    });
                //}

                //dest.DropOffDate = src.DropOffDate;
                //dest.DropOffDate = src.DropOffDate;
                //dest.Notes = src.Notes;
                //dest.PickUpAddress = new Address()
                //{
                //    Latitude = src.Latittude,
                //    Longitude = src.Longitude
                //};

            });

            CreateMap<MailItem, DeliveredMailItemsListResponseDTO>(MemberList.None)
                  .ForMember(dest => dest.BarCode, opt => opt.MapFrom(src => src.MailItemBarCode))
             .ForMember(dest => dest.DeliverDate, opt => opt.MapFrom(src => src.DeliveredDate))
             .ForMember(dest => dest.ReturnDate, opt => opt.MapFrom(src => src.ReturnedDate.HasValue ? src.ReturnedDate : null));

            CreateMap<WorkOrder, InternalWorkorderListForMobileDto>(MemberList.None)
                .ForMember(dest => dest.IssueAt, opt => opt.MapFrom(src => src.IssueAt))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.AssignedDate))
                .ForMember(dest => dest.DueDate, opt => opt.MapFrom(src => src.DeliveryDate))
                .ForMember(dest => dest.ItemsNo, opt => opt.MapFrom(src => src.MailItems.Count()))
                .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.WorkOrderStatus.Id))
                .ForMember(dest => dest.WorkOrderStatusName, opt => opt.MapFrom(src => src.WorkOrderStatus.Name))
                .ForMember(dest => dest.RemainingNo, opt => opt.MapFrom(src => (src.MailItems.Count()) - (src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Returned || c.StatusId == (int)MailItemStatuses.New).Count())))
                .ForMember(dest => dest.ReturnedNo, opt => opt.MapFrom(src => src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Returned).Count()))
                .ForMember(dest => dest.ItemsInformationNo, opt => opt.MapFrom(src => (src.MailItems.Where(c => c.StatusId == (int)MailItemStatuses.Delivered).Count())))
                .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.WorkOrderStatusId))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
                .ForMember(dest => dest.IsInternal, opt => opt.MapFrom(src => src.MailItems != null && src.MailItems.Count > 0 && src.MailItems.FirstOrDefault().Order.IsInternal))
                .ForMember(dest => dest.SenderName, opt => opt.MapFrom(src => src.MailItems.FirstOrDefault().Order.SenderName))
                .ForMember(dest => dest.BranchName, opt => opt.MapFrom(src => src.MailItems != null && src.MailItems.Count > 0 && !src.MailItems.FirstOrDefault().Order.IsInternal ? src.MailItems.FirstOrDefault().ConsigneeInfo.BranchName : null));

            CreateMap<MailItem, MailItemForMobileDto>(MemberList.None)
             .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.DriverId, opt => opt.MapFrom(src => src.Workorder != null ? src.Workorder.DriverId : 0))
             .ForMember(dest => dest.MailItemBarCode, opt => opt.MapFrom(src => src.MailItemBarCode))
             .ForMember(dest => dest.Area, opt => opt.MapFrom(src => src.DropOffAddress != null ? src.DropOffAddress.Area : ""))
             .ForMember(dest => dest.Flat, opt => opt.MapFrom(src => src.DropOffAddress != null ? src.DropOffAddress.Flat : ""))
             .ForMember(dest => dest.Floor, opt => opt.MapFrom(src => src.DropOffAddress != null ? src.DropOffAddress.Floor : ""))
             .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.DropOffAddress != null ? src.DropOffAddress.Street : ""))
             .ForMember(dest => dest.Building, opt => opt.MapFrom(src => src.DropOffAddress != null ? src.DropOffAddress.Building : ""))
             .ForMember(dest => dest.Block, opt => opt.MapFrom(src => src.DropOffAddress != null ? src.DropOffAddress.Block : ""))
             .ForMember(dest => dest.Governorate, opt => opt.MapFrom(src => src.DropOffAddress != null ? src.DropOffAddress.Governorate : ""))
             .ForMember(dest => dest.ContactName, opt => opt.MapFrom(src => src.ConsigneeInfo != null ? src.ConsigneeInfo.FirstName + src.ConsigneeInfo.MidelName + src.ConsigneeInfo.Lastname : ""))
             .ForMember(dest => dest.MobileNo, opt => opt.MapFrom(src => src.ConsigneeInfo != null ? src.ConsigneeInfo.Mobile : ""))
             .ForMember(dest => dest.WorkOrderId, opt => opt.MapFrom(src => src.WorkorderId))
             .ForMember(dest => dest.ItemstatusId, opt => opt.MapFrom(src => src.StatusId))
             .ForMember(dest => dest.Itemstatus, opt => opt.MapFrom(src => src.Status != null ? src.Status.Name : ""))
             .ForMember(dest => dest.WorkOrderStatusId, opt => opt.MapFrom(src => src.Workorder != null ? src.Workorder.WorkOrderStatus != null ? src.Workorder.WorkOrderStatus.Id : 0 : 0))
             .ForMember(dest => dest.WorkOrderStatus, opt => opt.MapFrom(src => src.Workorder != null ? src.Workorder.WorkOrderStatus != null ? src.Workorder.WorkOrderStatus.Name : "" : ""))
             .ForMember(dest => dest.DeliveryBefore, opt => opt.MapFrom(src => src.DeliveryBefore))
             .ForMember(dest => dest.ItemReturnReason, opt => opt.MapFrom(src => src.ItemReturnReason != null ? src.ItemReturnReason.Name : ""))
             .ForMember(dest => dest.ItemReturnReasonId, opt => opt.MapFrom(src => src.ItemReturnReason != null ? src.ItemReturnReason.Id : 0))
             .ForMember(dest => dest.OtherItemReturnReason, opt => opt.MapFrom(src => src.OtherItemReturnReason))
             .ForMember(dest => dest.SignatureImageUrl, opt => opt.MapFrom(src => (string.IsNullOrEmpty(src.SignatureImageURL) ? null : $"MailItemImages/Signature/{src.SignatureImageURL}")))
             .ForMember(dest => dest.ProofOfDeliveryImageUrl, opt => opt.MapFrom(src => src.ProofOfDeliveryImages != null ? src.ProofOfDeliveryImages.Select(a => $"MailItemImages/ProveOfDelivery/{ a.Path }").ToList() : null))
             .ForMember(dest => dest.ReceiverName, opt => opt.MapFrom(src => src.ReciverInfo != null ? src.ReciverInfo.FirstName + src.ReciverInfo.MidelName + src.ReciverInfo.Lastname : ""))
             .ForMember(dest => dest.ReceiverMobileNo, opt => opt.MapFrom(src => src.ReciverInfo != null ? src.ReciverInfo.Mobile : ""))
             .ForMember(dest => dest.ServiceSectorId, opt => opt.MapFrom(src => src.Order.ServiceSectorId))
             .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Notes)).AfterMap((src, dest) =>
             {

                 dest.ProofOfDeliveryImageUrl = new List<string>();

                 foreach (var item in src.ProofOfDeliveryImages)
                 {
                     dest.ProofOfDeliveryImageUrl.Add($"MailItemImages/ProveOfDelivery/" + (string.IsNullOrEmpty(item.Path)));
                 }

             });
            // .ForMember(dest => dest.DueDate, opt => opt.MapFrom(src =>  src.Order.DeliveryBefore ));

            CreateMap<MailItem, MailItemForDispatch>(MemberList.None)

                             .ForMember(dest => dest.ItemCode, opt => opt.MapFrom(src => src.MailItemBarCode))
                             .ForMember(dest => dest.ItemDeleveryTypeId, opt => opt.MapFrom(src => src.ItemDeleveryTypeId))
                             .ForMember(dest => dest.ItemDeleveryType, opt => opt.MapFrom(src => src.ItemDeleveryType.Name))
                             .ForMember(dest => dest.ItemType, opt => opt.MapFrom(src => src.ItemTypeId))
                             .ForMember(dest => dest.OrderCode, opt => opt.MapFrom(src => src.OrderId))
                             .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.OrderId))
                             .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.Name))
                             .ForMember(dest => dest.ItemWeight, opt => opt.MapFrom(src => string.Format("{0} {1}", src.Weight, src.WeightUOM != null ?
                                                                                                                    Thread.CurrentThread.CurrentCulture.IsArabic() ? src.WeightUOM.Name_ar : src.WeightUOM.Name_en : "")))
                             .ForMember(dest => dest.OrderIssueDate, opt => opt.MapFrom(src => src.Order.IssueAt))
                             .ForMember(dest => dest.DeliveryBeforeDate, opt => opt.MapFrom(src => src.Order.DeliveryBefore))
                             .ForMember(dest => dest.PersonalCustomersName, opt => opt.MapFrom(src => src.Order.BusinessOrder != null && src.Order.BusinessOrder.BusinessCustomer != null && !string.IsNullOrEmpty(src.Order.BusinessOrder.BusinessCustomer.BusinessName) ? src.Order.BusinessOrder.BusinessCustomer.BusinessName : ""))
                            //.ForMember(dest => dest.PersonalCustomerCompany, opt => opt.MapFrom(src => src.Order.BusinessOrder != null && src.Order.BusinessOrder.BusinessCustomer != null && !string.IsNullOrEmpty(src.Order.BusinessOrder.BusinessCustomer.BusinessName) ? src.Order.BusinessOrder.BusinessCustomer.MainContact.Company : ""))
                            .ForMember(dest => dest.SenderName, opt => opt.MapFrom(src => src.Order.BusinessOrder != null && src.Order.BusinessOrder.BusinessCustomer != null && src.Order.BusinessOrder.BusinessCustomer.MainContact != null && src.Order.BusinessOrder.BusinessCustomer.MainContact.User != null && !string.IsNullOrEmpty(src.Order.BusinessOrder.BusinessCustomer.MainContact.User.FirstName) ? src.Order.BusinessOrder.BusinessCustomer.MainContact.User.FirstName : ""))
                            .ForMember(dest => dest.ReceiverName, opt => opt.MapFrom(src => src.ConsigneeInfo != null ? src.ConsigneeInfo.FirstName : ""))
                             ;

            CreateMap<MailItemStatus, MailItemStatusDTO>(MemberList.None)
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            CreateMap<WorkOrderStatus, WorkOrderStatusDTO>(MemberList.None)
          .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
          .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            CreateMap<ReturnMailItemDTO, MailItem>(MemberList.None);

            CreateMap<UpdateWorkOrderStatusInputDTO, WorkOrder>(MemberList.None);

            CreateMap<ChangeWorkOrderStatusDTO, WorkOrder>(MemberList.None);

            CreateMap<MailItem, DeliveredMailItemsDetailsResponseDTO>(MemberList.None)
             .ForMember(dest => dest.DelivedDate, opt => opt.MapFrom(src => src.DeliveredDate))
     .ForMember(dest => dest.BusinessCustomerName, opt => opt.MapFrom(src => src.Order.BusinessOrder.BusinessCustomer.BusinessName))
       .ForMember(dest => dest.ParCode, opt => opt.MapFrom(src => src.MailItemBarCode))
     .ForMember(dest => dest.ConsigneeName, opt => opt.MapFrom(src => src.ConsigneeInfo.FirstName + src.ConsigneeInfo.MidelName + src.ConsigneeInfo.Lastname))
      .ForMember(dest => dest.RecieverName, opt => opt.MapFrom(src => src.ReciverInfo.FirstName + src.ReciverInfo.MidelName + src.ReciverInfo.Lastname))
        .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
        .ForMember(dest => dest.ReturnReason, opt => opt.MapFrom(src => src.ItemReturnReason.Name))

      ;

            CreateMap<MailItem, MailItemForm2DTO>(MemberList.None)
                             .ForMember(dest => dest.BusinessCustomerName, opt => opt.MapFrom(src => src.Order == null ? "" :
                                                                                  (src.Order.BusinessOrder == null ? " " :
                                                                                   (src.Order.BusinessOrder.BusinessCustomer == null ? "" : src.Order.BusinessOrder.BusinessCustomer.BusinessName))))
                             .ForMember(dest => dest.ConsigneeName, opt => opt.MapFrom(src => src.ConsigneeInfo == null ? "" : src.ConsigneeInfo.FirstName + " " + src.ConsigneeInfo.Lastname))
                             .ForMember(dest => dest.MailItemBarCode, opt => opt.MapFrom(src => src.MailItemBarCode))
                             .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                             .ForMember(dest => dest.ReturnResone, opt => opt.MapFrom(src => src.ItemReturnReason == null ? " " : src.ItemReturnReason.Name));





        }
    }
}