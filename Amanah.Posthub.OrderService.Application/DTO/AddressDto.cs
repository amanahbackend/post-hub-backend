﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class AddressDto
    {
        public string PACINumber { get; set; }
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
