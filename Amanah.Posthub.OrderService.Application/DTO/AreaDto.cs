﻿using System;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class AreaDto : BaseLookupsDto
    {
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public int StateId { get; set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime UpdatedDate { get; private set; }
        public DateTime DeletedDate { get; protected set; }
    }
}
