﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class AreaForDispatch
    {
        public string id { get; set; }
        public int? Raf { get; set; }
        public string Area { get; set; }
        //public string AssignedTo { get; set; }
        public int ItemsNo { get; set; }
    }
}
