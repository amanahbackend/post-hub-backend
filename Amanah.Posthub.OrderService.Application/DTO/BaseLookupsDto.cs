﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class BaseLookupsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public bool IsDeleted { get; set; }
    }
}
