﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class BlockForDispatch
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
