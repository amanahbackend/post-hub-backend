﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class BusinessOrderForLoadDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int? businessCustomerId { get; set; }
    }
}
