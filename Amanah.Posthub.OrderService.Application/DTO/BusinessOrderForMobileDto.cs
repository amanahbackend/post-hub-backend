﻿using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class BusinessOrderForMobileDto
    {
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public int? StatusId { get; set; }
        public string OrderStatus { get; set; }
        public int? OrderTypeId { get; set; }
        public string OrderType { get; set; }
        public int? ServiceSectorId { get; set; }
        public string ServiceSector { get; set; }
        public int? ServiceTypeId { get; set; }
        public string ServiceType { get; set; }
        public DateTime? IssueAt { get; set; }
        public DateTime? ReadyAt { get; set; }
        public DateTime? StartedAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public DateTime? FulfilledAt { get; set; }
        public decimal? TotalWeight { get; set; }
        public int? WeightUOMId { get; set; }
        public string WeightUOM { get; set; }
        public decimal? TotalPostage { get; set; }
        public int? PaymentMethodId { get; set; }
        public string PaymentMethod { get; set; }
        public int? CurrencyId { get; set; }
        public string Currency { get; set; }

        public int? DirectOrderId { get; set; }
        public string DirectOrder { get; set; }
        public int? TotalItemCount { get; set; }
        public string Address { get; set; }
        public string PickupNote { get; set; }
        public string DropOffNote { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int? ContactId { get; set; }
        public string ContactName { get; set; }
        public int? ContractId { get; set; }
        public int? MailItemsTypeId { get; set; }
        public string MailItemsTypeName { get; set; }
        public decimal? PostagePerPiece { get; set; }
        public int? CollectionFormSerialNo { get; set; }

        public int? HQBranchId { set; get; }
        public string BusinessName { set; get; }
        public int? BusinessTypeId { set; get; }
        public string BusinessCID { set; get; }
        public string BusinessCode { set; get; }
        public int? PickupLocationId { get; set; }
        public int? PickupRequestNotificationId { get; set; }
        public int? DepartmentId { get; set; }
        //
        public int? BusinessCustomerId { get; set; }
        public int? OrderById { get; set; }
        //
        public string BusinessTypeName { get; set; }
        public string OrderByName { get; set; }
        public string DepartmentName { get; set; }
        public string BusinessCustomerName { get; set; }
        public string BusinessCustomerCode { get; set; }
        public string HQBranchName { get; set; }
        public string ContractCode { get; set; }
        public string ContractStatus { get; set; }
        public int? BusinessCustomerBranchId { set; get; }
        public string BusinessCustomerBranchName { set; get; }
        public int? BusinessOrderId { get; set; }

        public long? AirWayBillNumber { get; set; }
        public int? CourierId { get; set; }
        public string CourierTrackingNumber { get; set; }
        public AddressDto PickUpAddress { get; set; }
        public bool IsMyAddress { get; set; }


        public decimal? OrderPackagePrice { get; set; }
        public int? PackingPackageId { get; set; }
        public List<ServiceResponseDTO> Services { get; set; }
        public List<MailItemDto> MailItem { get; set; }
        public string PaymentGateway { get; set; }
        public string TransactionId { get; set; }
        public string AuthCode { get; set; }
        public bool? IsCash { get; set; } 
    }
}
