﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class ClosedOrderDTO
    {
        public int Month { get; set; }
        public int Count { get; set; }
    }
}
