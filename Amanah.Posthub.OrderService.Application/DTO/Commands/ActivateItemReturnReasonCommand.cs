﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class ActivateItemReturnReasonCommand : IRequest<int>
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }
}
