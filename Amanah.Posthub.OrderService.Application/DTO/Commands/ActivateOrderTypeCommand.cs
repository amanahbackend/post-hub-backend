﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class ActivateOrderTypeCommand : IRequest<int>
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }
}
