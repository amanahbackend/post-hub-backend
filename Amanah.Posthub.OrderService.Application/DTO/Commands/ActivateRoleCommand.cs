﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class ActivateRoleCommand : IRequest<int>
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }
}
