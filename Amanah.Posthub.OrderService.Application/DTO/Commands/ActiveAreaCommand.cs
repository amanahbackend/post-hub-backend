﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class ActiveAreaCommand : IRequest<int>
    {
        public string Id { get; set; }
        public bool IsActive { get; set; }
    }
}
