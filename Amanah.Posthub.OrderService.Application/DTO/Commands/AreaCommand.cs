﻿using MediatR;
using System;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class AreaCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public int GovernrateId { get; set; }
        public bool IsDeleted { get; protected set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime UpdatedDate { get; private set; }
        public bool IsActive { get; set; }
        public DateTime DeletedDate { get; protected set; }
    }
}
