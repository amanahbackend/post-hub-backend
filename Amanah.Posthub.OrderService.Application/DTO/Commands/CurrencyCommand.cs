﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class CurrencyCommand : IRequest<int>
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string Code_en { get; set; }
        public string Code_ar { get; set; }
        public string Symbol { get; set; }
    }
}
