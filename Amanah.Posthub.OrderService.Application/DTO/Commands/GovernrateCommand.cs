﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class GovernrateCommand : IRequest<int>
    {
        public string Id { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int CountryId { get; set; }

    }
}
