﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class LanguageCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }
    }
}
