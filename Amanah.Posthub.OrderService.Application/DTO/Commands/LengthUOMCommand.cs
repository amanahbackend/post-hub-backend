﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class LengthUOMCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string Code { get; set; }

    }
}
