﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class MailItemTypeCommand : IRequest<int>
    {


        public int Id { get; set; }
        public bool IsDeleted { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }

    }
}
