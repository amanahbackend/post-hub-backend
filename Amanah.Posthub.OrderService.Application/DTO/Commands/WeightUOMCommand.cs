﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.DTO.Commands
{
    public class WeightUOMCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}