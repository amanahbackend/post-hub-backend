﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class ContactDto
    {
        public string FirstName { get; set; }
        public string MidelName { get; set; }
        public string Lastname { get; set; }
        public string Mobile { get; set; }
        public string Email{ get; set; }

        public int? BranchId { get; set; }
        public int? DepartmentId { get; set; }
        public string FloorNo { get; set; }

    }
}
