﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class ContractDto : BaseLookupsDto
    {
        public string Code { get; set; }
        public int? ContractStatusId { get; set; }
        public string ContractStatusName { get; set; }

    }
}
