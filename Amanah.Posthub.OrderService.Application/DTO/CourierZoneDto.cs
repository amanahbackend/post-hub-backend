﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class CourierZoneDto : BaseLookupsDto
    {
        public int CourierId { get; set; }
        public string CourierName { get; set; }

    }
}
