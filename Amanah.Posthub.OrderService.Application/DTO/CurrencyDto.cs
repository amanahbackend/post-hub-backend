﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class CurrencyDto 
    {
        public int Id { get; set; }
        public string Code_en { get; set; }
        public string Code_ar { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public bool IsDeleted { get; set; }            
        public string Symbol { get; set; }
    }
}
