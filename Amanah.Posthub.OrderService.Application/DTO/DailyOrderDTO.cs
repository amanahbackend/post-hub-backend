﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class DailyOrderDTO
    {
        // public DateTime CreateDate { get; set; }
        public int Count { get; set; }
        public string Day { get; set; }
    }
}
