﻿using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class DriverPhotoDTO
    {
        public int Id { get; set; }
        public IFormFile photo { get; set; }
    }
}
