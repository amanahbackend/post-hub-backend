﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class GovernrateDto : BaseLookupsDto
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
        public int? CountryId { get; set; }
        public string CountryName { get; set; }

    }
}
