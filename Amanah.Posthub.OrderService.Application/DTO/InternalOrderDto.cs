﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class InternalOrderDto
    {
        public int id { get; set; }
        public string Code { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
    }
}
