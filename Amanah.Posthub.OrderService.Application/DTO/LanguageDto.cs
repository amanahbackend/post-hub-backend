﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class LanguageDto : BaseLookupsDto
    {
        public string Code { get; set; }
    }
}
