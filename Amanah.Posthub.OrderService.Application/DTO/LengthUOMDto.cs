﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class LengthUOMDto : BaseLookupsDto
    {
        public string Code { get; set; }

    }
}
