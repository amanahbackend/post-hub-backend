﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class LookupResult<T>
    {
        public T Id { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
        public string Code { get; set; }
    }
}
