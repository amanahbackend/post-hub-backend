﻿using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class MailItemDto
    {
        public int Id { get; set; }
        public int? TotalItemCount { get; set; }
        public string MailItemBarCode { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public AddressDto PickUpAddress { get; set; }
        public AddressDto DropOffAddress { get; set; }
        public int? ConsigneeInfoId { get; set; }
        public ContactDto ConsigneeInfo { get; set; }
        public int? ReciverInfoId { get; set; }
        public ContactDto ReciverInfo { get; set; }
        public bool IsMatchConsigneeID { get; set; }
        public decimal? Weight { get; set; }
        public int? WeightUOMId { get; set; }
        public WeightUOMDto WeightUOM { get; set; }
        public int? Hight { get; set; }
        public int? Width { get; set; }
        public int? LengthUOMId { get; set; }
        public LengthUOMDto LengthUOM { get; set; }
        public string ReservedSpecial { get; set; }
        public string Notes { get; set; }
        public int? ItemTypeId { get; set; }
        public string ItemTypeName { get; set; }
        public MailItemTypeDto ItemType { get; set; }
        public int? StatusId { get; set; }
        public int? WorkorderId { get; set; }
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }
        public string FirstName { get; set; }
        public string ConsigneeInformation { get; set; }
        public int? OrderId { get; set; }
        public BusinessOrderDto Order { get; set; }
        public string FromSerial { get; set; }
        public DateTime? PickupDate { get; set; }
        public int? CaseNo { get; set; }
        public string Mobile { get; set; }
        public int? CountryId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? TotalPrice { get; set; }
        public string DriverName { get; set; }
        public string IssueDate { get; set; }
        //public string RecieverDescription { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string SignatureImageURL { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public List<string> ApprovedImagesUrl { get; set; }
        public string Status { get; set; }
        public string RecieverName { get; set; }

        public int? RemarkId { get; set; }
        public string Description { get; set; }
        public bool? PaymentResult { get; set; }
        public string ItemReturnReason { get; set; }


    }
}
