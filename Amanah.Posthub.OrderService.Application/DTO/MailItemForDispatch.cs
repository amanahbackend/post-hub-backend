﻿using System;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class MailItemForDispatch
    {
        public int Id { get; set; }

        // Item Info
        //public int? ItemCustomerType { get; set; }
        public string ItemCode { get; set; }
        public string ItemType { get; set; }
        public string ItemWeight { get; set; }

        // Item Status
        public int? ItemDeleveryTypeId { get; set; }
        public string ItemDeleveryType { get; set; }
        public string Status { get; set; }

        // Delivery Before
        public DateTime? DeliveryBeforeDate { get; set; }

        // Order Info
        public string OrderCode { get; set; }
        public int? OrderId { get; set; }

        //public AddressDto PickUpAddress { get; set; }
        //public AddressDto DropOffAddress { get; set; }

        public string OrderIssueDate { get; set; }

        //// Customer Info (Business)
        //public string BusinessCustomerLogo { get; set; }
        //public string BusinessIndustry { get; set; }
        //public string BusinessName { get; set; }
        //public string BusinessBranch { get; set; }

        //// Customer Info (Personal)
        public string PersonalCustomersName { get; set; }
        //public string PersonalCustomerCode { get; set; }
        public string PersonalCustomerCompany { get; set; }

        //// Sender
        public string SenderName { get; set; }
        //public string SenderAddress  { get; set; } // Area, Block, street

        //// Receiver
        public string ReceiverName { get; set; }
        //public string ReceiverAddress  { get; set; } // Area, Block, street




    }
}
