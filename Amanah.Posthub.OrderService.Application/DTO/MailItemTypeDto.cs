﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class MailItemTypeDto 
    {

        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public bool IsDeleted { get; set; }
    }
}
