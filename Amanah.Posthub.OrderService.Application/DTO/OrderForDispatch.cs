﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class OrderForDispatch
    {
        public int Id { get; set; }

        // Order Info
        public string OrderType { get; set; }
        public string OrderCode { get; set; }
        //public string OrderIssueDate { get; set; }

        // Delivery Date
        //public string DeliveryBeforeDate { get; set; }

        // Customer
        //public bool IsBusiness { get; set; }
        public string BusinessName { get; set; }
        //public string PersonalName { get; set; }
        public string CustomerName { get; set; }

        public string Status { get; set; }
        public int ItemsNo { get; set; }

        // Sender Info
        //public string SenderName { get; set; }
        //public string SenderAddress { get; set; } // PACI#, Gov., Area, Bock #, Street, Building
        //public string Latitude { get; set; }
        //public string Longitude { get; set; }

        //// Order status
        //public int OrderSatatusId { get; set; }

        //// Assigned to
        //public string Workorder { get; set; }
        //public string DriverName { get; set; }

    }
}
