﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class OrderListResponseDTO
    {

        public int Id { get; set; }

        ////Dates 
        public DateTime? IssueAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        //#TODO: Not Exist on the Order 
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? ReadyAt { get; set; }
        public DateTime? StartedAt { get; set; }

        ///Orde Info 
        public string OrderTypeId { get; set; }
        public string OrderTypeName { get; set; }
        public string OrderCode { get; set; }
        public decimal TotalWeight { get; set; }
        //#TODO: Not Exist on the Order 
        public decimal Price { get; set; }



        //Items Info 
        public int TotalItemCount { get; set; }
        //#TODO: Not Exist on the Order 
        public string Envelop { get; set; }

        public int StatusId { get; set; }
        public string StatusName { get; set; }

        ///BusinessOrders 
        public string BusinessOrders_Customer { get; set; }
        //#TODO: Not Exist on the Order 
        public string BusinessOrders_Business { get; set; }
        //#TODO: Not Exist on the Order 
        public string BusinessOrders_Branch { get; set; }
        //#TODO: Not Exist on the Order 
        public string BusinessOrders_BusinessCode { get; set; }


        ///Personal Orders 
        ///Where the come From 
        //#TODO: Not Exist on the Order 
        public string PersonalOrders_Name { get; set; }
        //#TODO: Not Exist on the Order 
        public string PersonalOrders_Company { get; set; }
        //#TODO: Not Exist on the Order 
        public string PersonalOrders_Code { get; set; }


        ///ٍSender 
        //BusinessOrders
        public string Sender_BusinessOrders_ContactName { get; set; }
        public string Sender_BusinessOrders_ContactMobile { get; set; }
        public string Sender_BusinessOrders_ContactAddress { get; set; }
        public string Sender_BusinessOrders_ContactBranch { get; set; }

        ///PersonalOrders 
        public string Sender_PersonalOrders_Name { get; set; }
        public string Sender_PersonalOrders_Mobile { get; set; }
        public Address Sender_PersonalOrders_Address { get; set; }

        ///Reciver 
        public string Reciver_PersonalOrders_Name { get; set; }
        public string Reciver_PersonalOrders_Mobile { get; set; }
        public Address Reciver_PersonalOrders_Address { get; set; }
        public string CourierTrackingNumber { get; set; }
        public string OrderPaymentStatus { get; set; }

    }
}
