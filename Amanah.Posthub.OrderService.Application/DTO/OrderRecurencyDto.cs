﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class OrderRecurencyDto : BaseLookupsDto
    {
        public string Code { get; set; }
    }
}
