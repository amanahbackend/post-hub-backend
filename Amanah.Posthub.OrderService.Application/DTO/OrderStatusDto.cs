﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class OrderStatusDto : BaseLookupsDto
    {
        public int Rank { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
