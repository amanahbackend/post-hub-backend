﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class PriceListStatusDto : BaseLookupsDto
    {
        public string Name { get; set; }
    }
}
