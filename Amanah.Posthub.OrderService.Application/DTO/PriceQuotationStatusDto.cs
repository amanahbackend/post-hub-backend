﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class PriceQuotationStatusDto : BaseLookupsDto
    {
        public string Code { get; set; }
    }
}
