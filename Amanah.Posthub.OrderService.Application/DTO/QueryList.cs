﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class QueryListReq
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
