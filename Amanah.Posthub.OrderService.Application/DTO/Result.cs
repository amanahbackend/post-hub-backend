﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class Result
    {
        public int TotalCount { get; set; }
        public string Error { get; set; }
        public object Value { get; set; }
    }

    public class ResultOrderTransaction
    {
        public string Error { get; set; }
        public object Value { get; set; }
        public string URL { get; set; }
        public string OrderValue { get; set; }
        public int? OrderType { get; set; }
    }
}
