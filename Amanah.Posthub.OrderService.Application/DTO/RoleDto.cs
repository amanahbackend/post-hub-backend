﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class RoleDto : BaseLookupsDto
    {
        public string Code { get; set; }
        public string Description { get; set; }

    }
}
