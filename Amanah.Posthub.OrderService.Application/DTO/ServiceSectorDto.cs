﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class ServiceSectorDto : BaseLookupsDto
    {
        public string Name { get; set; }
    }
}
