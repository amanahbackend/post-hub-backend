﻿using System;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class WorkorderForDispatchDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public DateTime? DeliveryBefore { get; set; }
    }
}
