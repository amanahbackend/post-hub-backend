﻿using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Infrastructure.Hangfire;
using Amanah.Posthub.Infrastructure.Hangfire.Attributes;
using Amanah.Posthub.OrderService.Application.AutomapperProfiles;
using Amanah.Posthub.SharedKernel.BackgroundJobs;
using Hangfire;
using Hangfire.SqlServer;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;

namespace Amanah.Posthub.OrderService.Application
{
    public static class DependencyInjection
    {
        public static void AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(typeof(OrderMapperProfile));
            services.AddScoped(typeof(IFirebasePushNotificationService), typeof(FirebasePushNotificationService));
            services.AddSingleton<IBackgroundJobManager, BackgroundJobManager>();
            services.AddScoped(typeof(IBackgroundJob<SendFireBaseNotificationJobModel>), typeof(SendFireBaseNotificationJob));
            services.AddHangfire((provider, hangfireConfig) => hangfireConfig
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseFilter(ActivatorUtilities.CreateInstance<StoreCurrentUserJobFilterAttribute>(provider))
                .UseSqlServerStorage(configuration["ConnectionString"], new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }))
                .AddHangfireServer(options =>
                { });
        }
    }
}
