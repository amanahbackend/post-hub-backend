﻿namespace Amanah.Posthub.OrderService.Application.Enums
{
    public enum AccountStatuses
    {
        Active = 1,
        Inactive = 2,
        Blocked = 3,
        BlockListed = 4
    }
}
