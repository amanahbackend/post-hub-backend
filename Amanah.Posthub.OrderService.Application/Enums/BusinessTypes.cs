﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Enums
{  
    public enum BusinessTypes { 
          Bank = 1,
         Court = 2,
        Society = 3,
        Club = 4,
        Lawyer = 5, Union = 6,
       Syndicate = 7, NGO = 8, Assosciation = 9,
      Advertisement = 10, Publications = 11,
       Other = 12,
  Individual = 13
    }
}
