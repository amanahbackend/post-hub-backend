﻿namespace Amanah.Posthub.OrderService.Application.Enums
{
    public enum ContractStatus
    {
        WaitingApproval = 1,
        Active = 2,
        Inactive = 3,
        OnHold = 4,
        Blocked = 5,
        Canceled = 6,
        InRenewal = 7,
        Closed = 8
    }
}
