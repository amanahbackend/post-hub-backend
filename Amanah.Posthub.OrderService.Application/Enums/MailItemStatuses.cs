﻿namespace Amanah.Posthub.OrderService.Application.Enums
{
    public enum MailItemStatuses
    {
        New = 1,
        Dispatched = 10,
        OnDelivery = 12,
        Delivered = 13,
        Returned = 15,
        ReadyForDispatch= 17
    }
}
