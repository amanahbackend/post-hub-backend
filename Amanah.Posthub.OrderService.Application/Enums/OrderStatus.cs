﻿namespace Amanah.Posthub.OrderService.Application.Enums
{
    public enum OrderStatuses
    {
        New = 1,
        InProcess = 6,
        Closed = 4,
        PostRoomReceived = 7,
        Received = 9
        //PartialyDispatched = 2,
        //Dispatched = 3,
    }
}
