﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Enums
{
    public enum PriceListStatuses
    {
        Draft = 1,
        Active = 2,
        Retired = 3,
        Special = 4
    }
}
