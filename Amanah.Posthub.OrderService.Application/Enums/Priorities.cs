﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Enums
{
    public enum Priorities
    {
        Later = 1,
        Normal = 2,
        Hight = 3,
        Critical = 4

    }
}
