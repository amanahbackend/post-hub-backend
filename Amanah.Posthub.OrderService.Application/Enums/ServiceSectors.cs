﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Enums
{
    public enum ServiceSectors
    {
        LocalPost = 1,
        LocalDelivery = 2,
        InternationalDelivery = 3,
        InternalPost = 4
    }
}
