﻿namespace Amanah.Posthub.OrderService.Application.Enums
{
    public enum WorkorderStatuses
    {
        New = 1,
        Assigned = 4,
        Dispatched = 5,
        Active = 7,
        Closed = 10,

        ReceivedByMandoob = 13,
        ReceivedByPostRoom = 14,
        ReceivedFromPostRoomByMandoob = 16,
        ReceivedByAnotherPostRoom = 17,
        ReceivedByAnotherMandoob = 18
    }
}
