﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Domain.Users.DomainServices;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.ActionLogs.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.SharedKernel.Authentication;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilites.UploadFile;

namespace Amanah.Posthub.OrderService.Application.Features.Accounts.Commands
{
    public class AccountRegistrationCommand : IRequest<int>
    {
        //AccountRegistrationDto
        public string BusinessName { set; get; }
        public string BusinessCID { set; get; }
        public IFormFile PersonalPhotoFile { set; get; }

        public string UserName { set; get; }
        public string Password { set; get; }
        public string FullName { set; get; }
        public string MobileNumber { get; set; }
        public int CountryId { set; get; }
        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address Location { set; get; }
    }
    public class AccountRegistrationCommandHandler : IRequestHandler<AccountRegistrationCommand, int>
    {
        private readonly IUnitOfWork _unityOfWork;
        private readonly ApplicationDbContext _context;
        private readonly IRepository<BusinessCustomer> _bussinesCustomerRepository;
        private readonly IRepository<BusinessContactAccount> _bussinesCustomerContactRepository;
        private readonly IRepository<BusinessCustomerBranch> _bussinesCustomerBranchRepository;
        private readonly IRepository<AccountLogs> _accountLogRepository;
        private readonly IUploadFormFileService _uploadFormFileService;
        private readonly IUserDomainService _userDomainService;
        private readonly IRoleDomainService _roleDomainService;

        public AccountRegistrationCommandHandler(IUnitOfWork unityOfWork,
            ApplicationDbContext context,
            IRepository<BusinessCustomer> bussinesCustomerRepository,
            IRepository<BusinessContactAccount> bussinesCustomerContactRepository,
            IRepository<BusinessCustomerBranch> bussinesCustomerBranchRepository,
            IRepository<AccountLogs> accountLogRepository,
            IUploadFormFileService uploadFormFileService,
            IUserDomainService userDomainService,
            IRoleDomainService roleDomainService
            )
        {
            _unityOfWork = unityOfWork;
            _context = context;
            _bussinesCustomerRepository = bussinesCustomerRepository;
            _bussinesCustomerContactRepository = bussinesCustomerContactRepository;
            _bussinesCustomerBranchRepository = bussinesCustomerBranchRepository;
            _accountLogRepository = accountLogRepository;
            _uploadFormFileService = uploadFormFileService;
            _userDomainService = userDomainService;
            _roleDomainService = roleDomainService;
        }
        public async Task<int> Handle(AccountRegistrationCommand command, CancellationToken cancellationToken)
        {
            int resultId = 0;
            await _unityOfWork.RunTransaction(async () =>
            {
                var bCustumer = new BusinessCustomer();

                bCustumer.AccountType = (int)BusinessCustomerType.IndividualCustomer;
                    //if (command.BusinessLogoFile != null)
                    //{
                    //    bCustumer.BusinessLogoUrl = await SetBusinessLogoAsync(command.PersonalPhotoFile);
                    //}
                    bCustumer.IsApproved = false;
                bCustumer.IsCompleted = true;
                bCustumer.BusinessTypeId = (int)BusinessTypes.Individual;
                bCustumer.BusinessName = command.FullName; // need test

                    _bussinesCustomerRepository.Add(bCustumer);

                    #region Create Login User

                    var roleName = "individualCustomer_" + DateTime.Now.ToString("yyyyMMddHHmmss");

                var user = new ApplicationUser();
                user.UserName = command.UserName;
                user.FirstName = command.FullName;
                user.Email = command.UserName;
                user.PhoneNumber = command.MobileNumber;
                user.IsLocked = false;

                var newUser = await _userDomainService.CreateAsync(user, command.Password, null, true);

                List<string> permissions = BsinessCustomerPermissionPovider.GetDafaultPermissions();
                var newrole = new ApplicationRole(roleName, newUser.Tenant_Id);
                newrole.Type = (int)RoleType.BusinessCustomer;
                var role = await _roleDomainService.CreateAsync(newrole, permissions);
                newUser.RoleNames = new List<string> { role.Name };
                await _userDomainService.AddUserToRolesAsync(newUser);
                    #endregion

                    var bCustumerContact = new BusinessContactAccount();
                bCustumerContact.UserID = newUser.Id;
                bCustumerContact.BusinessCustomerId = bCustumer.Id;
                    //if (command.PersonalPhotoFile != null)
                    //{
                    //    bCustumerContact.PersonalPhotoURL = await SetBusinessContactProfileAsync(command.PersonalPhotoFile);
                    //}
                    bCustumerContact.AccountStatusId = (int)AccountStatuses.Inactive;
                _bussinesCustomerContactRepository.Add(bCustumerContact);

                var bCustumerBranch = new BusinessCustomerBranch();
                bCustumerBranch.BusinessCustomerId = bCustumer.Id;
                bCustumerBranch.Name = command.FullName;
                bCustumerBranch.Email = command.UserName;
                bCustumerBranch.Mobile = command.MobileNumber;
                bCustumerBranch.CountryId = command.CountryId;
                bCustumerBranch.BranchContactId = bCustumerContact.Id;
                bCustumerBranch.PickupContactId = bCustumerContact.Id;
                bCustumerBranch.Location = command.Location;

                _bussinesCustomerBranchRepository.Add(bCustumerBranch);

                bCustumer.MainContactId = bCustumerContact.Id;
                bCustumer.HQBranchId = bCustumerBranch.Id;
                _bussinesCustomerRepository.Update(bCustumer);

                _accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "BussinesCustomer",
                    ActivityType = "Create",
                    Description = $"Manager {bCustumer.BusinessName} has been created from Mobile",
                    Record_Id = bCustumer.Id
                });

                await _context.SaveChangesAsync();
                    //resultId = bCustumer.Id;
                });

            return resultId;
        }
        private async Task<string> SetBusinessLogoAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "BusinessCustomerLogos";
                var processResult = await _uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }
        private async Task<string> SetBusinessContactProfileAsync(IFormFile file)
        {
            if (file != null)
            {
                var path = "BusinessContactProfile";
                var processResult = await _uploadFormFileService.UploadFileAsync(file, path);
                if (processResult.IsSucceeded)
                {
                    return processResult.ReturnData;
                }
                else
                {
                    throw processResult.Exception;
                }
            }
            return string.Empty;
        }
    }


    //}
}
