﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Accounts.Commands
{

    public class ActivateAccountCommand : IRequest<int>
    {
        public int BusinessCustomerId { get; set; }
        public class ActivateAccountCommandHandler : IRequestHandler<ActivateAccountCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public ActivateAccountCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(ActivateAccountCommand command, CancellationToken cancellationToken)
            {
                var bCAccounts = await _context
                    .BusinessCustomers.IgnoreQueryFilters()
                    .Where(a => a.Id == command.BusinessCustomerId)
                    .Include(x => x.BusinessCustomerContacts)
                        .ThenInclude(x => x.User)
                    .ToListAsync();

                if (bCAccounts == null)
                    return default;

                foreach (var businessCustomer in bCAccounts)
                {
                    businessCustomer.IsApproved = true;
                    foreach (var contact in businessCustomer.BusinessCustomerContacts)
                    {
                        contact.User.IsLocked = false;
                        contact.AccountStatusId = (int)AccountStatuses.Active;
                    }
                }
                await _context.SaveChangesAsync();
                return bCAccounts.FirstOrDefault().Id;
            }
        }
    }
}
