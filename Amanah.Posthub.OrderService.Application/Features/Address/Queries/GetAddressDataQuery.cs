﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Areas.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Address.Queries
{
    public class GetAddressDataQuery : IRequest<IEnumerable<object>>
    {

        public class GetAddressDataQueryHandler : IRequestHandler<GetAddressDataQuery, IEnumerable<object>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetAddressDataQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<IEnumerable<object>> Handle(GetAddressDataQuery query, CancellationToken cancellationToken)
            {
                var items = await _context
                     .Country
                     .Include(c => c.Governrates)
                     .ThenInclude(g => g.Areas)
                     .ThenInclude(a => a.Blocks)
                     .IgnoreQueryFilters()
                     .AsQueryable()
                     .AsNoTracking()
                     .IgnoreQueryFilters()
                     .Where(c => !c.IsDeleted)
                     .Select(c => new
                     {
                         Id = c.Id,
                         Name = c.Name,
                         Governrates = c.Governrates.Select(g => new
                         {
                             Id = g.Id,
                             NameEN = g.NameEN,
                             NameAR = g.NameAR,
                             Areas = g.Areas.Select(a => new
                             {
                                 Id = a.Id,
                                 NameEN = a.NameEN,
                                 NameAR = a.NameAR,
                                 Blocks = a.Blocks.Select(b => new
                                 {
                                     Id = b.Id,
                                     BlockNo = b.BlockNo
                                 })
                             })
                         }),
                     })
                     .ToListAsync();

                return items;
            }
        }
    }
}
