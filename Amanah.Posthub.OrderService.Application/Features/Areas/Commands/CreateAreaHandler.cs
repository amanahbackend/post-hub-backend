﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Areas.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Areas.Commands
{

    public class CreateAreaHandler : IRequestHandler<CreateAreaDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Area, int> _areaRepository;
        private readonly IRepository<Block, int> _blockRepository;
        private readonly IRepository<Governrate, int> _governateRepository;
        public CreateAreaHandler(ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Area, int> areaRepository,
            IRepository<Block, int> blockRepository,
            ILocalizer localizer, 
            IRepository<Governrate, int> governateRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _areaRepository = areaRepository;
            _blockRepository = blockRepository;
            _localizer = localizer;
            _governateRepository = governateRepository;
        }

        public async Task<int> Handle(CreateAreaDTO command, CancellationToken cancellationToken)
        {
            // avoid duplicate
           

            Area area = new Area();
            _unityOfWork.IsTenantFilterEnabled = false;


            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                // area = _mapper.Map<Area>(command);
                area.NameEN = command.NameEN;
                area.NameAR = command.NameAR;
                area.IsActive = true;// command.IsActive; Ramzy: set as true for now as its not needed now
                area.IsSystem = command.IsSystem;
                area.IsDeleted = false;
                area.FK_Governrate_Id = command.FK_Governrate_Id;
                area.Raf = command.Raf;
                area.CountryId = command.CountryId;
                if (command.AreaBlocks != null)
                {
                   
                    area.Blocks = command.AreaBlocks.Select(c => new Block
                    {
                        //    block.AreaId = area.Id;
                        IsActive = true, //c.IsActive, Ramzy: set as true for now as its not needed now
                        IsSystem = c.IsSystem,
                        BlockNo = c.BlockNo
                        //    area.Blocks.Add(block);
                    }).ToList();
                }
                var areaEF = _context.Area.Where(c => !c.IsDeleted && c.FK_Governrate_Id == command.FK_Governrate_Id && (c.NameAR == command.NameAR || c.NameEN == command.NameEN)).FirstOrDefault();
                if (areaEF == null)
                {
                    _areaRepository.Add(area);
                    await _unityOfWork.SaveChangesAsync();
                }
                else
                {
                    throw new DomainException(_localizer[Keys.Messages.AreaExist]);

                }



            });

            if (result)
            {
                return area.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
