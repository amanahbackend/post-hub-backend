﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.Areas.Commands.DTOs
{
    public class DeleteAreaDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
