﻿using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.Areas.Commands.DTOs
{
    public class UpdateAreaDTO : IRequest<int>
    {
        public int Id { get; set; }
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public int? CountryId { get; set; }
        public int? FK_Governrate_Id { get; set; }
        public int? Raf { get; set; }
        public List<BlockDTO> AreaBlocks { get; set; }

    }
}
