﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Areas.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Areas.Commands
{
    public class DeleteWorkOrderHandler : IRequestHandler<DeleteAreaDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Area, int> _areaRepository;


        public DeleteWorkOrderHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Area, int> areaRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _areaRepository = areaRepository;
        }
        public async Task<int> Handle(DeleteAreaDTO command, CancellationToken cancellationToken)
        {
            Area area = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                area = _context.Area.IgnoreQueryFilters().Where(c => c.Id == command.Id).FirstOrDefault();
                area.IsDeleted = true;
                _areaRepository.Update(area);
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return area.Id;
            }
            else
            {
                return 0;
            }

        }

    }
}
