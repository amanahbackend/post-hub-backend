﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Areas.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Areas
{

    public class UpdateAreaHandler : IRequestHandler<UpdateAreaDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Area, int> _areaRepository;
        private readonly ILocalizer _localizer;

        public UpdateAreaHandler(ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Area, int> areaRepository,
             ILocalizer localizer)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _areaRepository = areaRepository;
            _localizer = localizer;
        }

        public async Task<int> Handle(UpdateAreaDTO command, CancellationToken cancellationToken)
        {

            Area area = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                area = _context.Area.IgnoreQueryFilters().Where(c => c.Id == command.Id)
               .Include(c => c.Blocks)
               .FirstOrDefault();
                // area = _mapper.Map<Area>(command);
                area.NameEN = command.NameEN;
                area.NameAR = command.NameAR;
                area.IsActive = command.IsActive;
                area.IsSystem = command.IsSystem;
                area.FK_Governrate_Id = command.FK_Governrate_Id;
                area.CountryId = command.CountryId;
                area.Raf = command.Raf;
                if (command.AreaBlocks != null)
                {
                    if (area.Blocks == null)
                    {
                        area.Blocks = command.AreaBlocks.Select(c => new Block
                        {
                            IsActive = c.IsActive,
                            IsSystem = c.IsSystem,
                            BlockNo = c.BlockNo
                        }).ToList();
                    }
                    else
                    {
                        foreach (var item in command.AreaBlocks)
                        {
                            if (area.Blocks.Where(c => c.Id == item.Id && c.Id > 0).Any())
                            {
                                var blk = area.Blocks.Where(c => c.Id == item.Id).FirstOrDefault();
                                blk.BlockNo = item.BlockNo;
                                blk.IsActive = item.IsActive;
                                blk.IsSystem = item.IsSystem;
                            }
                            else
                            {
                                var blk = new Block();
                                blk.BlockNo = item.BlockNo;
                                blk.IsActive = item.IsActive;
                                blk.IsSystem = item.IsSystem;
                                area.Blocks.Add(blk);
                            }
                        }
                    }
                    var commandBlocks = command.AreaBlocks.Select(cm => cm.Id);
                    var toBeDeletedBlocks = area.Blocks.Where(m => !commandBlocks.Contains(m.Id));
                    if (toBeDeletedBlocks != null && toBeDeletedBlocks.Count() > 0)
                    {
                        foreach (var blc in toBeDeletedBlocks)
                        {
                            blc.IsDeleted = true;
                            _context.Blocks.Update(blc);
                        }
                    }



                }
                else
                {
                    area.Blocks = new List<Block>();
                }
                var areaEF = _context.Area.Where(c => !c.IsDeleted && c.Id !=command.Id && c.FK_Governrate_Id == command.FK_Governrate_Id && (c.NameAR == command.NameAR)).FirstOrDefault();
                if (areaEF == null)
                {
                    _areaRepository.Update(area);
                    await _unityOfWork.SaveChangesAsync();
                }
                else
                {
                    throw new DomainException(_localizer[Keys.Messages.AreaExist]);

                }
              
            });

            if (result)
            {
                return area.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
