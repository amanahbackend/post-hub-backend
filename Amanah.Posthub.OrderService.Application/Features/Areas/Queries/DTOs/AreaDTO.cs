﻿using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.Areas.Queries.DTOs
{
    public class AreaDTO
    {
        public int Id { get; set; }
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public int? FK_Governrate_Id { get; set; }
        public string GovernateName { get; set; }
        public int? FK_Country_Id { get; set; }
        public string CountryName { get; set; }
        public int? Raf { get; set; }
        public List<BlockDTO> AreaBlocks { get; set; }

    }
}
