﻿namespace Amanah.Posthub.OrderService.Application.Features.Areas.Queries.DTOs
{
    public class BlockDTO
    {
        public int Id { get; set; }
        public string BlockNo { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public int? AreaId { get; set; }
    }
}
