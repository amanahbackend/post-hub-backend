﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Areas.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Areas.Queries
{

    public class GetAllActiveAreaQuery : IRequest<List<AreaDTO>>
    {

        public class GetAllActiveAreaQueryHandler : IRequestHandler<GetAllActiveAreaQuery, List<AreaDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetAllActiveAreaQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<List<AreaDTO>> Handle(GetAllActiveAreaQuery query, CancellationToken cancellationToken)
            {
                var items = await _context
                     .Area
                     .AsQueryable()
                     .AsNoTracking()
                     .IgnoreQueryFilters()
                     .Where(c => !c.IsDeleted && c.IsActive == true)
                     .ProjectTo<AreaDTO>(_mapper.ConfigurationProvider).ToListAsync();

                return items;
            }
        }
    }
}
