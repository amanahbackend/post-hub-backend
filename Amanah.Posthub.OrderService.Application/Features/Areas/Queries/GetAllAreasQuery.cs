﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Areas.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Areas.Queries
{
    public class GetAllAreasQuery : PaginatedItemsViewModel, IRequest<PagedResult<AreaDTO>>
    {

        public class GetAllAreasQueryHandler : IRequestHandler<GetAllAreasQuery, PagedResult<AreaDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetAllAreasQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<PagedResult<AreaDTO>> Handle(GetAllAreasQuery query, CancellationToken cancellationToken)
            {
                var areas = new PagedResult<AreaDTO>();
                if (!string.IsNullOrEmpty(query.SearchBy))
                {
                    areas = await _context
                   .Area
                   .AsQueryable()
                   .AsNoTracking()
                   .IgnoreQueryFilters()
                   .Where(c => !c.IsDeleted && (c.NameEN.ToLower().Contains(query.SearchBy.ToLower())
                   || c.NameAR.ToLower().Contains(query.SearchBy.ToLower()))).OrderBy(c => c.Id)
                   .ProjectTo<AreaDTO>(_mapper.ConfigurationProvider)
                  .ToPagedResultAsync(query);

                }
                else
                {
                    areas = await _context
                   .Area
                   .AsQueryable()
                   .AsNoTracking()
                   .IgnoreQueryFilters()
                   .Where(c => !c.IsDeleted).OrderBy(c => c.Id)
                   .ProjectTo<AreaDTO>(_mapper.ConfigurationProvider)
                   .ToPagedResultAsync(query);

                }


                return areas;
            }
        }
    }
}
