﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Areas.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Areas.Queries
{
    public class GetAllBlocksByAreaIdQuery : IRequest<List<BlockDTO>>
    {
        public int? AreaId { get; set; }
        public class GetAllBlocksByAreaIdQueryHandler : IRequestHandler<GetAllBlocksByAreaIdQuery, List<BlockDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetAllBlocksByAreaIdQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<List<BlockDTO>> Handle(GetAllBlocksByAreaIdQuery query, CancellationToken cancellationToken)
            {

                var blocks = await _context
                  .Blocks
                  .AsQueryable()
                  .AsNoTracking()
                  .IgnoreQueryFilters()
                  .Where(c => !c.IsDeleted && c.AreaId == query.AreaId)
                  .ProjectTo<BlockDTO>(_mapper.ConfigurationProvider)
                 .ToListAsync();


                return blocks;
            }
        }
    }

}
