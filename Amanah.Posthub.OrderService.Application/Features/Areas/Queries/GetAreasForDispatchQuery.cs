﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Queries
{
    public class GetAreasForDispatchQuery : QueryListReq, IRequest<Result>
    {
        public class GetAreasForDispatchQueryHandler : IRequestHandler<GetAreasForDispatchQuery, Result>
        {
            private readonly ApplicationDbContext _context;
            public GetAreasForDispatchQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<Result> Handle(GetAreasForDispatchQuery query, CancellationToken cancellationToken)
            {
                var result = new Result();
                result.TotalCount = await _context.MailItems.IgnoreQueryFilters().Where(m => !m.IsDeleted 
                && !m.Order.IsDeleted
                && !m.Order.IsDraft
                && m.Order.ServiceSectorId== (int)ServiceSectors.LocalPost
                && !string.IsNullOrEmpty(m.DropOffAddress.Area) 
                && (m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.ReadyForDispatch))
                    .Select(a => a.DropOffAddress.Area)
                    .Distinct()
                    .CountAsync();

                if (result.TotalCount < 1)
                {
                    result.Error = "NotFound";
                    return result;
                }

                result.Value = await _context.MailItems.IgnoreQueryFilters().Where(m => !m.IsDeleted 
                && !m.Order.IsDeleted
                && !m.Order.IsDraft
                && m.Order.ServiceSectorId==(int) ServiceSectors.LocalPost
                && !string.IsNullOrEmpty(m.DropOffAddress.Area)
                && (m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.ReadyForDispatch))
                    .GroupBy(m => m.DropOffAddress.Area)
                    .Select(a => new AreaForDispatch()
                    {
                        id = a.Key,
                        //Raf=a.Raf, 
                        Area = a.Key,
                        ItemsNo = a.Count()
                    })
                    .OrderBy(m => m.id)
                    .Skip((query.PageNumber - 1) * query.PageSize)
                    .Take(query.PageSize)
                    .ToListAsync();

                return result;
            }
        }

    }

}
