﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.Blocks.Commands.DTOs
{
    public class CreateBlockDTO : IRequest<int>
    {
        public int Id { get; set; }
        public string PACINumber { get; set; }
        public string GovernorateNameEnglish { get; set; }
        public string GovernorateNameArabic { get; set; }
        public int GovernorateId { get; set; }
        public string AreaNameEnglish { get; set; }
        public string AreaNameArabic { get; set; }
        public int AreaId { get; set; }
        public string BlockNameEnglish { get; set; }
        public string BlockNameArabic { get; set; }
        public int BlockId { get; set; }
        public string StreetNameEnglish { get; set; }
        public string StreetNameArabic { get; set; }
        public int StreetId { get; set; }
        public decimal Longtiude { get; set; }
        public decimal Latitude { get; set; }
        public string AppartementNumber { get; set; }
        public string FloorNumber { get; set; }
        public string BuildingNumber { get; set; }
    }
}
