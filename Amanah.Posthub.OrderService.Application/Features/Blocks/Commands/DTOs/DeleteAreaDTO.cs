﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.Blocks.Commands.DTOs
{
    public class DeleteAreaDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
