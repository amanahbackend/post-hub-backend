﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Blocks.Queries
{
    public class GetBlocksByAreaIdForDispatchQuery : IRequest<IEnumerable<BlockForDispatch>>
    {
        public string AreaId { get; set; }
        public class GetBlocksByAreaIdForDispatchQueryHandler : IRequestHandler<GetBlocksByAreaIdForDispatchQuery, IEnumerable<BlockForDispatch>>
        {
            private readonly ApplicationDbContext _context;
            public GetBlocksByAreaIdForDispatchQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<BlockForDispatch>> Handle(GetBlocksByAreaIdForDispatchQuery query, CancellationToken cancellationToken)
            {
                //var x = 1;
                var orderList = await _context
                    .MailItems
                    .Include(m => m.Order)
                    .IgnoreQueryFilters()
                    .Where(m => !m.IsDeleted
                    && !m.Order.IsDeleted
                    && !m.Order.IsDraft
                    && m.Order.ServiceSectorId == (int)ServiceSectors.LocalPost
                    && m.DropOffAddress.Area.ToLower() == query.AreaId.ToLower()
                    && (m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.ReadyForDispatch))
                    //.GroupBy(m => m.DropOffAddress.Block)
                    .Select(a => new BlockForDispatch()
                    {
                        Id = a.DropOffAddress.Block,
                        Name = a.DropOffAddress.Block
                    })
                    .Distinct()
                    .OrderBy(m => m.Id)
                    .ToListAsync();

                if (orderList == null)
                {
                    return null;
                }
                //BlockForDispatch
                return orderList.AsReadOnly();
            }
        }

    }

}
