﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{
   
    public class ChangeReceiveOrderHandler : IRequestHandler<ChangeReceiveOrderInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
      //  private readonly IRepository<Order> _orderRepository;


        public ChangeReceiveOrderHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork
            //IRepository<Order> orderRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
          //  _orderRepository = orderRepository;
        }
        public async Task<int> Handle(ChangeReceiveOrderInputDTO command, CancellationToken cancellationToken)
        {
            Order order = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                 order = _context.Orders.Where(c => c.Id == command.OrderId).FirstOrDefault();
                if (order != null)
                {
                    order.StatusId = command.StatusId;
                    _context.Orders.Update(order);

                }

                await _unityOfWork.SaveChangesAsync();


            });
            if (result)
            {
                return order.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
