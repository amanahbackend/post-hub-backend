﻿
using Amanah.PaymentIntegrations.DTOs;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Amanah.PaymentIntegrations.DTOs.Enums;
using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using Microsoft.Extensions.Configuration;
using Utilities.Utilites.Exceptions;
using Amanah.Posthub.OrderService.Application.Shared.OrderPayment;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{
    public class CreateBusinessOrderCommand : IRequest<CreateBusinessOrderWithPaymentOutPut>
    {
        #region props
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public int? StatusId { get; set; }
        public int? OrderTypeId { get; set; }
        public int? ServiceSectorId { get; set; }
        public int? ServiceTypeId { get; set; }//>>
        public DateTime? IssueAt { get; set; }
        public DateTime? ReadyAt { get; set; }//>>
        public DateTime? StartedAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public DateTime? FulfilledAt { get; set; }
        public decimal TotalWeight { get; set; }
        public int? WeightUOMId { get; set; }
        public decimal TotalPostage { get; set; }
        public int? PaymentMethodId { get; set; }
        public int? CurrencyId { get; set; }
        //public int? TotalItemCount { get; set; }
        public List<MailItemDto> MailItems { get; set; }
        /// public Address PickupAddress { get; set; }
        public string PickupNote { get; set; }
        public decimal TotalOrderCostForPayment { get; set; }
        public string DropOffNote { get; set; }
        // Ramzy: need to be checked
        public string UserId { get; set; }
        ///////////////////////////////////////////////////////////////
        /// Business Order
        public int? BusinessCustomerId { get; set; }
        public int? BusinessCustomerBranchId { set; get; }

        //  public int? ContactId { get; set; }
        //   public int? ContractId { get; set; }
        public int? OrderById { get; set; }

        public int? MailItemsTypeId { get; set; }
        public decimal PostagePerPiece { get; set; }
        public int CollectionFormSerialNo { get; set; }
        //  public List<AttachmentFile> AttachedFiles { get; set; }
        public int? BusinessOrderId { get; set; }
        public string BusinessCustomerName { set; get; }
        public int? BusinessTypeId { set; get; }
        //  public string BusinessCID { set; get; }
        //  public string BusinessCode { set; get; }
        // public int? PickupLocationId { get; set; }
        public int? PickupRequestNotificationId { get; set; }
        public int? DepartmentId { get; set; }
        public int? TotalItemCount { get; set; }
        public int? ItemTypeId { get; set; }

        public int? AirWayBillNumber { get; set; }
        public int? CourierId { get; set; }
        public int? CourierTrackingNumber { get; set; }
        public List<IFormFile> OrderFile { set; get; }
        public List<string> ImageURL { get; set; }
        public AddressDto PickUpAddress { get; set; }
        public bool IsMyAddress { get; set; }


        public int? PackingPackageId { get; set; }
        public decimal? OrderPackagePrice { get; set; }
        public List<OrderServiceDto> BusinessOrderServices { get; set; }
        public string PaymentGateway { get; set; }

        #endregion

        public class CreateBusinessOrderCommandHandler :  IRequestHandler<CreateBusinessOrderCommand, CreateBusinessOrderWithPaymentOutPut>
        {
            private readonly ApplicationDbContext _context;
            private readonly ILogger<UnitOfWork> _logger;
            private readonly ICurrentUser _currentUser;
            private readonly IOrderPayment _orderPayment;
            private readonly IConfiguration _configuration;
            private readonly ILocalizer _localizer;
            public CreateBusinessOrderCommandHandler(ApplicationDbContext context,
                ILogger<UnitOfWork> logger,
                IConfiguration configuration,
                ICurrentUser currentUser,
                IOrderPayment orderPayment,
                ILocalizer localizer)
            {
                _context = context;
                _logger = logger;
                _currentUser = currentUser;
                _orderPayment = orderPayment;
                _configuration = configuration;
                _localizer = localizer;
            }
            public async Task<CreateBusinessOrderWithPaymentOutPut> Handle(CreateBusinessOrderCommand command, CancellationToken cancellationToken)
            {
                var order = new Order();
                CreateBusinessOrderWithPaymentOutPut payResponse = null;
                order.OrdersStatusLog = new List<OrdersStatusLog>()
                {
                    new OrdersStatusLog(){
                    CreatedBy = _currentUser.Id,
                    CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                    IsDeleted = false,
                    OrderStatusId = (int)OrderStatuses.New
                }};
                order.IsMyAddress = command.IsMyAddress;
                order.TotalItemCount = command.TotalItemCount;
                order.OrderCode = "OC" + (new Random()).Next(10000001, 99999999);
                order.StatusId = (int)OrderStatuses.New;
                order.OrderTypeId = 1;
                order.ServiceSectorId = command.ServiceSectorId;
                order.ServiceTypeId = command.ServiceTypeId;
                order.IssueAt = DateTime.Now;
                order.ReadyAt = command.ReadyAt;
                order.StartedAt = command.StartedAt;
                order.DeliveryBefore = command.DeliveryBefore;
                order.FulfilledAt = command.FulfilledAt;
                order.TotalWeight = command.TotalWeight;
                order.WeightUOMId = command.WeightUOMId;
                order.TotalPostage = command.TotalPostage;
                order.PaymentMethodId = command.PaymentMethodId;
                order.CurrencyId = command.CurrencyId;
                order.SenderFloorNo = order.SenderFloorNo != null ? order.SenderFloorNo : "not required"; //not required in international order
                //  order.PickupAddress = command.PickupAddress;
                if (command.PickUpAddress != null)
                {

                    order.PickupAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                    {
                        Area = command.PickUpAddress.Area,
                        Governorate = command.PickUpAddress.Governorate,
                        Street = command.PickUpAddress.Street,
                        Block = command.PickUpAddress.Block,
                        Building = command.PickUpAddress.Building,
                        Flat = command.PickUpAddress.Flat,
                        Floor = command.PickUpAddress.Floor,

                    };
                }
                order.PickupNote = command.PickupNote;
                order.DropOffNote = command.DropOffNote;
                order.CreatedBy = command.UserId;
                order.CreationTime = DateTime.UtcNow;
                order.IsDeleted = false;
                order.IsDraft = false;
                // order.BusinessOrderId = command.BusinessOrderId;
                // order.PickupLocationId = command.PickupLocationId;
                order.PickupRequestNotificationId = command.PickupRequestNotificationId;
                order.DepartmentId = command.DepartmentId;

                var businessOrder = new BusinessOrder();
                //businessOrder.AttachedFiles = !string.IsNullOrEmpty(command.ImageURL) ? new List<AttachmentFile>() { new AttachmentFile {
                //        Path=command.ImageURL
                //    } } : new List<AttachmentFile>();

                if (command.ImageURL != null && command.ImageURL.Count > 0)
                    businessOrder.AttachedFiles = command.ImageURL.Select(imgUrl => new AttachmentFile { Path = imgUrl }).ToList();

                // ContractId = command.ContractId,

                businessOrder.BusinessCustomerId = command.BusinessCustomerId;
                businessOrder.OrderById = command.OrderById;
                businessOrder.BusinessCustomerBranchId = command.BusinessCustomerBranchId;


                businessOrder.MailItemsTypeId = command.MailItemsTypeId;

                businessOrder.BusinessTypeId = command.BusinessTypeId;

                order.BusinessOrder = businessOrder;

                businessOrder.PackingPackageId = command.PackingPackageId != 0 && command.PackingPackageId != null ? command.PackingPackageId : null;
                businessOrder.OrderPackagePrice = command.OrderPackagePrice != 0 && command.OrderPackagePrice != null ? command.OrderPackagePrice : null;
                if (command.BusinessOrderServices != null)
                {
                    businessOrder.BusinessOrderServices = command.BusinessOrderServices.Select(s => new BusinessOrderService { ServiceId = s.ServiceId, Price = s.Price }).ToList();

                }


                //order.BusinessOrder = new BusinessOrder()
                //{

                //    AttachedFiles = !string.IsNullOrEmpty(command.ImageURL) ? new List<AttachmentFile>() { new AttachmentFile {
                //        Path=command.ImageURL
                //    } } : new List<AttachmentFile>(),

                //    OrderById = command.OrderById,
                //    // ContractId = command.ContractId,

                //    BusinessCustomerId = command.BusinessCustomerId,
                //    MailItemsTypeId = command.MailItemsTypeId,
                //    BusinessCustomerBranchId = command.BusinessCustomerBranchId,
                //    BusinessTypeId = command.BusinessTypeId


                //};

                //if (command.AirWayBillNumber!=null&& command.CourierId!=null&&
                //    command.CourierTrackingNumber!=null)
                //{

                //        order.OrderInternationalDelivery = new OrderInternationalDelivery()
                //        {
                //            AirWayBillNumber = command.AirWayBillNumber,
                //            CourierId = command.CourierId,
                //            CourierTrackingNumber = command.CourierTrackingNumber,
                //        };
                //}
                if (command.MailItems != null )
                {
                    order.MailItems = new List<MailItem>();

                    int mailItemId = await _context.MailItems.AsNoTracking().IgnoreQueryFilters().Select(x => x.Id).OrderBy(x => x).LastOrDefaultAsync();

                    int counter = mailItemId;
                    foreach (var m in command.MailItems)
                    {
                        var mailitem = new MailItem();
                        mailitem.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             // CreatedBy = command.UserId,
                             CreationTime =DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                             IsDeleted = false,
                             MailItemStatusId = (int)MailItemStatuses.New ,
                             WorkOrderId=m.WorkorderId
                         }
                         };
                        mailitem.CountryId = m.CountryId;
                        mailitem.TotalPrice = m.TotalPrice;
                        mailitem.StatusId = (int)MailItemStatuses.New;
                        mailitem.FromSerial = !string.IsNullOrEmpty(m.FromSerial) ? m.FromSerial : "";
                        mailitem.PickupDate = m.PickupDate;
                        mailitem.CaseNo = m.CaseNo;
                        mailitem.Quantity = m.Quantity;

                        mailitem.Weight = m.Weight != null ? m.Weight : null;
                        mailitem.WeightUOMId = m.WeightUOMId != null ? m.WeightUOMId : null;

                        //if (mailitem.ConsigneeInfoId == null)
                        //{
                        if (m.ConsigneeInfo != null && (!string.IsNullOrEmpty(m.ConsigneeInfo.FirstName) || !string.IsNullOrEmpty(m.ConsigneeInfo.Mobile)))
                            mailitem.ConsigneeInfo = new Contact
                            {
                                FirstName = m.ConsigneeInfo.FirstName,
                                Mobile = m.ConsigneeInfo.Mobile,
                            };
                        //}
                        //else
                        //{
                        //    mailitem.ConsigneeInfoId = m.ConsigneeInfoId;
                        //}
                        mailitem.ItemTypeId = command.ItemTypeId != null ? command.ItemTypeId : null;
                        mailitem.DeliveryBefore = m.DeliveryBefore != null ? m.DeliveryBefore : (DateTime?)null;
                        mailitem.IsMatchConsigneeID = m.IsMatchConsigneeID;
                        mailitem.MailItemBarCode = m.MailItemBarCode != null ? m.MailItemBarCode : null;
                        mailitem.ReservedSpecial = m.ReservedSpecial != null ? m.ReservedSpecial : null;
                        mailitem.Notes = m.Notes != null ? m.Notes : null;

                        if (mailitem.PickUpAddress != null)
                        {

                            mailitem.PickUpAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                            {
                                Area = m.PickUpAddress.Area,
                                Governorate = m.PickUpAddress.Governorate,
                                Street = m.PickUpAddress.Street,
                                Block = m.PickUpAddress.Block,
                                Building = m.PickUpAddress.Building,
                                Flat = m.PickUpAddress.Flat,
                                Floor = m.PickUpAddress.Floor,

                            };
                        }

                        mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                        {
                            Area = m.DropOffAddress.Area,
                            Governorate = m.DropOffAddress.Governorate,
                            Street = m.DropOffAddress.Street,
                            Block = m.DropOffAddress.Block,
                            Building = m.DropOffAddress.Building,
                            Flat = m.DropOffAddress.Flat,
                            Floor = m.DropOffAddress.Floor,
                        };

                        mailitem.DefaultAssignedDriverID = await GetDefaultDriverIdForMailITemAsync(mailitem);
                        if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                        {
                            counter += 1;
                            mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                        }
                        order.MailItems.Add(mailitem);
                    }
                }
                else
                    throw new DomainException(_localizer[Keys.Messages.MailitemRequired]);

                if (command.ServiceSectorId == 3 && command.MailItems.Count != 1)
                    throw new DomainException(_localizer[Keys.Messages.OnlyOneMailitemForInternationalOrder]);


                ///if business customer 
                if (_currentUser.IsBusinessCustomer())
                {
                    order.BusinessOrder.BusinessCustomerId = _currentUser.BusinessCustomerId;
                }
                if (command.BusinessTypeId == (int)BusinessTypes.Individual)
                {
                    var originalBusinessCustomerBranchId = _context.BusinessConactAccounts.FirstOrDefault(x => x.BusinessCustomerId == command.BusinessCustomerId)?.BusinessCustomerBranchId;
                    if (originalBusinessCustomerBranchId == 0 || originalBusinessCustomerBranchId == null)
                        order.BusinessOrder.BusinessCustomerBranchId = null;
                }

                _context.Orders.Add(order);
                await _context.SaveChangesAsync();

                if (command.ServiceSectorId == (int)ServiceSectors.InternationalDelivery)
                    return await _orderPayment.PayOrder(order.Id, command.TotalOrderCostForPayment, command.PaymentGateway);

                return payResponse = new CreateBusinessOrderWithPaymentOutPut { BusinessOrderId = order.Id, PaymentURL = "" };
            }

            private async Task<int?> GetDefaultDriverIdForMailITemAsync(MailItem item)
            {
                try
                {
                    var GeoFenceBlock = await _context.GeoFenceBlocks
                       .AsNoTracking()
                       .IgnoreQueryFilters()
                       .Where(x => x.Area == item.DropOffAddress.Area && x.Block == item.DropOffAddress.Block)
                       .Include(x => x.GeoFence)
                       .FirstOrDefaultAsync();

                    if (GeoFenceBlock == null)
                        return null;

                    var SelectedDriver = await _context.Driver
                        .AsNoTracking()
                        .IgnoreQueryFilters()
                        .Where(d =>
                           d.DriverDeliveryGeoFences.Any(x => x.GeoFenceId == GeoFenceBlock.GeoFenceId))
                       .FirstOrDefaultAsync();

                    if (SelectedDriver != null)
                    {
                        return SelectedDriver.Id;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }


            private string GenerateItemCodeAsync(int ItemId)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(ItemId):00000000}");
                return sb.ToString();
            }
        }
    }
    public class CreateBusinessOrderWithPaymentOutPut
    {
        public string PaymentURL { get; set; }
        public int BusinessOrderId { get; set; }
    }

}