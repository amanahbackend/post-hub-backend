﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Utilities.Utilites.Localization;
using Utilities.Utilites.Exceptions;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{
    public class CreateBusinessOrderForMobileCommand : IRequest<Result> 
    {
       // public string ServiceType { get; set; }
        public int? ServiceTypeId { get; set; }
        public FullAddress PickupAddress { get; set; }
       // public DateTime? OrderPlacementDateAndTime { get; set; }
        public DateTime? ReadyPickupAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        //public string ItemsType { get; set; }
        //public string ItemsTypeId { get; set; }
        public int ItemsNumber { get; set; }
        public List<NewMailItem> MailItems { get; set; }

        public int? MailItemsTypeId { get; set; }
        public int? BusinessCustomerId { get; set; }
        public int? BusinessConactAccountId { get; set; }
        public int? BusinessCustomerBranchId { set; get; }
        public string UserId { get; set; }
        public int? ServiceSectorId { get; set; }
        public List<IFormFile> OrderFiles { set; get; }
        public List<string> OrderFilesBase64 { set; get; }
        public List<string> ImagesURLs { get; set; }
        public class CreateBusinessOrderForMobileHandler : IRequestHandler<CreateBusinessOrderForMobileCommand, Result>
        {
            private readonly ApplicationDbContext _context;
            //private readonly ILogger<UnitOfWork> _logger;
            private readonly ICurrentUser _currentUser;
            private readonly IConfiguration _configuration;
            private readonly ILocalizer _localizer;
            public CreateBusinessOrderForMobileHandler(ApplicationDbContext context
                , ILogger<UnitOfWork> logger,
                ICurrentUser currentUser,
                 IConfiguration configuration)
            {
                _context = context;
                //_logger = logger;
                _currentUser = currentUser;
                _configuration = configuration;
            }

            public async Task<Result> Handle(CreateBusinessOrderForMobileCommand command, CancellationToken cancellationToken)
            {
                var result = new Result();
                var order = new Order();

                order.OrdersStatusLog = new List<OrdersStatusLog>()
                {
                    new OrdersStatusLog(){

                        CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                        CreatedBy=_currentUser.Id,
                        IsDeleted = false,
                        OrderStatusId = (int)OrderStatuses.New
                }};

                //order.IsMyAddress = command.IsMyAddress;
                order.TotalItemCount = command.ItemsNumber;
                order.OrderCode = "OC" + (new Random()).Next(10000001, 99999999);
                order.StatusId = (int)OrderStatuses.New;
                order.OrderTypeId = 1;
                //order.ServiceSectorId = command.ServiceSectorId; <=======
                order.ServiceTypeId = command.ServiceTypeId;
                order.ServiceSectorId = command.ServiceSectorId;
                order.IssueAt = DateTime.Now;
                order.ReadyAt = command.ReadyPickupAt;
                //order.StartedAt = command.StartedAt;
                order.DeliveryBefore = command.DeliveryBefore;
                //order.FulfilledAt = command.FulfilledAt;
                //order.TotalWeight = command.TotalWeight;
                //order.WeightUOMId = command.WeightUOMId;
                //order.TotalPostage = command.TotalPostage;
                //order.PaymentMethodId = command.PaymentMethodId;
                //order.CurrencyId = command.CurrencyId;

                //order.PickupNote = command.PickupNote;
                //order.DropOffNote = command.DropOffNote;
                order.CreatedBy = command.UserId;
                order.CreationTime = DateTime.UtcNow;
                order.IsDeleted = false;
                order.IsDraft = false;
                order.SenderFloorNo = "not required"; // not required in local post
                if (command.PickupAddress != null)
                {

                    order.PickupAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                    {
                        Area = command.PickupAddress.Area,
                        Governorate = command.PickupAddress.Governorate,
                        Street = command.PickupAddress.Street,
                        Block = command.PickupAddress.Block,
                        Building = command.PickupAddress.BuildingNo,
                        Flat = command.PickupAddress.FlatNo,
                        Floor = command.PickupAddress.FloorNo
                    };
                }

                order.BusinessOrder = new BusinessOrder()
                {
                    OrderById = command.BusinessConactAccountId,
                    //ContractId = command.ContractId,
                    BusinessCustomerId = command.BusinessCustomerId,
                    MailItemsTypeId = command.MailItemsTypeId,
                    BusinessCustomerBranchId = command.BusinessCustomerBranchId,
                    BusinessTypeId = 13
                };

                if (command.ImagesURLs != null && command.ImagesURLs.Count > 0)
                    order.BusinessOrder.AttachedFiles = command.ImagesURLs.Select(imgUrl => new AttachmentFile { Path = imgUrl }).ToList();

                if (command.MailItems != null)
                {
                    order.MailItems = new List<MailItem>();

                    int mailItemId = await _context.MailItems.IgnoreQueryFilters().Select(x => x.Id).OrderBy(x => x).LastOrDefaultAsync();

                    int counter = mailItemId;
                    foreach (var m in command.MailItems)
                    {
                        var mailitem = new MailItem();

                        mailitem.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                             {
                              CreatedBy = _currentUser.Id,
                             CreationTime =DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                             IsDeleted = false,
                             MailItemStatusId = (int)MailItemStatuses.New
                             }
                        };

                        mailitem.CountryId = m.DropOffAddress.CountryId;
                        mailitem.TotalPrice = m.TotalPrice;
                        mailitem.StatusId = (int)MailItemStatuses.New;
                        //mailitem.FromSerial = !string.IsNullOrEmpty(m.FromSerial) ? m.FromSerial : "";
                        //mailitem.PickupDate = m.PickupDate;
                        //mailitem.CaseNo = m.CaseNo;
                        mailitem.Weight = m.Weight != null ? m.Weight : null;
                        mailitem.WeightUOMId = m.WeightUOMId;
                        mailitem.Quantity = m.Quantity;
                        mailitem.ConsigneeInfo = new Contact
                        {
                            FirstName = m.ConsigneeFirstName,
                            Mobile = m.ConsigneeMobile,
                        };
                        mailitem.ItemTypeId = command.MailItemsTypeId != null ? command.MailItemsTypeId : null;
                        mailitem.DeliveryBefore = command.DeliveryBefore != null ? command.DeliveryBefore : (DateTime?)null;
                        mailitem.MailItemBarCode = m.MailItemBarCode != null ? m.MailItemBarCode : null;
                        //mailitem.ReservedSpecial = m.ReservedSpecial != null ? m.ReservedSpecial : null;
                        //mailitem.Notes = m.Notes != null ? m.Notes : null;

                        //mailitem.PickUpAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                        //{
                        //    Area = m.PickUpAddress.Area,
                        //    Governorate = m.PickUpAddress.Governorate,
                        //    Street = m.PickUpAddress.Street,
                        //    Block = m.PickUpAddress.Block,
                        //    Building = m.PickUpAddress.Building,
                        //    Flat = m.PickUpAddress.Flat,
                        //    Floor = m.PickUpAddress.Floor,

                        //};
                        // Ramzy: in this stage DropOffAddress impelemented
                        mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                        {
                            Area = m.DropOffAddress.Area,
                            Governorate = m.DropOffAddress.Governorate,
                            Street = m.DropOffAddress.Street,
                            Block = m.DropOffAddress.Block,
                            Building = m.DropOffAddress.BuildingNo,
                            Flat = m.DropOffAddress.FlatNo,
                            Floor = m.DropOffAddress.FloorNo
                        };

                        mailitem.DefaultAssignedDriverID = await GetDefaultDriverIdForMailITemAsync(mailitem);
                        if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                        {
                            counter += 1;
                            mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                        }
                        order.MailItems.Add(mailitem);


                    }
                }
                else
                    throw new DomainException(_localizer[Keys.Messages.MailitemRequired]);

                ///if business customer 
                if (_currentUser.IsBusinessCustomer())
                {
                    order.BusinessOrder.BusinessCustomerId = _currentUser.BusinessCustomerId; 
                }

                _context.Orders.Add(order);

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    //_logger.LogError(e, "Failed to run transaction in create order for mobile");
                    //throw new
                    //    (Keys.Validation.GeneralError);
                    result.Error = "error: " + ex.Message;
                }

                result.Value = order.Id;

                return result;
            }

            private async Task<int?> GetDefaultDriverIdForMailITemAsync(MailItem item)
            {
                try
                {
                    var GeoFenceBlock = await _context.GeoFenceBlocks
                        .IgnoreQueryFilters()
                       .Where(x => x.Area == item.DropOffAddress.Area && x.Block == item.DropOffAddress.Block)
                       .Include(x => x.GeoFence)
                       .FirstOrDefaultAsync();

                    var SelectedDriver = await _context.Driver
                        .IgnoreQueryFilters()
                        .Where(d =>
                           d.DriverDeliveryGeoFences.Any(x => x.GeoFenceId == GeoFenceBlock.GeoFenceId))
                       .FirstOrDefaultAsync();
                    if (SelectedDriver != null)
                    {
                        return SelectedDriver.Id;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception e)
                {
                    return null;
                }
            }


            private string GenerateItemCodeAsync(int ItemId)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(ItemId):00000000}");
                return sb.ToString();
            }
        }
    }
}
