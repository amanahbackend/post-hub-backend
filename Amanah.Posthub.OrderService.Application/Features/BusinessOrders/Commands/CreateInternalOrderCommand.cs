﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{
    public class CreateInternalOrderCommand : IRequest<Result>
    {
        public AddressDto PickUpAddress { get; private set; }

        public MailItemForInternalDto MailItem { get; set; }

        //public string SenderId { get; set; }
        public string SenderName { get; set; }
        public int? SenderBranchId { get; set; }
        public string SenderBranchName { get; set; }
        public string SenderEmail { get; set; }
        public string SenderFloorNo { get; set; }
        public bool IsInternal { get; set; }
        //public int? SecurityCaseNumber { get; set; }
        //public int? BranchId { get; set; }
        //public int? DriverId { get; set; }

        public class CreateInternalOrderCommandHandler : IRequestHandler<CreateInternalOrderCommand, Result>
        {

            private readonly ApplicationDbContext _context;
            private readonly ICurrentUser _currentUser;
            private readonly IUnitOfWork _unityOfWork;
            private readonly IFirebasePushNotificationService _pushNotificationService;
            private readonly IConfiguration _configuration;

            public CreateInternalOrderCommandHandler(ApplicationDbContext context,
                ICurrentUser currentUser,
                IUnitOfWork unityOfWork,
                IFirebasePushNotificationService pushNotificationService,
                IConfiguration configuration)
            {
                _context = context;
                _currentUser = currentUser;
                _unityOfWork = unityOfWork;
                _pushNotificationService = pushNotificationService;
                _configuration = configuration;
            }

            public async Task<Result> Handle(CreateInternalOrderCommand command, CancellationToken cancellationToken)
            {
                var result = new Result();

                try
                {
                    var driverId = await GetDefaultDriverIdForMailITemAsync(command);
                    if (!driverId.HasValue || driverId.Value <= 0)
                    {
                        result.Error = "No delegate assign to floor no. " + command.SenderFloorNo;
                        result.Value = false;
                    }
                    else
                        result.Value = await _unityOfWork.RunTransaction(async () =>
                        {
                            var order = new Order();

                            order.OrdersStatusLog = new List<OrdersStatusLog>()
                           {
                            new OrdersStatusLog(){
                                CreatedBy = _currentUser.Id,
                                CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                IsDeleted = false,
                                OrderStatusId = (int)OrderStatuses.New
                            },
                            new OrdersStatusLog()
                            {
                                CreatedBy = _currentUser.Id,
                                CreationTime =  DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                IsDeleted = false,
                                OrderStatusId = (int)OrderStatuses.InProcess
                            }
                           };

                            order.OrderCode = "OCI" + (new Random()).Next(10000001, 99999999);
                            order.StatusId = (int)OrderStatuses.New;
                            order.ServiceSectorId = (int)ServiceSectors.InternalPost;

                            order.IssueAt = DateTime.Now;
                            order.ReadyAt = DateTime.Now;
                            order.StartedAt = DateTime.Now;
                            order.DeliveryBefore = DateTime.Now.Date.AddDays(1).AddSeconds(-1);

                            if (command.PickUpAddress != null)
                            {

                                order.PickupAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                                {
                                    Area = command.PickUpAddress.Area,
                                    Governorate = command.PickUpAddress.Governorate,
                                    Street = command.PickUpAddress.Street,
                                    Block = command.PickUpAddress.Block,
                                    Building = command.PickUpAddress.Building,
                                    Flat = command.PickUpAddress.Flat,
                                    Floor = command.PickUpAddress.Floor,
                                };
                            }

                            order.CreatedBy = _currentUser.Id;
                            order.CreationTime = DateTime.UtcNow;
                            order.IsDeleted = false;
                            order.IsDraft = false;

                            ////order.SenderId = command.SenderId;
                            // if (string.IsNullOrEmpty(order.SenderId))
                            order.SenderName = command.SenderName;

                            order.SenderBranchId = command.SenderBranchId.HasValue ? command.SenderBranchId.Value : null;
                            order.SenderBranchName = command.SenderBranchName;

                            order.SenderEmail = command.SenderEmail;
                            order.SenderFloorNo = command.SenderFloorNo;
                            order.IsInternal = command.IsInternal;
                            order.StatementCode = order.OrderCode;

                            var mailitem = new MailItem();

                            int mailItemId = await _context.MailItems
                            .IgnoreQueryFilters()
                            .Select(x => x.Id)
                            .OrderBy(x => x)
                            .LastOrDefaultAsync();

                            int counter = mailItemId;

                            mailitem.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                            new MailItemsStatusLog()
                            {
                                CreatedBy = _currentUser.Id,
                                CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                IsDeleted = false,
                                MailItemStatusId = (int)MailItemStatuses.New
                            },
                            new MailItemsStatusLog()
                            {
                                CreatedBy = _currentUser.Id,
                                CreationTime = DateTime.UtcNow,
                                IsDeleted = false,
                                MailItemStatusId = (int)MailItemStatuses.Dispatched
                            }
                           };
                            mailitem.StatusId = (int)MailItemStatuses.New;

                            order.MailItems = new List<MailItem>();
                            mailitem.PickupDate = DateTime.Now;

                            mailitem.RemarkId = command.MailItem.RemarkId == 0 ? null : command.MailItem.RemarkId;
                            mailitem.Description = command.MailItem.Description;

                            //if (command.MailItem.ConsigneeInfo != null && (!string.IsNullOrEmpty(command.MailItem.ConsigneeInfo.FirstName) || !string.IsNullOrEmpty(command.MailItem.ConsigneeInfo.Mobile)))
                            mailitem.ConsigneeInfo = new Contact
                            {
                                FirstName = command.MailItem.ReciverName,
                                //Mobile = command.MailItem.Mobile,
                                Email = command.MailItem.ReciverEmail,
                                BranchId = command.MailItem.BranchId,
                                BranchName = command.MailItem.ReciverBranchName,
                                DepartmentId = command.MailItem.DepartmentId,
                                DepartmentName = command.MailItem.DepartmentName,
                                FloorNo = command.MailItem.ReciverfloorNo
                            };

                            //mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                            //{
                            //    Area = command.MailItem.DropOffAddress.Area,
                            //    Governorate = command.MailItem.DropOffAddress.Governorate,
                            //    Street = command.MailItem.DropOffAddress.Street,
                            //    Block = command.MailItem.DropOffAddress.Block,
                            //    Building = command.MailItem.DropOffAddress.Building,
                            //    Flat = command.MailItem.DropOffAddress.Flat,
                            //    Floor = command.MailItem.DropOffAddress.Floor,
                            //};


                            mailitem.DefaultAssignedDriverID = driverId == 0 ? null : driverId;// await GetDefaultDriverIdForMailITemAsync(command);
                            if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                            {
                                counter += 1;
                                mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                            }

                            ///if business customer 
                            if (_currentUser.IsBusinessCustomer() && _currentUser.BusinessCustomerId != null)
                            {
                                order.BusinessOrder.BusinessCustomerId = _currentUser.BusinessCustomerId;
                            }

                            //if (command.IsInternal)
                            //{
                            var workOrderCode = "WO" + (new Random()).Next(10000001, int.MaxValue).ToString();
                            mailitem.Workorder = new WorkOrder()
                            {
                                DriverId = mailitem.DefaultAssignedDriverID,
                                Code = workOrderCode,
                                WorkOrderStatusId = (int)WorkorderStatuses.Dispatched,
                                Priorty = WorkOrderPriorty.Hight,
                                PlannedStart = DateTime.Now,
                                IssueAt = DateTime.Now,
                                DeliveryBefore = DateTime.Now.Date.AddDays(1).AddSeconds(-1),
                                // StatementCode= workOrderCode, 
                                //SecurityCaseNumber=command.SecurityCaseNumber,
                            };
                            //}

                            order.MailItems.Add(mailitem);

                            _context.Orders.Add(order);


                            await _context.SaveChangesAsync();

                            if (command.IsInternal && mailitem.DefaultAssignedDriverID != null)
                            {
                                var driverUserId = await _context.Driver
                                .IgnoreQueryFilters()
                                .Where(x => x.Id == mailitem.DefaultAssignedDriverID)
                                .Select(x => x.UserId)
                                .FirstOrDefaultAsync();

                                await _pushNotificationService.SendAsync(driverUserId, "Post-Hub", "You Have New WorkOrder.", new { NotificationType = NotificationTypeEnum.NewWorkOrder });
                            }

                            await _unityOfWork.SaveChangesAsync();

                        });
                }
                catch (Exception ex)
                {
                    result.Error = ex.Message;
                    result.Value = false;
                    //throw;
                }

                return result;

            }

            private async Task<int?> GetDefaultDriverIdForMailITemAsync(CreateInternalOrderCommand command)
            {
                try
                {
                    int? driver = null;

                    if (command.IsInternal)
                    {
                        if (command.SenderBranchId.HasValue)
                            driver = await _context.DriverFloors
                                .Where(d => d.BranchId == command.SenderBranchId && d.Floor == command.SenderFloorNo)
                                .IgnoreQueryFilters()
                                .Select(d => d.DriverId)
                                .FirstOrDefaultAsync();
                        else
                            driver = await _context.DriverFloors
                            .Where(d => d.Branch.Name == command.SenderBranchName && d.Floor == command.SenderFloorNo)
                            .IgnoreQueryFilters()
                            .Select(d => d.DriverId)
                            .FirstOrDefaultAsync();
                    }
                    else
                    {
                        driver = await _context.DriverExternalBranchs
                            .Where(d => d.Branch.Name == command.MailItem.ReciverBranchName
                            && d.Driver.Branch.Name == command.SenderBranchName)
                            .IgnoreQueryFilters()
                            .Select(d => d.DriverId)
                            .FirstOrDefaultAsync();
                    }

                    return driver;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            private string GenerateItemCodeAsync(int ItemId)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(ItemId):00000000}");
                return sb.ToString();
            }

        }
    }
}
