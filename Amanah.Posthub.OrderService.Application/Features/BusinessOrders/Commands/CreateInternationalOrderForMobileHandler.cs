﻿using Amanah.PaymentIntegrations.DTOs;
using Amanah.PaymentIntegrations.DTOs.Enums;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Shared.OrderPayment;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;


namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{

    public class CreateInternationalOrderForMobileHandler : IRequestHandler<CreateBusinessOrderForMobileInputDTO, CreateBusinessOrderWithPaymentOutPut>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<UnitOfWork> _logger;
        private readonly ICurrentUser _currentUser;
        private readonly IOrderPayment _orderPayment;
        private readonly IConfiguration _configuration;
        private readonly ILocalizer _localizer;
        public CreateInternationalOrderForMobileHandler(ApplicationDbContext context,
             ILogger<UnitOfWork> logger,
             ICurrentUser currentUser,
             IOrderPayment orderPayment,
             IConfiguration configuration,
             ILocalizer localizer)
        {
            _context = context;
            _logger = logger;
            _currentUser = currentUser;
            _configuration = configuration;
            _localizer = localizer;
            _orderPayment = orderPayment;
        }
        public async Task<CreateBusinessOrderWithPaymentOutPut> Handle(CreateBusinessOrderForMobileInputDTO command, CancellationToken cancellationToken)
        {
            var order = new Order();

            order.OrdersStatusLog = new List<OrdersStatusLog>()
                {
                    new OrdersStatusLog(){
                    CreatedBy = _currentUser.Id,
                    CreationTime =  DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                    IsDeleted = false,
                    OrderStatusId =(int)OrderStatuses.New
                }};
            order.OrderCode = "OC" + (new Random()).Next(10000001, 99999999);
            order.StatusId = (int)OrderStatuses.New;
            order.OrderTypeId = (int)OrderType.Business;
            order.ServiceTypeId = command.ServiceTypeId;
            order.ServiceSectorId = (int)ServiceSectors.InternationalDelivery;
            order.IssueAt = DateTime.Now;
            order.ReadyAt = command.ReadyAt;
            order.DeliveryBefore = command.DeliveryBefore;
            order.PaymentMethodId = command.PaymentMethodId;

            order.SenderFloorNo = "";
            if (command.PickUpAddress != null)
            {
                order.PickupAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                {
                    Area = command.PickUpAddress.Area,
                    Governorate = command.PickUpAddress.Governorate,
                    Street = command.PickUpAddress.Street,
                    Block = command.PickUpAddress.Block,
                    Building = command.PickUpAddress.BuildingNo,
                    Flat = command.PickUpAddress.FlatNo,
                    Floor = command.PickUpAddress.FloorNo,
                    Latitude = command.PickUpAddress.Latitude,
                    Longitude = command.PickUpAddress.Longitude
                };
            }
            order.CreatedBy = command.UserId;
            order.CreationTime = DateTime.UtcNow;
            order.IsDeleted = false;
            order.IsDraft = false;

            var businessOrder = new BusinessOrder();

            if (command.ImagesURLs != null && command.ImagesURLs.Count > 0)
                businessOrder.AttachedFiles = command.ImagesURLs.Select(imgUrl => new AttachmentFile { Path = imgUrl }).ToList();

            businessOrder.BusinessCustomerId = command.BusinessCustomerId;
            businessOrder.OrderById = command.OrderById;
            businessOrder.BusinessCustomerBranchId = command.BusinessCustomerBranchId;
            businessOrder.MailItemsTypeId = command.MailItemsTypeId;
            order.BusinessOrder = businessOrder;
            businessOrder.PackingPackageId = command.PackingPackageId;
            businessOrder.OrderPackagePrice = command.OrderPackagePrice;
            if (command.BusinessOrderServices != null)
            {
                businessOrder.BusinessOrderServices = command.BusinessOrderServices.Select(s => new BusinessOrderService { ServiceId = s.ServiceId, Price = s.Price }).ToList();
            }

            if (command.MailItems != null)
            {
                order.MailItems = new List<MailItem>();
                int mailItemId = await _context.MailItems.IgnoreQueryFilters().Select(x => x.Id).OrderBy(x => x).LastOrDefaultAsync();
                int counter = mailItemId;
                foreach (var m in command.MailItems)
                {
                    var mailitem = new MailItem();
                    mailitem.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             CreatedBy = _currentUser.Id,
                             CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                             IsDeleted = false,
                             MailItemStatusId = (int)MailItemStatuses.New ,
                           //  WorkOrderId=m.WorkorderId
                         }
                         };
                    mailitem.TotalPrice = command.TotalWorkOrderPrice;
                    mailitem.CountryId = m.CountryId;
                    mailitem.StatusId = (int)MailItemStatuses.New;
                    mailitem.Quantity = m.Quantity;

                    mailitem.Weight = m.Weight != null ? m.Weight : null;
                    mailitem.WeightUOMId = m.WeightUOMId != null ? m.WeightUOMId : null;

                    mailitem.ConsigneeInfo = new Contact
                    {
                        FirstName = m.ConsigneeFirstName,
                        Mobile = m.ConsigneeMobile,
                    };
                    mailitem.ItemTypeId = command.ItemTypeId != null ? command.ItemTypeId : null;

                    mailitem.MailItemBarCode = m.MailItemBarCode != null ? m.MailItemBarCode : null;


                    //mailitem.PickUpAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                    //{
                    //    Area = m.PickUpAddress.Area,
                    //    Governorate = m.PickUpAddress.Governorate,
                    //    Street = m.PickUpAddress.Street,
                    //    Block = m.PickUpAddress.Block,
                    //    Building = m.PickUpAddress.Building,
                    //    Flat = m.PickUpAddress.Flat,
                    //    Floor = m.PickUpAddress.Floor,

                    //};

                    mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                    {
                        Area = m.DropOffAddress.Area,
                        Governorate = m.DropOffAddress.Governorate,
                        Street = m.DropOffAddress.Street,
                        Block = m.DropOffAddress.Block,
                        Building = m.DropOffAddress.BuildingNo,
                        Flat = m.DropOffAddress.FlatNo,
                        Floor = m.DropOffAddress.FloorNo,
                        Latitude = m.DropOffAddress.Latitude,
                        Longitude = m.DropOffAddress.Longitude
                    };

                    mailitem.DefaultAssignedDriverID = await GetDefaultDriverIdForMailITemAsync(mailitem);
                    if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                    {
                        counter += 1;
                        mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                    }
                    order.MailItems.Add(mailitem);
                }
            }
            else
                throw new DomainException(_localizer[Keys.Messages.MailitemRequired]);
            
            if (command.ServiceTypeId == 3 && command.MailItems.Count != 1)
                throw new DomainException(_localizer[Keys.Messages.OnlyOneMailitemForInternationalOrder]);

            ///if business customer 
            if (_currentUser.IsBusinessCustomer())
            {
                order.BusinessOrder.BusinessCustomerId = _currentUser.BusinessCustomerId;
            }

            _context.Orders.Add(order);
            try
            {
                await _context.SaveChangesAsync();
                string paymentMethod = _orderPayment.GetPaymentGateway(command.PaymentMethodId);
                return await _orderPayment.PayOrder(order.Id, command.TotalWorkOrderPrice, paymentMethod);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Failed to run transaction");
                throw new
                    (Keys.Validation.GeneralError);
            }
        }

        private async Task<int?> GetDefaultDriverIdForMailITemAsync(MailItem item)
        {
            var GeoFenceBlock = await _context.GeoFenceBlocks
               .IgnoreQueryFilters()
               .Where(x => x.Area == item.DropOffAddress.Area && x.Block == item.DropOffAddress.Block)
               .Include(x => x.GeoFence)
               .FirstOrDefaultAsync();

            if (GeoFenceBlock == null)
                return null;

            var SelectedDriver = await _context.Driver
               .IgnoreQueryFilters()
                .Where(d =>
                   d.DriverDeliveryGeoFences.Any(x => x.GeoFenceId == GeoFenceBlock.GeoFenceId))
               .FirstOrDefaultAsync();

            if (SelectedDriver != null)
            {
                return SelectedDriver.Id;
            }
            else
            {
                return null;
            }
        }


        private string GenerateItemCodeAsync(int ItemId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EX");
            sb.Append($"{(ItemId):00000000}");
            return sb.ToString();
        }

    }

}
