﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{

    public class CreateStatementHandler : IRequestHandler<CreateStatementInputDTO, PrintPostBagStatementDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly IFirebasePushNotificationService _pushNotificationService;
        private readonly IConfiguration _configuration;
        private ICurrentUser _currentUser;

        public CreateStatementHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<WorkOrder> workOrderRepository,
            IFirebasePushNotificationService pushNotificationService,
            IConfiguration configuration,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _pushNotificationService = pushNotificationService;
            _configuration = configuration;
            _currentUser = currentUser;
        }
        public async Task<PrintPostBagStatementDTO> Handle(CreateStatementInputDTO command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            List<MailItem> mailItems = new List<MailItem>();

            PrintPostBagStatementDTO printPostBagStatement = new PrintPostBagStatementDTO();

            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                var driver = await _context.DriverExternalBranchs
                .Include(d => d.Driver)
                .Where(d => d.BranchId == command.BranchId)
                .IgnoreQueryFilters()
                .Select(d => new { d.DriverId, d.Driver.UserId })
                .FirstOrDefaultAsync();

                // workOrder = _mapper.Map<WorkOrder>(command);
                var workOrderCode = "WO" + (new Random()).Next(10000001, int.MaxValue).ToString();
                workOrder = new WorkOrder()
                {
                    // DriverId = mailitem.DefaultAssignedDriverID,
                    Code = workOrderCode,
                    WorkOrderStatusId = (int)WorkorderStatuses.Dispatched,
                    Priorty = WorkOrderPriorty.Hight,
                    PlannedStart = DateTime.Now,
                    IssueAt = DateTime.Now,
                    DeliveryBefore = DateTime.Now.Date.AddDays(1).AddSeconds(-1),
                    SecurityCaseNumber = command.SecurityCaseNumber,
                    ServiceSectorId = (int?)ServiceSectors.InternalPost,
                    DriverId = driver != null ? driver.DriverId : null
                };
                _workOrderRepository.Add(workOrder);
                await _unityOfWork.SaveChangesAsync();

                ///Add Mail Items 
                ///
                var branch = await _context.Branch.IgnoreQueryFilters().Where(b => b.Id == command.BranchId).FirstOrDefaultAsync();

                mailItems = await _context.MailItems
                    .IgnoreQueryFilters()
                    .Where(x => x.ConsigneeInfo.BranchName == branch.Name && x.StatusId == (int?)MailItemStatuses.New)
                    .Include(c => c.MailItemsStatusLog)
                    .Include(c => c.ConsigneeInfo)
                    .Include(c => c.Workorder)
                    .ToListAsync();

                if (mailItems.Any())
                {
                    mailItems.ForEach(m =>
                    {
                        m.WorkorderId = workOrder.Id;
                        // save to logs ==>
                        if (m.MailItemsStatusLog != null && m.MailItemsStatusLog.Count > 0)
                        {
                            m.MailItemsStatusLog.Add(
                                 new MailItemsStatusLog()
                                 {
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                     IsDeleted = false,
                                     MailItemStatusId = (int)MailItemStatuses.Dispatched,
                                     WorkOrderId = workOrder.Id

                                 });
                        }
                        else
                        {
                            m.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             CreatedBy =  _currentUser.Id,
                             CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                             IsDeleted = false,
                             MailItemStatusId = (int) MailItemStatuses.Dispatched,
                             WorkOrderId = workOrder.Id
                         }
                         };
                        }

                    });
                    _context.MailItems.UpdateRange(mailItems);

                    if (mailItems[0].DefaultAssignedDriverID != null)
                    {
                        await _pushNotificationService.SendAsync(driver.UserId, "Post-Hub", "You Have New WorkOrder.", new { NotificationType = NotificationTypeEnum.NewWorkOrder });
                    }
                }



                await _unityOfWork.SaveChangesAsync();

            });
            if (result)
            {
                printPostBagStatement = _mapper.Map<PrintPostBagStatementDTO>(workOrder);
                if (mailItems.Any())
                {
                    var mailItemsDTO = _mapper.Map<List<MailItemResponseDTO>>(mailItems);
                    printPostBagStatement.ItemsList = mailItemsDTO;
                }

                return printPostBagStatement;
            }
            else
            {
                return default;
            }

        }
    }
}
