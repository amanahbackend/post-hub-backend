﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public class ChangeReceiveOrderInputDTO : IRequest<int>
    {
        public int OrderId { get; set; }
        public int StatusId { get; set; }
    }
}
