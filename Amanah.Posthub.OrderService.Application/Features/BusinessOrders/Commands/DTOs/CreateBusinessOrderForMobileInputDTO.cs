﻿using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public  class CreateBusinessOrderForMobileInputDTO:IRequest<CreateBusinessOrderWithPaymentOutPut>
    {
        #region props
        public int Id { get; set; }
      //  public string OrderCode { get; set; }
      //  public int? StatusId { get; set; }
      //  public int? OrderTypeId { get; set; }
       // public int? ServiceSectorId { get; set; }
        public int? ServiceTypeId { get; set; }
        public DateTime? ReadyAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public int PaymentMethodId { get; set; }
        public string UserId { get; set; } 
        public List<MailItemForMobileDTO> MailItems { get; set; }

        ///////////////////////////////////////////////////////////////
        /// Business Order
        public int? BusinessCustomerId { get; set; } 
        public int? BusinessCustomerBranchId { set; get; } 
        public int? OrderById { get; set; }
        public int? MailItemsTypeId { get; set; }
        //public int? AirWayBillNumber { get; set; }
        //public int? CourierId { get; set; }
        //public int? CourierTrackingNumber { get; set; }
        public List<IFormFile> OrderFiles { set; get; }
        public List<string> OrderFilesBase64 { set; get; }
        public List<string> ImagesURLs { get; set; }
        public FullAddress PickUpAddress { get; set; }
        //  public bool IsMyAddress { get; set; }
        public int? ItemTypeId { get; set; }
        public int PackingPackageId { get; set; }
        public decimal OrderPackagePrice { get; set; }
        public List<OrderServiceDto> BusinessOrderServices { get; set; }
        public decimal TotalWorkOrderPrice { get; set; }  
        // public string PaymentGateway { get; set; }
        #endregion


        //public string OrderPlacementDateAndTime { get; set; }

        //public string ItemsNumber { get; set; }

    }
}
