﻿using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public class CreateStatementInputDTO : IRequest<PrintPostBagStatementDTO>
    {
        public int? BranchId { get; set; }
        public int? SecurityCaseNumber { get; set; }

    }
}
