﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public class FullAddress
    {
        public string Country { get; set; }
        public int? CountryId { get; set; }
        public string Governorate { get; set; }
        public int? GovernorateId { get; set; }
        public string Area { get; set; }
        public int? AreaId { get; set; }
        public string Block { get; set; }
        public int? BlockId { get; set; }
        public string Street { get; set; }
        public int? StreetId { get; set; }
        public string BuildingNo { get; set; }
        public string FloorNo { get; set; }
        public string FlatNo { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
