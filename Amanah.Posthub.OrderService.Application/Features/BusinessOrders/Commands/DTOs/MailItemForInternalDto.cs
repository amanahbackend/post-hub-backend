﻿namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public class MailItemForInternalDto
    {
        public int Id { get; set; }
        public int? RemarkId { get; set; }
        public string Description { get; set; }
        public string ReciverName { get; set; }
        public string ReciverEmail { get; set; }
        public int? BranchId { get; set; }
        public string ReciverBranchName { get; set; }
        public int? DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string ReciverfloorNo { get; set; }



    }
}