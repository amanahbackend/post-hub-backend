﻿using Amanah.Posthub.OrderService.Application.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public class MailItemForMobileDTO
    {
        public int Id { get; set; }
        public int? TotalItemCount { get; set; }
        public string MailItemBarCode { get; set; }
       // public AddressDto PickUpAddress { get; set; }
        //  public AddressDto DropOffAddress { get; set; }
        public FullAddress DropOffAddress { get; set; }
        public int? CountryId { get; set; }

        public decimal? Weight { get; set; } //
        public int? WeightUOMId { get; set; } //
        public string ConsigneeFirstName { get; set; }
        public string ConsigneeMobile { get; set; }
        public decimal? Quantity { get; set; } //
        public decimal? TotalPrice { get; set; } //
      

    }
}
