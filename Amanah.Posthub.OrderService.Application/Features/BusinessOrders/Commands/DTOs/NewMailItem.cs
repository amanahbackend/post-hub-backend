﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public class NewMailItem 
    {
        public string ConsigneeFirstName { get; set; }
        public string ConsigneeName { get; set; } 
        public string ConsigneeMobile { get; set; }
        public decimal? Weight { get; set; }
        public string WeightUOM { get; set; }
        public int? WeightUOMId { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? TotalPrice { get; set; }
        // not needed as its loaded from the PQ depend on the wight in case of the local delevery
        public string ServiceTime { get; set; }
        public int? ServiceTimeId { get; set; }

        public FullAddress DropOffAddress { get; set; }
        public string MailItemBarCode { get; set; }
    }
}
