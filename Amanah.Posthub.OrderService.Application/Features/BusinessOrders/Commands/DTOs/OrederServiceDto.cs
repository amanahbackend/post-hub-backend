﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public class OrderServiceDto
    {
        public int ServiceId { get; set; }
        public decimal Price { get; set; }
    }
}
