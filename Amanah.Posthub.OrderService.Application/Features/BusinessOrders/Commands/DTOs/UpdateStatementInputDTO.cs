﻿using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs
{
    public class UpdateStatementInputDTO:IRequest<PrintPostBagStatementDTO>
    {
        public int WorkOrderId { get; set; }
        public IFormFile WorkOrderFile { set; get; }
        public string WorkOrderImageURL { get; set; }

    }
}
