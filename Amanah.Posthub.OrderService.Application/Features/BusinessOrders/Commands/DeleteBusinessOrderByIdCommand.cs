﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{
    public class DeleteBusinessOrderByIdCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteBusinessOrderByIdCommandHandler : IRequestHandler<DeleteBusinessOrderByIdCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteBusinessOrderByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteBusinessOrderByIdCommand command, CancellationToken cancellationToken)
            {
                var order = await _context.Orders.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (order == null) return default;
                order.IsDeleted = true;
                await _context.SaveChangesAsync();
                return order.Id;
            }
        }
    }
}
