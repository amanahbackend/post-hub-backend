﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.Express
{
    public class CreateBusinessOrderDraftForExpressCommand : IRequest<int>
    {
        #region props
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public int? StatusId { get; set; }
        public int? OrderTypeId { get; set; }
        public int? ServiceSectorId { get; set; }
        public int? ServiceTypeId { get; set; }//>>
        public DateTime? IssueAt { get; set; }
        public DateTime? ReadyAt { get; set; }//>>
        public DateTime? StartedAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public DateTime? FulfilledAt { get; set; }
        public decimal TotalWeight { get; set; }
        public int? WeightUOMId { get; set; }
        public decimal TotalPostage { get; set; }
        public int? PaymentMethodId { get; set; }
        public int? CurrencyId { get; set; }
        //public int? TotalItemCount { get; set; }
        public List<MailItemDto> MailItems { get; set; }
        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address PickupAddress { get; set; }
        public string PickupNote { get; set; }
        public string DropOffNote { get; set; }
        // Ramzy: need to be checked
        public string UserId { get; set; }
        ///////////////////////////////////////////////////////////////
        /// Business Order
        public int? BusinessCustomerId { get; set; }
        //   public int? ContactId { get; set; }
        // public int? ContractId { get; set; }
        public int? OrderById { get; set; }

        public int? MailItemsTypeId { get; set; }
        public decimal PostagePerPiece { get; set; }
        public int CollectionFormSerialNo { get; set; }
        public List<AttachmentFile> AttachedFiles { get; set; }
        public int? BusinessOrderId { get; set; }
        public int? BusinessCustomerBranchId { set; get; }
        public string BusinessName { set; get; }
        public int BusinessTypeId { set; get; }
        // public string BusinessCID { set; get; }
        // public string BusinessCode { set; get; }
        // public int? PickupLocationId { get; set; }
        public int? PickupRequestNotificationId { get; set; }
        public int? DepartmentId { get; set; }
        public int? TotalItemCount { get; set; }
        public int? ItemTypeId { get; set; }
        public int? AirWayBillNumber { get; set; }
        public int? CourierId { get; set; }
        public int? CourierTrackingNumber { get; set; }
        public List<IFormFile> OrderFiles { set; get; }
        public List<string> ImagesURLs { get; set; }
        public AddressDto PickUpAddress { get; set; }
        public bool IsMyAddress { get; set; }


        #endregion

        public class CreateBusinessOrderDraftForExpressCommandHandler : IRequestHandler<CreateBusinessOrderDraftForExpressCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public CreateBusinessOrderDraftForExpressCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(CreateBusinessOrderDraftForExpressCommand command, CancellationToken cancellationToken)
            {
                var order = new Order();


                order.OrdersStatusLog = new List<OrdersStatusLog>()
                {
                    new OrdersStatusLog(){
                    CreatedBy = command.UserId,
                    CreationTime = DateTime.UtcNow,
                    IsDeleted = false,
                    OrderStatusId =  (int)OrderStatuses.New
                }};
                order.IsMyAddress = command.IsMyAddress;
                order.TotalItemCount = command.TotalItemCount;
                order.OrderCode = "OrderCode" + (new Random()).Next(100001, 999999);
                order.StatusId = (int)OrderStatuses.New;
                order.OrderTypeId = 1;
                order.ServiceSectorId = command.ServiceSectorId;
                order.ServiceTypeId = command.ServiceTypeId;
                order.IssueAt = DateTime.Now;
                order.ReadyAt = command.ReadyAt;
                order.StartedAt = command.StartedAt;
                order.DeliveryBefore = command.DeliveryBefore;
                order.FulfilledAt = command.FulfilledAt;
                order.TotalWeight = command.TotalWeight;
                order.WeightUOMId = command.WeightUOMId;
                order.TotalPostage = command.TotalPostage;
                order.PaymentMethodId = command.PaymentMethodId;
                order.CurrencyId = command.CurrencyId;
                //   order.PickupAddress = command.PickupAddress;
                if (command.PickUpAddress != null)
                {

                    order.PickupAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                    {
                        Area = command.PickUpAddress.Area,
                        Governorate = command.PickUpAddress.Governorate,
                        Street = command.PickUpAddress.Street,
                        Block = command.PickUpAddress.Block,
                        Building = command.PickUpAddress.Building,
                        Flat = command.PickUpAddress.Flat,
                        Floor = command.PickUpAddress.Floor,

                    };
                }
                order.PickupNote = command.PickupNote;
                order.DropOffNote = command.DropOffNote;
                order.CreatedBy = command.UserId;
                order.CreationTime = DateTime.UtcNow;
                order.IsDeleted = false;
                order.IsDraft = true;
                // order.BusinessOrderId = command.BusinessOrderId;
                // order.PickupLocationId = command.PickupLocationId;
                order.PickupRequestNotificationId = command.PickupRequestNotificationId;
                order.DepartmentId = command.DepartmentId;

                if (command.ImagesURLs != null && command.ImagesURLs.Count > 0)
                    order.BusinessOrder.AttachedFiles = command.ImagesURLs.Select(imgUrl => new AttachmentFile { Path = imgUrl }).ToList();

                order.BusinessOrder = new BusinessOrder()
                {
                    //ContactId = command.ContactId,
                    OrderById = command.OrderById,
                    //ContractId = command.ContractId,
                    MailItemsTypeId = command.MailItemsTypeId,
                    BusinessCustomerBranchId = command.BusinessCustomerBranchId,
                    BusinessTypeId = command.BusinessTypeId

                };
                if (command.MailItems != null)
                {
                    order.MailItems = new List<MailItem>();
                    foreach (var m in command.MailItems)
                    {


                        var mailitem = new MailItem();

                        mailitem.Weight = m.Weight != null ? m.Weight : null;

                        if (mailitem.ConsigneeInfoId == null)
                        {
                            mailitem.ConsigneeInfo = new Contact
                            {
                                FirstName = m.ConsigneeInfo.FirstName,

                            };
                        }

                        else
                        {
                            mailitem.ConsigneeInfoId = m.ConsigneeInfoId;

                        }
                        mailitem.ItemTypeId = command.ItemTypeId != null ? command.ItemTypeId : null;
                        mailitem.DeliveryBefore = m.DeliveryBefore != null ? m.DeliveryBefore : (DateTime?)null;
                        mailitem.IsMatchConsigneeID = m.IsMatchConsigneeID;
                        mailitem.MailItemBarCode = m.MailItemBarCode != null ? m.MailItemBarCode : null;
                        mailitem.ReservedSpecial = m.ReservedSpecial != null ? m.ReservedSpecial : null;
                        mailitem.Notes = m.Notes != null ? m.Notes : null;

                        // Ramzy: not yet implemented
                        //mailitem.PickUpAddress = new Address
                        //{
                        //    Area = m.PickUpAddress.Area,
                        //    Governorate = m.PickUpAddress.Governorate,
                        //    Street = m.PickUpAddress.Street,
                        //    Block = m.PickUpAddress.Block,
                        //    Building = m.PickUpAddress.Building,
                        //    Flat=m.PickUpAddress.Flat,
                        //    Floor=m.PickUpAddress.Floor,

                        //};

                        // Ramzy: in this stage DropOffAddress impelemented
                        mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                        {
                            Area = m.DropOffAddress.Area,
                            Governorate = m.DropOffAddress.Governorate,
                            Street = m.DropOffAddress.Street,
                            Block = m.DropOffAddress.Block,
                            Building = m.DropOffAddress.Building,
                            Flat = m.DropOffAddress.Flat,
                            Floor = m.DropOffAddress.Floor,
                        };

                        // need to add mailitem status logs here

                        order.MailItems.Add(mailitem);
                    }
                }

                _context.Orders.Add(order);
                await _context.SaveChangesAsync();
                return order.Id;
            }
        }
    }
}
