﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading;
using Amanah.Posthub.OrderService.Application.Shared.OrderPayment;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.Express
{
    public class CreateBusinessOrderForExpressCommand : IRequest<CreateBusinessOrderWithPaymentOutPut>
    {
        #region props
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public int? StatusId { get; set; }
        public int? OrderTypeId { get; set; }
        public int? ServiceSectorId { get; set; }
        public int? ServiceTypeId { get; set; }//>>
        public DateTime? IssueAt { get; set; }
        public DateTime? ReadyAt { get; set; }//>>
        public DateTime? StartedAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public DateTime? FulfilledAt { get; set; }
        public decimal TotalWeight { get; set; }
        public int? WeightUOMId { get; set; }
        public decimal TotalPostage { get; set; }
        public int? PaymentMethodId { get; set; }
        public int? CurrencyId { get; set; }
        //public int? TotalItemCount { get; set; }
        public List<MailItemDto> MailItems { get; set; }
        /// public Address PickupAddress { get; set; }
        public string PickupNote { get; set; }
        public string DropOffNote { get; set; }
        // Ramzy: need to be checked
        public string UserId { get; set; }
        ///////////////////////////////////////////////////////////////
        /// Business Order
        public int? BusinessCustomerId { get; set; }
        public int? BusinessCustomerBranchId { set; get; }

        //  public int? ContactId { get; set; }
        //   public int? ContractId { get; set; }
        public int? OrderById { get; set; }

        public int? MailItemsTypeId { get; set; }
        public decimal PostagePerPiece { get; set; }
        public int CollectionFormSerialNo { get; set; }
        //  public List<AttachmentFile> AttachedFiles { get; set; }
        public int? BusinessOrderId { get; set; }
        public string BusinessCustomerName { set; get; }
        public int? BusinessTypeId { set; get; }
        //  public string BusinessCID { set; get; }
        //  public string BusinessCode { set; get; }
        // public int? PickupLocationId { get; set; }
        public int? PickupRequestNotificationId { get; set; }
        public int? DepartmentId { get; set; }
        public int? TotalItemCount { get; set; }
        public int? ItemTypeId { get; set; }

        public int? AirWayBillNumber { get; set; }
        public int? CourierId { get; set; }
        public int? CourierTrackingNumber { get; set; }
        public List<IFormFile> OrderFiles { set; get; }
        public List<string> ImagesURLs { get; set; }
        public AddressDto PickUpAddress { get; set; }
        public bool IsMyAddress { get; set; }

        public string PaymentGateway { get; set; }
        public decimal TotalOrderCostForPayment { get; set; }
        #endregion

        public class CreateBusinessOrderForExpressCommandHandler : IRequestHandler<CreateBusinessOrderForExpressCommand, CreateBusinessOrderWithPaymentOutPut>
        {
            private readonly ApplicationDbContext _context;
            private readonly ILogger<UnitOfWork> _logger;
            private readonly ICurrentUser _currentUser;
            private readonly IOrderPayment _orderPayment;
            public CreateBusinessOrderForExpressCommandHandler(ApplicationDbContext context,
                ILogger<UnitOfWork> logger,
                ICurrentUser currentUser,
                IOrderPayment orderPayment)
            {
                _context = context;
                _logger = logger;
                _currentUser = currentUser;
                _orderPayment = orderPayment;
            }
            public async Task<CreateBusinessOrderWithPaymentOutPut> Handle(CreateBusinessOrderForExpressCommand command, CancellationToken cancellationToken)
            {
                var order = new Order();
                CreateBusinessOrderWithPaymentOutPut payResponse = null;
                order.OrdersStatusLog = new List<OrdersStatusLog>()
                {
                    new OrdersStatusLog(){
                    CreatedBy = command.UserId,
                    CreationTime = DateTime.UtcNow,
                    IsDeleted = false,
                    OrderStatusId =(int)OrderStatuses.New
                }};
                order.IsMyAddress = command.IsMyAddress;
                order.TotalItemCount = command.TotalItemCount;
                order.OrderCode = "OC" + (new Random()).Next(10000001, 99999999);
                order.StatusId = (int)OrderStatuses.New;
                order.OrderTypeId = 1;
                order.ServiceSectorId = command.ServiceSectorId;
                order.ServiceTypeId = command.ServiceTypeId;
                order.IssueAt = DateTime.Now;
                order.ReadyAt = command.ReadyAt;
                order.StartedAt = command.StartedAt;
                order.DeliveryBefore = command.DeliveryBefore;
                order.FulfilledAt = command.FulfilledAt;
                order.TotalWeight = command.TotalWeight;
                order.WeightUOMId = command.WeightUOMId;
                order.TotalPostage = command.TotalPostage;
                order.PaymentMethodId = command.PaymentMethodId;
                order.CurrencyId = command.CurrencyId;
                //  order.PickupAddress = command.PickupAddress;
                order.SenderFloorNo = order.SenderFloorNo != null ? order.SenderFloorNo : "not required"; //not required in international order
                if (command.PickUpAddress != null)
                {

                    order.PickupAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                    {
                        Area = command.PickUpAddress.Area,
                        Governorate = command.PickUpAddress.Governorate,
                        Street = command.PickUpAddress.Street,
                        Block = command.PickUpAddress.Block,
                        Building = command.PickUpAddress.Building,
                        Flat = command.PickUpAddress.Flat,
                        Floor = command.PickUpAddress.Floor,

                    };
                }
                order.PickupNote = command.PickupNote;
                order.DropOffNote = command.DropOffNote;
                order.CreatedBy = command.UserId;
                order.CreationTime = DateTime.UtcNow;
                order.IsDeleted = false;
                order.IsDraft = false;
                // order.BusinessOrderId = command.BusinessOrderId;
                // order.PickupLocationId = command.PickupLocationId;
                order.PickupRequestNotificationId = command.PickupRequestNotificationId;
                order.DepartmentId = command.DepartmentId;

                var businessOrder = new BusinessOrder();

                if (command.ImagesURLs != null && command.ImagesURLs.Count > 0)
                    businessOrder.AttachedFiles = command.ImagesURLs.Select(imgUrl => new AttachmentFile { Path = imgUrl }).ToList();

                // ContractId = command.ContractId,

                businessOrder.BusinessCustomerId = command.BusinessCustomerId;
                businessOrder.OrderById = command.OrderById;
                businessOrder.BusinessCustomerBranchId = command.BusinessCustomerBranchId;


                businessOrder.MailItemsTypeId = command.MailItemsTypeId;

                businessOrder.BusinessTypeId = command.BusinessTypeId;

                order.BusinessOrder = businessOrder;

                //order.BusinessOrder = new BusinessOrder()
                //{

                //    AttachedFiles = !string.IsNullOrEmpty(command.ImageURL) ? new List<AttachmentFile>() { new AttachmentFile {
                //        Path=command.ImageURL
                //    } } : new List<AttachmentFile>(),

                //    OrderById = command.OrderById,
                //    // ContractId = command.ContractId,

                //    BusinessCustomerId = command.BusinessCustomerId,
                //    MailItemsTypeId = command.MailItemsTypeId,
                //    BusinessCustomerBranchId = command.BusinessCustomerBranchId,
                //    BusinessTypeId = command.BusinessTypeId


                //};

                //if (command.AirWayBillNumber!=null&& command.CourierId!=null&&
                //    command.CourierTrackingNumber!=null)
                //{

                //        order.OrderInternationalDelivery = new OrderInternationalDelivery()
                //        {
                //            AirWayBillNumber = command.AirWayBillNumber,
                //            CourierId = command.CourierId,
                //            CourierTrackingNumber = command.CourierTrackingNumber,
                //        };
                //}
                if (command.MailItems != null)
                {
                    order.MailItems = new List<MailItem>();

                    int mailItemId = await _context.MailItems.Select(x => x.Id).OrderBy(x => x).LastOrDefaultAsync();

                    int counter = mailItemId;
                    foreach (var m in command.MailItems)
                    {
                        var mailitem = new MailItem();
                        mailitem.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             // CreatedBy = command.UserId,
                             CreationTime = DateTime.UtcNow,
                             IsDeleted = false,
                             MailItemStatusId = (int)MailItemStatuses.New ,
                             WorkOrderId=m.WorkorderId
                         }
                         };
                        mailitem.CountryId = m.CountryId;
                        mailitem.TotalPrice = m.TotalPrice;
                        mailitem.StatusId = (int)MailItemStatuses.New;
                        mailitem.FromSerial = !string.IsNullOrEmpty(m.FromSerial) ? m.FromSerial : "";
                        mailitem.PickupDate = m.PickupDate;
                        mailitem.CaseNo = m.CaseNo;
                        mailitem.Quantity = m.Quantity;

                        mailitem.Weight = m.Weight != null ? m.Weight : null;
                        mailitem.WeightUOMId = m.WeightUOMId != null ? m.WeightUOMId : null;

                        //if (mailitem.ConsigneeInfoId == null)
                        //{
                        if (m.ConsigneeInfo != null && (!string.IsNullOrEmpty(m.ConsigneeInfo.FirstName) || !string.IsNullOrEmpty(m.ConsigneeInfo.Mobile)))
                            mailitem.ConsigneeInfo = new Contact
                            {
                                FirstName = m.ConsigneeInfo.FirstName,
                                Mobile = m.ConsigneeInfo.Mobile,
                            };
                        //}
                        //else
                        //{
                        //    mailitem.ConsigneeInfoId = m.ConsigneeInfoId;
                        //}
                        mailitem.ItemTypeId = command.ItemTypeId != null ? command.ItemTypeId : null;
                        mailitem.DeliveryBefore = m.DeliveryBefore != null ? m.DeliveryBefore : (DateTime?)null;
                        mailitem.IsMatchConsigneeID = m.IsMatchConsigneeID;
                        mailitem.MailItemBarCode = m.MailItemBarCode != null ? m.MailItemBarCode : null;
                        mailitem.ReservedSpecial = m.ReservedSpecial != null ? m.ReservedSpecial : null;
                        mailitem.Notes = m.Notes != null ? m.Notes : null;

                        mailitem.PickUpAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                        {
                            Area = m.PickUpAddress.Area,
                            Governorate = m.PickUpAddress.Governorate,
                            Street = m.PickUpAddress.Street,
                            Block = m.PickUpAddress.Block,
                            Building = m.PickUpAddress.Building,
                            Flat = m.PickUpAddress.Flat,
                            Floor = m.PickUpAddress.Floor,

                        };
                        // Ramzy: in this stage DropOffAddress impelemented
                        mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                        {
                            Area = m.DropOffAddress.Area,
                            Governorate = m.DropOffAddress.Governorate,
                            Street = m.DropOffAddress.Street,
                            Block = m.DropOffAddress.Block,
                            Building = m.DropOffAddress.Building,
                            Flat = m.DropOffAddress.Flat,
                            Floor = m.DropOffAddress.Floor,
                        };

                        // need to add mailitem status logs here
                        mailitem.DefaultAssignedDriverID = await GetDefaultDriverIdForMailITemAsync(mailitem);
                        if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                        {
                            counter += 1;
                            mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                        }
                        order.MailItems.Add(mailitem);
                    }
                }

                ///if business customer 
                if (_currentUser.IsBusinessCustomer())
                {
                    order.BusinessOrder.BusinessCustomerId = _currentUser.BusinessCustomerId;
                }


                _context.Orders.Add(order);
                await _context.SaveChangesAsync();

                if (command.ServiceSectorId == (int)ServiceSectors.InternationalDelivery)
                    return await _orderPayment.PayOrder(order.Id, command.TotalOrderCostForPayment, command.PaymentGateway);

                return payResponse = new CreateBusinessOrderWithPaymentOutPut { BusinessOrderId = order.Id, PaymentURL = "" };

            }

            private async Task<int?> GetDefaultDriverIdForMailITemAsync(MailItem item)
            {
                try
                {
                    var GeoFenceBlock = await _context.GeoFenceBlocks
                       .Where(x => x.Area == item.DropOffAddress.Area && x.Block == item.DropOffAddress.Block)
                       .Include(x => x.GeoFence)
                       .FirstOrDefaultAsync();

                    if (GeoFenceBlock == null)
                        return null;

                    var SelectedDriver = await _context.Driver
                        .Where(d =>
                           d.DriverDeliveryGeoFences.Any(x => x.GeoFenceId == GeoFenceBlock.GeoFenceId))
                       .FirstOrDefaultAsync();

                    if (SelectedDriver != null)
                    {
                        return SelectedDriver.Id;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }


            private string GenerateItemCodeAsync(int ItemId)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(ItemId):00000000}");
                return sb.ToString();
            }

        }
    }

}
