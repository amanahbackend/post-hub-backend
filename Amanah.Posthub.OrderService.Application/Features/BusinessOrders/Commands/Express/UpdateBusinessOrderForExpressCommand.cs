﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.Express
{
    public class UpdateBusinessOrderForExpressCommand : IRequest<int>
    {
        #region props
        public AddressDto PickUpAddress { get; set; }
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public int? StatusId { get; set; }
        public int? OrderTypeId { get; set; }
        public int? ServiceSectorId { get; set; }
        public int? ServiceTypeId { get; set; }//>>
        public DateTime? IssueAt { get; set; }
        public DateTime? ReadyAt { get; set; }//>>
        public DateTime? StartedAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public DateTime? FulfilledAt { get; set; }
        public decimal TotalWeight { get; set; }
        public int? WeightUOMId { get; set; }
        public decimal TotalPostage { get; set; }
        public int? PaymentMethodId { get; set; }
        public int? CurrencyId { get; set; }
        //public int TotalItemCount { get; set; }
        public List<MailItemDto> MailItems { get; set; }
        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address PickupAddress { get; set; }
        public string PickupNote { get; set; }
        public string DropOffNote { get; set; }
        // Ramzy: need to be checked
        public string UserId { get; set; }
        ///////////////////////////////////////////////////////////////
        /// Business Order
        public int? BusinessCustomerId { get; set; }
        public int? BusinessCustomerBranchId { set; get; }

        //  public int? ContactId { get; set; }
        //   public int? ContractId { get; set; }
        public int? OrderById { get; set; }

        public int? MailItemsTypeId { get; set; }
        public decimal PostagePerPiece { get; set; }
        public int CollectionFormSerialNo { get; set; }
        public List<AttachmentFile> AttachedFiles { get; set; }
        public int? BusinessOrderId { get; set; }
        public string BusinessCustomerName { set; get; }
        public int BusinessTypeId { set; get; }
        //    public string BusinessCID { set; get; }
        // public string BusinessCode { set; get; }
        //  public int? PickupLocationId { get; set; }
        public int? PickupRequestNotificationId { get; set; }
        public int? DepartmentId { get; set; }
        public int? TotalItemCount { get; set; }
        public int? ItemTypeId { get; set; }

        public long? AirWayBillNumber { get; set; }
        public int? CourierId { get; set; }
        public string CourierTrackingNumber { get; set; }
        //  public bool isInternational { get; set; }
        public IFormFile OrderFile { set; get; }
        public string ImageURL { get; set; }
        public bool IsMyAddress { get; set; }

        #endregion
        public class UpdateBusinessOrderForExpressCommandHandler : IRequestHandler<UpdateBusinessOrderForExpressCommand, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly ILogger<UnitOfWork> _logger;

            public UpdateBusinessOrderForExpressCommandHandler(ApplicationDbContext context,
                ILogger<UnitOfWork> logger)
            {
                _context = context;
                _logger = logger;
            }
            public async Task<int> Handle(UpdateBusinessOrderForExpressCommand command, CancellationToken cancellationToken)
            {
                var order = _context.Orders.Where(a => a.Id == command.Id)
                    .IgnoreQueryFilters()
                    .Include(a => a.OrderInternationalDelivery)
                    .Include(a => a.PickupAddress)
                    .Include(a => a.BusinessOrder)
                    .ThenInclude(c => c.BusinessCustomerBranch)
                    .Include(a => a.BusinessOrder)
                    .ThenInclude(c => c.BusinessType)
                    .Include(a => a.MailItems)
                    .ThenInclude(c => c.ConsigneeInfo)
                    .Include(c => c.MailItems)
                    .ThenInclude(c => c.DropOffAddress)
                    // .Include(c=>c.MailItems.Where(c=>c.OrderId==command.Id).FirstOrDefault().co)
                    .FirstOrDefault();

                if (order == null)
                {
                    return default;
                }
                else
                {

                    order.OrdersStatusLog = new List<OrdersStatusLog>()
                {
                    new OrdersStatusLog(){
                    CreatedBy = command.UserId,
                    CreationTime = DateTime.UtcNow,
                    IsDeleted = false,
                    OrderStatusId = command.StatusId
                }};
                    order.IsMyAddress = command.IsMyAddress;
                    order.TotalItemCount = command.TotalItemCount;
                    //order.OrderCode = command.OrderCode;
                    //order.StatusId = command.StatusId;
                    //order.OrderTypeId = command.OrderTypeId;
                    // order.ServiceSectorId = command.ServiceSectorId;
                    order.ServiceTypeId = command.ServiceTypeId;
                    // order.IssueAt = command.IssueAt;
                    order.ReadyAt = command.ReadyAt;
                    order.StartedAt = command.StartedAt;
                    order.DeliveryBefore = command.DeliveryBefore;
                    order.FulfilledAt = command.FulfilledAt;
                    order.TotalWeight = command.TotalWeight;
                    order.WeightUOMId = command.WeightUOMId;
                    order.TotalPostage = command.TotalPostage;
                    order.PaymentMethodId = command.PaymentMethodId;
                    order.CurrencyId = command.CurrencyId;
                    //  order.PickupAddress = command.PickupAddress;
                    if (command.PickUpAddress != null)
                    {
                        if (order.PickupAddress == null)
                        {

                            order.PickupAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                            {
                                Area = command.PickUpAddress.Area,
                                Governorate = command.PickUpAddress.Governorate,
                                Street = command.PickUpAddress.Street,
                                Block = command.PickUpAddress.Block,
                                Building = command.PickUpAddress.Building,
                                Flat = command.PickUpAddress.Flat,
                                Floor = command.PickUpAddress.Floor,

                            };
                        }
                        else
                        {
                            order.PickupAddress.Area = command.PickUpAddress.Area;
                            order.PickupAddress.Governorate = command.PickUpAddress.Governorate;
                            order.PickupAddress.Street = command.PickUpAddress.Street;
                            order.PickupAddress.Block = command.PickUpAddress.Block;
                            order.PickupAddress.Building = command.PickUpAddress.Building;
                            order.PickupAddress.Flat = command.PickUpAddress.Flat;
                            order.PickupAddress.Floor = command.PickUpAddress.Floor;
                        }
                    }

                    order.PickupNote = command.PickupNote;
                    order.DropOffNote = command.DropOffNote;
                    order.CreatedBy = command.UserId;
                    order.CreationTime = DateTime.UtcNow;
                    order.IsDeleted = false;
                    order.IsDraft = false;
                    order.BusinessOrderId = command.BusinessOrderId;
                    //  order.PickupLocationId = command.PickupLocationId;
                    order.PickupRequestNotificationId = command.PickupRequestNotificationId;
                    order.DepartmentId = command.DepartmentId;

                    if (order.BusinessOrder == null)
                    {
                        order.BusinessOrder = new BusinessOrder()
                        {
                            AttachedFiles = !string.IsNullOrEmpty(command.ImageURL) ? new List<AttachmentFile>() { new AttachmentFile {
                             Path=command.ImageURL
                           } } : new List<AttachmentFile>(),
                            OrderById = command.OrderById,
                            //  ContractId = command.ContractId,
                            MailItemsTypeId = command.MailItemsTypeId,
                            BusinessTypeId = command.BusinessTypeId,

                            BusinessCustomerBranchId = command.BusinessCustomerBranchId,
                            BusinessCustomerId = command.BusinessCustomerId,
                        };
                    }
                    else
                    {
                        //    order.BusinessOrder.AttachedFiles = !string.IsNullOrEmpty(command.ImageURL) ? new List<AttachmentFile>() { new AttachmentFile {
                        //    Path=command.ImageURL
                        //} } : new List<AttachmentFile>();
                        order.BusinessOrder.OrderById = command.OrderById;
                        // order.BusinessOrder.ContractId = command.ContractId;
                        order.BusinessOrder.MailItemsTypeId = command.MailItemsTypeId;
                        order.BusinessOrder.BusinessCustomerBranchId = command.BusinessCustomerBranchId;
                        order.BusinessOrder.BusinessCustomerId = command.BusinessCustomerId;


                        //if (order.BusinessOrder.BusinessCustomer == null)
                        //{
                        //    order.BusinessOrder.BusinessCustomer = new BusinessCustomer
                        //    {
                        //        HQBranchId = command.HQBranchId,
                        //        BusinessName = command.BusinessCustomerName,
                        //       // BusinessCID = command.BusinessCID,
                        //        BusinessTypeId = command.BusinessTypeId
                        //    };
                        //}
                        //else
                        //{
                        //   // order.BusinessOrder.BusinessCustomer.BusinessCID = command.BusinessCID;
                        //    order.BusinessOrder.BusinessCustomer.BusinessTypeId = command.BusinessTypeId;
                        //}


                        //order.BusinessOrder.HQBranchId = command.HQBranchId;
                        order.BusinessOrder.BusinessTypeId = command.BusinessTypeId;

                    }
                    if (command.AirWayBillNumber != null && command.CourierId != null &&
                        command.CourierTrackingNumber != null)
                    {
                        if (order.OrderInternationalDelivery == null)
                            order.OrderInternationalDelivery = new OrderInternationalDelivery()
                            {
                                AirWayBillNumber = command.AirWayBillNumber,
                                CourierId = command.CourierId,
                                CourierTrackingNumber = command.CourierTrackingNumber,
                            };
                        else
                        {
                            order.OrderInternationalDelivery.AirWayBillNumber = command.AirWayBillNumber;
                            order.OrderInternationalDelivery.CourierId = command.CourierId;
                            order.OrderInternationalDelivery.CourierTrackingNumber = command.CourierTrackingNumber;
                        }
                    }
                    int mailItemId = await _context.MailItems.Select(x => x.Id).OrderBy(x => x).LastOrDefaultAsync();
                    int counter = mailItemId;



                    // Ramzy: need to check if the mailitems is exist >> to be updated or not exist >> to be added and delete the extra.
                    if (command.MailItems != null && command.MailItems.Count > 0)
                    {

                        if (order.MailItems == null && order.MailItems.Count == 0)
                        {
                            order.MailItems = new List<MailItem>();
                            foreach (var m in command.MailItems)
                            {
                                var mailitem = new MailItem();
                                mailitem.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                             {
                             // CreatedBy = command.UserId,
                             CreationTime = DateTime.UtcNow,
                             IsDeleted = false,
                             WorkOrderId = mailitem.WorkorderId,
                             MailItemStatusId = (int)MailItemStatuses.New
                           }
                         };
                                mailitem.CountryId = m.CountryId;
                                mailitem.TotalPrice = m.TotalPrice;
                                mailitem.FromSerial = !string.IsNullOrEmpty(m.FromSerial) ? m.FromSerial : "";
                                mailitem.PickupDate = m.PickupDate;
                                mailitem.CaseNo = m.CaseNo;
                                mailitem.Weight = m.Weight != null ? m.Weight : null;
                                mailitem.WeightUOMId = m.WeightUOMId;

                                if (mailitem.ConsigneeInfoId == null)
                                {
                                    mailitem.ConsigneeInfo = new Contact
                                    {
                                        FirstName = m.ConsigneeInfo.FirstName,
                                        Mobile = m.ConsigneeInfo.Mobile,
                                    };
                                }
                                else
                                {
                                    // mailitem.ConsigneeInfoId = m.ConsigneeInfoId;
                                }
                                mailitem.ItemTypeId = command.ItemTypeId != null ? command.ItemTypeId : null;
                                mailitem.DeliveryBefore = m.DeliveryBefore != null ? m.DeliveryBefore : (DateTime?)null;
                                mailitem.IsMatchConsigneeID = m.IsMatchConsigneeID;
                                mailitem.MailItemBarCode = m.MailItemBarCode != null ? m.MailItemBarCode : null;

                                if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                                {
                                    counter += 1;
                                    mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                                }

                                mailitem.ReservedSpecial = m.ReservedSpecial != null ? m.ReservedSpecial : null;
                                mailitem.Notes = m.Notes != null ? m.Notes : null;

                                mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                                {
                                    Area = m.DropOffAddress.Area,
                                    Governorate = m.DropOffAddress.Governorate,
                                    Street = m.DropOffAddress.Street,
                                    Block = m.DropOffAddress.Block,
                                    Building = m.DropOffAddress.Building,
                                    Flat = m.DropOffAddress.Flat,
                                    Floor = m.DropOffAddress.Floor,
                                };

                                // need to add mailitem status logs here if updated
                                // 

                                order.MailItems.Add(mailitem);
                            }
                        }
                        else
                        {
                            //var toBeDeletedMailItems = new List<MailItem>();

                            foreach (var cm in command.MailItems)
                            {

                                if (order.MailItems.Where(m => m.Id == cm.Id && cm.Id > 0).Any())
                                {
                                    // update mailItem
                                    var mailitem = order.MailItems.Where(m => m.MailItemBarCode == cm.MailItemBarCode).FirstOrDefault();

                                    mailitem.CountryId = cm.CountryId;
                                    mailitem.TotalPrice = cm.TotalPrice;
                                    mailitem.FromSerial = !string.IsNullOrEmpty(cm.FromSerial) ? cm.FromSerial : "";
                                    mailitem.PickupDate = cm.PickupDate;
                                    mailitem.CaseNo = cm.CaseNo;
                                    mailitem.Weight = cm.Weight != null ? cm.Weight : null;
                                    mailitem.WeightUOMId = cm.WeightUOMId;

                                    if (mailitem.ConsigneeInfoId == null)
                                    {
                                        if (cm.ConsigneeInfo != null)
                                            mailitem.ConsigneeInfo = new Contact()
                                            {
                                                FirstName = cm.ConsigneeInfo.FirstName,
                                                Mobile = cm.ConsigneeInfo.Mobile
                                            };
                                    }
                                    else
                                    {
                                        mailitem.ConsigneeInfoId = cm.ConsigneeInfoId;
                                    }
                                    mailitem.ItemTypeId = command.ItemTypeId != null ? command.ItemTypeId : null;
                                    mailitem.DeliveryBefore = cm.DeliveryBefore != null ? cm.DeliveryBefore : (DateTime?)null;
                                    mailitem.IsMatchConsigneeID = cm.IsMatchConsigneeID;
                                    mailitem.MailItemBarCode = cm.MailItemBarCode != null ? cm.MailItemBarCode : null;
                                    if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                                    {
                                        counter += 1;
                                        mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                                    }


                                    mailitem.ReservedSpecial = cm.ReservedSpecial != null ? cm.ReservedSpecial : null;
                                    mailitem.Notes = cm.Notes != null ? cm.Notes : null;

                                    mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                                    {
                                        Area = cm.DropOffAddress.Area,
                                        Governorate = cm.DropOffAddress.Governorate,
                                        Street = cm.DropOffAddress.Street,
                                        Block = cm.DropOffAddress.Block,
                                        Building = cm.DropOffAddress.Building,
                                        Flat = cm.DropOffAddress.Flat,
                                        Floor = cm.DropOffAddress.Floor,
                                    };

                                    // need to add mailitem status logs here if updated
                                    // 

                                }
                                else
                                {
                                    // add mail item
                                    var mailitem = new MailItem();
                                    mailitem.CountryId = cm.CountryId;
                                    mailitem.TotalPrice = cm.TotalPrice;
                                    mailitem.FromSerial = !string.IsNullOrEmpty(cm.FromSerial) ? cm.FromSerial : "";
                                    mailitem.PickupDate = cm.PickupDate;
                                    mailitem.CaseNo = cm.CaseNo;
                                    mailitem.Weight = cm.Weight != null ? cm.Weight : null;
                                    mailitem.WeightUOMId = cm.WeightUOMId;

                                    if (mailitem.ConsigneeInfo == null)
                                    {
                                        mailitem.ConsigneeInfo = new Contact
                                        {
                                            FirstName = cm.ConsigneeInfo.FirstName,
                                            Mobile = cm.ConsigneeInfo.Mobile,
                                        };
                                    }
                                    else
                                    {
                                        /// mailitem.ConsigneeInfoId = cm.ConsigneeInfoId;
                                    }
                                    mailitem.ItemTypeId = command.ItemTypeId != null ? command.ItemTypeId : null;
                                    mailitem.DeliveryBefore = cm.DeliveryBefore != null ? cm.DeliveryBefore : (DateTime?)null;
                                    mailitem.IsMatchConsigneeID = cm.IsMatchConsigneeID;
                                    mailitem.MailItemBarCode = cm.MailItemBarCode != null ? cm.MailItemBarCode : null;
                                    if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                                    {
                                        counter += 1;
                                        mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                                    }


                                    mailitem.ReservedSpecial = cm.ReservedSpecial != null ? cm.ReservedSpecial : null;
                                    mailitem.Notes = cm.Notes != null ? cm.Notes : null;


                                    mailitem.DropOffAddress = new Amanah.Posthub.Service.Domain.Addresses.Entities.Address
                                    {
                                        Area = cm.DropOffAddress.Area,
                                        Governorate = cm.DropOffAddress.Governorate,
                                        Street = cm.DropOffAddress.Street,
                                        Block = cm.DropOffAddress.Block,
                                        Building = cm.DropOffAddress.Building,
                                        Flat = cm.DropOffAddress.Flat,
                                        Floor = cm.DropOffAddress.Floor,
                                    };

                                    // need to add mailitem status logs here if updated
                                    // 



                                    order.MailItems.Add(mailitem);
                                }
                            }

                            var commandMailItems = command.MailItems.Select(cm => cm.Id);
                            var toBeDeletedMailItems = order.MailItems.Where(m => !commandMailItems.Contains(m.Id));
                            foreach (var item in toBeDeletedMailItems)
                            {
                                item.IsDeleted = true;

                                _context.MailItems.Update(item);
                                // _context.SaveChanges();

                            }

                        }
                    }
                    else
                    {
                        order.MailItems = new List<MailItem>();
                    }
                    _context.Orders.Update(order);
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "Failed to run transaction");
                        throw new DomainException(Keys.Validation.GeneralError);
                    }
                    return order.Id;
                }
            }


            private string GenerateItemCodeAsync(int ItemId)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(ItemId):00000000}");
                return sb.ToString();
            }

        }
    }
}
