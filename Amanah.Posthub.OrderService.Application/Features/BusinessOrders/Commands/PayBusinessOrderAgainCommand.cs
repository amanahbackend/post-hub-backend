﻿using Amanah.PaymentIntegrations.DTOs;
using Amanah.PaymentIntegrations.DTOs.Enums;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.EntityFramework.DbContext;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{
    public class PayBusinessOrderAgainCommand : IRequest<CreateBusinessOrderWithPaymentOutPut>
    {
        #region props
        public int Id { get; set; }
        public string PaymentGateway { get; set; }
        public decimal OrderTotalPrice { get; set; }

        #endregion
        public class PayBusinessOrderAgainCommandHandler : IRequestHandler<PayBusinessOrderAgainCommand, CreateBusinessOrderWithPaymentOutPut>
        {
            private readonly ApplicationDbContext _context;
            private readonly ILogger<UnitOfWork> _logger;
            private readonly IPaymentService _paymentService;
            private readonly IConfiguration _configuration;
            public PayBusinessOrderAgainCommandHandler(ApplicationDbContext context, IPaymentService paymentService ,
                IConfiguration configuration ,
                ILogger<UnitOfWork> logger)
            {
                _context = context;
                _logger = logger;
                _paymentService = paymentService;
                _configuration = configuration;
            }
            public async Task<CreateBusinessOrderWithPaymentOutPut> Handle(PayBusinessOrderAgainCommand command, CancellationToken cancellationToken)
            {
                CreateBusinessOrderWithPaymentOutPut payResponse = null;
                var order = _context.Orders.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();
                if (order == null)
                {
                    return default;
                }
                else
                {
                    _context.SaveChanges();
                    try
                    {
                        if (command.PaymentGateway != "cash")
                        {
                            payResponse = await SaveOrderPayment(order.Id, command.OrderTotalPrice, command.PaymentGateway);
                            return payResponse;
                        }
                        else
                        {
                            var response = await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
                            {
                                ReferenceId = order.Id.ToString(),
                                TotalPrice = (double)command.OrderTotalPrice,
                                PaymentGateway = "cash"
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, "Failed to run transaction");
                        throw new DomainException(Keys.Validation.GeneralError);
                    }
                    return payResponse = new CreateBusinessOrderWithPaymentOutPut { BusinessOrderId = order.Id, PaymentURL = "" };
                }
            }
            private async Task<CreateBusinessOrderWithPaymentOutPut> SaveOrderPayment(int orderId, decimal orderTotalPrice , string payMethod)
            {
                CreateBusinessOrderWithPaymentOutPut createBusinessOrderOutPut = new CreateBusinessOrderWithPaymentOutPut();
                var successUrl = _configuration.GetSection("UpPaymentsConfiguration")["SuccessURL"];
                var failUrl = _configuration.GetSection("UpPaymentsConfiguration")["FailURL"];
                var NotifyUrl = _configuration.GetSection("UpPaymentsConfiguration")["NotifyURL"];
                var paymentMode = _configuration.GetSection("UpPaymentsConfiguration")["PaymentMode"];
                var response = await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
                {
                    ReferenceId = orderId.ToString(), 
                    TotalPrice = (double)orderTotalPrice,
                    PaymentGateway = payMethod == "knet" ? "knet" : "cc",
                    CurrencyCode = "KWD",
                    PaymentMode = paymentMode == "1" ? PaymentMode.TestingMode : PaymentMode.ProductionMode,
                    Whitelabled = WhiteLabel.On, // without direct url ... the customer will need to choose his prefered way  
                    UpPaymentConfiguration = new UpPaymentConfigurationDTO
                    {
                        SuccessURL = successUrl,
                        //"https://amanah-paymentintegrations-test.conveyor.cloud/api/Payments/Success",
                        FailURL = failUrl,
                        NotifyURL = NotifyUrl,
                    },
                    Product = new RequestPaymentProductRequestDTO
                    {
                        Title = "OrderPackagesPayment",
                        Names = new string[1] { orderId.ToString() },
                        Prices = new double[1] { (double)orderTotalPrice },
                        Quantities = new string[1] { "1" },
                    }

                });
                createBusinessOrderOutPut.BusinessOrderId = orderId;
                createBusinessOrderOutPut.PaymentURL = response.Data.PaymentURL;
                return createBusinessOrderOutPut;

            }
        }
    }
}