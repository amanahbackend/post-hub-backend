﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.Enums;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{

    public class ReceiveOrderCommand : IRequest<int>
    {

        public int BranchId { set; get; }
        public string Code { set; get; }

    }
    public class ReceiveOrdeHandler : IRequestHandler<ReceiveOrderCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;
        private readonly IFirebasePushNotificationService _pushNotificationService;
        private readonly ILocalizer _localizer;

        public ReceiveOrdeHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser,
            IFirebasePushNotificationService pushNotificationService,
            ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;
            _pushNotificationService = pushNotificationService;
            _localizer = localizer;

        }
        public async Task<int> Handle(ReceiveOrderCommand query, CancellationToken cancellationToken)
        {

            var order = await _context
                 .Orders
                 .Include(c => c.MailItems)
                 .ThenInclude(c => c.ConsigneeInfo)
                 .Include(c => c.MailItems)
                 .ThenInclude(c => c.Workorder)
                 .ThenInclude(c => c.Driver)
                 .IgnoreQueryFilters()
                 .Where(x => !x.IsDeleted && !x.IsDraft && x.ServiceSectorId == (int)ServiceSectors.InternalPost
                  && (x.MailItems.Any() && x.MailItems.FirstOrDefault().Workorder.Code == query.Code)
                 // && x.MailItems.FirstOrDefault().ConsigneeInfo.BranchId==query.BranchId
                  ).FirstOrDefaultAsync();

            if (order != null && order.MailItems.Any())
            {
                if (order.MailItems.FirstOrDefault().Workorder.WorkOrderStatusId != (int?)WorkorderStatuses.ReceivedByPostRoom
                    && order.MailItems.FirstOrDefault().Workorder.WorkOrderStatusId != (int?)WorkorderStatuses.Closed)
                {

                    order.MailItems.FirstOrDefault().ConsigneeInfo.BranchId = query.BranchId;
                    order.MailItems.FirstOrDefault().Workorder.WorkOrderStatusId = (int?)WorkorderStatuses.ReceivedByPostRoom;

                    var driverUserId = order.MailItems.FirstOrDefault().Workorder.Driver.UserId;

                    _context.Orders.Update(order);
                    _context.SaveChanges();

                    await _pushNotificationService.SendAsync(driverUserId, "Post-Hub", "Your WorkOrder Received By PostRoom.", new { NotificationType = NotificationTypeEnum.NewWorkOrder });

                    return order.Id;
                }
                else
                {
                    throw new DomainException(_localizer[Keys.Messages.WorkorderRecieved]);

                }

            }
            else
            {
                throw new DomainException(_localizer[Keys.Messages.WorkorderDoNotHaveItems]);

            }

            return default;
        }
    }
}
