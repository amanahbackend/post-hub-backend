﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{
    public class TrackingNumberDto : IRequest<int>
    {
        public int OrderId { get; set; }
        public string TrackingNumber { get; set; }

        public class TrackingNumberHandler : IRequestHandler<TrackingNumberDto, int>
        {
            private readonly ApplicationDbContext _context;

            public TrackingNumberHandler(ApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(TrackingNumberDto command, CancellationToken cancellationToken)
            {
                var order = await _context.Orders.IgnoreQueryFilters().Where(o => o.Id == command.OrderId).Include(o=>o.OrderInternationalDelivery).FirstOrDefaultAsync();

                if (order == null) return default;

                if (order.OrderInternationalDelivery == null)
                    order.OrderInternationalDelivery = new OrderInternationalDelivery { CourierTrackingNumber = command.TrackingNumber };
                else
                    order.OrderInternationalDelivery.CourierTrackingNumber = command.TrackingNumber;
                
                await _context.SaveChangesAsync();
                return order.Id;
            }
        }
    }
}
