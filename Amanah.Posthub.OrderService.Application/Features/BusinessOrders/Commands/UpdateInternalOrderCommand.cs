﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{
    public class UpdateInternalOrderCommand : IRequest<Result>
    {
        public int Id { get; set; }
        public MailItemForInternalDto MailItem { get; set; }
        public string SenderName { get; set; }
        public int? SenderBranchId { get; set; }
        public string SenderEmail { get; set; }
        public string SenderFloorNo { get; set; }
        public bool IsInternal { get; set; }
        //public int? SecurityCaseNumber { get; set; }
        //public int? BranchId { get; set; }
        //public int? DriverId { get; set; }

        public class UpdateInternalOrderCommandHandler : IRequestHandler<UpdateInternalOrderCommand, Result>
        {

            private readonly ApplicationDbContext _context;
            private readonly ICurrentUser _currentUser;
            private readonly IUnitOfWork _unityOfWork;
            private readonly IFirebasePushNotificationService _pushNotificationService;


            public UpdateInternalOrderCommandHandler(ApplicationDbContext context,
                ICurrentUser currentUser,
                IUnitOfWork unityOfWork,
                IFirebasePushNotificationService pushNotificationService)
            {
                _context = context;
                _currentUser = currentUser;
                _unityOfWork = unityOfWork;
                _pushNotificationService = pushNotificationService;
            }

            public async Task<Result> Handle(UpdateInternalOrderCommand command, CancellationToken cancellationToken)
            {
                var result = new Result();
                int? driverId = null;

                driverId = await _context.DriverFloors
                   .Where(d => d.BranchId == command.SenderBranchId && d.Floor == command.SenderFloorNo)
                   .IgnoreQueryFilters()
                   .Select(d => d.DriverId)
                   .FirstOrDefaultAsync();

                if (!driverId.HasValue || driverId.Value <= 0)
                {
                    result.Error = "No delegate assign to floor no. " + command.SenderFloorNo;
                    result.Value = false;
                }
                else
                {
                    var order = await _context.Orders
                    .Include(o => o.MailItems)
                    .ThenInclude(m => m.ConsigneeInfo)
                    .IgnoreQueryFilters()
                    .Where(o => !o.IsDeleted && o.Id == command.Id)
                    .FirstOrDefaultAsync();

                    order.SenderName = command.SenderName;
                    order.SenderBranchId = command.SenderBranchId;
                    order.SenderEmail = command.SenderEmail;
                    order.SenderFloorNo = command.SenderFloorNo;
                    order.IsInternal = command.IsInternal;

                    var mailitem = order.MailItems.Where(m => m.Id == command.MailItem.Id).FirstOrDefault();
                    if (mailitem != null)
                    {
                        mailitem.RemarkId = command.MailItem.RemarkId;
                        mailitem.Description = command.MailItem.Description;

                        mailitem.ConsigneeInfo.FirstName = command.MailItem.ReciverName;
                        mailitem.ConsigneeInfo.Email = command.MailItem.ReciverEmail;
                        mailitem.ConsigneeInfo.BranchName = command.MailItem.ReciverBranchName;
                        mailitem.ConsigneeInfo.DepartmentName = command.MailItem.DepartmentName;
                        mailitem.ConsigneeInfo.FloorNo = command.MailItem.ReciverfloorNo;

                        _context.Orders.Update(order);

                        result.TotalCount = await _context.SaveChangesAsync();
                        result.Value = true;

                        return result;
                    }
                }
                return result;
            }

        }


    }
}
