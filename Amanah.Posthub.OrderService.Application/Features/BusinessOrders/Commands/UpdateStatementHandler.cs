﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands
{

    public class UpdateStatementHandler : IRequestHandler<UpdateStatementInputDTO, PrintPostBagStatementDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;


        public UpdateStatementHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<WorkOrder> workOrderRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
        }
        public async Task<PrintPostBagStatementDTO> Handle(UpdateStatementInputDTO command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            // List<MailItem> mailItems = new List<MailItem>();

            PrintPostBagStatementDTO printPostBagStatement = new PrintPostBagStatementDTO();

            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                workOrder = _context.Workorders.Where(c => c.Id == command.WorkOrderId)
                .Include(c => c.MailItems)
                .ThenInclude(c => c.ConsigneeInfo)
                .FirstOrDefault();

                if (workOrder != null)
                {
                    workOrder.WorkOrderImageURL = command.WorkOrderImageURL;
                    _workOrderRepository.Update(workOrder);

                }
                await _unityOfWork.SaveChangesAsync();


            });
            if (result)
            {
                printPostBagStatement = _mapper.Map<PrintPostBagStatementDTO>(workOrder);
                if (workOrder.MailItems.Any())
                {
                    var mailItemsDTO = _mapper.Map<List<MailItemResponseDTO>>(workOrder.MailItems);
                    printPostBagStatement.ItemsList = mailItemsDTO;
                }

                return printPostBagStatement;
            }
            else
            {
                return default;
            }

        }
    }
}
