﻿using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs
{
    public class InternalOrderQueryDto
    {
        public string SenderName { get; set; }
        public int? SenderBranchId { get; set; }
        public string SenderBranchName { get; set; }
        public string SenderEmail { get; set; }
        public string SenderFloorNo { get; set; }
        public bool IsInternal { get; set; }
        public IEnumerable<MailItemForInternalDto> MailItem { get; set; }
    }
}