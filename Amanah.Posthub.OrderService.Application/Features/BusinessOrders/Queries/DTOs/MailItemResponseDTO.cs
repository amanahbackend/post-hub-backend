﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs
{
    public class MailItemResponseDTO
    {
        public int Id { get; set; }
        public int? TotalItemCount { get; set; }
        public string MailItemBarCode { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string DeliveryOn { get; set; }
        public DateTime? DeliveryAt { get; set; }
        //public int SecurityCaseNumber { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
      

    }
}
