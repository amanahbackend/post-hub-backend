﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs
{
    public class PaymentDetailsForMobileDto
    {
        public bool? Result { get; set; } 
        public string AuthCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public double TotalPrice { get; set; }
        public string OrderId { get; set; }
        public string TransactionId { get; set; }
        public bool? IsCash { get; set; } 
    }


}
