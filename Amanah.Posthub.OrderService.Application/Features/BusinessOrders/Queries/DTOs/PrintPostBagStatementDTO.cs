﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs
{
    public class PrintPostBagStatementDTO
    {
        public int Id { get; set; }
        public int? BranchId { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string Code { get; set; }
        public int SecurityCaseNumber { get; set; }
        public List<MailItemResponseDTO> ItemsList { get; set; }
        public int ItemsNo { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string RecieveOn { get; set; }
        public DateTime? RecieveAt { get; set; }
        public string WorkOrderImageURL { get; set; }

    }
}
