﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs
{
    public class ReceiveOrderListResponseDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string SenderName { get; set; }
        public string SenderBranch { get; set; }
        public string ReciverBranch { get; set; }
        public int SenderBranchId { get; set; }
        public string Description { get; set; }
        public string StatementCode { get; set; }
       
    }
}
