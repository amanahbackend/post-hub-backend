﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs
{
    public class StatementListResponseDTO
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public int? SecurityCaseNumber { get; set; }
        public int? WorkOrderStatusId { get; set; }

        public string Code { get; set; }
        public string BranchName { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public int NumberOfItems { get; set; }
        public string Status { get; set; }
        
    }
}
