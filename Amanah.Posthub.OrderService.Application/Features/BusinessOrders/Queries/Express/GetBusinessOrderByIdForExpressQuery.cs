﻿using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.Express
{
    public class GetBusinessOrderByIdForExpressQuery : IRequest<BusinessOrderDto>
    {
        public int Id { get; set; }
        public class GetBusinessOrderByIdForExpressQueryHandler : IRequestHandler<GetBusinessOrderByIdForExpressQuery, BusinessOrderDto>
        {
            private readonly ApplicationDbContext _context;
            private readonly IPaymentService _paymentService;
            public GetBusinessOrderByIdForExpressQueryHandler(ApplicationDbContext context, IPaymentService paymentService)
            {
                _context = context;
                _paymentService = paymentService;
            }
            public async Task<BusinessOrderDto> Handle(GetBusinessOrderByIdForExpressQuery query, CancellationToken cancellationToken)
            {
                var order = new BusinessOrderDto();
               
                var orderEF = _context
                    .Orders
                    .Include(x => x.BusinessOrder).ThenInclude(x => x.AttachedFiles).Include(x => x.BusinessOrder)
                    .ThenInclude(x => x.BusinessCustomerBranch)
                    .Include(x => x.BusinessOrder)
                    .ThenInclude(x => x.BusinessCustomer)
                     .Include(x => x.BusinessOrder)
                    .ThenInclude(x => x.BusinessType)
                     .Include(x => x.BusinessOrder)
                    .ThenInclude(x => x.Contract)
                    .ThenInclude(x => x.ContractStatus)
                    .Include(c => c.OrderInternationalDelivery)
                    .Include(c => c.PickupAddress)
                    .IgnoreQueryFilters()
                    .Where(a => a.Id == query.Id);

                if (orderEF.FirstOrDefault(a=>a.ServiceSectorId == (int)ServiceSectors.InternationalDelivery) != null) {

                    var orderPayment = await _paymentService.GetPaymentTransactionAsync(query.Id.ToString());
                    order = orderEF.Select(c => new BusinessOrderDto
                    {
                        Id = c.Id,
                        ContactId = c.BusinessOrder.ContactId,
                        ContractId = c.BusinessOrder != null ? c.BusinessOrder.ContractId : null,
                        CustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,
                        BusinessCustomerName = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomer != null ?
                             c.BusinessOrder.BusinessCustomer.BusinessName : "" : "",
                        DeliveryBefore = c.DeliveryBefore,
                        DirectOrderId = c.DirectOrderId,
                        FulfilledAt = c.FulfilledAt,
                        IssueAt = c.IssueAt,
                        OrderCode = c.OrderCode,
                        OrderStatus = c.Status.Name,
                        OrderType = c.OrderType.Name,
                        OrderTypeId = c.OrderTypeId,
                        ReadyAt = c.ReadyAt,
                        ServiceType = c.ServiceType.Name,
                        ServiceTypeId = c.ServiceTypeId,
                        StatusId = c.StatusId,
                        StartedAt = c.StartedAt,

                        DepartmentId = c.DepartmentId,
                        PickupLocationId = c.PickupLocationId,
                        PickupRequestNotificationId = c.PickupRequestNotificationId,

                        BusinessTypeId = c.BusinessOrder != null ? c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessTypeId : null : null,

                        //BusinessCode = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                        //c.BusinessOrder.BusinessCustomer.BusinessCode:"":"",

                        //BusinessCID = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                        //c.BusinessOrder.BusinessCustomer.BusinessCID:"":"",

                        ImgsUrl = c.BusinessOrder.AttachedFiles != null ? c.BusinessOrder.AttachedFiles.Select(a => $"UploadFile/OrderFiles/{ a.Path }").ToList() : null,


                        BusinessCustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,

                        BusinessTypeName = c.BusinessOrder != null ? c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessType != null ?
                             c.BusinessOrder.BusinessType.Name_en : "" : "" : "",
                        DepartmentName = c.Department != null ? c.Department.Name_en : "",

                        //HQBranchName = c.BusinessOrder != null ?
                        //c.BusinessOrder.BusinessCustomer != null ?
                        //c.BusinessOrder.BusinessCustomer.HQBranch != null ?
                        //c.BusinessOrder.BusinessCustomer.HQBranch.Name : "" : "" : "",

                        ContractCode = c.BusinessOrder != null ? c.BusinessOrder.Contract != null ?
                             c.BusinessOrder.Contract.Code : "" : "",

                        ContractStatus = c.BusinessOrder != null ?
                             c.BusinessOrder.Contract != null ?
                             c.BusinessOrder.Contract.ContractStatus != null ? c.BusinessOrder.Contract.ContractStatus.Name : "" : "" : "",

                        BusinessCustomerBranchId = c.BusinessOrder != null ?
                              c.BusinessOrder.BusinessCustomerBranchId : null,

                        BusinessCustomerBranchName = c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessCustomerBranch != null ?
                             c.BusinessOrder.BusinessCustomerBranch.Name : "" : "",
                        BusinessOrderId = c.BusinessOrderId,
                        ServiceSectorId = c.ServiceSectorId,
                        AirWayBillNumber = c.OrderInternationalDelivery.AirWayBillNumber,
                        CourierId = c.OrderInternationalDelivery.CourierId,
                        CourierTrackingNumber = c.OrderInternationalDelivery.CourierTrackingNumber,
                        OrderById = c.BusinessOrder.OrderById,
                        PickUpAddress = new AddressDto
                        {
                            Area = c.PickupAddress.Area,
                            Governorate = c.PickupAddress.Governorate,
                            Street = c.PickupAddress.Street,
                            Block = c.PickupAddress.Block,
                            Building = c.PickupAddress.Building,
                            Flat = c.PickupAddress.Flat,
                            Floor = c.PickupAddress.Floor,
                        },
                        IsMyAddress = c.IsMyAddress,
                        MailItem = c.MailItems.Count > 0 ? c.MailItems.Select(a => new MailItemDto
                        {
                            MailItemBarCode = a.MailItemBarCode,
                            ConsigneeInfo = new ContactDto { FirstName = a.ConsigneeInfo.FirstName, Mobile = a.ConsigneeInfo.Mobile },
                            Quantity = a.Quantity,
                            TotalPrice = a.TotalPrice,
                        }).ToList() : null,
                        PaymentGateway = orderPayment.Data.TransactionResponseDetails != null ? orderPayment.Data.TransactionResponseDetails.PaymentType : string.Empty,
                        PaymentMethodId = orderPayment.Data.TransactionResponseDetails != null && orderPayment.Data.TransactionResponseDetails.PaymentType == "knet" ? 1 : 2,
                        TransactionId = orderPayment.Data.TransactionResponseDetails != null ? orderPayment.Data.TransactionResponseDetails.TransactionId : string.Empty,
                        AuthCode = orderPayment.Data.TransactionResponseDetails != null ? orderPayment.Data.TransactionResponseDetails.Auth : string.Empty,
                        //    OrderAttachments = c.BusinessOrder.AttachedFiles.Count > 0? c.BusinessOrder.AttachedFiles.Select(x => new OrderAttachmentDto
                        //    { 
                        //        OrderAttachmentPath=string.IsNullOrEmpty(x.Path) ? $"UploadFile/OrderFiles/" + "avtar.jpeg" : $"UploadFile/OrderFiles/" + x.Path ,
                        //        OrderAttachmentName = string.IsNullOrEmpty(x.Path) ? "avtar.jpeg" : x.Path,
                        //    }).ToList()
                        //:null
                    }).FirstOrDefault();
                }else
                {
                    order = orderEF.Select(c => new BusinessOrderDto
                    {
                        Id = c.Id,
                        ContactId = c.BusinessOrder.ContactId,
                        ContractId = c.BusinessOrder != null ? c.BusinessOrder.ContractId : null,
                        CustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,
                        BusinessCustomerName = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomer != null ?
                                               c.BusinessOrder.BusinessCustomer.BusinessName : "" : "",
                        DeliveryBefore = c.DeliveryBefore,
                        DirectOrderId = c.DirectOrderId,
                        FulfilledAt = c.FulfilledAt,
                        IssueAt = c.IssueAt,
                        OrderCode = c.OrderCode,
                        OrderStatus = c.Status.Name,
                        OrderType = c.OrderType.Name,
                        OrderTypeId = c.OrderTypeId,
                        ReadyAt = c.ReadyAt,
                        ServiceType = c.ServiceType.Name,
                        ServiceTypeId = c.ServiceTypeId,
                        StatusId = c.StatusId,
                        StartedAt = c.StartedAt,

                        DepartmentId = c.DepartmentId,
                        PickupLocationId = c.PickupLocationId,
                        PickupRequestNotificationId = c.PickupRequestNotificationId,

                        BusinessTypeId = c.BusinessOrder != null ? c.BusinessOrder != null ?
                                               c.BusinessOrder.BusinessTypeId : null : null,

                        //BusinessCode = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                        //c.BusinessOrder.BusinessCustomer.BusinessCode:"":"",

                        //BusinessCID = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                        //c.BusinessOrder.BusinessCustomer.BusinessCID:"":"",

                        ImgsUrl = c.BusinessOrder.AttachedFiles != null ? c.BusinessOrder.AttachedFiles.Select(a => $"UploadFile/OrderFiles/{a.Path}").ToList() : null,


                        BusinessCustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,

                        BusinessTypeName = c.BusinessOrder != null ? c.BusinessOrder != null ?
                                               c.BusinessOrder.BusinessType != null ?
                                               c.BusinessOrder.BusinessType.Name_en : "" : "" : "",
                        DepartmentName = c.Department != null ? c.Department.Name_en : "",

                        //HQBranchName = c.BusinessOrder != null ?
                        //c.BusinessOrder.BusinessCustomer != null ?
                        //c.BusinessOrder.BusinessCustomer.HQBranch != null ?
                        //c.BusinessOrder.BusinessCustomer.HQBranch.Name : "" : "" : "",

                        ContractCode = c.BusinessOrder != null ? c.BusinessOrder.Contract != null ?
                                               c.BusinessOrder.Contract.Code : "" : "",

                        ContractStatus = c.BusinessOrder != null ?
                                               c.BusinessOrder.Contract != null ?
                                               c.BusinessOrder.Contract.ContractStatus != null ? c.BusinessOrder.Contract.ContractStatus.Name : "" : "" : "",

                        BusinessCustomerBranchId = c.BusinessOrder != null ?
                                                c.BusinessOrder.BusinessCustomerBranchId : null,

                        BusinessCustomerBranchName = c.BusinessOrder != null ?
                                               c.BusinessOrder.BusinessCustomerBranch != null ?
                                               c.BusinessOrder.BusinessCustomerBranch.Name : "" : "",
                        BusinessOrderId = c.BusinessOrderId,
                        ServiceSectorId = c.ServiceSectorId,
                        AirWayBillNumber = c.OrderInternationalDelivery.AirWayBillNumber,
                        CourierId = c.OrderInternationalDelivery.CourierId,
                        CourierTrackingNumber = c.OrderInternationalDelivery.CourierTrackingNumber,
                        OrderById = c.BusinessOrder.OrderById,
                        PickUpAddress = new AddressDto
                        {
                            Area = c.PickupAddress.Area,
                            Governorate = c.PickupAddress.Governorate,
                            Street = c.PickupAddress.Street,
                            Block = c.PickupAddress.Block,
                            Building = c.PickupAddress.Building,
                            Flat = c.PickupAddress.Flat,
                            Floor = c.PickupAddress.Floor,
                        },
                        IsMyAddress = c.IsMyAddress,
                        MailItem = c.MailItems.Count > 0 ? c.MailItems.Select(a => new MailItemDto
                        {
                            MailItemBarCode = a.MailItemBarCode,
                            ConsigneeInfo = new ContactDto { FirstName = a.ConsigneeInfo.FirstName, Mobile = a.ConsigneeInfo.Mobile },
                            Quantity = a.Quantity,
                            TotalPrice = a.TotalPrice,
                        }).ToList() : null,
                        //    OrderAttachments = c.BusinessOrder.AttachedFiles.Count > 0? c.BusinessOrder.AttachedFiles.Select(x => new OrderAttachmentDto
                        //    { 
                        //        OrderAttachmentPath=string.IsNullOrEmpty(x.Path) ? $"UploadFile/OrderFiles/" + "avtar.jpeg" : $"UploadFile/OrderFiles/" + x.Path ,
                        //        OrderAttachmentName = string.IsNullOrEmpty(x.Path) ? "avtar.jpeg" : x.Path,
                        //    }).ToList()
                        //:null
                    }).FirstOrDefault();
                }
                if (order == null) return null;
                return order;
            }
        }
    }
}
