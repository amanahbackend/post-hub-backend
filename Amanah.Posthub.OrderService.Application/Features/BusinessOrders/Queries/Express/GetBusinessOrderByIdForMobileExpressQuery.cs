﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.Express
{
    public class GetBusinessOrderByIdForMobileExpressQuery : IRequest<BusinessOrderForMobileDto>
    {
        public int Id { get; set; }
        public class GetBusinessOrderByIdForMobileExpressQueryHandler : IRequestHandler<GetBusinessOrderByIdForMobileExpressQuery, BusinessOrderForMobileDto>
        {
            private readonly ApplicationDbContext _context;
            public GetBusinessOrderByIdForMobileExpressQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<BusinessOrderForMobileDto> Handle(GetBusinessOrderByIdForMobileExpressQuery query, CancellationToken cancellationToken)
            {

                var order = _context.Orders.IgnoreQueryFilters().Where(a => a.Id == query.Id)
                         .Select(c => new BusinessOrderForMobileDto
                         {
                             Id = c.Id,
                             ContactId = c.BusinessOrder.ContactId,
                             ContractId = c.BusinessOrder != null ? c.BusinessOrder.ContractId : null,
                             CustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,
                             BusinessCustomerName = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomer != null ?
                             c.BusinessOrder.BusinessCustomer.BusinessName : "" : "",
                             DeliveryBefore = c.DeliveryBefore,
                             DirectOrderId = c.DirectOrderId,
                             FulfilledAt = c.FulfilledAt,
                             IssueAt = c.IssueAt,
                             OrderCode = c.OrderCode,
                             OrderStatus = c.Status.Name,
                             OrderType = c.OrderType.Name,
                             OrderTypeId = c.OrderTypeId,
                             ReadyAt = c.ReadyAt,
                             ServiceType = c.ServiceType.Name,
                             ServiceTypeId = c.ServiceTypeId,
                             StatusId = c.StatusId,
                             StartedAt = c.StartedAt,

                             DepartmentId = c.DepartmentId,
                             PickupLocationId = c.PickupLocationId,
                             PickupRequestNotificationId = c.PickupRequestNotificationId,

                             BusinessTypeId = c.BusinessOrder != null ? c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessTypeId : null : null,

                             //BusinessCode = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                             //c.BusinessOrder.BusinessCustomer.BusinessCode:"":"",

                             //BusinessCID = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                             //c.BusinessOrder.BusinessCustomer.BusinessCID:"":"",


                             BusinessCustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,

                             BusinessTypeName = c.BusinessOrder != null ? c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessType != null ?
                             c.BusinessOrder.BusinessType.Name_en : "" : "" : "",
                             DepartmentName = c.Department != null ? c.Department.Name_en : "",

                             //HQBranchName = c.BusinessOrder != null ?
                             //c.BusinessOrder.BusinessCustomer != null ?
                             //c.BusinessOrder.BusinessCustomer.HQBranch != null ?
                             //c.BusinessOrder.BusinessCustomer.HQBranch.Name : "" : "" : "",

                             ContractCode = c.BusinessOrder != null ? c.BusinessOrder.Contract != null ?
                             c.BusinessOrder.Contract.Code : "" : "",

                             ContractStatus = c.BusinessOrder != null ?
                             c.BusinessOrder.Contract != null ?
                             c.BusinessOrder.Contract.ContractStatus != null ? c.BusinessOrder.Contract.ContractStatus.Name : "" : "" : "",

                             BusinessCustomerBranchId = c.BusinessOrder != null ?
                              c.BusinessOrder.BusinessCustomerBranchId : null,

                             BusinessCustomerBranchName = c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessCustomerBranch != null ?
                             c.BusinessOrder.BusinessCustomerBranch.Name : "" : "",
                             BusinessOrderId = c.BusinessOrderId,
                             ServiceSectorId = c.ServiceSectorId,
                             AirWayBillNumber = c.OrderInternationalDelivery.AirWayBillNumber,
                             CourierId = c.OrderInternationalDelivery.CourierId,
                             CourierTrackingNumber = c.OrderInternationalDelivery.CourierTrackingNumber,
                             OrderById = c.BusinessOrder.OrderById,
                             PickUpAddress = new AddressDto
                             {
                                 Area = c.PickupAddress.Area,
                                 Governorate = c.PickupAddress.Governorate,
                                 Street = c.PickupAddress.Street,
                                 Block = c.PickupAddress.Block,
                                 Building = c.PickupAddress.Building,
                                 Flat = c.PickupAddress.Flat,
                                 Floor = c.PickupAddress.Floor,
                             },
                             IsMyAddress = c.IsMyAddress,

                         }).FirstOrDefault();
                if (order == null) return null;
                return order;
            }
        }
    }
}

