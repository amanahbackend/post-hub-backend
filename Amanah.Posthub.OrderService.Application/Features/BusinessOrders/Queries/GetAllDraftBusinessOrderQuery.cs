﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{


    public class GetAllDraftBusinessOrderQuery : IRequest<List<BusinessOrderForLoadDTO>>
    {
        public class GetAllDraftBusinessOrderQueryHandler : IRequestHandler<GetAllDraftBusinessOrderQuery, List<BusinessOrderForLoadDTO>>
        {
            private readonly ApplicationDbContext _context;
            public GetAllDraftBusinessOrderQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<List<BusinessOrderForLoadDTO>> Handle(GetAllDraftBusinessOrderQuery query, CancellationToken cancellationToken)
            {
                   
                var orderList = _context.Orders.AsNoTracking().IgnoreQueryFilters().Where(a => a.IsDraft == true && a.IsDeleted != true)
                         .Select(c => new BusinessOrderForLoadDTO
                         {
                             Id = c.Id,
                             Code = c.OrderCode,
                             businessCustomerId = c.BusinessOrder.BusinessCustomerId,

                         }).ToList();
                if (orderList == null) return null;
                return orderList;
            }
        }
    }
}
