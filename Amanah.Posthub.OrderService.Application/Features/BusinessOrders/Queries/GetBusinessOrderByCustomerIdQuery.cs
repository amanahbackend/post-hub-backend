﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{

    public class GetBusinessOrderByCustomerIdQuery : IRequest<BusinessOrderDto>
    {
        public int CustomerId { get; set; }
        public class GetBusinessOrderByCustomerIdQueryHandler : IRequestHandler<GetBusinessOrderByCustomerIdQuery, BusinessOrderDto>
        {
            private readonly ApplicationDbContext _context;
            public GetBusinessOrderByCustomerIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<BusinessOrderDto> Handle(GetBusinessOrderByCustomerIdQuery query, CancellationToken cancellationToken)
            {
                var order = _context.Orders.IgnoreQueryFilters().Where(a => a.BusinessOrder.BusinessCustomerId == query.CustomerId)
                    .Select(c => new BusinessOrderDto
                    {
                        Id = c.Id,
                        // CollectionFormSerialNo=c.BusinessOrder.BusinessCustomer.CollectionFormSerialNo,
                        ContactId = c.BusinessOrder.ContactId,
                        ContactName = c.BusinessOrder.Contact.FirstName + " " + c.BusinessOrder.Contact.Lastname,
                        ContractId = c.BusinessOrder.ContractId,
                        //  ContractName=c.Contract.
                        Currency = Thread.CurrentThread.CurrentCulture.IsArabic() ? c.Currency.Code_ar : c.Currency.Code_en,
                        CurrencyId = c.CurrencyId,
                        CustomerId = c.BusinessOrder.BusinessCustomerId,
                        CustomerName = c.BusinessOrder.BusinessCustomer.BusinessName,
                        DeliveryBefore = c.DeliveryBefore,
                        // DirectOrder=c.DirectOrder.,
                        DirectOrderId = c.DirectOrderId,
                        DropOffNote = c.DropOffNote,
                        FulfilledAt = c.FulfilledAt,
                        IssueAt = c.IssueAt,
                        TotalItemCount = c.TotalItemCount,
                        MailItemsTypeId = c.BusinessOrder.MailItemsTypeId,
                        MailItemsTypeName = Thread.CurrentThread.CurrentCulture.IsArabic() ? c.BusinessOrder.MailItemsType.Name_ar : c.BusinessOrder.MailItemsType.Name_en ,
                        OrderCode = c.OrderCode,
                        OrderStatus = c.Status.Name,
                        OrderType = c.OrderType.Name,
                        OrderTypeId = c.OrderTypeId,
                        PaymentMethod = c.PaymentMethod.Name,
                        PaymentMethodId = c.PaymentMethodId,
                        PickupNote = c.PickupNote,
                        PostagePerPiece = c.BusinessOrder.PostagePerPiece,
                        ReadyAt = c.ReadyAt,
                        ServiceSector = c.ServiceSector.Name_en,
                        ServiceSectorId = c.ServiceSectorId,
                        ServiceType = c.ServiceType.Name,
                        ServiceTypeId = c.ServiceTypeId,
                        StatusId = c.StatusId,
                        StartedAt = c.StartedAt,
                        TotalPostage = c.TotalPostage,
                        TotalWeight = c.TotalWeight,
                        WeightUOM = c.WeightUOM.Name_en,
                        WeightUOMId = c.WeightUOMId,
                        BusinessCustomerName = c.BusinessOrder.BusinessCustomer.BusinessName,
                        BusinessTypeName = c.BusinessOrder.BusinessCustomer.BusinessType.Name_en,
                        OrderByName = c.BusinessOrder.BusinessContactAccount.User.UserName,
                        DepartmentName = c.Department.Name_en,
                        BusinessCustomerCode = c.BusinessOrder.BusinessCustomer.BusinessCode,
                        HQBranchName = c.BusinessOrder.BusinessCustomer.HQBranch.Name,
                        ContractCode = c.BusinessOrder.Contract.Code,
                        HQBranchId = c.BusinessOrder.BusinessCustomer.HQBranchId,
                        DepartmentId = c.DepartmentId,
                        PickupLocationId = c.PickupLocationId,
                        PickupRequestNotificationId = c.PickupRequestNotificationId,
                        BusinessTypeId = c.BusinessOrder.BusinessCustomer.BusinessTypeId,
                        BusinessCode = c.BusinessOrder.BusinessCustomer.BusinessCode,
                        BusinessCID = c.BusinessOrder.BusinessCustomer.BusinessCID,
                        OrderById = c.BusinessOrder.OrderById,
                        BusinessCustomerId = c.BusinessOrder.BusinessCustomerId,
                    }

                    ).FirstOrDefault();
                if (order == null) return null;
                return order;
            }
        }
    }
}
