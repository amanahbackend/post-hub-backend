﻿using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
    public class GetBusinessOrderByIdForMobileQuery : IRequest<BusinessOrderForMobileDto>
    {
        public int Id { get; set; }
        public class GetBusinessOrderByIdForMobileQueryHandler : IRequestHandler<GetBusinessOrderByIdForMobileQuery, BusinessOrderForMobileDto>
        {
            private readonly ApplicationDbContext _context;
            private readonly IPaymentService _paymentService;
            public GetBusinessOrderByIdForMobileQueryHandler(ApplicationDbContext context, IPaymentService paymentService)
            {
                _context = context;
                _paymentService = paymentService;
            }
            public async Task<BusinessOrderForMobileDto> Handle(GetBusinessOrderByIdForMobileQuery query, CancellationToken cancellationToken)
            {
                var orderPayment = await _paymentService.GetPaymentTransactionAsync(query.Id.ToString());

                var order = _context.Orders
                        .Include(x => x.BusinessOrder)
                        .ThenInclude(x => x.BusinessCustomerBranch)
                        .Include(x => x.BusinessOrder)
                        .ThenInclude(x => x.BusinessCustomer)
                        .Include(x => x.BusinessOrder)
                        .ThenInclude(x => x.BusinessType)
                        .Include(x => x.BusinessOrder)
                        .ThenInclude(x => x.Contract)
                        .ThenInclude(x => x.ContractStatus)
                        .Include(c => c.OrderInternationalDelivery)
                        .Include(c => c.PickupAddress).IgnoreQueryFilters().Where(a => a.Id == query.Id)
                         .Select(c => new BusinessOrderForMobileDto
                         {
                             Id = c.Id,
                             ContactId = c.BusinessOrder.ContactId,
                             ContractId = c.BusinessOrder != null ? c.BusinessOrder.ContractId : null,
                             CustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,
                             BusinessCustomerName = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomer != null ?
                             c.BusinessOrder.BusinessCustomer.BusinessName : "" : "",
                             DeliveryBefore = c.DeliveryBefore,
                             DirectOrderId = c.DirectOrderId,
                             FulfilledAt = c.FulfilledAt,
                             IssueAt = c.IssueAt,
                             OrderCode = c.OrderCode,
                             OrderStatus = c.Status.Name,
                             OrderType = c.OrderType.Name,
                             OrderTypeId = c.OrderTypeId,
                             ReadyAt = c.ReadyAt,
                             ServiceType = c.ServiceType.Name,
                             ServiceTypeId = c.ServiceTypeId,
                             StatusId = c.StatusId,
                             StartedAt = c.StartedAt,

                             DepartmentId = c.DepartmentId,
                             PickupLocationId = c.PickupLocationId,
                             PickupRequestNotificationId = c.PickupRequestNotificationId,

                             BusinessTypeId = c.BusinessOrder != null ? c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessTypeId : null : null,

                             //BusinessCode = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                             //c.BusinessOrder.BusinessCustomer.BusinessCode:"":"",

                             //BusinessCID = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                             //c.BusinessOrder.BusinessCustomer.BusinessCID:"":"",


                             BusinessCustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,

                             BusinessTypeName = c.BusinessOrder != null ? c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessType != null ?
                             c.BusinessOrder.BusinessType.Name_en : "" : "" : "",
                             DepartmentName = c.Department != null ? c.Department.Name_en : "",

                             //HQBranchName = c.BusinessOrder != null ?
                             //c.BusinessOrder.BusinessCustomer != null ?
                             //c.BusinessOrder.BusinessCustomer.HQBranch != null ?
                             //c.BusinessOrder.BusinessCustomer.HQBranch.Name : "" : "" : "",

                             ContractCode = c.BusinessOrder != null ? c.BusinessOrder.Contract != null ?
                             c.BusinessOrder.Contract.Code : "" : "",

                             ContractStatus = c.BusinessOrder != null ?
                             c.BusinessOrder.Contract != null ?
                             c.BusinessOrder.Contract.ContractStatus != null ? c.BusinessOrder.Contract.ContractStatus.Name : "" : "" : "",

                             BusinessCustomerBranchId = c.BusinessOrder != null ?
                              c.BusinessOrder.BusinessCustomerBranchId : null,

                             BusinessCustomerBranchName = c.BusinessOrder != null ?
                             c.BusinessOrder.BusinessCustomerBranch != null ?
                             c.BusinessOrder.BusinessCustomerBranch.Name : "" : "",
                             BusinessOrderId = c.BusinessOrderId,
                             ServiceSectorId = c.ServiceSectorId,
                             AirWayBillNumber = c.OrderInternationalDelivery.AirWayBillNumber,
                             CourierId = c.OrderInternationalDelivery.CourierId,
                             CourierTrackingNumber = c.OrderInternationalDelivery.CourierTrackingNumber,
                             OrderById = c.BusinessOrder.OrderById,
                             PickUpAddress = new AddressDto
                             {
                                 Area = c.PickupAddress.Area,
                                 Governorate = c.PickupAddress.Governorate,
                                 Street = c.PickupAddress.Street,
                                 Block = c.PickupAddress.Block,
                                 Building = c.PickupAddress.Building,
                                 Flat = c.PickupAddress.Flat,
                                 Floor = c.PickupAddress.Floor,
                             },
                             IsMyAddress = c.IsMyAddress,
                             PackingPackageId = c.BusinessOrder.PackingPackageId,
                             OrderPackagePrice = c.BusinessOrder.OrderPackagePrice,
                             MailItem = c.MailItems.Select(a => new MailItemDto
                             {
                                 MailItemBarCode = a.MailItemBarCode,
                                 ConsigneeInfo = new ContactDto { FirstName = a.ConsigneeInfo.FirstName },
                                 Quantity = a.Quantity,
                                 TotalPrice = a.TotalPrice,
                             }).ToList(),
                             PaymentGateway = orderPayment.Data.TransactionResponseDetails != null ? orderPayment.Data.TransactionResponseDetails.PaymentType : string.Empty,
                             PaymentMethodId = orderPayment.Data.TransactionResponseDetails != null && orderPayment.Data.TransactionResponseDetails.PaymentType == "knet" ? 1 : 2,
                             TransactionId = orderPayment.Data.TransactionResponseDetails != null ? orderPayment.Data.TransactionResponseDetails.TransactionId : string.Empty,
                             AuthCode = orderPayment.Data.TransactionResponseDetails != null ? orderPayment.Data.TransactionResponseDetails.Auth : string.Empty,
                             IsCash = orderPayment.Data.PaymentRequestDetails != null ? (orderPayment.Data.PaymentRequestDetails.PaymentGateway == "cash" ? true : false) : null,
                             Services = c.BusinessOrder.BusinessOrderServices.Select(a => new ServiceResponseDTO
                             {
                                 Id = a.Service.Id,
                                 Name_en = a.Service.Name_en,
                                 Name_ar = a.Service.Name_ar,
                                 Price = a.Service.Price,
                                 Name = Thread.CurrentThread.CurrentCulture.IsArabic() ? a.Service.Name_ar : a.Service.Name_en,
                                 PhotoUrl = $"UploadFile/ServiceFile/{(string.IsNullOrEmpty(a.Service.PhotoUrl) ? "avtar.jpeg" : a.Service.PhotoUrl) }"
                             }).ToList()
                         }).FirstOrDefault();
                if (order == null) return null;
                return order;
            }
        }
    }
}
