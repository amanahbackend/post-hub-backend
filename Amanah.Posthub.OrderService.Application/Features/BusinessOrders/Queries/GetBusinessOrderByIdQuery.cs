﻿using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
    public class GetBusinessOrderByIdQuery : IRequest<BusinessOrderDto>
    {
        public int Id { get; set; }
        public class GetBusinessOrderByIdQueryHandler : IRequestHandler<GetBusinessOrderByIdQuery, BusinessOrderDto>
        {
            private readonly IPaymentService _paymentService;

            private readonly ApplicationDbContext _context;
            public GetBusinessOrderByIdQueryHandler(ApplicationDbContext context, IPaymentService paymentService)
            {
                _context = context;
                _paymentService = paymentService;
            }
            public async Task<BusinessOrderDto> Handle(GetBusinessOrderByIdQuery query, CancellationToken cancellationToken)
            {
                var orderPayment = await _paymentService.GetPaymentTransactionAsync(query.Id.ToString());
               
                BusinessOrderDto order = new BusinessOrderDto();
                
                     order = _context
                   .Orders
                   .Include(x => x.BusinessOrder)
                   .ThenInclude(x => x.BusinessCustomerBranch)
                   .Include(x => x.BusinessOrder)
                   .ThenInclude(x => x.BusinessCustomer)
                    .Include(x => x.BusinessOrder)
                   .ThenInclude(x => x.BusinessType)
                    .Include(x => x.BusinessOrder)
                   .ThenInclude(x => x.Contract)
                   .ThenInclude(x => x.ContractStatus)
                   .Include(c => c.OrderInternationalDelivery)
                   .Include(c => c.PickupAddress)
                   .IgnoreQueryFilters()
                   .Where(a => a.Id == query.Id)
                   .Select(c => new BusinessOrderDto
                   {
                       Id = c.Id,
                       ContactId = c.BusinessOrder.ContactId,
                       ContractId = c.BusinessOrder != null ? c.BusinessOrder.ContractId : null,
                       CustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,
                       BusinessCustomerName = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomer != null ?
                            c.BusinessOrder.BusinessCustomer.BusinessName : "" : "",
                       DeliveryBefore = c.DeliveryBefore,
                       DirectOrderId = c.DirectOrderId,
                       FulfilledAt = c.FulfilledAt,
                       IssueAt = c.IssueAt,
                       OrderCode = c.OrderCode,
                       OrderStatus = c.Status.Name,
                       OrderType = c.OrderType.Name,
                       OrderTypeId = c.OrderTypeId,
                       ReadyAt = c.ReadyAt,
                       ServiceType = c.ServiceType.Name,
                       ServiceTypeId = c.ServiceTypeId,
                       StatusId = c.StatusId,
                       StartedAt = c.StartedAt,

                       DepartmentId = c.DepartmentId,
                       PickupLocationId = c.PickupLocationId,
                       PickupRequestNotificationId = c.PickupRequestNotificationId,

                       BusinessTypeId = c.BusinessOrder != null ? c.BusinessOrder != null ?
                            c.BusinessOrder.BusinessTypeId : null : null,

                       ////BusinessCode = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                       ////c.BusinessOrder.BusinessCustomer.BusinessCode:"":"",

                       ////BusinessCID = c.BusinessOrder!=null? c.BusinessOrder.BusinessCustomer!=null ?
                       ////c.BusinessOrder.BusinessCustomer.BusinessCID:"":"",

                       ImgsUrl = c.BusinessOrder.AttachedFiles != null ? c.BusinessOrder.AttachedFiles.Select(a => $"UploadFile/OrderFiles/{ a.Path }").ToList() : null,

                       BusinessCustomerId = c.BusinessOrder != null ? c.BusinessOrder.BusinessCustomerId : null,

                       BusinessTypeName = c.BusinessOrder != null ? c.BusinessOrder != null ?
                            c.BusinessOrder.BusinessType != null ?
                            c.BusinessOrder.BusinessType.Name_en : "" : "" : "",
                       DepartmentName = c.Department != null ? c.Department.Name_en : "",

                       ////HQBranchName = c.BusinessOrder != null ?
                       ////c.BusinessOrder.BusinessCustomer != null ?
                       ////c.BusinessOrder.BusinessCustomer.HQBranch != null ?
                       ////c.BusinessOrder.BusinessCustomer.HQBranch.Name : "" : "" : "",

                       ContractCode = c.BusinessOrder != null ? c.BusinessOrder.Contract != null ?
                            c.BusinessOrder.Contract.Code : "" : "",

                       ContractStatus = c.BusinessOrder != null ?
                            c.BusinessOrder.Contract != null ?
                            c.BusinessOrder.Contract.ContractStatus != null ? c.BusinessOrder.Contract.ContractStatus.Name : "" : "" : "",

                       BusinessCustomerBranchId = c.BusinessOrder.BusinessCustomerBranchId != null ?
                             c.BusinessOrder.BusinessCustomerBranchId : null,

                       BusinessCustomerBranchName = c.BusinessOrder != null ?
                            c.BusinessOrder.BusinessCustomerBranch != null ?
                            c.BusinessOrder.BusinessCustomerBranch.Name : "" : "",
                       BusinessOrderId = c.BusinessOrderId,
                       ServiceSectorId = c.ServiceSectorId,
                       AirWayBillNumber = c.OrderInternationalDelivery.AirWayBillNumber!=null? c.OrderInternationalDelivery.AirWayBillNumber :null,
                       CourierId = c.OrderInternationalDelivery.CourierId !=null? c.OrderInternationalDelivery.CourierId:null,
                       CourierTrackingNumber = c.OrderInternationalDelivery.CourierTrackingNumber !=null? c.OrderInternationalDelivery.CourierTrackingNumber:null,
                       OrderById = c.BusinessOrder.OrderById !=null? c.BusinessOrder.OrderById:null,
                       PickUpAddress = new AddressDto
                       {
                           Area = c.PickupAddress != null ? c.PickupAddress.Area : string.Empty,
                           Governorate = c.PickupAddress != null ? c.PickupAddress.Governorate : string.Empty,
                           Street = c.PickupAddress != null ? c.PickupAddress.Street : string.Empty,
                           Block = c.PickupAddress != null ? c.PickupAddress.Block : string.Empty,
                           Building = c.PickupAddress != null ? c.PickupAddress.Building : string.Empty,
                           Flat = c.PickupAddress != null ? c.PickupAddress.Flat : string.Empty,
                           Floor = c.PickupAddress != null ? c.PickupAddress.Floor : string.Empty
                       },
                       IsMyAddress = c.IsMyAddress,
                       PackingPackageId = c.BusinessOrder.PackingPackageId != null ? c.BusinessOrder.PackingPackageId : null,
                       OrderPackagePrice = c.BusinessOrder.OrderPackagePrice != null ? c.BusinessOrder.OrderPackagePrice : null,
                       MailItem = c.MailItems.Count > 0 ? c.MailItems.Select(a => new MailItemDto
                       {
                           MailItemBarCode = a.MailItemBarCode,
                           ConsigneeInfo = new ContactDto { FirstName = a.ConsigneeInfo.FirstName, Mobile = a.ConsigneeInfo.Mobile },
                           Quantity = a.Quantity,
                           TotalPrice = a.TotalPrice,
                       }).ToList() : null,
                       PaymentGateway = orderPayment.Data.TransactionResponseDetails != null ? orderPayment.Data.TransactionResponseDetails.PaymentType : string.Empty,
                       PaymentMethodId = orderPayment.Data.TransactionResponseDetails!=null && orderPayment.Data.TransactionResponseDetails.PaymentType == "knet" ? 1 : 2,
                       TransactionId = orderPayment.Data.TransactionResponseDetails !=null ? orderPayment.Data.TransactionResponseDetails.TransactionId :string.Empty,
                       AuthCode = orderPayment.Data.TransactionResponseDetails!=null? orderPayment.Data.TransactionResponseDetails.Auth :string.Empty,
                       Services = c.BusinessOrder.BusinessOrderServices.Count > 0 ? c.BusinessOrder.BusinessOrderServices.Select(a => new ServiceResponseDTO
                       {
                           Id = a.Service.Id,
                           Name_en = a.Service.Name_en,
                           Name_ar = a.Service.Name_ar,
                           Price = a.Service.Price,
                           Name = Thread.CurrentThread.CurrentCulture.IsArabic() ? a.Service.Name_ar : a.Service.Name_en,
                           PhotoUrl = $"UploadFile/ServiceFile/{(string.IsNullOrEmpty(a.Service.PhotoUrl) ? "avtar.jpeg" : a.Service.PhotoUrl) }"
                       }).ToList() : null

                   })?.FirstOrDefault();
                

                if (order == null) return null;
                return order;
            }
        }
    }
}
