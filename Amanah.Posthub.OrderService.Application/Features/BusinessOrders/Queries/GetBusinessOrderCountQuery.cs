﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
    public class GetBusinessOrderCountQuery : IRequest<int>
    {

        public class GetBusinessOrderCountQueryHandler : IRequestHandler<GetBusinessOrderCountQuery, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly ICurrentUser _currentUser;

            public GetBusinessOrderCountQueryHandler(
                ApplicationDbContext context,
                ICurrentUser currentUser)
            {
                _context = context;
                _currentUser = currentUser;
            }
            public async Task<int> Handle(GetBusinessOrderCountQuery query, CancellationToken cancellationToken)
            {
                // Ramzy: need items list
                var orderCount = _context.Orders.IgnoreQueryFilters().Where(c => !c.IsDeleted &&
                      (!_currentUser.IsBusinessCustomer()
                           || (c.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)
                      )).Count();
                if (orderCount == 0)
                {
                    return 0;
                }
                return orderCount;
            }
        }
    }
}
