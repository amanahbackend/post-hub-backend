﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
    public class GetBusinessOrderListQuery : IRequest<IEnumerable<object>>
    {

        public class GetAllOrdersQueryHandler : IRequestHandler<GetBusinessOrderListQuery, IEnumerable<object>>
        {
            private readonly ApplicationDbContext _context;
            public GetAllOrdersQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<object>> Handle(GetBusinessOrderListQuery query, CancellationToken cancellationToken)
            {
                // Ramzy: need items list
                var orderList = await _context.Orders.IgnoreQueryFilters().Select(o => new { o.OrderCode }).ToListAsync();
                if (orderList == null)
                {
                    return null;
                }
                return orderList.AsReadOnly();
            }
        }

    }
}
