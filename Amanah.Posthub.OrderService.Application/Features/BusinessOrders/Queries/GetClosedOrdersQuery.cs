﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{

    public class GetClosedOrdersQuery : IRequest<IEnumerable<ClosedOrderDTO>>
    {

        public class GetClosedOrdersQueryHandler : IRequestHandler<GetClosedOrdersQuery, IEnumerable<ClosedOrderDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly ICurrentUser _currentUser;

            public GetClosedOrdersQueryHandler(ApplicationDbContext context, ICurrentUser currentUser)
            {
                _context = context;
                _currentUser = currentUser;
            }
            public async Task<IEnumerable<ClosedOrderDTO>> Handle(GetClosedOrdersQuery query, CancellationToken cancellationToken)
            {
                var orderList = await _context.Orders.IgnoreQueryFilters().Where(c => !c.IsDeleted
                && c.StatusId == (int)OrderStatuses.Closed 
                &&
                      (!_currentUser.IsBusinessCustomer()
                           || (c.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)
                      ))
                   .GroupBy(x => x.CreationTime.Date.Month)
                   .Select(c => new ClosedOrderDTO
                   {
                       Month = c.Key,
                       Count = c.Count()
                   })
                   .ToListAsync();
                if (orderList == null)
                {
                    return null;
                }
                return orderList;
            }
        }

    }
}
