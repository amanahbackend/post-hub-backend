﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{

    public class GetDailyOrdersQuery : IRequest<IEnumerable<DailyOrderDTO>>
    {

        public class GetDailyOrdersQueryHandler : IRequestHandler<GetDailyOrdersQuery, IEnumerable<DailyOrderDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly ICurrentUser _currentUser;

            public GetDailyOrdersQueryHandler(
                ApplicationDbContext context,
                ICurrentUser currentUser)
            {
                _context = context;
                _currentUser = currentUser;
            }
            public async Task<IEnumerable<DailyOrderDTO>> Handle(GetDailyOrdersQuery query, CancellationToken cancellationToken)
            {
                var orderList = await _context.Orders.IgnoreQueryFilters().Where(c => !c.IsDeleted
                    && c.CreationTime.Date.Month == DateTime.Now.Date.Month
                    &&
                      (!_currentUser.IsBusinessCustomer()
                           || (c.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)
                      ))
                    .GroupBy(x => new { x.CreationTime.Date.Month, x.CreationTime.Date.Day })
                    .Select(c => new DailyOrderDTO
                    {

                        Day = c.Key.Day.ToString(),//+"-" + c.Key.Month,
                        Count = c.Count()
                    })
                    .ToListAsync();
                if (orderList == null)
                {
                    return null;
                }
                return orderList;
            }
        }

    }
}
