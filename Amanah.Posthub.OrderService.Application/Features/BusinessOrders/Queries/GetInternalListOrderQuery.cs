﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{

    public class GetInternalListOrderQuery : PaginatedItemsViewModel, IRequest<PagedResult<InternalOrderDto>>
    {
        #region props
        public string Code { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        #endregion

        public class GetInternalListOrderQueryHandler : IRequestHandler<GetInternalListOrderQuery, PagedResult<InternalOrderDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetInternalListOrderQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<PagedResult<InternalOrderDto>> Handle(GetInternalListOrderQuery query, CancellationToken cancellationToken)
            {
                //var orders = await _context.Orders
                //    .Include(o => o.MailItems)
                //    .ThenInclude(m => m.ConsigneeInfo)
                //    //.Include(o => o.Sender)
                //    //.ThenInclude(s => s.User)
                //    .IgnoreQueryFilters()
                //    .Where(o => !o.IsDeleted
                //    && !o.IsDraft
                //    && o.ServiceSectorId == (int)ServiceSectors.InternalPost
                //    //&& o.IsInternal
                //    && (query.Code == null
                //    || o.OrderCode.Contains(query.Code))
                //    && (query.SenderName == null
                //    || o.SenderName.Contains(query.SenderName))
                //    && (query.ReceiverName == null
                //    || (o.MailItems.Count > 0 && o.MailItems.FirstOrDefault().ConsigneeInfo.FirstName.Contains(query.ReceiverName)))
                //    )
                //    .OrderByDescending(c => c.Id)
                //    .AsNoTracking()
                //    .AsQueryable()
                //    .Select(o => new InternalOrderDto()
                //    {
                //        id = o.Id,
                //        Code = o.OrderCode,
                //        SenderName = o.SenderName,
                //        ReceiverName = o.MailItems.FirstOrDefault().ConsigneeInfo.FirstName
                //    })
                //    .ToPagedResultAsync(query);

                var ordersEF = _context.Orders
                    .Include(o => o.MailItems)
                    .ThenInclude(m => m.ConsigneeInfo)
                    //.Include(o => o.Sender)
                    //.ThenInclude(s => s.User)
                    .IgnoreQueryFilters()
                    .Where(o => !o.IsDeleted
                    && !o.IsDraft
                    && o.ServiceSectorId == (int)ServiceSectors.InternalPost)
                    .OrderByDescending(c => c.Id)
                    .AsNoTracking()
                    .AsQueryable();

                if (!string.IsNullOrEmpty(query.Code))
                {
                    ordersEF = ordersEF.Where(o => o.OrderCode.Replace(" ", String.Empty).Contains(query.Code.Replace(" ", String.Empty)));
                }
                if (!string.IsNullOrEmpty(query.SenderName))
                {
                    ordersEF = ordersEF.Where(o => o.SenderName.Replace(" ", String.Empty).Contains(query.SenderName.Replace(" ", String.Empty)));

                }
                if (!string.IsNullOrEmpty(query.ReceiverName))
                {
                    ordersEF = ordersEF.Where(o => (o.MailItems.Count > 0 && o.MailItems.FirstOrDefault().ConsigneeInfo.FirstName.Replace(" ", String.Empty).Contains(query.ReceiverName.Replace(" ", String.Empty))));

                }
                var orders = await ordersEF.Select(o => new InternalOrderDto()
                {
                    id = o.Id,
                    Code = o.OrderCode,
                    SenderName = o.SenderName,
                    ReceiverName = o.MailItems.FirstOrDefault().ConsigneeInfo.FirstName
                }) .ToPagedResultAsync(query);

                return orders;
            }
        }
    }
}
