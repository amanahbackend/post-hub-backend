﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using MediatR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
    public class GetOrderTransactionsQuery : IRequest<IEnumerable<PaymentDetailsForMobileDto>>
    {
        public int OrderId { get; set; }

        public class GetOrderTransactionsQueryHandler : IRequestHandler<GetOrderTransactionsQuery, IEnumerable<PaymentDetailsForMobileDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetOrderTransactionsQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<IEnumerable<PaymentDetailsForMobileDto>> Handle(GetOrderTransactionsQuery request, CancellationToken cancellationToken)
            {
                try
                {
                    var transactionsList = _context.PaymentTransactions
                         .Where(a => a.ReferenceId == request.OrderId.ToString())
                        .Select(a => new PaymentDetailsForMobileDto
                        {
                            Result = a.IsSuccessTransaction,
                            AuthCode = a.TransactionResponseDetails != null ? ((JObject)JsonConvert.DeserializeObject(a.TransactionResponseDetails)).Value<string>("Auth") : null,
                            TransactionId = a.TransactionResponseDetails != null ? ((JObject)JsonConvert.DeserializeObject(a.TransactionResponseDetails)).Value<string>("TransactionId") : null,
                            CreatedDate = a.CreatedDate,
                            OrderId = a.OrderId,
                            TotalPrice = a.TotalPrice,
                            IsCash = a.PaymentRequestDetails != null ? ( ((JObject)JsonConvert.DeserializeObject(a.PaymentRequestDetails)).Value<string>("PaymentGateway") == "cash" ? true : false) : null,
                        }).ToList();

                    return transactionsList;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
