﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{

    public class GetReceiveOrderPaginatedListQuery : PaginatedItemsViewModel, IRequest<PagedResult<ReceiveOrderListResponseDTO>>
    {

        // public int BranchId { set; get; }

    }
    public class GetReceiveOrderPaginatedListQueryHandler : IRequestHandler<GetReceiveOrderPaginatedListQuery, PagedResult<ReceiveOrderListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetReceiveOrderPaginatedListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;

        }
        public async Task<PagedResult<ReceiveOrderListResponseDTO>> Handle(GetReceiveOrderPaginatedListQuery query, CancellationToken cancellationToken)
        {

            var ordersList = await _context
                 .Orders
                 .IgnoreQueryFilters()
                 .Where(x => !x.IsDeleted && x.IsDraft == false && x.ServiceSectorId == (int)ServiceSectors.InternalPost
                 && x.MailItems.FirstOrDefault().Workorder.WorkOrderStatusId == (int?)WorkorderStatuses.ReceivedByPostRoom)
                 .OrderByDescending(wo => wo.Id)
                 .AsQueryable()
                 .AsNoTracking()
                 .ProjectTo<ReceiveOrderListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(query);

            return ordersList;
        }
    }
}
