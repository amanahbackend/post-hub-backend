﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;


namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
   
    public class GetStatementFilteredPaginatedListQuery : PaginatedItemsViewModel, IRequest<PagedResult<StatementListResponseDTO>>
    {

         //public int? BranchId { set; get; }
         //public string Branch { set; get; }
         //public string Code { set; get; }
         //public string Status { set; get; }
         //public int? StatusId { set; get; }

    }
    public class GetStatementFilteredPaginatedListQueryHandler : IRequestHandler<GetStatementFilteredPaginatedListQuery, PagedResult<StatementListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetStatementFilteredPaginatedListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;

        }
        public async Task<PagedResult<StatementListResponseDTO>> Handle(GetStatementFilteredPaginatedListQuery query, CancellationToken cancellationToken)
        {

            var WorkordersEF =  _context
                 .Workorders
                 .IgnoreQueryFilters()
                 .Include(c => c.MailItems)
                 .ThenInclude(c => c.ConsigneeInfo)
                 .ThenInclude(c => c.Branch)
                 .Where(x => !x.IsDeleted && x.ServiceSectorId == (int)ServiceSectors.InternalPost)
                 .OrderByDescending(wo => wo.Id)
                 .AsQueryable()
                 .AsNoTracking();
            //.ProjectTo<StatementListResponseDTO>(_mapper.ConfigurationProvider)
            //.ToPagedResultAsync(query);
            if (!string.IsNullOrEmpty(query.SearchBy))
            {
                WorkordersEF = WorkordersEF.Where(c => c.WorkOrderStatus.Name.Contains(query.SearchBy)
                 || c.Code.Contains(query.SearchBy)
                 );
            }
            var ordersList = await WorkordersEF.ProjectTo<StatementListResponseDTO>(_mapper.ConfigurationProvider)
                .ToPagedResultAsync(query);

            return ordersList;
        }
    }
}
