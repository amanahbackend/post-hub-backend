﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;


namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
   
    public class GetStatementPaginatedListQuery : PaginatedItemsViewModel, IRequest<PagedResult<StatementListResponseDTO>>
    {

        // public int BranchId { set; get; }

    }
    public class GetStatementPaginatedListQueryHandler : IRequestHandler<GetStatementPaginatedListQuery, PagedResult<StatementListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetStatementPaginatedListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;

        }
        public async Task<PagedResult<StatementListResponseDTO>> Handle(GetStatementPaginatedListQuery query, CancellationToken cancellationToken)
        {

            var ordersList = await _context
                 .Workorders
                 .IgnoreQueryFilters()
                 .Include(c=>c.MailItems)
                 .ThenInclude(c=>c.ConsigneeInfo)
                 .ThenInclude(c=>c.Branch)
                 .Where(x => !x.IsDeleted  && x.ServiceSectorId == (int)ServiceSectors.InternalPost)
                 .OrderByDescending(wo => wo.Id)
                 .AsQueryable()
                 .AsNoTracking()
                 .ProjectTo<StatementListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(query);

            return ordersList;
        }
    }
}
