﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
    public class InternalOrderByIdQuery : IRequest<object>
    {
        public int Id { get; set; }
        public class InternalOrderByIdQueryHandler : IRequestHandler<InternalOrderByIdQuery, object>
        {
            private readonly ApplicationDbContext _context;
            private readonly ICurrentUser _currentUser;

            public InternalOrderByIdQueryHandler(
                ApplicationDbContext context,
                ICurrentUser currentUser)
            {
                _context = context;
                _currentUser = currentUser;
            }

            public async Task<object> Handle(InternalOrderByIdQuery query, CancellationToken cancellationToken)
            {
                var order = await _context.Orders
                    .Include(o => o.MailItems)
                    .ThenInclude(m => m.ConsigneeInfo)
                    .Where(o => !o.IsDeleted && o.Id == query.Id)
                    .Select(o => new InternalOrderQueryDto
                    {
                        SenderName = o.SenderName,
                        SenderBranchId = o.SenderBranchId,
                        SenderBranchName = o.SenderBranchId.HasValue ? o.SenderBranch.Name : o.SenderBranchName,
                        SenderEmail = o.SenderEmail,
                        SenderFloorNo = o.SenderFloorNo,
                        IsInternal = o.IsInternal,
                        MailItem = o.MailItems.Where(m => !m.IsDeleted).Select(m => new MailItemForInternalDto
                        {
                            Id = m.Id,
                            RemarkId = m.RemarkId,
                            Description = m.Description,
                            ReciverName = m.ConsigneeInfo.FirstName,
                            ReciverEmail = m.ConsigneeInfo.Email,
                            ReciverBranchName = m.ConsigneeInfo.BranchName,
                            DepartmentName = m.ConsigneeInfo.DepartmentName,
                            ReciverfloorNo = m.ConsigneeInfo.FloorNo,
                        })
                    })
                    .FirstOrDefaultAsync();

                return order;
            }
        }
    }
}
