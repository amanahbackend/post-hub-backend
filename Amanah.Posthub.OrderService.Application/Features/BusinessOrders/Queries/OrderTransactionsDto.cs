﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{
    public class OrderTransactionsDto
    {
        public int TransactionId { get; set; }
        public int TransactionResultStatusId { get; set; }
        public string TransactionResultStatusName { get; set; }
        public DateTime TransactionDate { get; set; }
    }
}