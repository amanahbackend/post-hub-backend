﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Queries
{


    public class PrintPostBagStatementQuery : IRequest<PrintPostBagStatementDTO>
    {
        public int WorkOrderId { set; get; }
    }
    public class PrintPostBagStatementQueryHandler : IRequestHandler<PrintPostBagStatementQuery, PrintPostBagStatementDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public PrintPostBagStatementQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;

        }
        public async Task<PrintPostBagStatementDTO> Handle(PrintPostBagStatementQuery query, CancellationToken cancellationToken)
        {
            PrintPostBagStatementDTO order = new PrintPostBagStatementDTO();
            var orderEF = await _context
                 .Workorders
                 .IgnoreQueryFilters()
                 .AsQueryable()
                 .AsNoTracking()
                 .Include(c => c.MailItems)
                 .ThenInclude(c => c.ConsigneeInfo)
                 .ThenInclude(c => c.Branch)
                 .Where(x => x.Id == query.WorkOrderId && !x.IsDeleted  && x.ServiceSectorId == (int)ServiceSectors.InternalPost)
                 // .ProjectTo<PrintPostBagStatementDTO>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync();

            order = _mapper.Map<PrintPostBagStatementDTO>(orderEF);
            var mailItems = _mapper.Map<List<MailItemResponseDTO>>(orderEF.MailItems);
            order.ItemsList = mailItems;

            return order;
        }
    }
}
