﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Commands
{

    public class ChangeContractStatusHandler : IRequestHandler<ChangeContractStatusInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Contract> _contractRepository;


        public ChangeContractStatusHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Contract> contractRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _contractRepository = contractRepository;
        }
        public async Task<int> Handle(ChangeContractStatusInputDTO command, CancellationToken cancellationToken)
        {
            Contract contract = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                contract = _context.Contracts.AsNoTracking().Where(c => c.Id == command.ContractId)
                .FirstOrDefault();

                if (contract != null)
                {
                    contract.ContractStatusId = command.ContractStatusId;


                }

                _contractRepository.Update(contract);
                await _unityOfWork.SaveChangesAsync();

            });
            if (result)
            {
                return contract.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
