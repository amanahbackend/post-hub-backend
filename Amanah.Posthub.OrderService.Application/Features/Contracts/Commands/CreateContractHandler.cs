﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Commands
{
    public class CreateContractHandler : IRequestHandler<CreateContractInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Contract> _contractRepository;
        private readonly ILocalizer _localizer;


        public CreateContractHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Contract> contractRepository,
            ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _contractRepository = contractRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(CreateContractInputDTO command, CancellationToken cancellationToken)
        {
            Contract contract = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
          {
              contract = _mapper.Map<Contract>(command);
              var contractEF = _context.Contracts.Where(c => ! c.IsDeleted && c.ContractDate.Value.Date == command.ContractDate.Value.Date
                               && c.ContractStartDate.Value.Date == command.ContractStartDate.Value.Date
                               && c.ContractEndDate.Value.Date == command.ContractEndDate.Value.Date).FirstOrDefault();
              if (contractEF == null)
              {

                  if (command.Terms != null)
                  {
                      contract.ExtraTerms = command.Terms.Select(c => new ExtraTerm
                      {
                          Term = c.Term
                      }).ToList();
                  }
                  contract.ContractStatusId = (int?) ContractStatus.Active;
                  contract.Code = await GenerateContractCodeAsync();
                  _contractRepository.Add(contract);
                  await _unityOfWork.SaveChangesAsync();

              }
              else
              {
                  
                  throw new DomainException(_localizer[Keys.Messages.ContractExist]);
                  
              }

          });
            if (result)
            {
                return contract.Id;
            }
            else
            {
                return 0;
            }

        }
    
    
        private async Task<string> GenerateContractCodeAsync()
        {
           int ContractLastid= await  _context.Contacts.Select(x => x.Id).OrderBy(x=>x).LastOrDefaultAsync();
            StringBuilder sb = new StringBuilder();
            sb.Append("CONT");
            sb.Append($"{(ContractLastid + 1):00000000}");
           return sb.ToString();
        }
    
    }
}
