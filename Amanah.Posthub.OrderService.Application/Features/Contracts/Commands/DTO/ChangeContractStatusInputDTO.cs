﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO
{
    public class ChangeContractStatusInputDTO : IRequest<int>
    {
        public int ContractStatusId { get; set; }
        public int ContractId { get; set; }
    }
}
