﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO
{
    public class DeleteContractInputDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
