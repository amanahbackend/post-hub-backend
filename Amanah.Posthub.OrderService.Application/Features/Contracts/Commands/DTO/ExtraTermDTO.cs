﻿namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO
{
    public class ExtraTermDTO
    {
        public int Id { get; set; }
        public string Term { get; set; }
        public int? ContractId { get; set; }
    }
}
