﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Commands
{

    public class DeleteContractHandler : IRequestHandler<DeleteContractInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Contract, int> _contractRepository;


        public DeleteContractHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Contract, int> contractRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _contractRepository = contractRepository;
        }
        public async Task<int> Handle(DeleteContractInputDTO command, CancellationToken cancellationToken)
        {
            Contract contract = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                contract = _context.Contracts.IgnoreQueryFilters().Where(c => c.Id == command.Id).FirstOrDefault();
                if (contract != null)
                {
                    contract.IsDeleted = true;
                    _contractRepository.Update(contract);
                    await _unityOfWork.SaveChangesAsync();
                }


            });
            if (result)
            {
                return contract.Id;
            }
            else
            {
                return 0;
            }

        }

    }
}
