﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.contracts.Commands
{
    public class UpdateContractHandler : IRequestHandler<UpdateContractInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Contract> _contractRepository;
        private readonly ILocalizer _localizer;


        public UpdateContractHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Contract> contractRepository,
            ILocalizer localizer)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _contractRepository = contractRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(UpdateContractInputDTO command, CancellationToken cancellationToken)
        {
            Contract contract = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                //  contract = _mapper.Map<Contract>(command);

                // if(command.Terms!=null)
                // contract.ExtraTerms = _mapper.Map<List<ExtraTerm>>(command.Terms);

                contract = _context.Contracts.AsNoTracking().IgnoreQueryFilters().Where(c => c.Id == command.Id)
            .Include(c => c.ExtraTerms.Where(x => !x.IsDeleted))
            .FirstOrDefault();
                if (string.IsNullOrEmpty(command.Code))
                   command.Code = contract.Code;
                contract = _mapper.Map<Contract>(command);
                var contractEF = _context.Contracts.Where(c => !c.IsDeleted && c.Id != command.Id && c.ContractDate.Value.Date == command.ContractDate.Value.Date
                                 && c.ContractStartDate.Value.Date == command.ContractStartDate.Value.Date
                                 && c.ContractEndDate.Value.Date == command.ContractEndDate.Value.Date).FirstOrDefault();
                if (contractEF == null)
                {
                    if (contract != null)
                    {
                        // contract.Id=command.Id;
                        contract.Code = command.Code;
                        contract.ContractDate = command.ContractDate;
                        contract.ContractStartDate = command.ContractStartDate;
                        contract.ContractEndDate = command.ContractEndDate;
                        contract.ContractStatusId = command.ContractStatusId;
                        contract.ContractTypeId = command.ContractTypeId;
                        contract.CurrencyId = command.CurrencyId;
                        contract.DistriputionAllowance = command.DistriputionAllowance;
                        contract.InstallmentsAfterEachMessage = command.InstallmentsAfterEachMessage;
                        contract.InstallmentsAtContracting = command.InstallmentsAtContracting;
                        contract.InstallmentsPaymentNotLaterThan = command.InstallmentsPaymentNotLaterThan;
                        contract.InstallmentsPaymentNotLaterThanPeriodType = command.InstallmentsPaymentNotLaterThanPeriodType;
                        contract.IsPrintAndFixEnabledEnabled = command.IsPrintAndFixEnabledEnabled;
                        contract.IsPrintReciptListAndCollectEnabled = command.IsPrintReciptListAndCollectEnabled;
                        contract.ItemSupplyPeriod = command.ItemSupplyPeriod;
                        contract.NoOfMessages = command.NoOfMessages;
                        contract.NumberOfUnits = command.NumberOfUnits;
                        contract.PaymentNotLetterThan = command.PaymentNotLetterThan;
                        contract.PaymentNotLetterThanPeriodType = command.PaymentNotLetterThanPeriodType;
                        contract.PaymentType = command.PaymentType;
                        contract.PricePerUnit = command.PricePerUnit;
                        contract.ServiceSectorId = command.ServiceSectorId;
                        contract.BusinessCustomerId = command.BusinessCustomerId;
                        contract.RFQId = command.RFQId;

                        contract.ServiceDistributionPeriodValue = command.ServiceDistributionPeriodValue;
                        contract.PaymentNotLetterThanPeriodValue = command.PaymentNotLetterThanPeriodValue;
                        contract.ItemsSupplyAheadOf = command.ItemsSupplyAheadOf;
                        contract.ItemsSupplyAheadOfValue = command.ItemsSupplyAheadOfValue;
                        contract.ServiceDistributionPeriod = command.ServiceDistributionPeriod;
                        contract.ServiceDistributionPeriodValue = command.ServiceDistributionPeriodValue;
                        contract.PaymentTermsMessageNo = command.PaymentTermsMessageNo;

                    }
                    if (command.Terms != null)
                    {
                        if (contract.ExtraTerms == null)
                        {
                            contract.ExtraTerms = command.Terms.Select(c => new ExtraTerm
                            {
                                Term = c.Term
                            }).ToList();
                        }
                        else
                        {
                            foreach (var item in command.Terms)
                            {
                                if (contract.ExtraTerms.Where(c => c.Id == item.Id && c.Id > 0).Any())
                                {
                                    var trm = contract.ExtraTerms.Where(c => c.Id == item.Id).FirstOrDefault();
                                    trm.Term = item.Term;

                                }
                                else
                                {
                                    var trm = new ExtraTerm();
                                    trm.Term = item.Term;

                                    contract.ExtraTerms.Add(trm);
                                }
                            }
                        }
                        var commandTerms = command.Terms.Select(cm => cm.Id);
                        var toBeDeletedTerms = contract.ExtraTerms.Where(m => !commandTerms.Contains(m.Id));
                        if (toBeDeletedTerms != null && toBeDeletedTerms.Count() > 0)
                        {
                            foreach (var trm in toBeDeletedTerms)
                            {
                                trm.IsDeleted = true;
                                _context.ExtraTerms.Update(trm);
                                //await _unityOfWork.SaveChangesAsync();

                            }
                        }
                    }

                    else
                    {
                        contract.ExtraTerms = new List<ExtraTerm>();
                    }
                    _contractRepository.Update(contract);
                    await _unityOfWork.SaveChangesAsync();
                }
                else
                {

                    throw new DomainException(_localizer[Keys.Messages.ContractExist]);

                }
            });
            if (result)
            {
                return contract.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}


