﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{
    public class ContractStatussLookupQuery : IRequest<IEnumerable<LookupResult<int>>>
    {

        public class ContractStatussLookupQueryHandler : IRequestHandler<ContractStatussLookupQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public ContractStatussLookupQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(ContractStatussLookupQuery query, CancellationToken cancellationToken)
            {
                // need to upload ContractStatuss Entity
                var contractList = await _context.ContractStatus
                    .IgnoreQueryFilters()
                    .Where(c => !c.IsDeleted && c.IsActive)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,
                        NameAr = c.Name
                    }).ToListAsync();

                if (contractList == null)
                {
                    return null;
                }
                return contractList;
            }
        }

    }
}
