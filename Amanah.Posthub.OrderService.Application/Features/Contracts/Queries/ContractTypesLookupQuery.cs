﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{
    public class ContractTypesLookupQuery : IRequest<IEnumerable<LookupResult<int>>>
    {

        public class ContractTypesLookupQueryHandler : IRequestHandler<ContractTypesLookupQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public ContractTypesLookupQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(ContractTypesLookupQuery query, CancellationToken cancellationToken)
            {
                //need to upload ContractTypes Entity
                var contractList = await _context.ContractType
                    .IgnoreQueryFilters()
                     .Where(c => !c.IsDeleted && c.IsActive)
                     .Select(c => new LookupResult<int>
                     {
                         Id = c.Id,
                         Name = c.Name,
                     }).ToListAsync();

                if (contractList == null)
                {
                    return null;
                }
                return contractList;

                //return new List<LookupResult<int>>() { new LookupResult<int>() { Id = 1, Name = "ContractType test", NameAr = "ContractType test" } };
            }
        }

    }
}
