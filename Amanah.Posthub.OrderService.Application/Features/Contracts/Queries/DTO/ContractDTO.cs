﻿using Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries.DTO
{
    public class ContractDTO
    {
        public int Id { get; set; }
        public int? ServiceSectorId { get; set; }
        public int? ContractTypeId { get; set; }
        public DateTime? ContractDate { get; set; }
        public DateTime? ContractStartDate { set; get; }
        public DateTime? ContractEndDate { set; get; }
        /// <summary>
        /// /Customer Info 
        /// </summary>
        public int? BusinessCustomerId { get; set; }
        public string BusinessCustomerName { get; set; }
        public decimal? NoOfMessages { get; set; }
        public bool IsPrintReciptListAndCollectEnabled { get; set; }
        public bool IsPrintAndFixEnabledEnabled { get; set; }
        // public decimal? ServiceDistriputionPeriod { get; set; }
        public decimal? ItemSupplyPeriod { get; set; }
        public decimal? DistriputionAllowance { get; set; }
        ///Payment Terms 
        public int PaymentType { set; get; }
        public int? PaymentNotLetterThan { set; get; }
        public int PaymentNotLetterThanPeriodType { set; get; }
        /// Installments 
        public decimal? InstallmentsAtContracting { set; get; }
        public decimal? InstallmentsAfterEachMessage { set; get; }
        public decimal? InstallmentsPaymentNotLaterThan { set; get; }
        public int InstallmentsPaymentNotLaterThanPeriodType { set; get; }
        public int? ContractStatusId { get; set; }
        public string Code { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? PricePerUnit { get; set; }
        public int? NumberOfUnits { get; set; }
        public string Currency { get; set; }
        public int ContractDays { get; set; }
        public string MainContactName { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public int? RFQId { get; set; }

        public decimal? PaymentNotLetterThanPeriodValue { set; get; }
        public int ItemsSupplyAheadOf { set; get; }
        public decimal? ItemsSupplyAheadOfValue { set; get; }
        public int ServiceDistributionPeriod { set; get; }
        public decimal? ServiceDistributionPeriodValue { set; get; }
        public int? PaymentTermsMessageNo { get; set; }

        public List<ExtraTermDTO> Terms { get; set; }

    }
}
