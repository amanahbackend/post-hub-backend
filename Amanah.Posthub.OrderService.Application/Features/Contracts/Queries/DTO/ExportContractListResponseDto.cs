﻿using System;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class ExportContractListResponseDto
    {
        public DateTime ContractDate { get; set; }
        public DateTime ContractStartDate { set; get; }
        public DateTime ContractEndDate { set; get; }
        public string ContractTypeName { get; set; }
        //public int ContractTypeId { get; set; }
        public string BusinessIndustry { get; set; }
        public string CustomerName { get; set; }
        public string PaymentTypeName { get; set; }
        public string PaymentTypeId { get; set; }
        public string Code { get; set; }
        //public int? ContractStatusId { get; set; }
        public string ContractStatusName { get; set; }

    }
}
