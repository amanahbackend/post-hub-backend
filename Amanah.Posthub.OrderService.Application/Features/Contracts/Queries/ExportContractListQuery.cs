﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{

    public class ExportContractListQuery : IRequest<List<ExportContractListResponseDto>>
    { }

    public class ExportContractListQueryHandler : IRequestHandler<ExportContractListQuery, List<ExportContractListResponseDto>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public ExportContractListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<ExportContractListResponseDto>> Handle(ExportContractListQuery query, CancellationToken cancellationToken)
        {
            // Ramzy: need items list
            var contractList = await _context
                .Contracts
                .IgnoreQueryFilters()
                .ProjectTo<ExportContractListResponseDto>(_mapper.ConfigurationProvider)
                .ToListAsync();

            if (contractList == null)
            {
                return null;
            }
            return contractList;
        }
    }


}
