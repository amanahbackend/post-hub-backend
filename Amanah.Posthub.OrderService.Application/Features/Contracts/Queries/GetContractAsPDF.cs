﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.ExternalServcies;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Infrastructure.ExternalServices.Settings;
using Amanah.Posthub.SharedKernel.Common.Reporting;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{

    public class GetContractAsPDF : IRequest<byte[]>
    {
        public int ContractId { set; get; }
        public bool SendContractToCustomerEmail { set; get; } = false;


    }

    public class GetContractAsPDFHandler : IRequestHandler<GetContractAsPDF, byte[]>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        protected readonly IWebHostEnvironment _webHostEnvirnoment;
        protected readonly IRDLCrenderService _RDLCrenderService;
        private readonly IEmailSenderservice _emailsender;
        private readonly EmailSettings _emailSettings;


        public GetContractAsPDFHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IWebHostEnvironment webHostEnvirnoment,
            IRDLCrenderService RDLCrenderService,
            IEmailSenderservice emailsender,
            IOptions<EmailSettings> emailSettings)
        {
            _context = context;
            _mapper = mapper;
            _webHostEnvirnoment = webHostEnvirnoment;
            _RDLCrenderService = RDLCrenderService;
            _emailsender = emailsender;
            _emailSettings = emailSettings.Value;
        }
        public async Task<byte[]> Handle(GetContractAsPDF query, CancellationToken cancellationToken)
        {
            // Ramzy: need items list
            var contractData = await _context
                .Contracts
                .Include(x => x.RFQ)
                .Include(x => x.BusinessCustomer)
                     .ThenInclude(c => c.MainContact).ThenInclude(c => c.User)
                 .Include(x => x.BusinessCustomer)
                     .ThenInclude(c => c.HQBranch)

                 .IgnoreQueryFilters()
                 .Where(x => x.Id == query.ContractId)
                .FirstOrDefaultAsync();

            if (contractData == null)
            {
                return null;
            }

            string path = $"{this._webHostEnvirnoment.ContentRootPath}\\ExportedTemplates\\ContractTemplateModel6.rdlc";
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            if (contractData.RFQ != null)
            {
                if (contractData.RFQ.Total.HasValue)
                {
                    path = $"{this._webHostEnvirnoment.ContentRootPath}\\ExportedTemplates\\ContractTemplateModel7.rdlc";
                    parameters.Add("RFQTotal", contractData.RFQ.Total.Value.ToString());
                }
            }


            var area = contractData.BusinessCustomer != null ? contractData.BusinessCustomer.HQBranch != null ?
                 contractData.BusinessCustomer.HQBranch.Location != null ? contractData.BusinessCustomer.HQBranch.Location.Area : "" : "" : "";
            var block = contractData.BusinessCustomer != null ? contractData.BusinessCustomer.HQBranch != null ?
                  contractData.BusinessCustomer.HQBranch.Location != null ? contractData.BusinessCustomer.HQBranch.Location.Block : "" : "" : "";
            var street = contractData.BusinessCustomer != null ? contractData.BusinessCustomer.HQBranch != null ?
                 contractData.BusinessCustomer.HQBranch.Location != null ? contractData.BusinessCustomer.HQBranch.Location.Street : "" : "" : "";

            var fullName = contractData.BusinessCustomer != null ? contractData.BusinessCustomer.MainContact != null ?
                 contractData.BusinessCustomer.MainContact.User != null ? contractData.BusinessCustomer.MainContact.User.FullName : "" : "" : "";
            var email = contractData.BusinessCustomer != null ? contractData.BusinessCustomer.MainContact != null ?
                contractData.BusinessCustomer.MainContact.User != null ? contractData.BusinessCustomer.MainContact.User.Email : "" : "" : "";

            parameters.Add("BusinessCustomerName", contractData.BusinessCustomer.BusinessName);
            parameters.Add("BusinessCustomerAddress", area + "-" + block + "-" + street);
            parameters.Add("BusinessCustomerContact", fullName);
            parameters.Add("NumberOfUnits", contractData.NumberOfUnits.HasValue ? contractData.NumberOfUnits.Value.ToString() : "");
            parameters.Add("PricePerUnit", contractData.PricePerUnit.HasValue ? contractData.PricePerUnit.Value.ToString() : "");
            parameters.Add("TotalValue", contractData.PricePerUnit.HasValue && contractData.NumberOfUnits.HasValue ? (contractData.PricePerUnit.Value * contractData.NumberOfUnits.Value).ToString() : " ");
            parameters.Add("ContractDays", (contractData.ContractEndDate - contractData.ContractStartDate).Value.TotalDays.ToString());

            var result = _RDLCrenderService.RenderRdlc(path, ExportReportType.Pdf, parameters, null);


            try
            {
                if (query.SendContractToCustomerEmail)
                {
                    var attchments = new List<byte[]>();

                    attchments.Add(result);

                    var body = string.Format(MailBodyTemplates.SendContractMail, fullName, _emailSettings.SenderName);
                    await _emailsender.SendEmailAsync($"Post Express Contract ", body, new List<string> { email }, attchments);

                }
            }
            catch (Exception e)
            {
                throw new DomainException(e.Message);
            }

            return result;
        }
    }


}
