﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.Contracts.Queries.DTO;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{

    public class GetContractByIdQuery : IRequest<ContractDTO>
    {
        public int Id { get; set; }
        public class GetContractByIdQueryHandler : IRequestHandler<GetContractByIdQuery, ContractDTO>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetContractByIdQueryHandler(
                ApplicationDbContext context,
                IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<ContractDTO> Handle(GetContractByIdQuery query, CancellationToken cancellationToken)
            {
                // Ramzy: need items list
                var contract = await _context
                    .Contracts.AsNoTracking()
                    .Include(c => c.ExtraTerms)
                    .IgnoreQueryFilters()
                    .Where(c => !c.IsDeleted && c.Id == query.Id)
                    // .ProjectTo<ContractDTO>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync();
                if (contract == null)
                {
                    return null;
                }
                var mappedContract = _mapper.Map<ContractDTO>(contract);
                mappedContract.Terms = _mapper.Map<List<ExtraTermDTO>>(contract.ExtraTerms);

                return mappedContract;
            }
        }
    }
}
