﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{

    public class GetContractListQuery : IRequest<IEnumerable<ContractListResponseDto>>
    {


    }

    public class GetContractListQueryHandler : IRequestHandler<GetContractListQuery, IEnumerable<ContractListResponseDto>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetContractListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<IEnumerable<ContractListResponseDto>> Handle(GetContractListQuery query, CancellationToken cancellationToken)
        {
            // Ramzy: need items list
            var contractList = await _context
                .Contracts
                .IgnoreQueryFilters()
                .ProjectTo<ContractListResponseDto>(_mapper.ConfigurationProvider)
                .ToListAsync();

            if (contractList == null)
            {
                return null;
            }
            return contractList;
        }
    }


}
