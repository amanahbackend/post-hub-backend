﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{

    public class GetContractPagginatedListQuery : PaginatedItemsViewModel, IRequest<PagedResult<ContractListResponseDto>>
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? ContractTypeId { get; set; }
        public int? ContractStatusId { get; set; }
        public int? ServiceSectorId { get; set; }

    }

    public class GetContractPagginatedListHandler : IRequestHandler<GetContractPagginatedListQuery, PagedResult<ContractListResponseDto>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetContractPagginatedListHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PagedResult<ContractListResponseDto>> Handle(GetContractPagginatedListQuery query, CancellationToken cancellationToken)
        {
            // Ramzy: need items list
            var contractList = new PagedResult<ContractListResponseDto>();
            var queryExist = _context
                .Contracts
                .IgnoreQueryFilters()
                .AsQueryable()
                .AsNoTracking()
                .Where(contract => !contract.IsDeleted).OrderByDescending(c => c.Id);
            // .ProjectTo<ContractListResponseDto>(_mapper.ConfigurationProvider)
            //.ToPagedResultAsync(query);
            if (!string.IsNullOrWhiteSpace(query.SearchBy))
            {
                queryExist = queryExist.Where(contract => (
                     contract.Code.Contains(query.SearchBy) ||
                     contract.BusinessCustomer.BusinessCode.Contains(query.SearchBy) ||
                     contract.BusinessCustomer.BusinessName.Contains(query.SearchBy))).OrderByDescending(c => c.Id);

            }
            if (query.FromDate != null && query.ToDate != null && query.ServiceSectorId > 0 && query.ContractStatusId > 0 && query.ContractTypeId > 0)
            {
                queryExist = queryExist.Where(c => (
                                    c.ContractStartDate.Value.Date == query.FromDate.Value.Date && c.ServiceSectorId == query.ServiceSectorId &&
                                    c.ContractEndDate.Value.Date == query.ToDate.Value.Date && c.ContractStatusId == query.ContractStatusId &&
                                    c.ContractTypeId == query.ContractTypeId)).OrderByDescending(c => c.Id);
            }
            contractList = await queryExist.ProjectTo<ContractListResponseDto>(_mapper.ConfigurationProvider).ToPagedResultAsync(query);

            if (!contractList.Result.Any())
            {
                return null;
            }
            return contractList;
        }
    }

}
