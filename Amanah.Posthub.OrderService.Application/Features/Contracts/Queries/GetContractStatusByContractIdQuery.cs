﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{

    public class GetContractStatusByContractIdQuery : IRequest<ContractDto>
    {
        public int ContractStatusId { get; set; }
        public class GetAllOrdersQueryHandler : IRequestHandler<GetContractStatusByContractIdQuery, ContractDto>
        {
            private readonly ApplicationDbContext _context;
            public GetAllOrdersQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<ContractDto> Handle(GetContractStatusByContractIdQuery query, CancellationToken cancellationToken)
            {
                // Ramzy: need items list
                var contract = await _context.Contracts.IgnoreQueryFilters().Where(c => c.Id == query.ContractStatusId)
                    .Select(c => new ContractDto
                    {
                        Id = c.Id,
                        ContractStatusId = c.ContractStatusId,
                        Code = c.Code,
                        ContractStatusName = c.ContractStatus.Name,
                    }).FirstOrDefaultAsync();

                if (contract == null)
                {
                    return null;
                }
                return contract;
            }
        }

    }
}
