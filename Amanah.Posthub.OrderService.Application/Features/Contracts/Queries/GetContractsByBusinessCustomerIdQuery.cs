﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{

    public class GetContractsByBusinessCustomerIdQuery : IRequest<List<ContractListResponseDto>>
    {
        public int BusinessCustomerId { get; set; }

    }

    public class GetContractsByBusinessCustomerIdQueryHandler : IRequestHandler<GetContractsByBusinessCustomerIdQuery, List<ContractListResponseDto>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetContractsByBusinessCustomerIdQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<ContractListResponseDto>> Handle(GetContractsByBusinessCustomerIdQuery query, CancellationToken cancellationToken)
        {
            // Ramzy: need items list
            var contractList = await _context
                .Contracts
                .IgnoreQueryFilters()
                .Where(c => !c.IsDeleted && c.BusinessCustomerId == query.BusinessCustomerId)
                .ProjectTo<ContractListResponseDto>(_mapper.ConfigurationProvider)
                .ToListAsync();

            if (contractList == null)
            {
                return null;
            }
            return contractList;
        }
    }
}
