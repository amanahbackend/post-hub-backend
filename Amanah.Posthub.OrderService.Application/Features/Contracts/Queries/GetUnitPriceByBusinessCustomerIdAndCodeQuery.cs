﻿using Amanah.Posthub.Context;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Amanah.Posthub.OrderService.Application.Features.Contracts.Queries
{

    public class GetUnitPriceByBusinessCustomerIdAndCodeQuery : IRequest<decimal>
    {
        public int BusinessCustomerId { get; set; }
        public string ContractCode { get; set; }

    }

    public class GetUnitPriceByBusinessCustomerIdAndCodeQueryHandler : IRequestHandler<GetUnitPriceByBusinessCustomerIdAndCodeQuery, decimal>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetUnitPriceByBusinessCustomerIdAndCodeQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<decimal> Handle(GetUnitPriceByBusinessCustomerIdAndCodeQuery query, CancellationToken cancellationToken)
        {
            var unitPrice = _context
                .Contracts
                .IgnoreQueryFilters()
                .FirstOrDefault(c => !c.IsDeleted && c.BusinessCustomerId == query.BusinessCustomerId && c.Code == query.ContractCode);
               

            if (unitPrice.PricePerUnit == null)
            {
                return 0;
            }
            return unitPrice.PricePerUnit.Value;
        }
    }
}
