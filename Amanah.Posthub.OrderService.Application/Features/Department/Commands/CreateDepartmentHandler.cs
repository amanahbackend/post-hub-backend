﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Department.Commands.DTO;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Department.Commands
{

    public class CreateDepartmentHandler : IRequestHandler<CreateDepartmentInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department> _departmentRepository;
        public CreateDepartmentHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department> departmentRepository,
            ILocalizer localizer)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _departmentRepository = departmentRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(CreateDepartmentInputDTO command, CancellationToken cancellationToken)
        {
            // avoid duplicate
            var isDepartmentExist = _context.Departments.IgnoreQueryFilters() 
                    .Where(a => !a.IsDeleted && (a.Name_en == command.Name_en || a.Name_ar == command.Name_ar))
                    .OrderByDescending(a => a.Id).ToList();

            if (isDepartmentExist.Count > 0)
                throw new DomainException(_localizer[Keys.Messages.DepartmentExist]); 


            Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department department = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                if (command != null)
                {

                    department = _mapper.Map<Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department>(command);

                    _departmentRepository.Add(department);
                }
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return department.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
