﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Department.Commands.DTO
{
    public class DeleteDepartmentInputDTO : IRequest<int>
    {   
        public int Id { get; set; }
    }
}

