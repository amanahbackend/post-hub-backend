﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Department.Commands.DTO;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Amanah.Posthub.OrderService.Application.Features.Department.Commands
{

    public class DeleteDepartmentHandler : IRequestHandler<DeleteDepartmentInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department> _departmentRepository;


        public DeleteDepartmentHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department> departmentRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _departmentRepository = departmentRepository;
        }
        public async Task<int> Handle(DeleteDepartmentInputDTO command, CancellationToken cancellationToken)
        {
            Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department department = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                if (command.Id > 0)
                {

                    department = _context.Departments.IgnoreQueryFilters().Where(c => c.Id == command.Id).FirstOrDefault();
                    if (department != null)
                    {
                        department.IsDeleted = true;
                        _departmentRepository.Update(department);
                    }
                }
                await _unityOfWork.SaveChangesAsync();

            });
            if (result)
            {
                return department.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
