﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Department.Commands.DTO;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Department.Commands
{
    public class UpdateDepartmentHandler : IRequestHandler<UpdateDepartmentInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department> _departmentRepository;
        private readonly ILocalizer _localizer;

        public UpdateDepartmentHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department> departmentRepository,
            ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _departmentRepository = departmentRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(UpdateDepartmentInputDTO command, CancellationToken cancellationToken)
        {
            Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department department = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                if (command != null)
                {
                    var isDepartmentExist = _context.Departments.IgnoreQueryFilters()
                   .Where(a => !a.IsDeleted && a.Id != command.Id && (a.Name_en == command.Name_en || a.Name_ar == command.Name_ar))
                   .OrderByDescending(a => a.Id).ToList();
                    department = _mapper.Map<Amanah.Posthub.Service.Domain.CompanyProfile.Entities.Department>(command);

                    if (isDepartmentExist.Count == 0)
                    {
                        _departmentRepository.Update(department);
                        await _unityOfWork.SaveChangesAsync();

                    }
                    else
                    {
                        throw new DomainException(_localizer[Keys.Messages.DepartmentExist]);

                    }


                }
            });
            if (result)
            {
                return department.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
