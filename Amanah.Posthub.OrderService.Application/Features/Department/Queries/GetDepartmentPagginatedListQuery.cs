﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Department.Queries.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Department.Queries
{
    public class GetDepartmentPagginatedListQuery : PaginatedItemsViewModel, IRequest<PagedResult<DepartmentResponseDTO>>
    {
    }
  

    public class GetDepartmentPagginatedListQueryHandler : IRequestHandler<GetDepartmentPagginatedListQuery, PagedResult<DepartmentResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetDepartmentPagginatedListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PagedResult<DepartmentResponseDTO>> Handle(GetDepartmentPagginatedListQuery query, CancellationToken cancellationToken)
        {
            var departmentList = new PagedResult< DepartmentResponseDTO>();
           
                 departmentList = await _context
                            .Departments
                            .AsQueryable()
                            .AsNoTracking()
                            .IgnoreQueryFilters()
                            .Where(c => !c.IsDeleted)
                            .OrderBy(c => c.Id)
                            .Select(c=>new DepartmentResponseDTO { 
                                Id=c.Id,
                                IsActive=c.IsActive,
                                IsSystem=c.IsSystem,
                                Name_ar=c.Name_ar,
                                Name_en=c.Name_en,
                                FloorNo=c.FloorNo
                            })
                           // .ProjectTo<DepartmentResponseDTO>(_mapper.ConfigurationProvider)
                            .ToPagedResultAsync(query);
           
            if (departmentList == null)
            {
                return null;
            }
            return departmentList;
        }
    }
}
