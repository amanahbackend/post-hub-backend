﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.DriverRoles.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.DriverRoles.Commands
{

    public class CreateDriverRoleHandler : IRequestHandler<CreateDriverRoleInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<DriverRole> _driverRoleRepository;
        private readonly ILocalizer _localizer;

        public CreateDriverRoleHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<DriverRole> driverRoleRepository,
            ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _driverRoleRepository = driverRoleRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(CreateDriverRoleInputDTO command, CancellationToken cancellationToken)
        {
            // avoid duplicate

            DriverRole driverRole = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                if (command != null)
                {

                    driverRole = _mapper.Map<DriverRole>(command);
                    var driverRoleEF = _context.DriverRoles.Where(c => !c.IsDeleted && (c.Name_ar == command.Name_ar || c.Name_en == command.Name_en)).FirstOrDefault();
                    if (driverRoleEF == null)
                    {
                        _driverRoleRepository.Add(driverRole);
                        await _unityOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        throw new DomainException(_localizer[Keys.Messages.DriverRoleExist]);

                    }


                }
            });
            if (result)
            {
                return driverRole.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
