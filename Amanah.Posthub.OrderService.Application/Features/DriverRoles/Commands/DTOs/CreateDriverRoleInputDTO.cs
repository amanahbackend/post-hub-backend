﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.DriverRoles.Commands.DTOs
{
    public class CreateDriverRoleInputDTO : IRequest<int>
    {
       
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
    }
}
