﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.DriverRoles.Commands.DTOs
{
    public class DeleteDriverRoleInputDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
