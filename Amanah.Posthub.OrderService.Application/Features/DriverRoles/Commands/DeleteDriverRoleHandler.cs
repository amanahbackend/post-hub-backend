﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.DriverRoles.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.DriverRoles.Commands
{

    public class DeleteDriverRoleHandler : IRequestHandler<DeleteDriverRoleInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<DriverRole> _driverRoleRepository;
        public DeleteDriverRoleHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<DriverRole> driverRoleRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _driverRoleRepository = driverRoleRepository;
        }
        public async Task<int> Handle(DeleteDriverRoleInputDTO command, CancellationToken cancellationToken)
        {
            DriverRole driverRole = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                if (command != null)
                {

                    driverRole = _context.DriverRoles.Where(c => c.Id == command.Id).FirstOrDefault();
                    if (driverRole != null)
                    {
                        driverRole.IsDeleted = true;
                        _driverRoleRepository.Update(driverRole);

                    }
                }
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return driverRole.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
