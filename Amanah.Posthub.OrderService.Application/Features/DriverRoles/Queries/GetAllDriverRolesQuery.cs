﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.DriverRoles.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;
using Utilities.Extensions;
using Amanah.Posthub.OrderService.Application.DTO;

namespace Amanah.Posthub.OrderService.Application.Features.DriverRoles.Queries
{
   
    public class GetAllDriverRolesQuery : PaginatedItemsViewModel, IRequest<List<LookupResult<int>>>
    {
    }


    public class GetAllDriverRolesQueryHandler : IRequestHandler<GetAllDriverRolesQuery, List<LookupResult<int>>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllDriverRolesQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<LookupResult<int>>> Handle(GetAllDriverRolesQuery query, CancellationToken cancellationToken)
        {
            var DriverRoleList = new List<LookupResult<int>>();

            DriverRoleList = await _context
                       .DriverRoles
                       .AsQueryable()
                       .AsNoTracking()
                       .IgnoreQueryFilters()
                       .Where(c => !c.IsDeleted)
                       .OrderByDescending(c => c.Id)
                       .Select(c => new LookupResult<int>
                       {
                           Id = c.Id,
                           Name = Thread.CurrentThread.CurrentCulture.IsArabic() ? c.Name_ar: c.Name_en,
                       })
                       // .ProjectTo<DepartmentResponseDTO>(_mapper.ConfigurationProvider)
                       .ToListAsync();

            if (DriverRoleList == null)
            {
                return null;
            }
            return DriverRoleList;
        }
    }
}
