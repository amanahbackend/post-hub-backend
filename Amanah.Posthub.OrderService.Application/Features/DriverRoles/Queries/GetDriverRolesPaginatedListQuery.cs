﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.DriverRoles.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.DriverRoles.Queries
{
   
    public class GetDriverRolesPaginatedListQuery : PaginatedItemsViewModel, IRequest<PagedResult<DriverRoleResponseDTO>>
    {
    }


    public class GetDriverRolesPaginatedListQueryHandler : IRequestHandler<GetDriverRolesPaginatedListQuery, PagedResult<DriverRoleResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetDriverRolesPaginatedListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PagedResult<DriverRoleResponseDTO>> Handle(GetDriverRolesPaginatedListQuery query, CancellationToken cancellationToken)
        {
            var DriverRoleList = new PagedResult<DriverRoleResponseDTO>();

            DriverRoleList = await _context
                       .DriverRoles
                       .AsQueryable()
                       .AsNoTracking()
                       .IgnoreQueryFilters()
                       .Where(c => !c.IsDeleted)
                       .OrderBy(c => c.Id)
                       .Select(c => new DriverRoleResponseDTO
                       {
                           Id = c.Id,
                           Name_ar = c.Name_ar,
                           Name_en = c.Name_en
                       })
                       // .ProjectTo<DepartmentResponseDTO>(_mapper.ConfigurationProvider)
                       .ToPagedResultAsync(query);

            if (DriverRoleList == null)
            {
                return null;
            }
            return DriverRoleList;
        }
    }
}
