﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Commands.DTO
{
    public class DeleteInvoiceInputDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
