﻿using MediatR;
using System;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Commands.DTO
{
    public class UpdateInvoiceInputDTO : IRequest<int>
    {
        public int Id { get; set; }
        public DateTime ValidToDate { get; set; }
        public DateTime ValidFromDate { get; set; }
        public int? BusinessCustomerId { get; set; }
        public string ContractCode { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceTopic { get; set; }
        public int ContractId { get; set; }
        public int? NoOfItems { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalUnitsPrice { get; set; }
        public string InvoiceStatusName { get; set; }

    }
}
