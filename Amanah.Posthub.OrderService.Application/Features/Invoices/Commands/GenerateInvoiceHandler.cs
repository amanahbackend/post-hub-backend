﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Invoices.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Commands
{
    class GenerateInvoiceHandler : IRequestHandler<GenerateInvoiceInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Invoice> _invoiceRepository;


        public GenerateInvoiceHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Invoice> invoiceRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _invoiceRepository = invoiceRepository;
        }
        public async Task<int> Handle(GenerateInvoiceInputDTO command, CancellationToken cancellationToken)
        {
            Invoice invoice = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                invoice = _mapper.Map<Invoice>(command);
                invoice.InvoiceNo = generateInvCode();
                invoice.InvoiceStatus = InvoiceStatus.New;
                invoice.ServiceSectorId = 26;
                _invoiceRepository.Add(invoice);
                await _context.SaveChangesAsync();

            });
            if (result)
            {
                return invoice.Id;
            }
            else
            {
                return 0;
            }

        }
        private string generateInvCode()
        {
            string invCode = "";
            var lastInvoice = _context.Invoices.IgnoreQueryFilters().AsQueryable().OrderBy(x => x.Id).LastOrDefault();
            if (lastInvoice != null)
            {
                var oldInvNo = lastInvoice.InvoiceNo;
                string[] stringParts = oldInvNo.Split(new string[] { "Inv-000000" }, StringSplitOptions.None);
                var rank = int.Parse(stringParts[1]) + 1;
                invCode = "Inv-000000" + rank;
            }
            else
            {
                invCode = "Inv-000000" + "1";
            }
            return invCode;
        }

    }
}

