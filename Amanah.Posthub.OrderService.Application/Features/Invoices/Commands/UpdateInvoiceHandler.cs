﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Invoices.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Commands
{

    public class UpdateInvoiceHandler : IRequestHandler<UpdateInvoiceInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Invoice> _invoiceRepository;


        public UpdateInvoiceHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Invoice> invoiceRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _invoiceRepository = invoiceRepository;
        }
        public async Task<int> Handle(UpdateInvoiceInputDTO command, CancellationToken cancellationToken)
        {
            // Invoice invoice = null;          
            _unityOfWork.IsTenantFilterEnabled = false;
            var invoice = _context
                .Invoices
                .AsQueryable()
                .AsNoTracking()
                .IgnoreQueryFilters()
                .FirstOrDefault(x => x.Id == command.Id);
            if (invoice != null)
            {
                bool result = await _unityOfWork.RunTransaction(async () =>
                {
                    invoice.InvoicePeriodFrom = command.ValidFromDate;
                    invoice.InvoicePeriodTo = command.ValidToDate;
                    _invoiceRepository.Update(invoice);
                    await _unityOfWork.SaveChangesAsync();
                });
                if (result)
                {
                    return invoice.Id;
                }
                else
                {
                    return 0;
                }
            }
            return 0;
        }

    }
}

