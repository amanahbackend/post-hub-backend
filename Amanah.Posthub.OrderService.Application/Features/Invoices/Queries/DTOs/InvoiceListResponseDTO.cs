﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Queries.DTOs
{
    public class InvoiceListResponseDTO
    {
        public int Id { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceTopic { get; set; }
        public string ServiceSector { get; set; }
        public string InvoiceStatus { get; set; }
        public DateTime? InvoicePeriodFrom { get; set; }
        public DateTime? InvoicePeriodTo { get; set; }
        public string BusinessCustomerId { get; set; }
        public string BusinessCustomerName { get; set; }
        public string ContractCode { get; set; }
        public decimal TotalValue { get; set; }
    }
}
