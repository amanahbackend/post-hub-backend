﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Invoices.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Queries
{


    public class GetAllInvoicesPaginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<InvoiceListResponseDTO>>
    {
        public List<int> BusinessCustomerId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<int> sectorTypeId { get; set; }
        public List<int> statusId { get; set; }
    }
    public class GetAllInvoicesPagginatedQueryHandler : IRequestHandler<GetAllInvoicesPaginatedQuery, PagedResult<InvoiceListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllInvoicesPagginatedQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }

        public async Task<PagedResult<InvoiceListResponseDTO>> Handle(GetAllInvoicesPaginatedQuery filter, CancellationToken cancellationToken)
        {

            var query = _context
                 .Invoices.Where(c => !c.IsDeleted)
                 .AsQueryable()
                 .AsNoTracking();

            if (filter.BusinessCustomerId != null && filter.BusinessCustomerId.Count > 0)
            {
                query = query.Where(x => filter.BusinessCustomerId.Contains((int)x.Contract.BusinessCustomerId));
            }
            if (filter.FromDate != null)
            {
                query = query.Where(x => x.InvoicePeriodFrom.Value.Date == filter.FromDate.Value.Date);
            }
            if (filter.ToDate != null)
            {
                query = query.Where(x => x.InvoicePeriodTo.Value.Date == filter.ToDate.Value.Date);
            }
            if (filter.statusId != null && filter.statusId.Count > 0)
            {
                query = query.Where(x => filter.statusId.Contains((int)x.InvoiceStatus));
            }
            if (filter.sectorTypeId != null && filter.sectorTypeId.Count > 0)
            {
                query = query.Where(x => filter.sectorTypeId.Contains((int)x.ServiceSectorId));
            }
            var mapped = await query.OrderByDescending(a=>a.Id).IgnoreQueryFilters()
                .ProjectTo<InvoiceListResponseDTO>(_mapper.ConfigurationProvider)
                .ToPagedResultAsync(filter);
            if (mapped == null)
            {
                return null;
            }
            return mapped;

        }
    }
}
