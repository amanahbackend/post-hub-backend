﻿using Amanah.Posthub.Context;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Queries
{

    public class GetInvoicesCountQuery : IRequest<int>
    {

        public class GetInvoicesCountQueryHandler : IRequestHandler<GetInvoicesCountQuery, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetInvoicesCountQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }

            public async Task<int> Handle(GetInvoicesCountQuery filter, CancellationToken cancellationToken)
            {

                var invoicesCount = _context
                     .Invoices.Where(c => !c.IsDeleted)
                     .AsQueryable()
                     .IgnoreQueryFilters()
                     .AsNoTracking().Count();

                return invoicesCount;





            }
        }
    }
}
