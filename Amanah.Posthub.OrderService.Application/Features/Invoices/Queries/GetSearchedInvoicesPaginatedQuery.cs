﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Invoices.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Queries
{
    public class GetSearchedInvoicesPaginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<InvoiceListResponseDTO>>
    {
        public string SearchBy { get; set; }
    }
    public class GetSearchedInvoicesPaginatedQueryHandler : IRequestHandler<GetSearchedInvoicesPaginatedQuery, PagedResult<InvoiceListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetSearchedInvoicesPaginatedQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<InvoiceListResponseDTO>> Handle(GetSearchedInvoicesPaginatedQuery filter, CancellationToken cancellationToken)
        {

            var query = _context
                 .Invoices
                 .AsQueryable().AsNoTracking().IgnoreQueryFilters();
            if (!string.IsNullOrEmpty(filter.SearchBy))
            {

                query = query.Where(pl => pl.InvoiceNo.ToLower().Contains(filter.SearchBy.ToLower())
                     || pl.Contract.Code.ToLower().Contains(filter.SearchBy.ToLower())
                    || pl.Contract.BusinessCustomer.BusinessCode.ToLower().Contains(filter.SearchBy.ToLower())
                    || pl.Contract.BusinessCustomer.BusinessName.ToLower().Contains(filter.SearchBy.ToLower())
                    );
            }

            var rfqs = await query
                 .ProjectTo<InvoiceListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(filter);

            return rfqs;
        }
    }





}

