﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Invoices.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Invoices.Queries
{
    public class InvoiceDetailsQuery : IRequest<InvoiceDetailsResponseDTO>
    {
        public int Id { set; get; }
    }
    public class GetInvoiceDetailsQueryHandler : IRequestHandler<InvoiceDetailsQuery, InvoiceDetailsResponseDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetInvoiceDetailsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<InvoiceDetailsResponseDTO> Handle(InvoiceDetailsQuery query, CancellationToken cancellationToken)
        {

            var invoice = await _context
                 .Invoices
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.Id == query.Id)
                 .ProjectTo<InvoiceDetailsResponseDTO>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync();

            return invoice;
        }
    }





}

