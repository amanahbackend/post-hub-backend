﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.AddressTag.Commands
{

    public class ActivateAddressTagCommandHandler : IRequestHandler<ActiveAddressTagCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActivateAddressTagCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActiveAddressTagCommand command, CancellationToken cancellationToken)
        {
            var addressTags = _context.AddressTags.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (addressTags == null)
            {
                return default;
            }
            else
            {
                addressTags.IsActive = command.IsActive;
                _context.AddressTags.Update(addressTags);
                await _context.SaveChangesAsync();
                return addressTags.Id;
            }
        }
    }
}
