﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.AddressTag.Commands
{

    public class CreateAddressTagCommand : IRequestHandler<AddressTagCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateAddressTagCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(AddressTagCommand command, CancellationToken cancellationToken)
        {
            var addressTag = new Amanah.Posthub.Service.Domain.Entities.Lookups.AddressTag();

            addressTag.Name = command.Name;
            addressTag.IsDeleted = false;

            _context.AddressTags.Add(addressTag);
            await _context.SaveChangesAsync();
            return addressTag.Id;
        }
    }
}
