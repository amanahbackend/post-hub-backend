﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.AddressTag.Commands
{

    public class DeleteAddressTagCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteAddressTagCommandHandler : IRequestHandler<DeleteAddressTagCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteAddressTagCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteAddressTagCommand command, CancellationToken cancellationToken)
            {
                var addressTags = await _context.AddressTags.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (addressTags == null)
                {
                    return default;
                }
                addressTags.IsDeleted = true;
                _context.AddressTags.Update(addressTags);
                await _context.SaveChangesAsync();
                return addressTags.Id;
            }
        }
    }
}
