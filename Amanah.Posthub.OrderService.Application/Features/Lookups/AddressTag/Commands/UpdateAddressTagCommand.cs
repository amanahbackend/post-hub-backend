﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.AddressTag.Commands
{
    public class UpdateAddressTagCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public class UpdateAddressTagCommandHandler : IRequestHandler<UpdateAddressTagCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateAddressTagCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateAddressTagCommand command, CancellationToken cancellationToken)
            {
                var addressTags = _context.AddressTags.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (addressTags == null)
                {
                    return default;
                }
                else
                {
                    addressTags.Name = command.Name;
                    addressTags.IsActive = command.IsActive;

                    _context.AddressTags.Update(addressTags);
                    await _context.SaveChangesAsync();
                    return addressTags.Id;
                }
            }
        }
    }
}
