﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.AddressTag.Queries
{

    public class GetActiveAddressTagQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveAddressTagQueryHandler : IRequestHandler<GetActiveAddressTagQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveAddressTagQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveAddressTagQuery query, CancellationToken cancellationToken)
            {
                var addressTags = _context.AddressTags.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true).
                    Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (addressTags == null) return null;
                return addressTags;
            }
        }
    }
}
