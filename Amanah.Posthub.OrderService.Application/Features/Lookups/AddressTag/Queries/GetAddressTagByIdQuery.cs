﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.AddressTag.Queries
{

    public class GetAddressTagByIdQuery : IRequest<AddresstagDto>
    {
        public int Id { get; set; }
        public class GetAddressTagByIdQueryHandler : IRequestHandler<GetAddressTagByIdQuery, AddresstagDto>
        {
            private readonly ApplicationDbContext _context;
            public GetAddressTagByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<AddresstagDto> Handle(GetAddressTagByIdQuery query, CancellationToken cancellationToken)
            {
                var addressTag = _context.AddressTags.IgnoreQueryFilters().Where(a => a.Id == query.Id).Select(c => new AddresstagDto
                {
                    Id = c.Id,
                    Name = c.Name,
                    IsActive = c.IsActive,

                }).FirstOrDefault();
                if (addressTag == null) return null;
                return addressTag;
            }
        }
    }
}
