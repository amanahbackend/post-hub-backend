﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.AddressTag.Queries
{

    public class GetAddressTagListQuery : IRequest<IEnumerable<AddresstagDto>>
    {
        public class GetActiveAddressTagQueryHandler : IRequestHandler<GetAddressTagListQuery, IEnumerable<AddresstagDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveAddressTagQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<AddresstagDto>> Handle(GetAddressTagListQuery query, CancellationToken cancellationToken)
            {
                var addressTags = _context.AddressTags.IgnoreQueryFilters().Where(c => c.IsDeleted != true).
                    Select(c => new AddresstagDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem,

                    }).ToList();
                if (addressTags == null) return null;
                return addressTags;
            }
        }
    }
}
