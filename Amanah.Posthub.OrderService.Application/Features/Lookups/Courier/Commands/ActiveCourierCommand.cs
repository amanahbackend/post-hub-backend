﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Commands
{

    public class ActiveCourierCommandHandler : IRequestHandler<ActivateCourierCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveCourierCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateCourierCommand command, CancellationToken cancellationToken)
        {
            var courier = _context.Couriers.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (courier == null)
            {
                return default;
            }
            else
            {
                courier.IsActive = command.IsActive;
                _context.Couriers.Update(courier);
                await _context.SaveChangesAsync();
                return courier.Id;
            }
        }
    }
}
