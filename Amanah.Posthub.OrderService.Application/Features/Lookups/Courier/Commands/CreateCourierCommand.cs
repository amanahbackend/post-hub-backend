﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Commands
{

    public class CreateCourierCommand : IRequestHandler<CourierCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateCourierCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(CourierCommand command, CancellationToken cancellationToken)
        {
            var courier = new Amanah.Posthub.Service.Domain.Entities.Lookups.Courier();

            courier.Name = command.Name;
            courier.IsDeleted = false;

            _context.Couriers.Add(courier);
            await _context.SaveChangesAsync();
            return courier.Id;
        }
    }
}
