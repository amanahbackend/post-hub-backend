﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Commands
{

    public class DeleteCourierCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteCourierCommandHandler : IRequestHandler<DeleteCourierCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteCourierCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteCourierCommand command, CancellationToken cancellationToken)
            {
                var courier = await _context.Couriers.IgnoreQueryFilters().Where(a => a.Id == command.Id && !a.IsSystem).FirstOrDefaultAsync();
                if (courier == null)
                {
                    return default;
                }
                courier.IsDeleted = true;
                _context.Couriers.Update(courier);
                await _context.SaveChangesAsync();
                return courier.Id;
            }
        }
    }
}
