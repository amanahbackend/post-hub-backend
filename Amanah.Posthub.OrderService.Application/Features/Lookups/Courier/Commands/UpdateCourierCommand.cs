﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Commands
{
    public class UpdateCourierCommand : IRequest<int>
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public class UpdateCourierCommandHandler : IRequestHandler<UpdateCourierCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateCourierCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateCourierCommand command, CancellationToken cancellationToken)
            {
                var courier = _context.Couriers.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (courier == null)
                {
                    return default;
                }
                else
                {
                    courier.Name = command.Name;
                    courier.IsActive = command.IsActive;

                    _context.Couriers.Update(courier);
                    await _context.SaveChangesAsync();
                    return courier.Id;
                }
            }
        }
    }
}