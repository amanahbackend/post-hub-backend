﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Queries
{
    public class GetActiveCourierListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveCourierListQueryHandler : IRequestHandler<GetActiveCourierListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveCourierListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveCourierListQuery query, CancellationToken cancellationToken)
            {
                var couriers = _context.Couriers.IgnoreQueryFilters().Where(c => !c.IsDeleted
                && c.IsActive == true).
                    Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (couriers == null) return null;
                return couriers;
            }
        }
    }
}
