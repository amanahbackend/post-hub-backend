﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Queries
{

    public class GetCourierByIdQuery : IRequest<CourierDto>
    {
        public int Id { get; set; }
        public class GetCourierByIdQueryHandler : IRequestHandler<GetCourierByIdQuery, CourierDto>
        {
            private readonly ApplicationDbContext _context;
            public GetCourierByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<CourierDto> Handle(GetCourierByIdQuery query, CancellationToken cancellationToken)
            {
                var couriers = _context.Couriers.IgnoreQueryFilters().Where(a => a.Id == query.Id).
                    Select(c => new CourierDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,

                    }).FirstOrDefault();
                if (couriers == null) return null;
                return couriers;
            }
        }
    }
}
