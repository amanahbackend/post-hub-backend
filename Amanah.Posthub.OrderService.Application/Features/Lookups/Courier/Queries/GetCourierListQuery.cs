﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Courier.Queries
{

    public class GetCourierListQuery : IRequest<IEnumerable<CourierDto>>
    {
        public class GetCourierListQueryHandler : IRequestHandler<GetCourierListQuery, IEnumerable<CourierDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetCourierListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<CourierDto>> Handle(GetCourierListQuery query, CancellationToken cancellationToken)
            {
                var couriers = _context.Couriers.IgnoreQueryFilters().Where(c => c.IsDeleted != true).
                    Select(c => new CourierDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem
                    }).ToList();
                if (couriers == null) return null;
                return couriers;
            }
        }
    }
}
