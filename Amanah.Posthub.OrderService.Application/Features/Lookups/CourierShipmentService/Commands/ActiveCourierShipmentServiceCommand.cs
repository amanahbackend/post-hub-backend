﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierShipmentService.Commands
{

    public class ActiveCourierShipmentServiceCommandHandler : IRequestHandler<ActivateCourierShipmentServiceCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveCourierShipmentServiceCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateCourierShipmentServiceCommand command, CancellationToken cancellationToken)
        {
            var courierShipmentService = _context.CourierShipmentServices.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (courierShipmentService == null)
            {
                return default;
            }
            else
            {
                courierShipmentService.IsActive = command.IsActive;
                _context.CourierShipmentServices.Update(courierShipmentService);
                await _context.SaveChangesAsync();
                return courierShipmentService.Id;
            }
        }
    }
}
