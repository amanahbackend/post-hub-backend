﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierShipmentService.Commands
{

    public class CreateCourierShipmentServiceCommand : IRequestHandler<CourierShipmentServiceCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateCourierShipmentServiceCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(CourierShipmentServiceCommand command, CancellationToken cancellationToken)
        {
            var courierShipmentService = new Amanah.Posthub.Service.Domain.Entities.Lookups.CourierShipmentService();

            courierShipmentService.Name = command.Name;
            courierShipmentService.IsDeleted = false;

            _context.CourierShipmentServices.Add(courierShipmentService);
            await _context.SaveChangesAsync();
            return courierShipmentService.Id;
        }
    }
}
