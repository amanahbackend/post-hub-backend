﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierShipmentService.Commands
{

    public class DeleteCourierShipmentServiceCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteCourierShipmentServiceCommandHandler : IRequestHandler<DeleteCourierShipmentServiceCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteCourierShipmentServiceCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteCourierShipmentServiceCommand command, CancellationToken cancellationToken)
            {
                var courierShipmentService = await _context.CourierShipmentServices.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (courierShipmentService == null)
                {
                    return default;
                }
                courierShipmentService.IsDeleted = true;
                _context.CourierShipmentServices.Update(courierShipmentService);
                await _context.SaveChangesAsync();
                return courierShipmentService.Id;
            }
        }
    }
}
