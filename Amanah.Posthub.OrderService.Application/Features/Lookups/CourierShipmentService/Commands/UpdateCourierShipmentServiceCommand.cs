﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierShipmentService.Commands
{
    public class UpdateCourierShipmentServiceCommand : IRequest<int>
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public class UpdateCourierShipmentServiceCommandHandler : IRequestHandler<UpdateCourierShipmentServiceCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateCourierShipmentServiceCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateCourierShipmentServiceCommand command, CancellationToken cancellationToken)
            {
                var courierShipmentService = _context.CourierShipmentServices.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (courierShipmentService == null)
                {
                    return default;
                }
                else
                {
                    courierShipmentService.Name = command.Name;
                    courierShipmentService.IsActive = command.IsActive;

                    _context.CourierShipmentServices.Update(courierShipmentService);
                    await _context.SaveChangesAsync();
                    return courierShipmentService.Id;
                }
            }
        }
    }
}