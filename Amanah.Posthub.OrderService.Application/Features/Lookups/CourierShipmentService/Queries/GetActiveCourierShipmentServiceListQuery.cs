﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierShipmentService.Queries
{

    public class GetActiveCourierShipmentServiceListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveCourierShipmentServiceListQueryHandler : IRequestHandler<GetActiveCourierShipmentServiceListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveCourierShipmentServiceListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveCourierShipmentServiceListQuery query, CancellationToken cancellationToken)
            {
                var courierShipmentServiceList = _context.CourierShipmentServices.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true).
                    Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (courierShipmentServiceList == null) return null;
                return courierShipmentServiceList;
            }
        }
    }
}
