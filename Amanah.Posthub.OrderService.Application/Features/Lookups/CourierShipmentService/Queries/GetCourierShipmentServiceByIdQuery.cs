﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierShipmentService.Queries
{

    public class GetCourierShipmentServiceByIdQuery : IRequest<CourierShipmentServiceDto>
    {
        public int Id { get; set; }
        public class GetCourierShipmentServiceByIdQueryHandler : IRequestHandler<GetCourierShipmentServiceByIdQuery, CourierShipmentServiceDto>
        {
            private readonly ApplicationDbContext _context;
            public GetCourierShipmentServiceByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<CourierShipmentServiceDto> Handle(GetCourierShipmentServiceByIdQuery query, CancellationToken cancellationToken)
            {
                var courierShipmentService = _context.CourierShipmentServices.IgnoreQueryFilters().Where(a => a.Id == query.Id)
                    .Select(c => new CourierShipmentServiceDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,

                    }).FirstOrDefault();
                if (courierShipmentService == null) return null;
                return courierShipmentService;
            }
        }
    }
}
