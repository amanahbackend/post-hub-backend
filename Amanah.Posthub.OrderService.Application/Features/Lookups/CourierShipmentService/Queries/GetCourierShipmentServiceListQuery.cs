﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierShipmentService.Queries
{

    public class GetCourierShipmentServiceListQuery : IRequest<IEnumerable<CourierShipmentServiceDto>>
    {
        public class GetCourierShipmentServiceListQueryHandler : IRequestHandler<GetCourierShipmentServiceListQuery, IEnumerable<CourierShipmentServiceDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetCourierShipmentServiceListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<CourierShipmentServiceDto>> Handle(GetCourierShipmentServiceListQuery query, CancellationToken cancellationToken)
            {
                var courierShipmentServiceList = _context.CourierShipmentServices.IgnoreQueryFilters().Where(c => c.IsDeleted != true).
                    Select(c => new CourierShipmentServiceDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem,
                    }).ToList();
                if (courierShipmentServiceList == null) return null;
                return courierShipmentServiceList;
            }
        }
    }
}
