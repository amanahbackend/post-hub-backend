﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierZone.Commands
{

    public class ActiveCourierZoneCommandHandler : IRequestHandler<ActivateCourierZoneCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveCourierZoneCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateCourierZoneCommand command, CancellationToken cancellationToken)
        {
            var courierZone = _context.CourierZones.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (courierZone == null)
            {
                return default;
            }
            else
            {
                courierZone.IsActive = command.IsActive;
                _context.CourierZones.Update(courierZone);
                await _context.SaveChangesAsync();
                return courierZone.Id;
            }
        }
    }
}
