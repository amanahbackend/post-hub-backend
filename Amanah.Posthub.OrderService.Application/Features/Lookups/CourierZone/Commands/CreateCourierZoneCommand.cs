﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierZone.Commands
{

    public class CreateCourierZoneCommand : IRequestHandler<CourierZoneCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateCourierZoneCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(CourierZoneCommand command, CancellationToken cancellationToken)
        {
            var courier = new Amanah.Posthub.Service.Domain.Entities.Lookups.CourierZone();

            courier.Name = command.Name;
            courier.IsDeleted = false;
            courier.IsActive = command.IsActive;
            courier.CourierId = command.CourierId;

            _context.CourierZones.Add(courier);
            await _context.SaveChangesAsync();
            return courier.Id;
        }
    }
}
