﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierZone.Commands
{

    public class DeleteCourierZoneCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteCourierZoneCommandHandler : IRequestHandler<DeleteCourierZoneCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteCourierZoneCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteCourierZoneCommand command, CancellationToken cancellationToken)
            {
                var courierZone = await _context.CourierZones.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (courierZone == null)
                {
                    return default;
                }
                courierZone.IsDeleted = true;
                _context.CourierZones.Update(courierZone);
                await _context.SaveChangesAsync();
                return courierZone.Id;
            }
        }
    }
}
