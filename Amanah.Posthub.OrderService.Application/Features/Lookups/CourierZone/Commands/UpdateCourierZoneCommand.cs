﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierZone.Commands
{
    public class UpdateCourierZoneCommand : IRequest<int>
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int CourierId { get; set; }
        public class UpdateCourierZoneCommandHandler : IRequestHandler<UpdateCourierZoneCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateCourierZoneCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateCourierZoneCommand command, CancellationToken cancellationToken)
            {
                var courierZone = _context.CourierZones.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (courierZone == null)
                {
                    return default;
                }
                else
                {
                    courierZone.Name = command.Name;
                    courierZone.IsActive = command.IsActive;
                    courierZone.CourierId = command.CourierId;

                    _context.CourierZones.Update(courierZone);
                    await _context.SaveChangesAsync();
                    return courierZone.Id;
                }
            }
        }
    }
}
