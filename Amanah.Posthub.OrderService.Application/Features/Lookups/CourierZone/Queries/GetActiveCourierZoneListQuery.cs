﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierZone.Queries
{
    public class GetActiveCourierZoneListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveCourierZoneListQueryHandler : IRequestHandler<GetActiveCourierZoneListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveCourierZoneListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveCourierZoneListQuery query, CancellationToken cancellationToken)
            {
                var couriers = _context.CourierZones.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true).
                    Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (couriers == null) return null;
                return couriers;
            }
        }
    }
}
