﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierZone.Queries
{

    public class GetCourierZoneListQuery : IRequest<IEnumerable<CourierZoneDto>>
    {
        public class GetCourierZoneListQueryHandler : IRequestHandler<GetCourierZoneListQuery, IEnumerable<CourierZoneDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetCourierZoneListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<CourierZoneDto>> Handle(GetCourierZoneListQuery query, CancellationToken cancellationToken)
            {
                var couriers = _context.CourierZones.IgnoreQueryFilters().Where(c => c.IsDeleted != true).
                    Select(c => new CourierZoneDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        CourierName = c.Courier.Name,
                        IsSystem = c.IsSystem
                    }).ToList();
                if (couriers == null) return null;
                return couriers;
            }
        }
    }
}
