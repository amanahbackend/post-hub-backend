﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.CourierZone.Queries
{

    public class GetCourierZoneByIdQuery : IRequest<CourierZoneDto>
    {
        public int Id { get; set; }
        public class GetCourierByIdQueryHandler : IRequestHandler<GetCourierZoneByIdQuery, CourierZoneDto>
        {
            private readonly ApplicationDbContext _context;
            public GetCourierByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<CourierZoneDto> Handle(GetCourierZoneByIdQuery query, CancellationToken cancellationToken)
            {
                var couriers = _context.CourierZones.IgnoreQueryFilters().Where(a => a.Id == query.Id).
                    Select(c => new CourierZoneDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        CourierName = c.Courier.Name
                    }).FirstOrDefault();
                if (couriers == null) return null;
                return couriers;
            }
        }
    }
}
