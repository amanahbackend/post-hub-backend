﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Currency.Commands
{
    public class ActiveCurrencyCommand : IRequestHandler<ActivateCurrencyCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveCurrencyCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateCurrencyCommand command, CancellationToken cancellationToken)
        {
            var currency = _context.Currencies.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (currency == null)
            {
                return default;
            }
            else
            {
                currency.IsActive = command.IsActive;
                _context.Currencies.Update(currency);
                await _context.SaveChangesAsync();
                return currency.Id;
            }
        }
    }
}
