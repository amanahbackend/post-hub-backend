﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Currency.Commands
{

    public class CreateCurrencyCommandHandler : IRequestHandler<CurrencyCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateCurrencyCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(CurrencyCommand command, CancellationToken cancellationToken)
        {
            var currency = new Amanah.Posthub.Service.Domain.Entities.Lookups.Currency();



            currency.Code_en = command.Code_en;
            currency.Code_ar = command.Code_ar;
            currency.IsDeleted = false;
            currency.IsActive = command.IsActive;
            currency.Symbol = command.Symbol;

            _context.Currencies.Add(currency);
            await _context.SaveChangesAsync();
            return currency.Id;
        }
    }
}
