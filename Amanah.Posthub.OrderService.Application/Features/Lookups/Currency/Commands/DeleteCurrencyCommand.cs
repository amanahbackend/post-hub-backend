﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Currency.Commands
{
    public class DeleteCurrencyCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteCurrencyCommandHandler : IRequestHandler<DeleteCurrencyCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteCurrencyCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteCurrencyCommand command, CancellationToken cancellationToken)
            {
                var currency = await _context.Currencies.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (currency == null)
                {
                    return default;
                }
                currency.IsDeleted = true;
                _context.Currencies.Update(currency);
                await _context.SaveChangesAsync();
                return currency.Id;
            }
        }
    }
}
