﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Currency.Commands
{
    public class UpdateCurrencyCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Code_en { get; set; }
        public string Code_ar { get; set; }
        public bool IsActive { get; set; }
        public class UpdateCurrencyCommandHandler : IRequestHandler<UpdateCurrencyCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateCurrencyCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateCurrencyCommand command, CancellationToken cancellationToken)
            {
                var currency = _context.Currencies.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (currency == null)
                {
                    return default;
                }
                else
                {
                    currency.Code_en = command.Code_en;
                    currency.Code_ar = command.Code_ar;
                    currency.IsActive = command.IsActive;

                    _context.Currencies.Update(currency);
                    await _context.SaveChangesAsync();
                    return currency.Id;
                }
            }
        }
    }
}