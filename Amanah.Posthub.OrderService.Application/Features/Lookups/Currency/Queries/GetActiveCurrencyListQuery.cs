﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Currency.Queries
{
    public class GetActiveCurrencyListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveCurrencyListQueryHandler : IRequestHandler<GetActiveCurrencyListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveCurrencyListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveCurrencyListQuery query, CancellationToken cancellationToken)
            {
                var currencyList = _context.Currencies.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Code_en,
                        NameAr = c.Code_ar,
                    }).ToList();
                if (currencyList == null) return null;
                return currencyList;
            }
        }
    }
}
