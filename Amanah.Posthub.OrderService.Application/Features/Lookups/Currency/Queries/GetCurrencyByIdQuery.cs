﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Currency.Queries
{
    public class GetCurrencyByIdQuery : IRequest<CurrencyDto>
    {
        public int Id { get; set; }
        public class GetCurrencyByIdQueryHandler : IRequestHandler<GetCurrencyByIdQuery, CurrencyDto>
        {
            private readonly ApplicationDbContext _context;
            public GetCurrencyByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<CurrencyDto> Handle(GetCurrencyByIdQuery query, CancellationToken cancellationToken)
            {
                var currency = _context.Currencies.IgnoreQueryFilters().Where(a => a.Id == query.Id)
                    .Select(c => new CurrencyDto
                    {
                        Id = c.Id,
                        Code_ar = c.Code_ar,
                        Code_en = c.Code_en,
                        IsActive = c.IsActive,
                        Symbol = c.Symbol
                    }).FirstOrDefault();
                if (currency == null) return null;
                return currency;
            }
        }
    }
}
