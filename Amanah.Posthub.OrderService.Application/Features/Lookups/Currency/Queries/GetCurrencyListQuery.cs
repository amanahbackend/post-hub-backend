﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Currency.Queries
{
    public class GetCurrencyListQuery : IRequest<IEnumerable<CurrencyDto>>
    {
        public class GetCurrencyListQueryHandler : IRequestHandler<GetCurrencyListQuery, IEnumerable<CurrencyDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetCurrencyListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<CurrencyDto>> Handle(GetCurrencyListQuery query, CancellationToken cancellationToken)
            {
                var currencyList = _context.Currencies.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                    .Select(c => new CurrencyDto
                    {
                        Id = c.Id,
                        Code_ar = c.Code_ar,
                        Code_en = c.Code_en,
                        IsActive = c.IsActive,
                        Symbol = c.Symbol,
                        IsSystem = c.IsSystem,
                    }).ToList();
                if (currencyList == null) return null;
                return currencyList;
            }
        }
    }
}
