﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Commands
{
    public class ActivateGovernrateCommandHandler : IRequestHandler<ActivateGovernrateCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActivateGovernrateCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateGovernrateCommand command, CancellationToken cancellationToken)
        {
            var Governrate = _context.Governrates.Where(a => a.Id == command.Id).FirstOrDefault();

            if (Governrate == null)
            {
                return default;
            }
            else
            {
                Governrate.IsActive = command.IsActive;
                _context.Governrates.Update(Governrate);
                await _context.SaveChangesAsync();
                return Governrate.Id;
            }
        }
    }
}
