﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Commands
{
    public class CreateGovernrateCommand : IRequestHandler<GovernrateCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        public CreateGovernrateCommand(ApplicationDbContext context, ILocalizer localizer)
        {
            _context = context;
            _localizer = localizer;
        }
        public async Task<int> Handle(GovernrateCommand command, CancellationToken cancellationToken)
        {
            var governrate = new Amanah.Posthub.Service.Domain.Addresses.Entities.Governrate()
            {
                NameAR = command.NameAr,
                NameEN = command.NameEn,
                IsDeleted = false,
                IsActive = command.IsActive,
                CountryId = command.CountryId
            };

            // avoid duplicate
            var isGovernrateExist = _context.Governrates.IgnoreQueryFilters()
                    .Where(a => !a.IsDeleted && (a.NameEN == command.NameEn || a.NameAR == command.NameAr))
                    .OrderByDescending(a => a.Id).ToList();

            if (isGovernrateExist.Count == 0)
            {
                _context.Governrates.Add(governrate);
                await _context.SaveChangesAsync();
                return governrate.Id;
            }
            throw new DomainException(_localizer[Keys.Messages.GovernrateExist]); 
        }
    }
}
