﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Commands
{

    public class DeleteGovernrateCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteStateCommandHandler : IRequestHandler<DeleteGovernrateCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteStateCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteGovernrateCommand command, CancellationToken cancellationToken)
            {
                var state = await _context.Governrates.Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (state == null)
                {
                    return default;
                }
                state.IsDeleted = true;
                _context.Governrates.Update(state);
                await _context.SaveChangesAsync();
                return state.Id;
            }
        }
    }
}
