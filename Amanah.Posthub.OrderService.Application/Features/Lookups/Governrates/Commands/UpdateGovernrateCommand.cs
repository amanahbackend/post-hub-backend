﻿using Amanah.Posthub.Context;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Commands
{
    public class UpdateGovernrateCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public bool IsActive { get; set; }
        public class UpdateGovernrateCommandHandler : IRequestHandler<UpdateGovernrateCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateGovernrateCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateGovernrateCommand command, CancellationToken cancellationToken)
            {
                var Governrate = _context.Governrates.Where(a => a.Id == command.Id).FirstOrDefault();

                if (Governrate == null)
                {
                    return default;
                }
                else
                {
                    Governrate.NameEN = command.NameEn;
                    Governrate.NameAR = command.NameAr;
                    Governrate.IsActive = command.IsActive;
                    Governrate.CountryId = command.CountryId;
                    _context.Governrates.Update(Governrate);
                    await _context.SaveChangesAsync();
                    return Governrate.Id;
                }
            }
        }
    }
}