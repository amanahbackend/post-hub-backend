﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;


namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Governrates.Queries
{
   
    public class GetActiveGovernrateListByCountryIdQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public int? CountryId { get; set; }
        public class GetActiveGovernrateListByCountryIdQueryHandler : IRequestHandler<GetActiveGovernrateListByCountryIdQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveGovernrateListByCountryIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveGovernrateListByCountryIdQuery query, CancellationToken cancellationToken)
            {
                var governrateList = _context.Governrates.IgnoreQueryFilters().Where(c => ! c.IsDeleted  
                   && c.IsActive == true 
                   && c.CountryId==query.CountryId).
                    Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = Thread.CurrentThread.CurrentCulture.IsArabic() ? c.NameAR:c.NameEN,
                       

                    }).ToList();
                if (governrateList == null) return null;
                return governrateList;
            }
        }
    }
}
