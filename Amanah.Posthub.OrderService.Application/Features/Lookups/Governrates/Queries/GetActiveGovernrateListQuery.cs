﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Queries
{

    public class GetActiveGovernrateListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveStateListQueryHandler : IRequestHandler<GetActiveGovernrateListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveStateListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveGovernrateListQuery query, CancellationToken cancellationToken)
            {
                var governrateList = _context.Governrates.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true).
                    Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.NameEN,
                        NameAr = c.NameAR

                    }).ToList();
                if (governrateList == null) return null;
                return governrateList;
            }
        }
    }
}
