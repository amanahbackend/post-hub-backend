﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Queries
{

    public class GetGovernrateByIdQuery : IRequest<GovernrateDto>
    {
        public int Id { get; set; }
        public class GetGovernrateByIdQueryHandler : IRequestHandler<GetGovernrateByIdQuery, GovernrateDto>
        {
            private readonly ApplicationDbContext _context;
            public GetGovernrateByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<GovernrateDto> Handle(GetGovernrateByIdQuery query, CancellationToken cancellationToken)
            {
                var Governrate = _context.Governrates.IgnoreQueryFilters().Where(a => a.Id == query.Id).Select(c => new GovernrateDto
                {
                    Id = c.Id,
                    Name = c.NameEN,
                    NameAr = c.NameAR,
                    IsActive = c.IsActive,
                }).FirstOrDefault();

                if (Governrate == null) return null;
                return Governrate;
            }
        }
    }
}
