﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Governrate.Queries
{

    public class GetGovernrateListQuery : IRequest<IEnumerable<GovernrateDto>>
    {
        public class GetGovernrateListQueryHandler : IRequestHandler<GetGovernrateListQuery, IEnumerable<GovernrateDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetGovernrateListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<GovernrateDto>> Handle(GetGovernrateListQuery query, CancellationToken cancellationToken)
            {
                var Governrates = _context.Governrates.IgnoreQueryFilters().Where(c => c.IsDeleted != true).
                    Select(c => new GovernrateDto
                    {
                        Id = c.Id,
                        Name = c.NameEN,
                        NameAr = c.NameAR,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem,
                        CountryId = c.CountryId,
                        CountryName = c.Country != null ? c.Country.Name : "",

                    }).OrderBy(c => c.Id).ToList();
                if (Governrates == null) return null;
                return Governrates;
            }
        }
    }
}
