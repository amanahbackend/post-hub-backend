﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.Queries
{

    public class GetActiveItemReturnReasonListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveItemReturnReasonListQueryHandler : IRequestHandler<GetActiveItemReturnReasonListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveItemReturnReasonListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveItemReturnReasonListQuery query, CancellationToken cancellationToken)
            {
                var lengthUOMs = _context.ItemReturnReasons.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (lengthUOMs == null) return null;
                return lengthUOMs;
            }
        }
    }
}
