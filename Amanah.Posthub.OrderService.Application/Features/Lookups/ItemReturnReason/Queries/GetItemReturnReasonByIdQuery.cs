﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.Queries
{

    public class GetItemReturnReasonByIdQuery : IRequest<ItemReturnReasonDto>
    {
        public int Id { get; set; }
        public class GetItemReturnReasonByIdQueryHandler : IRequestHandler<GetItemReturnReasonByIdQuery, ItemReturnReasonDto>
        {
            private readonly ApplicationDbContext _context;
            public GetItemReturnReasonByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<ItemReturnReasonDto> Handle(GetItemReturnReasonByIdQuery query, CancellationToken cancellationToken)
            {
                var itemReturnReason = _context.ItemReturnReasons.IgnoreQueryFilters().Where(a => a.Id == query.Id)
                    .Select(c => new ItemReturnReasonDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,

                    }).FirstOrDefault();
                if (itemReturnReason == null) return null;
                return itemReturnReason;
            }
        }
    }
}
