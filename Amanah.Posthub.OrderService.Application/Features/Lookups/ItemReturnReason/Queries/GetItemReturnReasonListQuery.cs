﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.Queries
{

    public class GetItemReturnReasonListQuery : IRequest<IEnumerable<ItemReturnReasonDto>>
    {
        public class GetItemReturnReasonListQueryHandler : IRequestHandler<GetItemReturnReasonListQuery, IEnumerable<ItemReturnReasonDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetItemReturnReasonListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<ItemReturnReasonDto>> Handle(GetItemReturnReasonListQuery query, CancellationToken cancellationToken)
            {
                var itemReturnReasons = _context.ItemReturnReasons.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                    .Select(c => new ItemReturnReasonDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem
                    }).OrderBy(c => c.Id).ToList();
                if (itemReturnReasons == null) return null;
                return itemReturnReasons;
            }
        }
    }
}
