﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.commands
{

    public class ActiveItemReturnReasonCommand : IRequestHandler<ActivateItemReturnReasonCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveItemReturnReasonCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateItemReturnReasonCommand command, CancellationToken cancellationToken)
        {
            var itemReturnReason = _context.ItemReturnReasons.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (itemReturnReason == null)
            {
                return default;
            }
            else
            {
                itemReturnReason.IsActive = command.IsActive;
                _context.ItemReturnReasons.Update(itemReturnReason);
                await _context.SaveChangesAsync();
                return itemReturnReason.Id;
            }
        }
    }
}
