﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.commands
{
    class CreateItemReturnReasonCommand
    {
    }
    public class CreateItemReturnReasonCommandHandler : IRequestHandler<ItemReturnReasonCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        public CreateItemReturnReasonCommandHandler(ApplicationDbContext context, ILocalizer localizer)
        {
            _context = context;
            _localizer = localizer;
        }
        public async Task<int> Handle(ItemReturnReasonCommand command, CancellationToken cancellationToken)
        {
            var itemReturnReason = new Amanah.Posthub.Service.Domain.Entities.Lookups.ItemReturnReason();
            itemReturnReason.Name = command.Name;
            itemReturnReason.IsDeleted = false;
            itemReturnReason.IsActive = command.IsActive;

            // avoid duplicate
            var isReturnReasonExist = _context.ItemReturnReasons.IgnoreQueryFilters()
                    .Where(a => !a.IsDeleted && a.Name == command.Name)
                    .OrderByDescending(a => a.Id).ToList();

            if (isReturnReasonExist.Count == 0)
            {
                _context.ItemReturnReasons.Add(itemReturnReason);
                await _context.SaveChangesAsync();
                return itemReturnReason.Id;
            }
            throw new DomainException(_localizer[Keys.Messages.ItemReturnReasonExist]); 
        }
    }
}
