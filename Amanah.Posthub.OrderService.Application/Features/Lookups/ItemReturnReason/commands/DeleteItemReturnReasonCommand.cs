﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.commands
{

    public class DeleteItemReturnReasonCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteWeightUOMByIdCommandHandler : IRequestHandler<DeleteItemReturnReasonCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteWeightUOMByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteItemReturnReasonCommand command, CancellationToken cancellationToken)
            {
                var itemReturnReason = await _context.ItemReturnReasons.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (itemReturnReason == null)
                {
                    return default;
                }
                itemReturnReason.IsDeleted = true;
                _context.ItemReturnReasons.Update(itemReturnReason);
                await _context.SaveChangesAsync();
                return itemReturnReason.Id;
            }
        }
    }
}
