﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ItemReturnReason.commands
{
    public class UpdateItemReturnReasonCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public class UpdateItemReturnReasonCommandHandler : IRequestHandler<UpdateItemReturnReasonCommand, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly ILocalizer _localizer;

            public UpdateItemReturnReasonCommandHandler(ApplicationDbContext context, ILocalizer localizer)
            {
                _context = context;
                _localizer = localizer;
            }
            public async Task<int> Handle(UpdateItemReturnReasonCommand command, CancellationToken cancellationToken)
            {
                var itemReturnReason = _context.ItemReturnReasons.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (itemReturnReason == null)
                {
                    return default;
                }
                else
                {
                    var isReturnReasonExist = _context.ItemReturnReasons.IgnoreQueryFilters()
                   .Where(a => !a.IsDeleted && a.Id != command.Id && a.Name == command.Name)
                   .OrderByDescending(a => a.Id).ToList();

                    if (isReturnReasonExist.Count == 0)
                    {
                        itemReturnReason.Name = command.Name;
                        itemReturnReason.IsActive = command.IsActive;

                        _context.ItemReturnReasons.Update(itemReturnReason);
                        await _context.SaveChangesAsync();
                        return itemReturnReason.Id;
                    }
                    else
                    {
                        throw new DomainException(_localizer[Keys.Messages.ItemReturnReasonExist]);

                    }
                }
            }
        }
    }
}
