﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Commands
{
    public class ActiveLanguageCommandHandler : IRequestHandler<ActivateLanguageCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveLanguageCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateLanguageCommand command, CancellationToken cancellationToken)
        {
            var language = _context.Languages.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (language == null)
            {
                return default;
            }
            else
            {
                language.IsActive = command.IsActive;
                _context.Languages.Update(language);
                await _context.SaveChangesAsync();
                return language.Id;
            }
        }
    }
}
