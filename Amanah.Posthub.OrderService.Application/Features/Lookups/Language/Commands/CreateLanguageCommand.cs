﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Commands
{
    public class CreateLanguageCommandHandler : IRequestHandler<LanguageCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateLanguageCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(LanguageCommand command, CancellationToken cancellationToken)
        {
            var language = new Amanah.Posthub.Service.Domain.Entities.Lookups.Language();

            language.Name = command.Name;
            language.Code = command.Code;
            language.IsDeleted = false;
            language.IsActive = command.IsActive;

            _context.Languages.Add(language);
            await _context.SaveChangesAsync();
            return language.Id;
        }
    }
}
