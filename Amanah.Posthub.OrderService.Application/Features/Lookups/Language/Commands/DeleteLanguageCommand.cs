﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Commands
{

    public class DeleteLanguageCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteLanguageCommandHandler : IRequestHandler<DeleteLanguageCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteLanguageCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteLanguageCommand command, CancellationToken cancellationToken)
            {
                var language = await _context.Languages.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (language == null)
                {
                    return default;
                }
                language.IsDeleted = true;
                _context.Languages.Update(language);
                await _context.SaveChangesAsync();
                return language.Id;
            }
        }
    }
}
