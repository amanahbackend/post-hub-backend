﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Commands
{
    public class UpdateLanguageCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }

        public class UpdateLanguageCommandHandler : IRequestHandler<UpdateLanguageCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateLanguageCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateLanguageCommand command, CancellationToken cancellationToken)
            {
                var language = _context.Languages.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (language == null)
                {
                    return default;
                }
                else
                {
                    language.Code = command.Code;
                    language.Name = command.Name;
                    language.IsActive = command.IsActive;

                    _context.Languages.Update(language);
                    await _context.SaveChangesAsync();
                    return language.Id;
                }
            }
        }
    }
}
