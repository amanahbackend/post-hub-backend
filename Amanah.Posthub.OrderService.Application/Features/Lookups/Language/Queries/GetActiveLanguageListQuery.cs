﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Queries
{

    public class GetActiveLanguageListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveLanguageListQueryHandler : IRequestHandler<GetActiveLanguageListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveLanguageListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveLanguageListQuery query, CancellationToken cancellationToken)
            {
                var languageList = _context.Languages.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (languageList == null) return null;
                return languageList;
            }
        }
    }
}
