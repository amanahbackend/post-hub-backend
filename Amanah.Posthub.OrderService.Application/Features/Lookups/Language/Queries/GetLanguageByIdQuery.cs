﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Queries
{

    public class GetLanguageByIdQuery : IRequest<LanguageDto>
    {
        public int Id { get; set; }
        public class GetLanguageByIdQueryHandler : IRequestHandler<GetLanguageByIdQuery, LanguageDto>
        {
            private readonly ApplicationDbContext _context;
            public GetLanguageByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<LanguageDto> Handle(GetLanguageByIdQuery query, CancellationToken cancellationToken)
            {
                var language = _context.Languages.IgnoreQueryFilters().Where(a => a.Id == query.Id)
                    .Select(c => new LanguageDto
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Name = c.Name,
                        IsActive = c.IsActive,

                    }).FirstOrDefault();
                if (language == null) return null;
                return language;
            }
        }
    }
}
