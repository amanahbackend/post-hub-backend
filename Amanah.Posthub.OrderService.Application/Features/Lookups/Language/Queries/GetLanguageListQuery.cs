﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Language.Queries
{
    public class GetLanguageListQuery : IRequest<IEnumerable<LanguageDto>>
    {
        public class GetLanguageListQueryHandler : IRequestHandler<GetLanguageListQuery, IEnumerable<LanguageDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetLanguageListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LanguageDto>> Handle(GetLanguageListQuery query, CancellationToken cancellationToken)
            {
                var languageList = _context.Languages.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                    .Select(c => new LanguageDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Code = c.Code,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem
                    }).OrderBy(c => c.Id).ToList();
                if (languageList == null) return null;
                return languageList;
            }
        }
    }
}
