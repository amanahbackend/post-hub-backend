﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Commands
{

    public class ActiveLengthUOMCommand : IRequestHandler<ActivateLengthUOMCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveLengthUOMCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateLengthUOMCommand command, CancellationToken cancellationToken)
        {
            var lengthUOM = _context.LengthUOMs.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (lengthUOM == null)
            {
                return default;
            }
            else
            {
                lengthUOM.IsActive = command.IsActive;
                _context.LengthUOMs.Update(lengthUOM);
                await _context.SaveChangesAsync();
                return lengthUOM.Id;
            }
        }
    }
}
