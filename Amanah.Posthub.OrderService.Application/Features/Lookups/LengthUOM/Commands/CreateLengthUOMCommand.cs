﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Commands
{

    public class CreateWeightUOMCommandHandler : IRequestHandler<LengthUOMCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;

        public CreateWeightUOMCommandHandler(ApplicationDbContext context,ILocalizer localizer)
        {
            _context = context;
            _localizer = localizer;
        }
        public async Task<int> Handle(LengthUOMCommand command, CancellationToken cancellationToken)
        {
            var lengthUOM = new Amanah.Posthub.Service.Domain.Entities.Lookups.LengthUOM();
            lengthUOM.Name = command.Name;
            lengthUOM.Code = command.Code;
            lengthUOM.IsDeleted = false;
            lengthUOM.IsActive = command.IsActive;

            // avoid duplicate
            var isLengthExist = _context.LengthUOMs.IgnoreQueryFilters() 
                    .Where(a => !a.IsDeleted && (a.Name == command.Name || a.Code == command.Code))
                    .OrderByDescending(a => a.Id).ToList();

            if (isLengthExist.Count == 0)
            {
                _context.LengthUOMs.Add(lengthUOM);
                await _context.SaveChangesAsync();
                return lengthUOM.Id;
            }
            throw new DomainException(_localizer[Keys.Messages.LengthExist]);
        }
    }
}
