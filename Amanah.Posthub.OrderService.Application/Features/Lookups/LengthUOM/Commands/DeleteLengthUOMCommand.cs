﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Commands
{

    public class DeleteLengthUOMCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteWeightUOMByIdCommandHandler : IRequestHandler<DeleteLengthUOMCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteWeightUOMByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteLengthUOMCommand command, CancellationToken cancellationToken)
            {
                var lengthUOM = await _context.LengthUOMs.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (lengthUOM == null)
                {
                    return default;
                }
                lengthUOM.IsDeleted = true;
                _context.LengthUOMs.Update(lengthUOM);
                await _context.SaveChangesAsync();
                return lengthUOM.Id;
            }
        }
    }
}
