﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Commands
{
    public class UpdateLengthUOMCommand : IRequest<int>
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }

        public class UpdateLengthUOMCommandHandler : IRequestHandler<UpdateLengthUOMCommand, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly ILocalizer _localizer;

            public UpdateLengthUOMCommandHandler(ApplicationDbContext context, ILocalizer localizer)
            {
                _context = context;
                _localizer = localizer;
            }
            public async Task<int> Handle(UpdateLengthUOMCommand command, CancellationToken cancellationToken)
            {
                var lengthUOM = _context.LengthUOMs.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (lengthUOM == null)
                {
                    return default;
                }
                else
                {


                    var isLengthExist = _context.LengthUOMs.IgnoreQueryFilters()
                  .Where(a => !a.IsDeleted && a.Id != command.Id && (a.Name == command.Name || a.Code == command.Code))
                  .OrderByDescending(a => a.Id).ToList();

                    if (isLengthExist.Count == 0)
                    {
                        lengthUOM.Code = command.Code;
                        lengthUOM.Name = command.Name;
                        lengthUOM.IsActive = command.IsActive;

                        _context.LengthUOMs.Update(lengthUOM);
                        await _context.SaveChangesAsync();
                        return lengthUOM.Id;
                    }
                    throw new DomainException(_localizer[Keys.Messages.LengthExist]);
                }
            }
        }
    }
}
