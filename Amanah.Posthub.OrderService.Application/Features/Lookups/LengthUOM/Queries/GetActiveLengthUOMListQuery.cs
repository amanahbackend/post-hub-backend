﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Queries
{
    public class GetActiveLengthUOMListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveLengthUOMListQueryHandler : IRequestHandler<GetActiveLengthUOMListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveLengthUOMListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveLengthUOMListQuery query, CancellationToken cancellationToken)
            {
                var lengthUOMs = _context.LengthUOMs.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (lengthUOMs == null) return null;
                return lengthUOMs;
            }
        }
    }
}
