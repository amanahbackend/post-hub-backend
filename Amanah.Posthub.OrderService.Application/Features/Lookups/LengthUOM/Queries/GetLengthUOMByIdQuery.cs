﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Queries
{

    public class GetLengthUOMByIdQuery : IRequest<LengthUOMDto>
    {
        public int Id { get; set; }
        public class GetLengthUOMByIdQueryHandler : IRequestHandler<GetLengthUOMByIdQuery, LengthUOMDto>
        {
            private readonly ApplicationDbContext _context;
            public GetLengthUOMByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<LengthUOMDto> Handle(GetLengthUOMByIdQuery query, CancellationToken cancellationToken)
            {
                var weightUOM = _context.WeightUOMs.IgnoreQueryFilters().Where(a => a.Id == query.Id).Select(c => new LengthUOMDto
                {
                    Id = c.Id,
                    Code = c.Code,
                    Name = c.Name_en,
                    IsActive = c.IsActive,

                }).FirstOrDefault();
                if (weightUOM == null) return null;
                return weightUOM;
            }
        }
    }
}
