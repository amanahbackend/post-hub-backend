﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.LengthUOM.Queries
{
    public class GetLengthUOMListQuery : IRequest<IEnumerable<LengthUOMDto>>
    {
        public class GetLengthUOMListQueryHandler : IRequestHandler<GetLengthUOMListQuery, IEnumerable<LengthUOMDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetLengthUOMListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LengthUOMDto>> Handle(GetLengthUOMListQuery query, CancellationToken cancellationToken)
            {
                var lengthUOMs = _context.LengthUOMs.IgnoreQueryFilters().Where(c => !c.IsDeleted)
                    .Select(c => new LengthUOMDto
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem
                    }).OrderBy(c => c.Id).ToList();
                if (lengthUOMs == null) return null;
                return lengthUOMs;
            }
        }
    }
}
