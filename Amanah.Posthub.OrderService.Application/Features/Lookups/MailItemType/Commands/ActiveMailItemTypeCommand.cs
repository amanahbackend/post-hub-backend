﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Commands
{
    public class ActiveMailItemTypeCommand : IRequestHandler<ActivateMailItemTypeCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveMailItemTypeCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateMailItemTypeCommand command, CancellationToken cancellationToken)
        {
            var mailItemType = _context.MailItemTypes
                                .IgnoreQueryFilters()
                .Where(a => a.Id == command.Id).FirstOrDefault();

            if (mailItemType == null)
            {
                return default;
            }
            else
            {
                mailItemType.IsActive = command.IsActive;
                _context.MailItemTypes.Update(mailItemType);
                await _context.SaveChangesAsync();
                return mailItemType.Id;
            }
        }
    }
}
