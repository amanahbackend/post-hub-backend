﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Commands
{

    public class CreateMailItemTypeCommandHandler : IRequestHandler<MailItemTypeCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        public CreateMailItemTypeCommandHandler(ApplicationDbContext context, ILocalizer localizer)
        {
            _context = context;
            _localizer = localizer;
        }
        public async Task<int> Handle(MailItemTypeCommand command, CancellationToken cancellationToken)
        {

            var mailItemType = new Amanah.Posthub.Service.Domain.Entities.Lookups.MailItemType();
            mailItemType.Name_ar = command.Name_ar;
            mailItemType.Name_en = command.Name_en;
            mailItemType.IsDeleted = false;
            mailItemType.IsActive = command.IsActive;

            // avoid duplicate
            var isMailTypeExist = _context.MailItemTypes.IgnoreQueryFilters()
                    .Where(a => !a.IsDeleted && (a.Name_en == command.Name_en || a.Name_ar == command.Name_ar))
                    .OrderByDescending(a => a.Id).ToList();

            if (isMailTypeExist.Count == 0)
            {
                _context.MailItemTypes.Add(mailItemType);
                await _context.SaveChangesAsync();
                return mailItemType.Id;
            }
            throw new DomainException(_localizer[Keys.Messages.MailItemTypeExist]); 
        }
    }
}
