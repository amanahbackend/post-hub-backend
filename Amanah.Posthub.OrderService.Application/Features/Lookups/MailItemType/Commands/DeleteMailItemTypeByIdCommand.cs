﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Commands
{

    public class DeleteMailItemTypeByIdCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteWeightUOMByIdCommandHandler : IRequestHandler<DeleteMailItemTypeByIdCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteWeightUOMByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteMailItemTypeByIdCommand command, CancellationToken cancellationToken)
            {
                var mailItemTypes = await _context.MailItemTypes
                     .IgnoreQueryFilters()
                    .Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (mailItemTypes == null)
                {
                    return default;
                }
                mailItemTypes.IsDeleted = true;
                _context.MailItemTypes.Update(mailItemTypes);
                await _context.SaveChangesAsync();
                return mailItemTypes.Id;
            }
        }
    }
}
