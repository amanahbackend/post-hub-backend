﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Commands
{
    public class UpdateMailItemTypeCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsActive { get; set; }
        public class UpdateMailItemTypeCommandHandler : IRequestHandler<UpdateMailItemTypeCommand, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly ILocalizer _localizer;

            public UpdateMailItemTypeCommandHandler(ApplicationDbContext context, ILocalizer localizer)
            {
                _context = context;
                _localizer = localizer;
            }
            public async Task<int> Handle(UpdateMailItemTypeCommand command, CancellationToken cancellationToken)
            {
                var mailItemType = _context.MailItemTypes
                    .IgnoreQueryFilters()
                    .Where(a => a.Id == command.Id).FirstOrDefault();

                if (mailItemType == null)
                {
                    return default;
                }
                else
                {
                    var isMailTypeExist = _context.MailItemTypes.IgnoreQueryFilters()
                   .Where(a => !a.IsDeleted && a.Id != command.Id && (a.Name_en == command.Name_en || a.Name_ar == command.Name_ar))
                   .OrderByDescending(a => a.Id).ToList();

                    if (isMailTypeExist.Count == 0)
                    {
                        mailItemType.Name_en = command.Name_en;
                        mailItemType.Name_ar = command.Name_ar;

                        mailItemType.IsActive = command.IsActive;

                        _context.MailItemTypes.Update(mailItemType);
                        await _context.SaveChangesAsync();
                        return mailItemType.Id;
                    }
                    else
                    {
                        throw new DomainException(_localizer[Keys.Messages.MailItemTypeExist]);

                    }
                }
            }
        }
    }
}
