﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Queries
{

    public class GetActiveMailItemTypeListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveLengthUOMListQueryHandler : IRequestHandler<GetActiveMailItemTypeListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveLengthUOMListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveMailItemTypeListQuery query, CancellationToken cancellationToken)
            {
                var mailItemTypes = _context.MailItemTypes
                    .IgnoreQueryFilters()
                    .Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = Thread.CurrentThread.CurrentCulture.IsArabic() ? c.Name_ar : c.Name_en,

                        
                    }).ToList();
                if (mailItemTypes == null) return null;
                return mailItemTypes;
            }
        }
    }
}
