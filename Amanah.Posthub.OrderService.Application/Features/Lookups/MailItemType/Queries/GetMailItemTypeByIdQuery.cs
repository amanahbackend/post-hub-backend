﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Queries
{

    public class GetMailItemTypeByIdQuery : IRequest<MailItemTypeDto>
    {
        public int Id { get; set; }
        public class GetMailItemTypeByIdQueryHandler : IRequestHandler<GetMailItemTypeByIdQuery, MailItemTypeDto>
        {
            private readonly ApplicationDbContext _context;
            public GetMailItemTypeByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<MailItemTypeDto> Handle(GetMailItemTypeByIdQuery query, CancellationToken cancellationToken)
            {
                var mailItemType = _context.MailItemTypes
                    .IgnoreQueryFilters()
                    .Where(a => a.Id == query.Id)
                    .Select(c => new MailItemTypeDto
                    {
                        Id = c.Id,
                        Name_en = c.Name_en,
                        Name_ar=c.Name_ar,
                        IsActive = c.IsActive,

                    }).FirstOrDefault();
                if (mailItemType == null) return null;
                return mailItemType;
            }
        }
    }
}
