﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.MailItemType.Queries
{
    public class GetMailItemTypeListQuery : IRequest<IEnumerable<MailItemTypeDto>>
    {
        public class GetActiveLengthUOMListQueryHandler : IRequestHandler<GetMailItemTypeListQuery, IEnumerable<MailItemTypeDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveLengthUOMListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<MailItemTypeDto>> Handle(GetMailItemTypeListQuery query, CancellationToken cancellationToken)
            {
                var mailItemTypes = _context.MailItemTypes
                     .IgnoreQueryFilters()
                    .Where(c => c.IsDeleted != true)
                    .Select(c => new MailItemTypeDto
                    {
                        Id = c.Id,
                        Name_ar =   c.Name_ar,
                        Name_en= c.Name_en,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem
                    }).OrderBy(c => c.Id).ToList();
                if (mailItemTypes == null) return null;
                return mailItemTypes;
            }
        }
    }
}
