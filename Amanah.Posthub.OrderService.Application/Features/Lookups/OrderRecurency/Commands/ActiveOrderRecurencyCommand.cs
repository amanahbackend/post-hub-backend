﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Commands
{

    public class ActiveOrderRecurencyCommand : IRequestHandler<ActivateOrderRecurencyCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveOrderRecurencyCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateOrderRecurencyCommand command, CancellationToken cancellationToken)
        {
            var orderRecurency = _context.OrderRecurencies.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (orderRecurency == null)
            {
                return default;
            }
            else
            {
                orderRecurency.IsActive = command.IsActive;
                _context.OrderRecurencies.Update(orderRecurency);
                await _context.SaveChangesAsync();
                return orderRecurency.Id;
            }
        }
    }
}
