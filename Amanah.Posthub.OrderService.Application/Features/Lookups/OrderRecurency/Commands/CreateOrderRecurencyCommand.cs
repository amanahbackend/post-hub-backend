﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Commands
{

    public class CreateOrderRecurencyCommandHandler : IRequestHandler<OrderRecurencyCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateOrderRecurencyCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(OrderRecurencyCommand command, CancellationToken cancellationToken)
        {
            var orderRecurency = new Amanah.Posthub.Service.Domain.Entities.Lookups.OrderRecurency();



            orderRecurency.Name = command.Name;
            orderRecurency.Code = command.Code;
            orderRecurency.IsDeleted = false;
            orderRecurency.IsActive = command.IsActive;


            _context.OrderRecurencies.Add(orderRecurency);
            await _context.SaveChangesAsync();
            return orderRecurency.Id;
        }
    }
}
