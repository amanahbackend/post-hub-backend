﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Commands
{

    public class DeleteOrderRecurencyCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteOrderRecurencyCommandHandler : IRequestHandler<DeleteOrderRecurencyCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteOrderRecurencyCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteOrderRecurencyCommand command, CancellationToken cancellationToken)
            {
                var orderRecurency = await _context.OrderRecurencies.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (orderRecurency == null)
                {
                    return default;
                }
                orderRecurency.IsDeleted = true;
                _context.OrderRecurencies.Update(orderRecurency);
                await _context.SaveChangesAsync();
                return orderRecurency.Id;
            }
        }
    }
}
