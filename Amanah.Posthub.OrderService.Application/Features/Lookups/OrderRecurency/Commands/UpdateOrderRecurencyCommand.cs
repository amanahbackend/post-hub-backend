﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Commands
{
    public class UpdateOrderRecurencyCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }

        public class UpdateOrderRecurencyCommandHandler : IRequestHandler<UpdateOrderRecurencyCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateOrderRecurencyCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateOrderRecurencyCommand command, CancellationToken cancellationToken)
            {
                var orderRecurency = _context.OrderRecurencies.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (orderRecurency == null)
                {
                    return default;
                }
                else
                {
                    orderRecurency.Code = command.Code;
                    orderRecurency.Name = command.Name;
                    orderRecurency.IsActive = command.IsActive;

                    _context.OrderRecurencies.Update(orderRecurency);
                    await _context.SaveChangesAsync();
                    return orderRecurency.Id;
                }
            }
        }
    }
}