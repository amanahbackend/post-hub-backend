﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Queries
{

    public class GetActiveOrderRecurencyList : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveOrderRecurencyListHandler : IRequestHandler<GetActiveOrderRecurencyList, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveOrderRecurencyListHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveOrderRecurencyList query, CancellationToken cancellationToken)
            {
                var orderRecurency = _context.OrderRecurencies.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (orderRecurency == null) return null;
                return orderRecurency;
            }
        }
    }
}
