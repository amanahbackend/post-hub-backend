﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Queries
{
    public class GetOrderRecurencyByIdQuery : IRequest<OrderRecurencyDto>
    {
        public int Id { get; set; }
        public class GetorderRecurencyByIdQueryHandler : IRequestHandler<GetOrderRecurencyByIdQuery, OrderRecurencyDto>
        {
            private readonly ApplicationDbContext _context;
            public GetorderRecurencyByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<OrderRecurencyDto> Handle(GetOrderRecurencyByIdQuery query, CancellationToken cancellationToken)
            {
                var orderRecurency = _context.OrderRecurencies.IgnoreQueryFilters().Where(a => a.Id == query.Id).Select(c => new OrderRecurencyDto
                {
                    Id = c.Id,
                    Code = c.Code,
                    Name = c.Name,
                    IsActive = c.IsActive,

                }).FirstOrDefault();
                if (orderRecurency == null) return null;
                return orderRecurency;
            }
        }
    }
}
