﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderRecurency.Queries
{
    public class GetOrderRecurencyList : IRequest<IEnumerable<OrderRecurencyDto>>
    {
        public class GetOrderRecurencyListHandler : IRequestHandler<GetOrderRecurencyList, IEnumerable<OrderRecurencyDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetOrderRecurencyListHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<OrderRecurencyDto>> Handle(GetOrderRecurencyList query, CancellationToken cancellationToken)
            {
                var orderRecurency = _context.OrderRecurencies.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                    .Select(c => new OrderRecurencyDto
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem
                    }).ToList();
                if (orderRecurency == null) return null;
                return orderRecurency;
            }
        }
    }
}
