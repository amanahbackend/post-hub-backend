﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Commands
{

    public class ActiveOrderStatusCommand : IRequestHandler<ActivateOrderStatusCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveOrderStatusCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateOrderStatusCommand command, CancellationToken cancellationToken)
        {
            var orderStatus = _context.OrderStatuss.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (orderStatus == null)
            {
                return default;
            }
            else
            {
                orderStatus.IsActive = command.IsActive;
                _context.OrderStatuss.Update(orderStatus);
                await _context.SaveChangesAsync();
                return orderStatus.Id;
            }
        }
    }
}
