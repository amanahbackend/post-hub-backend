﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Commands
{
    public class CreateOrderStatusCommandHandler : IRequestHandler<OrderStatusCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateOrderStatusCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(OrderStatusCommand command, CancellationToken cancellationToken)
        {
            var orderStatus = new Amanah.Posthub.Service.Domain.Entities.Lookups.OrderStatus();

            orderStatus.Name = command.Name;
            orderStatus.Code = command.Code;
            orderStatus.IsDeleted = false;
            orderStatus.IsActive = command.IsActive;
            orderStatus.Description = command.Description;
            orderStatus.Rank = command.Rank;

            _context.OrderStatuss.Add(orderStatus);
            await _context.SaveChangesAsync();
            return orderStatus.Id;
        }
    }
}
