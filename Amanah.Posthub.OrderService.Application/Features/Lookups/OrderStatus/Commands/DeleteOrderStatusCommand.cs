﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Commands
{

    public class DeleteOrderStatusCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteOrderStatusCommandHandler : IRequestHandler<DeleteOrderStatusCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteOrderStatusCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteOrderStatusCommand command, CancellationToken cancellationToken)
            {
                var orderStatus = await _context.OrderStatuss.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (orderStatus == null)
                {
                    return default;
                }
                orderStatus.IsDeleted = true;
                _context.OrderStatuss.Update(orderStatus);
                await _context.SaveChangesAsync();
                return orderStatus.Id;
            }
        }
    }
}
