﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Commands
{
    public class UpdateOrderStatusCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Code { get; set; }
        public class UpdateOrderStatusCommandHandler : IRequestHandler<UpdateOrderStatusCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateOrderStatusCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateOrderStatusCommand command, CancellationToken cancellationToken)
            {
                var orderStatus = _context.OrderStatuss.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (orderStatus == null)
                {
                    return default;
                }
                else
                {
                    orderStatus.Description = command.Description;
                    orderStatus.Code = command.Code;
                    orderStatus.Name = command.Name;
                    orderStatus.IsActive = command.IsActive;

                    _context.OrderStatuss.Update(orderStatus);
                    await _context.SaveChangesAsync();
                    return orderStatus.Id;
                }
            }
        }
    }
}