﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Queries
{
    public class GetActiveOrderStatusQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveOrderStatusQueryHandler : IRequestHandler<GetActiveOrderStatusQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveOrderStatusQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveOrderStatusQuery query, CancellationToken cancellationToken)
            {
                var orderStatus = _context.OrderStatuss.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }).ToList();
                if (orderStatus == null) return null;
                return orderStatus;
            }
        }
    }
}
