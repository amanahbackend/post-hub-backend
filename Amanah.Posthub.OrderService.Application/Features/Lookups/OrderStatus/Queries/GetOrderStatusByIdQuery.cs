﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Queries
{
    public class GetOrderStatusByIdQuery : IRequest<OrderStatusDto>
    {
        public int Id { get; set; }
        public class GetOrderStatusByIdQueryHandler : IRequestHandler<GetOrderStatusByIdQuery, OrderStatusDto>
        {
            private readonly ApplicationDbContext _context;
            public GetOrderStatusByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<OrderStatusDto> Handle(GetOrderStatusByIdQuery query, CancellationToken cancellationToken)
            {
                var orderStatus = _context.OrderStatuss.IgnoreQueryFilters().Where(a => a.Id == query.Id)
                    .Select(c => new OrderStatusDto
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Name = c.Name,
                        Description = c.Description,
                        IsActive = c.IsActive,

                    }).FirstOrDefault();
                if (orderStatus == null) return null;
                return orderStatus;
            }
        }
    }
}
