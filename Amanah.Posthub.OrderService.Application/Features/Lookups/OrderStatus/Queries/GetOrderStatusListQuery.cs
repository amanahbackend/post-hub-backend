﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderStatus.Queries
{

    public class GetOrderStatusListQuery : IRequest<IEnumerable<OrderStatusDto>>
    {
        public class GetOrderStatusListQueryHandler : IRequestHandler<GetOrderStatusListQuery, IEnumerable<OrderStatusDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetOrderStatusListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<OrderStatusDto>> Handle(GetOrderStatusListQuery query, CancellationToken cancellationToken)
            {
                var orderStatus = _context.OrderStatuss.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                    .Select(c => new OrderStatusDto
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Name = c.Name,
                        Description = c.Description,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem
                    }).ToList();
                if (orderStatus == null) return null;
                return orderStatus;
            }
        }
    }
}
