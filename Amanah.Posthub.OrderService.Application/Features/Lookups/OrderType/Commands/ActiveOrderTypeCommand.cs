﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderType.Commands
{

    public class ActiveOrderTypeCommand : IRequestHandler<ActivateOrderTypeCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveOrderTypeCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateOrderTypeCommand command, CancellationToken cancellationToken)
        {
            var orderType = _context.OrderTypes.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

            if (orderType == null)
            {
                return default;
            }
            else
            {
                orderType.IsActive = command.IsActive;
                _context.OrderTypes.Update(orderType);
                await _context.SaveChangesAsync();
                return orderType.Id;
            }
        }
    }
}
