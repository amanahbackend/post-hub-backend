﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderType.Commands
{
    public class CreateOrderTypeCommandHandler : IRequestHandler<OrderTypeCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateOrderTypeCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(OrderTypeCommand command, CancellationToken cancellationToken)
        {
            var orderType = new Amanah.Posthub.Service.Domain.Entities.Lookups.OrderType();

            orderType.Name = command.Name;
            orderType.IsDeleted = false;
            orderType.IsActive = command.IsActive;

            _context.OrderTypes.Add(orderType);
            await _context.SaveChangesAsync();
            return orderType.Id;
        }
    }
}
