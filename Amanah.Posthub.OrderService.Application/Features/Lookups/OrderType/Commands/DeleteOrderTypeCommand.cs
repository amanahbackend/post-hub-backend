﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderType.Commands
{

    public class DeleteOrderTypeCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteOrderTypeCommandHandler : IRequestHandler<DeleteOrderTypeCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteOrderTypeCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteOrderTypeCommand command, CancellationToken cancellationToken)
            {
                var orderType = await _context.OrderTypes.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (orderType == null)
                {
                    return default;
                }
                orderType.IsDeleted = true;
                _context.OrderTypes.Update(orderType);
                await _context.SaveChangesAsync();
                return orderType.Id;
            }
        }
    }
}
