﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderType.Commands
{
    public class UpdateOrderTypeCommand : IRequest<int>
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public class UpdateOrderTypeCommandHandler : IRequestHandler<UpdateOrderTypeCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateOrderTypeCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateOrderTypeCommand command, CancellationToken cancellationToken)
            {
                var orderType = _context.OrderTypes.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefault();

                if (orderType == null)
                {
                    return default;
                }
                else
                {
                    orderType.Name = command.Name;
                    orderType.IsActive = command.IsActive;

                    _context.OrderTypes.Update(orderType);
                    await _context.SaveChangesAsync();
                    return orderType.Id;
                }
            }
        }
    }
}