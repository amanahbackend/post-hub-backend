﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderType.Queries
{

    public class GetActiveOrderTypeListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveOrderTypeListQueryHandler : IRequestHandler<GetActiveOrderTypeListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveOrderTypeListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveOrderTypeListQuery query, CancellationToken cancellationToken)
            {
                var orderTypeList = _context.OrderTypes.IgnoreQueryFilters().Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,
                    }).ToList();
                if (orderTypeList == null) return null;
                return orderTypeList;
            }
        }
    }
}
