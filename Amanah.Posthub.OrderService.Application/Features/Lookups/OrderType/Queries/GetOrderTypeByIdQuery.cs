﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderType.Queries
{
    public class GetOrderTypeByIdQuery : IRequest<OrderTypeDto>
    {
        public int Id { get; set; }
        public class GetOrderTypeByIdQueryHandler : IRequestHandler<GetOrderTypeByIdQuery, OrderTypeDto>
        {
            private readonly ApplicationDbContext _context;
            public GetOrderTypeByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<OrderTypeDto> Handle(GetOrderTypeByIdQuery query, CancellationToken cancellationToken)
            {
                var weightUOM = _context.OrderTypes.IgnoreQueryFilters().Where(a => a.Id == query.Id).Select(c => new OrderTypeDto
                {
                    Id = c.Id,
                    Name = c.Name,
                    IsActive = c.IsActive,

                }).FirstOrDefault();
                if (weightUOM == null) return null;
                return weightUOM;
            }
        }
    }
}
