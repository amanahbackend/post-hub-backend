﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.OrderType.Queries
{

    public class GetOrderTypeListQuery : IRequest<IEnumerable<OrderTypeDto>>
    {
        public class GetOrderTypeListQueryHandler : IRequestHandler<GetOrderTypeListQuery, IEnumerable<OrderTypeDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetOrderTypeListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<OrderTypeDto>> Handle(GetOrderTypeListQuery query, CancellationToken cancellationToken)
            {
                var orderTypeList = _context.OrderTypes.IgnoreQueryFilters().Where(c => c.IsDeleted != true)
                    .Select(c => new OrderTypeDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem
                    }).ToList();
                if (orderTypeList == null) return null;
                return orderTypeList;
            }
        }
    }
}
