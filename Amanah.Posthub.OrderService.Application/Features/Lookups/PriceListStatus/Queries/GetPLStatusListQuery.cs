﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.PriceListStatus.Queries
{
    public class GetPLStatusListQuery : IRequest<IEnumerable<PriceListStatusDto>>
    {
        public class GetPLStatusListQueryHandler : IRequestHandler<GetPLStatusListQuery, IEnumerable<PriceListStatusDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetPLStatusListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<PriceListStatusDto>> Handle(GetPLStatusListQuery query, CancellationToken cancellationToken)
            {
                var pqStatusList = _context.PriceListStatuses
                    .IgnoreQueryFilters()
                    .Where(c => c.IsDeleted != true)
                    .Select(c => new PriceListStatusDto
                    {
                        Id = c.Id,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem,
                    }).ToList();
                if (pqStatusList == null) return null;
                return pqStatusList;
            }
        }
    }
}
