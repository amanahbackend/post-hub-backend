﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.PriceQuotationStatus.Queries
{
    public class GetPQStatusListQuery : IRequest<IEnumerable<PriceQuotationStatusDto>>
    {
        public class GetPQStatusListQueryHandler : IRequestHandler<GetPQStatusListQuery, IEnumerable<PriceQuotationStatusDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetPQStatusListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<PriceQuotationStatusDto>> Handle(GetPQStatusListQuery query, CancellationToken cancellationToken)
            {
                var pqStatusList = _context.PriceQuotationStatuses
                    .IgnoreQueryFilters()
                    .Where(c => c.IsDeleted != true)
                    .Select(c => new PriceQuotationStatusDto
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Name = c.Name,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem,
                    }).ToList();
                if (pqStatusList == null) return null;
                return pqStatusList;
            }
        }
    }
}
