﻿using Amanah.Posthub.OrderService.Application.DTO.Commands;
using  Amanah.Posthub.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Role.Commands
{
    public class ActiveRoleCommand : IRequestHandler<ActivateRoleCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveRoleCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateRoleCommand command, CancellationToken cancellationToken)
        {
            var role = _context.Roles.Where(a => a.Id == command.Id).FirstOrDefault();
            if (role == null)
            {
                return default;
            }
            else
            {
                role.IsActive = command.IsActive;
                _context.Roles.Update(role);
                await _context.SaveChangesAsync();
                return role.Id;
            }
        }
    }
}
