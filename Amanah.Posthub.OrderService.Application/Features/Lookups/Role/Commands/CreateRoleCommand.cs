﻿using Amanah.Posthub.OrderService.Application.DTO.Commands;
using  Amanah.Posthub.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Role.Commands
{
  
    public class CreateRoleCommandHandler : IRequestHandler<RoleCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateRoleCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(RoleCommand command, CancellationToken cancellationToken)
        {
            var role = new Amanah.Posthub.Service.Domain.Entities.Lookups.Role();



            role.Name = command.Name;
            role.Code = command.Code;
            role.IsDeleted = false;
            role.IsActive = command.IsActive;
            role.Description = command.Description;


            _context.Roles.Add(role);
            await _context.SaveChangesAsync();
            return role.Id;
        }
    }
}
