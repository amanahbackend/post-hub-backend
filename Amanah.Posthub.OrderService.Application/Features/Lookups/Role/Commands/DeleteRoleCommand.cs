﻿using  Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Role.Commands
{

    public class DeleteRoleByIdCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteRoleByIdCommandHandler : IRequestHandler<DeleteRoleByIdCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteRoleByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteRoleByIdCommand command, CancellationToken cancellationToken)
            {
                var role = await _context.Roles.Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (role == null)
                {
                    return default;
                }
                role.IsDeleted = true;
                _context.Roles.Update(role);
                await _context.SaveChangesAsync();
                return role.Id;
            }
        }
    }
}
