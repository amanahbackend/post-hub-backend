﻿using Amanah.Posthub.OrderService.Application.DTO.Commands;
using  Amanah.Posthub.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Role.Commands
{
    public class UpdateRoleCommandHandler : IRequestHandler<RoleCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public UpdateRoleCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(RoleCommand command, CancellationToken cancellationToken)
        {
            var role = _context.Roles.Where(a => a.Id == command.Id).FirstOrDefault();

            if (role == null)
            {
                return default;
            }
            else
            {
                role.Description = command.Description;
                role.Code = command.Code;
                role.Name = command.Name;
                role.IsActive = command.IsActive;

                _context.Roles.Update(role);
                await _context.SaveChangesAsync();
                return role.Id;
            }
        }
    }
}
