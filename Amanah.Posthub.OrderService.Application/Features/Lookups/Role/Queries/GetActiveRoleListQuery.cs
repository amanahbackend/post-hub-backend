﻿using Amanah.Posthub.OrderService.Application.DTO;
using  Amanah.Posthub.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Role.Queries
{
    public class GetActiveRoleListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveroleListListQueryHandler : IRequestHandler<GetActiveRoleListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveroleListListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveRoleListQuery query, CancellationToken cancellationToken)
            {
                var roleList = _context.Roles.Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = c.Name,

                    }).ToList();
                if (roleList == null) return null;
                return roleList;
            }
        }
    }
}
