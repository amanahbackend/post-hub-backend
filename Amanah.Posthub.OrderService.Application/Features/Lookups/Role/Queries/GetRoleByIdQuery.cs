﻿using Amanah.Posthub.OrderService.Application.DTO;
using  Amanah.Posthub.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Role.Queries
{
    public class GetRoleByIdQuery : IRequest<RoleDto>
    {
        public int Id { get; set; }
        public class GetRoleByIdQueryHandler : IRequestHandler<GetRoleByIdQuery, RoleDto>
        {
            private readonly ApplicationDbContext _context;
            public GetRoleByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<RoleDto> Handle(GetRoleByIdQuery query, CancellationToken cancellationToken)
            {
                var role = _context.Roles.Where(a => a.Id == query.Id)
                    .Select(c => new RoleDto{
                    Id = c.Id,
                    Code = c.Code,
                    Name = c.Name,
                    Description = c.Description,
                    IsActive = c.IsActive,
                }).FirstOrDefault();
                if (role == null) return null;
                return role;
            }
        }
    }
}
