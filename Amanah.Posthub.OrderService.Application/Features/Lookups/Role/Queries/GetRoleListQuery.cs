﻿using Amanah.Posthub.OrderService.Application.DTO;
using  Amanah.Posthub.Context;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.Role.Queries
{
    public class GetRoleListQuery : IRequest<IEnumerable<RoleDto>>
    {
        public class GetActiveroleListListQueryHandler : IRequestHandler<GetRoleListQuery, IEnumerable<RoleDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveroleListListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<RoleDto>> Handle(GetRoleListQuery query, CancellationToken cancellationToken)
            {
                var roleList = _context.Roles.Where(c => c.IsDeleted != true )
                    .Select(c => new RoleDto
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Name = c.Name,
                        Description = c.Description,
                        IsActive = c.IsActive,
                    }).ToList();
                if (roleList == null) return null;
                return roleList;
            }
        }
    }
}
