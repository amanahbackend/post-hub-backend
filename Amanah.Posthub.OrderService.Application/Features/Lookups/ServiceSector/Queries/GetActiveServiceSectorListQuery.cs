﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ServiceSector.Queries
{

    public class GetActiveServiceSectorListQuery : IRequest<IEnumerable<object>>
    {
        public class GetActiveServiceSectorListQueryHandler : IRequestHandler<GetActiveServiceSectorListQuery, IEnumerable<object>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveServiceSectorListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<object>> Handle(GetActiveServiceSectorListQuery query, CancellationToken cancellationToken)
            {
                var serviceSectors = _context.ServiceSectors
                    .IgnoreQueryFilters()
                    .Where(c => !c.IsDeleted && c.IsActive == true)
                    .Select(c => new ServiceSectorDto
                    {
                        Id = c.Id,
                        Name = Thread.CurrentThread.CurrentCulture.IsArabic() ? c.Name_ar : c.Name_en,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem,
                    })
                    .ToList();
                if (serviceSectors == null) return null;
                return serviceSectors;
            }
        }
    }
}
