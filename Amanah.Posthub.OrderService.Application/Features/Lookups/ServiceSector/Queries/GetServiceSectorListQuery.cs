﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.ServiceSector.Queries
{
    public class GetServiceSectorListQuery : IRequest<IEnumerable<ServiceSectorDto>>
    {
        public class GetServiceSectorListQueryHandler : IRequestHandler<GetServiceSectorListQuery, IEnumerable<ServiceSectorDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetServiceSectorListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<ServiceSectorDto>> Handle(GetServiceSectorListQuery query, CancellationToken cancellationToken)
            {
                var serviceSectorList = _context.ServiceSectors
                    .IgnoreQueryFilters()
                    .Where(c => c.IsDeleted != true)
                    .Select(c => new ServiceSectorDto
                    {
                        Id = c.Id,
                        Name = c.Name_en,
                        IsActive = c.IsActive,
                        IsSystem = c.IsSystem,
                    }).ToList();
                if (serviceSectorList == null) return null;
                return serviceSectorList;
            }
        }
    }
}
