﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Commands
{
    public class CreateTermsAndConditionsCommandHandler : IRequestHandler<CreateTermsAndConditionsCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        private readonly IMapper _mapper;

        public CreateTermsAndConditionsCommandHandler(ApplicationDbContext context, ILocalizer localizer, IMapper mapper)
        {
            _context = context;
            _localizer = localizer;
            _mapper = mapper;
        }
        public async Task<int> Handle(CreateTermsAndConditionsCommand command, CancellationToken cancellationToken)
        {
            // avoid duplicate
            var isTermsAndConditionsExist = await _context.TermsAndConditionsLookups.AnyAsync(a => !a.IsDeleted
                                                       && (a.Title_Ar == command.Title_Ar || a.Title_En == command.Title_En));

            if (isTermsAndConditionsExist)
                throw new DomainException(_localizer[Keys.Messages.TermsAndConditionsExist]);

            try
            {
                var TermsAndConditions = _mapper.Map<TermsAndConditionsLookup>(command);
                _context.TermsAndConditionsLookups.Add(TermsAndConditions);
                await _context.SaveChangesAsync();
                return TermsAndConditions.Id;
            }
            catch (Exception ex)
            {
                throw new Exception($"error: {ex.Message}");
            }
        }
    }
}
