﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Commands.DTOs
{
    public class UpdateTermsAndConditionsCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Title_En { get; set; }
        public string Title_Ar { get; set; }
        public string TermsAndConditions_En { get; set; }
        public string TermsAndConditions_Ar { get; set; }
        public bool IsActive { get; set; }
    }
}
