﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Commands.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Commands
{
    public class DeleteTermsAndConditionsCommandHandler : IRequestHandler<DeleteTermsAndConditionsCommand, int>
    {
        private readonly ApplicationDbContext _context;

        public DeleteTermsAndConditionsCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(DeleteTermsAndConditionsCommand command, CancellationToken cancellationToken)
        {

            try
            {
                var termsAndConditions = await _context.TermsAndConditionsLookups.SingleOrDefaultAsync(a => a.Id == command.Id);

                termsAndConditions.IsDeleted = true;
                _context.TermsAndConditionsLookups.Update(termsAndConditions);
                await _context.SaveChangesAsync();
                return termsAndConditions.Id;
            }
            catch (Exception ex)
            {
                throw new Exception($"error: {ex.Message}");
            }

        }
    }
}
