﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Queries
{
    public class GetFirstTermsAndConditionsQuery : IRequest<TermsAndConditionsDto>
    { 
    }

    public class GetAllTermsAndConditionsQueryHandler : IRequestHandler<GetFirstTermsAndConditionsQuery, TermsAndConditionsDto>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllTermsAndConditionsQueryHandler( ApplicationDbContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<TermsAndConditionsDto> Handle(GetFirstTermsAndConditionsQuery query, CancellationToken cancellationToken)
        {
            try
            {
                var termsAndConditions = await _context.TermsAndConditionsLookups.AsNoTracking().FirstOrDefaultAsync(c => !c.IsDeleted);
                return _mapper.Map<TermsAndConditionsDto>(termsAndConditions);
            }
            catch (Exception ex)
            {
                throw new Exception($"error: {ex.Message}");
            }
        }
    }
}
