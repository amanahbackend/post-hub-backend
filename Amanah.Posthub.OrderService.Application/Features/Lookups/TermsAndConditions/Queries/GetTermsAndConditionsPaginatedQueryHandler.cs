﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.TermsAndConditions.Queries
{
    public class GetTermsAndConditionsPaginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<TermsAndConditionsDto>>
    {
    } 

    public class GetTermsAndConditionsPaginatedQueryHandler : IRequestHandler<GetTermsAndConditionsPaginatedQuery, PagedResult<TermsAndConditionsDto>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetTermsAndConditionsPaginatedQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PagedResult<TermsAndConditionsDto>> Handle(GetTermsAndConditionsPaginatedQuery query, CancellationToken cancellationToken)
        {
            try
            {
                return await _context.TermsAndConditionsLookups.AsNoTracking()
                               .Where(c => !c.IsDeleted).OrderBy(c => c.Id)
                               .ProjectTo<TermsAndConditionsDto>(_mapper.ConfigurationProvider)
                               .ToPagedResultAsync(query);
            }
            catch (Exception ex)
            {
                throw new Exception($"error: {ex.Message}");
            }
        }
    }
}
