﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.WeightUOM.Commands
{
    public class ActiveWeightUOMCommand : IRequestHandler<ActivateWeightUOMCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public ActiveWeightUOMCommand(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(ActivateWeightUOMCommand command, CancellationToken cancellationToken)
        {
            var wightUOM = _context.WeightUOMs
                .IgnoreQueryFilters()
                .Where(a => a.Id == command.Id).FirstOrDefault();

            if (wightUOM == null)
            {
                return default;
            }
            else
            {
                wightUOM.IsActive = command.IsActive;
                _context.WeightUOMs.Update(wightUOM);
                await _context.SaveChangesAsync();
                return wightUOM.Id;
            }
        }
    }
}
