﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO.Commands;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.WeightUOM.Commands
{
    public class CreateWeightUOMCommandHandler : IRequestHandler<WeightUOMCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly ILocalizer _localizer;
        public CreateWeightUOMCommandHandler(ApplicationDbContext context, ILocalizer localizer)
        {
            _context = context;
            _localizer = localizer;
        }
        public async Task<int> Handle(WeightUOMCommand command, CancellationToken cancellationToken)
        {
            var weightUOM = new Amanah.Posthub.Service.Domain.Entities.Lookups.WeightUOM();
            weightUOM.Name_en = command.Name_en;
            weightUOM.Name_ar = command.Name_ar;
            weightUOM.Code = command.Code;
            weightUOM.IsDeleted = false;
            weightUOM.IsActive = command.IsActive;
            weightUOM.Description = command.Description;

            // avoid duplicate
            var isWeightExist = _context.WeightUOMs.IgnoreQueryFilters()
                    .Where(a => !a.IsDeleted && (a.Name_en == command.Name_en || a.Name_ar == command.Name_ar || a.Code == command.Code))
                    .OrderByDescending(a => a.Id).ToList();

            if (isWeightExist.Count == 0)
            {
                _context.WeightUOMs.Add(weightUOM);
                await _context.SaveChangesAsync();
                return weightUOM.Id;
            }
            throw new DomainException(_localizer[Keys.Messages.WeightExist]);

        }
    }
}

