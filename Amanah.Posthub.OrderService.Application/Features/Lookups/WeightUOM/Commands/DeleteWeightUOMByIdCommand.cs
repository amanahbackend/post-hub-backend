﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.WeightUOM.Commands
{
    public class DeleteWeightUOMByIdCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteWeightUOMByIdCommandHandler : IRequestHandler<DeleteWeightUOMByIdCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteWeightUOMByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteWeightUOMByIdCommand command, CancellationToken cancellationToken)
            {
                var weightUOM = await _context.WeightUOMs
                    .IgnoreQueryFilters()
                    .Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (weightUOM == null)
                {
                    return default;
                }
                weightUOM.IsDeleted = true;
                _context.WeightUOMs.Update(weightUOM);
                await _context.SaveChangesAsync();
                return weightUOM.Id;
            }
        }
    }
}
