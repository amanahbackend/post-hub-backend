﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.WeightUOM.Commands
{
    public class UpdateWeightUOMCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool IsActive { get; set; }

        public class UpdateWeightUOMCommandHandler : IRequestHandler<UpdateWeightUOMCommand, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly ILocalizer _localizer;

            public UpdateWeightUOMCommandHandler(ApplicationDbContext context, ILocalizer localizer)
            {
                _context = context;
                _localizer = localizer;
            }
            public async Task<int> Handle(UpdateWeightUOMCommand command, CancellationToken cancellationToken)
            {
                var wightUOM = _context.WeightUOMs
                    .IgnoreQueryFilters()
                  .Where(a => a.Id == command.Id).FirstOrDefault();

                if (wightUOM == null)
                {
                    return default;
                }
                else
                {

                    // avoid duplicate
                    var isWeightExist = _context.WeightUOMs.IgnoreQueryFilters()
                            .Where(a => !a.IsDeleted && a.Id != command.Id && (a.Name_en == command.Name_en || a.Name_ar == command.Name_ar || a.Code == command.Code))
                            .OrderByDescending(a => a.Id).ToList();

                    if (isWeightExist.Count == 0)
                    {
                        wightUOM.Description = command.Description;
                        wightUOM.Code = command.Code;
                        wightUOM.Name_ar = command.Name_ar;
                        wightUOM.Name_en = command.Name_en;
                        wightUOM.IsActive = command.IsActive;

                        _context.WeightUOMs.Update(wightUOM);
                        await _context.SaveChangesAsync();
                        return wightUOM.Id;
                    }
                    throw new DomainException(_localizer[Keys.Messages.WeightExist]);
                }
            }
        }
    }
}