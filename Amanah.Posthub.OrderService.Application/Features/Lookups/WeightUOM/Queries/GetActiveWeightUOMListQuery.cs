﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.WeightUOM.Queries
{
    public class GetActiveWeightUOMListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveWeightUOMListQueryHandler : IRequestHandler<GetActiveWeightUOMListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveWeightUOMListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveWeightUOMListQuery query, CancellationToken cancellationToken)
            {
                var weightUOM = _context.WeightUOMs
                    .IgnoreQueryFilters()
                    .Where(c => c.IsDeleted != true && c.IsActive == true)
                    .Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = Thread.CurrentThread.CurrentCulture.IsArabic() ? c.Name_ar : c.Name_en,


                    }).ToList();
                if (weightUOM == null) return null;
                return weightUOM;
            }
        }
    }
}
