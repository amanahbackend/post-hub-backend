﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Lookups.WeightUOM.Queries
{
    public class GetWeightUOMListQuery : IRequest<IEnumerable<WeightUOMDto>>
    {
        public class GetAllWeightUMsQueryHandler : IRequestHandler<GetWeightUOMListQuery, IEnumerable<WeightUOMDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetAllWeightUMsQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<WeightUOMDto>> Handle(GetWeightUOMListQuery query, CancellationToken cancellationToken)
            {
                var weightUOM = _context.WeightUOMs
                    .IgnoreQueryFilters()      
                    .Where(c => c.IsDeleted != true).Select(c => new WeightUOMDto
                {
                    Id = c.Id,
                    Code = c.Code,
                    Name_ar = c.Name_ar,
                    Name_en = c.Name_en,
                    Description = c.Description,
                    IsActive = c.IsActive,
                    IsSystem = c.IsSystem
                }).OrderBy(c => c.Id).ToList();
                if (weightUOM == null) return null;
                return weightUOM;
            }
        }
    }
}
