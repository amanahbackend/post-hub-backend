﻿using Amanah.Posthub.Service.Domain.Entities;
using System;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.OrderService.Application.Features.MailItemStatusLogs.Queries.DTOs
{
    public class TrackMailItemStatusLogDto
    {
        public int MailItemStatusId { get; set; }
        public string MailItemStatusName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? OrderId { get; set; }
    }
}
