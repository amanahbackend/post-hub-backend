﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.MailItemStatusLogs.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Amanah.Posthub.OrderService.Application.Features.MailItemStatusLogs.Queries.Express
{
    public class GetMailItemStatusLogsByItemCodeForMobileExpressQuery : IRequest<Result>
    {
        public string Code { set; get; }
    }
    public class GetMailItemStatusLogsByItemCodeForMobileExpressQueryHandler : IRequestHandler<GetMailItemStatusLogsByItemCodeForMobileExpressQuery, Result>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetMailItemStatusLogsByItemCodeForMobileExpressQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;

        }
        public async Task<Result> Handle(GetMailItemStatusLogsByItemCodeForMobileExpressQuery query, CancellationToken cancellationToken)
        {
            var result = new Result();

            result.TotalCount = await _context
                .MailItemsStatusLogs
                .IgnoreQueryFilters()
                .Where(c => c.MailItem.MailItemBarCode == query.Code
                 && !c.IsDeleted
                 && (!_currentUser.IsBusinessCustomer()
                 || (c.MailItem.Order.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId))).CountAsync();

            if (result.TotalCount < 1)
            {
                result.Error = "Code Not Found";
                return result;
            }

            result.Value = await _context
                 .MailItemsStatusLogs
                 .IgnoreQueryFilters()
                 .AsQueryable()
                 .AsNoTracking()
                 .Where(c => c.MailItem.MailItemBarCode == query.Code
                 && !c.IsDeleted
                 && (!_currentUser.IsBusinessCustomer()
                           || (c.MailItem.Order.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)))
                 .ProjectTo<TrackMailItemStatusLogDto>(_mapper.ConfigurationProvider).ToListAsync();

            return result;
        }
    }
}

