﻿
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.MailItemStatusLogs.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class GetMailItemStatusLogsByItemCodeQuery : IRequest<List<TrackMailItemStatusLogDto>>
    {
        public string Code { set; get; }
    }
    public class GetMailItemStatusLogsByItemCodeQueryHandler : IRequestHandler<GetMailItemStatusLogsByItemCodeQuery, List<TrackMailItemStatusLogDto>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetMailItemStatusLogsByItemCodeQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;

        }
        public async Task<List<TrackMailItemStatusLogDto>> Handle(GetMailItemStatusLogsByItemCodeQuery query, CancellationToken cancellationToken)
        {
            var mailItemStatusLogs = await _context
                 .MailItemsStatusLogs
                 .Include(m=>m.MailItem)
                 .ThenInclude(m=>m.Status)
                 .Include(m => m.MailItem)
                 .ThenInclude(m => m.DropOffAddress)
                 .IgnoreQueryFilters()
                 .AsQueryable()
                 .AsNoTracking()
                 .Where(c => c.MailItem.MailItemBarCode == query.Code
                 && !c.IsDeleted && !c.MailItem.IsDeleted
                 &&
                      (!_currentUser.IsBusinessCustomer()
                           || (c.MailItem.Order.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)
                      )
                 )
                 .ProjectTo<TrackMailItemStatusLogDto>(_mapper.ConfigurationProvider).ToListAsync();

            #region refactor
            var OrderId = await _context.MailItems.IgnoreQueryFilters()
                 .AsNoTracking().Where(m => m.MailItemBarCode == query.Code).FirstOrDefaultAsync();
            foreach (var item in mailItemStatusLogs)
            {
                item.OrderId = OrderId.OrderId;
            }
            #endregion
            return mailItemStatusLogs;
        }
    }
}

