﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using MediatR;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Commands
{
    public class CreateMailItemCommand : IRequest<int>
    {
        //private readonly IUnitOfWork _UnitOfWork;
        //private readonly IMapper _mapper;
        #region Fileds

        public string MailItemBarCode { get; set; }

        public DateTime DeliveryBefore { get; set; }

        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address PickUpAddress { get; set; }
        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address DropOffAddress { get; set; }

        public int ConsigneeInfoId { get; set; }
        public Contact ConsigneeInfo { get; set; }
        public int ReciverInfoId { get; set; }
        public Contact ReciverInfo { get; set; }
        public bool IsMatchConsigneeId { get; set; }

        public AttachmentFile ProofOfDeliveryImage { get; set; }
        public AttachmentFile SignatureImage { get; set; }

        public int Weight { get; set; }
        public int? WeightUOMId { get; set; }
        public int? LengthUOMId { get; set; }

        public int Hight { get; set; }
        public int Width { get; set; }

        public string ReservedSpecial { get; set; }
        public string Notes { get; set; }

        public int ItemTypeId { get; set; }
        public int StatusId { get; set; }
        public MailItemStatus Status { get; set; }

        // Ramzy: need to be checked
        public string UserId { get; set; }

        #endregion

        public class CreateMailItemCommandHandler : IRequestHandler<CreateMailItemCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public CreateMailItemCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(CreateMailItemCommand command, CancellationToken cancellationToken)
            {
                var mailItem = new MailItem();

                mailItem.MailItemsStatusLog.Add(new MailItemsStatusLog()
                {
                    CreatedBy = command.UserId,
                    CreationTime = DateTime.UtcNow,
                    IsDeleted = false,
                    MailItemStatus = command.Status
                });

                mailItem.MailItemBarCode = command.MailItemBarCode;
                mailItem.DeliveryBefore = command.DeliveryBefore;
                mailItem.PickUpAddress = command.PickUpAddress;
                mailItem.DropOffAddress = command.DropOffAddress;
                mailItem.ConsigneeInfoId = command.ConsigneeInfoId;
                mailItem.ConsigneeInfo = command.ConsigneeInfo;
                mailItem.ReciverInfoId = command.ReciverInfoId;
                mailItem.ReciverInfo = command.ReciverInfo;
                mailItem.IsMatchConsigneeID = command.IsMatchConsigneeId;
                // mailItem.ProofOfDeliveryImage = command.ProofOfDeliveryImage;
                //  mailItem.SignatureImage = command.SignatureImage;
                mailItem.Weight = command.Weight;
                mailItem.WeightUOMId = command.WeightUOMId;
                mailItem.LengthUOMId = command.WeightUOMId;
                mailItem.Hight = command.Hight;
                mailItem.Width = command.Width;
                mailItem.ReservedSpecial = command.ReservedSpecial;
                mailItem.Notes = command.Notes;
                mailItem.ItemTypeId = command.ItemTypeId;
                mailItem.StatusId = command.StatusId;
                mailItem.Status = command.Status;

                int itemId= _context.MailItems.Select(x => x.Id).OrderBy(x => x).LastOrDefault();
                if (string.IsNullOrEmpty(mailItem.MailItemBarCode))
                {
                    itemId += 1;
                    mailItem.MailItemBarCode = GenerateItemCodeAsync(itemId );
                }
                //  mailItem.CreatedBy = command.UserId;
                //  mailItem.CreationTime = DateTime.UtcNow;
                mailItem.IsDeleted = false;

                _context.MailItems.Add(mailItem);
                await _context.SaveChangesAsync();
                return mailItem.Id;
            }

            private string GenerateItemCodeAsync(int ItemId)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(ItemId):00000000}");
                return sb.ToString();
            }
        }


     
    }
}
