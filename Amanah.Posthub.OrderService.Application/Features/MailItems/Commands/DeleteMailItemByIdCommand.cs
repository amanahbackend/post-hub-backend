﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Commands
{
    public class DeleteMailItemByIdCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteMailItemByIdCommandHandler : IRequestHandler<DeleteMailItemByIdCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteMailItemByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteMailItemByIdCommand command, CancellationToken cancellationToken)
            {
                var order = await _context.MailItems.IgnoreQueryFilters().Where(a => a.Id == command.Id).FirstOrDefaultAsync();
                if (order == null) return default;
                order.IsDeleted = true;
                await _context.SaveChangesAsync();
                return order.Id;
            }
        }
    }
}
