﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Commands
{
    public class UpdateMailItemCommand : IRequest<int>
    {
        #region Fileds
        public int Id { get; set; }
        public string MailItemBarCode { get; set; }
         
        //  public DateTime DeliveryBefore { get; set; }
 
        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address PickUpAddress { get; set; }
        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address DropOffAddress { get; set; }

        public int ConsigneeInfoId { get; set; }
       // public Contact ConsigneeInfo { get; set; }
        public string ConsigneeInformation { get; set; }
        public string Mobile { get; set; }

        //public int ReciverInfoId { get; set; }
        //public Contact ReciverInfo { get; set; }
        //public bool IsMatchConsigneeId { get; set; }

        //public AttachmentFile ProofOfDeliveryImage { get; set; }
        //        public AttachmentFile SignatureImage { get; set; }

        public int? Weight { get; set; }
        public int? WeightUOMId { get; set; }
        public int? LengthUOMId { get; set; }

        public int? Hight { get; set; }
        public int? Width { get; set; }

        //public string ReservedSpecial { get; set; }
        //public string Notes { get; set; }

        public int ItemTypeId { get; set; }
      //  public int StatusId { get; set; }
        //public MailItemStatus Status { get; set; }

        // Ramzy: need to be checked
        //public string UserId { get; set; }

        #endregion
        public class UpdateMailItemCommandHandler : IRequestHandler<UpdateMailItemCommand, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly ICurrentUser _currentUser;

            public UpdateMailItemCommandHandler(
                ApplicationDbContext context,
                ICurrentUser currentUser)
            {
                _context = context;
                _currentUser = currentUser;

            }
            public async Task<int> Handle(UpdateMailItemCommand command, CancellationToken cancellationToken)
            {
                var mailItem = _context.MailItems
                    .Include(x=>x.ConsigneeInfo)
                    .IgnoreQueryFilters()
                    .Where(a => a.Id == command.Id).FirstOrDefault();

                if (mailItem == null)
                {
                    return default;
                }
                else
                {
                    //mailItem.MailItemsStatusLog.Add(new MailItemsStatusLog()
                    //{
                    //    CreatedBy = _currentUser.Id,
                    //    CreationTime = DateTime.UtcNow,
                    //    IsDeleted = false,
                    //    MailItemStatusId = command.StatusId
                    //});

                    mailItem.MailItemBarCode = command.MailItemBarCode;
                    mailItem.PickUpAddress = command.PickUpAddress;
                    mailItem.DropOffAddress = command.DropOffAddress;
                    mailItem.ConsigneeInfoId = command.ConsigneeInfoId;
                    if (mailItem.ConsigneeInfo != null)
                    {
                        mailItem.ConsigneeInfo.FirstName = command.ConsigneeInformation;
                        mailItem.ConsigneeInfo.Mobile = command.Mobile;

                    }
                    //mailItem.ProofOfDeliveryImage = command.ProofOfDeliveryImage;
                    //mailItem.SignatureImage = command.SignatureImage;
                    mailItem.Weight = command.Weight;
                    mailItem.WeightUOMId = command.WeightUOMId;
                    mailItem.LengthUOMId = command.LengthUOMId;
                    mailItem.Hight = command.Hight;
                    mailItem.Width = command.Width;
                    //mailItem.ReservedSpecial = command.ReservedSpecial;
                    //mailItem.Notes = command.Notes;
                    //mailItem.ItemTypeId = command.ItemTypeId;
                    /// mailItem.StatusId = command.StatusId;
                    //mailItem.CreatedBy = _currentUser.Id;
                    //mailItem.CreationTime = DateTime.UtcNow;
                    mailItem.StatusId = (int)MailItemStatuses.ReadyForDispatch;
                    mailItem.IsDeleted = false;
                    int itemId = _context.MailItems.IgnoreQueryFilters().Select(x => x.Id).OrderBy(x => x).LastOrDefault();
                    if (string.IsNullOrEmpty(mailItem.MailItemBarCode))
                    {
                        itemId += 1;
                        mailItem.MailItemBarCode = GenerateItemCodeAsync(itemId);
                    }
                    try
                    {
                        await _context.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {

                    }
                  
                    return mailItem.Id;
                }

                
            }
            private string GenerateItemCodeAsync(int ItemId)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(ItemId):00000000}");
                return sb.ToString();
            }


        }
    }
}
