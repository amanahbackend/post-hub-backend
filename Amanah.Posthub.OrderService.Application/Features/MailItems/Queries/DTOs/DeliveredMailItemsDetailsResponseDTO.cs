﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.DTOs
{
    public class DeliveredMailItemsDetailsResponseDTO
    {
        public string BusinessCustomerName { get; set; }
        public string ParCode { get; set; }
        public string ConsigneeName { get; set; }
        public string RecieverName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? DelivedDate { get; set; }
        public string ReturnReason { get; set; }
    }
}
