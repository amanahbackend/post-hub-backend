﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.DTOs
{
    public class DeliveredMailItemsListResponseDTO
    {
        public int Id { get; set; }
        public string BarCode { get; set; }
        public DateTime DeliverDate { get; set; }
        public DateTime CreationDate { get; set; }
        public string ReturnDate { get; set; }
        public string RecierverName { get; set; }
    }
}
