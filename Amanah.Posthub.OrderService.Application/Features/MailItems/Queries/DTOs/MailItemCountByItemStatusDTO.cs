﻿namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class MailItemCountByItemStatusDTO
    {
        public int? StatusId { get; set; }
        public string Status { get; set; }
        public int Count { get; set; }
    }
}
