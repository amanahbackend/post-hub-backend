﻿using System;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    public class MailItemForm2DTO
    {
        public string BusinessCustomerName { get; set; }
        public string MailItemBarCode { get; set; }
        public string ConsigneeName { get; set; }
        public string ReturnResone { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
