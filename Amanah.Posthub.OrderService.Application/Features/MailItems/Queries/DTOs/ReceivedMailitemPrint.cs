﻿using System;

namespace Amanah.Posthub.OrderService.Application.DTO
{
    internal class ReceivedMailitemPrint
    {
        public string Code { get; set; }
        public string SenderName { get; set; }
        public string ReciverName { get; set; }
        public DateTime? DelivaryDate { get; set; }
    }
}