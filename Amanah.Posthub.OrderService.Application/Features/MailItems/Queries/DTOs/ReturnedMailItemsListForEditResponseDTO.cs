﻿using Amanah.Posthub.OrderService.Application.DTO;
using System;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.DTOs
{
    public class ReturnedMailItemsListForEditResponseDTO
    {
        public int Id { get; set; }
        public int DriverId { set; get; }
        public string DriverName { set; get; }
        public string ReturnedResone { set; get; }
        public int ReturnedResoneId { set; get; }
        public string OrderCode { get; set; }
        public int? WorkorderId { get; set; }

        public string MailItemBarCode { get; set; }

        //public int? TotalItemCount { get; set; }
        //public DateTime? DeliveryBefore { get; set; }
        //public AddressDto PickUpAddress { get; set; }
        //public AddressDto DropOffAddress { get; set; }
        //public int? ConsigneeInfoId { get; set; }
        //public ContactDto ConsigneeInfo { get; set; }
        //public int? ReciverInfoId { get; set; }
        //public ContactDto ReciverInfo { get; set; }
        //public bool IsMatchConsigneeID { get; set; }
        //public int? Weight { get; set; }
        //public int? WeightUOMId { get; set; }
        //public WeightUOMDto WeightUOM { get; set; }
        //public int? Hight { get; set; }
        //public int? Width { get; set; }
        //public int? LengthUOMId { get; set; }
        //public LengthUOMDto LengthUOM { get; set; }
        //public string ReservedSpecial { get; set; }
        //public string Notes { get; set; }
        //public int? ItemTypeId { get; set; }
        //public string ItemTypeName { get; set; }
        //public MailItemTypeDto ItemType { get; set; }
        //public int? StatusId { get; set; }
        //public string Status { get; set; }

        //public string Governorate { get; set; }
        //public string Area { get; set; }
        //public string Block { get; set; }
        //public string Street { get; set; }
        //public string Building { get; set; }
        //public string Floor { get; set; }
        //public string Flat { get; set; }
        //public string FirstName { get; set; }
        //public string ConsigneeInformation { get; set; }
        //public string FromSerial { get; set; }
        //public DateTime? PickupDate { get; set; }
        //public int? CaseNo { get; set; }
        //public string Mobile { get; set; }
        //public int? CountryId { get; set; }
        //public decimal? Quantity { get; set; }
        //public decimal? TotalPrice { get; set; }

    }
}
