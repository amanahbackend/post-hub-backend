﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class DeliveredMailItemsDetailsQuery : IRequest<DeliveredMailItemsDetailsResponseDTO>
    {
        public int Id { set; get; }
    }
    public class DeliveredMailItemsDetailsQueryHandler : IRequestHandler<DeliveredMailItemsDetailsQuery, DeliveredMailItemsDetailsResponseDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public DeliveredMailItemsDetailsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<DeliveredMailItemsDetailsResponseDTO> Handle(DeliveredMailItemsDetailsQuery query, CancellationToken cancellationToken)
        {

            var mailItemObj = await _context.MailItems
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.Id == query.Id)
                 .ProjectTo<DeliveredMailItemsDetailsResponseDTO>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync();
            //var mappedObj= _mapper.Map<RFQDetailsResponseDTO>(rfqsList);
            //mappedObj.PqItems= _mapper.Map<CreatePQItemsInputDTO>(rfqsList.RFQItemPrices.First());
            //mappedObj.PqItems.Total = mappedObj.PqItems.HasAmount?(decimal) mappedObj.PqItems.Amount * mappedObj.PqItems.ItemPrice : mappedObj.PqItems.ItemPrice;
            //mappedObj.RFQItems= _mapper.Map<List<CreateRFQItemsInputDTO>>(rfqsList.RFQItemPrices);

            return mailItemObj;
        }
    }





}
