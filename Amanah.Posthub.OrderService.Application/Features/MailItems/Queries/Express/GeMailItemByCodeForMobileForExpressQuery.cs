﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.Express
{

    public class GeMailItemByCodeForMobileForExpressQuery : IRequest<Result>
    {
        public string Code { set; get; }
        public int? DriverId { get; set; }
    }
    public class GeMailItemByCodeForMobileForExpressQueryHandler : IRequestHandler<GeMailItemByCodeForMobileForExpressQuery, Result>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILocalizer _localizer;

        public GeMailItemByCodeForMobileForExpressQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;


        }
        public async Task<Result> Handle(GeMailItemByCodeForMobileForExpressQuery query, CancellationToken cancellationToken)
        {
            var result = new Result();
            var item = await _context
                 .MailItems
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(c => c.MailItemBarCode == query.Code && !c.IsDeleted && c.StatusId != (int)MailItemStatuses.Returned && c.StatusId != (int)MailItemStatuses.Delivered && (query.DriverId == null || c.Workorder.DriverId == query.DriverId))
                 .ProjectTo<MailItemForMobileDto>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();

            if (item == null)
            {
                result.Error = _localizer[Keys.Validation.NotAssignedToYourAccount];
                return result;
            }

            result.Value = item;
            return result;
        }
    }
}
