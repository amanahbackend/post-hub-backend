﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.Express
{

    public class GeMailItemByIdForMobileForExpressQuery : IRequest<MailItemForMobileDto>
    {
        public int Id { set; get; }
    }
    public class GeMailItemByIdForMobileForExpressQueryHandler : IRequestHandler<GeMailItemByIdForMobileForExpressQuery, MailItemForMobileDto>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GeMailItemByIdForMobileForExpressQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<MailItemForMobileDto> Handle(GeMailItemByIdForMobileForExpressQuery query, CancellationToken cancellationToken)
        {
            var items = await _context
                 .MailItems
                 .Include(c => c.DropOffAddress)
                 .Include(c => c.Status)
                 .Include(c => c.ItemReturnReason)
                 .Include(c => c.ConsigneeInfo)
                 .Include(c => c.ReciverInfo)
                 .Include(c => c.Workorder)
                 .ThenInclude(c => c.WorkOrderStatus)
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(c => c.Id == query.Id && !c.IsDeleted)
                 .ProjectTo<MailItemForMobileDto>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();

            return items;
        }
    }
}

