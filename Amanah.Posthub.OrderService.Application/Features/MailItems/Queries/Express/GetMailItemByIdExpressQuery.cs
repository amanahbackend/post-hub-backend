﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.Express
{
    public class GetMailItemByIdExpressQuery : IRequest<MailItemDto>
    {
        public int Id { get; set; }
        public class GetMailItemByIdExpressQueryHandler : IRequestHandler<GetMailItemByIdExpressQuery, MailItemDto>
        {
            private readonly ApplicationDbContext _context;
            public GetMailItemByIdExpressQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<MailItemDto> Handle(GetMailItemByIdExpressQuery query, CancellationToken cancellationToken)
            {
                var mailItem = await _context.MailItems.IgnoreQueryFilters().Where(a => a.Id == query.Id)
                    .Select(m => new MailItemDto
                    {
                        Id = m.Id,
                        Weight = m.Weight,
                        ConsigneeInformation = m.ConsigneeInfo.FirstName,
                        Mobile = m.ConsigneeInfo.Mobile,
                        ConsigneeInfoId = m.ConsigneeInfoId,
                        ItemTypeId = m.ItemTypeId,
                        ItemTypeName = m.ItemType.Name_en,
                        DeliveryBefore = m.DeliveryBefore,
                        IsMatchConsigneeID = m.IsMatchConsigneeID,
                        MailItemBarCode = m.MailItemBarCode,
                        ReservedSpecial = m.ReservedSpecial,
                        Notes = m.Notes,
                        Area = m.DropOffAddress.Area,
                        Governorate = m.DropOffAddress.Governorate,
                        Street = m.DropOffAddress.Street,
                        Block = m.DropOffAddress.Block,
                        Building = m.DropOffAddress.Building,
                        Floor = m.DropOffAddress.Floor,
                        Flat = m.DropOffAddress.Flat,
                        TotalItemCount = m.Order.TotalItemCount,
                        WeightUOMId = m.WeightUOMId,
                        CountryId = m.CountryId,
                        Quantity = m.Quantity,
                        TotalPrice = m.TotalPrice
                    }).FirstOrDefaultAsync();

                return mailItem;
            }
        }
    }
}
