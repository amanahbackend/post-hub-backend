﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.Express
{
    public class GetMailItemListByOrderIdForMobileExpressQuery : PaginatedItemsViewModel, IRequest<PagedResult<MailItemDto>>
    {
        public class GetMailItemListByOrderIdForMobileExpressQueryHandler : IRequestHandler<GetMailItemListByOrderIdForMobileExpressQuery, PagedResult<MailItemDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetMailItemListByOrderIdForMobileExpressQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<PagedResult<MailItemDto>> Handle(GetMailItemListByOrderIdForMobileExpressQuery query, CancellationToken cancellationToken)
            {
                var mailItems = await _context.MailItems
                    .Include(c => c.Order)
                    .Include(c => c.ConsigneeInfo)
                    .Include(c => c.ItemType)
                    .Include(c => c.DropOffAddress)
                    .IgnoreQueryFilters()
                    .Where(c => c.OrderId == query.Id)
                    .Select(m => new MailItemDto
                    {
                        Id = m.Id,
                        Weight = m.Weight,
                        ConsigneeInformation = m.ConsigneeInfo != null ? m.ConsigneeInfo.FirstName : "",
                        Mobile = m.ConsigneeInfo != null ? m.ConsigneeInfo.Mobile : "",
                        ConsigneeInfoId = m.ConsigneeInfoId,
                        ItemTypeId = m.ItemTypeId,
                        ItemTypeName = m.ItemType != null ? m.ItemType.Name_en : "",
                        DeliveryBefore = m.DeliveryBefore,
                        IsMatchConsigneeID = m.IsMatchConsigneeID,
                        MailItemBarCode = m.MailItemBarCode,
                        ReservedSpecial = m.ReservedSpecial,
                        Notes = m.Notes,
                        Area = m.DropOffAddress.Area,
                        Governorate = m.DropOffAddress != null ? m.DropOffAddress.Governorate : "",
                        Street = m.DropOffAddress != null ? m.DropOffAddress.Street : "",
                        Block = m.DropOffAddress != null ? m.DropOffAddress.Block : "",
                        Building = m.DropOffAddress != null ? m.DropOffAddress.Building : "",
                        Floor = m.DropOffAddress != null ? m.DropOffAddress.Floor : "",
                        Flat = m.DropOffAddress != null ? m.DropOffAddress.Flat : "",
                        TotalItemCount = m.Order != null ? m.Order.TotalItemCount : 0,
                        WeightUOMId = m.WeightUOMId,
                        CountryId = m.CountryId,
                        Quantity = m.Quantity,
                        TotalPrice = m.TotalPrice
                    }).ToPagedResultAsync(query);

                if (mailItems == null) return null;

                return mailItems;
            }
        }
    }
}
