﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Localization;
using BarcodeLib;
using Utilites.UploadFile;
using Utilites.ProcessingResult;
using System.Drawing;
using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{

    public class GeMailItemByCodeForMobileQuery : IRequest<Result>
    {
        public string Code { set; get; }
        public int? DriverId { get; set; }
    }
    public class GeMailItemByCodeForMobileQueryHandler : IRequestHandler<GeMailItemByCodeForMobileQuery, Result>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILocalizer _localizer;
        private readonly IUploadFormFileService _MailItemBarcodeImgUploader;

        public GeMailItemByCodeForMobileQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ILocalizer localizer,
            IUploadFormFileService MailItemBarcodeImgUploader)
        {
            _context = context;
            _mapper = mapper;
            _localizer = localizer;
            _MailItemBarcodeImgUploader = MailItemBarcodeImgUploader;

        }
        public async Task<Result> Handle(GeMailItemByCodeForMobileQuery query, CancellationToken cancellationToken)
        {
            var result = new Result();
            var item = new MailItemForMobileDto();
            if (query.DriverId != null && query.DriverId > 0)
            {
                item = await _context
                                .MailItems
                                .Include(m => m.ConsigneeInfo)
                                .Include(m => m.ReciverInfo)
                                .Include(m => m.Workorder)
                                .ThenInclude(m => m.WorkOrderStatus)
                                .Include(m => m.DropOffAddress)
                                .Include(m => m.Status)
                                .Include(m => m.ItemReturnReason)
                                .Include(m => m.Order)
                                .AsQueryable()
                                .AsNoTracking()
                                .IgnoreQueryFilters()
                                .Where(c => c.MailItemBarCode == query.Code && !c.IsDeleted && c.StatusId != (int)MailItemStatuses.Returned && c.StatusId != (int)MailItemStatuses.Delivered )//comment for pickup of international&& (query.DriverId == null || c.Workorder.DriverId == query.DriverId))
                                .ProjectTo<MailItemForMobileDto>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();
            }
            else
            {
                item = await _context
                                .MailItems
                                .Include(m => m.ConsigneeInfo)
                                .Include(m => m.ReciverInfo)
                                .Include(m => m.Workorder)
                                .ThenInclude(m => m.WorkOrderStatus)
                                .Include(m => m.DropOffAddress)
                                .Include(m => m.Status)
                                .Include(m => m.ItemReturnReason)
                                .Include(m => m.Order)
                                .AsQueryable()
                                .AsNoTracking()
                                .IgnoreQueryFilters()
                                .Where(c => c.MailItemBarCode == query.Code && !c.IsDeleted && c.StatusId != (int)MailItemStatuses.Returned && c.StatusId != (int)MailItemStatuses.Delivered)
                                .ProjectTo<MailItemForMobileDto>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();
            }


            if (item == null)
            {
                result.Error = _localizer[Keys.Validation.NotAssignedToYourAccount];
                return result;
            }
            if (item.OrderId != null)
            {
                var OrderAttachments = await (from attachmentFiles in _context.AttachmentFiles
                                              join order in _context.Orders on attachmentFiles.BusinessOrderId equals order.BusinessOrderId
                                              where order.Id == item.OrderId
                                              select attachmentFiles).AsNoTracking().ToListAsync(cancellationToken: cancellationToken);
                if (OrderAttachments.Count >= 0)
                {
                    item.Attachments = OrderAttachments;
                } 
            }
            item.MailItemBarCodeImgURL = GenerateMailItemBarcodeImg(item.MailItemBarCode).ReturnData;
            result.Value = item;
            return result;
        }

        internal ProcessResult<string> GenerateMailItemBarcodeImg(string DataToEncode)
        {
            if (string.IsNullOrWhiteSpace(DataToEncode.ToString()))
            {
                TaskCanceledException AbortEx = new TaskCanceledException();
                throw AbortEx;
            }
            Barcode MailItemBarcode = new Barcode();
            MailItemBarcode.IncludeLabel = true;
            Image MailItemBarcodeImg = MailItemBarcode.Encode(TYPE.CODE128, DataToEncode);
            var MailItemBarcodeImgURL = _MailItemBarcodeImgUploader.UploadMailItemBarcodeImgAsync(MailItemBarcodeImg, "MailItemImages/BarcodesForMobile");
            return MailItemBarcodeImgURL.Result;
        }
    }
}
