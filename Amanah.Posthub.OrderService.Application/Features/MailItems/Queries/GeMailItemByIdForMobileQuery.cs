﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using BarcodeLib;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilites.ProcessingResult;
using System.Drawing;
using Utilites.UploadFile;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{

    public class GeMailItemByIdForMobileQuery : IRequest<MailItemForMobileDto>
    {
        public int Id { set; get; }
    }
    public class GeMailItemByIdForMobileQueryHandler : IRequestHandler<GeMailItemByIdForMobileQuery, MailItemForMobileDto>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUploadFormFileService _MailItemBarcodeImgUploader;

        public GeMailItemByIdForMobileQueryHandler(
            ApplicationDbContext context,
            IMapper mapper, IUploadFormFileService MailItemBarcodeImgUploader
            )
        {
            _context = context;
            _mapper = mapper;
            _MailItemBarcodeImgUploader = MailItemBarcodeImgUploader;
        }
        public async Task<MailItemForMobileDto> Handle(GeMailItemByIdForMobileQuery query, CancellationToken cancellationToken)
        {
            var items = await _context
                 .MailItems
                 .Include(c=>c.ConsigneeInfo)
                 .Include(c=>c.ReciverInfo)
                 .Include(c=>c.DropOffAddress)
                 .Include(c=>c.Status)
                 .Include(c=>c.ItemReturnReason)
                 .Include(c=>c.Workorder)
                 .ThenInclude(c=>c.WorkOrderStatus)
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters().Include(m=>m.Order)
                 .Where(c => c.Id == query.Id && !c.IsDeleted)
                 .ProjectTo<MailItemForMobileDto>(_mapper.ConfigurationProvider).FirstOrDefaultAsync();

            if (items.OrderId != null)
            {
                var OrderAttachments = await (from attachmentFiles in _context.AttachmentFiles
                                              join order in _context.Orders on attachmentFiles.BusinessOrderId equals order.BusinessOrderId
                                              where order.Id == items.OrderId
                                              select attachmentFiles).AsNoTracking().ToListAsync(cancellationToken: cancellationToken);

                if (OrderAttachments.Count > 0)
                {
                    items.Attachments = OrderAttachments;
                }
            }
            items.MailItemBarCodeImgURL = GenerateMailItemBarcodeImg(items.MailItemBarCode).ReturnData;
            return items;
        }
        internal ProcessResult<string> GenerateMailItemBarcodeImg(string DataToEncode)
        {
            if (string.IsNullOrWhiteSpace(DataToEncode.ToString()))
            {
                TaskCanceledException AbortEx = new TaskCanceledException();
                throw AbortEx;
            }
            Barcode MailItemBarcode = new Barcode();
            MailItemBarcode.IncludeLabel = true;
            Image MailItemBarcodeImg = MailItemBarcode.Encode(TYPE.CODE128, DataToEncode);
            var MailItemBarcodeImgURL = _MailItemBarcodeImgUploader.UploadMailItemBarcodeImgAsync(MailItemBarcodeImg, "MailItemImages/BarcodesForMobile");
            return MailItemBarcodeImgURL.Result;
        }
    }
}
