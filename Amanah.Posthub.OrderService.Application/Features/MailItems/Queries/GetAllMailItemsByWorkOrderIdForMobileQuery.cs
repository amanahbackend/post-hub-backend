﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class GetAllMailItemsByWorkOrderIdForMobileQuery : PaginatedItemsViewModel, IRequest<PagedResult<MailItemForMobileDto>>
    {
        public int WorkOrderId { set; get; }
    }

    public class GetAllMailItemByWorkOrderIdForMobileQueryHandler : IRequestHandler<GetAllMailItemsByWorkOrderIdForMobileQuery, PagedResult<MailItemForMobileDto>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllMailItemByWorkOrderIdForMobileQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<MailItemForMobileDto>> Handle(GetAllMailItemsByWorkOrderIdForMobileQuery query, CancellationToken cancellationToken)
        {
            var items = await _context
                 .MailItems
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(c => c.WorkorderId == query.WorkOrderId && !c.IsDeleted)
                 .ProjectTo<MailItemForMobileDto>(_mapper.ConfigurationProvider).ToPagedResultAsync(query);


            return items;
        }
    }
}
