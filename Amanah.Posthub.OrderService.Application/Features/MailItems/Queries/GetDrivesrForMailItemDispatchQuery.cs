﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class GetDrivesrForMailItemDispatchQuery : QueryListReq, IRequest<Result>
    {
    }
    public class GetDrivesrForMailItemDispatchQueryHandler : IRequestHandler<GetDrivesrForMailItemDispatchQuery, Result>
    {
        private readonly ApplicationDbContext _context;
        public GetDrivesrForMailItemDispatchQueryHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Result> Handle(GetDrivesrForMailItemDispatchQuery query, CancellationToken cancellationToken)
        {
            var result = new Result();
            result.TotalCount = await _context.MailItems
                .Include(m=> m.Order)
                .IgnoreQueryFilters()
                .Where(m => !m.IsDeleted 
                && !m.Order.IsDeleted
                && !m.Order.IsDraft
                && m.Order.ServiceSectorId == (int)ServiceSectors.LocalPost
                && m.DefaultAssignedDriverID != null
                && (m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.ReadyForDispatch))
                .Select(a => a.DefaultAssignedDriverID)
                .Distinct()
                .CountAsync();

            if (result.TotalCount < 1)
            {
                result.Error = "NotFound";
                return result;
            }

            result.Value = await _context.MailItems
                .Include(c => c.Order)
                .IgnoreQueryFilters()
                .Where(m => !m.IsDeleted 
                && !m.Order.IsDeleted
                && !m.Order.IsDraft
                && m.Order.ServiceSectorId == (int)ServiceSectors.LocalPost
                && m.DefaultAssignedDriverID != null
                && (m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.ReadyForDispatch))
                .IgnoreQueryFilters()
                .GroupBy(m => new { m.DefaultAssignedDriverID, m.DefaultAssignedDriver.User.UserName })
                .Select(a => new
                {
                    id = a.Key.DefaultAssignedDriverID,
                    DriverUserName = a.Key.UserName,
                    ItemsNo = a.Count()
                })
                .OrderBy(m => m.id)
                .Skip((query.PageNumber - 1) * query.PageSize)
                .Take(query.PageSize)
                .ToListAsync();

            return result;
        }
    }

}
