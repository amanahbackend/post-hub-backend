﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class GetMailItemByIdQuery : IRequest<MailItemDto>
    {
        public int Id { get; set; }
        public class GetMailItemByIdQueryHandler : IRequestHandler<GetMailItemByIdQuery, MailItemDto>
        {
            private readonly ApplicationDbContext _context;
            public GetMailItemByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<MailItemDto> Handle(GetMailItemByIdQuery query, CancellationToken cancellationToken)
            {
                var mailItem = await _context.MailItems.IgnoreQueryFilters().Where(a => a.Id == query.Id)
                    .Select(m => new MailItemDto
                    {
                        Id = m.Id,
                        Weight = m.Weight,
                        ConsigneeInformation = m.ConsigneeInfo.FirstName,
                        Mobile = m.ConsigneeInfo.Mobile,
                        ConsigneeInfoId = m.ConsigneeInfoId,
                        ItemTypeId = m.ItemTypeId,
                        ItemTypeName = m.ItemType.Name_en,
                        DeliveryBefore = m.DeliveryBefore,
                        IsMatchConsigneeID = m.IsMatchConsigneeID,
                        MailItemBarCode = m.MailItemBarCode,
                        ReservedSpecial = m.ReservedSpecial,
                        Notes = m.Notes,
                        Area = m.DropOffAddress.Area,
                        Governorate = m.DropOffAddress.Governorate,
                        Street = m.DropOffAddress.Street,
                        Block = m.DropOffAddress.Block,
                        Building = m.DropOffAddress.Building,
                        Floor = m.DropOffAddress.Floor,
                        Flat = m.DropOffAddress.Flat,
                        TotalItemCount = m.Order.TotalItemCount,
                        WeightUOMId = m.WeightUOMId,
                        CountryId = m.CountryId,
                        Quantity = m.Quantity,
                        TotalPrice = m.TotalPrice,
                        Latitude = m.StatusId == (int)MailItemStatuses.Returned || m.StatusId == (int)MailItemStatuses.Delivered ?
                      m.DropOffAddress.Latitude : m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.Dispatched
                      || m.StatusId == (int)MailItemStatuses.OnDelivery ? m.PickUpAddress.Latitude : "",
                        Longitude = m.StatusId == (int)MailItemStatuses.Returned || m.StatusId == (int)MailItemStatuses.Delivered ?
                      m.DropOffAddress.Longitude : m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.Dispatched
                      || m.StatusId == (int)MailItemStatuses.OnDelivery ? m.PickUpAddress.Longitude : "",
                        DeliveryDate = m.DeliveredDate,
                        RecieverName = m.ReciverInfo.FirstName + m.ReciverInfo.MidelName + m.ReciverInfo.Lastname,
                        SignatureImageURL = string.IsNullOrEmpty(m.SignatureImageURL) ? null : $"MailItemImages/Signature/" + m.SignatureImageURL,
                        ApprovedImagesUrl = m.ProofOfDeliveryImages.Select(x => string.IsNullOrEmpty(x.Path) ? null : $"MailItemImages/ProveOfDelivery/" + x.Path).ToList(),
                        Status = m.Status.Name
                    }).FirstOrDefaultAsync();

                return mailItem;
            }
        }
    }
}
