﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{

    public class GetMailItemCountByItemStatusQuery : IRequest<IEnumerable<object>>
    {

        public class GetMailItemCountByItemStatusQueryHandler : IRequestHandler<GetMailItemCountByItemStatusQuery, IEnumerable<object>>
        {
            private readonly ApplicationDbContext _context;
            public GetMailItemCountByItemStatusQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<object>> Handle(GetMailItemCountByItemStatusQuery query, CancellationToken cancellationToken)
            {
                // Ramzy: need items list
                var orderList = await _context.MailItems.AsNoTracking().IgnoreQueryFilters()
                    .Where(c => c.IsDeleted == false && c.StatusId != null)
                    .GroupBy(c =>new { c.Status.Name ,c.Status.Id})
                    .Select(m => new MailItemCountByItemStatusDTO
                    {
                        Count = m.Count(),
                        Status = m.Key.Name,
                        StatusId=m.Key.Id

                    })
                    .ToListAsync();
                if (orderList == null)
                {
                    return null;
                }
                return orderList;
            }
        }

    }
}
