﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class GetMailItemForrm2Query : IRequest<IEnumerable<object>>
    {
        public List<int> MailItemIds { set; get; }

        public class GetMailItemForrm2Handler : IRequestHandler<GetMailItemForrm2Query, IEnumerable<object>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetMailItemForrm2Handler(ApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<IEnumerable<object>> Handle(GetMailItemForrm2Query query, CancellationToken cancellationToken)
            {
                // Ramzy: need items list
                var MailItemsList = await _context
                    .MailItems
                    .IgnoreQueryFilters()
                    .Where(x=>x.IsDeleted !=true && query.MailItemIds.Contains(x.Id))
                    .ProjectTo<MailItemForm2DTO>(_mapper.ConfigurationProvider).ToListAsync();
                if (MailItemsList == null)
                {
                    return null;
                }
                return MailItemsList.AsReadOnly();
            }
        }

    }
}
