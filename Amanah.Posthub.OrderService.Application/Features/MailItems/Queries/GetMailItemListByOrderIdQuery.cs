﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class GetMailItemListByOrderIdQuery : IRequest<IEnumerable<MailItemDto>>
    {
        public int Id { get; set; }
        public string MailItemBarCode { get; set; }
        public string ConsigneeName { get; set; }
        public class GetMailItemListByOrderIdQueryHandler : IRequestHandler<GetMailItemListByOrderIdQuery, IEnumerable<MailItemDto>>
        {
            private readonly ApplicationDbContext _context;
            public GetMailItemListByOrderIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<MailItemDto>> Handle(GetMailItemListByOrderIdQuery query, CancellationToken cancellationToken)
            {
                var queryEf = _context.MailItems
                    .Include(x => x.PickUpAddress)
                    .Include(x => x.DropOffAddress)
                    .Include(x => x.ConsigneeInfo)
                    .Include(x => x.ItemType)
                    .Include(x => x.ReciverInfo)
                    .Include(x => x.ProofOfDeliveryImages)
                    .Include(x => x.Workorder)
                    .ThenInclude(x => x.Driver)
                    .ThenInclude(x => x.User)
                    .IgnoreQueryFilters()
                    .Where(c => c.OrderId == query.Id);

                if (!string.IsNullOrEmpty(query.MailItemBarCode))
                    queryEf = queryEf.Where(m => m.MailItemBarCode == query.MailItemBarCode);

                if (!string.IsNullOrEmpty(query.ConsigneeName))
                    queryEf = queryEf.Where(m => m.ConsigneeInfo.FirstName == query.ConsigneeName);

                var mailItems = queryEf.Select(m => new MailItemDto
                {
                    Id = m.Id,
                    Weight = m.Weight,
                    ConsigneeInformation = m.ConsigneeInfo.FirstName,
                    Mobile = m.ConsigneeInfo.Mobile,
                    ConsigneeInfoId = m.ConsigneeInfoId,
                    ItemTypeId = m.ItemTypeId,
                    ItemTypeName = m.ItemType.Name_en,
                    DeliveryBefore = m.DeliveryBefore,
                    IsMatchConsigneeID = m.IsMatchConsigneeID,
                    MailItemBarCode = m.MailItemBarCode,
                    ReservedSpecial = m.ReservedSpecial,
                    Notes = m.Notes,
                    Area = m.DropOffAddress.Area,
                    Governorate = m.DropOffAddress.Governorate,
                    Street = m.DropOffAddress.Street,
                    Block = m.DropOffAddress.Block,
                    Building = m.DropOffAddress.Building,
                    Floor = m.DropOffAddress.Floor,
                    Flat = m.DropOffAddress.Flat,
                    Latitude = m.StatusId == (int)MailItemStatuses.Returned || m.StatusId == (int)MailItemStatuses.Delivered ?
                      m.DropOffAddress.Latitude : m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.Dispatched
                      || m.StatusId == (int)MailItemStatuses.OnDelivery ? m.PickUpAddress.Latitude : "",
                    Longitude = m.StatusId == (int)MailItemStatuses.Returned || m.StatusId == (int)MailItemStatuses.Delivered ?
                      m.DropOffAddress.Longitude : m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.Dispatched
                      || m.StatusId == (int)MailItemStatuses.OnDelivery ? m.PickUpAddress.Longitude : "",
                    TotalItemCount = m.Order.TotalItemCount,
                    WeightUOMId = m.WeightUOMId,
                    CountryId = m.CountryId,
                    Quantity = m.Quantity,
                    TotalPrice = m.TotalPrice,
                    Status = m.Status.Name,
                    DriverName = m.Workorder != null ?
                                                        m.Workorder.Driver != null ?
                                                           m.Workorder.Driver.User != null ? m.Workorder.Driver.User.FullName : "" : "" : "",
                    IssueDate = m.Workorder != null ?
                                                        m.Workorder.IssueAt.ToString() : ""
                                                        ,
                    DeliveryDate = m.DeliveredDate,
                    SignatureImageURL = string.IsNullOrEmpty(m.SignatureImageURL) ? null : $"MailItemImages/Signature/" + m.SignatureImageURL,
                    ApprovedImagesUrl = m.ProofOfDeliveryImages.Select(x => string.IsNullOrEmpty(x.Path) ? null : $"MailItemImages/ProveOfDelivery/" + x.Path).ToList(),
                    RecieverName = m.ReciverInfo != null ? m.ReciverInfo.FirstName + m.ReciverInfo.MidelName + m.ReciverInfo.Lastname : string.Empty,
                    StatusId = m.StatusId,
                    ItemReturnReason = m.ItemReturnReasonId != null ? m.ItemReturnReason.Name : m.OtherItemReturnReason,
                });
                if (mailItems == null) return null;
                return mailItems;
            }
        }
    }
}
