﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class GetMailItemListQuery : IRequest<IEnumerable<object>>
    {

        public class GetAllOrdersQueryHandler : IRequestHandler<GetMailItemListQuery, IEnumerable<object>>
        {
            private readonly ApplicationDbContext _context;
            public GetAllOrdersQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<object>> Handle(GetMailItemListQuery query, CancellationToken cancellationToken)
            {
                // Ramzy: need items list
                var orderList = await _context.MailItems.IgnoreQueryFilters().Select(o => new { o.MailItemBarCode }).ToListAsync();
                if (orderList == null)
                {
                    return null;
                }
                return orderList.AsReadOnly();
            }
        }

    }
}
