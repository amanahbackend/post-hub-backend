﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class GetMailItemsByAssignedDriverForDispatchQuery : IRequest<IEnumerable<MailItemForDispatch>>
    {
        public int DriverId { get; set; }
    }
    public class GetMailItemsByAssignedDriverForDispatchQueryHandler : IRequestHandler<GetMailItemsByAssignedDriverForDispatchQuery, IEnumerable<MailItemForDispatch>>
    {
        private readonly ApplicationDbContext _context;
        public GetMailItemsByAssignedDriverForDispatchQueryHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<MailItemForDispatch>> Handle(GetMailItemsByAssignedDriverForDispatchQuery query, CancellationToken cancellationToken)
        {
            var mailItems = _context
                 .MailItems
                 .Include(c=>c.Order)
                 .IgnoreQueryFilters()
                .Where(a => a.DefaultAssignedDriverID == query.DriverId
                && !a.IsDeleted
                && !a.Order.IsDeleted
                && !a.Order.IsDraft
                && a.Order.ServiceSectorId == (int)ServiceSectors.LocalPost
                && (a.StatusId == (int)MailItemStatuses.New || a.StatusId == (int)MailItemStatuses.ReadyForDispatch))
                .Select(a => new MailItemForDispatch()
                {
                    Id = a.Id,
                    ItemCode = a.MailItemBarCode,
                    ItemType = Thread.CurrentThread.CurrentCulture.IsArabic() ? a.ItemType.Name_ar : a.ItemType.Name_en,
                    ItemWeight = a.Weight + " " + (Thread.CurrentThread.CurrentCulture.IsArabic() ? a.WeightUOM.Name_ar : a.WeightUOM.Name_en),
                    ItemDeleveryType = a.ItemDeleveryType.Name,
                    Status = a.Status.Name,
                    DeliveryBeforeDate = a.DeliveryBefore,
                    OrderCode = a.Order.OrderCode,
                    OrderId = a.OrderId
                }).ToList();

            if (mailItems == null)
                return null;

            return mailItems.AsReadOnly();
        }
    }

}
