﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{

    public class GetMailItemsGroupByOrderQuery : IRequest<IEnumerable<MailItemForDispatch>>
    {
        public class GetMailItemsGroupByOrderQueryHandler : IRequestHandler<GetMailItemsGroupByOrderQuery, IEnumerable<MailItemForDispatch>>
        {
            private readonly ApplicationDbContext _context;
            public GetMailItemsGroupByOrderQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<MailItemForDispatch>> Handle(GetMailItemsGroupByOrderQuery query, CancellationToken cancellationToken)
            {
                List<MailItemForDispatch> m = new List<MailItemForDispatch>();
                // Ramzy: need items list
                //var orderList= await _context.MailItems.GroupBy(i=> i.or)
                //if (orderList == null)
                //{
                //    return null;
                //}
                return m.AsReadOnly();
            }
        }

    }
}
