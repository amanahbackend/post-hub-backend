﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{

    public class GetReturnedMailItemsForEditPaginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<ReturnedMailItemsListForEditResponseDTO>>
    {
        public string SearchBy { get; set; }
        public int? DriverId { get; set; }

    }

    public class GetReturnedMailItemsForEditHandler : IRequestHandler<GetReturnedMailItemsForEditPaginatedQuery, PagedResult<ReturnedMailItemsListForEditResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetReturnedMailItemsForEditHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<ReturnedMailItemsListForEditResponseDTO>> Handle(GetReturnedMailItemsForEditPaginatedQuery query, CancellationToken cancellationToken)
        {

            var mailItemList = _context
                 .MailItems
                 .IgnoreQueryFilters()
                 .Include(x=>x.Status)
                 .Include(m=>m.ConsigneeInfo)
                 .Include(m => m.ItemType)
                 .Include(m => m.Workorder)
                       .ThenInclude(m=>m.Driver)
                          .ThenInclude(m=>m.User)
                 .Include(m => m.ItemReturnReason)
                 .Where(x => 
                     x.IsDeleted != true 
                    &&   x.StatusId == (int)MailItemStatuses.Returned 
                    &&  (string.IsNullOrEmpty(query.SearchBy) || x.Id.ToString()== query.SearchBy || x.MailItemBarCode.Contains(query.SearchBy) || x.Order.OrderCode.Contains(query.SearchBy))
                    && (!query.DriverId.HasValue || query.DriverId.Value == 0 || x.Workorder.DriverId.Value == query.DriverId.Value)
                   )
                 .AsQueryable()
                 .AsNoTracking();

            var mapped = await mailItemList.Select(m => new ReturnedMailItemsListForEditResponseDTO
            {

                Id = m.Id,
                 DriverId = m.Workorder.DriverId.HasValue ? m.Workorder.DriverId.Value : 0,
                DriverName = m.Workorder.Driver != null ? m.Workorder.Driver.User.UserName : "",
                ReturnedResoneId = m.ItemReturnReasonId.HasValue ? m.ItemReturnReasonId.Value : 0,
                ReturnedResone = m.ItemReturnReason != null ? m.ItemReturnReason.Name : "",
                OrderCode = m.Order.OrderCode !=null ? m.Order.OrderCode:string.Empty,
                MailItemBarCode = m.MailItemBarCode,
                //ConsigneeInformation = m.ConsigneeInfo.FirstName,
                //Mobile = m.ConsigneeInfo.Mobile,
                //ConsigneeInfoId = m.ConsigneeInfoId,
                //ItemTypeId = m.ItemTypeId,
                //ItemTypeName = m.ItemType.Name_en,
                //DeliveryBefore = m.DeliveryBefore,
                //IsMatchConsigneeID = m.IsMatchConsigneeID,
                //MailItemBarCode = m.MailItemBarCode,
                //ReservedSpecial = m.ReservedSpecial,
                //Notes = m.Notes,
                //Area = m.DropOffAddress.Area,
                //Governorate = m.DropOffAddress.Governorate,
                //Street = m.DropOffAddress.Street,
                //Block = m.DropOffAddress.Block,
                //Building = m.DropOffAddress.Building,
                //Floor = m.DropOffAddress.Floor,
                //Flat = m.DropOffAddress.Flat,
                //TotalItemCount = m.Order.TotalItemCount,
                //WeightUOMId = m.WeightUOMId,
                //CountryId = m.CountryId,
                //Quantity = m.Quantity,
                //TotalPrice = m.TotalPrice,
                //StatusId = m.StatusId.Value,
                //Status = m.StatusId.HasValue ? m.Status.Name : "",

            })
            .ToPagedResultAsync(query);


            return mapped;
        }
    }
}

