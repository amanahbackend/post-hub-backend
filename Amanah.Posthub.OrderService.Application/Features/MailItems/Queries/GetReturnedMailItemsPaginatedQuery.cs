﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.MailItems.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{

    public class GetReturnedMailItemsPaginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<DeliveredMailItemsListResponseDTO>>
    {
        public int? BusinessCustomerId { set; get; }
        public string BarCode { get; set; }
        public DateTime? FromDate { set; get; }
        public DateTime? ToDate { set; get; }
    }

    public class GetReturnedMailItemsPaginatedQueryQueryHandler : IRequestHandler<GetReturnedMailItemsPaginatedQuery, PagedResult<DeliveredMailItemsListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetReturnedMailItemsPaginatedQueryQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<DeliveredMailItemsListResponseDTO>> Handle(GetReturnedMailItemsPaginatedQuery query, CancellationToken cancellationToken)
        {

            var mailItemList = _context
                 .MailItems.IgnoreQueryFilters().Where(x => x.StatusId == (int)MailItemStatuses.Returned)
                 .AsQueryable()
                 .AsNoTracking();
            if (query.BusinessCustomerId != null)
            {
                mailItemList = mailItemList.Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId);
            }
            if (query.FromDate.HasValue && query.ToDate.HasValue)
            {
                mailItemList = mailItemList.Where(x => x.ReturnedDate.Value.Date >= query.FromDate.Value.Date
                               && x.ReturnedDate.Value.Date <= query.ToDate.Value.Date);
            }
         
            if (!string.IsNullOrEmpty(query.BarCode))
            {
                mailItemList = mailItemList.Where(x => x.MailItemBarCode == query.BarCode);
            }

            var mapped = await mailItemList.ProjectTo<DeliveredMailItemsListResponseDTO>(_mapper.ConfigurationProvider)
                  .ToPagedResultAsync(query);


            return mapped;
        }
    }
}

