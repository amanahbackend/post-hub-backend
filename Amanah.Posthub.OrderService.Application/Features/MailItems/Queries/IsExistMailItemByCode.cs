﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{

    public class IsExistMailItemByCodeQuery : IRequest<bool>
    {
        public string Code { set; get; }
    }
    public class IsExistMailItemByCodeQueryHandler : IRequestHandler<IsExistMailItemByCodeQuery, bool>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public IsExistMailItemByCodeQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<bool> Handle(IsExistMailItemByCodeQuery query, CancellationToken cancellationToken)
        {
            var result = await _context
                 .MailItems
                 .IgnoreQueryFilters()
                 .AsQueryable()
                 .AsNoTracking()
                 .AnyAsync(x=>x.IsDeleted!=true && x.MailItemBarCode == query.Code.Trim());

            return result;
        }
    }
}
