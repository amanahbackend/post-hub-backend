﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.MailItems.Queries
{
    public class ReceivedMailitemsPrintQuery : IRequest<Result>
    {
        public List<int> MailitemsIds { get; set; }
        public List<int> OrdersIds { get; set; }

        public class ReceivedMailitemsPrintQueryHandler : IRequestHandler<ReceivedMailitemsPrintQuery, Result>
        {
            private readonly ApplicationDbContext _context;

            public ReceivedMailitemsPrintQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
            }

            public async Task<Result> Handle(ReceivedMailitemsPrintQuery query, CancellationToken cancellationToken)
            {
                var result = new Result();

                var mailitemsPrint = await _context.MailItems
                    .Include(m => m.Order)
                    //.ThenInclude(o => o.Sender)
                    //.ThenInclude(s => s.User)
                    .Include(m => m.ConsigneeInfo)
                    .Include(m => m.Workorder)
                    .Where(m => !m.IsDeleted
                    && (query.MailitemsIds == null
                    || query.MailitemsIds.Contains(m.Id))
                    && (query.OrdersIds == null
                    || query.OrdersIds.Contains(m.OrderId.Value))
                    )
                    .IgnoreQueryFilters()
                    .AsQueryable()
                    .AsNoTracking()
                    .Select(m => new ReceivedMailitemPrint
                    {
                        //Code = GenerateMailitemPrintCode(m.Id),
                        Code = m.MailItemBarCode,
                        SenderName = m.Order.SenderName,
                        ReciverName = m.ConsigneeInfo.FirstName,
                        DelivaryDate = m.Workorder.DeliveryDate
                    })
                    .ToListAsync();

                if (mailitemsPrint == null || mailitemsPrint.Count <= 0)
                {
                    result.Error = "mailitems Print Not Found";
                }
                else
                {
                    result.Value = mailitemsPrint;
                    result.TotalCount = mailitemsPrint.Count();
                }
                return result;
            }

            private static string GenerateMailitemPrintCode(int id)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(id):0000000000}");
                return sb.ToString();
            }
        }
    }
}
