﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries.DTOs
{
    public class TrackOrderStatusLogDto
    {
        public int OrderStatusId { get; set; }
        public int OrderId { get; set; }
        public string OrderStatusName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string TimeOfDate { get; set; }
    }
}
