﻿using System.Linq;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries.Express
{
    public class GetOrderStatusLogsByOrderCodeForMobileExpressQuery : IRequest<Result>
    {
        public string Code { set; get; }
    }
    public class GetOrderStatusLogsByOrderCodeForMobileExpressQueryHandler : IRequestHandler<GetOrderStatusLogsByOrderCodeForMobileExpressQuery, Result>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetOrderStatusLogsByOrderCodeForMobileExpressQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;
        }
        public async Task<Result> Handle(GetOrderStatusLogsByOrderCodeForMobileExpressQuery query, CancellationToken cancellationToken)
        {
            var result = new Result();

            result.TotalCount = await _context
                .OrdersStatusLogs
                .Where(c => (
                 (c.Order.OrderCode != null
                 && c.Order.OrderCode.ToLower().Contains(query.Code.ToLower())))
                 && !c.IsDeleted
                 && (!_currentUser.IsBusinessCustomer()
                 || (c.Order.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId))).CountAsync();

            if (result.TotalCount < 1)
            {
                result.Error = "Code Not Found";
                return result;
            }

            result.Value = await _context
                 .OrdersStatusLogs
                 .Include(c => c.OrderStatus)
                 .AsQueryable()
                 .AsNoTracking()
                 .Where(c => (
                 (c.Order.OrderCode != null
                 && c.Order.OrderCode.ToLower().Contains(query.Code.ToLower())))
                 && !c.IsDeleted
                 && (!_currentUser.IsBusinessCustomer()
                 || (c.Order.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)))
                 .ProjectTo<TrackOrderStatusLogDto>(_mapper.ConfigurationProvider).ToListAsync();

            return result;
        }
    }
}
