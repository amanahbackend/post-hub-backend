﻿
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries
{
    public class GetOrderStatusLogsByOrderCodeQuery : IRequest<List<TrackOrderStatusLogDto>>
    {
        public string Code { set; get; }
    }
    public class GetOrderStatusLogsByOrderCodeQueryHandler : IRequestHandler<GetOrderStatusLogsByOrderCodeQuery, List<TrackOrderStatusLogDto>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetOrderStatusLogsByOrderCodeQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;
        }
        public async Task<List<TrackOrderStatusLogDto>> Handle(GetOrderStatusLogsByOrderCodeQuery query, CancellationToken cancellationToken)
        {
            var orderStatusLogs = await _context
                 .OrdersStatusLogs
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(c => (
                 (c.Order.OrderCode != null
                    && c.Order.OrderCode.ToLower().Contains(query.Code.ToLower()))
                 )
                 && !c.IsDeleted
                 &&
                      (!_currentUser.IsBusinessCustomer()
                           || (c.Order.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)
                      ))
                 .ProjectTo<TrackOrderStatusLogDto>(_mapper.ConfigurationProvider).ToListAsync();

            return orderStatusLogs;
        }
    }
}

