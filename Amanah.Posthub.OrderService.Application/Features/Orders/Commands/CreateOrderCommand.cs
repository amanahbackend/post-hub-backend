﻿
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Commands
{
    public class CreateOrderCommand : IRequest<int>
    {
        #region Fileds
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public int StatusId { get; set; }
        public int OrderTypeId { get; set; }
        public int ServiceSectorId { get; set; }
        public int ServiceTypeId { get; set; }//>>

        public DateTime IssueAt { get; set; }
        public DateTime ReadyAt { get; set; }//>>
        public DateTime StartedAt { get; set; }
        public DateTime DeliveryBefore { get; set; }
        public DateTime FulfilledAt { get; set; }

        public decimal TotalWeight { get; set; }
        public int WeightUOMId { get; set; }

        public decimal TotalPostage { get; set; }

        public int PaymentMethodId { get; set; }
        public int CurrencyId { get; set; }

        public int? TotalItemCount { get; set; }
        public List<MailItem> MailItems { get; set; }

        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address PickupAddress { get; set; }
        public string PickupNote { get; set; }
        public string DropOffNote { get; set; }

        // Ramzy: need to be checked
        public string UserId { get; set; }

        ///////////////////////////////////////////////////////////////
        /// Business Order

        public int CustomerId { get; set; }
        public int ContactId { get; set; }
        public int ContractId { get; set; }
        public int MailItemsTypeId { get; set; }
        public decimal PostagePerPiece { get; set; }
        public int CollectionFormSerialNo { get; set; }

        public List<AttachmentFile> AttachedImages { get; set; }
        public List<AttachmentFile> AttachedFiles { get; set; }

        /////////////////////////////////////////////////////////////////
        /// Direct Order

        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address DropOffAddress { get; set; }
        public int SenderInfo { get; set; }
        public int ConsigneeInfoId { get; set; }
        public bool IsMatchConsigneeID { get; set; }
        public int ReciverInfoId { get; set; }

        #endregion


    }
}