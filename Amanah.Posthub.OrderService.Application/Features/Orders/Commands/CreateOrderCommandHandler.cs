﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Commands
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, int>
    {
        private readonly ApplicationDbContext _context;
        public CreateOrderCommandHandler(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Handle(CreateOrderCommand command, CancellationToken cancellationToken)
        {
            var order = new Order();

            order.OrdersStatusLog.Add(new OrdersStatusLog()
            {
                CreatedBy = command.UserId,
                CreationTime = DateTime.UtcNow,
                IsDeleted = false,
                OrderStatusId = (int)OrderStatuses.New
            });

            order.OrderCode = command.OrderCode;
            order.StatusId = command.StatusId;
            order.OrderTypeId = command.OrderTypeId;
            order.ServiceSectorId = command.ServiceSectorId;
            order.ServiceTypeId = command.ServiceTypeId;
            order.IssueAt = command.IssueAt;
            order.ReadyAt = command.ReadyAt;
            order.StartedAt = command.StartedAt;
            order.DeliveryBefore = command.DeliveryBefore;
            order.FulfilledAt = command.FulfilledAt;
            order.TotalWeight = command.TotalWeight;
            order.WeightUOMId = command.WeightUOMId;
            order.TotalPostage = command.TotalPostage;
            order.PaymentMethodId = command.PaymentMethodId;
            order.CurrencyId = command.CurrencyId;
            //Ramzy: need to be implemented
            //order.BusinessOrder = command.BusinessOrder;
            //order.DirectOrder = command.DirectOrder;
            //order.MailItemsCount = command.MailItemsCount;
            order.MailItems = command.MailItems;
            order.PickupAddress = command.PickupAddress;
            order.PickupNote = command.PickupNote;
            order.DropOffNote = command.DropOffNote;
            order.CreatedBy = command.UserId;
            order.CreationTime = DateTime.UtcNow;
            order.IsDeleted = false;
           
            int mailItemId = await _context.MailItems.Select(x => x.Id).OrderBy(x => x).LastOrDefaultAsync();

            int counter = mailItemId;  
            ///Get Default Driver 
            foreach (var mailItem in order.MailItems)
            {
                if (string.IsNullOrEmpty(mailItem.MailItemBarCode))
                {
                    counter += 1;
                    mailItem.MailItemBarCode = GenerateItemCodeAsync(counter);
                }
                
                mailItem.DefaultAssignedDriverID = await GetDefaultDriverIdForMailItemAsync(mailItem);
            }


            order.BusinessOrder = new BusinessOrder()
            {
                AttachedFiles = new List<AttachmentFile>()
            };

            _context.Orders.Add(order);
            await _context.SaveChangesAsync();
            return order.Id;
        }





        private async Task<int?> GetDefaultDriverIdForMailItemAsync(MailItem item)
        {
            var GeoFenceBlock = await _context.GeoFenceBlocks
               .Where(x => x.Area == item.DropOffAddress.Area && x.Block == item.DropOffAddress.Block)
               .Include(x => x.GeoFence)
               .FirstOrDefaultAsync();

            var SelectedDriver = await _context.Driver
                .Where(d =>
                   d.DriverDeliveryGeoFences.Any(x => x.GeoFenceId == GeoFenceBlock.GeoFenceId))
               .FirstOrDefaultAsync();
            if (SelectedDriver != null)
            {
                return SelectedDriver.Id;
            }
            else
            {
                return null;
            }

        }

        private string GenerateItemCodeAsync(int ItemId)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("EX");
            sb.Append($"{(ItemId):00000000}");
            return sb.ToString();
        }


    }
}

