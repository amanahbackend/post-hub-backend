﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Commands
{
    public class DeleteOrderByIdCommand : IRequest<int>
    {
        public int Id { get; set; }
        public class DeleteOrderByIdCommandHandler : IRequestHandler<DeleteOrderByIdCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteOrderByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteOrderByIdCommand command, CancellationToken cancellationToken)
            {
                var order = await _context.Orders
                    .Include(o=> o.MailItems)
                    //.Include(s=> s.OrderServices)
                    .Where(a => a.Id == command.Id)
                    .FirstOrDefaultAsync();

                if (order == null) return default;
                order.IsDeleted = true;

                order.MailItems.ForEach(m => { 
                    m.IsDeleted = true; 
                });

                await _context.SaveChangesAsync();
                return order.Id;
            }
        }
    }
}
