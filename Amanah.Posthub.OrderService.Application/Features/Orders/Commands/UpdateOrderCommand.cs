﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Commands
{
    public class UpdateOrderCommand : IRequest<int>
    {
        #region Fileds
        public int Id { get; set; }
        public string OrderCode { get; set; }
        public int StatusId { get; set; }
        public int OrderTypeId { get; set; }
        public int ServiceSectorId { get; set; }
        public int ServiceTypeId { get; set; }

        public DateTime IssueAt { get; set; }
        public DateTime ReadyAt { get; set; }
        public DateTime StartedAt { get; set; }
        public DateTime DeliveryBefore { get; set; }
        public DateTime FulfilledAt { get; set; }

        public decimal TotalWeight { get; set; }
        public int WeightUOMId { get; set; }

        public decimal TotalPostage { get; set; }

        public int PaymentMethodId { get; set; }
        public int CurrencyId { get; set; }

        public int? BusinessOrderId { get; set; }
        // public BusinessOrder BusinessOrder { get; set; }
        public int? DirectOrderId { get; set; }
        public DirectOrder DirectOrder { get; set; }

        public int? TotalItemCount { get; set; }
        public List<MailItem> MailItems { get; set; }


        public Amanah.Posthub.Service.Domain.Addresses.Entities.Address PickupAddress { get; set; }
        public string PickupNote { get; set; }
        public string DropOffNote { get; set; }

        // Ramzy: need to be checked
        public string UserId { get; set; }


        //public int PackingPackageId { get; set; }
        //public decimal OrderPackagePrice { get; set; }


        #endregion
        public class UpdateOrderCommandHandler : IRequestHandler<UpdateOrderCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public UpdateOrderCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(UpdateOrderCommand command, CancellationToken cancellationToken)
            {
                var order = _context.Orders.Where(a => a.Id == command.Id).Include(a => a.DirectOrder).FirstOrDefault();

                if (order == null)
                {
                    return default;
                }
                else
                {
                    order.OrdersStatusLog.Add(new OrdersStatusLog()
                    {
                        CreatedBy = command.UserId,
                        CreationTime = DateTime.UtcNow,
                        IsDeleted = false,
                        OrderStatusId = command.StatusId
                    });

                    order.OrderCode = command.OrderCode;
                    order.StatusId = command.StatusId;
                    order.OrderTypeId = command.OrderTypeId;
                    order.ServiceSectorId = command.ServiceSectorId;
                    order.ServiceTypeId = command.ServiceTypeId;
                    order.IssueAt = command.IssueAt;
                    order.ReadyAt = command.ReadyAt;
                    order.StartedAt = command.StartedAt;
                    order.DeliveryBefore = command.DeliveryBefore;
                    order.FulfilledAt = command.FulfilledAt;
                    order.TotalWeight = command.TotalWeight;
                    order.WeightUOMId = command.WeightUOMId;
                    order.TotalPostage = command.TotalPostage;
                    order.PaymentMethodId = command.PaymentMethodId;
                    order.CurrencyId = command.CurrencyId;
                    //Ramzy: need to be implemented
                    //order.BusinessOrder = command.BusinessOrder;
                    //order.DirectOrder= command.DirectOrder;
                    order.TotalItemCount = command.TotalItemCount;
                    order.MailItems = command.MailItems;


                    int mailItemId = await _context.MailItems.Select(x => x.Id).OrderBy(x => x).LastOrDefaultAsync();
                    int counter = mailItemId;

                    foreach (var mailitem in order.MailItems) { 
                        if (string.IsNullOrEmpty(mailitem.MailItemBarCode))
                        {
                            counter += 1;
                            mailitem.MailItemBarCode = GenerateItemCodeAsync(counter);
                        }
                    }

                    order.PickupAddress = command.PickupAddress;
                    order.PickupNote = command.PickupNote;
                    order.DropOffNote = command.DropOffNote;

                    order.CreatedBy = command.UserId;
                    order.CreationTime = DateTime.UtcNow;
                    order.IsDeleted = false;

                    //order.PackingPackageId = command.PackingPackageId;
                    //order.OrderPackagePrice = command.OrderPackagePrice;

                    await _context.SaveChangesAsync();
                    return order.Id;
                }
            }


            private string GenerateItemCodeAsync(int ItemId)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("EX");
                sb.Append($"{(ItemId):00000000}");
                return sb.ToString();
            }

        }
    }
}