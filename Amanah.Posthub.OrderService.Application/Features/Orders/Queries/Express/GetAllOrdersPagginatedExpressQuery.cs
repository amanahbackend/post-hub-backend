﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Queries.Express
{
    public class GetAllOrdersPagginatedExpressQuery : PaginatedItemsViewModel, IRequest<PagedResult<OrderListResponseDTO>>
    {
        public DateTime? CreationDateFrom { set; get; }
        public DateTime? CreationDateTo { set; get; }
        public DateTime? DeliveryBeforeFrom { set; get; }
        public DateTime? DeliveryBeforeTo { set; get; }
        //public List<int?> ServiceSectorIds { set; get; }
        public List<int> OrderTypeIds { set; get; }
        public List<int> OrderStatusIds { set; get; }
        public List<string> Governorates { set; get; }
        public List<string> Areas { set; get; }
        public string? OrderCode { set; get; }
        public int? ServiceSectorId { set; get; }
        public string MailItemBarCode { set; get; }
        public string ConsigneeName { set; get; }
    }
    public class GetAllOrdersPagginatedExpressQueryHandler : IRequestHandler<GetAllOrdersPagginatedExpressQuery, PagedResult<OrderListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;

        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetAllOrdersPagginatedExpressQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;
        }
        public async Task<PagedResult<OrderListResponseDTO>> Handle(GetAllOrdersPagginatedExpressQuery query, CancellationToken cancellationToken)
        {
            //var ConsigneeInfo = await _context.Contacts.Where(c => c.FirstName == query.ConsigneeName).FirstOrDefaultAsync();
            var ordersList = await _context
                 .Orders
                 .Include(x => x.BusinessOrder)
                 .ThenInclude(x => x.BusinessCustomer)
                 .Include(x => x.MailItems)
                 .IgnoreQueryFilters()
                 .AsQueryable()
                 .Where(x => x.IsDeleted != true
                 && x.IsDraft != true
                 && (query.MailItemBarCode == null
                 || x.MailItems.FirstOrDefault(m => m.MailItemBarCode == query.MailItemBarCode) != null)
                 && (query.ConsigneeName == null
                 || x.MailItems.FirstOrDefault(m => m.ConsigneeInfo.FirstName == query.ConsigneeName) != null)
                 && (query.ServiceSectorId == null
                 || x.ServiceSectorId == query.ServiceSectorId)
                 && (query.OrderCode == null
                 || x.OrderCode == query.OrderCode)
                 && (query.Areas == null
                 || !query.Areas.Any()
                 || query.Areas.Contains(x.BusinessOrder.Contact.Address.Area)
                 || query.Areas.Contains(x.DirectOrder.ReciverInfo.Address.Area))
                 && (query.Governorates == null
                 || !query.Governorates.Any()
                 || query.Governorates.Contains(x.BusinessOrder.Contact.Address.Governorate)
                 || query.Governorates.Contains(x.DirectOrder.ReciverInfo.Address.Governorate))
                 && (query.OrderStatusIds == null
                 || !query.OrderStatusIds.Any()
                 || query.OrderStatusIds.Contains(x.StatusId ?? 0))
                 && (query.OrderTypeIds == null
                 || !query.OrderTypeIds.Any()
                 || query.OrderTypeIds.Contains(x.OrderTypeId ?? 0))
                 //&& (query.ServiceSectorIds == null
                 //|| !query.ServiceSectorIds.Any()
                 //|| query.ServiceSectorIds.Contains(x.ServiceSectorId))
                 && (!query.CreationDateFrom.HasValue
                 || (x.CreationTime.Date >= query.CreationDateFrom.Value.Date))
                 && (!query.CreationDateTo.HasValue
                 || (x.CreationTime.Date <= query.CreationDateTo.Value.Date))
                 && (!query.DeliveryBeforeFrom.HasValue
                 || (x.DeliveryBefore.Value.Date >= query.DeliveryBeforeFrom.Value.Date))
                 && (!query.DeliveryBeforeTo.HasValue
                 || (x.DeliveryBefore.Value.Date <= query.DeliveryBeforeTo.Value.Date))
                 && (!_currentUser.IsBusinessCustomer()
                 || (x.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)))
                 .OrderByDescending(c => c.Id)
                 .AsNoTracking()
                 .ProjectTo<OrderListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(query);

            MapPaymentStatus(ordersList);
            return ordersList;
        }



        private void MapPaymentStatus(PagedResult<OrderListResponseDTO> ordersList)
        {
            var transactionsList = _context.PaymentTransactions
                .Select(a => new
                {
                    Id = a.Id,
                    ReferenceId = a.ReferenceId,
                    IsSuccessTransaction = a.IsSuccessTransaction,
                    PaymentGateway = a.PaymentRequestDetails != null ? ((JObject)JsonConvert.DeserializeObject(a.PaymentRequestDetails)).Value<string>("PaymentGateway") : null,
                }).ToList();

            Parallel.ForEach(ordersList.Result, item =>
            {
                var isSuccessTrans = transactionsList.Where(x => x.ReferenceId == item.Id.ToString()).OrderBy(x => x.Id).ToList();
                item.OrderPaymentStatus = (isSuccessTrans.LastOrDefault()?.IsSuccessTransaction == true) ? "Success" : (isSuccessTrans.LastOrDefault()?.PaymentGateway == "cash" && isSuccessTrans.LastOrDefault()?.IsSuccessTransaction == null) ? "Pending" : "Fail";
            });
        }

    }
}

