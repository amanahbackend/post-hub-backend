﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Queries
{

    public class GetAllOrdersForExportToExcelQuery : PaginatedItemsViewModel, IRequest<List<OrderListExportToExcelResponseDTO>>
    {
        public DateTime? CreationDateFrom { set; get; }
        public DateTime? CreationDateTo { set; get; }
        public DateTime? DeliveryBeforeFrom { set; get; }
        public DateTime? DeliveryBeforeTo { set; get; }
        //public List<int?> ServiceSectorIds { set; get; }
        public List<int> OrderTypeIds { set; get; }
        public List<int> OrderStatusIds { set; get; }
        public List<string> Governorates { set; get; }
        public List<string> Areas { set; get; }
        public string? OrderCode { set; get; }
        public int? ServiceSectorId { get; set; }
    }
    public class GetAllOrdersForExportToExcelQueryHandler : IRequestHandler<GetAllOrdersForExportToExcelQuery, List<OrderListExportToExcelResponseDTO>>
    {
        private readonly ApplicationDbContext _context;

        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetAllOrdersForExportToExcelQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;
        }
        public async Task<List<OrderListExportToExcelResponseDTO>> Handle(GetAllOrdersForExportToExcelQuery query, CancellationToken cancellationToken)
        {
            var ordersList = await _context
                 .Orders
                 .Include(x => x.BusinessOrder)
                 .ThenInclude(x => x.BusinessCustomer)
                 .IgnoreQueryFilters()
                 .AsQueryable()
                 .Where(x => x.IsDeleted != true 
                 && x.IsDraft != true
                 && (query.ServiceSectorId == null
                 || x.ServiceSectorId == query.ServiceSectorId)
                 && (query.OrderCode == null
                 || x.OrderCode == query.OrderCode)
                 && (query.Areas == null
                 || !query.Areas.Any()
                 || query.Areas.Contains(x.BusinessOrder.Contact.Address.Area)
                 || query.Areas.Contains(x.DirectOrder.ReciverInfo.Address.Area))
                 && (query.Governorates == null
                 || !query.Governorates.Any()
                 || query.Governorates.Contains(x.BusinessOrder.Contact.Address.Governorate)
                 || query.Governorates.Contains(x.DirectOrder.ReciverInfo.Address.Governorate))
                 && (query.OrderStatusIds == null
                 || !query.OrderStatusIds.Any()
                 || query.OrderStatusIds.Contains(x.StatusId ?? 0))
                 && (query.OrderTypeIds == null
                 || !query.OrderTypeIds.Any()
                 || query.OrderTypeIds.Contains(x.OrderTypeId ?? 0))
                 //&& (query.ServiceSectorIds == null
                 //|| !query.ServiceSectorIds.Any()
                 //|| query.ServiceSectorIds.Contains(x.ServiceSectorId))
                 && (!query.CreationDateFrom.HasValue
                 || (x.CreationTime.Date >= query.CreationDateFrom.Value.Date))
                 && (!query.CreationDateTo.HasValue
                 || (x.CreationTime.Date <= query.CreationDateTo.Value.Date))
                 && (!query.DeliveryBeforeFrom.HasValue
                 || (x.DeliveryBefore.Value.Date >= query.DeliveryBeforeFrom.Value.Date))
                 && (!query.DeliveryBeforeTo.HasValue
                 || (x.DeliveryBefore.Value.Date <= query.DeliveryBeforeTo.Value.Date))
                 && (!_currentUser.IsBusinessCustomer()
                 || (x.BusinessOrder.BusinessCustomerId == _currentUser.BusinessCustomerId)))
                 .OrderByDescending(c => c.Id)
                 .AsNoTracking()
                 .ProjectTo<OrderListExportToExcelResponseDTO>(_mapper.ConfigurationProvider)
                 .ToListAsync();

            return ordersList;
        }
    }
}
