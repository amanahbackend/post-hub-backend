﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Queries
{
    public class GetOrderByIdQuery : IRequest<Order>
    {
        public int Id { get; set; }
        public class GetOrderByIdQueryHandler : IRequestHandler<GetOrderByIdQuery, Order>
        {
            private readonly ApplicationDbContext _context;
            public GetOrderByIdQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<Order> Handle(GetOrderByIdQuery query, CancellationToken cancellationToken)
            {
                var order = _context.Orders.Where(a => a.Id == query.Id).FirstOrDefault();
                if (order == null) return null;
                return order;
            }
        }
    }
}
