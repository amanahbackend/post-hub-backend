﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Queries
{
    public class GetOrderForDispatchQuery : QueryListReq, IRequest<Result>
    {

        public class GetOrderForDispatchQueryHandler : IRequestHandler<GetOrderForDispatchQuery, Result>
        {

            private readonly ApplicationDbContext _context;
            private ICurrentUser _currentUser;
            public GetOrderForDispatchQueryHandler(
                ApplicationDbContext context,
                ICurrentUser currentUser)
            {
                _context = context;
                _currentUser = currentUser;
            }
            public async Task<Result> Handle(GetOrderForDispatchQuery query, CancellationToken cancellationToken)
            {
                var result = new Result();

                result.TotalCount = await _context.Orders
                    .Include(o => o.MailItems)
                    .Where(o => !o.IsDeleted
                    && !o.IsDraft
                    && o.ServiceSectorId == (int)ServiceSectors.LocalPost
                    && o.StatusId == (int)OrderStatuses.New
                    && (o.MailItems != null
                    && o.MailItems.Any(a => !a.IsDeleted && (a.StatusId == (int)MailItemStatuses.New || a.StatusId == (int)MailItemStatuses.ReadyForDispatch))))
                    .Select(o => o.Id)
                    .CountAsync();

                if (result.TotalCount < 1)
                {
                    result.Error = "NotFound";
                    return result;
                }

                result.Value = await _context.Orders
                    .Include(o => o.MailItems)
                    .Include(o => o.Status)
                    .Include(o => o.OrderType)
                    .Include(o => o.BusinessOrder)
                    .ThenInclude(o => o.BusinessCustomer)
                    .Where(o => !o.IsDeleted
                    && !o.IsDraft
                    && o.ServiceSectorId == (int)ServiceSectors.LocalPost
                    && o.StatusId == (int)OrderStatuses.New
                    && o.MailItems != null
                    && (o.MailItems.Any(a => !a.IsDeleted && (a.StatusId == (int)MailItemStatuses.New || a.StatusId == (int)MailItemStatuses.ReadyForDispatch))))
                    .OrderByDescending(c => c.Id)
                    .Select(o => new OrderForDispatch()
                    {
                        Id = o.Id,
                        OrderCode = o.OrderCode,
                        OrderType = o.OrderType.Name,

                        //IsBusiness = o.BusinessOrderId.HasValue,
                        BusinessName = o.BusinessOrderId.HasValue ? o.BusinessOrder.BusinessCustomer.BusinessType.Name_ar : o.DirectOrder.Customer.Name,
                        CustomerName = o.BusinessOrderId.HasValue ? o.BusinessOrder.BusinessCustomer.BusinessName : o.DirectOrder.Customer.Name,
                        Status = o.Status.Name,
                        ItemsNo = o.MailItems != null ? o.MailItems.Count(a => !a.IsDeleted && (a.StatusId == (int)MailItemStatuses.New || a.StatusId == (int)MailItemStatuses.ReadyForDispatch)) : 0
                    })
                    .Skip((query.PageNumber - 1) * query.PageSize)
                    .Take(query.PageSize)
                    .ToListAsync();

                return result;
            }
        }

    }
}
