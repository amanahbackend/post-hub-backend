﻿using Amanah.Posthub.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Queries
{
    public class GetOrderListQuery : IRequest<IEnumerable<object>>
    {

        public class GetAllOrdersQueryHandler : IRequestHandler<GetOrderListQuery, IEnumerable<object>>
        {
            private readonly ApplicationDbContext _context;
            public GetAllOrdersQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<object>> Handle(GetOrderListQuery query, CancellationToken cancellationToken)
            {
                // Ramzy: need items list
                var orderList = await _context.Orders.Select(o => new { o.OrderCode }).ToListAsync();
                if (orderList == null)
                {
                    return null;
                }
                return orderList.AsReadOnly();
            }
        }

    }
}
