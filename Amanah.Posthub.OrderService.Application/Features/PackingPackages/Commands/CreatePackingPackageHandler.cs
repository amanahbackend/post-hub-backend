﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands
{

    public class CreatePackingPackageHandler : IRequestHandler<CreatePackingPackageInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<PackingPackage> _packingPackageRepository;
        private readonly ILocalizer _localizer;

        public CreatePackingPackageHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<PackingPackage> packingPackageRepository,
            ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _packingPackageRepository = packingPackageRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(CreatePackingPackageInputDTO command, CancellationToken cancellationToken)
        {
            PackingPackage packingPackage = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                if (command != null)
                {
                    bool packingPackageCountryExist = false;
                    var packingPackageEF = _context.PackingPackages.IgnoreQueryFilters()
                    .Include(c => c.PackingPackageCountries)
                    .Where(c => !c.IsDeleted
                    && (c.Name_ar == command.Name_ar || c.Name_en == command.Name_en)).FirstOrDefault();

                    //if (command.PackingPackageCountries.Any() )
                    //{
                        
                    //        packingPackageCountryExist = _context.PackingPackageCountries.IgnoreQueryFilters().Where(c => !c.IsDeleted && command.PackingPackageCountries
                    //                                                     .Select(c => c.CountryId).ToList()
                    //                                                     .Contains(c.CountryId)).ToList().Any();
                    //}
                   
                    if (packingPackageEF == null && !packingPackageCountryExist)
                    {
                        packingPackage = _mapper.Map<PackingPackage>(command);

                        _packingPackageRepository.Add(packingPackage);
                        await _unityOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        throw new DomainException(_localizer[Keys.Messages.PackingPackageExist]);

                    }



                }
            });
            if (result)
            {
                return packingPackage.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
