﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands
{

    public class DeletePackingPackageHandler : IRequestHandler<DeletePackingPackageInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<PackingPackage> _packingPackageRepository;
        public DeletePackingPackageHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<PackingPackage> packingPackageRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _packingPackageRepository = packingPackageRepository;
        }
        public async Task<int> Handle(DeletePackingPackageInputDTO command, CancellationToken cancellationToken)
        {
            PackingPackage packingPackage = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                if (command != null)
                {


                    packingPackage = _context.PackingPackages.Where(c => c.Id == command.Id)
                     .Include(c => c.PackingPackageCountries)
                     .FirstOrDefault();
                    if (packingPackage != null)
                    {
                        packingPackage.IsDeleted = true;
                        _packingPackageRepository.Update(packingPackage);


                    }

                }
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return packingPackage.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
