﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands
{

    public class UpdatePackingPackagesHandler : IRequestHandler<UpdatePackingPackageInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<PackingPackage> _packingPackageRepository;
        private readonly ILocalizer _localizer;

        public UpdatePackingPackagesHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<PackingPackage> packingPackageRepository,
            ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _packingPackageRepository = packingPackageRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(UpdatePackingPackageInputDTO command, CancellationToken cancellationToken)
        {
            PackingPackage packingPackage = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                if (command != null)
                {

                    //  packingPackage = _mapper.Map<PackingPackage>(command);
                    packingPackage = _context.PackingPackages.Where(c => c.Id == command.Id)
                    .Include(c => c.PackingPackageCountries.Where(x => !x.IsDeleted))
                    .FirstOrDefault();
                    if (packingPackage != null)
                    {
                        bool packingPackageCountryExist = false;
                        var packingPackageEF = _context.PackingPackages.IgnoreQueryFilters()
                        .Include(c => c.PackingPackageCountries)
                        .Where(c => !c.IsDeleted && c.Id != command.Id
                        && (c.Name_ar == command.Name_ar || c.Name_en == command.Name_en)).FirstOrDefault();

                        //if (command.PackingPackageCountries.Any())
                        //{
                        //    packingPackageCountryExist = _context.PackingPackageCountries.IgnoreQueryFilters()
                        //                                                 .Where(c => !c.IsDeleted && c.PackingPackageId != command.Id
                        //                                                 && command.PackingPackageCountries
                        //                                                 .Select(c => c.CountryId).ToList()
                        //                                                 .Contains(c.CountryId)).ToList().Any();
                        //}

                        if (packingPackageEF == null && !packingPackageCountryExist)
                        {
                            packingPackage.Name_ar = command.Name_ar;
                            packingPackage.Name_en = command.Name_en;
                            packingPackage.Description_ar = command.Description_ar;
                            packingPackage.Description_en = command.Description_en;
                            packingPackage.DimensionHight = command.DimensionHight;
                            packingPackage.DimensionLength = command.DimensionLength;
                            packingPackage.DimensionWidth = command.DimensionWidth;
                            packingPackage.MaxWeight = command.MaxWeight;

                            if (command.PackingPackageCountries != null)
                            {
                                if (packingPackage.PackingPackageCountries == null)
                                {
                                    packingPackage.PackingPackageCountries = command.PackingPackageCountries.Select(c => new PackingPackageCountry
                                    {
                                        Price = c.Price,
                                        CountryId = c.CountryId,
                                    }).ToList();
                                }
                                else
                                {
                                    foreach (var item in command.PackingPackageCountries)
                                    {
                                        if (packingPackage.PackingPackageCountries.Where(c => c.Id == item.Id && c.Id > 0).Any())
                                        {
                                            var packingPackageCountry = packingPackage.PackingPackageCountries.Where(c => c.Id == item.Id).FirstOrDefault();
                                            packingPackageCountry.Price = item.Price;
                                            packingPackageCountry.CountryId = item.CountryId;

                                        }
                                        else
                                        {
                                            var packingPackageCountry = new PackingPackageCountry();
                                            packingPackageCountry.Price = item.Price;
                                            packingPackageCountry.CountryId = item.CountryId;


                                            packingPackage.PackingPackageCountries.Add(packingPackageCountry);
                                        }
                                    }
                                }
                                var commandPackingPackageCountries = command.PackingPackageCountries.Select(cm => cm.Id);
                                var toBeDeletedPackingPackageCountries = packingPackage.PackingPackageCountries.Where(m => !commandPackingPackageCountries.Contains(m.Id));
                                if (toBeDeletedPackingPackageCountries != null && toBeDeletedPackingPackageCountries.Count() > 0)
                                {
                                    foreach (var packingPackageCountry in toBeDeletedPackingPackageCountries)
                                    {
                                        packingPackageCountry.IsDeleted = true;
                                        _context.PackingPackageCountries.Update(packingPackageCountry);
                                        //await _unityOfWork.SaveChangesAsync();

                                    }
                                }
                            }
                            else
                            {
                                packingPackage.PackingPackageCountries = new List<PackingPackageCountry>();
                            }
                            _packingPackageRepository.Update(packingPackage);
                            await _unityOfWork.SaveChangesAsync();

                        }
                        else
                        {
                            throw new DomainException(_localizer[Keys.Messages.PackingPackageExist]);

                        }
                    }
                }

            });
            if (result)
            {
                return packingPackage.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
