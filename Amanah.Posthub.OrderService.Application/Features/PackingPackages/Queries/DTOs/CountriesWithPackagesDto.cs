﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs
{
    public class CountriesWithPackagesDto
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Code { set; get; }
        public string Flag { set; get; }
        public string FlagUrl { set; get; }
        public string TopLevel { set; get; }
        public int? CourierZoneId { set; get; }
        public string CourierZoneName { set; get; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
