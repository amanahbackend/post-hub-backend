﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs
{
    public class CountriesWithPackagesForMobileDTO
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public string Code { set; get; }
        public IEnumerable<GovernratesForMobileDTO> Governrates { get; set; }
    }
}
