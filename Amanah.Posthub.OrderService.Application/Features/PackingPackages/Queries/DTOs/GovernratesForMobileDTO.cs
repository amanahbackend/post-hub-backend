﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs
{
    public class GovernratesForMobileDTO
    {
        public int Id { get; set; }
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public IEnumerable<AreasForMobileDTO> Areas { get; set; }
    }
}
