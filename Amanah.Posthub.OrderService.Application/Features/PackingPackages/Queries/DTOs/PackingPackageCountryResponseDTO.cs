﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs
{
    public class PackingPackageCountryResponseDTO
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public int CountryId { get; set; }
    }
}
