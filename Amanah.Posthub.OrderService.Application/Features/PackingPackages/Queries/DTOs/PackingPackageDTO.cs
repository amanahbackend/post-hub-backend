﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs
{
    public class PackingPackageDTO
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public string Description_en { get; set; }
        public string Description_ar { get; set; }
        public decimal DimensionWidth { get; set; }
        public decimal DimensionLength { get; set; }
        public decimal DimensionHight { get; set; }
        public decimal MaxWeight { get; set; }
        public List<PackingPackageCountryResponseDTO> PackingPackageCountries { get; set; }
    }
}
