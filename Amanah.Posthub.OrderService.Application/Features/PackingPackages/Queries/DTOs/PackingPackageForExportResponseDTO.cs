﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs
{
    public class PackingPackageForExportResponseDTO
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
    }
}
