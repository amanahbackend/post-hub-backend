﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs
{
    public class PackingPackageForMobileDTO
    {
        public int Id { get; set; }
        public string Name{ get; set; }
        public string Description { get; set; }
        public decimal DimensionWidth { get; set; }
        public decimal DimensionLength { get; set; }
        public decimal DimensionHight { get; set; }
        public decimal MaxWeight { get; set; }
        public decimal Price { get; set; }
    }
}
