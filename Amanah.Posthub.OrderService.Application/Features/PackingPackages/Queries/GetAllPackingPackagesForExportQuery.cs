﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;


namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries
{
   

    public class GetAllPackingPackagesForExportQuery : PaginatedItemsViewModel, IRequest<List<PackingPackageForExportResponseDTO>>
    {
    }
    public class GetAllPackingPackagesForExportQueryHandler : IRequestHandler<GetAllPackingPackagesForExportQuery, List<PackingPackageForExportResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllPackingPackagesForExportQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<PackingPackageForExportResponseDTO>> Handle(GetAllPackingPackagesForExportQuery query, CancellationToken cancellationToken)
        {
            var PackingPackageList = new List<PackingPackageForExportResponseDTO>();

            PackingPackageList = await _context
                       .PackingPackages
                       .AsQueryable()
                       .AsNoTracking()
                       .IgnoreQueryFilters()
                       .Where(c => !c.IsDeleted)
                       .OrderBy(c => c.Id)
                       .Select(c => new PackingPackageForExportResponseDTO
                       {
                           Name_ar = c.Name_ar,
                           Name_en = c.Name_en,
                       }).ToListAsync();

            if (PackingPackageList == null)
            {
                return null;
            }
            return PackingPackageList;
        }
    }
}
