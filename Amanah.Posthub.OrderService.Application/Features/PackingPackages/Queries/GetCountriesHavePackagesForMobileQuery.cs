﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs;
using Amanah.Posthub.SharedKernel.Utilites.Helpers;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries
{

    public class GetCountriesHavePackagesForMobileQuery : IRequest<IEnumerable<CountriesWithPackagesForMobileDTO>>
    {

        public class GetCountriesHavePackagesForMobileQueryHandler : IRequestHandler<GetCountriesHavePackagesForMobileQuery, IEnumerable<CountriesWithPackagesForMobileDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetCountriesHavePackagesForMobileQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<IEnumerable<CountriesWithPackagesForMobileDTO>> Handle(GetCountriesHavePackagesForMobileQuery query, CancellationToken cancellationToken)
            {
                var items = _context
                     .PackingPackageCountries
                     .Include(c => c.PackingPackage)
                     .Include(c => c.Country)
                     .ThenInclude(c => c.Governrates)
                     .ThenInclude(g => g.Areas)
                     .ThenInclude(a => a.Blocks)
                     .IgnoreQueryFilters()
                     .Where(c => !c.PackingPackage.IsDeleted && !c.IsDeleted && c.PackingPackageId > 0)
                     .AsQueryable()
                     .AsNoTracking()
                     .Select(c => new CountriesWithPackagesForMobileDTO
                     {
                         Id = c.Country.Id,
                         Name = c.Country.Name,
                         Governrates = c.Country.Governrates.Select(g => new GovernratesForMobileDTO
                         {
                             Id = g.Id,
                             NameEN = g.NameEN,
                             NameAR = g.NameAR,
                             Areas = g.Areas.Select(a => new AreasForMobileDTO
                             {
                                 Id = a.Id,
                                 NameEN = a.NameEN,
                                 NameAR = a.NameAR,

                                 Blocks = a.Blocks.Select(c => new BlocksForMobileDTO { BlockNo = c.BlockNo, Id = c.Id }),
                             })
                         }),
                     }).AsEnumerable();
                var distinctItems = new List<CountriesWithPackagesForMobileDTO>();

                if (items.Any())
                {
                    distinctItems = items.DistinctBy(c => c.Id).ToList();
                }
                var kuwaitCountry = _context
                  .Country
                  .Include(c => c.Governrates)
                  .ThenInclude(g => g.Areas)
                  .ThenInclude(a => a.Blocks)
                  .IgnoreQueryFilters()
                  .AsQueryable()
                  .AsNoTracking()
                  .IgnoreQueryFilters()
                  .Where(c => !c.IsDeleted && c.Id == (int)Countries.Kuwait)
                  .Select(c => new CountriesWithPackagesForMobileDTO
                  {
                      Id = c.Id,
                      Name = c.Name,
                      Governrates = c.Governrates.Select(g => new GovernratesForMobileDTO
                      {
                          Id = g.Id,
                          NameEN = g.NameEN,
                          NameAR = g.NameAR,
                          Areas = g.Areas.Select(a => new AreasForMobileDTO
                          {
                              Id = a.Id,
                              NameEN = a.NameEN,
                              NameAR = a.NameAR,
                              Blocks = a.Blocks.Select(b => new BlocksForMobileDTO
                              {
                                  Id = b.Id,
                                  BlockNo = b.BlockNo
                              })
                          })
                      }),
                  }).FirstOrDefault();

                if (distinctItems.Any())
                {
                    distinctItems.Add(kuwaitCountry);
                    return distinctItems;

                }
                return new List<CountriesWithPackagesForMobileDTO>() { kuwaitCountry };
            }
        }
    }
}
