﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries
{
    public class GetCountriesHavePackagesQuery : IRequest<List<CountriesWithPackagesDto>>
    {

    }

    public class GetCountriesHavePackagesQueryHandler : IRequestHandler<GetCountriesHavePackagesQuery, List<CountriesWithPackagesDto>>
    {
        private readonly ApplicationDbContext _context;

        public GetCountriesHavePackagesQueryHandler(ApplicationDbContext context)
        {
            this._context = context;
        }

        public async Task<List<CountriesWithPackagesDto>> Handle(GetCountriesHavePackagesQuery request, CancellationToken cancellationToken)
        {
            var packages = await _context
                       .PackingPackageCountries.IgnoreQueryFilters()
                       .Include(c => c.Country)
                       .Include(c => c.PackingPackage)
                       .Where(c => !c.PackingPackage.IsDeleted && !c.IsDeleted && c.PackingPackageId > 0)
                       .Select(p => new CountriesWithPackagesDto
                       {
                           Id = p.Country.Id,
                           Code = p.Country.Code,
                           CourierZoneId = p.Country.CourierZoneId,
                           CourierZoneName = p.Country.CourierZone.Name,
                           Flag = p.Country.Flag,
                           FlagUrl = p.Country.Flag,
                           IsActive = p.Country.IsActive,
                           IsDeleted = p.Country.IsDeleted,
                           IsSystem = p.Country.IsSystem,
                           Name = p.Country.Name,
                           TopLevel = p.Country.TopLevel
                       }).Distinct()
                       .ToListAsync();

            return packages;
        }
    }
}
