﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries
{
    public class GetPackagesByCountryIdQuery : IRequest<List<PackagesWithPriceDto>>
    {
        public int CountryId { get; set; }
    }

    public class GetAllPackingPackageQueryHandler : IRequestHandler<GetPackagesByCountryIdQuery, List<PackagesWithPriceDto>>
    {
        private readonly ApplicationDbContext _context;

        public GetAllPackingPackageQueryHandler(ApplicationDbContext context)
        {
            this._context = context;
        }

        public async Task<List<PackagesWithPriceDto>> Handle(GetPackagesByCountryIdQuery request, CancellationToken cancellationToken)
        {
            var packages = await _context
                       .PackingPackageCountries
                       .IgnoreQueryFilters()
                       .Include(p => p.PackingPackage)
                       .Where(c => !c.PackingPackage.IsDeleted && !c.IsDeleted && c.CountryId == request.CountryId)
                       .Select(p => new PackagesWithPriceDto
                       {
                           Id = p.PackingPackage.Id,
                           Name_ar = p.PackingPackage.Name_ar,
                           Name_en = p.PackingPackage.Name_en,
                           Price = p.Price,
                           Name = Thread.CurrentThread.CurrentCulture.IsArabic() ? p.PackingPackage.Name_ar : p.PackingPackage.Name_en,
                           PackageCountryId=p.Id
                       })
                       .ToListAsync();

            return packages;
        }
    }
}
