﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries
{

    public class GetPackingPackageByCountryIdForMobileQuery : IRequest<List<PackingPackageForMobileDTO>>
    {
        public int CountryId { get; set; }
    }

    public class GetPackingPackageByCountryIdForMobileQueryHandler : IRequestHandler<GetPackingPackageByCountryIdForMobileQuery, List<PackingPackageForMobileDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetPackingPackageByCountryIdForMobileQueryHandler(ApplicationDbContext context, IMapper mapper)
        {
            this._context = context;
            _mapper = mapper;
        }

        public async Task<List<PackingPackageForMobileDTO>> Handle(GetPackingPackageByCountryIdForMobileQuery query, CancellationToken cancellationToken)
        {
            var PackingPackageList = await _context
                       .PackingPackageCountries
                       .Include(c => c.Country)
                       .Include(p => p.PackingPackage)
                       .Where(c => !c.PackingPackage.IsDeleted && !c.IsDeleted && c.CountryId == query.CountryId)
                       .IgnoreQueryFilters()
                       .ProjectTo<PackingPackageForMobileDTO>(_mapper.ConfigurationProvider)
                       .ToListAsync();

            return PackingPackageList;
        }
    }
}
