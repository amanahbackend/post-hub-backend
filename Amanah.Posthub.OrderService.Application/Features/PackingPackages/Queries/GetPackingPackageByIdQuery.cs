﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Commands.DTOs;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries
{
   
    public class GetPackingPackageByIdQuery : IRequest<PackingPackageDTO>
    {
        public int Id { get; set; }
        public class GetPackingPackageByIdQueryHandler : IRequestHandler<GetPackingPackageByIdQuery, PackingPackageDTO>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetPackingPackageByIdQueryHandler(
                ApplicationDbContext context,
                IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<PackingPackageDTO> Handle(GetPackingPackageByIdQuery query, CancellationToken cancellationToken)
            {
                var packingPackage = await _context
                    .PackingPackages
                    .AsNoTracking()
                    .Include(c => c.PackingPackageCountries.Where(c=>!c.IsDeleted))
                    .Where(c => !c.IsDeleted && c.Id == query.Id)
                    .FirstOrDefaultAsync();
                if (packingPackage == null)
                {
                    return null;
                }
                var mappedPackingPackage = _mapper.Map<PackingPackageDTO>(packingPackage);
                mappedPackingPackage.PackingPackageCountries = _mapper.Map<List<PackingPackageCountryResponseDTO>>(packingPackage.PackingPackageCountries);

                return mappedPackingPackage;
            }
        }
    }
}
