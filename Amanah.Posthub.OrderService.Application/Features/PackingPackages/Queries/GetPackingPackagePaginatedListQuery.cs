﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;


namespace Amanah.Posthub.OrderService.Application.Features.PackingPackages.Queries
{
   
    public class GetPackingPackagePaginatedListQuery : PaginatedItemsViewModel, IRequest<PagedResult<PackingPackageResponseDTO>>
    {
    }


    public class GetDepartmentPagginatedListQueryHandler : IRequestHandler<GetPackingPackagePaginatedListQuery, PagedResult<PackingPackageResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetDepartmentPagginatedListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PagedResult<PackingPackageResponseDTO>> Handle(GetPackingPackagePaginatedListQuery query, CancellationToken cancellationToken)
        {
            var PackingPackageList = new PagedResult<PackingPackageResponseDTO>();

            PackingPackageList = await _context
                       .PackingPackages
                       .AsQueryable()
                       .AsNoTracking()
                       .IgnoreQueryFilters()
                       .Where(c => !c.IsDeleted)
                       .OrderBy(c => c.Id)
                       .Select(c => new PackingPackageResponseDTO
                       {
                           Id = c.Id,
                           Name_ar = c.Name_ar,
                           Name_en = c.Name_en,
                       })
                       .ToPagedResultAsync(query);

            if (PackingPackageList == null)
            {
                return null;
            }
            return PackingPackageList;
        }
    }
}
