﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using Amanah.Posthub.Service.Domain.PriceList.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands
{
    class CreatePLHandler : IRequestHandler<CreatePLInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<PriceList> _plRepository;


        public CreatePLHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<PriceList> plRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _plRepository = plRepository;
        }
        public async Task<int> Handle(CreatePLInputDTO command, CancellationToken cancellationToken)
        {
            PriceList priceList = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
          {
              priceList = _mapper.Map<PriceList>(command);
              if (command.PlItems.Count > 0 && command.PlItems != null)
              {
                  var mappedLst = _mapper.Map<List<PriceListItems>>(command.PlItems);
                  priceList.PriceListItems = mappedLst;
              }
              _plRepository.Add(priceList);
              await _context.SaveChangesAsync();

          });
            if (result)
            {
                return priceList.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
