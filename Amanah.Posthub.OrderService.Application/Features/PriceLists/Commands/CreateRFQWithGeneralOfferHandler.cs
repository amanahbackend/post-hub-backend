﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using Amanah.Posthub.Service.Domain.PriceList.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands
{
    class CreateRFQWithGeneralOfferHandler : IRequestHandler<CreateRFQWithGeneralOfferInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<RFQ> _rfqRepository;
        private readonly ILocalizer _localizer;

        public CreateRFQWithGeneralOfferHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<RFQ> rfqRepository,
            ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _rfqRepository = rfqRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(CreateRFQWithGeneralOfferInputDTO command, CancellationToken cancellationToken)
        {
            RFQ rfq = null;
            RFQItems rFQItem = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            try
            {
                bool result = await _unityOfWork.RunTransaction(async () =>
                {
                    rfq = _mapper.Map<RFQ>(command);
                    //rfq.Total = rfq.HasAmount == true ? rfq.Amount * rfq.Price : rfq.Quantity * rfq.Price;             
                    await _unityOfWork.SaveChangesAsync();
                    if (command != null)
                    {
                        var rfqEF = _context.RFQ.Where(c => c.PQDate.Value.Date == command.PQDate.Value.Date
                          && c.PQValidToDate.Value.Date == command.ValidToDate.Value.Date && !c.IsDeleted).FirstOrDefault();
                        if (rfqEF == null)
                        {
                            if (command.PqItems != null)
                            {
                                rfq.RFQItemPrices = new List<RFQItems>();
                                rFQItem = _mapper.Map<RFQItems>(command.PqItems);
                                rFQItem.CurrencyId = (int)command.CurrencyId;
                                rFQItem.HasAmount = command.PqItems.HasAmount == null ? false : command.PqItems.HasAmount;
                                rfq.RFQItemPrices.Add(rFQItem);
                            }
                            if (command.RFQItems.Count > 0 && command.RFQItems != null)
                            {
                                RFQItems countryPqItems = null;
                                var mappedLst = _mapper.Map<List<RFQItems>>(command.RFQItems);
                                foreach (var item in mappedLst)
                                {
                                    item.CurrencyId = (int)command.CurrencyId;
                                    //item.RoundTripShippingPrice = item.CommingShippingPrice * item.ToGoShippingPrice;
                                }
                                rfq.RFQItemPrices = mappedLst;
                            }
                            rfq.PriceQuotationStatusId = 2;
                            if (rfq.PQDate.Value.Date == DateTime.UtcNow.Date) rfq.PQDate = rfq.PQDate;
                            if (rfq.PQValidToDate.Value.Date == DateTime.UtcNow.Date) rfq.PQValidToDate = rfq.PQValidToDate;
                            else
                            {
                                rfq.PQDate = rfq.PQDate.Value.AddDays(1);
                                rfq.PQValidToDate = rfq.PQValidToDate.Value.AddDays(1);
                            }

                            rfq.PQCode = await GenerateRFQCodeAsync();
                            _rfqRepository.Add(rfq);
                            await _context.SaveChangesAsync();
                        }
                        else
                        {
                            throw new DomainException(_localizer[Keys.Messages.RFQWithSamepqDateAndValidatetoDateExist]);

                        }
                    }

                });
                if (result)
                {
                    return rfq.Id;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception Ex)
            {
                return 0;
            }

        }


        private async Task<string> GenerateRFQCodeAsync()
        {
            int RFQLastid = await _context.RFQ.Select(x => x.Id).OrderBy(x => x).LastOrDefaultAsync();
            StringBuilder sb = new StringBuilder();
            sb.Append("QUT");
            sb.Append($"{(RFQLastid + 1):00000000}");
            return sb.ToString();
        }

    }
}
