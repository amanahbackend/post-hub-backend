﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO
{
    public class CreatePLInputDTO : IRequest<int>
    {
        public string Code { get; set; }
        public int? StatusId { get; set; }
        public DateTime? ValidFromDate { get; set; }
        public DateTime? ValidToDate { get; set; }
        public int? SectorTypeId { get; set; }
        public List<CreatePLItemsInputDTO> PlItems { get; set; }
    }
}
