﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO
{
    public class CreatePLItemsInputDTO : IRequest<int>
    {
        public int Id { get; set; }
        public string FromArea { get; set; }
        public string ToArea { get; set; }
        public double? FromWeight { get; set; }
        public double? ToWeight { get; set; }
        public int ServiceTime { get; set; }
        public int? ServiceTimeUnitId { get; set; }
        public string ServiceTimeUnit { get; set; }
        public decimal Price { get; set; }
    }
}
