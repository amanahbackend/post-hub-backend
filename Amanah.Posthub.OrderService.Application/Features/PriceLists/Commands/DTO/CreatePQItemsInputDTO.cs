﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO
{
    public class CreatePQItemsInputDTO : IRequest<int>
    {
        public int Id { get; set; }
        public string ItemDescription { get; set; }
        public decimal? ItemPrice { get; set; }
        public int? CurrencyId { get; set; }
        public string CurrencyName { get; set; }
        public string Notes { get; set; }
        public double? Amount { get; set; }
        public bool? HasAmount { get; set; }
        public decimal? Total { get; set; }

    }
}
