﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO
{
    public class CreateRFQInputDTO : IRequest<int>
    {

        public bool? IsGeneralOffer { get; set; }
        public int? CustomerId { get; set; }
       // public string PQCode { get; set; }
        public string LetterTexHtml { get; set; }
        public string LetterSubHtml { get; set; }
        public string PqSubHtml { get; set; }
        public string Services { get; set; }
        public int? StatusId { get; set; }
        public DateTime? PQDate { get; set; }
        public DateTime? ValidToDate { get; set; }
        public int? SectorTypeId { get; set; }
        public int? CurrencyId { get; set; }
        public List<CreateRFQItemsInputDTO> RFQItems { get; set; }
        public CreatePQItemsInputDTO PqItems { get; set; }


    }
}
