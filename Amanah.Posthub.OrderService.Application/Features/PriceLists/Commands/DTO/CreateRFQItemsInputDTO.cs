﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO
{

    public class CreateRFQItemsInputDTO : IRequest<int>
    {
        public int Id { get; set; }
        public bool? ShipppingToMultiCountries { get; set; }
        public int? CountryId { get; set; }
        public double? Weight { get; set; }
        public decimal? ToGoShippingPrice { get; set; }
        public decimal? CommingShippingPrice { get; set; }
        public decimal? RoundTripShippingPrice { get; set; }

    }
}
