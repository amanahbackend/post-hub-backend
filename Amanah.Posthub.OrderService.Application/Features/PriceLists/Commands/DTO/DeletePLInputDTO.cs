﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO
{
    public class DeletePLInputDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
