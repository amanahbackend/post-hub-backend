﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO
{
    public class DeleteRFQInputDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
