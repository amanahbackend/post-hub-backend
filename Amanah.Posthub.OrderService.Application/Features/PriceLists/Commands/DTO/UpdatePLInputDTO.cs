﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO
{
    public class UpdatePLInputDTO : IRequest<int>
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int? ServiceSectorId { get; set; }
        public int?  PriceListStatusId { get; set; }
        public DateTime? ValidFromDate { get; set; }
        public DateTime? ValidToDate { get; set; }
        public int? SectorTypeId { get; set; }
        public List<CreatePLItemsInputDTO> PlItems { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

    }
}
