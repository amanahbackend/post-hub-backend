﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using Amanah.Posthub.Service.Domain.PriceList.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands
{

    public class UpdatePLHandler : IRequestHandler<UpdatePLInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<PriceList> _rfQRepository;


        public UpdatePLHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<PriceList> rfQRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _rfQRepository = rfQRepository;
        }
        public async Task<int> Handle(UpdatePLInputDTO command, CancellationToken cancellationToken)
        {
            PriceList priceList = null;
            PriceListItems pLItem = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                priceList = _mapper.Map<PriceList>(command);
                if (command.PlItems.Count > 0 && command.PlItems != null)
                {
                    var oldPlItem = _context.PriceListItems.IgnoreQueryFilters()
                    .Where(x => ! x.IsDeleted && x.PriceListId == priceList.Id).Skip((command.PageNumber - 1) *(command.PageSize))
                    .Take(command.PageSize).ToList(); 

                    if (command.PlItems.Count > 0 && command.PlItems != null)
                    {
                        if (oldPlItem == null)
                        {
                            oldPlItem = command.PlItems.Select(item => new PriceListItems
                            {
                                ToWeight = item.ToWeight,
                                FromArea = item.FromArea,
                                ToArea = item.ToArea,
                                FromWeight = item.FromWeight,
                                ServiceTime = item.ServiceTime,
                                ServiceTimeUnitId = item.ServiceTimeUnitId,
                                ServiceTimeUnit = item.ServiceTimeUnit,
                                Price = item.Price,
                            }).ToList();
                        }
                        else
                        {
                            foreach (var item in command.PlItems)
                            {
                                if (oldPlItem.Where(c => c.Id == item.Id && item.Id > 0).Any())
                                {
                                    var plItem = oldPlItem.Where(c => c.Id == item.Id).FirstOrDefault();
                                    plItem.ToWeight = item.ToWeight;
                                    plItem.FromArea = item.FromArea;
                                    plItem.ToArea = item.ToArea;
                                    plItem.FromWeight = item.FromWeight;
                                    plItem.ServiceTime = item.ServiceTime;
                                    plItem.ServiceTimeUnitId = item.ServiceTimeUnitId;
                                    plItem.ServiceTimeUnit = item.ServiceTimeUnit;
                                    plItem.Price = item.Price;
                                }
                                else
                                {
                                    var plItem = new PriceListItems();
                                    plItem.ToWeight = item.ToWeight;
                                    plItem.FromArea = item.FromArea;
                                    plItem.ToArea = item.ToArea;
                                    plItem.FromWeight = item.FromWeight;
                                    plItem.ServiceTime = item.ServiceTime;
                                    plItem.ServiceTimeUnitId = item.ServiceTimeUnitId;
                                    plItem.ServiceTimeUnit = item.ServiceTimeUnit;
                                    plItem.Price = item.Price;

                                    oldPlItem.Add(plItem);
                                }
                            }
                        }
                        var commandplItem = command.PlItems.Select(cm => cm.Id);
                        var toBeDeletedplItem = oldPlItem.Where(m => !commandplItem.Contains(m.Id));
                        if (toBeDeletedplItem != null && toBeDeletedplItem.Count() > 0)
                        {
                            foreach (var plitem in toBeDeletedplItem)
                            {
                                plitem.IsDeleted = true;
                                _context.PriceListItems.Update(plitem);
                            }
                        }
                    }
                    var mappedLst = _mapper.Map<List<PriceListItems>>(oldPlItem);
                    priceList.PriceListItems = mappedLst;
                }
                _rfQRepository.Update(priceList);
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return priceList.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
