﻿namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    using AutoMapper;
    using global::Amanah.Posthub.Context;
    using global::Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
    using global::Amanah.Posthub.Service.Domain.PriceList.Entities;
    using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Utilities.Utilites.Exceptions;
    using Utilities.Utilites.Localization;

    namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands
    {
        public class UpdateRFQHandler : IRequestHandler<UpdateRFQDTO, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unityOfWork;
            private readonly IRepository<RFQ> _rfQRepository;
            private readonly ILocalizer _localizer;


            public UpdateRFQHandler(
                ApplicationDbContext context,
                IMapper mapper,
                IUnitOfWork unityOfWork,
                IRepository<RFQ> rfQRepository,
                ILocalizer localizer)
            {
                _context = context;
                _mapper = mapper;
                _unityOfWork = unityOfWork;
                _rfQRepository = rfQRepository;
                _localizer = localizer;
            }
            public async Task<int> Handle(UpdateRFQDTO command, CancellationToken cancellationToken)
            {
                RFQ rfQ = null;
                RFQItems rFQItem = null;
                var oldPqItems = new List<RFQItems>();
                _unityOfWork.IsTenantFilterEnabled = false;

                bool result = await _unityOfWork.RunTransaction(async () =>
                {
                   
                    if (command != null)
                    {
                        rfQ = _mapper.Map<RFQ>(command);

                        var rfqEF = _context.RFQ.Where(c => (command.PQDate.Value.Date >= c.PQDate.Value.Date
                                    && command.ValidToDate.Value.Date <= c.PQValidToDate.Value.Date)
                                    && !c.IsDeleted && c.Id != command.Id).FirstOrDefault();

                        if (rfqEF == null)
                        {
                            if (command.PqItems != null)
                            {
                                rFQItem = _context.RFQItems.IgnoreQueryFilters().Where(x => x.RFQId == rfQ.Id).FirstOrDefault();
                                if (rFQItem == null)
                                {
                                    rFQItem = _mapper.Map<RFQItems>(command.PqItems);
                                }
                                else
                                {
                                    if (rFQItem != null)
                                    {
                                        rFQItem.ItemPrice = command.PqItems.ItemPrice;
                                        rFQItem.HasAmount = command.PqItems.HasAmount;
                                        rFQItem.Description = command.PqItems.ItemDescription;
                                        rFQItem.Quantity = command.PqItems.Amount;
                                    }
                                    else
                                    {
                                        var pqItem = new RFQItems();
                                        rFQItem.ItemPrice = command.PqItems.ItemPrice;
                                        rFQItem.HasAmount = command.PqItems.HasAmount;
                                        rFQItem.Description = command.PqItems.ItemDescription;
                                        rFQItem.Quantity = command.PqItems.Amount;
                                        rFQItem.CurrencyId = (int)command.CurrencyId;

                                        rfQ.RFQItemPrices.Add(rFQItem);
                                    }

                                }

                            }

                            if (command.RFQItems.Count > 0 && command.RFQItems != null)
                            {
                                 oldPqItems = _context.RFQItems.IgnoreQueryFilters().Where(x => x.RFQId == rfQ.Id).ToList();
                                if (oldPqItems == null)
                                {
                                    oldPqItems = _mapper.Map<List<RFQItems>>(command.RFQItems);
                                }
                                else
                                {
                                    foreach (var item in command.RFQItems)
                                    {
                                        if (oldPqItems.Where(c => c.Id == item.Id && item.Id > 0).Any())
                                        {
                                            var pqItem = oldPqItems.Where(c => c.Id == item.Id).FirstOrDefault();
                                          //  pqItem.ShipppingToMultiCountries = item.ShipppingToMultiCountries;
                                            pqItem.CountryId = item.CountryId;
                                            pqItem.Weight = item.Weight;
                                            pqItem.ToGoShippingPrice = item.ToGoShippingPrice;
                                            pqItem.CommingShippingPrice = item.CommingShippingPrice;
                                            pqItem.RoundTripShippingPrice = item.RoundTripShippingPrice;
        }
                                        else
                                        {
                                            var pqItem = new RFQItems();
                                          //  pqItem.ShipppingToMultiCountries = item.ShipppingToMultiCountries;
                                            pqItem.CountryId = item.CountryId;
                                            pqItem.Weight = item.Weight;
                                            pqItem.ToGoShippingPrice = item.ToGoShippingPrice;
                                            pqItem.CommingShippingPrice = item.CommingShippingPrice;
                                            pqItem.RoundTripShippingPrice = item.RoundTripShippingPrice;

                                            oldPqItems.Add(pqItem);
                                        }
                                    }
                                }
                                var commandpqItem = command.RFQItems.Select(cm => cm.Id);
                                var toBeDeletedpqItem = oldPqItems.Where(m => !commandpqItem.Contains(m.Id));
                                if (toBeDeletedpqItem != null && toBeDeletedpqItem.Count() > 0)
                                {
                                    foreach (var pqitem in toBeDeletedpqItem)
                                    {
                                        pqitem.IsDeleted = true;
                                        _context.RFQItems.Update(pqitem);
                                    }
                                }

                                foreach (var item in oldPqItems)
                                {
                                    item.CurrencyId = (int)command.CurrencyId;
                                   
                                }
                            }

                            rfQ.PriceQuotationStatusId = command.StatusId ?? 2;
                            if (command.RFQItems.Count > 0 && command.RFQItems != null)
                            {
                                rfQ.RFQItemPrices = oldPqItems;
                            }
                            _rfQRepository.Update(rfQ);
                            await _unityOfWork.SaveChangesAsync();
                        }
                        else
                        {
                            throw new DomainException(_localizer[Keys.Messages.RFQWithSamepqDateAndValidatetoDateExist]);

                        }
                    }

                });
                if (result)
                {
                    return rfQ.Id;
                }
                else
                {
                    return 0;
                }

            }
        }
    }

}
