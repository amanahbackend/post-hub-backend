﻿namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    using AutoMapper;
    using global::Amanah.Posthub.Context;
    using global::Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
    using global::Amanah.Posthub.Service.Domain.PriceList.Entities;
    using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Utilities.Utilites.Exceptions;
    using Utilities.Utilites.Localization;

    namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands
    {
        public class UpdateRFQWithGeneralOfferHandler : IRequestHandler<UpdateRFQWithGeneralOfferDTO, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unityOfWork;
            private readonly IRepository<RFQ> _rfQRepository;
            private readonly ILocalizer _localizer;


            public UpdateRFQWithGeneralOfferHandler(
                ApplicationDbContext context,
                IMapper mapper,
                IUnitOfWork unityOfWork,
                IRepository<RFQ> rfQRepository,
                ILocalizer localizer)
            {
                _context = context;
                _mapper = mapper;
                _unityOfWork = unityOfWork;
                _rfQRepository = rfQRepository;
                _localizer = localizer;
            }
            public async Task<int> Handle(UpdateRFQWithGeneralOfferDTO command, CancellationToken cancellationToken)
            {
                RFQ rfQ = null;
                RFQItems rFQItem = null;
                _unityOfWork.IsTenantFilterEnabled = false;

                bool result = await _unityOfWork.RunTransaction(async () =>
                {
                    rfQ = _mapper.Map<RFQ>(command);
                    if (command != null)
                    {
                        var rfqEF = _context.RFQ.IgnoreQueryFilters().Where(c => c.PQDate.Value.Date == command.PQDate.Value.Date
                          && c.PQValidToDate.Value.Date == command.ValidToDate.Value.Date && !c.IsDeleted && c.Id != command.Id).FirstOrDefault();
                        if (rfqEF == null)
                        {
                            if (command.PqItems != null)
                            {
                                var oldPqItem = _context.RFQItems.IgnoreQueryFilters().FirstOrDefault(x => x.RFQId == rfQ.Id);
                                _context.RFQItems.Remove(oldPqItem);
                                rfQ.RFQItemPrices = new List<RFQItems>();
                                rFQItem = _mapper.Map<RFQItems>(command.PqItems);
                                rFQItem.CurrencyId = (int)command.CurrencyId;
                                rfQ.RFQItemPrices.Add(rFQItem);
                            }
                            if (command.RFQItems.Count > 0 && command.RFQItems != null)
                            {
                                RFQItems countryPqItems = null;
                                var oldPqItem = _context.RFQItems.IgnoreQueryFilters().Where(x => x.RFQId == rfQ.Id);
                                _context.RFQItems.RemoveRange(oldPqItem);
                                var mappedLst = _mapper.Map<List<RFQItems>>(command.RFQItems);
                                foreach (var item in mappedLst)
                                {
                                    item.CurrencyId = (int)command.CurrencyId;
                                    // item.RoundTripShippingPrice = item.CommingShippingPrice * item.ToGoShippingPrice;
                                }
                                rfQ.RFQItemPrices = mappedLst;
                            }
                            _rfQRepository.Update(rfQ);
                            await _unityOfWork.SaveChangesAsync();
                        }
                        else
                        {
                            throw new DomainException(_localizer[Keys.Messages.RFQWithSamepqDateAndValidatetoDateExist]);

                        }
                    }

                });
                if (result)
                {
                    return rfQ.Id;
                }
                else
                {
                    return 0;
                }

            }
        }
    }

}
