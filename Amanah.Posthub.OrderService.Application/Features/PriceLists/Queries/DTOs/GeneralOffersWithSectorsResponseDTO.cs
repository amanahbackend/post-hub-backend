﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs
{
    public class GeneralOffersWithSectorsResponseDTO
    {       
        public string SectorTypeName { get; set; }
        public int SectorTypeId { get; set; }
        public bool IsGeneralOffer { get; set; }

    }
}
