﻿using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs
{
    public class IneternationalRFQDetailsResponseDTO
    {
        public List<InternationalRFQDetails> internationalRFQDetails { get; set; }
        public decimal? TotalBudget { get; set; }
        public string CustomerName { get; set; }
    }
    public class InternationalRFQDetails
    {
        public string Description { get; set; }
        public decimal? ItemPrice { get; set; }
        public string CountryName { get; set; }
        public double? Quantity { get; set; }
        public double? Weight { get; set; }
        public decimal? ToGoShippingPrice { get; set; }
        public decimal? CommingShippingPrice { get; set; }
        public decimal? RoundTripShippingPrice { get; set; }
    }
}
