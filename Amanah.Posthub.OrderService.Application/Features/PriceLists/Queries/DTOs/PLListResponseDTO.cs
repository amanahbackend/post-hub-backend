﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs
{
    public class PLListResponseDTO
    {
        public int Id { get; set; }
        public int PriceListId { get; set; }
        public string Code { get; set; }
        //public int? ServiceSectorId { get; set; }     
        //public int? PriceListStatusId { get; set; }
        public string PriceListStatus { get; set; }
        public string ServiceSectorName { get; set; }
        public DateTime? ValidToDate { get; set; }
        public string FromArea { get; set; }
        public string ToArea { get; set; }
        public double FromWeight { get; set; }
        public double ToWeight { get; set; }
        public int ServiceTime { get; set; }
        public string ServiceTimeUnit { get; set; }
        public decimal Price { get; set; }
        public int ItemsCount { get; set; }
    }
}
