﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs
{
     public class RFQCountriesResultDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
