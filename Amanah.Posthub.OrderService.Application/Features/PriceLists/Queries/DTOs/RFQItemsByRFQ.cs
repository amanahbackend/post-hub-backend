﻿namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs
{
    public class RFQItemsByRFQ
    {
        public int Id { get; set; }
        public int RFQId { get; set; }
        public int? CurrencyId { get; set; }
        public string CurrencyCode { get; set; }
        public bool? HasAmount { get; set; }
        public decimal? ItemPrice { get; set; }
        public double? Quantity { get; set; }
        public decimal? RoundTripShippingPrice { get; set; }
        public int BusinessCustomerId { get; set; }
        public string BusinessCustomerName { get; set; }
    }
}
