﻿namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs
{
    public class RFQItemsDTO
    {
        public int Id { get; set; }
        public bool? ShipppingToMultiCountries { get; set; }
        public int? CountryId { get; set; }
        public double? Weight { get; set; }
        public decimal? ToGoShippingPrice { get; set; }
        public decimal? CommingShippingPrice { get; set; }
        public decimal? RoundTripShippingPrice { get; set; }
    }
}
