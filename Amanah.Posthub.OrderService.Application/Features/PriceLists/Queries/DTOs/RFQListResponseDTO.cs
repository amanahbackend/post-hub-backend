﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs
{
    public class RFQListResponseDTO
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Code { get; set; }
        public string LetterText { get; set; }
        public string LetterSubject { get; set; }
        public string PqSubject { get; set; }
        public string Services { get; set; }
        public string PQStatusName { get; set; }
        public DateTime? PqDate { get; set; }
        public DateTime? PqValidToDate { get; set; }
        public string SectorTypeName { get; set; }
        public int SectorTypeId { get; set; }
        public decimal Total { get; set; }
    }
}
