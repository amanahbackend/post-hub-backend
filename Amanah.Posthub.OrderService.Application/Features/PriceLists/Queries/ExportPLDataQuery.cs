﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class ExportPLDataQuery : IRequest<List<PLListResponseDTO>>
    {

    }
    public class ExportPLDataQueryHandler : IRequestHandler<ExportPLDataQuery, List<PLListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public ExportPLDataQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<List<PLListResponseDTO>> Handle(ExportPLDataQuery query, CancellationToken cancellationToken)
        {

            var rfqs = await _context
                 .PriceListItems.Include(x => x.PriceList)
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .ProjectTo<PLListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToListAsync();

            return rfqs;
        }
    }





}
