﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetAllPLPagginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<PLListResponseDTO>>
    {
        public List<string> Area { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<int> sectorTypeId { get; set; }
        public List<int> statusId { get; set; }
    }
    public class GetAllPLPagginatedQueryHandler : IRequestHandler<GetAllPLPagginatedQuery, PagedResult<PLListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllPLPagginatedQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<PLListResponseDTO>> Handle(GetAllPLPagginatedQuery filter, CancellationToken cancellationToken)
        {
            var priceLists = await _context
                 .PriceLists
                 .Include(x => x.PriceListItems)
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .ProjectTo<PLListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(filter);

            return priceLists;

            // Ramzy: refactored and left if the customer need multiable PL with filter ////////////////////////
            //var query = _context
            //     .PriceListItems.Include(x => x.PriceList)
            //     .AsQueryable()
            //     .AsNoTracking();

            //if (filter.Area != null && filter.Area.Count > 0)
            //{
            //    query = query.Where(x => filter.Area.Contains(x.FromArea) || filter.Area.Contains(x.ToArea));
            //}
            //if (filter.FromDate != null)
            //{
            //    query = query.Where(x => x.PriceList.ValidFromDate.Value.Date == filter.FromDate.Value.Date);
            //}
            //if (filter.ToDate != null)
            //{
            //    query = query.Where(x => x.PriceList.ValidToDate.Value.Date == filter.ToDate.Value.Date);
            //}
            //if (filter.statusId != null && filter.statusId.Count > 0)
            //{
            //    query = query.Where(x => filter.statusId.Contains((int)x.PriceList.PriceListStatusId));
            //}
            //if (filter.sectorTypeId != null && filter.sectorTypeId.Count > 0)
            //{
            //    query = query.Where(x => filter.sectorTypeId.Contains((int)x.PriceList.ServiceSectorId));
            //}
            //var mapped = await query.ProjectTo<PLListResponseDTO>(_mapper.ConfigurationProvider)
            //  .ToPagedResultAsync(filter);

            //return mapped;
        }
    }





}
