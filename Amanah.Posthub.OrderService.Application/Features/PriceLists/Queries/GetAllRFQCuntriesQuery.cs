﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetAllRFQCuntriesQuery : IRequest<List<RFQCountriesResultDTO>>
    {

        public class GetAllRFQCuntriesQueryHandler : IRequestHandler<GetAllRFQCuntriesQuery, List<RFQCountriesResultDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly ICurrentUser _currentUser;

            public GetAllRFQCuntriesQueryHandler(ApplicationDbContext context, ICurrentUser currentUser)
            {
                _context = context;
                _currentUser = currentUser;
            }
            public async Task<List<RFQCountriesResultDTO>> Handle(GetAllRFQCuntriesQuery query, CancellationToken cancellationToken)
            {
                var admin = await _context.Admins.FirstOrDefaultAsync();

                var countriesEF = _context.RFQItems.IgnoreQueryFilters().AsNoTracking()
                                      .Include(a => a.Country).Where(a => !a.IsDeleted);

                if (admin == null)
                    countriesEF = countriesEF.Where(a => !a.IsDeleted && a.RFQ.BusinessCustomerId == _currentUser.BusinessCustomerId);

                var countries = await countriesEF.Select(a => new RFQCountriesResultDTO
                {
                    Id = a.Country != null ? a.Country.Id : null,
                    Name = a.Country != null ? a.Country.Name : null,
                }).Distinct().ToListAsync();

                return countries;
            }
        }
    }


}
