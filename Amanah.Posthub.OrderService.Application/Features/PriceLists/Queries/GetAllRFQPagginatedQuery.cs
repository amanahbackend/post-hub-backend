﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetAllRFQPagginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<RFQListResponseDTO>>
    {

    }
    public class GetAllRFQPagginatedQueryHandler : IRequestHandler<GetAllRFQPagginatedQuery, PagedResult<RFQListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllRFQPagginatedQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<RFQListResponseDTO>> Handle(GetAllRFQPagginatedQuery query, CancellationToken cancellationToken)
        {


            var rfqs = await _context
                 .RFQ.OrderByDescending(c => c.Id)
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .ProjectTo<RFQListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(query);

            return rfqs;
        }
    }





}
