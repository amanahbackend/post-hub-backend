﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetFilteredPLPagginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<PLListResponseDTO>>
    {
        public string SearchBy { get; set; }
    }
    public class GetFilteredPLPagginatedQueryHandler : IRequestHandler<GetFilteredPLPagginatedQuery, PagedResult<PLListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetFilteredPLPagginatedQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<PLListResponseDTO>> Handle(GetFilteredPLPagginatedQuery filter, CancellationToken cancellationToken)
        {

            var query = _context
                 .PriceListItems
                 .Where(c => !c.IsDeleted)
                 .Include(x => x.PriceList)
                 .AsQueryable().IgnoreQueryFilters().AsNoTracking();
            if (!string.IsNullOrEmpty(filter.SearchBy))
            {

                query = query.Where(pl => pl.PriceList.Code.ToLower().Contains(filter.SearchBy.ToLower())
                     || pl.FromArea.ToLower().Contains(filter.SearchBy.ToLower())
                    || pl.ToArea.ToLower().Contains(filter.SearchBy.ToLower())
                    );
            }

            var rfqs = await query
                 .ProjectTo<PLListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(filter);

            return rfqs;
        }
    }





}
