﻿
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.OrderStatusLogs.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetGenealOffersWithSectorsQuery : IRequest<List<GeneralOffersWithSectorsResponseDTO>>
    {
       
    }
    public class GetGenealOffersWithSectorsQueryHandler : IRequestHandler<GetGenealOffersWithSectorsQuery, List<GeneralOffersWithSectorsResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetGenealOffersWithSectorsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;
        }
        public async Task<List<GeneralOffersWithSectorsResponseDTO>> Handle(GetGenealOffersWithSectorsQuery query, CancellationToken cancellationToken)
        {
            var orderStatusLogs = await _context
                 .RFQ
                 .AsQueryable()
                 .AsNoTracking()
                 .Where(c => c.IsGeneralOffer.Value == true                
                 && !c.IsDeleted
                 )
                 .ProjectTo<GeneralOffersWithSectorsResponseDTO>(_mapper.ConfigurationProvider).ToListAsync();

            return orderStatusLogs;
        }
    }
}

