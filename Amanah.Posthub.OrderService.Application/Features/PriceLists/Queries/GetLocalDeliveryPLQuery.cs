﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetLocalDeliveryPLQuery : IRequest<List<PLListResponseDTO>>
    {
        //    public List<string> Area { get; set; }
        //    public DateTime? FromDate { get; set; }
        //    public DateTime? ToDate { get; set; }
        //    public List<int> sectorTypeId { get; set; }
        //    public List<int> statusId { get; set; }
    }
    public class GetLocalDeliveryPLQueryHandler : IRequestHandler<GetLocalDeliveryPLQuery, List<PLListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetLocalDeliveryPLQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<List<PLListResponseDTO>> Handle(GetLocalDeliveryPLQuery filter, CancellationToken cancellationToken)
        {
            var priceListItems = await _context
                 .PriceListItems
                 .Include(pl => pl.PriceList)
                 .IgnoreQueryFilters()
                 .Where(pl => !pl.IsDeleted
                 && !pl.PriceList.IsDeleted
                 && pl.PriceList.PriceListStatusId == (int)PriceListStatuses.Active
                 && pl.PriceList.ServiceSectorId == (int)SERVICESECTORES.LocalDelivery)
                 .AsQueryable()
                 .AsNoTracking()
                 .ProjectTo<PLListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToListAsync();

            return priceListItems;
        }
    }





}
