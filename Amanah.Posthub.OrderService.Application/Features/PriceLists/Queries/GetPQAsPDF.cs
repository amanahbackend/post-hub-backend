﻿using Amanah.Posthub.Context;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.Infrastructure.ExternalServices.Settings;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using Amanah.Posthub.SharedKernel.Common.Reporting;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReportsManager.Datasets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetPQAsPDF : IRequest<byte[]>
    {
        public int Id { set; get; }
        public bool SendContractToCustomerEmail { set; get; } = false;

    }




    public class GetPQAsPDFHandler : IRequestHandler<GetPQAsPDF, byte[]>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        protected readonly IWebHostEnvironment _webHostEnvirnoment;
        protected readonly IRDLCrenderService _RDLCrenderService;
        private readonly IEmailSenderservice _emailsender;
        private readonly EmailSettings _emailSettings;


        public GetPQAsPDFHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IWebHostEnvironment webHostEnvirnoment,
            IRDLCrenderService RDLCrenderService,
            IEmailSenderservice emailsender,
            IOptions<EmailSettings> emailSettings)
        {
            _context = context;
            _mapper = mapper;
            _webHostEnvirnoment = webHostEnvirnoment;
            _RDLCrenderService = RDLCrenderService;
            _emailsender = emailsender;
            _emailSettings = emailSettings.Value;
        }
        public async Task<byte[]> Handle(GetPQAsPDF query, CancellationToken cancellationToken)
        {

            IneternationalRFQDetailsResponseDTO ineternationalRFQDetailsResponseDTO = new IneternationalRFQDetailsResponseDTO();
            ineternationalRFQDetailsResponseDTO.TotalBudget = 0;
            var rfqsList = await _context
                  .RFQ
                  .Include(c => c.BusinessCustomer)
                  .Include(c => c.RFQItemPrices)
                  .ThenInclude(c => c.Country)
                  .AsQueryable()
                  .AsNoTracking()
                  .IgnoreQueryFilters()
                  .Where(x => x.Id == query.Id).ToListAsync();
            ineternationalRFQDetailsResponseDTO.CustomerName = rfqsList.FirstOrDefault().BusinessCustomer?.BusinessName;

            var mappedPQObj = _mapper.Map<List<InternationalRFQDetails>>(rfqsList.SelectMany(x => x.RFQItemPrices));
            foreach (var item in mappedPQObj)
            {
                ineternationalRFQDetailsResponseDTO.TotalBudget += item.RoundTripShippingPrice;
            }
            ineternationalRFQDetailsResponseDTO.internationalRFQDetails = mappedPQObj;

            if (ineternationalRFQDetailsResponseDTO == null)
            {
                return null;
            }


            string path = $"{this._webHostEnvirnoment.ContentRootPath}\\RDLCTemplates\\PQTemplate.rdlc";
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            //if(contractData.RFQ != null)
            //{
            //    if (contractData.RFQ.Total.HasValue)
            //    {
            //        path = $"{this._webHostEnvirnoment.ContentRootPath}\\ExportedTemplates\\PQTemplate.rdlc";
            //        parameters.Add("RFQTotal", contractData.RFQ.Total.Value.ToString());
            //    }
            //}



            parameters.Add("CustomerName", ineternationalRFQDetailsResponseDTO.CustomerName);
            parameters.Add("TotalValue", ineternationalRFQDetailsResponseDTO.TotalBudget.HasValue ? ineternationalRFQDetailsResponseDTO.TotalBudget.Value.ToString() : "-");

            var DataSet = ineternationalRFQDetailsResponseDTO.internationalRFQDetails.Select(x => new InternationalRFQDetailsDS()
            {
                CommingShippingPrice = x.CommingShippingPrice,
                CountryName = x.CountryName,
                ItemPrice = x.ItemPrice,
                Weight = x.Weight,
                ToGoShippingPrice = x.ToGoShippingPrice,
                RoundTripShippingPrice = x.RoundTripShippingPrice,
                Quantity = x.Quantity,
                Description = x.Description

            }).ToList();


            Dictionary<string, object> DataSources = new Dictionary<string, object>();

            DataSources.Add("InternatioalPQDetails", DataSet);

            try
            {
                var result = _RDLCrenderService.RenderRdlc(path, ExportReportType.Pdf, parameters, DataSources);


                //if (query.SendContractToCustomerEmail)
                //{
                //    var attchments = new List<byte[]>();

                //    attchments.Add(result);

                //    var body = string.Format(MailBodyTemplates.SendContractMail, contractData.BusinessCustomer.MainContact.User.FullName, _emailSettings.SenderName);
                //    await _emailsender.SendEmailAsync($"Post Express Contract ", body, new List<string> { contractData.BusinessCustomer.MainContact.User.Email }, attchments);

                //}
                return result;

            }
            catch (Exception e)
            {
                throw new DomainException(e.Message);
            }

        }
    }

}
