﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetRFQItemsByBusinessCustomerIdQuery : IRequest<List<RFQItemsDTO>>
    {
        public int BusinessCustomerId { get; set; }

        public class GetRFQDetailsQueryHandler : IRequestHandler<GetRFQItemsByBusinessCustomerIdQuery, List<RFQItemsDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetRFQDetailsQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<List<RFQItemsDTO>> Handle(GetRFQItemsByBusinessCustomerIdQuery query, CancellationToken cancellationToken)
            {

                var rfqsItems = await _context
                     .RFQItems
                     .AsQueryable()
                     .AsNoTracking()
                     .IgnoreQueryFilters()
                     .Where(x => x.RFQ.BusinessCustomerId == query.BusinessCustomerId)
                     .ProjectTo<RFQItemsDTO>(_mapper.ConfigurationProvider)
                     .ToListAsync();


                return rfqsItems;
            }
        }
    }
}
