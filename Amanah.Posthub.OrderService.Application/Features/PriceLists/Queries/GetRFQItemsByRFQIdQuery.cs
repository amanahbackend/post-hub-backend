﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{

    public class GetRFQItemsByRFQIdQuery : IRequest<List<RFQItemsByRFQ>>
    {
        public int RFQId { set; get; }
    }
    public class GetRFQItemsByRFQIdQueryHandler : IRequestHandler<GetRFQItemsByRFQIdQuery, List<RFQItemsByRFQ>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetRFQItemsByRFQIdQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<List<RFQItemsByRFQ>> Handle(GetRFQItemsByRFQIdQuery query, CancellationToken cancellationToken)
        {          

            var rfqItemsList = await _context
                 .RFQItems
                 .Include(r=> r.RFQ) 
                 .ThenInclude(r=> r.BusinessCustomer)               
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.RFQId == query.RFQId).IgnoreQueryFilters()
                 .ProjectTo<RFQItemsByRFQ>(_mapper.ConfigurationProvider)
                 .ToListAsync();
          //  var mappedRfqItemsList = _mapper.Map<List<RFQItemsByRFQ>>(rfqItemsList);

            return rfqItemsList;
        }
    }
}
