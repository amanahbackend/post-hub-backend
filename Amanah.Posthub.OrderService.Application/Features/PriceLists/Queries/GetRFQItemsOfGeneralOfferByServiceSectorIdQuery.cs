﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetRFQItemsOfGeneralOfferByServiceSectorIdQuery : IRequest<List<RFQItemsDTO>>
    {
        public int ServiceSectorId { get; set; }

        public class GetRFQDetailsQueryHandler : IRequestHandler<GetRFQItemsOfGeneralOfferByServiceSectorIdQuery, List<RFQItemsDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetRFQDetailsQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<List<RFQItemsDTO>> Handle(GetRFQItemsOfGeneralOfferByServiceSectorIdQuery query, CancellationToken cancellationToken)
            {

                var rfqsItems = await _context
                     .RFQItems
                     .AsQueryable()
                     .AsNoTracking()
                     .IgnoreQueryFilters()
                     .Where(x => x.RFQ.ServiceSectorId == query.ServiceSectorId && x.RFQ.IsGeneralOffer.Value==true)
                     .ProjectTo<RFQItemsDTO>(_mapper.ConfigurationProvider)
                     .ToListAsync();


                return rfqsItems;
            }
        }
    }
}
