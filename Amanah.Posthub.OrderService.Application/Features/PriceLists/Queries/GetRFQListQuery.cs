﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class GetRFQListQuery : IRequest<List<RFQDTOList>>
    {

        public class GetRFQListQueryHandler : IRequestHandler<GetRFQListQuery, List<RFQDTOList>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetRFQListQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<List<RFQDTOList>> Handle(GetRFQListQuery query, CancellationToken cancellationToken)
            {
                var rfqList = await _context
                     .RFQ
                     .AsQueryable()
                     .AsNoTracking()
                 // .Where(x => x.Id == query.Id)
                     .IgnoreQueryFilters()
                     .ProjectTo<RFQDTOList>(_mapper.ConfigurationProvider)
                     .ToListAsync();

                return rfqList;
            }
        }
    }
}
