﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class InternationalRFQDetailsQuery : IRequest<IneternationalRFQDetailsResponseDTO>
    {
        public int Id { set; get; }
    }
    public class GetInternationalRFQDetailsQueryHandler : IRequestHandler<InternationalRFQDetailsQuery, IneternationalRFQDetailsResponseDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetInternationalRFQDetailsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<IneternationalRFQDetailsResponseDTO> Handle(InternationalRFQDetailsQuery query, CancellationToken cancellationToken)
        {
            IneternationalRFQDetailsResponseDTO ineternationalRFQDetailsResponseDTO = new IneternationalRFQDetailsResponseDTO();
            ineternationalRFQDetailsResponseDTO.TotalBudget = 0;
            var rfqsList = await _context
                  .RFQ.Include(c => c.BusinessCustomer).Include(c => c.RFQItemPrices).ThenInclude(c => c.Country)
                  .AsQueryable()
                  .AsNoTracking()
                  .IgnoreQueryFilters()
                  .Where(x => x.Id == query.Id).ToListAsync();
            ineternationalRFQDetailsResponseDTO.CustomerName = rfqsList.FirstOrDefault().BusinessCustomer.BusinessName;
            var mappedPQObj = _mapper.Map<List<InternationalRFQDetails>>(rfqsList.SelectMany(x => x.RFQItemPrices));
            foreach (var item in mappedPQObj)
            {
                ineternationalRFQDetailsResponseDTO.TotalBudget += item.RoundTripShippingPrice;
            }
            ineternationalRFQDetailsResponseDTO.internationalRFQDetails = mappedPQObj;
            return ineternationalRFQDetailsResponseDTO;
        }
    }





}
