﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class PLDetailsQuery : IRequest<PLDetailsResponseDTO>
    {
        public int Id { set; get; }
    }
    public class GetPLDetailsQueryHandler : IRequestHandler<PLDetailsQuery, PLDetailsResponseDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetPLDetailsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PLDetailsResponseDTO> Handle(PLDetailsQuery query, CancellationToken cancellationToken)
        {

            var rfqsList = await _context
                 .PriceLists.Include(c => c.PriceListItems)
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.Id == query.Id)
                 //.ProjectTo<PLDetailsResponseDTO>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync();
            var mappedObj = _mapper.Map<PLDetailsResponseDTO>(rfqsList);
            mappedObj.PlItems = _mapper.Map<List<CreatePLItemsInputDTO>>(rfqsList.PriceListItems);

            return mappedObj;
        }
    }





}
