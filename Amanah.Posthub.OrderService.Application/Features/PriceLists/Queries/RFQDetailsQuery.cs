﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.PriceLists.Commands.DTO;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.PriceLists.Queries
{
    public class RFQDetailsQuery : IRequest<RFQDetailsResponseDTO>
    {
        public int Id { set; get; }
    }
    public class GetRFQDetailsQueryHandler : IRequestHandler<RFQDetailsQuery, RFQDetailsResponseDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetRFQDetailsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<RFQDetailsResponseDTO> Handle(RFQDetailsQuery query, CancellationToken cancellationToken)
        {

            var rfqsList = await _context
                 .RFQ.Include(c => c.RFQItemPrices.Where(c=> !c.IsDeleted)).ThenInclude(c => c.Currency).Include(c => c.BusinessCustomer)
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.Id == query.Id)
                 //.ProjectTo<RFQDetailsResponseDTO>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync();
            var mappedObj = _mapper.Map<RFQDetailsResponseDTO>(rfqsList);
            mappedObj.PqItems = _mapper.Map<CreatePQItemsInputDTO>(rfqsList.RFQItemPrices.First());
            if (mappedObj.PqItems.HasAmount != null)
                mappedObj.PqItems.Total = mappedObj.PqItems.HasAmount.Value ? (decimal)mappedObj.PqItems.Amount * mappedObj.PqItems.ItemPrice : mappedObj.PqItems.ItemPrice;
            mappedObj.RFQItems = _mapper.Map<List<CreateRFQItemsInputDTO>>(rfqsList.RFQItemPrices);

            return mappedObj;
        }
    }





}
