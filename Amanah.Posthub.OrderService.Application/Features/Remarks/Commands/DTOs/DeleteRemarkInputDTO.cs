﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Remarks.Commands.DTOs
{
    public  class DeleteRemarkInputDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
