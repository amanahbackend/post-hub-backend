﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Remarks.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Remarks.Commands
{
  
    public class DeleteRemarkHandler : IRequestHandler<DeleteRemarkInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Remark> _remarkRepository;


        public DeleteRemarkHandler(
             ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Remark> remarkRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _remarkRepository = remarkRepository;
        }
        public async Task<int> Handle(DeleteRemarkInputDTO command, CancellationToken cancellationToken)
        {
            Remark remark = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                if (command.Id > 0)
                {

                    remark = _context.Remarks.Where(c => c.Id == command.Id).FirstOrDefault();
                    if (remark != null)
                    {
                        remark.IsDeleted = true;
                        _remarkRepository.Update(remark);
                    }
                }
                await _unityOfWork.SaveChangesAsync();

            });
            if (result)
            {
                return remark.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
