﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Remarks.Commands.DTOs;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Remarks.Commands
{

    public class UpdateRemarkHandler : IRequestHandler<UpdateRemarkInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Remark> _remarkRepository;
        private readonly ILocalizer _localizer;

        public UpdateRemarkHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Remark> remarkRepository,
            ILocalizer localizer

            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _remarkRepository = remarkRepository;
            _localizer = localizer;

        }
        public async Task<int> Handle(UpdateRemarkInputDTO command, CancellationToken cancellationToken)
        {
            Remark remark = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {

                if (command != null)
                {
                    var existedRemark = _context.Remarks.Where(c => !c.IsDeleted && c.Id != command.Id && (c.Name_en == command.Name_en || c.Name_ar == command.Name_ar)).FirstOrDefault();

                    remark = _mapper.Map<Remark>(command);
                    if (existedRemark == null)
                    {
                        _remarkRepository.Update(remark);
                        await _unityOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        throw new DomainException(_localizer[Keys.Messages.RemarkExist]);

                    }

                }
            });
            if (result)
            {
                return remark.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
