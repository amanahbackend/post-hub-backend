﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using System.Collections.Generic;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Extensions;

namespace Amanah.Posthub.OrderService.Application.Features.Remarks.Queries
{
   
    public class GetActiveRemarkListQuery : IRequest<IEnumerable<LookupResult<int>>>
    {
        public class GetActiveRemarkListQueryHandler : IRequestHandler<GetActiveRemarkListQuery, IEnumerable<LookupResult<int>>>
        {
            private readonly ApplicationDbContext _context;
            public GetActiveRemarkListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<LookupResult<int>>> Handle(GetActiveRemarkListQuery query, CancellationToken cancellationToken)
            {
                var remarks = _context.Remarks.Where(c => ! c.IsDeleted  && c.IsActive == true).
                    Select(c => new LookupResult<int>
                    {
                        Id = c.Id,
                        Name = System.Threading.Thread.CurrentThread.CurrentCulture.IsArabic()? c.Name_ar:c.Name_en,

                    }).ToList();
                if (remarks == null) return null;
                return remarks;
            }
        }
    }
}
