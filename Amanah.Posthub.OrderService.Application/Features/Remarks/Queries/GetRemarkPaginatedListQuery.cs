﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Remarks.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Remarks.Queries
{
   
    public class GetRemarkPaginatedListQuery : PaginatedItemsViewModel, IRequest<PagedResult<RemarkResponseDTO>>
    {
        public class GetRemarkPaginatedListQueryHandler : IRequestHandler<GetRemarkPaginatedListQuery, PagedResult<RemarkResponseDTO>>
        {
            private readonly ApplicationDbContext _context;
            public GetRemarkPaginatedListQueryHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<PagedResult<RemarkResponseDTO>> Handle(GetRemarkPaginatedListQuery query, CancellationToken cancellationToken)
            {
                var remarks = await _context.Remarks
                    .IgnoreQueryFilters()
                    .Where(c => !c.IsDeleted  )
                    .Select(c => new RemarkResponseDTO
                    {
                        Id = c.Id,
                        Name_ar =  c.Name_ar ,
                        IsActive=c.IsActive,
                        Name_en=c.Name_en

                    }).OrderBy(r => r.Id).ToPagedResultAsync(query); 
                if (remarks == null) return null;
                return remarks;
            }
        }
    }
}
