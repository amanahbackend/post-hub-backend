﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{

    public class BusinessCustomerAndItsBranchesForExportQuery : IRequest<List<BusinessCustomerAndBranchesDTO>>
    {
        public int? BusinessCustomerId { set; get; }
        public DateTime? fromDate { set; get; }
        public DateTime? toDate { set; get; }
        //   public int? Month { set; get; }


    }
    public class BusinessCustomerAndItsBranchesForExportQueryHandler : IRequestHandler<BusinessCustomerAndItsBranchesForExportQuery, List<BusinessCustomerAndBranchesDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public BusinessCustomerAndItsBranchesForExportQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<List<BusinessCustomerAndBranchesDTO>> Handle(BusinessCustomerAndItsBranchesForExportQuery query, CancellationToken cancellationToken)
        {


            var mailItemList = await _context
              .MailItems

              .AsQueryable()
              .AsNoTracking()
              .IgnoreQueryFilters()
              .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId &&
              x.DeliveredDate.Value.Date >= query.fromDate.Value.Date
              && x.DeliveredDate.Value.Date <= query.toDate.Value.Date
              && !x.IsDeleted
              )
             .Select(c => new BusinessCustomerAndBranchesDTO
             {
                 Id = c.Id,
                 ItemsNo = c.Order.MailItems.Where(x => x.Status.Id == 13 && !x.IsDeleted)
                 .GroupBy(c => c.DeliveredDate.Value.Day).Count(),
                 DeliveryDate = c.DeliveredDate,
                 BusinessBranchName = c.Order.BusinessOrder.BusinessCustomer.HQBranch.Name
             }).ToListAsync();

            return mailItemList;



        }
    }
}
