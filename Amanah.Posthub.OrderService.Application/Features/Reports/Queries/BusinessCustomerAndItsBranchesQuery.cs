﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{

    public class BusinessCustomerAndItsBranchesQuery : PaginatedItemsViewModel, IRequest<PagedResult<BusinessCustomerAndBranchesDTO>>
    {
        public int? BusinessCustomerId { set; get; }
        //  public DateTime? fromDate { set; get; }
        // public DateTime? toDate { set; get; }
        public int? Month { set; get; }
        public int? Year { set; get; }


    }
    public class BusinessCustomerAndItsBranchesQueryHandler : IRequestHandler<BusinessCustomerAndItsBranchesQuery, PagedResult<BusinessCustomerAndBranchesDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public BusinessCustomerAndItsBranchesQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<BusinessCustomerAndBranchesDTO>> Handle(BusinessCustomerAndItsBranchesQuery query, CancellationToken cancellationToken)
        {

            //var mailItemList = await _context
            //     .Orders  
            //     .AsQueryable()
            //     .AsNoTracking()
            //     .Where(x => x.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId &&
            //     x.CreationTime.Date >= query.fromDate.Value.Date
            //     && x.CreationTime.Date <= query.toDate.Value.Date&& !x.IsDeleted 
            //     )
            //     .ProjectTo<BusinessCustomerAndBranchesDTO>(_mapper.ConfigurationProvider)
            //    .ToPagedResultAsync(query);

            //return mailItemList;

            var mailItemList = await _context
              .MailItems

              .AsQueryable()
              .AsNoTracking()
              .IgnoreQueryFilters()
              .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId &&
              x.DeliveredDate.Value.Date.Month == query.Month
              && x.DeliveredDate.Value.Date.Year == query.Year
              && !x.IsDeleted
              )
             .Select(c => new BusinessCustomerAndBranchesDTO
             {
                 Id = c.Id,
                 ItemsNo = c.Order.MailItems.Where(x => x.Status.Id == 13 && !x.IsDeleted)
                 .GroupBy(c => c.DeliveredDate.Value.Day).Count(),
                 DeliveryDate = c.DeliveredDate,
                 BusinessBranchName = c.Order.BusinessOrder.BusinessCustomer.HQBranch.Name
             })
             .ToPagedResultAsync(query);


            //var mailItemList = await (from q in _context.MailItems.AsNoTracking().Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId
            //&& x.DeliveredDate.Value.Month == query.Month
            //&& !x.IsDeleted)
            //           let item = (from a in _context.MailItems.AsNoTracking() select a)
            //                        .Where(x => x.Status.Id == 13 && !x.IsDeleted)
            //                       .GroupBy(x => x.DeliveredDate.Value.Date)
            //                       .Select(g => new { DeliveredDate = (g.Key).Date, ItemsNo = g.Count() }).FirstOrDefault()
            //           select new BusinessCustomerAndBranchesDTO
            //           {
            //                DeliveryDate= item.DeliveredDate,
            //                ItemsNo=item.ItemsNo,
            //                BusinessBranchName = q.Order.BusinessOrder.BusinessCustomer.HQBranch.Name
            //           }).ToPagedResultAsync(query);



            return mailItemList;



        }
    }
}
