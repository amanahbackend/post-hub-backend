﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{


    public class BusinessCustomerBranchesWithTotalItemsQuery : IRequest<PaginatedBusinessCustomerItemsPerBranchesReport>
    {
        public int? BusinessCustomerId { set; get; }
        public DateTime? fromDate { set; get; }
        public DateTime? toDate { set; get; }
        public List<int> BrancheIds { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
    public class BusinessCustomerBranchesWithTotalItemsQueryHandler
       //: IRequestHandler<BusinessCustomerBranchesWithTotalItemsQuery, PagedResult<BusinessCustomerBranchesReportRows>>
       : IRequestHandler<BusinessCustomerBranchesWithTotalItemsQuery, PaginatedBusinessCustomerItemsPerBranchesReport>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public BusinessCustomerBranchesWithTotalItemsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PaginatedBusinessCustomerItemsPerBranchesReport> Handle(BusinessCustomerBranchesWithTotalItemsQuery query, CancellationToken cancellationToken)
        {
            var PagedResult = new PaginatedBusinessCustomerItemsPerBranchesReport();
            var listResult = new List<BusinessCustomerBranchesReportRows>();
            //Must Have Value 
            var listOfWantedBranchees = _context.BusinessCustomerBranchs.AsNoTracking().IgnoreQueryFilters()
                .Where(c => c.BusinessCustomerId == query.BusinessCustomerId && query.BrancheIds.Contains(c.Id))

                .ToList();

            var dataSource = _context.MailItems
               .Include(x => x.Order)
               .ThenInclude(x => x.BusinessOrder)
               .ThenInclude(x => x.BusinessCustomer)
               .ThenInclude(x => x.BusinessCustomerBranches).AsQueryable()
              .AsNoTracking().IgnoreQueryFilters()
              .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId &&
              ((x.DeliveredDate.Value.Date >= query.fromDate.Value.Date
              && x.DeliveredDate.Value.Date <= query.toDate.Value.Date) || (x.ReturnedDate.Value.Date >= query.fromDate.Value.Date
              && x.ReturnedDate.Value.Date <= query.toDate.Value.Date))
              && !x.IsDeleted
              );

            var dataSourcelist = dataSource.ToList();

            var data = dataSource
                  .Where(x => x.StatusId == (int)MailItemStatuses.Returned || x.StatusId == (int)MailItemStatuses.Delivered)
                  .Select(x => new
                  {
                      //branches = x.Order.BusinessOrder.BusinessCustomer.BusinessCustomerBranches.Select(b => b.Id).ToList(),
                      brancheId = x.Order.BusinessOrder.BusinessCustomerBranchId,
                      date = (x.StatusId == (int)MailItemStatuses.Delivered ? x.DeliveredDate : x.ReturnedDate) == null ? new DateTime().Date : (x.StatusId == (int)MailItemStatuses.Delivered ? x.DeliveredDate : x.ReturnedDate).Value.Date,
                  }).ToList();

            try
            {
                var listData = data.GroupBy(x => new { x.date.Date, x.brancheId }).ToList();
                foreach (var datedate in listData)
                {
                    BusinessCustomerBranchesReportRows row = new BusinessCustomerBranchesReportRows()
                    {
                        columns = new List<ReportResultCell>()
                    };
                    var column = new ReportResultCell();
                    row.Date = datedate.Key.Date;
                    foreach (var branch in listOfWantedBranchees)
                    {
                        //column.Brabch = branch.Name;
                        //column.count = datedate.Count(x => x.branches.Contains(branch.Id));
                        if (branch.Id > 0 && branch.Name != null)
                        {
                            row.columns.Add(new ReportResultCell()
                            {
                                BranchName = branch.Name,
                                BranchId = branch.Id,
                                // count = datedate.Count(x => x.branches.Contains(branch.Id))
                                count = datedate.Count(x => x.brancheId == branch.Id)
                            });
                        }
                    }
                    listResult.Add(row);
                    var skip = (query.PageNumber - 1) * query.PageSize;
                    var totalCount = listResult.Count();
                    var pageItems = listResult.Skip(skip.Value)
                        .Take(query.PageSize.Value)
                        .ToArray();
                    PagedResult.Count = totalCount;
                    PagedResult.Result = pageItems.ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return PagedResult;
        }
    }
}
