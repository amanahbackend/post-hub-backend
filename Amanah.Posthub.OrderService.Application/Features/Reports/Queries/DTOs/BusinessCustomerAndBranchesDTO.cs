﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs
{
    public class BusinessCustomerAndBranchesDTO
    {
        public int Id { get; set; }
        public string BusinessBranchName { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? ItemsNo { get; set; }
        // public int? TotalRowsNo { get; set; }
        public int? TotalItemsDeliveredForBranch { get; set; }
        public int? Day { get; set; }
    }
}
