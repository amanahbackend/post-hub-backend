﻿using Amanah.Posthub.Service.Domain.BusinessCustomers;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs
{
    public class PaginatedBusinessCustomerItemsPerBranchesReport
    {
        public List<BusinessCustomerBranchesReportRows> Result { get; set; }
        public int Count { get; set; }

    }
    public class BusinessCustomerBranchesReportRows
        {
            public List<ReportResultCell> columns { get; set; }
            public DateTime Date { get; set; }

        }
        public class ReportResultCell
        {
            public int count { get; set; }
            public string BranchName { get; set; }
            public int BranchId { get; set; }
        }
    }

