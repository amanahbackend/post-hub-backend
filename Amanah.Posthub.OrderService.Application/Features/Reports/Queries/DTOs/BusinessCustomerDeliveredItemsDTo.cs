﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs
{
    public class BusinessCustomerDeliveredItemsDTo
    {
        public int? DeliveredItemsNo { get; set; }
        public DateTime? DeliveryDate { get; set; }       
    }
}
