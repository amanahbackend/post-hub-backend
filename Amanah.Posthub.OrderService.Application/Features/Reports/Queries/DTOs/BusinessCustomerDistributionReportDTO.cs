﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs
{
    public class BusinessCustomerDistributionReportDTO
    {
        public int Id { get; set; }
        public string BusinessName { get; set; }
        public DateTime? DayOfMonth { get; set; }
        public int DeliveredDay { get; set; }
        public string BranchName { get; set; }
        public int? ItemsNo { get; set; }
        public decimal UnitPrice { get; set; }

    }
}
