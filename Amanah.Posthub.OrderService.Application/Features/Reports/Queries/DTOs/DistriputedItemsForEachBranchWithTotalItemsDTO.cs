﻿using Amanah.Posthub.Service.Domain.BusinessCustomers;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs
{    
    public class PaginatedDistriputedItemsForEachBranchWithTotalItemsReport
    {
        public List<ReportTable> Result { get; set; }
        public int Count { get; set; }
    }
    public class ReportTable
    {
        public string BranchName { get; set; }
        public List<DistriputedItemsForEachBranchRows> TableDataPerBranch { get; set; }
    }
    public class DistriputedItemsForEachBranchRows
    {           
            //public List<DistriputedItemsForEachBranchResultCell> columns { get; set; }
            public DateTime Date { get; set; }
            public int count { get; set; }
    }
        public class DistriputedItemsForEachBranchResultCell
    {
            public int count { get; set; }     
            public string branchName { get; set; }
        }
    }

