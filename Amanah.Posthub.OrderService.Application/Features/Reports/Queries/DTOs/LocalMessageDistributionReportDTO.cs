﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs
{
    public class LocalMessageDistributionReportDTO
    {
        public int Id { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public int? ItemsNo { get; set; }
        public int? ReturnItemsNo { get; set; }
        public int? DeliveredItemsNo { get; set; }
        public int? RemainingItemsNo { get; set; }
        public string WorkOrderCode { get; set; }
        public string DriverName { get; set; }
        //  public string Area { get; set; }
        public string FromSerial { get; set; }//رقم الكشف
        //public string WrokOrderCode { get; set; }// امر التوزيع
        public DateTime? PickupDate { get; set; }
        public int? CaseNo { get; set; }
    }
}
