﻿
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{


    public class DistriputedItemsForEachBranchWithTotalItemsQuery : IRequest<PaginatedDistriputedItemsForEachBranchWithTotalItemsReport>
    {
        public int? BusinessCustomerId { set; get; }
        public DateTime? fromDate { set; get; }
        public DateTime? toDate { set; get; }
        public List<int> BrancheIds { get; set; }
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
    }
    public class DistriputedItemsForEachBranchWithTotalItemsQueryHandler
       //: IRequestHandler<DistriputedItemsForEachBranchWithTotalItemsQuery.cs, PagedResult<DistriputedItemsForEachBranchRows>>
       : IRequestHandler<DistriputedItemsForEachBranchWithTotalItemsQuery, PaginatedDistriputedItemsForEachBranchWithTotalItemsReport>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public DistriputedItemsForEachBranchWithTotalItemsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PaginatedDistriputedItemsForEachBranchWithTotalItemsReport> Handle(DistriputedItemsForEachBranchWithTotalItemsQuery query, CancellationToken cancellationToken)
        {
            var PagedResult = new PaginatedDistriputedItemsForEachBranchWithTotalItemsReport();
            var listResult = new List<DistriputedItemsForEachBranchRows>();
            var TablesList = new List<ReportTable>();
            //Must Have Value 
            var listOfWantedBranchees = _context.BusinessCustomerBranchs.AsNoTracking().IgnoreQueryFilters()
                .Where(c => c.BusinessCustomerId == query.BusinessCustomerId && query.BrancheIds.Contains(c.Id))

                .ToList();

            var dataSource = _context.MailItems
               .Include(x => x.Order)
               .ThenInclude(x => x.BusinessOrder)
               .ThenInclude(x => x.BusinessCustomer)
               .ThenInclude(x => x.BusinessCustomerBranches).AsQueryable()
              .AsNoTracking().IgnoreQueryFilters()
              .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId &&
              ((x.DeliveredDate.Value.Date >= query.fromDate.Value.Date
              && x.DeliveredDate.Value.Date <= query.toDate.Value.Date) || (x.ReturnedDate.Value.Date >= query.fromDate.Value.Date
              && x.ReturnedDate.Value.Date <= query.toDate.Value.Date))
              && !x.IsDeleted
              );

            var dataSourcelist = dataSource.ToList();

            var data = dataSource
                  .Where(x => x.StatusId == (int)MailItemStatuses.Returned || x.StatusId == (int)MailItemStatuses.Delivered)
                  .Select(x => new
                  {
                      //branches = x.Order.BusinessOrder.BusinessCustomer.BusinessCustomerBranches.Select(b => b.Id).ToList(),
                      brancheId= x.Order.BusinessOrder.BusinessCustomerBranchId,
                      date = (x.StatusId == (int)MailItemStatuses.Delivered ? x.DeliveredDate : x.ReturnedDate) == null ? new DateTime().Date : (x.StatusId == (int)MailItemStatuses.Delivered ? x.DeliveredDate : x.ReturnedDate).Value.Date,
                  }).ToList();

            //try
            //{
            var listData = data.GroupBy(x => new { x.date.Date,x.brancheId }).ToList();
            foreach (var branchItem in listOfWantedBranchees)
            {
                ReportTable table = new ReportTable()
                {
                    TableDataPerBranch = new List<DistriputedItemsForEachBranchRows>()
                };
                table.BranchName = branchItem.Name;
                foreach (var datedate in listData)
                {
                    DistriputedItemsForEachBranchRows row = new DistriputedItemsForEachBranchRows()
                    {
                        //columns = new List<DistriputedItemsForEachBranchResultCell>()
                    };
                    var column = new DistriputedItemsForEachBranchResultCell();
                    row.Date = datedate.Key.Date;
                    //row.count = datedate.Count(x => x.branches.Contains(branchItem.Id));
                    row.count = datedate.Count(x => x.brancheId== branchItem.Id);
                 
                    table.TableDataPerBranch.Add(row);
                }
                TablesList.Add(table);
                //var skip = (query.PageNumber - 1) * query.PageSize;
                var totalCount = TablesList.Count();
                var pageItems = TablesList//.Skip(skip.Value)
                                          //    .Take(query.PageSize.Value)
                    .ToArray();
                PagedResult.Count = totalCount;
                PagedResult.Result = TablesList;
            }

            //}
            //catch (Exception ex)
            //{

            //}
            return PagedResult;
        }
    }
}
