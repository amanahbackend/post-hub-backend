﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Shared;
using AutoMapper;
using MediatR;
//using System.Data.SqlClient;
using Microsoft.Data.SqlClient;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries 
{
   public class GetBusinessCustomerDistributionReportQuery : PaginatedItemsViewModel, IRequest<IEnumerable<BusinessCustomerDistributionReportDTO>>
    {
        public int? BusinessCustomerId { set; get; }
        //  public DateTime? fromDate { set; get; }
        // public DateTime? toDate { set; get; }
        public int? Month { set; get; }
        public int? Year { set; get; }
        public int? PageNumber { set; get; }
        public int? PageSize { set; get; }
    }
    public class GetBusinessCustomerDistributionReportQueryHandler : IRequestHandler<GetBusinessCustomerDistributionReportQuery, IEnumerable<BusinessCustomerDistributionReportDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IExecuteStoredProc _executeStoredProc;
        private readonly IMapper _mapper;

        public GetBusinessCustomerDistributionReportQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IExecuteStoredProc executeStoredProc
            )
        {
            _context = context;
            _mapper = mapper;
            _executeStoredProc = executeStoredProc;
             
        }
        public async Task<IEnumerable<BusinessCustomerDistributionReportDTO>> Handle(GetBusinessCustomerDistributionReportQuery query, CancellationToken cancellationToken)
        {

            //var bcustomerlist =  _context.BusinessCustomerBranchs
            //        .Where(x => x.BusinessCustomerId == query.BusinessCustomerId)
            //        .AsNoTracking()
            //        .Select(c=>c.Id)
            //        .ToList();
            //// return bcustomerlist;

            //var mailItemList = _context
            //  .MailItems

            //  .AsQueryable()
            //  .AsNoTracking()
            //  .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId &&
            //  x.DeliveredDate.Value.Date.Month == query.Month
            //  && x.DeliveredDate.Value.Date.Year == query.Year
            //  && !x.IsDeleted
            //  ).GroupBy(c=>c.DeliveredDate.Value.Day)
            // .Select(c =>
            // new BusinessCustomerDistributionReportDTO
            // {
            //    // Id = c.Id,
            //     //ItemsNo = c.Order.MailItems.Where(x => x.Status.Id == 13 && !x.IsDeleted)
            //     // .GroupBy(c =>new { c.DeliveredDate.Value.Day , c.Order.BusinessOrder.BusinessCustomer.HQBranch.Name }).Count(),
            //     //DeliveryDate = c.DeliveredDate,
            //     //BranchName = c.Order.BusinessOrder.BusinessCustomer.HQBranch.Name
            //     ItemsNo=c.Count()
            // }
            // )
            // .Skip(query.PageNumber * query.PageSize).Take(query.PageSize).ToList();
            //// .ToPagedResultAsync(query);
            //// var newMailItemList = mailItemList.DistinctBy(i => i.DeliveryDate.Value.Day).ToList();

            var parameters = new List<SqlParameter> {
            new SqlParameter("@Month", SqlDbType.Int) { Value =  query.Month ?? (object) DBNull.Value },
            new SqlParameter("@Year", SqlDbType.Int) { Value = query.Year ?? (object) DBNull.Value },
            new SqlParameter("@BusinessCustomerId", SqlDbType.Int) { Value = query.BusinessCustomerId ?? (object) DBNull.Value },
            new SqlParameter("@PageNumber",SqlDbType.Int) { Value =  query.PageNumber ?? (object) DBNull.Value },
            new SqlParameter("@PageSize", SqlDbType.Int)  { Value = query.PageSize ?? (object) DBNull.Value } };

            var mailItemList = _executeStoredProc.ExecuteStoredByName<BusinessCustomerDistributionReportDTO>("sp_GetBusinessCustomerDistribution", parameters.ToArray()).ToList();


            return mailItemList;

        }

    } 
}
