﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{

    public class GetAllLocalMessageDistributionReportForExcelQuery : IRequest<List<LocalMessageDistributionReportDTO>>
    {
        public int BusinessCustomerId { set; get; }
        public DateTime? FromDate { set; get; }
        public DateTime? ToDate { set; get; }
    }
    public class GetAllLocalMessageDistributionReportForExcelQueryHandler : IRequestHandler<GetAllLocalMessageDistributionReportForExcelQuery, List<LocalMessageDistributionReportDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllLocalMessageDistributionReportForExcelQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<List<LocalMessageDistributionReportDTO>> Handle(GetAllLocalMessageDistributionReportForExcelQuery query, CancellationToken cancellationToken)
        {
            try
            {
                var mailItemList = _context
                 .MailItems
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId
                 && (query.FromDate.HasValue && x.DeliveredDate.Value.Date >= query.FromDate.Value.Date)
                 && (query.ToDate.HasValue && x.DeliveredDate.Value.Date <= query.ToDate.Value.Date)
                 )
                 //.GroupBy(c => c.FromSerial)
                 .ProjectTo<LocalMessageDistributionReportDTO>(_mapper.ConfigurationProvider)
                 .ToList();

                return mailItemList;
            }
            catch (Exception ex)
            {
                return new List<LocalMessageDistributionReportDTO>();
                throw new Exception(ex.Message);
            }


        }
    }
}
