﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{
    public class GetBusinessCustomerDeliveredItemsPagedReportToEachBranch : PaginatedItemsViewModel, IRequest<PagedResult<BusinessCustomerDeliveredItemsDTo>>
    {
        public int? BusinessCustomerId { set; get; }
        public List<int> BranchId { set; get; }
        public DateTime? FromDate { set; get; }
        public DateTime? ToDate { set; get; }
    }
    public class GetBusinessCustomerDeliveredItemsPagedReportToEachBranchHandler : IRequestHandler<GetBusinessCustomerDeliveredItemsPagedReportToEachBranch, PagedResult<BusinessCustomerDeliveredItemsDTo>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetBusinessCustomerDeliveredItemsPagedReportToEachBranchHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<BusinessCustomerDeliveredItemsDTo>> Handle(GetBusinessCustomerDeliveredItemsPagedReportToEachBranch query, CancellationToken cancellationToken)
        {
            var mailItemList = await _context
                      .MailItems
                      .Include(x => x.Order)
                      .ThenInclude(x => x.BusinessOrder)
                       .AsQueryable()
                       .AsNoTracking()
                       .IgnoreQueryFilters()
                        .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId
                        && (query.BranchId.Contains((int)x.Order.BusinessOrder.BusinessCustomerBranchId))
                  && (query.FromDate.HasValue && x.DeliveredDate.Value.Date >= query.FromDate.Value.Date)
                  && (query.ToDate.HasValue && x.DeliveredDate.Value.Date <= query.ToDate.Value.Date) && !x.IsDeleted
                  ).GroupBy(c => c.DeliveredDate.Value.Date)
                  .Select(c => new BusinessCustomerDeliveredItemsDTo
                  {
                      DeliveredItemsNo = c.Count()
                  ,
                      DeliveryDate = c.Key
                  })           
                  .ToPagedResultAsync(query);

            return mailItemList;
        }
    }
}
