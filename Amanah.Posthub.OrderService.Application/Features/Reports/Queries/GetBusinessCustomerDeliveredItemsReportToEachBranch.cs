﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{
    public class GetBusinessCustomerDeliveredItemsReportToEachBranch : PaginatedItemsViewModel, IRequest<IEnumerable<BusinessCustomerDeliveredItemsDTo>>
    
    {
        public int? BusinessCustomerId { set; get; }
        public List<int> BranchId { set; get; }
        public DateTime? FromDate { set; get; }
        public DateTime? ToDate { set; get; }

        public class GetBusinessCustomerDeliveredItemsReportToEachBranchHandler : IRequestHandler<GetBusinessCustomerDeliveredItemsReportToEachBranch, IEnumerable<BusinessCustomerDeliveredItemsDTo>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetBusinessCustomerDeliveredItemsReportToEachBranchHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<IEnumerable<BusinessCustomerDeliveredItemsDTo>> Handle(GetBusinessCustomerDeliveredItemsReportToEachBranch query, CancellationToken cancellationToken)
            {
                var mailItemList = await _context
                      .MailItems
                      .Include(x => x.Order)
                      .ThenInclude(x => x.BusinessOrder)
                       .AsQueryable()
                       .AsNoTracking()
                       .IgnoreQueryFilters()
                        .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId
                        && (query.BranchId.Contains((int)x.Order.BusinessOrder.BusinessCustomerBranchId))
                 && (query.FromDate.HasValue && x.DeliveredDate.Value.Date >= query.FromDate.Value.Date)
                  && (query.ToDate.HasValue && x.DeliveredDate.Value.Date <= query.ToDate.Value.Date) && !x.IsDeleted
                   ).GroupBy(c => c.DeliveredDate.Value.Date) 
                  .Select(c => new BusinessCustomerDeliveredItemsDTo
                  {
                      DeliveredItemsNo = c.Count()
                  ,
                      DeliveryDate = c.Key
                  })
                 .ToListAsync();

                return mailItemList;
            }
        }

    }
}
