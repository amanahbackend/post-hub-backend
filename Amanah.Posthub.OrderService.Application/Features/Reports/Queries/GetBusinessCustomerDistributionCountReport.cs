﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Shared;
using AutoMapper;
using MediatR;
//using System.Data.SqlClient;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{


    public class GetBusinessCustomerDistributionCountReport : IRequest<object>
    {
        public int? BusinessCustomerId { set; get; }
        public DateTime? FromDate { set; get; }
        public DateTime? ToDate { set; get; }
        /// public int? Month { set; get; }
        // public int? Year { set; get; }
        //   public int? PageNumber { set; get; }
        //  public int? PageSize { set; get; }



    }
    public class GetBusinessCustomerDistributionCountReportHandler : IRequestHandler<GetBusinessCustomerDistributionCountReport, object>
    {
        private readonly ApplicationDbContext _context;
        private readonly IExecuteStoredProc _executeStoredProc;
        private readonly IMapper _mapper;

        public GetBusinessCustomerDistributionCountReportHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IExecuteStoredProc executeStoredProc
            )
        {
            _context = context;
            _mapper = mapper;
            _executeStoredProc = executeStoredProc;

        }
        public async Task<object> Handle(GetBusinessCustomerDistributionCountReport query, CancellationToken cancellationToken)
        {



            var parameters = new List<SqlParameter> {
            new SqlParameter("@FromDate", SqlDbType.Date) { Value =  query.FromDate ?? (object) DBNull.Value },
            new SqlParameter("@ToDate", SqlDbType.Date) { Value = query.ToDate ?? (object) DBNull.Value },
            new SqlParameter("@BusinessCustomerId", SqlDbType.Int) { Value = query.BusinessCustomerId ?? (object) DBNull.Value }
           // new SqlParameter("@PageNumber",SqlDbType.Int) { Value =  query.PageNumber ?? (object) DBNull.Value },
           // new SqlParameter("@PageSize", SqlDbType.Int)  { Value = query.PageSize ?? (object) DBNull.Value }
           };

            var mailItemList = _executeStoredProc.ExecuteStoredByName<BusinessCustomerDistributionReportDTO>("sp_GetBusinessCustomerDistributionCount", parameters.ToArray()).ToList();


            return mailItemList;



        }

    }
}
