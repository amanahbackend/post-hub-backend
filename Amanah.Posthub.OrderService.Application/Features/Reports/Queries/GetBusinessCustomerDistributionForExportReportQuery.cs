﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using Amanah.Posthub.OrderService.Application.Shared;
using AutoMapper;
using MediatR;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{

    public class GetBusinessCustomerDistributionForExportReportQuery : IRequest<List<BusinessCustomerDistributionReportDTO>>
    {
        public int? BusinessCustomerId { set; get; }
        //  public DateTime? fromDate { set; get; }
        // public DateTime? toDate { set; get; }
        public int? Month { set; get; }
        public int? Year { set; get; }
        //  public int? PageNumber { set; get; }
        //  public int? PageSize { set; get; }



    }
    public class GetBusinessCustomerDistributionForExportReportQueryQueryHandler : IRequestHandler<GetBusinessCustomerDistributionForExportReportQuery, List<BusinessCustomerDistributionReportDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IExecuteStoredProc _executeStoredProc;
        private readonly IMapper _mapper;

        public GetBusinessCustomerDistributionForExportReportQueryQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IExecuteStoredProc executeStoredProc
            )
        {
            _context = context;
            _mapper = mapper;
            _executeStoredProc = executeStoredProc;

        }
        public async Task<List<BusinessCustomerDistributionReportDTO>> Handle(GetBusinessCustomerDistributionForExportReportQuery query, CancellationToken cancellationToken)
        {

            var parameters = new List<SqlParameter> {
            new SqlParameter("@Month", SqlDbType.Int) { Value =  query.Month ?? (object) DBNull.Value },
            new SqlParameter("@Year", SqlDbType.Int) { Value = query.Year ?? (object) DBNull.Value },
            new SqlParameter("@BusinessCustomerId", SqlDbType.Int) { Value = query.BusinessCustomerId ?? (object) DBNull.Value },
           // new SqlParameter("@PageNumber",SqlDbType.Int) { Value =  query.PageNumber ?? (object) DBNull.Value },
           // new SqlParameter("@PageSize", SqlDbType.Int)  { Value = query.PageSize ?? (object) DBNull.Value }
            
            };
            var mailItemList = _executeStoredProc.ExecuteStoredByName<BusinessCustomerDistributionReportDTO>("sp_GetBusinessCustomerDistributionForExport", parameters.ToArray()).ToList();
            return mailItemList;
        }

    }
}
