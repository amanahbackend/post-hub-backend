﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Reports.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Reports.Queries
{

    public class GetLocalMessageDistributionReportQuery : PaginatedItemsViewModel, IRequest<PagedResult<LocalMessageDistributionReportDTO>>
    {
        public int BusinessCustomerId { set; get; }
        public DateTime? FromDate { set; get; }
        public DateTime? ToDate { set; get; }
    }
    public class GetLocalMessageDistributionReportQueryHandler : IRequestHandler<GetLocalMessageDistributionReportQuery, PagedResult<LocalMessageDistributionReportDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetLocalMessageDistributionReportQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<LocalMessageDistributionReportDTO>> Handle(GetLocalMessageDistributionReportQuery query, CancellationToken cancellationToken)
        {
            var mailItemList = await _context
                 .MailItems
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.Order.BusinessOrder.BusinessCustomerId == query.BusinessCustomerId
                 && (query.FromDate.HasValue && x.DeliveredDate.Value.Date >= query.FromDate.Value.Date)
                 && (query.ToDate.HasValue && x.DeliveredDate.Value.Date <= query.ToDate.Value.Date)
                 )
                 .ProjectTo<LocalMessageDistributionReportDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(query);

            return mailItemList;
        }
    }
}
