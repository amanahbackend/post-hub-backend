﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Services.Commands.DTOs;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Commands
{

    public class CreateServiceHandler : IRequestHandler<CreateServiceInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly ILocalizer _localizer;
        private readonly IRepository<Amanah.Posthub.Service.Domain.Entities.Service> _serviceRepository;

        public CreateServiceHandler(ApplicationDbContext context, IMapper mapper, IUnitOfWork unityOfWork,
               IRepository<Amanah.Posthub.Service.Domain.Entities.Service> serviceRepository,
               ILocalizer localizer)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _serviceRepository = serviceRepository;
            _localizer = localizer;
        }

        public async Task<int> Handle(CreateServiceInputDTO command, CancellationToken cancellationToken)
        {
            // avoid duplicate
            var isServiceExist = _context.Services.IgnoreQueryFilters()
                    .Where(a => !a.IsDeleted && (a.Name_en == command.Name_en || a.Name_ar == command.Name_ar))
                    .OrderByDescending(a => a.Id).ToList();

            if (isServiceExist.Count > 0)
                throw new DomainException(_localizer[Keys.Messages.ServiceExist]);

            Amanah.Posthub.Service.Domain.Entities.Service service = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                if (command != null)
                {

                        service = _mapper.Map<Amanah.Posthub.Service.Domain.Entities.Service>(command);
                        _serviceRepository.Add(service);
                        await _unityOfWork.SaveChangesAsync();
                  
                }
            });

            if (result)
                return service.Id;
            else
                return 0;
        }
    }
}
