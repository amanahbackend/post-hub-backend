﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Commands.DTOs
{
    public class CreateServiceInputDTO : IRequest<int>
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public decimal Price { get; set; }
        public string PhotoUrl { get; set; }
        public IFormFile PhotoUrlFile { get; set; }
    }
}
