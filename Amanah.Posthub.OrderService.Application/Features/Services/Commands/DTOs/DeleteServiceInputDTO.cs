﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Commands.DTOs
{
    public class DeleteServiceInputDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
