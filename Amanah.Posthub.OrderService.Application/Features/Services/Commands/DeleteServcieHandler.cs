﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Services.Commands.DTOs;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Commands
{

    public class DeleteServcieHandler : IRequestHandler<DeleteServiceInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Amanah.Posthub.Service.Domain.Entities.Service> _serviceRepository;
        public DeleteServcieHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Amanah.Posthub.Service.Domain.Entities.Service> serviceRepository
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _serviceRepository = serviceRepository;
        }
        public async Task<int> Handle(DeleteServiceInputDTO command, CancellationToken cancellationToken)
        {
            Amanah.Posthub.Service.Domain.Entities.Service service = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                if (command.Id > 0)
                {

                    service = _context.Services.Where(c => c.Id == command.Id).FirstOrDefault();
                    if (service != null)
                    {
                        service.IsDeleted = true;
                        _serviceRepository.Update(service);

                    }
                }
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return service.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
