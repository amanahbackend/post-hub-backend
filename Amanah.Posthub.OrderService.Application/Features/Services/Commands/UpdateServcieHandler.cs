﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Services.Commands.DTOs;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Commands
{

    public class UpdateServcieHandler : IRequestHandler<UpdateServcieInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<Amanah.Posthub.Service.Domain.Entities.Service> _serviceRepository;
        private readonly ILocalizer _localizer;

        public UpdateServcieHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<Amanah.Posthub.Service.Domain.Entities.Service> serviceRepository,
             ILocalizer localizer
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _serviceRepository = serviceRepository;
            _localizer = localizer;
        }
        public async Task<int> Handle(UpdateServcieInputDTO command, CancellationToken cancellationToken)
        {
            Amanah.Posthub.Service.Domain.Entities.Service service = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                if (command != null)
                {
                    var serviceEF = _context.Services.IgnoreQueryFilters().Where(c => !c.IsDeleted && c.Id != command.Id
                                   && (c.Name_ar == command.Name_ar || c.Name_en == command.Name_en)).FirstOrDefault();
                    if (serviceEF == null)
                    {

                        service = _mapper.Map<Amanah.Posthub.Service.Domain.Entities.Service>(command);

                        _serviceRepository.Update(service);
                        await _unityOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        throw new DomainException(_localizer[Keys.Messages.ServiceExist]);

                    }
                }
            });
            if (result)
            {
                return service.Id;
            }
            else
            {
                return 0;
            }
        }
    }
}
