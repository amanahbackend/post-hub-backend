﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs
{
    public class ServiceResponseDTO
    {
        public int Id { get; set; }
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public decimal Price { get; set; }
        public string PhotoUrl { get; set; }
        public string Name { get; set; }
    }
}
