﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Qureies
{

    public class GetAllServicesForMobileQuery : IRequest<List<ServiceResponseForMobileDTO>>
    {
    }

    public class GetAllServicesForMobileQueryHandler : IRequestHandler<GetAllServicesForMobileQuery, List<ServiceResponseForMobileDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllServicesForMobileQueryHandler(ApplicationDbContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        public async Task<List<ServiceResponseForMobileDTO>> Handle(GetAllServicesForMobileQuery query, CancellationToken cancellationToken)
        {
            var services = await _context.Services
                .AsNoTracking()
                .IgnoreQueryFilters()
                .Where(c => !c.IsDeleted)
                .ProjectTo<ServiceResponseForMobileDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
            return services;
        }
    }
}
