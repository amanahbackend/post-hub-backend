﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Qureies
{
    public class GetAllServicesQuery : IRequest<List<ServiceResponseDTO>>
    {
    }

    public class GetAllServicesQueryHandler : IRequestHandler<GetAllServicesQuery, List<ServiceResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllServicesQueryHandler(ApplicationDbContext context, IMapper mapper)
        {
            this._context = context; 
            this._mapper = mapper;
        }

        public async Task<List<ServiceResponseDTO>> Handle(GetAllServicesQuery request, CancellationToken cancellationToken)
        {
            //var services = await _context.Services.Select(c => new ServiceResponseDTO
            //{
            //    Id = c.Id,
            //    Price = c.Price,
            //    Name_ar = c.Name_ar,
            //    Name_en = c.Name_en,
            //    PhotoUrl = $"UploadFile/ServiceFile/" + (string.IsNullOrEmpty(c.PhotoUrl) ? "avtar.jpeg" : c.PhotoUrl),
            //}).ToListAsync();
            //return services;

            return await _context.Services.IgnoreQueryFilters().Where(s => !s.IsDeleted).ProjectTo<ServiceResponseDTO>(_mapper.ConfigurationProvider).ToListAsync(); 
        }
    }
} 
