﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Services.Qureies.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.Services.Qureies
{
  
    public class GetServicesPaginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<ServiceResponseDTO>>
    {
    }


    public class GetDepartmentPagginatedListQueryHandler : IRequestHandler<GetServicesPaginatedQuery, PagedResult<ServiceResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetDepartmentPagginatedListQueryHandler(
            ApplicationDbContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<PagedResult<ServiceResponseDTO>> Handle(GetServicesPaginatedQuery query, CancellationToken cancellationToken)
        {
            var serviceList = new PagedResult<ServiceResponseDTO>();

            serviceList = await _context
                       .Services
                       .AsQueryable()
                       .AsNoTracking()
                       .IgnoreQueryFilters()
                       .Where(c => !c.IsDeleted)
                       .OrderBy(c => c.Id)
                       .Select(c => new ServiceResponseDTO
                       {
                           Id = c.Id,
                           Price=c.Price,
                           Name_ar = c.Name_ar,
                           Name_en = c.Name_en,
                           PhotoUrl = $"UploadFile/ServiceFile/" + (string.IsNullOrEmpty(c.PhotoUrl) ? "avtar.jpeg" : c.PhotoUrl),
                       })
                       .ToPagedResultAsync(query);

            if (serviceList == null)
            {
                return null;
            }
            return serviceList;
        }
    }
}
