﻿using Amanah.Posthub.OrderService.Application.Enums;
using AutoMapper;
using global::Amanah.Posthub.Context;
using global::Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using global::Amanah.Posthub.Service.Domain.Entities;
using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    public class AssignDriverToWorkOrderHandler : IRequestHandler<AssignWorkOrderToDriverInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        //private readonly IFirebasePushNotificationService _pushNotificationService;



        public AssignDriverToWorkOrderHandler(
                ApplicationDbContext context,
                IMapper mapper,
                IUnitOfWork unityOfWork,
                //IFirebasePushNotificationService pushNotificationService,
                IRepository<WorkOrder> workOrderRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            //_pushNotificationService = pushNotificationService;
            _workOrderRepository = workOrderRepository;
        }
        public async Task<int> Handle(AssignWorkOrderToDriverInputDTO command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                workOrder = await _workOrderRepository.GetByIdAsync(command.WorkOrderId);
                if (workOrder != null)
                {


                    workOrder.DriverId = command.DriverId;
                    //var workOrderStatus =_context.WorkOrderStatus.Where(x => x.Name == "Assigned").FirstOrDefault();
                    workOrder.WorkOrderStatusId = (int)WorkorderStatuses.Dispatched;
                    _workOrderRepository.Update(workOrder);
                    await _unityOfWork.SaveChangesAsync();
                }
                //var driverUserId=  await _context.Driver
                //    .Where(x => x.Id == command.DriverId)
                //    .Select(x => x.UserId)
                //    .FirstOrDefaultAsync();
                //    await _pushNotificationService.SendAsync(driverUserId, "Post-Hub", "You Have New WorkOrder.", new { NotificationType = NotificationTypeEnum.NewWorkOrder });

            });
            if (result)
            {
                if (workOrder != null)
                {
                    return workOrder.Id;

                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }
    }
}


