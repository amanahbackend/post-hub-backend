﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using AutoMapper;
using global::Amanah.Posthub.Context;
using global::Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using global::Amanah.Posthub.Service.Domain.Entities;
using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Authentication;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    public class AutoAssignDriverToWorkOrderHandler : IRequestHandler<AutoAssignWorkOrderToDriverInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly IFirebasePushNotificationService _pushNotificationService;
        private readonly IConfiguration _configuration;
        private ICurrentUser _currentUser;


        public AutoAssignDriverToWorkOrderHandler(
                ApplicationDbContext context,
                IMapper mapper,
                IUnitOfWork unityOfWork,
                IRepository<WorkOrder> workOrderRepository,
                IFirebasePushNotificationService pushNotificationService,
                IConfiguration configuration,
                ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _pushNotificationService = pushNotificationService;
            _configuration = configuration;
            _currentUser = currentUser;
        }
        public async Task<int> Handle(AutoAssignWorkOrderToDriverInputDTO command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                Random rnd = new Random();
                foreach (var defaultDriverId in command.DefaultDriverIds)
                {
                    WorkOrder workOrder = new WorkOrder();

                    var mailItems = await _context
                    .MailItems
                    .Include(m=> m.MailItemsStatusLog)
                    .Include(m=> m.Order)
                    .ThenInclude(m=> m.OrdersStatusLog)
                        .IgnoreQueryFilters()
                        .Where(a => a.DefaultAssignedDriverID == defaultDriverId
                            && !a.IsDeleted
                            && !a.Order.IsDeleted
                            && !a.Order.IsDraft
                            && a.Order.ServiceSectorId == (int)ServiceSectors.LocalPost
                            && (a.StatusId == (int)MailItemStatuses.New || a.StatusId == (int)MailItemStatuses.ReadyForDispatch))
                        .Include(a => a.Order)
                        .ToListAsync();
                    if (mailItems.Count == 0)
                    {
                        continue;
                    }
                    foreach (var item in mailItems)
                    {
                        item.StatusId = (int)MailItemStatuses.Dispatched;
                        item.Order.StatusId = (int)OrderStatuses.InProcess;
                        if (item.Order.OrdersStatusLog != null && item.Order.OrdersStatusLog.Count > 0)
                        {
                            item.Order.OrdersStatusLog.Add(
                                 new OrdersStatusLog()
                                 {
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                     IsDeleted = false,
                                     OrderStatusId = (int)OrderStatuses.InProcess
                                 });
                        }
                        else
                        {
                            item.Order.OrdersStatusLog = new List<OrdersStatusLog>() {
                                    new OrdersStatusLog()
                                    {
                                        CreatedBy =_currentUser.Id,
                                        CreationTime =DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                        OrderStatusId =  (int)OrderStatuses.InProcess
                                    }
                                };
                        }

                        if (item.MailItemsStatusLog != null && item.MailItemsStatusLog.Count > 0)
                        {
                            item.MailItemsStatusLog.Add(
                                 new MailItemsStatusLog()
                                 {
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                     IsDeleted = false,
                                     WorkOrderId = item.WorkorderId,
                                     MailItemStatusId = (int)MailItemStatuses.Dispatched
                                 });
                        }
                        else
                        {
                            item.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                                    new MailItemsStatusLog()
                                    {
                                        CreatedBy=_currentUser.Id,
                                        CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                         WorkOrderId=item.WorkorderId,
                                        MailItemStatusId = (int)MailItemStatuses.Dispatched
                                    }
                                };
                        }
                        _context.MailItems.Update(item);
                    }
                    workOrder.MailItems = mailItems;
                    workOrder.WorkOrderStatusId = (int)WorkOrderStatuses.Dispatched;
                    workOrder.DriverId = defaultDriverId;
                    workOrder.Priorty = WorkOrderPriorty.Normal;
                    workOrder.IssueAt = DateTime.Now;
                    workOrder.AssignedDate = DateTime.Now;
                    workOrder.ItemsNo = mailItems.Count;
                    workOrder.Code = "AUTO" + rnd.Next(10000001, 999999999).ToString();
                    _workOrderRepository.Add(workOrder);

                    var driverUserId = await _context.Driver
                        .IgnoreQueryFilters()
                        .Where(x => x.Id == defaultDriverId)
                        .Select(x => x.UserId)
                        .FirstOrDefaultAsync();
                    await _pushNotificationService.SendAsync(driverUserId, "Post-Hub", "You Have New WorkOrder #" + workOrder.Code, new { NotificationType = NotificationTypeEnum.NewWorkOrder });
                }
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return 200;
            }
            else
            {
                return 0;
            }

        }
    }
}


