﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Authentication;
using Microsoft.Extensions.Configuration;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    public class ChangeWorkOrderStatusCommand : IRequest<int>
    {
        public int Id { get; set; }
        public int NewStatusId { get; set; }

        public class ChangeWorkOrderStatusCommandHandler : IRequestHandler<ChangeWorkOrderStatusCommand, int>
        {

            private readonly ApplicationDbContext _context;
            private readonly IConfiguration _configuration;
            private ICurrentUser _currentUser;
            public ChangeWorkOrderStatusCommandHandler(
                 ApplicationDbContext context,
                 IConfiguration configuration,
                 ICurrentUser currentUser)
            {
                _context = context;
                _configuration = configuration;
                _currentUser = currentUser;
            }

            public async Task<int> Handle(ChangeWorkOrderStatusCommand query, CancellationToken cancellationToken)
            {
                var workorder = await _context.Workorders
                    .Include(w => w.MailItems)
                    .ThenInclude(w => w.MailItemsStatusLog)
                    .Where(w => w.Id == query.Id)
                    .FirstOrDefaultAsync();

                workorder.WorkOrderStatusId = query.NewStatusId;

                workorder.MailItems.ForEach(item =>
                {
                    item.StatusId = (int)MailItemStatuses.Dispatched;
                    if (item.MailItemsStatusLog != null && item.MailItemsStatusLog.Count > 0)
                    {
                        item.MailItemsStatusLog.Add(
                             new MailItemsStatusLog()
                             {
                                 CreatedBy = _currentUser.Id,
                                 CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                 IsDeleted = false,
                                 WorkOrderId = item.WorkorderId,
                                 MailItemStatusId = (int)MailItemStatuses.Dispatched
                             });
                    }
                    else
                    {
                        item.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                                    new MailItemsStatusLog()
                                    {
                                        CreatedBy=_currentUser.Id,
                                        CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                         WorkOrderId=item.WorkorderId,
                                        MailItemStatusId = (int)MailItemStatuses.Dispatched
                                    }
                                };
                    }
                });
                _context.Workorders.Update(workorder);
                var result = await _context.SaveChangesAsync();

                return result;
            }

        }
    }
}
