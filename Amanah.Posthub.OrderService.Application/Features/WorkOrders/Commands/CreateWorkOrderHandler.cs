﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Authentication;
using Microsoft.Extensions.Configuration;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    class CreateWorkOrderHandler : IRequestHandler<CreateWorkOrderInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly IConfiguration _configuration;
        private ICurrentUser _currentUser;


        public CreateWorkOrderHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<WorkOrder> workOrderRepository,
            IConfiguration configuration,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _configuration = configuration;
            _currentUser = currentUser;
        }
        public async Task<int> Handle(CreateWorkOrderInputDTO command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
          {
              workOrder = _mapper.Map<WorkOrder>(command);
              _workOrderRepository.Add(workOrder);
              await _unityOfWork.SaveChangesAsync();

              ///Add Mail Items 
              if (command.MailItemIds != null)
              {
                  var mailItems = await _context.MailItems
                       .Include(c => c.MailItemsStatusLog)
                       .Include(c => c.Order)
                       .ThenInclude(c => c.OrdersStatusLog)
                       .IgnoreQueryFilters()
                       .Where(x => command.MailItemIds.Contains(x.Id))
                       .ToListAsync();

                  foreach (MailItem item in mailItems)
                  {
                      item.WorkorderId = workOrder.Id;
                      item.StatusId = (int)MailItemStatuses.Dispatched; // set its status to be 'Dispatched'
                      item.Order.StatusId = (int)OrderStatuses.InProcess;
                      if (item.Order.OrdersStatusLog != null && item.Order.OrdersStatusLog.Count > 0)
                      {
                          item.Order.OrdersStatusLog.Add(
                               new OrdersStatusLog()
                               {
                                   CreatedBy = _currentUser.Id,
                                   CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                   IsDeleted = false,
                                   OrderStatusId = (int)OrderStatuses.InProcess
                               });
                      }
                      else
                      {
                          item.Order.OrdersStatusLog = new List<OrdersStatusLog>() {
                                    new OrdersStatusLog()
                                    {
                                        CreatedBy =_currentUser.Id,
                                        CreationTime =DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                        OrderStatusId =  (int)OrderStatuses.InProcess
                                    }
                                };
                      }

                      if (item.MailItemsStatusLog != null && item.MailItemsStatusLog.Count > 0)
                      {
                          item.MailItemsStatusLog.Add(
                               new MailItemsStatusLog()
                               {
                                   CreatedBy = _currentUser.Id,
                                   CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                   IsDeleted = false,
                                   WorkOrderId = item.WorkorderId,
                                   MailItemStatusId = (int)MailItemStatuses.Dispatched
                               });
                      }
                      else
                      {
                          item.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                                    new MailItemsStatusLog()
                                    {
                                        CreatedBy=_currentUser.Id,
                                        CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                         WorkOrderId=item.WorkorderId,
                                        MailItemStatusId = (int)MailItemStatuses.Dispatched
                                    } };
                      }
                      _context.MailItems.Update(item);
                      // add to status log

                  }
                  await _unityOfWork.SaveChangesAsync();
              }
          });
            if (result)
            {
                return workOrder.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
