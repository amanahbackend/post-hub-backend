﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    public class CreateWorkorderFromDriversCommand : IRequest<bool>
    {
        public List<int> DriversId { get; set; }

        public class CreateWorkorderFromDriversCommandHandler : IRequestHandler<CreateWorkorderFromDriversCommand, bool>
        {
            private readonly ApplicationDbContext _context;
            private readonly IRepository<WorkOrder> _workOrderRepository;
            private readonly IUnitOfWork _unityOfWork;
            private readonly IConfiguration _configuration;
            private ICurrentUser _currentUser;

            public CreateWorkorderFromDriversCommandHandler(ApplicationDbContext context,
                IRepository<WorkOrder> workOrderRepository,
                IUnitOfWork unityOfWork,
                IConfiguration configuration,
                ICurrentUser currentUser)
            {
                _context = context;
                _workOrderRepository = workOrderRepository;
                _unityOfWork = unityOfWork;
                _configuration = configuration;
                _currentUser = currentUser;
            }

            public async Task<bool> Handle(CreateWorkorderFromDriversCommand request, CancellationToken cancellationToken)
            {
                bool result = false;
                var driverGiofences = await _context.DriverDeliveryGeoFence
                    .Include(d => d.GeoFence)
                    .ThenInclude(d => d.GeoFenceBlocks)
                    .IgnoreQueryFilters()
                    .Where(d => !d.GeoFence.IsDeleted && request.DriversId.Contains(d.Driver.Id))
                    .Select(d => new DriverGeoFences
                    {
                        DriverId = d.DriverId,
                        GeoFenceId = d.GeoFenceId,
                        AreaBlocks = d.GeoFence.GeoFenceBlocks
                        .Where(g => !g.IsDeleted)
                        .Select(g => new AreaBlocks
                        {
                            Area = g.Area,
                            Block = g.Block
                        }).ToList()
                    }).ToListAsync();

                if (driverGiofences != null && driverGiofences.Any())
                {
                    _unityOfWork.IsTenantFilterEnabled = false;

                    result = await _unityOfWork.RunTransaction(async () =>
                    {
                        foreach (var d in driverGiofences)
                        {
                            var areas = d.AreaBlocks.Select(a => a.Area);
                            var blocks = d.AreaBlocks.Select(a => a.Block);

                            var mailitems = await _context.MailItems
                            .Include(c => c.MailItemsStatusLog)
                            .Include(c => c.Order)
                            .ThenInclude(c => c.OrdersStatusLog)
                            .IgnoreQueryFilters()
                            .Where(m => !m.IsDeleted && (m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.Returned) && (areas != null && areas.Contains(m.DropOffAddress.Area)) || (blocks != null && blocks.Contains(m.DropOffAddress.Block)))
                            .ToListAsync();

                            if (mailitems.Count > 0)
                            {
                                WorkOrder workOrder = null;
                                workOrder = new WorkOrder();
                                workOrder.DriverId = d.DriverId;
                                workOrder.Code = "Code" + (new Random()).Next(10000001, int.MaxValue);
                                workOrder.WorkOrderStatusId = (int)WorkorderStatuses.Dispatched;
                                workOrder.Priorty = WorkOrderPriorty.Hight;
                                workOrder.PlannedStart = DateTime.Now;
                                workOrder.IssueAt = DateTime.Now;
                                workOrder.DeliveryBefore = DateTime.Now.Date.AddDays(1).AddSeconds(-1);

                                _workOrderRepository.Add(workOrder);

                                foreach (MailItem item in mailitems)
                                {
                                    item.WorkorderId = workOrder.Id;
                                    item.StatusId = (int)MailItemStatuses.Dispatched; // set its status to be 'Dispatched'
                                    item.Order.StatusId = (int)OrderStatuses.InProcess;
                                    if (item.Order.OrdersStatusLog != null && item.Order.OrdersStatusLog.Count > 0)
                                    {
                                        item.Order.OrdersStatusLog.Add(
                                             new OrdersStatusLog()
                                             {
                                                 CreatedBy = _currentUser.Id,
                                                 CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                                 IsDeleted = false,
                                                 OrderStatusId = (int)OrderStatuses.InProcess
                                             });
                                    }
                                    else
                                    {
                                        item.Order.OrdersStatusLog = new List<OrdersStatusLog>() {
                                    new OrdersStatusLog()
                                    {
                                        CreatedBy =_currentUser.Id,
                                        CreationTime =DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                        OrderStatusId =  (int)OrderStatuses.InProcess
                                    }
                                };
                                    }

                                    if (item.MailItemsStatusLog != null && item.MailItemsStatusLog.Count > 0)
                                    {
                                        item.MailItemsStatusLog.Add(
                                             new MailItemsStatusLog()
                                             {
                                                 CreatedBy = _currentUser.Id,
                                                 CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                                 IsDeleted = false,
                                                 WorkOrderId = item.WorkorderId,
                                                 MailItemStatusId = (int)MailItemStatuses.Dispatched
                                             });
                                    }
                                    else
                                    {
                                        item.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                                    new MailItemsStatusLog()
                                    {
                                        CreatedBy=_currentUser.Id,
                                        CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                         WorkOrderId=item.WorkorderId,
                                        MailItemStatusId = (int)MailItemStatuses.Dispatched
                                    } };
                                    }
                                    _context.MailItems.Update(item);
                                }
                            }
                        }
                        await _unityOfWork.SaveChangesAsync();
                        result = true;
                    });
                }
                return result;
            }
        }

        public class DriverGeoFences
        {
            public int DriverId { get; set; }
            public int GeoFenceId { get; set; }
            public List<AreaBlocks> AreaBlocks { get; set; }
        }
        public class AreaBlocks
        {
            public string Area { get; set; }
            public string Block { get; set; }
        }
    }
}
