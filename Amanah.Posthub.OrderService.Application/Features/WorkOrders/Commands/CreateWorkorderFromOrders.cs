﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    public class CreateWorkorderFromOrdersHandler : IRequestHandler<WorkorderFromOrdersDto, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly IConfiguration _configuration;
        private ICurrentUser _currentUser;

        public CreateWorkorderFromOrdersHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<WorkOrder> workOrderRepository,
            IConfiguration configuration,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _configuration = configuration;
            _currentUser = currentUser;
        }
        public async Task<int> Handle(WorkorderFromOrdersDto command, CancellationToken cancellationToken)
        {
            if (command.MailItemsId.Count <= 0 && command.OrdersId.Count <= 0)
                return 0;

            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                var mailitems = await _context.MailItems
                .Include(c => c.MailItemsStatusLog)
                .Include(c => c.Order)
                .ThenInclude(c => c.OrdersStatusLog)
                .IgnoreQueryFilters()
                .Where(m => !m.IsDeleted && (m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.Returned) && ((command.MailItemsId != null && command.MailItemsId.Contains(m.Id)) || command.OrdersId != null && command.OrdersId.Contains(m.OrderId.Value)))
                .ToListAsync();

                if (mailitems.Count > 0)
                {
                    workOrder = new WorkOrder();
                    workOrder.DriverId = command.DriverId;
                    workOrder.Code = "Code" + (new Random()).Next(10000001, int.MaxValue).ToString();
                    workOrder.WorkOrderStatusId = 5;
                    workOrder.Priorty = WorkOrderPriorty.Hight;
                    workOrder.PlannedStart = DateTime.Now;
                    workOrder.IssueAt = DateTime.Now;
                    workOrder.DeliveryBefore = DateTime.Now.Date.AddDays(1).AddSeconds(-1);


                    _workOrderRepository.Add(workOrder);
                    await _unityOfWork.SaveChangesAsync();

                    // Add Mail Items
                    foreach (MailItem item in mailitems)
                    {
                        item.WorkorderId = workOrder.Id;
                        item.StatusId = (int)MailItemStatuses.Dispatched; // set its status to be 'Dispatched'
                        item.Order.StatusId = (int)OrderStatuses.InProcess;

                        // add to status log
                        if (item.Order.OrdersStatusLog != null && item.Order.OrdersStatusLog.Count > 0)
                        {
                            item.Order.OrdersStatusLog.Add(
                                 new OrdersStatusLog()
                                 {
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                     IsDeleted = false,
                                     OrderStatusId = (int)OrderStatuses.InProcess
                                 });
                        }
                        else
                        {
                            item.Order.OrdersStatusLog = new List<OrdersStatusLog>() {
                                    new OrdersStatusLog()
                                    {
                                        CreatedBy =_currentUser.Id,
                                        CreationTime =DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                        OrderStatusId =  (int)OrderStatuses.InProcess
                                    }
                                };
                        }

                        if (item.MailItemsStatusLog != null && item.MailItemsStatusLog.Count > 0)
                        {
                            item.MailItemsStatusLog.Add(
                                 new MailItemsStatusLog()
                                 {
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                     IsDeleted = false,
                                     WorkOrderId = item.WorkorderId,
                                     MailItemStatusId = (int)MailItemStatuses.Dispatched
                                 });
                        }
                        else
                        {
                            item.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                                    new MailItemsStatusLog()
                                    {
                                        CreatedBy=_currentUser.Id,
                                        CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                        IsDeleted = false,
                                         WorkOrderId=item.WorkorderId,
                                        MailItemStatusId = (int)MailItemStatuses.Dispatched
                                    } };
                        }
                        _context.MailItems.Update(item);

                    }

                    await _unityOfWork.SaveChangesAsync();

                }
            });

            if (result)
            {
                return workOrder.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
