﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class ActivateWorkOrderInputDTO : IRequest<int>
    {
        public int Id { get; set; }
        public int WorkOrderStatusId { get; set; }
    }
}
