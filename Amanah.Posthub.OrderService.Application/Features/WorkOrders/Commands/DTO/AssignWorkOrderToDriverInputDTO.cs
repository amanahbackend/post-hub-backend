﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class AssignWorkOrderToDriverInputDTO : IRequest<int>
    {
        public int DriverId { set; get; }
        public int WorkOrderId { set; get; }

    }


}
