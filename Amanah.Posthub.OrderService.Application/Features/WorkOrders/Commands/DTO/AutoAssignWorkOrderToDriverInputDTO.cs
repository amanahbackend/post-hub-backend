﻿using MediatR;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class AutoAssignWorkOrderToDriverInputDTO : IRequest<int>
    {
        public List<int> DefaultDriverIds { set; get; }
    }


}
