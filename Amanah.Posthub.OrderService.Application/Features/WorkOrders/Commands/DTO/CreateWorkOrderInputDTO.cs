﻿using Amanah.Posthub.Service.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class CreateWorkOrderInputDTO : IRequest<int>
    {
        public string Code { get; set; }
        public DateTime? IssueAt { get; set; }
        public WorkOrderPriorty Priorty { set; get; }
        public DateTime? PlannedStart { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public int WorkOrderStatusId { get; set; }
        public DateTime? LastStatusDate { get; set; }
        public int ItemsNo { get; set; }
        public string AreasCovered { get; set; }
        public List<int> MailItemIds { set; get; }

    }
}
