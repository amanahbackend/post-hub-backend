﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class DeleteWorkOrderInputDTO : IRequest<int>
    {
        public int Id { get; set; }
    }
}
