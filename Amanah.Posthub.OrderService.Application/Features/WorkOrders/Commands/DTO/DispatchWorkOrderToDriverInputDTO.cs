﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class DispatchWorkOrderToDriverInputDTO : IRequest<int>
    {
        public int DriverId { set; get; }
        public int WorkOrderId { set; get; }

    }
}
