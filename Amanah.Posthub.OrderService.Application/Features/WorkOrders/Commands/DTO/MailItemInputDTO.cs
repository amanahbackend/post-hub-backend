﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class MailItemInputDTO
    {

        public string MailItemBarCode { get; set; }

        public DateTime? DeliveryBefore { get; set; }

        public Service.Domain.Addresses.Entities.Address PickUpAddress { get; set; }
        public Service.Domain.Addresses.Entities.Address DropOffAddress { get; set; }

        public int? ConsigneeInfoId { get; set; }
        public int? ReciverInfoId { get; set; }
        public bool IsMatchConsigneeID { get; set; }
        public int? Weight { get; set; }
        public int? WeightUOMId { get; set; }
        public int? Hight { get; set; }
        public int? Width { get; set; }
        public int? LengthUOMId { get; set; }
        public string ReservedSpecial { get; set; }
        public string Notes { get; set; }
        public int? ItemTypeId { get; set; }
        public int? StatusId { get; set; }
        public int? WorkorderId { get; set; }
        public int? OrderId { get; set; }
        public int ItemDeleveryTypeId { get; set; } // 1 for Pickup, 2 for Drop
        public List<int> MailItemIds { set; get; }

    }
}
