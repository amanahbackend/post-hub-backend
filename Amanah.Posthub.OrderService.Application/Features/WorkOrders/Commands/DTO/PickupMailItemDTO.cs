﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class PickupMailItemDTO : IRequest<int>
    {
        public int Id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

    }
}
