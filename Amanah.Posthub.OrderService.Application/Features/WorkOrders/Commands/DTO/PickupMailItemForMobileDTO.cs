﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class PickupMailItemForMobileDTO : IRequest<int>
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string SignatureImgUrl { set; get; }
        public List<string> PickupImgsUrls { set; get; }
        public IFormFile SignatureFile { set; get; }
        public List<IFormFile> PickupFiles { set; get; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
