﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class ReassignWorkOrderToDriverInputDTO : IRequest<int>
    {
        public int DriverId { set; get; }
        public int WorkOrderId { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
    }
}
