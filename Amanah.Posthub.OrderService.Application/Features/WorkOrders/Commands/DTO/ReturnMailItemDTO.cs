﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class ReturnMailItemDTO : IRequest<int>
    {
        public int Id { get; set; }
        public int WorkOrderId { get; set; }
        public int? ItemReturnReasonId { get; set; }
        public string OtherItemReturnReason { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

}
