﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class UpdateMailItemInputDto : IRequest<int>
    {
        #region Fileds
        public int Id { get; set; }
        public string MailItemBarCode { get; set; }
        public DateTime? DropOffDate { get; set; }
        public string Latittude { get; set; }
        public string Longitude { get; set; }
        public string Notes { get; set; }
        public int? StatusId { get; set; }
        public List<string> ProofOfDeliveryImageURLs { get; set; }
        public string SignatureImageURL { get; set; }
        public string RecieverName { get; set; }
        public string RecieverMobile { get; set; }
        public int? WorkOrderId { get; set; }
        public IFormFile SignatureFile { set; get; }
        public IFormFileCollection PhotosFiles { set; get; }
        #endregion

    }
    public class Image
    {
        public string Mime { get; set; }
        public string Data { get; set; }
    }
}
