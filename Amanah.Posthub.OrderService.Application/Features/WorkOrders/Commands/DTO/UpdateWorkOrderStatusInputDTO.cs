﻿using MediatR;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class UpdateWorkOrderStatusInputDTO : IRequest<int>
    {
        public int Id { get; set; }
        public int? WorkOrderStatusId { get; set; }
    }
}
