﻿using MediatR;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class WorkorderFromOrdersDto : IRequest<int>
    {
        public int DriverId { get; set; }
        public List<int> OrdersId { get; set; }
        public List<int> MailItemsId { get; set; }
    }



}
