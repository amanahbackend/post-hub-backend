﻿using MediatR;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO
{
    public class WorkorderFromAreaBlockDto : IRequest<int>
    {
        public int DriverId { get; set; }
        public List<string> AreasId { get; set; }
        public List<string> BlocksId { get; set; }
        public List<int> MailItemsId { get; set; }
    }
}
