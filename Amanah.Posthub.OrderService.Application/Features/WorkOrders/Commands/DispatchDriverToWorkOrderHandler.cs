﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.Enums;
using AutoMapper;
using global::Amanah.Posthub.Context;
using global::Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using global::Amanah.Posthub.Service.Domain.Entities;
using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{

    public class DispatchDriverToWorkOrderHandler : IRequestHandler<DispatchWorkOrderToDriverInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly ICurrentUser _currentUser;
        private readonly IFirebasePushNotificationService _pushNotificationService;


        public DispatchDriverToWorkOrderHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<WorkOrder> workOrderRepository,
            ICurrentUser currentUser,
            IFirebasePushNotificationService pushNotificationService)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _currentUser = currentUser;
            _pushNotificationService = pushNotificationService;
        }
        public async Task<int> Handle(DispatchWorkOrderToDriverInputDTO command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                workOrder = _context.Workorders
                           .IgnoreQueryFilters()
                           .Where(c => c.Id == command.WorkOrderId) //await _workOrderRepository.GetByIdAsync(command.WorkOrderId);
                           .Include(m => m.MailItems)
                           .ThenInclude(m => m.MailItemsStatusLog)
                           .Include(m => m.MailItems)
                           .ThenInclude(c => c.Order)
                           .FirstOrDefault();
                if (workOrder != null)
                {


                    workOrder.DriverId = command.DriverId;
                    workOrder.WorkOrderStatusId = (int)WorkorderStatuses.Dispatched;
                    if (workOrder.MailItems != null && workOrder.MailItems.Count > 0)
                    {

                        workOrder.MailItems.ForEach(m =>
                     {

                         if (m.MailItemsStatusLog != null && m.MailItemsStatusLog.Count > 0)
                         {
                             m.MailItemsStatusLog.Add(
                                  new MailItemsStatusLog()
                                  {
                                      // CreatedBy = command.UserId,
                                      CreationTime = DateTime.UtcNow,
                                      IsDeleted = false,
                                      WorkOrderId=m.WorkorderId,
                                      MailItemStatusId = (int)MailItemStatuses.Dispatched

                                  });
                         }
                         else
                         {
                             m.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             // CreatedBy = command.UserId,
                             CreationTime = DateTime.UtcNow,
                             IsDeleted = false,
                              WorkOrderId=m.WorkorderId,
                             MailItemStatusId = (int)MailItemStatuses.Dispatched
                         }
                              };
                         }

                         m.StatusId = (int)MailItemStatuses.Dispatched;
                         m.Order.StatusId = (int)OrderStatuses.InProcess;
                     });
                    }
                    _workOrderRepository.Update(workOrder);


                    await _unityOfWork.SaveChangesAsync();

                    var driverUserId = await _context.Driver
                     .IgnoreQueryFilters()
                     .Where(x => x.Id == command.DriverId)
                     .Select(x => x.UserId)
                     .FirstOrDefaultAsync();
                    if (driverUserId != null)
                        await _pushNotificationService.SendAsync(driverUserId, "Post-Hub", "You Have New WorkOrder.", new { NotificationType = NotificationTypeEnum.NewWorkOrder });

                }

            });
            if (result)
            {
                if (workOrder != null)
                {

                    return workOrder.Id;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }
    }

}
