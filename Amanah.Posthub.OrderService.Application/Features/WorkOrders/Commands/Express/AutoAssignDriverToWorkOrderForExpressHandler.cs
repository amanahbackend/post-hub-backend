﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using AutoMapper;
using global::Amanah.Posthub.Context;
using global::Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using global::Amanah.Posthub.Service.Domain.Entities;
using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.Express
{
    public class AutoAssignWorkOrderToDriverForExpressCommand : IRequest<int>
    {
        public List<int> DefaultDriverIds { set; get; }
    }


    public class AutoAssignDriverToWorkOrderForExpressHandler : IRequestHandler<AutoAssignWorkOrderToDriverForExpressCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly IFirebasePushNotificationService _pushNotificationService;


        public AutoAssignDriverToWorkOrderForExpressHandler(
                ApplicationDbContext context,
                IMapper mapper,
                IUnitOfWork unityOfWork,
                IRepository<WorkOrder> workOrderRepository,
                IFirebasePushNotificationService pushNotificationService)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _pushNotificationService = pushNotificationService;
        }
        public async Task<int> Handle(AutoAssignWorkOrderToDriverForExpressCommand command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                Random rnd = new Random();
                foreach (var defaultDriverId in command.DefaultDriverIds)
                {
                    WorkOrder workOrder = new WorkOrder();

                    var mailItems = await _context
                    .MailItems
                    .Include(m => m.Order)
                        .IgnoreQueryFilters()
                        .Where(a => a.DefaultAssignedDriverID == defaultDriverId
                            && !a.IsDeleted
                            && !a.Order.IsDeleted
                            && !a.Order.IsDraft
                            && a.Order.ServiceSectorId == (int)ServiceSectors.LocalPost
                            && (a.StatusId == (int)MailItemStatuses.New || a.StatusId == (int)MailItemStatuses.ReadyForDispatch))
                        .Include(a => a.Order)
                        .ToListAsync();
                    if (mailItems.Count == 0)
                    {
                        continue;
                    }
                    foreach (var item in mailItems)
                    {
                        item.StatusId = (int)MailItemStatuses.Dispatched;
                        item.Order.StatusId = (int)OrderStatuses.InProcess;
                        _context.MailItems.Update(item);
                    }
                    workOrder.MailItems = mailItems;
                    workOrder.WorkOrderStatusId = (int)WorkOrderStatuses.Dispatched;
                    workOrder.DriverId = defaultDriverId;
                    workOrder.Priorty = WorkOrderPriorty.Normal;
                    workOrder.IssueAt = DateTime.Now;
                    workOrder.AssignedDate = DateTime.Now;
                    workOrder.ItemsNo = mailItems.Count;
                    workOrder.Code = "AUTO" + rnd.Next(10000001, 999999999).ToString();
                    _workOrderRepository.Add(workOrder);

                    var driverUserId = await _context.Driver
                        .IgnoreQueryFilters()
                        .Where(x => x.Id == defaultDriverId)
                        .Select(x => x.UserId)
                        .FirstOrDefaultAsync();
                    await _pushNotificationService.SendAsync(driverUserId, "Post-Hub", "You Have New WorkOrder #" + workOrder.Code, new { NotificationType = NotificationTypeEnum.NewWorkOrder });
                }
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return 200;
            }
            else
            {
                return 0;
            }

        }
    }
}


