﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.Express
{
    public class CreateWorkOrderForExpressCommand : IRequest<int>
    {
        public string Code { get; set; }
        public DateTime? IssueAt { get; set; }
        public WorkOrderPriorty Priorty { set; get; }
        public DateTime? PlannedStart { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public int WorkOrderStatusId { get; set; }
        public DateTime? LastStatusDate { get; set; }
        public int ItemsNo { get; set; }
        public string AreasCovered { get; set; }
        public List<int> MailItemIds { set; get; }

    }

    public class CreateWorkOrderForExpressHandler : IRequestHandler<CreateWorkOrderForExpressCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly ICurrentUser _currentUser;


        public CreateWorkOrderForExpressHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<WorkOrder> workOrderRepository,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _currentUser = currentUser;
        }
        public async Task<int> Handle(CreateWorkOrderForExpressCommand command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                workOrder = _mapper.Map<WorkOrder>(command);
                _workOrderRepository.Add(workOrder);
                await _unityOfWork.SaveChangesAsync();

                ///Add Mail Items 
                if (command.MailItemIds != null)
                {
                    var mailItems = await _context.MailItems
                         .Include(c => c.MailItemsStatusLog)
                         .Include(c => c.Order)
                         .ThenInclude(c => c.OrdersStatusLog)
                         .IgnoreQueryFilters()
                         .Where(x => command.MailItemIds.Contains(x.Id))
                         .ToListAsync();

                    foreach (MailItem item in mailItems)
                    {
                        item.WorkorderId = workOrder.Id;
                        item.StatusId = (int)MailItemStatuses.Dispatched; // set its status to be 'Dispatched'
                        item.Order.StatusId = (int)OrderStatuses.InProcess;
                        if (item.MailItemsStatusLog != null && item.MailItemsStatusLog.Count > 0)
                        {
                            item.MailItemsStatusLog.Add(
                                 new MailItemsStatusLog()
                                 {
                                     CreatedBy = _currentUser.Id,
                                     WorkOrderId = item.WorkorderId,
                                     CreationTime = DateTime.UtcNow,
                                     IsDeleted = false,
                                     MailItemStatusId = (int)MailItemStatuses.Dispatched

                                 });
                        }
                        else
                        {
                            item.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                                             new MailItemsStatusLog()
                                         {
                                              CreatedBy =  _currentUser.Id,
                                             WorkOrderId = item.WorkorderId,
                                             CreationTime = DateTime.UtcNow,
                                             IsDeleted = false,
                                             MailItemStatusId = (int)MailItemStatuses.Dispatched
                                         }
                                         };
                        }

                        if (item.Order.OrdersStatusLog != null && item.Order.OrdersStatusLog.Count > 0)
                        {
                            item.Order.OrdersStatusLog.Add(
                                 new OrdersStatusLog()
                                 {
                                     OrderId = item.OrderId,
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow,
                                     IsDeleted = false,
                                     OrderStatusId = (int)OrderStatuses.InProcess,
                                     LastModificationTime = DateTime.UtcNow,
                                     DeletionTime = DateTime.UnixEpoch,
                                     Note = "",
                                     DeletedBy = "",
                                     LastModifiedBy = ""

                                 });
                        }
                        else
                        {
                            item.Order.OrdersStatusLog = new List<OrdersStatusLog>() {
                                             new OrdersStatusLog()
                                         {

                                                     OrderId = item.OrderId,
                                                     CreatedBy = _currentUser.Id,
                                                     CreationTime = DateTime.UtcNow,
                                                     IsDeleted = false,
                                                     OrderStatusId = (int)OrderStatuses.InProcess,
                                                     LastModificationTime = DateTime.UtcNow,
                                                     DeletionTime = DateTime.UnixEpoch,
                                                     Note = "",
                                                     DeletedBy = "",
                                                     LastModifiedBy = ""
                                         }
                                         };
                        }
                        _context.MailItems.Update(item);
                        // add to status log

                    }
                    await _unityOfWork.SaveChangesAsync();
                }
            });
            if (result)
            {
                return workOrder.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
