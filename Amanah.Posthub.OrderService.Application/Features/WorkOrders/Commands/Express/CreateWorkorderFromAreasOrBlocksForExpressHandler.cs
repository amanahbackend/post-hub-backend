﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.Express
{

    public class WorkorderFromAreaBlockForExpressCommand : IRequest<int>
    {
        public int DriverId { get; set; }
        public List<string> AreasId { get; set; }
        public List<string> BlocksId { get; set; }
        public List<int> MailItemsId { get; set; }
    }

    public class CreateWorkorderFromAreasOrBlocksForExpressHandler : IRequestHandler<WorkorderFromAreaBlockForExpressCommand, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly ICurrentUser _currentUser;

        public CreateWorkorderFromAreasOrBlocksForExpressHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<WorkOrder> workOrderRepository,
             ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _currentUser = currentUser;
        }
        public async Task<int> Handle(WorkorderFromAreaBlockForExpressCommand command, CancellationToken cancellationToken)
        {
            if (command.MailItemsId.Count <= 0 && command.AreasId.Count <= 0 && command.BlocksId.Count <= 0)
                return 0;

            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                var mailitems = await _context.MailItems
                .Include(c => c.MailItemsStatusLog)
                .Include(c => c.Order)
                .ThenInclude(c => c.OrdersStatusLog)
                .IgnoreQueryFilters()
                .Where(m => !m.IsDeleted && (m.StatusId == (int)MailItemStatuses.New || m.StatusId == (int)MailItemStatuses.Returned) && ((command.MailItemsId != null && command.MailItemsId.Contains(m.Id)) || (command.AreasId != null && command.AreasId.Contains(m.DropOffAddress.Area)) || (command.BlocksId != null && command.BlocksId.Contains(m.DropOffAddress.Block))))
                .ToListAsync();

                if (mailitems.Count > 0)
                {
                    workOrder = new WorkOrder();
                    workOrder.DriverId = command.DriverId;
                    workOrder.Code = "Code" + (new Random()).Next(10000001, int.MaxValue);
                    workOrder.WorkOrderStatusId = (int)WorkorderStatuses.Dispatched;
                    workOrder.Priorty = WorkOrderPriorty.Hight;
                    workOrder.PlannedStart = DateTime.Now;
                    workOrder.IssueAt = DateTime.Now;
                    workOrder.DeliveryBefore = DateTime.Now.Date.AddDays(1).AddSeconds(-1);

                    _workOrderRepository.Add(workOrder);
                    await _unityOfWork.SaveChangesAsync();

                    // Add Mail Items
                    foreach (MailItem item in mailitems)
                    {
                        item.WorkorderId = workOrder.Id;
                        item.StatusId = (int)MailItemStatuses.Dispatched; // set its status to be 'Dispatched'
                        item.Order.StatusId = (int)OrderStatuses.InProcess;
                        if (item.MailItemsStatusLog != null && item.MailItemsStatusLog.Count > 0)
                        {
                            item.MailItemsStatusLog.Add(
                                 new MailItemsStatusLog()
                                 {
                                     // CreatedBy = command.UserId,
                                     WorkOrderId = item.WorkorderId,
                                     CreationTime = DateTime.UtcNow,
                                     IsDeleted = false,
                                     MailItemStatusId = (int)MailItemStatuses.Dispatched

                                 });
                        }
                        else
                        {
                            item.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             // CreatedBy = command.UserId,
                             WorkOrderId = item.WorkorderId,
                             CreationTime = DateTime.UtcNow,
                             IsDeleted = false,
                             MailItemStatusId = (int)MailItemStatuses.Dispatched
                         }
                         };
                        }
                        if (item.Order.OrdersStatusLog != null && item.Order.OrdersStatusLog.Count > 0)
                        {
                            item.Order.OrdersStatusLog.Add(
                                 new OrdersStatusLog()
                                 {
                                     OrderId = item.OrderId,
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow,
                                     IsDeleted = false,
                                     OrderStatusId = (int)OrderStatuses.InProcess,
                                     LastModificationTime = DateTime.UtcNow,
                                     DeletionTime = DateTime.UnixEpoch,
                                     Note = "",
                                     DeletedBy = "",
                                     LastModifiedBy = ""

                                 });
                        }
                        else
                        {
                            item.Order.OrdersStatusLog = new List<OrdersStatusLog>() {
                                             new OrdersStatusLog()
                                         {

                                                     OrderId = item.OrderId,
                                                     CreatedBy = _currentUser.Id,
                                                     CreationTime = DateTime.UtcNow,
                                                     IsDeleted = false,
                                                     OrderStatusId = (int)OrderStatuses.InProcess,
                                                     LastModificationTime = DateTime.UtcNow,
                                                     DeletionTime = DateTime.UnixEpoch,
                                                     Note = "",
                                                     DeletedBy = "",
                                                     LastModifiedBy = ""
                                         }
                                         };
                        }

                        _context.MailItems.Update(item);
                    }
                    await _unityOfWork.SaveChangesAsync();
                    result = true;

                }
                else result = false;
            });
            if (result)
            {
                return workOrder.Id;
            }
            else
            {
                return 0;
            }

        }
    }


}
