﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using Amanah.PaymentIntegrations.Services;
using Amanah.PaymentIntegrations.DTOs;
using Amanah.PaymentIntegrations.DTOs.Enums;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Microsoft.Extensions.Configuration;
using Amanah.Posthub.BASE.Authentication;


namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    public class PickupMailItemForMobileHandler : IRequestHandler<PickupMailItemForMobileDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<MailItem> _workOrderRepository;
        private readonly IPaymentService _paymentService;
        private readonly IConfiguration _configuration;
        private ICurrentUser _currentUser;

        public PickupMailItemForMobileHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<MailItem> workOrderRepository,
            IPaymentService paymentService,
            IConfiguration configuration,
            ICurrentUser currentUser)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _paymentService = paymentService;
            _configuration = configuration;
            _currentUser = currentUser;
        }
        public async Task<int> Handle(PickupMailItemForMobileDTO command, CancellationToken cancellationToken)
        {
            MailItem mailItem = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                //mailItem = _mapper.Map<MailItem>(command);

                mailItem = _context.MailItems
                .IgnoreQueryFilters()
                .Where(c => c.Id == command.Id)
                .Include(a => a.PickUpAddress)
                .Include(a => a.MailItemsStatusLog)
                .Include(a => a.Workorder)
                .FirstOrDefault();

                if (mailItem != null)
                {

                    mailItem.StatusId = (int)MailItemStatuses.OnDelivery;//in progress
                    if (mailItem.Workorder !=null)
                    {
                        mailItem.Workorder.WorkOrderStatusId = (int)WorkorderStatuses.Active;// Active

                    }
                    if (mailItem.MailItemsStatusLog != null && mailItem.MailItemsStatusLog.Count > 0)
                    {
                        mailItem.MailItemsStatusLog.Add(
                             new MailItemsStatusLog()
                             {
                                 // CreatedBy = command.UserId,
                                 WorkOrderId = mailItem.WorkorderId,
                                 CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                 IsDeleted = false,
                                 MailItemStatusId = (int)MailItemStatuses.OnDelivery

                             });
                    }
                    else
                    {
                        mailItem.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             // CreatedBy = command.UserId,
                             CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                             IsDeleted = false,
                             WorkOrderId = mailItem.WorkorderId,
                            // OldStatusId = mailItemEF.StatusId,
                             MailItemStatusId =  (int)MailItemStatuses.OnDelivery
                         }
                         };
                    }


                    mailItem.StatusId = (int)MailItemStatuses.OnDelivery; // set its status to be 'OnDelivery'

                    if (mailItem.Workorder !=null)
                    {
                        mailItem.Workorder.WorkOrderStatusId = (int)WorkorderStatuses.Active;// Active

                    }

                    if (mailItem.PickUpAddress != null)
                    {
                        mailItem.PickUpAddress.Latitude = command.Latitude;
                        mailItem.PickUpAddress.Longitude = command.Longitude;
                    }

                    if (mailItem.PickUpAddress == null)
                    {
                        mailItem.PickUpAddress = new Service.Domain.Addresses.Entities.Address()
                        {
                            Latitude = command.Latitude,
                            Longitude = command.Longitude
                        };
                    }

                    var totalPrice = await _context.Orders.Include(o => o.BusinessOrder).ThenInclude(a => a.BusinessOrderServices)
                                                          .Where(o => o.Id == command.OrderId).Select(a => new {
                                                              PackageCost = a.BusinessOrder.OrderPackagePrice,
                                                              ServicesCosts = a.BusinessOrder.BusinessOrderServices.Select(s => s.Price).ToList()
                                                          }).SingleOrDefaultAsync();

                    var orderTotalPrice = totalPrice.PackageCost;
                    if (totalPrice.ServicesCosts != null && totalPrice.ServicesCosts.Count > 0)
                    {
                        foreach (var serviceCost in totalPrice.ServicesCosts)
                            orderTotalPrice += serviceCost;
                    }
                     //var payResponse = await SaveOrderPayment(command.Id, orderTotalPrice.Value);
                    //return payResponse;
                    //var response = await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
                    //{
                    //    ReferenceId = command.Id.ToString(),
                    //    TotalPrice = (double)orderTotalPrice,
                    //    PaymentGateway = "cash"
                    //});

                    mailItem.SignatureImageURL = command.SignatureImgUrl;
                    if (command.PickupImgsUrls != null && command.PickupImgsUrls.Count > 0)
                        mailItem.ProofOfDeliveryImages = command.PickupImgsUrls.Select(imgUrl => new AttachmentFile { Path = imgUrl }).ToList();

                    _workOrderRepository.Update(mailItem);
                    UpdateOrderTransactionPayment(command.OrderId);
                    await _unityOfWork.SaveChangesAsync();
                }

            });
            if (result)
            {
                if (mailItem != null)
                    return mailItem.Id;
                else return 0;
            }
            else
            {
                return 0;
            }

        }
        private void UpdateOrderTransactionPayment(int orderId)
        {
            var orderPayment = _context.PaymentTransactions.Where(x => x.ReferenceId == orderId.ToString()).OrderByDescending(x => x.Id).FirstOrDefault();
            orderPayment.IsSuccessTransaction = true;
            var result = _context.PaymentTransactions.Update(orderPayment);            
        }
        public class PickUpMailItemWithPaymentOutPut
        {
            public string PaymentURL { get; set; }
            public int BusinessOrderId { get; set; }
        }

    }
}
