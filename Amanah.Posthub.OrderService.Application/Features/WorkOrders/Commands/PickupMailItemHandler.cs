﻿using Amanah.PaymentIntegrations.DTOs;
using Amanah.PaymentIntegrations.DTOs.Enums;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders
{

    public class PickupMailItemHandler : IRequestHandler<PickupMailItemDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<MailItem> _workOrderRepository;
        private readonly IPaymentService _paymentService;
        private readonly IConfiguration _configuration;
       
        private ICurrentUser _currentUser;

        public PickupMailItemHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<MailItem> workOrderRepository ,
            IPaymentService paymentService ,
            IConfiguration configuration,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
            _paymentService = paymentService;
            _configuration = configuration;
          
            _currentUser = currentUser;
        }
        public async Task<int> Handle(PickupMailItemDTO command, CancellationToken cancellationToken)
        {
            MailItem mailItem = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                //mailItem = _mapper.Map<MailItem>(command);

                mailItem = _context.MailItems
                .IgnoreQueryFilters()
                .Where(c => c.Id == command.Id)
                .Include(a => a.PickUpAddress)
                .Include(a => a.MailItemsStatusLog)
                .Include(a => a.Workorder)
                .FirstOrDefault();

                if (mailItem != null)
                {

                    mailItem.StatusId = (int)MailItemStatuses.OnDelivery;//in progress
                    mailItem.Workorder.WorkOrderStatusId = (int)WorkorderStatuses.Active;// Active

                    if (mailItem.MailItemsStatusLog != null && mailItem.MailItemsStatusLog.Count > 0)
                    {
                        mailItem.MailItemsStatusLog.Add(
                             new MailItemsStatusLog()
                             {
                                 CreatedBy = _currentUser.Id,
                                 WorkOrderId = mailItem.WorkorderId,
                                 CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                 IsDeleted = false,
                                 MailItemStatusId = (int)MailItemStatuses.OnDelivery

                             });
                    }
                    else
                    {
                        mailItem.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             CreatedBy = _currentUser.Id,
                             CreationTime =DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                             IsDeleted = false,
                             WorkOrderId = mailItem.WorkorderId,
                             MailItemStatusId =  (int)MailItemStatuses.OnDelivery
                         }
                         };
                    }

                    mailItem.StatusId = (int)MailItemStatuses.OnDelivery;//in progress
                    mailItem.Workorder.WorkOrderStatusId = (int)WorkorderStatuses.Active;// Active


                    if (mailItem.PickUpAddress != null)
                    {
                        mailItem.PickUpAddress.Latitude = command.Latitude;
                        mailItem.PickUpAddress.Longitude = command.Longitude;
                    }

                    if (mailItem.PickUpAddress == null)
                    {
                        mailItem.PickUpAddress = new Service.Domain.Addresses.Entities.Address()
                        {
                            Latitude = command.Latitude,
                            Longitude = command.Longitude
                        };
                    }

                    // hemdan comment it because Packages and Payment for only International and I have another endpoint for international 

                    //mailItem.StatusId = (int)MailItemStatuses.OnDelivery; // set its status to be 'OnDelivery'
                    //mailItem.Workorder.WorkOrderStatusId = (int)WorkorderStatuses.Active;// Active
                    //var totalPrice = await _context.Orders.Include(o => o.BusinessOrder).ThenInclude(a => a.BusinessOrderServices)
                    //                                     .Where(o => o.Id == mailItem.OrderId).Select(a => new
                    //                                     {
                    //                                         PackageCost = a.BusinessOrder.OrderPackagePrice,
                    //                                         ServicesCosts = a.BusinessOrder.BusinessOrderServices.Select(s => s.Price).ToList()
                    //                                     }).SingleOrDefaultAsync();

                    //var orderTotalPrice = totalPrice.PackageCost;
                    //if (totalPrice.ServicesCosts != null && totalPrice.ServicesCosts.Count > 0)
                    //{
                    //    foreach (var serviceCost in totalPrice.ServicesCosts)
                    //        orderTotalPrice += serviceCost;
                    //}
                    //var payResponse = await SaveOrderPayment(mailItem.OrderId.Value, orderTotalPrice.Value);
                    _workOrderRepository.Update(mailItem);
                    await _unityOfWork.SaveChangesAsync();



                }

            });
            if (result)
            {
                if (mailItem != null)
                    return mailItem.Id;
                else return 0;
            }
            else
            {
                return 0;
            }

        }
        private async Task<PickUpMailItemWithPaymentOutPut> SaveOrderPayment(int orderId, decimal orderTotalPrice)
        {
            PickUpMailItemWithPaymentOutPut pickUpMailItemWithPaymentOutPut = new PickUpMailItemWithPaymentOutPut();
            var successUrl = _configuration.GetSection("UpPaymentsConfiguration")["SuccessURL"];
            var failUrl = _configuration.GetSection("UpPaymentsConfiguration")["FailURL"];
            var NotifyUrl = _configuration.GetSection("UpPaymentsConfiguration")["NotifyURL"];
            var paymentMode = _configuration.GetSection("UpPaymentsConfiguration")["PaymentMode"];
            var response = await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
            {
                ReferenceId = orderId.ToString(),
                TotalPrice = (double)orderTotalPrice,
                PaymentGateway = "knet",
                CurrencyCode = "KWD",
                PaymentMode = paymentMode == "1" ? PaymentMode.TestingMode : PaymentMode.ProductionMode,
                Whitelabled = WhiteLabel.On, // without direct url ... the customer will need to choose his prefered way  
                UpPaymentConfiguration = new UpPaymentConfigurationDTO
                {
                    SuccessURL = successUrl,
                    //"https://amanah-paymentintegrations-test.conveyor.cloud/api/Payments/Success",
                    FailURL = failUrl,
                    NotifyURL = NotifyUrl,
                },
                Product = new RequestPaymentProductRequestDTO
                {
                    Title = "OrderPackagesPayment",
                    Names = new string[1] { orderId.ToString() },
                    Prices = new double[1] { (double)orderTotalPrice },
                    Quantities = new string[1] { "1" },
                }

            });
            pickUpMailItemWithPaymentOutPut.BusinessOrderId = orderId;
            pickUpMailItemWithPaymentOutPut.PaymentURL = response.Data.PaymentURL;
            return pickUpMailItemWithPaymentOutPut;

        }
        public class PickUpMailItemWithPaymentOutPut
        {
            public string PaymentURL { get; set; }
            public int BusinessOrderId { get; set; }
        }
    }
}
