﻿using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using AutoMapper;
using global::Amanah.Posthub.Context;
using global::Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using global::Amanah.Posthub.Service.Domain.Entities;
using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{

    public class ReassignDriverToWorkorderHandler : IRequestHandler<ReassignWorkOrderToDriverInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;
        private readonly IFirebasePushNotificationService _pushNotificationService;



        public ReassignDriverToWorkorderHandler(
                ApplicationDbContext context,
                IMapper mapper,
                IUnitOfWork unityOfWork,
                IFirebasePushNotificationService pushNotificationService,
                IRepository<WorkOrder> workOrderRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _pushNotificationService = pushNotificationService;
            _workOrderRepository = workOrderRepository;
        }
        public async Task<int> Handle(ReassignWorkOrderToDriverInputDTO command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;
            int? oldDriverId = null;
            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                workOrder = await _context.Workorders.Where(c=>c.Id==command.WorkOrderId)
                              .Include(c=>c.Driver)
                              .ThenInclude(c=>c.CurrentLocation)
                              .FirstOrDefaultAsync();
                if (workOrder != null)
                {

                    oldDriverId = workOrder.DriverId;
                    workOrder.DriverId = command.DriverId;
                    //var workOrderStatus =_context.WorkOrderStatus.Where(x => x.Name == "Assigned").FirstOrDefault();
                    workOrder.WorkOrderStatusId = (int)WorkorderStatuses.Dispatched;
                    if (workOrder.Driver.CurrentLocation != null)
                    {
                        workOrder.Driver.CurrentLocation.Latitude = command.Latitude;
                        workOrder.Driver.CurrentLocation.Longitude = command.Longitude;
                    }
                    else
                    {
                        workOrder.Driver.CurrentLocation = new DriverCurrentLocation
                        {
                            Longitude=command.Longitude,
                            Latitude=command.Latitude,
                            
                        };
                    }
                    _workOrderRepository.Update(workOrder);
                    await _unityOfWork.SaveChangesAsync();
                }
                var newDriverUserId = await _context.Driver
                    .Where(x => x.Id == command.DriverId)
                    .Select(x => x.UserId)
                    .FirstOrDefaultAsync();

                await _pushNotificationService.SendAsync(newDriverUserId, "Post-Hub", "You Have New WorkOrder.", new { NotificationType = NotificationTypeEnum.NewWorkOrder });

                if (oldDriverId != null)
                {
                    var existDriverUserId = await _context.Driver
                                     .Where(x => x.Id == oldDriverId)
                                     .Select(x => x.UserId)
                                     .FirstOrDefaultAsync();
                    await _pushNotificationService.SendAsync(existDriverUserId, "Post-Hub", "Your  WorkOrder Has been Assign to another driver.", new { NotificationType = NotificationTypeEnum.ReassignWorkOrder });

                }

            });
            if (result)
            {
                if (workOrder != null)
                {
                    return workOrder.Id;

                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }

        }
    }
}
