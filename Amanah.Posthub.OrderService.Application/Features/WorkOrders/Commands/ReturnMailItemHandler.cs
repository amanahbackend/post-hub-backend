﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Authentication;
using Microsoft.Extensions.Configuration;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders
{

    public class ReturnMailItemHandler : IRequestHandler<ReturnMailItemDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<MailItem> _mailitemRepository;
        private readonly IRepository<WorkOrder> _workorderRepository;
        //private readonly IRepository<Order> _orderRepository;
        private readonly IConfiguration _configuration;
        private ICurrentUser _currentUser;

        public ReturnMailItemHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<MailItem> mailitemRepository,
            IRepository<WorkOrder> workorderRepository,
            IConfiguration configuration,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _mailitemRepository = mailitemRepository;
            _workorderRepository = workorderRepository;
            _configuration = configuration;
            _currentUser = currentUser;
        }
        public async Task<int> Handle(ReturnMailItemDTO command, CancellationToken cancellationToken)
        {
            //MailItem mailItem = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                //mailItem = _mapper.Map<MailItem>(command);

                var mailItemEF = _context.MailItems
                .IgnoreQueryFilters()
                .Where(c => c.Id == command.Id)
                .Include(a => a.DropOffAddress)
                .Include(a => a.MailItemsStatusLog)
                .FirstOrDefault();

                if (mailItemEF != null)
                {
                    if (mailItemEF.MailItemsStatusLog != null && mailItemEF.MailItemsStatusLog.Count > 0)
                    {
                        mailItemEF.MailItemsStatusLog.Add(
                             new MailItemsStatusLog()
                             {
                                 CreatedBy = _currentUser.Id,
                                 WorkOrderId = mailItemEF.WorkorderId,
                                 CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                 IsDeleted = false,
                                 MailItemStatusId = (int)MailItemStatuses.Returned

                             });
                    }
                    else
                    {
                        mailItemEF.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             CreatedBy = _currentUser.Id,
                             WorkOrderId = mailItemEF.WorkorderId,
                             CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                             IsDeleted = false,
                             MailItemStatusId = (int)MailItemStatuses.Returned
                         }
                         };
                    }


                    mailItemEF.StatusId = (int)MailItemStatuses.Returned; //returned
                    mailItemEF.ReturnedDate = DateTime.Now;

                    if (command.ItemReturnReasonId != null)
                        mailItemEF.ItemReturnReasonId = command.ItemReturnReasonId == 0 ? null : command.ItemReturnReasonId;

                    if (command.OtherItemReturnReason != null)
                        mailItemEF.OtherItemReturnReason = command.OtherItemReturnReason;

                    if (mailItemEF.DropOffAddress != null)
                    {
                        mailItemEF.DropOffAddress.Latitude = command.Latitude;
                        mailItemEF.DropOffAddress.Longitude = command.Longitude;
                    }

                    if (mailItemEF.DropOffAddress == null)
                    {
                        mailItemEF.DropOffAddress = new Service.Domain.Addresses.Entities.Address()
                        {
                            Latitude = command.Latitude,
                            Longitude = command.Longitude
                        };
                    }

                    if (_context.MailItems.Where(x => x.Id != command.Id && !x.IsDeleted && x.WorkorderId == command.WorkOrderId && x.StatusId != (int)MailItemStatuses.Delivered && x.StatusId != (int)MailItemStatuses.Returned).Count() <= 0)
                    {
                        var workOrderEF = _context.Workorders.IgnoreQueryFilters().Where(c => c.Id == command.WorkOrderId).FirstOrDefault();
                        workOrderEF.WorkOrderStatusId = (int)WorkorderStatuses.Closed;
                        workOrderEF.DeliveryDate = DateTime.Now;

                        _workorderRepository.Update(workOrderEF);
                    }

                    _mailitemRepository.Update(mailItemEF);
                    await _unityOfWork.SaveChangesAsync();
                }

            });
            if (result)
            {
                return command.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
