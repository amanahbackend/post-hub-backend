﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.BLL.Enums;
using Amanah.Posthub.Infrastructure.ExternalServices;
using Amanah.Posthub.OrderService.Application.Enums;
using AutoMapper;
using global::Amanah.Posthub.Context;
using global::Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using global::Amanah.Posthub.Service.Domain.Entities;
using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{

    public class UpdateMailItemForMobileHandler : IRequestHandler<UpdateMailItemInputDto, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private ICurrentUser _currentUser;
        private readonly IRepository<MailItem> _mailItemRepository;
        private readonly IRepository<WorkOrder> _workOrderRepository;

        private readonly IFirebasePushNotificationService _pushNotificationService;
        private readonly IConfiguration _configuration;

        public UpdateMailItemForMobileHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            ICurrentUser currentUser,
            IRepository<MailItem> mailItemRepository,
            IRepository<WorkOrder> workOrderRepository,
            IFirebasePushNotificationService pushNotificationService,
             IConfiguration configuration
            )
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _currentUser = currentUser;
            _mailItemRepository = mailItemRepository;
            _workOrderRepository = workOrderRepository;
            _pushNotificationService = pushNotificationService;
            _configuration = configuration;
        }
        public async Task<int> Handle(UpdateMailItemInputDto command, CancellationToken cancellationToken)
        {
            MailItem mailItem = null;
            bool isWorkorderClosed = false;
            int? driverId = null;
            MailItem mailItemEF = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                 mailItemEF = _context.MailItems
                .IgnoreQueryFilters()
                .Where(c => c.Id == command.Id)
                .Include(c => c.ReciverInfo)
                .Include(c => c.MailItemsStatusLog)
                .Include(c => c.PickUpAddress)
                .Include(c => c.DropOffAddress)
                .Include(c => c.ProofOfDeliveryImages)
                .Include(c => c.Workorder)
                .Include(c => c.Order)
                .ThenInclude(c => c.BusinessOrder)
                .ThenInclude(c => c.BusinessCustomer)
                .ThenInclude(c => c.BusinessCustomerContacts)
                .Include(c => c.Order)
                .ThenInclude(c=>c.OrdersStatusLog)
                .FirstOrDefault();

                mailItem = _mapper.Map<MailItem>(command);
                if (mailItemEF != null)
                {


                    if (mailItemEF.MailItemsStatusLog != null && mailItemEF.MailItemsStatusLog.Count > 0)
                    {
                        mailItemEF.MailItemsStatusLog.Add(
                             new MailItemsStatusLog()
                             {
                                 CreatedBy = _currentUser.Id,
                                 CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                 IsDeleted = false,
                                 MailItemStatusId = mailItem.StatusId,
                                 WorkOrderId = mailItemEF.WorkorderId

                             });
                    }
                    else
                    {
                        mailItemEF.MailItemsStatusLog = new List<MailItemsStatusLog>() {
                             new MailItemsStatusLog()
                         {
                             CreatedBy =  _currentUser.Id,
                             CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                             IsDeleted = false,
                             MailItemStatusId = mailItem.StatusId,
                             WorkOrderId = mailItemEF.WorkorderId
                         }
                         };
                    }

                    mailItemEF.MailItemBarCode = mailItem.MailItemBarCode;
                    mailItemEF.DropOffDate = mailItem.DropOffDate;

                    if (mailItemEF.DropOffAddress != null)
                    {
                        mailItemEF.DropOffAddress.Latitude = command.Latittude;
                        mailItemEF.DropOffAddress.Longitude = command.Longitude;
                    }

                    if (mailItemEF.DropOffAddress == null)
                    {
                        mailItemEF.DropOffAddress = new Service.Domain.Addresses.Entities.Address()
                        {
                            Latitude = command.Latittude,
                            Longitude = command.Longitude
                        };
                    }
                    if (command.StatusId == (int)MailItemStatuses.Delivered)
                    {
                        mailItemEF.DeliveredDate = DateTime.Now;
                        //mailItemEF.Order.StatusId = (int)OrderStatuses.Closed;
                    }

                    mailItemEF.Notes = mailItem.Notes;
                    mailItemEF.StatusId = mailItem.StatusId;
                    mailItemEF.SignatureImageURL = command.SignatureImageURL;
                    if (mailItemEF.ReciverInfo != null)
                    {
                        mailItemEF.ReciverInfo.FirstName = command.RecieverName;
                        mailItemEF.ReciverInfo.Mobile = command.RecieverMobile;
                    }
                    if (mailItemEF.ReciverInfo == null)
                    {
                        mailItemEF.ReciverInfo = new Contact
                        {
                            FirstName = command.RecieverName,
                            Mobile = command.RecieverMobile
                        };
                        await _unityOfWork.SaveChangesAsync();
                        mailItemEF.ReciverInfoId=mailItemEF.ReciverInfo.Id;

                    }
                    if (command.ProofOfDeliveryImageURLs != null && command.ProofOfDeliveryImageURLs.Count > 0)
                    {
                        mailItemEF.ProofOfDeliveryImages = command.ProofOfDeliveryImageURLs.Select(x => new AttachmentFile
                        {
                            Path = x
                        }).ToList();
                    }

                    // close workorder with Delivered and Returned items
                    if (_context.MailItems.IgnoreQueryFilters().Where(x => !x.IsDeleted
                    && x.Id != command.Id
                    && x.WorkorderId == command.WorkOrderId
                    && x.StatusId != (int)MailItemStatuses.Delivered
                    && x.StatusId != (int)MailItemStatuses.Returned
                    )
                    .Count() <= 0)
                    {
                        mailItemEF.Workorder.WorkOrderStatusId = (int)WorkorderStatuses.Closed;
                        mailItemEF.Workorder.DeliveryDate = DateTime.Now;
                        isWorkorderClosed = true;
                        driverId = mailItemEF.Workorder.DriverId;
                    }

                    // close order with Delivered items
                    if (_context.MailItems.IgnoreQueryFilters().Where(x => !x.IsDeleted
                    && x.Id != command.Id
                    && x.OrderId == mailItemEF.OrderId
                    && x.StatusId != (int)MailItemStatuses.Delivered
                    //&& x.StatusId != (int)MailItemStatuses.Returned
                    )
                    .Count() <= 0)
                    {
                        // close Order
                        mailItemEF.Order.StatusId = (int)OrderStatuses.Closed;
                        mailItemEF.Order.DeliveryDate = DateTime.Now;

                        // Add new satus to order status log

                        if (mailItemEF.Order.OrdersStatusLog != null && mailItemEF.Order.OrdersStatusLog.Count > 0)
                        {
                            mailItemEF.Order.OrdersStatusLog.Add(
                                 new OrdersStatusLog()
                                 {
                                     OrderId = mailItemEF.OrderId,
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                     IsDeleted = false,
                                     OrderStatusId = (int)OrderStatuses.Closed,
                                     LastModificationTime = DateTime.UtcNow,
                                     DeletionTime = DateTime.UnixEpoch,
                                     Note = "",
                                     DeletedBy = "",
                                     LastModifiedBy = ""
                                 });
                        }
                        else
                        {
                            mailItemEF.Order.OrdersStatusLog = new List<OrdersStatusLog>() {
                             new OrdersStatusLog()
                         {
                                     OrderId = mailItemEF.OrderId,
                                     CreatedBy = _currentUser.Id,
                                     CreationTime = DateTime.UtcNow.AddHours(Convert.ToInt32(_configuration.GetValue<string>("UTCHour"))),
                                     IsDeleted = false,
                                     OrderStatusId = (int)OrderStatuses.Closed,
                                     LastModificationTime = DateTime.UtcNow,
                                     DeletionTime = DateTime.UnixEpoch,
                                     Note = "",
                                     DeletedBy = "",
                                     LastModifiedBy = ""   
                         }
                         };
                        }

                        // send closed order notification to (Order by) User
                        var customerUserId = mailItemEF.Order.BusinessOrder.BusinessCustomer.BusinessCustomerContacts
                        .Where(c => c.BusinessCustomerId == mailItemEF.Order.BusinessOrder.BusinessCustomerId).FirstOrDefault().UserID;
                        await _pushNotificationService.SendAsync(customerUserId, "Post-Hub", "Your order has been closed.", new { NotificationType = NotificationTypeEnum.CloseOrder });

                    }
                    _mailItemRepository.Update(mailItemEF);
                    await _unityOfWork.SaveChangesAsync();
                }


              


            });
            if (result)
            {
                if (mailItemEF != null)
                    return mailItemEF.Id;
                else
                    return 0;
            }
            else
            {
                return 0;
            }

        }
    }
}
