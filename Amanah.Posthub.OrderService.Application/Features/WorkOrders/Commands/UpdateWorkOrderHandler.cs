﻿namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{
    using AutoMapper;
    using global::Amanah.Posthub.Context;
    using global::Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
    using global::Amanah.Posthub.Service.Domain.Entities;
    using global::Amanah.Posthub.SharedKernel.Domain.Repositories;
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
    {
        public class UpdateContractHandler : IRequestHandler<UpdateWorkOrderDTO, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly IUnitOfWork _unityOfWork;
            private readonly IRepository<WorkOrder> _workOrderRepository;


            public UpdateContractHandler(
                ApplicationDbContext context,
                IMapper mapper,
                IUnitOfWork unityOfWork,
                IRepository<WorkOrder> workOrderRepository)
            {
                _context = context;
                _mapper = mapper;
                _unityOfWork = unityOfWork;
                _workOrderRepository = workOrderRepository;
            }
            public async Task<int> Handle(UpdateWorkOrderDTO command, CancellationToken cancellationToken)
            {
                WorkOrder workOrder = null;
                _unityOfWork.IsTenantFilterEnabled = false;

                bool result = await _unityOfWork.RunTransaction(async () =>
                {
                    workOrder = _mapper.Map<WorkOrder>(command);
                    _workOrderRepository.Update(workOrder);
                    await _unityOfWork.SaveChangesAsync();
                    ///Add Mail Items 
                    if (command.MailItemIds != null)
                    {
                        var mailItems = await _context.MailItems
                           .Where(x => command.MailItemIds.Contains(x.Id))
                           .ToListAsync();

                        foreach (MailItem item in mailItems)
                        {
                            item.WorkorderId = workOrder.Id;
                            _context.MailItems.Update(item);
                        }
                        await _unityOfWork.SaveChangesAsync();
                    }

                });
                if (result)
                {
                    return workOrder.Id;
                }
                else
                {
                    return 0;
                }

            }
        }
    }

}
