﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Commands
{


    public class UpdateWorkOrderStatusHandler : IRequestHandler<UpdateWorkOrderStatusInputDTO, int>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unityOfWork;
        private readonly IRepository<WorkOrder> _workOrderRepository;


        public UpdateWorkOrderStatusHandler(
            ApplicationDbContext context,
            IMapper mapper,
            IUnitOfWork unityOfWork,
            IRepository<WorkOrder> workOrderRepository)
        {
            _context = context;
            _mapper = mapper;
            _unityOfWork = unityOfWork;
            _workOrderRepository = workOrderRepository;
        }
        public async Task<int> Handle(UpdateWorkOrderStatusInputDTO command, CancellationToken cancellationToken)
        {
            WorkOrder workOrder = null;
            _unityOfWork.IsTenantFilterEnabled = false;

            bool result = await _unityOfWork.RunTransaction(async () =>
            {
                workOrder = _mapper.Map<WorkOrder>(command);
                var workOrderEF = _context.Workorders.IgnoreQueryFilters().Where(c => c.Id == command.Id).FirstOrDefault();
                if (workOrderEF != null)
                {
                    workOrderEF.WorkOrderStatusId = workOrder.WorkOrderStatusId;
                }
                _workOrderRepository.Update(workOrderEF);
                await _unityOfWork.SaveChangesAsync();
            });
            if (result)
            {
                return workOrder.Id;
            }
            else
            {
                return 0;
            }

        }
    }
}
