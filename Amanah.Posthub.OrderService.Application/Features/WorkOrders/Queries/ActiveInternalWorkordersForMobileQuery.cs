﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{
    public class ActiveInternalWorkordersForMobileQuery : IRequest<Result>
    {
        public class AssignedInternalWorkordersForMobileQueryHandler : IRequestHandler<ActiveInternalWorkordersForMobileQuery, Result>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly ICurrentUser _currentUser;

            public AssignedInternalWorkordersForMobileQueryHandler(
                ApplicationDbContext context,
                IMapper mapper,
                ICurrentUser currentUser
                )
            {
                _context = context;
                _mapper = mapper;
                _currentUser = currentUser;

            }

            public async Task<Result> Handle(ActiveInternalWorkordersForMobileQuery request, CancellationToken cancellationToken)
            {
                var result = new Result();

                var workorders = await _context.Workorders
                    .Include(w => w.Driver)
                    .Include(w => w.MailItems)
                    .ThenInclude(m => m.Order)
                    .ThenInclude(o => o.SenderBranch)
                    .Include(w => w.MailItems)
                    .ThenInclude(m => m.ConsigneeInfo)
                    .Include(w => w.WorkOrderStatus)
                    .Where(w => !w.IsDeleted && w.Driver.UserId == _currentUser.Id && w.WorkOrderStatusId == (int)WorkorderStatuses.Active)
                    .ProjectTo<InternalWorkorderListForMobileDto>(_mapper.ConfigurationProvider)
                    .ToListAsync();

                if (workorders == null || workorders.Count <= 0)
                    result.Error = "Ther is no workorders";
                else
                {
                    result.TotalCount = workorders.Count;
                    result.Value = workorders;
                }


                return result;
            }
        }

    }
}
