﻿using System;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs
{
    public class ClosedWorkOrderForMobileResponseDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        ///Work Order Info 
        public string ColorIndicator { get; set; }
        //public int ItemsNo { get; set; }
        ///Status
        public int WorkOrderStatusId { get; set; }
        public string WorkOrderStatusName { get; set; }
        ////Dates 
        public DateTime IssueAt { get; set; }
        public DateTime StartDate { get; set; }
        //#TODO: Not Exist on the Order 
        public DateTime DueDate { get; set; }

        public int ItemsNo { get; set; }
        public int ReturnedNo { get; set; }
        public int RemainingNo { get; set; }
        public int ItemsInformationNo { get; set; }

        public DateTime CloseDate { get; set; }

    }

}
