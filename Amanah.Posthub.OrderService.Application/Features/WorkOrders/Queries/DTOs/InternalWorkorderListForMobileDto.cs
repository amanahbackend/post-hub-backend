﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs
{
    public class InternalWorkorderListForMobileDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string SenderName { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public int WorkOrderStatusId { get; set; }
        public string WorkOrderStatusName { get; set; }
        public bool IsInternal { get; set; }
        public DateTime IssueAt { get; internal set; }
        public DateTime StartDate { get; internal set; }
        public DateTime DueDate { get; internal set; }
        public int ItemsNo { get; internal set; }
        public int RemainingNo { get; internal set; }
        public int ReturnedNo { get; internal set; }
        public int ItemsInformationNo { get; internal set; }
    }
}
