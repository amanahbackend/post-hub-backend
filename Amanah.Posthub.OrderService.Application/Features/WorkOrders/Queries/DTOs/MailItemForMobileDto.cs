﻿using Amanah.Posthub.Service.Domain.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs
{
    public class MailItemForMobileDto
    {
        public int Id { get; set; }
        public string MailItemBarCode { get; set; }
        public string MailItemBarCodeImgURL { get; set; }
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }

        public string MobileNo { get; set; }
        public string Itemstatus { get; set; }
        public int? ItemstatusId { get; set; }
        public int? WorkOrderId { get; set; }
        public int WorkOrderStatusId { get; set; }
        public string WorkOrderStatus { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? ReadyAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public string ContactName { get; set; }
        public AttachmentFile ProofOfDeliveryImage { get; set; }
        public AttachmentFile SignatureImage { get; set; }
        public int? ItemReturnReasonId { get; set; }
        public string ItemReturnReason { get; set; }
        public string OtherItemReturnReason { get; set; }
        public string Notes { get; set; }
        public string SignatureImageUrl { get; set; }
        public List<string> ProofOfDeliveryImageUrl { get; set; }
        public int? DriverId { get; set; }        
        public string ReceiverName { get; set; }
        public string ReceiverMobileNo { get; set; }
        public List<AttachmentFile> Attachments { get; set; }
        public int? OrderId { get; set; }

        public int? ServiceSectorId { get; set; }

    }
}
