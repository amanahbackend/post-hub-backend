﻿using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.Service.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.OrderService.Application.Features.Orders.Queries.DTOs
{
    public class WorkOrderDetailsResponseDTO
    {
        public int Id { get; set; }

        ///Work Order Info 
        public string SectorTypeName { get; set; }
        public string WOrderNo { get; set; }
        public int ItemsNo { get; set; }

        ////Dates 
        public DateTime IssueAt { get; set; }
        public DateTime DeliveryBefore { get; set; }
        //#TODO: Not Exist on the Order 
        public DateTime PickupDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime AssignedDate { get; set; }


        ///Status
        public int StatusId { get; set; }
        public string StatusName { get; set; }


        ///Areas 
        ///
        public string AreaRefCode { get; set; }
        public string Areas { get; set; }


        //Assigned To
        public string AssignedToName { get; set; }
        public string AssignedToCode { get; set; }
        public string AssignedToMobile { get; set; }
        public string AssignedToStatus { get; set; }

        public string Code { get; set; }
        public WorkOrderPriorty Priorty { set; get; }
        public DateTime? PlannedStart { get; set; }
        public int WorkOrderStatusId { get; set; }
        public DateTime? LastStatusDate { get; set; }
        public string AreasCovered { get; set; }
        public List<MailItemForDispatch> MailItems { set; get; }




        ////Dispatched To
        //public string DispatchedToName { get; set; }
        //public string DispatchedToCode { get; set; }
        //public string DispatchedDate { get; set; }
        //public string DispatchedToMobile { get; set; }
        //public string DispatchedToStatus { get; set; }

    }
}
