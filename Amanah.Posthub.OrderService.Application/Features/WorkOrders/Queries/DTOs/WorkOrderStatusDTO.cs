﻿namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs
{
    public class WorkOrderStatusDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
