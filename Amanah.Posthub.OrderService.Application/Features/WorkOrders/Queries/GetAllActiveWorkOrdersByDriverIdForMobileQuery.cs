﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{

    public class GetAllActiveWorkOrdersByDriverIdForMobileQuery : PaginatedItemsViewModel, IRequest<PagedResult<WorkOrderForMobileResponseDTO>>
    {
        public int DriverId { set; get; }
    }
    public class GetAllActiveWorkOrdersByDriverIdForMobileQueryHandler : IRequestHandler<GetAllActiveWorkOrdersByDriverIdForMobileQuery, PagedResult<WorkOrderForMobileResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllActiveWorkOrdersByDriverIdForMobileQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<WorkOrderForMobileResponseDTO>> Handle(GetAllActiveWorkOrdersByDriverIdForMobileQuery query, CancellationToken cancellationToken)
        {
            var workOrdersList = await _context
                 .Workorders
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.DriverId == query.DriverId && !x.IsDeleted && x.WorkOrderStatusId == (int)WorkOrderStatuses.Active)
                 .ProjectTo<WorkOrderForMobileResponseDTO>(_mapper.ConfigurationProvider).ToPagedResultAsync(query);


            return workOrdersList;
        }
    }
}
