﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{

    public class GetAllAssignedWorkOrdersByDriverIdForMobileQuery : PaginatedItemsViewModel, IRequest<PagedResult<WorkOrderForMobileResponseDTO>>
    {
        public int DriverId { set; get; }
    }
    public class GetAllAssignedWorkOrdersByDriverIdForMobileQueryHandler : IRequestHandler<GetAllAssignedWorkOrdersByDriverIdForMobileQuery, PagedResult<WorkOrderForMobileResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllAssignedWorkOrdersByDriverIdForMobileQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<WorkOrderForMobileResponseDTO>> Handle(GetAllAssignedWorkOrdersByDriverIdForMobileQuery query, CancellationToken cancellationToken)
        {
            var workOrdersList = await _context
                 .Workorders
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.DriverId == query.DriverId && !x.IsDeleted && x.WorkOrderStatusId == (int)WorkorderStatuses.Dispatched)
                 .ProjectTo<WorkOrderForMobileResponseDTO>(_mapper.ConfigurationProvider)
                .ToPagedResultAsync(query);

            return workOrdersList;
        }
    }
}
