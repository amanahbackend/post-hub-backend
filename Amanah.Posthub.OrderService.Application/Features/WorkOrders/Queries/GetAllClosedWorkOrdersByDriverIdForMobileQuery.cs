﻿using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Enums;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{

    public class GetAllClosedWorkOrdersByDriverIdForMobileQuery : PaginatedItemsViewModel, IRequest<PagedResult<ClosedWorkOrderForMobileResponseDTO>>
    {
        public int DriverId { set; get; }
        public DateTime? fromDate { set; get; }
        public DateTime? toDate { set; get; }
    }
    public class GetAllClosedWorkOrdersByDriverIdForMobileQueryHandler : IRequestHandler<GetAllClosedWorkOrdersByDriverIdForMobileQuery, PagedResult<ClosedWorkOrderForMobileResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllClosedWorkOrdersByDriverIdForMobileQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<PagedResult<ClosedWorkOrderForMobileResponseDTO>> Handle(GetAllClosedWorkOrdersByDriverIdForMobileQuery query, CancellationToken cancellationToken)
        {
            var workOrdersList = await _context
                 .Workorders
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.DriverId == query.DriverId
                 && !x.IsDeleted
                 && x.WorkOrderStatusId == (int)WorkorderStatuses.Closed
                 && (query.fromDate.HasValue && x.DeliveryDate.Value >= query.fromDate.Value)
                 && (query.toDate.HasValue && x.DeliveryDate.Value <= query.toDate.Value)
                 )
                 .OrderByDescending(w => w.DeliveryDate)
                 .ProjectTo<ClosedWorkOrderForMobileResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(query);

            return workOrdersList;
        }
    }
}
