﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{

    public class GetAllMailItemStatusForMobileQuery : IRequest<List<MailItemStatusDTO>>
    {

        public class GetAllMailItemStatusForMobileQueryHandler : IRequestHandler<GetAllMailItemStatusForMobileQuery, List<MailItemStatusDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetAllMailItemStatusForMobileQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<List<MailItemStatusDTO>> Handle(GetAllMailItemStatusForMobileQuery query, CancellationToken cancellationToken)
            {
                var items = await _context
                     .MailItemStatuss
                     .AsQueryable()
                     .AsNoTracking()
                     .IgnoreQueryFilters()
                     .Where(c => !c.IsDeleted && c.IsActive == true)
                     .ProjectTo<MailItemStatusDTO>(_mapper.ConfigurationProvider).ToListAsync();

                return items;
            }
        }
    }
}
