﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{
    public class GetAllWorkOrderForExportQuery : IRequest<List<WorkOrderExportResponseDTO>>
    {
        public DateTime? CreationDateFrom { set; get; }
        public DateTime? CreationDateTo { set; get; }
        public DateTime? DeliveryBeforeFrom { set; get; }
        public DateTime? DeliveryBeforeTo { set; get; }
        public List<int> ServiceSectorIds { set; get; }
        public List<int> OrderTypeIds { set; get; }
        public List<int> OrderStatusIds { set; get; }
        public List<string> Governorates { set; get; }
        public List<string> Areas { set; get; }
    }
    public class GetAllWorkOrderForExportQueryHandler : IRequestHandler<GetAllWorkOrderForExportQuery, List<WorkOrderExportResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetAllWorkOrderForExportQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<List<WorkOrderExportResponseDTO>> Handle(GetAllWorkOrderForExportQuery query, CancellationToken cancellationToken)
        {

            var ordersList = await _context
                 .Workorders                
                 .IgnoreQueryFilters()
                 .AsQueryable()
                 .IgnoreQueryFilters()
                 .Where(x=>x.IsDeleted !=true )
                 .AsNoTracking()
                 .ProjectTo<WorkOrderExportResponseDTO>(_mapper.ConfigurationProvider)
                 .ToListAsync();

            return ordersList;
        }
    }





}
