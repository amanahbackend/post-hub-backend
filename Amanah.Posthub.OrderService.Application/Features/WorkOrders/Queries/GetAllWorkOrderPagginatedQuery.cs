﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Context;
using Amanah.Posthub.DATA.Helpers;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.eShopOnContainers.Services.Catalog.API.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Utilities.Utilites.Paging;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{
    public class GetAllWorkOrdersPagginatedQuery : PaginatedItemsViewModel, IRequest<PagedResult<WorkOrderListResponseDTO>>
    {
        public DateTime? CreationDateFrom { set; get; }
        public DateTime? CreationDateTo { set; get; }
        public DateTime? DeliveryBeforeFrom { set; get; }
        public DateTime? DeliveryBeforeTo { set; get; }
        public List<int> ServiceSectorIds { set; get; }
        public List<int> OrderTypeIds { set; get; }
        public List<int> OrderStatusIds { set; get; }
        public List<string> Governorates { set; get; }
        public List<string> Areas { set; get; }
    }
    public class GetAllWorkOrderPagginatedQueryHandler : IRequestHandler<GetAllWorkOrdersPagginatedQuery, PagedResult<WorkOrderListResponseDTO>>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICurrentUser _currentUser;

        public GetAllWorkOrderPagginatedQueryHandler(
            ApplicationDbContext context,
            IMapper mapper,
            ICurrentUser currentUser
            )
        {
            _context = context;
            _mapper = mapper;
            _currentUser = currentUser;

        }
        public async Task<PagedResult<WorkOrderListResponseDTO>> Handle(GetAllWorkOrdersPagginatedQuery query, CancellationToken cancellationToken)
        {

            var ordersList = await _context
                 .Workorders
                 .IgnoreQueryFilters()
                 .Where(x=>x.IsDeleted !=true)
                 .Where(x => (string.IsNullOrEmpty(query.SearchBy)
                 ||
                 x.Code.Contains(query.SearchBy)
                 )
                 )
                 .OrderByDescending(wo => wo.Id)  
                 .AsQueryable()
                 .AsNoTracking()
                 .ProjectTo<WorkOrderListResponseDTO>(_mapper.ConfigurationProvider)
                 .ToPagedResultAsync(query);

            return ordersList;
        }
    }





}
