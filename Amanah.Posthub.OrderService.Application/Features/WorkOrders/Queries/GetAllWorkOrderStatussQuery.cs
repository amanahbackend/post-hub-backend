﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{

    public class GetAllWorkOrderStatussQuery : IRequest<List<WorkOrderStatusDTO>>
    {

        public class GetAllMailItemStatusForMobileQueryHandler : IRequestHandler<GetAllWorkOrderStatussQuery, List<WorkOrderStatusDTO>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetAllMailItemStatusForMobileQueryHandler(
                ApplicationDbContext context,
                IMapper mapper
                )
            {
                _context = context;
                _mapper = mapper;

            }
            public async Task<List<WorkOrderStatusDTO>> Handle(GetAllWorkOrderStatussQuery query, CancellationToken cancellationToken)
            {
                var items = await _context
                     .WorkOrderStatus
                     .AsQueryable()
                     .AsNoTracking()
                     .IgnoreQueryFilters()
                     .Where(c => !c.IsDeleted && c.IsActive == true)
                     .ProjectTo<WorkOrderStatusDTO>(_mapper.ConfigurationProvider).ToListAsync();

                return items;
            }
        }
    }
}
