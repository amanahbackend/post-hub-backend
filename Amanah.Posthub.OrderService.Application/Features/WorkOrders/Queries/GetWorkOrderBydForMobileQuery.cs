﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{

    public class GetWorkOrderBydForMobileQuery : IRequest<WorkOrderForMobileResponseDTO>
    {
        public int Id { set; get; }
    }
    public class GetWorkOrderBydForMobileQueryHandler : IRequestHandler<GetWorkOrderBydForMobileQuery, WorkOrderForMobileResponseDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetWorkOrderBydForMobileQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<WorkOrderForMobileResponseDTO> Handle(GetWorkOrderBydForMobileQuery query, CancellationToken cancellationToken)
        {
            var workOrder = await _context
                 .Workorders
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.Id == query.Id)
                 .ProjectTo<WorkOrderForMobileResponseDTO>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync();

            return workOrder;
        }
    }
}
