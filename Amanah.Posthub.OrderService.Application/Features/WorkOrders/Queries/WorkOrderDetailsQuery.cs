﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.Features.Orders.Queries.DTOs;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{
    public class WorkOrderDetailsQuery : IRequest<WorkOrderDetailsResponseDTO>
    {
        public int Id { set; get; }
    }
    public class GetWorkOrderDetailsQueryHandler : IRequestHandler<WorkOrderDetailsQuery, WorkOrderDetailsResponseDTO>
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetWorkOrderDetailsQueryHandler(
            ApplicationDbContext context,
            IMapper mapper
            )
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<WorkOrderDetailsResponseDTO> Handle(WorkOrderDetailsQuery query, CancellationToken cancellationToken)
        {
            var ordersList = await _context
                 .Workorders
                 .AsQueryable()
                 .AsNoTracking()
                 .IgnoreQueryFilters()
                 .Where(x => x.Id == query.Id)
                 .ProjectTo<WorkOrderDetailsResponseDTO>(_mapper.ConfigurationProvider)
                 .FirstOrDefaultAsync();

            return ordersList;
        }
    }





}
