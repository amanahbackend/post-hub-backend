﻿using Amanah.Posthub.Context;
using Amanah.Posthub.OrderService.Application.DTO;
using Amanah.Posthub.OrderService.Application.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Features.WorkOrders.Queries
{
    public class WorkorderForDispatchQuery : QueryListReq, IRequest<Result>
    {

        public class GetWorkorderForDispatchQueryHandler : IRequestHandler<WorkorderForDispatchQuery, Result>
        {
            private readonly ApplicationDbContext _context;

            public GetWorkorderForDispatchQueryHandler(ApplicationDbContext context)
            {
                _context = context;

            }
            public async Task<Result> Handle(WorkorderForDispatchQuery query, CancellationToken cancellationToken)
            {
                var result = new Result();

                result.TotalCount = await _context.Workorders
                     .IgnoreQueryFilters()
                     .Where(x => !x.IsDeleted && x.WorkOrderStatusId == (int)WorkorderStatuses.New)
                     .Select(w => w.Id)
                     .CountAsync();

                if (result.TotalCount < 1)
                {
                    result.Error = "NotFound";
                    return result;
                }

                result.Value = await _context.Workorders.Where(x => !x.IsDeleted && x.WorkOrderStatusId == (int)WorkorderStatuses.New).OrderByDescending(c => c.Id)
                     .Select(w => new WorkorderForDispatchDto()
                     {
                         Id = w.Id,
                         Code = w.Code,
                         DeliveryBefore = w.DeliveryBefore
                     })
                     .Skip((query.PageNumber - 1) * query.PageSize)
                     .Take(query.PageSize)
                     .ToListAsync();

                return result;
            }
        }

    }



}
