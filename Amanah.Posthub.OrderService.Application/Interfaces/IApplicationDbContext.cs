﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
namespace Amanah.Posthub.OrderService.Application.Interfaces
{
    public interface ApplicationDbContext
    {
        DbSet<Order> Orders { get; set; }
        DbSet<DirectOrder> DirectOrders { get; set; }
        //  DbSet<BusinessOrder> BusinessOrders { get; set; }

        DbSet<OrdersStatusLog> OrdersStatusLogs { get; set; }
        DbSet<OrderStatus> OrderStatuss { get; set; }
        DbSet<OrderType> OrderTypes { get; set; }

        //DbSet<ServiceSector> ServiceSectors { get; set; }
        DbSet<ServiceType> ServiceTypes { get; set; }

        DbSet<MailItem> MailItems { get; set; }
        public DbSet<ItemDeleveryType> ItemDeleveryTypes { get; set; }
        DbSet<MailItemType> MailItemTypes { get; set; }
        DbSet<MailItemStatus> MailItemStatuss { get; set; }
        DbSet<MailItemsStatusLog> MailItemsStatusLogs { get; set; }

        DbSet<PaymentMethod> PaymentMethods { get; set; }
        DbSet<Currency> Currencies { get; set; }


        DbSet<WorkOrder> Workorders { get; set; }

        DbSet<WeightUOM> WeightUOMs { get; set; }
        DbSet<AttachmentFile> AttachmentFiles { get; set; }

        DbSet<Contact> Contacts { get; set; }
        //DbSet<Customer> Customers { get; set; }

        //DbSet<Address> Addresses { get; set; }

        //// Lookups

        // DbSet<AccountStatus> AccountStatus { get; set; }
        DbSet<AddressTag> AddressTags { get; set; }
        DbSet<CollectionMethod> CollectionMethods { get; set; }
        DbSet<ContactFunction> ContactFunctions { get; set; }
        DbSet<ContractStatus> ContractStatus { get; set; }
        //DbSet<Country> Countries { get; set; }
        DbSet<CreditStatus> CreditStatus { get; set; }
        DbSet<Currency> PaymentCurrencies { get; set; }
        //DbSet<DriverDutyStatus> DriverDutyStatus { get; set; }
        //DbSet<Gender> Genders { get; set; }
        DbSet<Language> Languages { get; set; }
        DbSet<LengthUOM> LengthUOMs { get; set; }
        DbSet<OrderRecurency> OrderRecurencies { get; set; }
        //DbSet<Role> Roles { get; set; }
        //DbSet<ServiceSector> ServicSectors { get; set; }
        DbSet<ShipServiceType> ShipServiceTypes { get; set; }
        //DbSet<UserAccessType> UserAccessTypes { get; set; }
        //DbSet<UserAccountStatus> UserAccountStatus { get; set; }
        DbSet<WorkOrderStatus> WorkOrderStatus { get; set; }
        //DbSet<Industry> Industries { get; set; }
        //DbSet<State> States { get; set; }
        DbSet<Area> Areas { get; set; }
        DbSet<Courier> Couriers { get; set; }
        DbSet<CourierZone> CourierZones { get; set; }
        DbSet<ItemReturnReason> ItemReturnReasons { get; set; }
        DbSet<CourierShipmentService> CourierShipmentServices { get; set; }

        Task<int> SaveChangesAsync();
    }
}
