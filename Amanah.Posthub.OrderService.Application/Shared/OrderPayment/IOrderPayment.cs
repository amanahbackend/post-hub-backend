﻿using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Shared.OrderPayment
{
    public interface IOrderPayment
    {
        Task<CreateBusinessOrderWithPaymentOutPut> PayOrder(int orderId, decimal orderTotalPrice, string payMethode);
        string GetPaymentGateway(int paymentMethodId);
    }
}