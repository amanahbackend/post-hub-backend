﻿using Amanah.PaymentIntegrations.DTOs;
using Amanah.PaymentIntegrations.DTOs.Enums;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Amanah.PaymentIntegrations.Services;
using Amanah.Posthub.OrderService.Application.Features.BusinessOrders.Commands;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Shared.OrderPayment
{
    public class OrderPayment : IOrderPayment
    {
        private readonly IPaymentService _paymentService;
        private readonly IConfiguration _configuration;
        public OrderPayment(IConfiguration configuration, IPaymentService paymentService)
        {
            _configuration = configuration;
            _paymentService = paymentService;
        }

        public string GetPaymentGateway(int paymentMethodId)
        {
            return paymentMethodId switch
            {
                1 => "knet",
                2 => "mastercard",
                3 => "visa",
                4 => "cash",
                _ => throw new Exception("invalid card"),
            };
        }

        public async Task<CreateBusinessOrderWithPaymentOutPut> PayOrder(int orderId, decimal orderTotalPrice, string payMethode)
        {
            if (payMethode == "cash")
                return await CashPayment(orderId, orderTotalPrice);
            else
                return await OnlinePayment(orderId, orderTotalPrice, payMethode);
        }

        private async Task<CreateBusinessOrderWithPaymentOutPut> CashPayment(int orderId, decimal orderTotalPrice)
        {
            await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
            {
                ReferenceId = orderId.ToString(),
                TotalPrice = (double)orderTotalPrice,
                PaymentGateway = "cash"
            });

            return new CreateBusinessOrderWithPaymentOutPut { BusinessOrderId = orderId, PaymentURL = "" };
        }

        private async Task<CreateBusinessOrderWithPaymentOutPut> OnlinePayment(int orderId, decimal orderTotalPrice, string payMethode)
        {
            var successUrl = _configuration.GetSection("UpPaymentsConfiguration")["SuccessURL"];
            var failUrl = _configuration.GetSection("UpPaymentsConfiguration")["FailURL"];
            var NotifyUrl = _configuration.GetSection("UpPaymentsConfiguration")["NotifyURL"];
            var paymentMode = _configuration.GetSection("UpPaymentsConfiguration")["PaymentMode"];
            var response = await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
            {
                ReferenceId = orderId.ToString(),
                TotalPrice = (double)orderTotalPrice,
                PaymentGateway = payMethode == "knet" ? "knet" : "cc",
                CurrencyCode = "KWD",
                PaymentMode = paymentMode == "1" ? PaymentMode.TestingMode : PaymentMode.ProductionMode,
                Whitelabled = WhiteLabel.On, // without direct url ... the customer will need to choose his prefered way  
                UpPaymentConfiguration = new UpPaymentConfigurationDTO
                {
                    SuccessURL = successUrl,
                    //"https://amanah-paymentintegrations-test.conveyor.cloud/api/Payments/Success",
                    FailURL = failUrl,
                    NotifyURL = NotifyUrl,
                },
                Product = new RequestPaymentProductRequestDTO
                {
                    Title = "OrderPackagesPayment",
                    Names = new string[1] { orderId.ToString() },
                    Prices = new double[1] { (double)orderTotalPrice },
                    Quantities = new string[1] { "1" },
                }

            });

            return new CreateBusinessOrderWithPaymentOutPut { BusinessOrderId = orderId, PaymentURL = response.Data.PaymentURL };
        }
    }
}
