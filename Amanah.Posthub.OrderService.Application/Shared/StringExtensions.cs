﻿using Amanah.Posthub.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
//using System.Data.SqlClient;

namespace Amanah.Posthub.OrderService.Application.Shared
{
    public interface IExecuteStoredProc
    {
        List<T> ExecuteStoredByName<T>(string storedName, params object[] parameters) where T : new();
        T ExecuteStoredByNameEntity<T>(string storedName, params object[] parameters) where T : new();
        int ExecuteStoredByNameToGetCount(string storedName, params object[] parameters);
        bool ExecuteStoredByNameToInsert(string storedName, params object[] parameters);
    }
    public class ExecuteStoredProc : IExecuteStoredProc
    {
        private readonly ApplicationDbContext _context;

        public ExecuteStoredProc(
            ApplicationDbContext context
            )
        {
            _context = context;

        }
        public List<T> ExecuteStoredByName<T>(string storedName, params object[] parameters) where T : new()
        {
            // using (ApplicationDbContext context = new ApplicationDbContext())
            // {
            _context.Database.OpenConnection();

            DbCommand cmd = _context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = storedName;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            if (parameters.Length > 0)
                cmd.Parameters.AddRange(parameters);

            List<T> resultlist;
            using (var reader = cmd.ExecuteReader())
            {
                resultlist = reader.MapTolist<T>();
            }
            _context.Database.CloseConnection();

            return resultlist;

            // }
        }
        public T ExecuteStoredByNameEntity<T>(string storedName, params object[] parameters) where T : new()
        {
            //  using (ApplicationDbContext context = new ApplicationDbContext())
            //  {
            _context.Database.OpenConnection();

            DbCommand cmd = _context.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = storedName;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            if (parameters.Length > 0)
                cmd.Parameters.AddRange(parameters);

            T newObject;
            using (var reader = cmd.ExecuteReader())
            {
                newObject = reader.MapToEntity<T>();
            }
            _context.Database.CloseConnection();

            return newObject;

            // }
        }
        public int ExecuteStoredByNameToGetCount(string storedName, params object[] parameters)
        {
            try
            {
                // using (ApplicationDbContext context = new ApplicationDbContext())
                //  {
                _context.Database.OpenConnection();

                DbCommand cmd = _context.Database.GetDbConnection().CreateCommand();
                cmd.CommandText = storedName;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (parameters.Length > 0)
                    cmd.Parameters.AddRange(parameters);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader != null && reader.HasRows)
                    {
                        return reader.VisibleFieldCount;
                    }
                }
                _context.Database.CloseConnection();

                return 0;

                // }
            }
            catch (Exception ex)
            {
                _context.Database.CloseConnection();

                return 0;

            }
        }


        public bool ExecuteStoredByNameToInsert(string storedName, params object[] parameters)
        {
            try
            {
                // using (ApplicationDbContext context = new ApplicationDbContext())
                // {
                _context.Database.OpenConnection();

                DbCommand cmd = _context.Database.GetDbConnection().CreateCommand();
                cmd.CommandText = storedName;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (parameters.Length > 0)
                    cmd.Parameters.AddRange(parameters);
                var result = Convert.ToInt32(cmd.ExecuteScalar());
                _context.Database.CloseConnection();

                return true;
                //  }
            }
            catch (Exception ex)
            {
                _context.Database.CloseConnection();

                return false;

            }
        }

    }
}
