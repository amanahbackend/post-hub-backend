﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.OrderService.Application.Shared.ValidationsHelper
{
    public interface IValidationsUploadFiles
    {
        void ValidateFileSize(IFormFile file);

        void ValidateFileSize(string base64);

        void ValidateAllFilesSize(IEnumerable<IFormFile> files, int maxCountOfFiles);

        void ValidateAllFilesSize(IEnumerable<string> files, int maxCountOfFiles);



    }
}
