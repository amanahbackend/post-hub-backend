﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.OrderService.Application.Shared.ValidationsHelper
{
    public class ValidationsUploadFiles : IValidationsUploadFiles
    {
        private readonly ILocalizer _localizer;

        public ValidationsUploadFiles(ILocalizer localizer)
        {
            _localizer = localizer;
        }
        public void ValidateFileSize(IFormFile file)
        {
            if (file.Length > 1_024_000)
                throw new DomainException(_localizer[Keys.Messages.MaxFileSize]);
        }

        public void ValidateFileSize(string base64)
        {
            if (base64.Length > 1_024_000)
                throw new DomainException(_localizer[Keys.Messages.MaxFileSize]);
        }

        public void ValidateAllFilesSize(IEnumerable<IFormFile> files, int maxCountOfFiles)
        {
            if (files.Count() > maxCountOfFiles)
                throw new DomainException(_localizer.Format(Keys.Messages.MaxCountOfFiles, maxCountOfFiles));

            foreach (var file in files)
                this.ValidateFileSize(file);
        }

        public void ValidateAllFilesSize(IEnumerable<string> files, int maxCountOfFiles)
        {
            if(files.Count() > maxCountOfFiles)
                throw new DomainException(_localizer.Format(Keys.Messages.MaxCountOfFiles,maxCountOfFiles));

            foreach (var file in files)
                this.ValidateFileSize(file);
        }

    }
}
