﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.ActionLogs.Entities
{
    public class AccountLogs : BaseEntity
    {
        public string ActivityType { get; set; }
        public string Description { get; set; }
        public string TableName { get; set; }
        public int Record_Id { get; set; }
    }
}
