﻿using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;

namespace Amanah.Posthub.Service.Domain.Addresses.Entities
{
    public class Address : ValueObject
    {
        public string PACINumber { get; set; }
        public string Governorate { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public Address() { }

        public Address(string governorate, string area, string block, string street, string bulding, string floor, string flat)
        {
            Governorate = governorate;
            Area = area;
            Block = block;
            Street = street;
            Building = bulding;
            Floor = floor;
            Flat = flat;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            // Using a yield return statement to return each element one at a time
            if (!string.IsNullOrWhiteSpace(Governorate))
            {
                yield return Governorate;
            }
            yield return Area;
            yield return Block;
            yield return Street;
            yield return Building;
            yield return Floor;
            yield return Flat;
        }

        public override string ToString()
        {
            return GetEqualityComponents().Select(item => (item ?? string.Empty).ToString()).Join(",");
        }

        public Address Clone()
        {
            return MemberwiseClone() as Address;
        }
    }


}
