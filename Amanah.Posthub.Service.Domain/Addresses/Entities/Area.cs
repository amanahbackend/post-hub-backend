﻿using Amanah.Posthub.BASE.Domain.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Addresses.Entities
{
    public class Area : BaseGlobalEntity<int>
    {
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public int? FK_Governrate_Id { get; set; }
        public Governrate Governrate { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public int? Raf { get; set; }
        public List<Block> Blocks { get; set; }


    }
}
