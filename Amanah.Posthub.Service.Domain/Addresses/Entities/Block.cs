﻿using Amanah.Posthub.BASE.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Addresses.Entities
{
    public class Block : BaseGlobalEntity<int>
    {
        public string BlockNo { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public int? AreaId { get; set; }
        [ForeignKey("AreaId")]
        [InverseProperty("Blocks")]
        public Area Area { get; set; }
    }
}
