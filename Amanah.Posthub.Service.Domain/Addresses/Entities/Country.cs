﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Addresses.Entities
{

    public class Country : BaseGlobalEntity<int>
    {
        public string Name { set; get; }
        public string Code { set; get; }
        public string Flag { set; get; }
        public string TopLevel { set; get; }
        public int? CourierZoneId { get; set; }
        public CourierZone CourierZone { get; set; }
        public List<Governrate> Governrates { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        
    }
}
