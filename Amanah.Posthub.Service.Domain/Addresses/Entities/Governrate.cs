﻿using Amanah.Posthub.BASE.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Addresses.Entities
{
    public class Governrate : BaseGlobalEntity<int>
    {
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public int? CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
        public List<Area> Areas { get; set; }
    }
}
