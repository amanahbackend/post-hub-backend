﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using System;

namespace Amanah.Posthub.Service.Domain.Admins.Entities
{
    public class Admin : BaseEntity<string>
    {
        public Admin() { }
        public Admin(string tenantId)
        {
            Tenant_Id = tenantId;
        }

        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int DisplayImage { get; set; }
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; }
        public string DeactivationReason { get; set; }
        public int ResidentCountryId { get; set; }

        public int? BranchId { set; get; }
        public virtual Branch Branch { set; get; }

        public string MobileNumber { get; set; }
        public Address Address { set; get; }
        public int NationalityId { get; set; }
        public Country Nationality { get; set; }
        public string NationalId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ProfilePhotoURL { get; set; }

        // public virtual ApplicationUser User { get; set; }
    }
}
