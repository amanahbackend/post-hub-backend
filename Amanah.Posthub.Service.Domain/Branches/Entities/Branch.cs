﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.CompanyProfile;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.Service.Domain.Resturants.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Branches.Entities
{
    public class Branch : BaseEntity<int>
    {
        public Branch()
        {

        }
        public Branch(string tenantId)
        {
            Tenant_Id = tenantId;
        }

        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public Country Country { get; set; }
        public string Phone { get; set; }
        public int? RestaurantId { get; set; }
        public int? GeoFenceId { get; set; }
        public int? CompanyId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool IsActivePrevious { get; set; }
        public Address Location { set; get; }

        public string BranchManagerId { get; set; }
        public ApplicationUser BranchManager { get; set; }


        public virtual GeoFence GeoFence { get; set; }
        public virtual Restaurant Restaurant { get; set; }
        public string Reason { get; set; }
        public Customer Customer { set; get; }
        public Company Company { set; get; }
        public List<WorkingHours> WorkingHours { get; set; }
        public int FloorsCount { get; set; }

    }

}
