﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.BusinessCustomers
{
    public class BusinessContactAccount : BaseEntity
    {

        ///Business-Account Contacts
        public string UserID { set; get; }
        public ApplicationUser User { get; set; }
        public string ContactCode { set; get; }
        public int? ContactFunctionId { set; get; }
        public ContactFunction ContactFunction { set; get; }

        public int? CourtesyId { set; get; }
        public Courtesy Courtesy { set; get; }

        public int AccountStatusId { get; set; }
        public AccountStatus AccountStatus { get; set; }
        public string PersonalPhotoURL { get; set; }
        public string MobileNumber { get; set; }
        public string Fax { get; set; }


        public int BusinessCustomerId { set; get; }
        [ForeignKey("BusinessCustomerId")]
        public BusinessCustomer BusinessCustomer { set; get; }

        public int BusinessCustomerBranchId { set; get; }
        public int? AccountType { get; set; }// 1 for business 2 for individual


    }
}
