﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.BusinessCustomers
{
    public class BusinessCustomer : BaseEntity
    {

        ///Bussines Info
        public string BusinessName { set; get; }
        public int? BusinessTypeId { set; get; }
        public CompanyProfile.Entities.BusinessType BusinessType { set; get; }
        public string BusinessLogoUrl { set; get; }
        public string BusinessCID { set; get; }
        public string BusinessCode { set; get; }
        public int? CollectionMethodId { set; get; }
        public CollectionMethod CollectionMethod { set; get; }
        public bool IsCompleted { set; get; } = false;
        public List<BusinessOrder> BusinessOrders { get; set; }
        public BusinessCustomerType BusinessCustomerType { set; get; }

        public int? HQBranchId { set; get; }
        [ForeignKey("HQBranchId")]
        public BusinessCustomerBranch HQBranch { set; get; }

        public int? MainContactId { set; get; }
        [ForeignKey("MainContactId")]
        public BusinessContactAccount MainContact { set; get; }
        public List<BusinessContactAccount> BusinessCustomerContacts { set; get; }
        public List<BusinessCustomerBranch> BusinessCustomerBranches { set; get; }
        public List<Contract> BusinessCustomerContracts { set; get; }
        public int? AccountType { get; set; } // 1 for business and 2 for individual
        public bool? IsApproved { get; set; }

    }


    public enum BusinessTypes
    {
        Individual = 13
    }


}
