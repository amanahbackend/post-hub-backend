﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.BusinessCustomers.Entities
{
    public class Courtesy : BaseEntity, ILookupEntity
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
    }
}
