﻿using System;

namespace Amanah.Posthub.Service.Domain.Common
{
    public abstract class BaseEntity<T>
    {
        public T Id { get; set; }

        public DateTime CreationTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastModificationTime { get; set; }
        public string LastModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletionTime { get; set; }
        public string DeletedBy { get; set; }

    }
}
