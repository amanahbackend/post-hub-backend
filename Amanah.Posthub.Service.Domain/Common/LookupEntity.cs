﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Common
{
    public abstract class LookupEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }

    }

}
