﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.SharedKernel.Domain.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.CompanyProfile.Entities
{
    public class BusinessType : BaseEntity, ILookupEntity
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
        public List<BusinessCustomer> BusinessCustomers { get; set; }
    }
}
