﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities
{
    public class CompanySocialLinks : BaseEntity
    {
        public int Id { set; get; }
        public int CompanyId { set; get; }
        public string SiteName { set; get; }
        public string SiteLink { set; get; }
    }
}
