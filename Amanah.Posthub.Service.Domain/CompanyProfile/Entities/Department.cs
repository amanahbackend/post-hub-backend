﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.CompanyProfile.Entities
{
    public class Department : BaseEntity, ILookupEntity
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
        public int FloorNo { get; set; }
        public List<Manager> Managers { set; get; }

    }
}
