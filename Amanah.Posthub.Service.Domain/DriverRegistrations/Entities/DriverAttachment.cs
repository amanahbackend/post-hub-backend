﻿using Amanah.Posthub.BASE.Domain.Entities;


namespace Amanah.Posthub.Service.Domain.DriverRegistrations.Entities
{
    public class DriverAttachment : BaseEntity<int>
    {
        public int DriverRegistrationId { get; set; }
        public DriverAttachmentType Type { get; set; }
        public string File { get; set; }
    }
}
