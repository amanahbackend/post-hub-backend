﻿namespace Amanah.Posthub.Service.Domain.DriverRegistrations.Entities
{
    public enum DriverAttachmentType
    {
        PersonalPhoto = 1,
        NationalId,
        DrivingLicense,
        CarApplication,
        CarInsurance,
        CarReferralDocument,
        CarPlateNumber,
        CarFrontSide,
        CarBackSide,
        CarLeftSide,
        CarRightSide,
        CarInsidePicture,
        TermsAndConditions,
        Auditing,
        Other
    }
}
