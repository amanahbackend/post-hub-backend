﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.DriverRegistrations.Entities
{
    public class JobStatus : BaseEntity
    {
        public string Name { get; set; }
    }
}
