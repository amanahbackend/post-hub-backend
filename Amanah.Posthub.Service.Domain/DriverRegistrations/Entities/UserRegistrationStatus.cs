﻿namespace Amanah.Posthub.Service.Domain.DriverRegistrations.Entities
{
    public enum UserRegistrationStatus
    {
        New = 1,
        Pending,
        Approved,
        Rejected
    }
}
