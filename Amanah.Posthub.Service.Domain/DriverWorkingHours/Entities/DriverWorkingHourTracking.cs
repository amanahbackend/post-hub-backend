﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;

namespace Amanah.Posthub.Service.Domain.DriverWorkingHours.Entities
{
    public class DriverWorkingHourTracking : BaseEntity<int>
    {
        private DriverWorkingHourTracking()
        {
        }

        public DriverWorkingHourTracking(
            int driverId,
            AgentStatusesEnum status,
            string note = null)
        {
            StatusId = (int)status;
            DriverId = driverId;
            Notes = note;
        }

        public int DriverId { get; private set; }
        public DateTime ActivityDateTime { get; set; } = DateTime.UtcNow;
        public int StatusId { get; private set; }
        public string ActionBy { get; set; }
        public string Notes { get; set; }
        public Driver Driver { get; private set; }

        private static readonly AgentStatusesEnum[] _startStauses = new[]
        {
            AgentStatusesEnum.Available
        };
        private static readonly AgentStatusesEnum[] _endStatuses = new[]
        {
            AgentStatusesEnum.Unavailable,
            AgentStatusesEnum.Offline,
            AgentStatusesEnum.Blocked
        };

        public bool IsStart() => _startStauses.Contains((AgentStatusesEnum)StatusId);
        public bool IsEnd() => _endStatuses.Contains((AgentStatusesEnum)StatusId);
        public bool HasEquivalentStatus(DriverWorkingHourTracking other)
        {
            if (other == null)
            {
                return false;
            }
            return IsStart() && other.IsStart() || IsEnd() && other.IsEnd();
        }


        public static DriverWorkingHourTracking CreateLogin(string tenantId, int driverId)
        {
            return new DriverWorkingHourTracking
            {
                Tenant_Id = tenantId,
                DriverId = driverId,
                StatusId = (int)AgentStatusesEnum.Available
            };
        }
        public DriverWorkingHourTracking CreateDayClose()
        {
            return new DriverWorkingHourTracking(DriverId, AgentStatusesEnum.Unavailable)
            {
                ActivityDateTime = ActivityDateTime.EndOfDayOrNowIfToday()
            };
        }
        public DriverWorkingHourTracking CreateDayOpening()
        {
            return new DriverWorkingHourTracking(DriverId, AgentStatusesEnum.Available)
            {
                ActivityDateTime = ActivityDateTime.BeginingOfDay()
            };
        }

        public override string ToString()
        {
            var items = new List<(string name, object value)>
            {
                ("DriverId", DriverId),
                ("ActivityDateTime",  ActivityDateTime),
                ("Status", (AgentStatusesEnum)StatusId)
            };
            if (Driver?.User != null)
            {
                items.Insert(1, ("DriverName", Driver.User.FullName));
            }

            return items.Select(item => $"{item.name}: {item.value}").Join(",");
        }
    }
}
