﻿using Amanah.Posthub.BASE.Domain.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Drivers.Entities
{
    public class AgentType : BaseGlobalEntity
    {
        public string Name { set; get; }
        public List<Driver> Drivers { set; get; }

    }
}