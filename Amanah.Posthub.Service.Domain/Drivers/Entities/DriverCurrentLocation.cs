﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;

namespace Amanah.Posthub.Service.Domain.Drivers.Entities
{
    public class DriverCurrentLocation : BaseEntity
    {
        public DriverCurrentLocation(string tenantId)
        {
            Tenant_Id = tenantId;
        }
        public DriverCurrentLocation()
        {
        }

        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Driver Driver { get; set; }
        public int? BranchId { get; set; }
        public Branch Branch { get; set; }
        //  public virtual ICollection<Driver> Drivers { get; set; }

    }
}