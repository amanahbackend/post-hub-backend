﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;

namespace Amanah.Posthub.Service.Domain.Drivers.Entities
{
    public class DriverExternalBranch : BaseEntity
    {
      
        public int DriverId { get; set; }
        public Driver Driver { get; set; }
        public int? BranchId { get; set; }
        public Branch Branch { get; set; }
    }
}
