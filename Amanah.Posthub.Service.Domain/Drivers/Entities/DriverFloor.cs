﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;

namespace Amanah.Posthub.Service.Domain.Drivers.Entities
{
   
    public class DriverFloor : BaseEntity
    {
        public string Floor { get; set; }
        public int BranchId { get; set; }
        [ForeignKey("BranchId")]
        [Required]
        public Branch Branch { get; set; }
        public int DriverId { get; set; }
        public virtual Driver Driver { get; set; }
    }
}
