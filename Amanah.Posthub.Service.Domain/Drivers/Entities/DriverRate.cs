﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;

namespace Amanah.Posthub.Service.Domain.Drivers.Entities
{
    public class DriverRate : BaseEntity
    {
        public int CustomerId { get; set; }
        public int DriverId { get; set; }
        public int TasksId { get; set; }
        public double Rate { get; set; }
        public string Note { get; set; }

        public virtual Driver Driver { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual SubTask Tasks { get; set; }
    }
}
