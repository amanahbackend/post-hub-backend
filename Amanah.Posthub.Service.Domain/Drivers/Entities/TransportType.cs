﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Drivers.Entities
{
    public class TransportType : BaseGlobalEntity
    {
        public string Name { set; get; }
    }
}
