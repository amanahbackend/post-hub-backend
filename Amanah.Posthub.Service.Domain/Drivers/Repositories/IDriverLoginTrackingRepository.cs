﻿using Amanah.Posthub.Service.Domain.LoginRequests.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Amanah.Posthub.Domain.Drivers.Repositories
{
    public interface IDriverLoginTrackingRepository : IRepository<DriverloginTracking>
    {
        Task UpdateAsync(List<int> driverIds);
        Task UpdateAsync(int driverId, double latitude, double longitude);
    }
}
