﻿using Amanah.Posthub.Service.Domain.Common;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class Account : BaseEntity<int>
    {
        public string Name { set; get; }
        public string Company { set; get; }
    }
}