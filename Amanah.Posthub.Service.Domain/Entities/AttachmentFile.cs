﻿using Amanah.Posthub.Service.Domain.Common;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class AttachmentFile : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public FileType FileType { get; set; }
        public string Note { get; set; }
        public int? BusinessOrderId { get; set; }
        public int? ProofOfDeliveryImageId { get; set; }
        public MailItem MailItemProofOfDeliveryImage { get; set; }
        public int? SignatureImageId { get; set; }
        public MailItem MailItemSignatureImage { get; set; }
    }
}