﻿using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.Service.Domain.Common;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class BusinessOrder : BaseEntity<int>
    {
        public int? BusinessCustomerId { get; set; }
        [ForeignKey("BusinessCustomerId")]
        public BusinessCustomer BusinessCustomer { get; set; }

        public int? BusinessCustomerBranchId { set; get; }
        [ForeignKey("BusinessCustomerBranchId")]
        public BusinessCustomerBranch BusinessCustomerBranch { set; get; }


        public int? OrderById { get; set; }
        [ForeignKey("OrderById")]
        public BusinessContactAccount BusinessContactAccount { get; set; }
        public int? ContactId { get; set; }
        [ForeignKey("ContactId")]
        public Contact Contact { get; set; }
        public int? ContractId { get; set; }
        [ForeignKey("ContractId")]
        public Contract Contract { get; set; }
        public int? MailItemsTypeId { get; set; }
        [ForeignKey("MailItemsTypeId")]
        public MailItemType MailItemsType { get; set; }
        public decimal? PostagePerPiece { get; set; }
        public int? CollectionFormSerialNo { get; set; }
        public List<AttachmentFile> AttachedFiles { get; set; }

        //public int? HQBranchId { set; get; }
        //[ForeignKey("HQBranchId")]
        //public BusinessCustomerBranch HQBranch { set; get; }

        public int? BusinessTypeId { set; get; }
        [ForeignKey("BusinessTypeId")]
        public BusinessType BusinessType { set; get; }


        public decimal? OrderPackagePrice { get; set; }
        public int? PackingPackageId { get; set; }
        [ForeignKey("PackingPackageId")]
        public PackingPackage PackingPackage { get; set; }
        public ICollection<BusinessOrderService> BusinessOrderServices { get; set; }
    }
}