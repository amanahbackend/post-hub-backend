﻿using Amanah.Posthub.BASE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class BusinessOrderService : BaseEntity
    {
        public int? BusinessOrderId { get; set; } 
        public BusinessOrder BusinessOrder { get; set; }
        public int? ServiceId { get; set; }
        public Service Service { get; set; }
        public decimal? Price { get; set; }
    }
}
