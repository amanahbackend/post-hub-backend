﻿
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Common;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class Contact : BaseEntity<int>
    {
        public string FirstName { get; set; }
        public string MidelName { get; set; }
        public string Lastname { get; set; }
        public string Mobile { get; set; }
        public Address Address { get; set; }
        public string Email { get; set; }

        public int? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public string BranchName { get; set; }
        public int? DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }
        public string DepartmentName { get; set; }
        public string FloorNo { get; set; }

    }
}