﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using Amanah.Posthub.Service.Domain.PriceList.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class Contract : BaseEntity
    {
        public ServiceSector ServiceSector { get; set; }
        public int? ServiceSectorId { get; set; }
        public ContractType ContractType { get; set; }
        public int? ContractTypeId { get; set; }
        public DateTime? ContractDate { get; set; }
        public DateTime? ContractStartDate { set; get; }
        public DateTime? ContractEndDate { set; get; }

        /// <summary>
        /// /Customer Info 
        /// </summary>
        /// 
        public int? BusinessCustomerId { get; set; }
        [ForeignKey("BusinessCustomerId")]
        [InverseProperty("BusinessCustomerContracts")]
        public BusinessCustomer BusinessCustomer { get; set; }
        public decimal? NoOfMessages { get; set; }
        public bool IsPrintReciptListAndCollectEnabled { get; set; }
        public bool IsPrintAndFixEnabledEnabled { get; set; }
        public decimal? ItemSupplyPeriod { get; set; }
        public decimal? DistriputionAllowance { get; set; }

        ///Payment Terms 
        public PaymentTypes? PaymentType { set; get; }
        public int? PaymentNotLetterThan { set; get; }

        /// Installments 
        public decimal? InstallmentsAtContracting { set; get; }
        public decimal? InstallmentsAfterEachMessage { set; get; }
        public decimal? InstallmentsPaymentNotLaterThan { set; get; }
        public PeriodType? InstallmentsPaymentNotLaterThanPeriodType { set; get; }


        public PeriodType? PaymentNotLetterThanPeriodType { set; get; }
        public decimal? PaymentNotLetterThanPeriodValue { set; get; }
        public PeriodType? ItemsSupplyAheadOf { set; get; }
        public decimal? ItemsSupplyAheadOfValue { set; get; }
        public PeriodType? ServiceDistributionPeriod { set; get; }
        public decimal? ServiceDistributionPeriodValue { set; get; }
        public int? PaymentTermsMessageNo { get; set; }


        public int? ContractStatusId { get; set; }
        public string Code { get; set; }
        public ContractStatus ContractStatus { get; set; }

        public int? CurrencyId { get; set; }
        [ForeignKey("CurrencyId")]
        [InverseProperty("Contracts")]
        public Currency Currency { get; set; }

        public decimal? PricePerUnit { get; set; }
        public int? NumberOfUnits { get; set; }
        public List<ExtraTerm> ExtraTerms { get; set; }

        public int? RFQId { get; set; }
        [ForeignKey("RFQId")]
        [InverseProperty("RFQContracts")]
        public RFQ RFQ { get; set; }


    }
}