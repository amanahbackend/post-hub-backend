﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Common;
namespace Amanah.Posthub.Service.Domain.Entities
{
    public class DirectOrder : BaseEntity<int>
    {
        public Account Customer { get; set; }
        public Address DropOffAddress { get; set; }
        public Contact SenderInfo { get; set; }
        public Contact ConsigneeInfo { get; set; }
        public bool IsMatchConsigneeID { get; set; }
        public Contact ReciverInfo { get; set; }
    }
}