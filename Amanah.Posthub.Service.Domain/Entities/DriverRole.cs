﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using Amanah.Posthub.SharedKernel.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Entities
{
   public class DriverRole : BaseEntity
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
    
    }
}
