﻿using Amanah.Posthub.BASE.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class ExtraTerm : BaseEntity
    {
        public string Term { get; set; }
        public int? ContractId { get; set; }
        [ForeignKey("ContractId")]
        [InverseProperty("ExtraTerms")]
        public Contract Contract { get; set; }
    }
}
