﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using System;


namespace Amanah.Posthub.Service.Domain.Entities
{
    public class Invoice : BaseEntity
    {
        public string InvoiceNo { get; set; }
        public string InvoiceTopic { get; set; }
        public int ServiceSectorId { get; set; }
        public ServiceSector ServiceSector { get; set; }
        public InvoiceStatus InvoiceStatus { get; set; }
        public DateTime? InvoicePeriodFrom { get; set; }
        public DateTime? InvoicePeriodTo { get; set; }
        public int? BusinessCustomerId { get; set; }
        public BusinessCustomer BusinessCustomer { get; set; }
        public int ContractId { get; set; }
        public Contract Contract { get; set; }
        public int? NoOfItems { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? TotalUnitsPrice { get; set; }
    }
    public enum InvoiceStatus
    {
        New = 1
    }
}
