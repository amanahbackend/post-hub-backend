﻿namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class AccountNotifiChannel
    {
        public int Id { get; set; }
        public string AccountNotification { get; set; }
        public string NotificChannel { get; set; }
    }
}
