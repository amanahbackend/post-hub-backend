﻿using Amanah.Posthub.Service.Domain.Common;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class ContractStatus : LookupEntity
    {
        public string Description { get; set; }
        public List<Contract> Contracts { get; set; }
    }
}
