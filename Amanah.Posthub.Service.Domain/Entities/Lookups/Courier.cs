﻿using Amanah.Posthub.Service.Domain.Common;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class Courier : LookupEntity
    {
        public List<CourierZone> CourierZones { get; set; }
    }
}
