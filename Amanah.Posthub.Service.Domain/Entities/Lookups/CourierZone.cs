﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Common;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class CourierZone : LookupEntity
    {
        public int CourierId { get; set; }
        public Courier Courier { get; set; }
        public List<Country> Countries { get; set; }
    }
}
