﻿using Amanah.Posthub.Service.Domain.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class Currency
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string Code_en { get; set; }
        public string Code_ar { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
        public string Symbol { get; set; }
        public List<Contract> Contracts { get; set; }
    }
}
