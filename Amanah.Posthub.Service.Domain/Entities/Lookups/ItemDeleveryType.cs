﻿using Amanah.Posthub.Service.Domain.Common;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class ItemDeleveryType : LookupEntity
    {
        public List<MailItem> MailItems { get; set; }

    }
}
