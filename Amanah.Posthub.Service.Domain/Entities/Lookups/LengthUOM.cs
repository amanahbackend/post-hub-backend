﻿using Amanah.Posthub.Service.Domain.Common;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class LengthUOM : LookupEntity
    {
        public string Code { get; set; }
    }
}
