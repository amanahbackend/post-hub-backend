﻿using Amanah.Posthub.Service.Domain.Common;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class MailItemStatus : LookupEntity
    {
        public int Rank { get; set; }
        public string Description { get; set; }
        //   public ICollection<MailItemsStatusLog> MailItemsStatusLogs { get; set; }

    }


}
