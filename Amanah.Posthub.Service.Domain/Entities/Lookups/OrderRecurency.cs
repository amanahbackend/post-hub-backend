﻿using Amanah.Posthub.Service.Domain.Common;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class OrderRecurency : LookupEntity
    {
        public string Code { get; set; }
    }
}
