﻿using Amanah.Posthub.Service.Domain.Common;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class OrderStatus : LookupEntity
    {
        public int Rank { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
