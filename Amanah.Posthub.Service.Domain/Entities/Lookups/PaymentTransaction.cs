﻿using Amanah.Posthub.Service.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class PaymentTransaction 
    {
        public long Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string OrderId { get; set; }
        public int TransactionStatusId { get; set; }
        public int PaymentGatewayId { get; set; }
        public string ReferenceId { get; set; }
        public double TotalPrice { get; set; }
        public bool? IsSuccessTransaction { get; set; }
        public string PaymentResponseDetails { get; set; }
        public string PaymentRequestDetails { get; set; }
        public string TransactionResponseDetails { get; set; }
        public DateTime? TransactionResponseDate { get; set; }

    }
}
