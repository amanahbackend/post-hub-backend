﻿namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public enum PaymentTypes
    {
        Cash = 1,
        PerMission = 2,
        Installment = 3,
        Credit = 4
    }

    public enum PeriodType
    {
        Hours = 1,
        Days = 2,
        Months = 3,
        Years = 4
    }
}
