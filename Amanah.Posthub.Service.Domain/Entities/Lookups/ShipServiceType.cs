﻿using Amanah.Posthub.Service.Domain.Common;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class ShipServiceType : LookupEntity
    {
        public string Description { get; set; }
    }
}
