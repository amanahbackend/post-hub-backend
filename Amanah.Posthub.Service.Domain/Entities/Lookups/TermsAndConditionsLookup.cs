﻿using Amanah.Posthub.Service.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class TermsAndConditionsLookup : LookupEntity
    {
        [Required, MaxLength(250)]
        public string Title_En { get; set; }

        [Required, MaxLength(250)]
        public string Title_Ar { get; set; }

        [Required,MaxLength(1000)]
        public string TermsAndConditions_En { get; set; }

        [Required, MaxLength(1000)]
        public string TermsAndConditions_Ar { get; set; }
    }
}
