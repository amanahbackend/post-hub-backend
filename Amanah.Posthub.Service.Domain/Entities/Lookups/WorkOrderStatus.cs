﻿using Amanah.Posthub.Service.Domain.Common;

namespace Amanah.Posthub.Service.Domain.Entities.Lookups
{
    public class WorkOrderStatus : LookupEntity
    {
        public int Rank { get; set; }
    }

    public enum WorkOrderStatuses
    {
        New = 1,
        Assigned = 4,
        Dispatched = 5,
        Active = 7,
        Closed = 10
    }
}
