﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
//using Amanah.Posthub.Service.Domain.Drivers.Entities;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class MailItem : BaseEntity
    {
        public string MailItemBarCode { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public Address PickUpAddress { get; set; }
        public Address DropOffAddress { get; set; }
        public int? Raf { get; set; }
        public int? ConsigneeInfoId { get; set; }
        [ForeignKey("ConsigneeInfoId")]
        public Contact ConsigneeInfo { get; set; }
        public int? ReciverInfoId { get; set; }
        [ForeignKey("ReciverInfoId")]
        public Contact ReciverInfo { get; set; }
        public bool IsMatchConsigneeID { get; set; }

        public List<AttachmentFile> ProofOfDeliveryImages { get; set; }
        public string SignatureImageURL { get; set; }
        public decimal? Weight { get; set; }
        public int? WeightUOMId { get; set; }
        public WeightUOM WeightUOM { get; set; }
        public int? Hight { get; set; }
        public int? Width { get; set; }
        public int? LengthUOMId { get; set; }
        public LengthUOM LengthUOM { get; set; }
        public string ReservedSpecial { get; set; }
        public string Notes { get; set; }
        public int? ItemTypeId { get; set; }
        public MailItemType ItemType { get; set; }
        public int? StatusId { get; set; }
        public MailItemStatus Status { get; set; }
        public List<MailItemsStatusLog> MailItemsStatusLog { get; set; }
        public int? WorkorderId { get; set; }
        public WorkOrder Workorder { get; set; }
        public int? OrderId { get; set; }
        [ForeignKey(nameof(OrderId))]
        [InverseProperty("MailItems")]
        public Order Order { get; set; }
        public int? ItemDeleveryTypeId { get; set; } // 1 for Pickup, 2 for Drop
        [ForeignKey("ItemDeleveryTypeId")]
        public ItemDeleveryType ItemDeleveryType { get; set; }
        public int? ItemReturnReasonId { get; set; }
        [ForeignKey("ItemReturnReasonId")]
        public ItemReturnReason ItemReturnReason { get; set; }
        public DateTime? DropOffDate { get; set; }
        public string OtherItemReturnReason { get; set; }
        public string FromSerial { get; set; }
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveredDate { get; set; }
        public DateTime? ReturnedDate { get; set; }
        public int? CaseNo { get; set; }
        public int? DefaultAssignedDriverID { get; set; }

        [ForeignKey("DefaultAssignedDriverID")]
        public Driver DefaultAssignedDriver { get; set; }

        public decimal? Quantity { get; set; }
        public decimal? TotalPrice { get; set; }
        public int? CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country Country { get; set; }

        public int? RemarkId { get; set; }
        [ForeignKey("RemarkId")]
        public Remark Remark { get; set; }
        public string Description { get; set; }
    }
}
