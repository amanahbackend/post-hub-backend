﻿using Amanah.Posthub.Service.Domain.Common;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class MailItemsStatusLog : BaseEntity<int>
    {
        public int? MailItemId { get; set; }
        [ForeignKey("MailItemId")]
        public MailItem MailItem { get; set; }
        public int? MailItemStatusId { get; set; }

        [ForeignKey("MailItemStatusId")]
        public MailItemStatus MailItemStatus { get; set; }

        public int? WorkOrderId { get; set; }

        [ForeignKey("WorkOrderId")]
        public WorkOrder WorkOrder { get; set; }

        public string Note { get; set; }
    }
}