﻿using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Common;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using Amanah.Posthub.Service.Domain.Managers.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class Order : BaseEntity<int>
    {
        public string OrderCode { get; set; }
        public int? StatusId { get; set; }
        //public int? TotalItemCount { get; set; }
        public OrderStatus Status { get; set; }
        public List<OrdersStatusLog> OrdersStatusLog { get; set; }
        public int? OrderTypeId { get; set; }
        public OrderType OrderType { get; set; }
        public int? ServiceSectorId { get; set; }
        public ServiceSector ServiceSector { get; set; }
        public int? ServiceTypeId { get; set; }
        public ServiceType ServiceType { get; set; }
        public DateTime? IssueAt { get; set; }
        public DateTime? ReadyAt { get; set; }
        public DateTime? StartedAt { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        public DateTime? FulfilledAt { get; set; }
        public decimal? TotalWeight { get; set; }
        public int? WeightUOMId { get; set; }
        public WeightUOM WeightUOM { get; set; }
        public decimal? TotalPostage { get; set; }
        public int? PaymentMethodId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public int? CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public int? BusinessOrderId { get; set; }
        public BusinessOrder BusinessOrder { get; set; }
        public int? DirectOrderId { get; set; }
        public DirectOrder DirectOrder { get; set; }
        public int? TotalItemCount { get; set; }
        [InverseProperty(nameof(MailItem.Order))]
        public List<MailItem> MailItems { get; set; }
        public Address PickupAddress { get; set; }
        public string PickupNote { get; set; }
        public string DropOffNote { get; set; }
        public bool IsDraft { get; set; }
        public int? PickupLocationId { get; set; }
        public int? PickupRequestNotificationId { get; set; }
        //public int? DriverId { get; set; }
        //public Driver Driver { get; set; }
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? PickupDate { get; set; }
        public int? Price { get; set; }
        public int? OrderInternationalDeliveryId { get; set; }
        [ForeignKey("OrderInternationalDeliveryId")]
        public OrderInternationalDelivery OrderInternationalDelivery { get; set; }
        public bool IsMyAddress { get; set; }
        public string SenderName { get; set; }
        //public string SenderId { get; set; }
        //[ForeignKey("SenderId")]
        //public Managers.Entities.Users Sender { get; set; }
        public int? SenderBranchId { get; set; }
        [ForeignKey("SenderBranchId")]
        public Branch SenderBranch { get; set; }
        public string SenderBranchName { get; set; }
        public string SenderEmail { get; set; }
        public string SenderFloorNo { get; set; }
        public bool IsInternal { get; set; }
        public string StatementCode { get; set; }


    }
}
