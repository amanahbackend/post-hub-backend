﻿using Amanah.Posthub.Service.Domain.Common;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class OrderInternationalDelivery : BaseEntity<int>
    {
        public long? AirWayBillNumber { get; set; }
        public int? CourierId { get; set; }
        public Courier Courier { get; set; }
        public string CourierTrackingNumber { get; set; }
        public List<Order> Orders { get; set; }
    }
}
