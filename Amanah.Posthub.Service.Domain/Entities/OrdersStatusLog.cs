﻿using Amanah.Posthub.Service.Domain.Common;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class OrdersStatusLog : BaseEntity<int>
    {
        public int? OrderStatusId { get; set; }
        public OrderStatus OrderStatus { get; set; }
        [ForeignKey("OrderId")]
        public int? OrderId { get; set; }
        public Order Order { get; set; }
        public string Note { get; set; }
    }
}