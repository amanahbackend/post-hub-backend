﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class PackingPackage : BaseEntity
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public string Description_en { get; set; }
        public string Description_ar { get; set; }
        public decimal DimensionWidth { get; set; }
        public decimal DimensionLength { get; set; }
        public decimal DimensionHight { get; set; }
        public decimal MaxWeight { get; set; }

        public List<PackingPackageCountry> PackingPackageCountries { get; set; }


    }
}
