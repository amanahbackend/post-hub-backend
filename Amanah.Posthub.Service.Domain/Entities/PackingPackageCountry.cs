﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.SharedKernel.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class PackingPackageCountry : BaseEntity
    {
        public decimal Price { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public int PackingPackageId { get; set; }
        public PackingPackage PackingPackage { get; set; }
    }
}
