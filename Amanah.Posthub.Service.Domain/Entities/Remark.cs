﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Entities
{
  
    public class Remark : BaseEntity
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsActive { get; set; }
    }
}
