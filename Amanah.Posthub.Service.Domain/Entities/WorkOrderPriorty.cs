﻿namespace Amanah.Posthub.Service.Domain.Entities
{
    public enum WorkOrderPriorty
    {
        Later = 1,
        Normal = 2,
        Hight = 3,
        Critical = 4
    }
}
