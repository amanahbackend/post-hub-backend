﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Entities
{
    public class WorkOrder : BaseEntity
    {
        public string Code { get; set; }
        public WorkorderType WorkorderType { get; set; }
        public List<MailItem> MailItems { get; set; }
        public int? DriverId { get; set; }
        public Driver Driver { set; get; }

        public int? WorkOrderStatusId { get; set; }
        public WorkOrderStatus WorkOrderStatus { set; get; }
        public DateTime? LastStatusDate { get; set; }
        public int? ItemsNo { get; set; }
        public string AreasCovered { get; set; }

        public DateTime? IssueAt { get; set; }
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? AssignedDate { get; set; }

        ///
        public int? PriortyId { set; get; }
        public WorkOrderPriorty Priorty { set; get; }
        public DateTime? PlannedStart { get; set; }
        public DateTime? DeliveryBefore { get; set; }
        //public string StatementCode { get; set; }
        public int? SecurityCaseNumber { get; set; }
        public int? ServiceSectorId { get; set; }
        public ServiceSector ServiceSector { get; set; }
        public string WorkOrderImageURL { get; set; }
        //public int? IsInternal { get; set; }

    }
}
