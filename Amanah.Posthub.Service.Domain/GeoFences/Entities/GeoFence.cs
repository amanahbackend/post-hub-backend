﻿using Amanah.Posthub.BASE.Domain.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.GeoFences.Entities
{
    public class GeoFence : BaseEntity
    {
        public GeoFence()
        {
            GeoFenceLocations = new HashSet<GeoFenceLocation>();
        }
        public GeoFence(string tenantId)
        {
            GeoFenceLocations = new HashSet<GeoFenceLocation>();
            GeoFenceBlocks = new HashSet<GeoFenceBlocks>();
            Tenant_Id = tenantId;
        }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<GeoFenceLocation> GeoFenceLocations { get; set; }
        public  ICollection<GeoFenceBlocks> GeoFenceBlocks { get; set; }

    }
}
