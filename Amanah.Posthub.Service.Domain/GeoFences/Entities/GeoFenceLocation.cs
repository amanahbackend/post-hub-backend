﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.GeoFences.Entities
{
    public class GeoFenceLocation : BaseEntity
    {
        public int GeoFenceId { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public virtual GeoFence GeoFence { get; set; }
        public int PointIndex { get; set; }
    }
}
