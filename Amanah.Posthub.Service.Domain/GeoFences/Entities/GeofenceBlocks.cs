﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.GeoFences.Entities
{
    public class GeoFenceBlocks : BaseEntity
    {
        public int GeoFenceId { get; set; }
        public string Area { get; set; }
        public string Block { get; set; }
        public virtual GeoFence GeoFence { get; set; }
    }
}
