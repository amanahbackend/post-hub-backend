﻿using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Domain.GeoFences.Repositories
{
    public interface IGeofenceRepository : IRepository<GeoFence>
    {
        Task<List<GeoFence>> GetWithGeofencLocationAsync(Expression<Func<GeoFence, bool>> expression);
    }
}
