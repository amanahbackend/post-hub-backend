﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;

namespace Amanah.Posthub.Service.Domain.LoginRequests.Entities
{
    public class DriverLoginRequest : BaseEntity<int>
    {
        /// <summary>
        /// DriverLoginRequestSatus enum type
        /// </summary>
        public byte Status { get; set; }
        public string Token { get; set; }
        public int DriverId { get; set; }

        public string ApprovedOrRejectBy { get; set; }

        public Driver Driver { get; set; }
        public string Reason { get; set; }

        private DriverLoginRequest()
        {
        }

        public DriverLoginRequest(string tenantId)
        {
            Tenant_Id = tenantId;
        }
    }
}
