﻿namespace Amanah.Posthub.Service.Domain.LoginRequests.Entities
{
    public enum DriverLoginRequestStatus : byte
    {
        Pending = 1,
        Approved,
        Rejected,
        Canceled
    }


}
