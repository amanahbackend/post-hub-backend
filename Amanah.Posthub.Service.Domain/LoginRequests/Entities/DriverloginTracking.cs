﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using System;

namespace Amanah.Posthub.Service.Domain.LoginRequests.Entities
{
    public class DriverloginTracking : BaseEntity
    {
        public int DriverId { get; set; }
        public DateTime? LoginDate { get; set; }
        public double? LoginLatitude { get; set; }
        public double? LoginLongitude { get; set; }
        public DateTime? LogoutDate { get; set; }
        public double? LogoutLatitude { get; set; }
        public double? LogoutLongitude { get; set; }
        public string LogoutActionBy { get; set; }
        public Driver Driver { get; set; }

        private DriverloginTracking()
        {
        }

        public DriverloginTracking(string tenantId)
        {
            Tenant_Id = tenantId;
        }
    }
}
