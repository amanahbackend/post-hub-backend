﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Managers.Entities
{
    public class Gender : BaseEntity, ILookupEntity
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }
    }
}
