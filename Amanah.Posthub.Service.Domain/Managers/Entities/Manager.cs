﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Teams.Entities;
using Amanah.Posthub.Service.Domain.Users.Entities;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Managers.Entities
{
    public class Manager : BaseEntity
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int GenderId { get; set; }
        public Gender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int NationalityId { get; set; }
        public Country Nationality { get; set; }
        public Address Address { set; get; }
        public string ProfilePicURL { get; set; }
        public string NationalId { get; set; }
        public DateTime? NationalIdExtractDate { get; set; }
        public DateTime? NationalIdExpiryDate { get; set; }
        public string SupervisorId { get; set; }
        public ApplicationUser Supervisor { get; set; }
        public int? ServiceSectorId { get; set; }
        public ServiceSector ServiceSector { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public int BranchId { get; set; }
        public Branch Branch { get; set; }
        public int AccountStatusId { get; set; }
        public AccountStatus AccountStatus { get; set; }
        public ICollection<TeamManager> TeamManagers { get; set; }
        public string MobileNumber { get; set; }
        public string CompanyEmail { get; set; }

    }
}
