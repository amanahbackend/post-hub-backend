﻿using Amanah.Posthub.BASE.Domain.Entities;
using System;

namespace Amanah.Posthub.Service.Domain.Notifications.Entities
{
    public class Notification : BaseEntity
    {
        public Notification()
        {
        }
        public Notification(string tenantId)
        {
            Tenant_Id = tenantId;
        }
        public string Title { get; set; }
        public bool IsSeen { get; set; }
        public string Body { get; set; }
        public DateTime Date { get; set; }
        public string ToUserId { get; set; }
        public string FromUserId { get; set; }

        //public ApplicationUser FromUser { get; set; }
        //public ApplicationUser ToUser { get; set; }

    }
}
