﻿using Amanah.Posthub.Service.Domain.Notifications.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.Notifications.Repositories
{
    public interface INotificationRepository : IRepository<Notification>
    {

    }
}
