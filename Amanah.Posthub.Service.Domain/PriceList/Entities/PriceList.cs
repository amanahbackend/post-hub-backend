﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.PriceList.Entities
{
    public class PriceList : BaseEntity
    {
        public string Code { get; set; }
        //public int? ServiceSectorId { get; set; }
        //public ServiceSector ServiceSector { get; set; }
        public int? ServiceSectorId { get; set; }
        [ForeignKey("ServiceSectorId")]
        public ServiceSector ServiceSector { get; set; }
        public int? PriceListStatusId { get; set; }
        public PriceListStatus PriceListStatus { get; set; }
        public DateTime? ValidToDate { get; set; }
        public DateTime? ValidFromDate { get; set; }
        public ICollection<PriceListItems> PriceListItems { get; set; }
    }
}
