﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.PriceList.Entities
{
    public class PriceListItems : BaseEntity
    {
        public string FromArea { get; set; }
        public string ToArea { get; set; }

        public double? FromWeight { get; set; }
        public double? ToWeight { get; set; }
        public int ServiceTime { get; set; }
        public int? ServiceTimeUnitId { get; set; }
        public string ServiceTimeUnit { get; set; }
        public decimal Price { get; set; }
        public int PriceListId { get; set; }
        public PriceList PriceList { get; set; }
    }
}
