﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.BusinessCustomers;
using Amanah.Posthub.Service.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Service.Domain.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.PriceList.Entities
{
    public class RFQ : BaseEntity
    {
        public string PQCode { get; set; }
        public string LetterText { get; set; }
        public string LetterSubject { get; set; }
        public string PQSubject { get; set; }
        public string Services { get; set; }
        public int? PriceQuotationStatusId { get; set; }
        public PriceQuotationStatus PriceQuotationStatus { get; set; }
        public DateTime? PQDate { get; set; }
        public DateTime? PQValidToDate { get; set; }
        public decimal? Total { get; set; }
        public int? BusinessCustomerId { get; set; }
        public bool? IsGeneralOffer { get; set; }
        public BusinessCustomer BusinessCustomer { get; set; }
        public int ServiceSectorId { get; set; }
        public ServiceSector ServiceSector { get; set; }
        public ICollection<RFQItems> RFQItemPrices { get; set; }
        public ICollection<Contract> RFQContracts { get; set; }

    }

}
