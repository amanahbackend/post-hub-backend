﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Entities.Lookups;

namespace Amanah.Posthub.Service.Domain.PriceList.Entities
{
    public class RFQItems : BaseEntity
    {
        public int RFQId { get; set; }
        public RFQ RFQ { get; set; }
        public string Description { get; set; }
        public bool? HasAmount { get; set; }
        public decimal? ItemPrice { get; set; }
        public int CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public double? Quantity { get; set; }
        public string Notes { get; set; }
        public double? Weight { get; set; }
        public decimal? ToGoShippingPrice { get; set; }
        public decimal? CommingShippingPrice { get; set; }
        public decimal? RoundTripShippingPrice { get; set; }
    }
}
