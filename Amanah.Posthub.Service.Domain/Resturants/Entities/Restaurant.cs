﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Resturants.Entities
{
    public class Restaurant : BaseEntity
    {
        public Restaurant()
        {
        }
        public Restaurant(string tenantId)
        {
            Tenant_Id = tenantId;
        }

        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Reason { get; set; }

        public virtual ICollection<Branch> Branchs { get; set; }

    }
}
