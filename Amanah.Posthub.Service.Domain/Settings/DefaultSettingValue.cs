﻿namespace Amanah.Posthub.BLL.Settings.Constant
{
    public class DefaultSettingValue
    {
        public DefaultSettingValue()
        {

        }

        public double ReachedDistanceRestrictionInMeter { get; set; } = 300;
        public int DriverLoginRequestExpirationInHour { get; set; } = 24;

        public string AutoAllocationMethod { get; set; } = "";
        public bool IsEnableAutoAllocation { get; set; } = false;

        public int FirstInFirstOutClubbingTimeInSec { get; set; } = 60;
        public int FirstInFirstOutMethodDriverOrderCapacity { get; set; } = 3;

        public double NearestAvailableMethodRadiusInKM { get; set; } = 100;
        public int NearestAvailableMethodNumberOfRetries { get; set; } = 10;
        public int NearestAvailableMethodOrderCapacity { get; set; } = 5;
        public int NearestAvailableMethodReScheduleRequestTimeInSec { get; set; } = 10;

        public bool IsServingInAllGeofences { get; set; }

    }
}
