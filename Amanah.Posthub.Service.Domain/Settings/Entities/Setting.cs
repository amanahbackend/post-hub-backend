﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Settings.Entities
{
    public class Setting : BaseEntity
    {
        public string SettingKey { set; get; }
        public string Value { set; get; }
        public string SettingDataType { set; get; }
        public string GroupId { set; get; }

        private Setting()
        {
        }
        public Setting(string tenantId)
        {
            Tenant_Id = tenantId;
        }
    }
}
