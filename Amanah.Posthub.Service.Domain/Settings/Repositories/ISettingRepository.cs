﻿using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.Service.Domain.Settings.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;

namespace Amanah.Posthub.Domain.MainTasks.Repositories
{
    public interface ISettingRepository : IRepository<Setting>
    {
        Task<int> GetAutoAllocationTypeidAsync();
        Task<Setting> GetSettingByKeyAsync(string key);
        IQueryable<Setting> GetSettingByKeyAsync(List<string> key);
        Task<Setting> GetAsync(int id);
        Task<bool> AddUserDefaultSettingsAsync(string tenantId, DefaultSettingValue defaultSetting, ResourceSet resourceSet);
    }

}
