﻿namespace Amanah.Posthub.BLL.Settings.Constant
{
    public class SettingKeyConstant
    {
        public const string NearestAvailableMethod = "NearestAvailableMethod";
        public const string NearestAvailableMethodOrderCapacity = "NearestAvailableMethodOrderCapacity";
        public const string NearestAvailableMethodRadiusInKM = "NearestAvailableMethodRadiusInKM";
        public const string NearestAvailableMethodNumberOfRetries = "NearestAvailableMethodNumberOfRetries";

        public const string OneByOneMethod = "OneByOneMethod";

        public const string DriverLoginRequestExpiration = "DriverLoginRequestExpiration";
        public const string ReachedDistanceRestriction = "ReachedDistanceRestriction";

        public const string SelectedAutoAllocationMethodName = "SelectedAutoAllocationMethodName";
        public const string IsEnableAutoAllocation = "IsEnableAutoAllocation";
        public const string FirstInFirstOutMethod = "FirstInFirstOutMethod";

    }
}
