﻿using Amanah.Posthub.BASE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Amanah.Posthub.Service.Domain.Tasks.Entities
{
    public class MainTask : BaseEntity
    {
        [NotMapped]
        public readonly static int OrderWeightValue = 1;

        [NotMapped]

        public int OrderWeight => 1;

        public int MainTaskTypeId { set; get; }
        public bool? IsCompleted { set; get; }
        public bool? IsDelayed { set; get; }
        public MainTaskType MainTaskType { set; get; }
        public int AssignmentType { get; set; }
        public bool IsFailToAutoAssignDriver { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int? MainTaskStatusId { set; get; }
        public double? EstimatedTime { set; get; }

        public MainTaskStatus MainTaskStatus { set; get; }
        public ICollection<SubTask> Tasks { set; get; }
    }
}
