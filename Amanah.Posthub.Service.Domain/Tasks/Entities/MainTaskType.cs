﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Tasks.Entities
{
    public class MainTaskType : BaseGlobalEntity
    {
        public string Name { set; get; }
    }
}
