﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Addresses.Entities;
using Amanah.Posthub.Service.Domain.Branches.Entities;
using Amanah.Posthub.Service.Domain.Customers.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.GeoFences.Entities;
using System;
using System.Collections.Generic;


namespace Amanah.Posthub.Service.Domain.Tasks.Entities
{
    public class SubTask : BaseEntity
    {
        public int MainTaskId { set; get; }
        public int TaskTypeId { set; get; }
        public int CustomerId { set; get; }
        public int TaskStatusId { set; get; }
        public int? DriverId { set; get; }
        public string OrderId { set; get; }
        public string Address { set; get; }
        public double? Latitude { set; get; }
        public double? Longitude { set; get; }
        public string Description { set; get; }
        public string Image { set; get; }
        public DateTime? PickupDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? StartDate { get; set; }
        public double? DelayTime { get; set; }
        public DateTime? SuccessfulDate { get; set; }
        public double? TotalTime { get; set; }
        public string Notes { set; get; }
        public string SignatureFileName { set; get; }
        public string SignatureURL { set; get; }
        public int? BranchId { get; set; }
        public Branch Branch { get; set; }
        public int? GeoFenceId { get; set; }
        public GeoFence GeoFence { get; set; }
        public double? TotalDistance { get; set; }
        public double? TotalTaskTime { get; set; }
        public bool? IsTaskReached { get; set; }
        public DateTime? ReachedTime { get; set; }

        public Address Location { set; get; }

        public MainTask MainTask { set; get; }
        public TaskType TaskType { set; get; }
        public Customer Customer { set; get; }
        public TaskStatus TaskStatus { set; get; }
        public double? EstimatedTime { set; get; }
        public Driver Driver { set; get; }
        public ICollection<TaskGallary> TaskGallaries { set; get; }
        public ICollection<TaskHistory> TaskHistories { set; get; }
        public ICollection<DriverTaskNotification> DriverTaskNotifications { set; get; }
        public ICollection<TaskRoute> TaskRoutes { set; get; }
        public virtual ICollection<DriverRate> DriverRates { get; set; }

    }
}
