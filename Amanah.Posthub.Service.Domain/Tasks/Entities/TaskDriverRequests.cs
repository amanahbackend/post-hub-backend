﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Tasks.Entities
{
    public class TaskDriverRequests : BaseEntity<int>
    {
        public int TaskId { set; get; }
        public int MainTaskId { set; get; }
        public MainTask MainTask { set; get; }
        public int? DriverId { set; get; }
        public int ResponseStatus { set; get; }
        public int RetriesCount { get; set; }
    }
}
