﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;

namespace Amanah.Posthub.Service.Domain.Tasks.Entities
{
    public class TaskRoute : BaseEntity<int>
    {
        public int TaskId { set; get; }
        public int DriverId { set; get; }
        public int? TaskStatusId { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public SubTask Task { set; get; }
        public Driver Driver { set; get; }
        public TaskStatus TaskStatus { set; get; }
    }
}
