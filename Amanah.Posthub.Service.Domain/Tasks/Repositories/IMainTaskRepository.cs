﻿using Amanah.Posthub.Service.Domain.Drivers.Entities;
using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Domain.MainTasks.Repositories
{
    public interface IMainTaskRepository : IRepository<MainTask>
    {
        Task<MainTask> GetWithTasksAsync(Expression<Func<MainTask, bool>> expression);
        Task<Driver> GetMainTaskCurrentDriverAsync(int id);
    }

}
