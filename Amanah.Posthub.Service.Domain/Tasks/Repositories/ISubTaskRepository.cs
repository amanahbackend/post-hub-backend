﻿using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Domain.SubTasks.Repositories
{
    public interface ISubTaskRepository : IRepository<SubTask>
    {
        Task<SubTask> GetSingleForStartAsync(int id);
        Task<bool> CheckTaskAsync(Expression<Func<SubTask, bool>> expression, bool? ignoreFilter = false);
    }

}
