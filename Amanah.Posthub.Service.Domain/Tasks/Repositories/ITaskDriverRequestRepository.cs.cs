﻿using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace Amanah.Posthub.Domain.MainTasks.Repositories
{
    public interface ITaskDriverRequestRepository : IRepository<TaskDriverRequests>
    {
        Task<TaskDriverRequests> GetTaskDriverRequestsLastAsync(int mainTaskId, int? driverId);
        IQueryable<TaskDriverRequests> GetTaskDriverRequests(int mainTaskId, int? driverId);
    }

}
