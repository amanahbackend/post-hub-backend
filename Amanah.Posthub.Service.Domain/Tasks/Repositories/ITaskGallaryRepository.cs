﻿using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.SubTasks.Repositories
{
    public interface ITaskGallaryRepository : IRepository<TaskGallary>
    {
    }

}
