﻿using Amanah.Posthub.Service.Domain.Tasks.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.TaskHistorys.Repositories
{
    public interface ITaskHistoryRepository : IRepository<TaskHistory>
    {

    }

}
