﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.Service.Domain.Teams.Entities
{
    public class LocationAccuracy : BaseGlobalEntity
    {
        public string Name { set; get; }
        public int? Duration { set; get; }
    }
}
