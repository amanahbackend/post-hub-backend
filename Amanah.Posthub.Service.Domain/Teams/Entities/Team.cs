﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Drivers.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Teams.Entities
{
    public class Team : BaseEntity
    {
        public Team(string tenantId)
        {
            Tenant_Id = tenantId;
        }
        public Team() { }

        public string Name { set; get; }
        public string Tags { set; get; }
        public string Address { set; get; }
        public int LocationAccuracyId { set; get; }
        public LocationAccuracy LocationAccuracy { get; set; }
        public virtual ICollection<TeamManager> TeamManagers { get; set; }
        public virtual ICollection<Driver> Drivers { get; set; }
    }
}
