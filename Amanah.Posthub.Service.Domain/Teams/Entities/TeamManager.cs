﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.Service.Domain.Managers.Entities;

namespace Amanah.Posthub.Service.Domain.Teams.Entities
{
    public class TeamManager : BaseEntity
    {
        public int TeamId { set; get; }
        public Team Team { get; set; }
        public int ManagerId { set; get; }
        public Manager Manager { get; set; }
    }
}
