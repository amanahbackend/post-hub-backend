﻿using System.Collections.Generic;

namespace Amanah.Posthub.Service.TenantRegistrations.Entities
{
    public class ApproveSettings
    {
        public bool IsFullVersion { get; set; }
        public string AutoAllocationMethod { get; set; }
        public List<int> SelectedModules { get; set; }

    }
}
