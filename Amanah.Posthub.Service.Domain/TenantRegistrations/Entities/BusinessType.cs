﻿namespace Amanah.Posthub.Service.TenantRegistrations.Entities
{
    public enum BusinessType
    {
        FoodDelivery = 1,
        GroceryDelivery,
        MedicineDelivery,
        RetailStore,
        HomeCleaningAndRepair,
        AppliancesAndElectricRepair,
        DryCleaning,
        Others
    }
}
