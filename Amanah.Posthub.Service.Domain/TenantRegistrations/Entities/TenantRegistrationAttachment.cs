﻿namespace Amanah.Posthub.Service.TenantRegistrations.Entities
{
    public class TenantRegistrationAttachment
    {
        public int Id { get; set; }
        public int TenantRegistrationId { get; set; }
        public string File { get; set; }
    }

}
