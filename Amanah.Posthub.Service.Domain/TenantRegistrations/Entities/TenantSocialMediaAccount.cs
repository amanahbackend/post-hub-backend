﻿namespace Amanah.Posthub.Service.TenantRegistrations.Entities
{
    public class TenantSocialMediaAccount
    {
        public int Id { get; set; }
        public int TenantRegistrationId { get; set; }
        public string AccountLink { get; set; }
    }

}
