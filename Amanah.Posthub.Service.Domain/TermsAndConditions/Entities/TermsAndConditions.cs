﻿using Amanah.Posthub.BASE.Domain.Entities;


namespace Amanah.Posthub.Service.Domain.TermsAndConditions.Entities
{
    public class TermsAndConditions : BaseGlobalEntity
    {
        public string TermsAndConditionsInEnglish { get; set; }
        public string TermsAndConditionsInArabic { get; set; }
        public TermsAndConditionsType Type { get; set; }
    }
}
