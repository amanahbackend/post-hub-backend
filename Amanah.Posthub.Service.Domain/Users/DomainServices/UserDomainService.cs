﻿using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.Domain.Users.DomainServices
{
    public interface IUserDomainService
    {
        Task<ApplicationUser> GetAsync(Expression<Func<ApplicationUser, bool>> expression);
        Task<ApplicationUser> GetAsync(string userId);

        Task<ApplicationUser> CreateAsync(ApplicationUser user, string password = null, string role = null, bool? isTenantUser = null);
        Task<ApplicationUser> CreateAsync(ApplicationUser user, List<string> roles, string password = null, bool? isTenantUser = null);
        Task<ApplicationUser> CreateAsync(ApplicationUser user);
        Task<ApplicationUser> EditAsync(ApplicationUser user, string role, string password = null);

        Task<ApplicationUser> LoginAsync(string password, string userName = null, string email = null);
        Task AddUserToRolesAsync(ApplicationUser user);
        Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user);
        Task<bool> ChangePasswordAsync(string userId, string currentPassword, string newPassword);
        Task<bool> ChangeCurrentPasswordAsync(string userId, string newPassword);
        Task<bool> ChangeUserPassword(string userId, string newPassword);
    }
    public class UserDomainService : IUserDomainService
    {
        private readonly IUserRepository userRepository;
        private readonly IRoleRepository roleRepository;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly IUnitOfWork unitOfWork;


        public UserDomainService(
            IUserRepository userRepository,
            IRoleRepository roleRepository,
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            IUnitOfWork unitOfWork)
        {
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.unitOfWork = unitOfWork;
        }

        public async Task<ApplicationUser> GetAsync(Expression<Func<ApplicationUser, bool>> expression)
        {
            return await userRepository.GetAsync(expression);
        }

        public async Task<ApplicationUser> GetAsync(string userId)
        {
            return await userManager.FindByIdAsync(userId);
        }

        public async Task<ApplicationUser> CreateAsync(
            ApplicationUser user,
            string password = null,
            string role = null,
            bool? isTenantUser = null)
        {
            IdentityResult userResult = await CreateUserAsync(user, password);
            if (userResult.Succeeded)
            {
                userRepository.AddUserClaim(user.Id, isTenantUser);
                await SetUserTenantIdAsync(user, isTenantUser);
                if (!string.IsNullOrEmpty(role))
                {
                    await CreateUserRoleAsync(user, role);
                }
                await unitOfWork.SaveChangesAsync();
            }
            else
            {
                throw new DomainException(userResult.Errors.FirstOrDefault().Description);
            }

            return user;
        }


        public async Task<ApplicationUser> CreateAsync(
           ApplicationUser user,
           List<string> roles,
           string password = null,
           bool? isTenantUser = null)
        {
            IdentityResult userResult = await CreateUserAsync(user, password);
            if (userResult.Succeeded)
            {
                userRepository.AddUserClaim(user.Id, isTenantUser);
                await SetUserTenantIdAsync(user, isTenantUser);
                if (roles != null && roles.Any())
                {
                    await CreateUserRoleAsync(user, roles);
                }
                await unitOfWork.SaveChangesAsync();
            }
            return user;
        }


        public async Task<ApplicationUser> CreateAsync(
            ApplicationUser user)
        {

            if (user.RoleNames != null && user.RoleNames.Count > 0)
            {
                var identityResult = await userManager.CreateAsync(user);
                if (identityResult.Succeeded)
                {
                    await AddUserToRolesAsync(user);

                    if (user.IsTenantUser)
                    {
                        await SetUserTenantIdAsync(user, user.IsTenantUser);
                    }
                }
                await unitOfWork.SaveChangesAsync();
            }
            else
            {
                return null;
            }
            return user;
        }


        public async Task<ApplicationUser> EditAsync(
            ApplicationUser user,
            string role,
            string password = null)
        {
            if (!string.IsNullOrEmpty(password))
            {
                await userManager.RemovePasswordAsync(user);
                await userManager.AddPasswordAsync(user, password);
            }
            var EditedUser = await userManager.FindByIdAsync(user.Id); //await userRepository.GetAllIQueryable().IgnoreQueryFilters().Where(x => x.Email == user.Email).FirstOrDefaultAsync(); //userManager.FindByEmailAsync(user.Email);
            EditedUser.FirstName = user.FirstName;
            EditedUser.LastName = user.LastName;
            EditedUser.CountryId = user.CountryId;
            EditedUser.PhoneNumber = user.PhoneNumber;
            EditedUser.MiddleName = user.MiddleName;

            await userManager.UpdateAsync(EditedUser);
            try
            {

                var existingRoles = await userManager.GetRolesAsync(user);
                if (existingRoles != null && existingRoles.Count > 0)
                    if (!existingRoles.Any(r => r.ToLower().Trim() == role.Trim().ToLower()))
                    {
                        var userForNewRole = await userManager.FindByIdAsync(user.Id);

                        await userManager.RemoveFromRolesAsync(userForNewRole, existingRoles);
                        await userManager.AddToRoleAsync(userForNewRole, role);
                    }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return user;
        }


        public async Task<ApplicationUser> LoginAsync(
            string password,
            string userName = null,
            string email = null)
        {
            ApplicationUser user = await userRepository.GetLoginAsync(userName, email);
            if (user != null)
            {
                if (await userManager.CheckPasswordAsync(user, password)) //valid password
                {
                    return user;
                }
            }
            return null;

        }

        public async Task AddUserToRolesAsync(ApplicationUser user)
        {
            if (user != null && user.RoleNames != null)
            {
                foreach (var role in user.RoleNames)
                {
                    var roleExistsInDifferentTenant = await roleRepository.GetAsync(
                        existingRole =>
                            existingRole.NormalizedName == role.Trim().ToUpper() &&
                            existingRole.Tenant_Id != user.Tenant_Id
                        );
                    if (roleExistsInDifferentTenant != null)
                    {
                        //throw new DomainException(Keys.Validation.RoleAlreadyExists);
                    }
                    if (await roleManager.FindByNameAsync(role) == null)
                    {
                        await roleManager.CreateAsync(new ApplicationRole(role));
                    }
                }
                await userManager.AddToRolesAsync(user, user.RoleNames);
            }
        }


        public async Task<string> GeneratePasswordResetTokenAsync(ApplicationUser user)
        {
            var token = await userManager.GeneratePasswordResetTokenAsync(user);
            return token;
        }


        public async Task<bool> ChangePasswordAsync(string userId, string currentPassword, string newPassword)
        {
            var user = await userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var result = await userManager.ChangePasswordAsync(user, currentPassword, newPassword);
                if (result.Succeeded)
                {
                    return true;
                }
            }
            return false;

        }
        public async Task<bool> ChangeUserPassword(string userId, string newPassword)
        {
            var user = await userManager.FindByIdAsync(userId);
            if (user != null)
            {

                //var user = await userManager.FindByIdAsync(userId);

                var token = await userManager.GeneratePasswordResetTokenAsync(user);

                var result = await userManager.ResetPasswordAsync(user, token, newPassword);
                if (result.Succeeded)
                {
                    return true;
                }
            }
            return false;


        }

        public async Task<bool> ChangeCurrentPasswordAsync(string userId, string newPassword)
        {  
            var user = await userManager.FindByIdAsync(userId);
            if (user != null)
            {
                
                //var user = await userManager.FindByIdAsync(userId);

                var token = await userManager.GeneratePasswordResetTokenAsync(user);

                var result = await userManager.ResetPasswordAsync(user, token, newPassword);
                if (result.Succeeded)
                {
                    return true;
                }
            }
            return false;

           
        }
        private async Task<IdentityResult> CreateUserAsync(ApplicationUser user, string password)
        {
            user.SecurityStamp = Guid.NewGuid().ToString("D");
            if (string.IsNullOrEmpty(password))
            {
                return await userManager.CreateAsync(user);
            }
            else
            {
                return await userManager.CreateAsync(user, password);
            }
        }
        private async Task CreateUserRoleAsync(ApplicationUser user, List<string> roles)
        {
            foreach (var role in roles)
            {
                await ValidateAndCreateRoleIfNotExistAsync(user, role);
            }
            await userManager.AddToRolesAsync(user, roles);
        }
        private async Task CreateUserRoleAsync(ApplicationUser user, string role)
        {
            await ValidateAndCreateRoleIfNotExistAsync(user, role);
            await userManager.AddToRoleAsync(user, role);
        }
        private async Task ValidateAndCreateRoleIfNotExistAsync(ApplicationUser user, string role)
        {
            var existingRole = await roleRepository.GetAsync(existingRole => existingRole.NormalizedName == role.Trim().ToUpper());

            var roleExistsInDifferentTenant = existingRole != null && (user.Tenant_Id == null || existingRole.Tenant_Id != user.Tenant_Id);
            if (roleExistsInDifferentTenant)
            {
                throw new DomainException(Keys.Validation.RoleAlreadyExists);
            }
            if (existingRole == null)
            {
                await roleManager.CreateAsync(new ApplicationRole(role, user.Tenant_Id));
            }
        }
        private async Task SetUserTenantIdAsync(ApplicationUser user, bool? isTenantUser)
        {
            if (isTenantUser.HasValue && isTenantUser.Value)
            {
                user.SetTenant(user.Id);
                await userManager.UpdateAsync(user);
            }
        }


    }
}
