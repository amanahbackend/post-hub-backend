﻿using Amanah.Posthub.BASE.Domain.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Amanah.Posthub.Service.Domain.Users.Entities
{
    public class ApplicationRole : IdentityRole, IBaseEntity<string>
    {
        private ApplicationRole()
        {
        }
        public ApplicationRole(string roleName)
          : base(roleName)
        {
        }
        public ApplicationRole(string roleName, string tenantId)
         : base(roleName)
        {
            Tenant_Id = tenantId;
        }

        public string CreatedBy_Id { get; private set; }
        public string UpdatedBy_Id { get; private set; }
        public string DeletedBy_Id { get; private set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; private set; }
        public DateTime UpdatedDate { get; private set; }
        public DateTime DeletedDate { get; private set; }
        public string Tenant_Id { get; private set; }

        public int Type { get; set; }

        public void SetIsDeleted(bool isDeleted)
        {
            IsDeleted = IsDeleted;
            if (isDeleted)
            {
                DeletedDate = DateTime.UtcNow;
            }
            else
            {
                DeletedDate = DateTime.MinValue;
            }
        }

        public virtual IReadOnlyCollection<IdentityRoleClaim<string>> RoleClaims { get; set; }
    }
}
