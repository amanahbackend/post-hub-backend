﻿using Amanah.Posthub.BASE.Domain.Entities;


namespace Amanah.Posthub.Service.Domain.Users.Entities
{
    public class UserDevice : BaseGlobalEntity
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public string FCMDeviceId { get; set; }

        public bool? IsLoggedIn { get; set; }
    }
}
