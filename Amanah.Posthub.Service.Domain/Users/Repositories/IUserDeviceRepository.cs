﻿using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Domain.Users.Repositories
{
    public interface IUserDeviceRepository : IRepository<UserDevice>
    {
        Task AddOrUpdateAsync(UserDevice userDevice);

        Task<List<UserDevice>> SoftDeleteAsync(Expression<Func<UserDevice, bool>> expression);
        Task<List<UserDevice>> HardDeleteAsync(Expression<Func<UserDevice, bool>> expression);
    }
}
