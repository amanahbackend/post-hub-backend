﻿using Amanah.Posthub.Service.Domain.Users.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Amanah.Posthub.Domain.Users.Repositories
{
    public interface IUserRepository : IRepository<ApplicationUser, string>
    {
        Task<ApplicationUser> GetLoginAsync(string username = null, string email = null);
        Task<ApplicationUser> GetAsync(Expression<Func<ApplicationUser, bool>> userExpression);
        Task<bool> CheckIfTenantHaveThePermissionAsync(string permissionName);

        void AddUserClaim(string userId, bool? isTenantUser = null);
    }
}
