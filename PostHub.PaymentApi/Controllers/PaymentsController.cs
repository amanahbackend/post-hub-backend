﻿using Amanah.PaymentIntegrations.Domain.IRepositories;
using Amanah.PaymentIntegrations.DTOs;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.DTOs;
using Amanah.PaymentIntegrations.PaymentGatewayConfigurations.UpPayments.Services;
using Amanah.PaymentIntegrations.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace PostHub.PaymentApi.Controllers
{
    [ApiController]
    [Route("api/Payments")]

    public class PaymentsController : ControllerBase
    {
        private readonly IPaymentService _paymentService;

        public PaymentsController(
            IPaymentService paymentService,
        ILogger<PaymentsController> logger)
        {
            logger.Log(LogLevel.Debug, "Start in constructor");
            _paymentService = paymentService;
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestPaymentRequestDTO requestPayment)
        {
            var response = await _paymentService.RequestPaymentAsync(requestPayment);
            return Ok(response);
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {

            var response = await _paymentService.RequestPaymentAsync(new RequestPaymentRequestDTO
            {
                ReferenceId = "10",
                TotalPrice = 100,
            });
            return Ok(response);
        }

        [HttpGet("GetByReferenceId/{referenceId}")]
        public async Task<IActionResult> GetByReferenceId(string referenceId)
        {
            //var response = await _paymentService.GetAllPaymentTransactionAsync(referenceId);
            var response = await _paymentService.GetPaymentTransactionAsync(referenceId);
            return Ok(response);
        }

        //[HttpGet("Success")]
        //public async Task Success(
        //  string payment_type,
        //  string PaymentID,
        //  string Result,
        //  string PostDate,
        //  string TranID,
        //  string Ref,
        //  string TrackID,
        //  string Auth,
        //  string OrderID,
        //  string cust_ref,
        //  string trnUdf)
        //{
        //    await _paymentService.EditPaymentTransactionResponseAsync(new UpPaymentsTransactionResponseDTO
        //    {
        //        IsSuccess = true,
        //        PaymentType = payment_type,
        //        PaymentId = PaymentID,
        //        PaymentResult = Result,
        //        OrderId = OrderID,
        //        TransactionId = TranID,
        //        TrackId = TrackID,
        //        PostDate = PostDate,
        //        TransactionReferenceId = Ref,
        //        Auth = Auth,
        //    });
        //}


        //[HttpGet("Fail")]
        //public async Task Fail(
        //  string PaymentID,
        //  string Result,
        //  string PostDate,
        //  string TranID,
        //  string Ref,
        //  string TrackID,
        //  string Auth,
        //  string OrderID,
        //  string cust_ref,
        //  string trnUdf)
        //{
        //    await _paymentService.EditPaymentTransactionResponseAsync(new UpPaymentsTransactionResponseDTO
        //    {
        //        IsSuccess = false,
        //        PaymentId = PaymentID,
        //        PaymentResult = Result,
        //        OrderId = OrderID,
        //        TransactionId = TranID,
        //        TrackId = TrackID,
        //        PostDate = PostDate,
        //        TransactionReferenceId = Ref,
        //        Auth = Auth,
        //    });
        //}


        /*
         https://amanah-paymentintegrations-test.conveyor.cloud/api/Payments/Fail
        ?PaymentID=100202204178693344
        &Result=NOT%20CAPTURED
        &PostDate=0210
        &TranID=202204178620519
        &Ref=204110000011
        &TrackID=f392eeadce5b76c0060358f506fb33e2
        &Auth=000000
        &OrderID=a@bc1598478745555541454555246545454545454544545451555505454
        &cust_ref=
        &trnUdf=
        */
    }
}
