﻿using System;

namespace ReportsManager.Datasets
{
    public class InternationalRFQDetailsDS
    {
        public string Description { get; set; }
        public decimal? ItemPrice { get; set; }
        public string CountryName { get; set; }
        public double? Quantity { get; set; }
        public double? Weight { get; set; }
        public decimal? ToGoShippingPrice { get; set; }
        public decimal? CommingShippingPrice { get; set; }
        public decimal? RoundTripShippingPrice { get; set; }
    }
}
