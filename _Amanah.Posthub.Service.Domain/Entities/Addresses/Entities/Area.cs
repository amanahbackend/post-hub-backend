﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class Area : BaseGlobalEntity  <string>
    {
        public string NameEN { get; set; }
        public string NameAR { get; set; }
        public string FK_Governrate_Id { get; set; }
    }
}
