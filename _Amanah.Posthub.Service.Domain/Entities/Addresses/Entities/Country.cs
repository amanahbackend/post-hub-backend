﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{

    public class Country : BaseGlobalEntity <int>
    {
        public string Name { set; get; }
        public string Code { set; get; }
        public string Flag { set; get; }
        public string TopLevel { set; get; }

    }
}
