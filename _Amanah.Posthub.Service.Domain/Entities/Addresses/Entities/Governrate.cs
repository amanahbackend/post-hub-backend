﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class Governrate : BaseGlobalEntity<string>
    {
        public string NameEN { get; set; }
        public string NameAR { get; set; }
    }
}
