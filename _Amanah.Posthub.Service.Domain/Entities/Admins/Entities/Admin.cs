﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class Admin : BaseEntity<string>
    {
        public Admin() { }
        public Admin(string tenantId)
        {
            Tenant_Id = tenantId;
        }

        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public int DisplayImage { get; set; }
        public string DashBoardLanguage { get; set; }
        public string TrackingPanelLanguage { get; set; }
        public string DeactivationReason { get; set; }
        public int ResidentCountryId { get; set; }

        public int? BranchId { set; get; }
        public virtual Branch Branch { set; get; }

        // public virtual ApplicationUser User { get; set; }
    }
}
