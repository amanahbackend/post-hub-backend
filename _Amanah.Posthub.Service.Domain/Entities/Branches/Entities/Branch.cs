﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.CompanyProfile;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using System.Collections.Generic;

namespace Amanah.Posthub.DATA.Entities
{
    public class Branch : BaseEntity <int>
    {
        public Branch()
        {

        }
        public Branch(string tenantId)
        {
            Tenant_Id = tenantId;
        }

        public string Name { get; set; }
        public int? CountryId { set; get; }
        public Country Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }


        public int? RestaurantId { get; set; }
        public int? GeoFenceId { get; set; }
        public int? CompanyId { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public bool IsActive { get; set; }
        public bool IsActivePrevious { get; set; }

        public Address Location { set; get; }

        public virtual GeoFence GeoFence { get; set; }
        public virtual Restaurant Restaurant { get; set; }
        public string Reason { get; set; }
        public Customer Customer { set; get; }
        public Company Company { set; get; }
        public List<Company> Companies { set; get; }
        
    }

}
