﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DeliverService.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.DeliverService.Domain.Managers.Entities;
using Amanah.Posthub.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amanah.Posthub.DeliverService.Domain.BusinessCustomers
{
    public class BusinessContactAccount :BaseEntity
    {

        ///Business-Account Contacts
        public string UserID { set; get; }
        public ApplicationUser User { get; set; }
        public string ContactCode { set; get; }
        public int ContactFunctionId { set; get; }
        public ContactFunction ContactFunction { set; get; }

        public int CourtesyId { set; get; }
        public Courtesy Courtesy { set; get; }

        public int AccountStatusId { get; set; }
        public AccountStatus AccountStatus { get; set; }
        public string PersonalPhotoURL { get; set; }
        public string MobileNumber { get; set; }
        public string Fax { get; set; }

        public int BusinessCustomerId { set; get; }
        public BusinessCustomer BusinessCustomer { set; get; }
        public int BusinessCustomerBranchId { set; get; }
    }
}
