﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.TenantRegistrations.Entities;
using Amanah.Posthub.DeliverService.Domain.BusinessCustomers.Entities;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Amanah.Posthub.DeliverService.Domain.BusinessCustomers
{
    public class BusinessCustomer : BaseEntity
    {

        ///Bussines Info
        public string BusinessName { set; get; }
        public int BusinessTypeId { set; get; }
        public CompanyProfile.Entities.BusinessType BusinessType { set; get; }
        public string BusinessLogoUrl { set; get; }
        public string BusinessCID { set; get; }
        public string BusinessCode{ set; get; }
        public int CollectionMethodId { set; get; }
        public CollectionMethod CollectionMethod { set; get; }
        public bool IsCompleted { set; get; } = false;

        public int? HQBranchId { set; get; }
        [ForeignKey("HQBranchId")]
        public BusinessCustomerBranch HQBranch { set; get; }
      
        public int? MainContactId { set; get; }
        [ForeignKey("MainContactId")]
        public BusinessContactAccount MainContact { set; get; }
        public List<BusinessContactAccount> BusinessCustomerContacts { set; get; }
        public List<BusinessCustomerBranch> BusinessCustomerBranches { set; get; }
    }
}
