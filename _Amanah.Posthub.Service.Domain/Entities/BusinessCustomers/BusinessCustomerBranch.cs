﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Amanah.Posthub.DeliverService.Domain.BusinessCustomers
{
    public class BusinessCustomerBranch : BaseEntity
    {
        public string Name { get; set; }
        public int? CountryId { set; get; }
        public Country Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Address Location { set; get; }
        ////
        public int BranchContactId { set; get; }
        [ForeignKey("BranchContactId")]
        public BusinessContactAccount BranchContact { set; get; }
        public int PickupContactId { set; get; }
      
        [ForeignKey("PickupContactId")]
        public BusinessContactAccount PickupContact { set; get; }
        public string CloseTime { get; set; }
        public string PreferedPickupTimeFrom { get; set; }
        public string PreferedPickupTimeTo { get; set; }
        public string PickupNotes { get; set; }
        public string DeliveryNotes { get; set; }
        public int BusinessCustomerId { set; get; }
        [ForeignKey("BusinessCustomerId")]
        public BusinessCustomer BusinessCustomer { set; get; }

    }
}
