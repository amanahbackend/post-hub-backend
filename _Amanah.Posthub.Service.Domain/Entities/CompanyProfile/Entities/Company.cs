﻿using System.Collections.Generic;
using System.Threading;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.DATA.Enums;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.Entities;
using Utilities.Extensions;

namespace Amanah.Posthub.DATA.CompanyProfile
{
    //Represent tenant Company every tenant has one row  
    public class Company : BaseEntity
    {
        /// <summary>
        /// Slogan data
        /// </summary>
        public string LogoUrl { get; set; }
        public string MissionSlogan { get; set; }
        

        /// Company Data
        public string Name { get; set; }
        
        public string BussinesDiscription{ get; set; }

        public string Address { get; set; }

        public Address CompanyAddress { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public List<CompanySocialLinks> SocialLinks { set; get; }
        public string TradeName { get; set; }

        public int? MainBranchId { get; set; }
        public Branch MainBranch { get; set; }

        public List<Branch> Branches { get; set;}
        public List<WorkingHours> WorkingHours { get; set; }
    }

}
