﻿using Amanah.Posthub.BASE.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities
{
    public class CompanySocialLinks : BaseEntity
    {
        public int Id { set; get; }
        public int CompanyId { set; get; }
        public string SiteName { set; get; }
        public string SiteLink { set; get; }
    }
}
