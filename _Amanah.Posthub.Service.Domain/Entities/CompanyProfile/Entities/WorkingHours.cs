﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.CompanyProfile;
using Amanah.Posthub.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities
{
    public  class WorkingHours: BaseEntity
    {
        public int CompanyId { set; get; }
        public DayOfWeek Day { set; get; }
        public TimeSpan? From { set; get; }
        public TimeSpan? To { set; get; }
        public TimeSpan? SecondFrom { set; get; }
        public TimeSpan? SecondTo { set; get; }
        public Company Company { get; set; }

    }
}
