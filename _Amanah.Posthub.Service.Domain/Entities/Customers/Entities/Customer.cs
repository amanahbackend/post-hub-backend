﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class Customer : BaseEntity <int>
    {
     
        public Customer()
        {
        }
        public Customer(string tenantId)
        {
            Tenant_Id = tenantId;
        }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Tags { get; set; }
        public int? CountryId { get; set; }
        public Country Country { get; set; }
        public int? BranchId { get; set; }
        public Branch Branch { get; set; }
        public Address Location { get; set; }

    }
}
