﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.Enums;

namespace Amanah.Posthub.DATA.Entities
{
    public class DriverAttachment :BaseEntity <int>
    {
        public int DriverRegistrationId { get; set; }
        public DriverAttachmentType Type { get; set; }
        public string File { get; set; }
    }
}
