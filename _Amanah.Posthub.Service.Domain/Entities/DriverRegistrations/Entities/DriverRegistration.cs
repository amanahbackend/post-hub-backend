﻿using System.Collections.Generic;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.Enums;
using Amanah.Posthub.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class DriverRegistration : BaseEntity <int>
    {
        public DriverRegistration()
        {
        }
        public DriverRegistration(string tenantId)
        {
            Tenant_Id = tenantId;
        }
        public UserRegistrationStatus UserRegistrationStatus { get; set; }
        public string ApprovedORejectedByUserId { get; set; }
        public ApplicationUser ApprovedORejectedByUser { get; set; }
        public string RejectReason { get; set; }

        public string Comment { get; set; }

        public int CountryId { get; set; }
        public Country Country { get; set; }
        public string AreaId { get; set; }
        public int TrainingRate { get; set; }
        public string TrainingLanguage { get; set; }
        public string TrainingComments { get; set; }
        public List<int> PlatFormZones { set; get; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }

        public string JobStatusName { get; set; }

        public string LicensePlate { get; set; }
        public int TransportTypeId { set; get; }
        public TransportType TransportType { get; set; }
        public string TransportColor { get; set; }
        public string TransportYearAndModel { get; set; }
        public int? DriverId { get; set; }
        public Driver Driver { get; set; }

        public List<DriverAttachment> DriverAttachments { get; set; }
    }
}
