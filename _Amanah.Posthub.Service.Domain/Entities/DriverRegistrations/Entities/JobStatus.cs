﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class JobStatus : BaseEntity
    {
        public string Name { get; set; }
    }
}
