﻿namespace Amanah.Posthub.DATA.Enums
{
    public enum UserRegistrationStatus
    {
        New = 1,
        Pending,
        Approved,
        Rejected
    }
}
