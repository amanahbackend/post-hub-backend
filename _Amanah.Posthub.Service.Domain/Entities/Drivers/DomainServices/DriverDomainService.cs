﻿using System;
using System.Collections.Generic;
using System.Text;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.Drivers.DomainServices
{
    public interface IDriverDomainService
    {

    }
    public class DriverDomainService : IDriverDomainService
    {
        private readonly IRepository<Driver> _driverRepository;

        public DriverDomainService(IRepository<Driver> driverRepository)
        {
            _driverRepository = driverRepository;
        }
    }
}
