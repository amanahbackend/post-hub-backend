﻿using System.Collections.Generic;
using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class AgentStatus : BaseGlobalEntity
    {
        public string Name { set; get; }
        public List<Driver> Drivers { set; get; }
    }
}