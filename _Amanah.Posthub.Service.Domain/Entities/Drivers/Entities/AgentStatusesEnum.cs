﻿namespace Amanah.Posthub.BLL.Enums
{
    public enum AgentStatusesEnum
    {
        Offline = 1,
        Available = 2,
        Unavailable = 3,
        Busy = 4,
        Blocked = 5,
    }
}
