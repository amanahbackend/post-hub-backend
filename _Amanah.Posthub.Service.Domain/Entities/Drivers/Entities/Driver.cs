﻿using System;
using System.Collections.Generic;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DeliverService.Domain.CompanyProfile.Entities;
using Amanah.Posthub.DeliverService.Domain.Managers.Entities;
using Amanah.Posthub.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class Driver : BaseEntity
    {
        public string ImageUrl { get; set; }
        public string Tags { set; get; }
        public string TransportDescription { get; set; }
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public string Role { get; set; }
        public int TeamId { set; get; }
        public Team Team { get; set; }
        public int? AgentTypeId { set; get; }
        public AgentType AgentType { get; set; }
        public int TransportTypeId { set; get; }
        public TransportType TransportType { get; set; }
        public int CountryId { set; get; }
        public Country Country { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
        public int? AgentStatusId { get; set; }
        /// <summary>
        ///Last Status before disconnect  
        /// </summary>
        public int? LastDisconnectStatusId { get; set; }
        public AgentStatus AgentStatus { set; get; }
        public DateTime? ReachedTime { get; set; }
        public DateTime AgentStatusUpdateDate { get; set; }
        public string DeviceType { get; set; }
        public string Version { get; set; }
        public string Reason { get; set; }
        public int MaxOrdersWeightsCapacity { get; set; } = 1;
        public int MinOrdersNoWait { get; set; } = 1;
        public DriverCurrentLocation CurrentLocation { get; set; }
        public bool IsInClubbingTime { get; set; }
        public DateTime? ClubbingTimeExpiration { get; set; }
        public double? DriverAvgRate { get; set; }
        public DateTime? TokenTimeExpiration { get; set; }
        public bool AllPickupGeoFences { get; set; }
        public bool AllDeliveryGeoFences { get; set; }

        public virtual ICollection<DriverPickUpGeoFence> DriverPickUpGeoFences { get; set; }
        public virtual ICollection<DriverDeliveryGeoFence> DriverDeliveryGeoFences { get; set; }
        public virtual ICollection<DriverLoginRequest> DriverLoginRequests { get; set; }
        public virtual ICollection<SubTask> AssignedTasks { get; set; }
        public virtual ICollection<DriverRate> DriverRates { get; set; }

        ////Added Things 
        public string MobileNumber { get; set; }
        public string LicenseNumber { get; set; }
        public int GenderId { get; set; }
        public Gender Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int NationalityId { get; set; }
        public Country Nationality { get; set; }
        public Address Address { set; get; }
        public string NationalId { get; set; }
        public string SupervisorId { get; set; }
        public ApplicationUser Supervisor { get; set; }
        public int ServiceSectorId { get; set; }
        public ServiceSector ServiceSector { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public int BranchId { get; set; }
        public Branch Branch { get; set; }
        public int AccountStatusId { get; set; }
        public AccountStatus AccountStatus { get; set; }
        public string CompanyEmail { get; set; }


    }
}
