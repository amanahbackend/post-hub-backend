﻿namespace Amanah.Posthub.DATA.Entities
{
    public class DriverPickUpGeoFence
    {
        public int Id { set; get; }
        public int DriverId { set; get; }
        public int GeoFenceId { set; get; }
        public Driver Driver { get; set; }
        public GeoFence GeoFence { get; set; }
    }
}
