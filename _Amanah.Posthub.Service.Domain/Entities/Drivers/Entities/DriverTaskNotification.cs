﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class DriverTaskNotification : BaseEntity
    {
        public int TaskId { set; get; }
        public int DriverId { set; get; }
        public string Description { set; get; }
        public string ActionStatus { set; get; }
        public bool? IsRead { set; get; }

        public Driver Driver { set; get; }
        public SubTask Task { set; get; }
    }
}
