﻿namespace Amanah.Posthub.BLL.Enums
{
    public enum TransportTypeEnum
    {
        Truck = 1,
        Car = 2,
        Scooter = 4,
        Cycle = 5,
        Walk = 6,
        Drone = 10
    }
}
