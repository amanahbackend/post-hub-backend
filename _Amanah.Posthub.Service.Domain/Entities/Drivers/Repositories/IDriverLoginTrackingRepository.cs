﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.Drivers.Repositories
{
    public interface IDriverLoginTrackingRepository : IRepository<DriverloginTracking>
    {
        Task UpdateAsync(List<int> driverIds);
        Task UpdateAsync(int driverId, double latitude, double longitude);
    }
}
