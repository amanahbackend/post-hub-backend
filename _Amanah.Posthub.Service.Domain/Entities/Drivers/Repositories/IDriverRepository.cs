﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.Drivers.Repositories
{
    public interface IDriverRepository : IRepository<Driver>
    {
        Task<Driver> GetAsync(int id);
        Task<Driver> GetWithGeofencesAsync(int id);
        Task<Driver> GetWithCurrentLocationAsync(int id);
        Task<Driver> GetByUserIdAsync(string userId);

        void Update(Driver driver, List<DriverPickUpGeoFence> driverPickUpGeoFences = null, List<DriverDeliveryGeoFence> driverDeliveryGeoFences = null);
        Task Remove(List<int> driverIds);
        Task Remove(int driverId);
        Task<bool> ChangeDriverStatusByIdAsync(int id, int statusId);
        Task<bool> ChangeDriverStatusByUserIdAsync(string userId, int statusId);
        bool ChangeDriverStatusByUserIdAsync(ref Driver driver, int statusId);
        Task<IQueryable<Manager>> GetDriverManagersByTeamIdAsync(int teamId);
        Task<bool> IsDriverAsync(string UserId);
    }

}
