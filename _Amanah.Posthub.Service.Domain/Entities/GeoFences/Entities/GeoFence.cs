﻿using System.Collections.Generic;
using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class GeoFence : BaseEntity
    {
        public GeoFence()
        {
            GeoFenceLocations = new HashSet<GeoFenceLocation>();
        }
        public GeoFence(string tenantId)
        {
            GeoFenceLocations = new HashSet<GeoFenceLocation>();
            Tenant_Id = tenantId;
        }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<GeoFenceLocation> GeoFenceLocations { get; set; }
    }
}
