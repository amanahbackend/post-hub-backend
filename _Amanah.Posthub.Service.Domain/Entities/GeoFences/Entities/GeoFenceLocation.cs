﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class GeoFenceLocation : BaseEntity
    {
        public int GeoFenceId { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public virtual GeoFence GeoFence { get; set; }
        public int PointIndex { get; set; }
    }
}
