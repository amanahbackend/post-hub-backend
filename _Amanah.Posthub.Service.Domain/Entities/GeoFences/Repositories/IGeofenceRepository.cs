﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.GeoFences.Repositories
{
    public interface IGeofenceRepository : IRepository<GeoFence>
    {
        Task<List<GeoFence>> GetWithGeofencLocationAsync(Expression<Func<GeoFence, bool>> expression);
    }
}
