﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class DriverLoginRequest : BaseEntity <int>
    {
        /// <summary>
        /// DriverLoginRequestSatus enum type
        /// </summary>
        public byte Status { get; set; }
        public string Token { get; set; }
        public int DriverId { get; set; }

        public string ApprovedOrRejectBy { get; set; }

        public Driver Driver { get; set; }
        public string Reason { get; set; }

        private DriverLoginRequest()
        {
        }

        public DriverLoginRequest(string tenantId)
        {
            Tenant_Id = tenantId;
        }
    }
}
