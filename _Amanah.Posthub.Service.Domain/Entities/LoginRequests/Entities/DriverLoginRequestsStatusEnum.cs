﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amanah.Posthub.DATA.Entities
{
    public enum DriverLoginRequestStatus : byte
    {
        Pending = 1,
        Approved,
        Rejected,
        Canceled
    }


}
