﻿using System;
using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class DriverloginTracking : BaseEntity
    {
        public int DriverId { get; set; }
        public DateTime? LoginDate { get; set; }
        public double? LoginLatitude { get; set; }
        public double? LoginLongitude { get; set; }
        public DateTime? LogoutDate { get; set; }
        public double? LogoutLatitude { get; set; }
        public double? LogoutLongitude { get; set; }
        public string LogoutActionBy { get; set; }
        public Driver Driver { get; set; }

        private DriverloginTracking()
        {
        }

        public DriverloginTracking(string tenantId)
        {
            Tenant_Id = tenantId;
        }
    }
}
