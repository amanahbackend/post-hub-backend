﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.SharedKernel.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amanah.Posthub.DeliverService.Domain.Managers.Entities
{
    public class Gender : BaseEntity, ILookupEntity
    {
        public string Name_en { get; set; }
        public string Name_ar { get; set; }

    }
}
