﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class ManagerDispatching : BaseEntity
    {
        public string DesignationName { get; set; }
        public string Zones { get; set; }
        public string Restaurants { get; set; }
        public string Branches { get; set; }

        public int ManagerId { get; set; }
        public Manager Manager { get; set; }
    }
}
