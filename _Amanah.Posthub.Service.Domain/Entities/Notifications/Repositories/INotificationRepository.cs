﻿using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.Notifications.Repositories
{
    public interface INotificationRepository : IRepository<Notification>
    {

    }
}
