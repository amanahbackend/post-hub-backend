﻿using System.Collections.Generic;
using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class Restaurant : BaseEntity
    {
        public Restaurant()
        {
        }
        public Restaurant(string tenantId)
        {
            Tenant_Id = tenantId;
        }

        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string Reason { get; set; }

        public virtual ICollection<Branch> Branchs { get; set; }

    }
}
