﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Resources;
using System.Threading.Tasks;
using Amanah.Posthub.BLL.Settings.Constant;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.MainTasks.Repositories
{
    public interface ISettingRepository : IRepository<Setting>
    {
        Task<int> GetAutoAllocationTypeidAsync();
        Task<Setting> GetSettingByKeyAsync(string key);
        IQueryable<Setting> GetSettingByKeyAsync(List<string> key);
        Task<Setting> GetAsync(int id);
        Task<bool> AddUserDefaultSettingsAsync(string tenantId, DefaultSettingValue defaultSetting, ResourceSet resourceSet);
    }

}
