﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amanah.Posthub.BLL.Enums
{
    public enum SettingGroupEnum
    {
        AutoAllocation = 1,
        General = 2
    }
}
