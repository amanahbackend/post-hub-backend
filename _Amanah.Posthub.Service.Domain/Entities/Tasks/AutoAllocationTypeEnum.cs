﻿namespace Amanah.Posthub.BLL.Enums
{
    public enum AutoAllocationTypeEnum  :byte
    {
        Manual=0,
        Fifo,
        Nearest,
        OneByOne
    }
}
