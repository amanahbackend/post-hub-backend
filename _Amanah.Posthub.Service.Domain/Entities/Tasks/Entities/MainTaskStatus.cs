﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class MainTaskStatus : BaseGlobalEntity
    {
        public string Name { set; get; }
    }
}
