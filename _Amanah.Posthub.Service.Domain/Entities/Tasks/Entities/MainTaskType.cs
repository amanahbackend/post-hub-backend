﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class MainTaskType : BaseGlobalEntity
    {
        public string Name { set; get; }
    }
}
