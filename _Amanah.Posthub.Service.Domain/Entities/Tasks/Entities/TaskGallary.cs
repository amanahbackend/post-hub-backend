﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class TaskGallary : BaseEntity
    {
        public TaskGallary()
        {
        }
        public TaskGallary(string tenantId)
        {
            Tenant_Id = tenantId;
        }

        public int? MainTaskId { set; get; }
        public int TaskId { set; get; }
        public int? DriverId { set; get; }


        public string FileName { set; get; }
        public string FileURL { set; get; }
        public SubTask Task { set; get; }
    }
}
