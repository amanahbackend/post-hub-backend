﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class TaskHistory : BaseEntity
    {
        public TaskHistory() { }
        public TaskHistory(string tenantId)
        {
            Tenant_Id = tenantId;
        }

        public int TaskId { set; get; }
        public int MainTaskId { set; get; }
        public int? FromStatusId { set; get; }
        public int? ToStatusId { set; get; }
        public string Reason { set; get; }
        public string ActionName { set; get; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Description { set; get; }

        public SubTask Task { set; get; }
        public TaskStatus FromStatus { set; get; }
        public TaskStatus ToStatus { set; get; }
    }
}
