﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class TaskStatus : BaseGlobalEntity
    {
        public string Name { set; get; }
        public string Color { set; get; }

    }
}
