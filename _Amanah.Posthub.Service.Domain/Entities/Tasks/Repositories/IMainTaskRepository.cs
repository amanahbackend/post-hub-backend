﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.MainTasks.Repositories
{
    public interface IMainTaskRepository : IRepository<MainTask>
    {
        Task<MainTask> GetWithTasksAsync(Expression<Func<MainTask, bool>> expression);
        Task<Driver> GetMainTaskCurrentDriverAsync(int id);
    }

}
