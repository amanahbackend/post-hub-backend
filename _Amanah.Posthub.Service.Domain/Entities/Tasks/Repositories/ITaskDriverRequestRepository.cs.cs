﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.MainTasks.Repositories
{
    public interface ITaskDriverRequestRepository : IRepository<TaskDriverRequests>
    {
        Task<TaskDriverRequests> GetTaskDriverRequestsLastAsync(int mainTaskId, int? driverId );
        IQueryable<TaskDriverRequests> GetTaskDriverRequests(int mainTaskId, int? driverId );
    }

}
