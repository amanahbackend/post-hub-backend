﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.SubTasks.Repositories
{
    public interface ITaskGallaryRepository : IRepository<TaskGallary>
    {
    }

}
