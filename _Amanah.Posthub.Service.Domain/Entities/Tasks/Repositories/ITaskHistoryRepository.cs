﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.TaskHistorys.Repositories
{
    public interface ITaskHistoryRepository : IRepository<TaskHistory>
    {
     
    }

}
