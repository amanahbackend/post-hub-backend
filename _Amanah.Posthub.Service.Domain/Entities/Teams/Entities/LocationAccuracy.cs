﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class LocationAccuracy : BaseGlobalEntity
    {
        public string Name { set; get; }
        public int? Duration { set; get; }
    }
}
