﻿using Amanah.Posthub.BASE.Domain.Entities;

namespace Amanah.Posthub.DATA.Entities
{
    public class TeamManager  :BaseEntity
    {
        public int TeamId { set; get; }
        public Team Team { get; set; }
        public int ManagerId { set; get; }
        public Manager Manager { get; set; }
    }
}
