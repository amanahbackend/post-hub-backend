﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amanah.Posthub.DATA.TenantRegistrations.Entities
{
    public class ApproveSettings
    {
        public bool IsFullVersion { get; set; }
        public string AutoAllocationMethod { get; set; }
        public List<int> SelectedModules { get; set; }

    }
}
