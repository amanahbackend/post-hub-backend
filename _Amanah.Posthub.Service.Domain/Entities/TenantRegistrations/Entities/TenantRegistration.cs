﻿using System.Collections.Generic;
using System.Threading;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.DATA.Enums;
using Amanah.Posthub.DATA.TenantRegistrations.Entities;
using Amanah.Posthub.Entities;
using Utilities.Extensions;

namespace Amanah.Posthub.DATA.TenantRegistrations
{
    public class TenantRegistration : BaseEntity
    {
        public UserRegistrationStatus UserRegistrationStatus { get; set; }
        public string ApprovedORejectedByUserId { get; set; }
        public ApplicationUser ApprovedORejectedByUser { get; set; }
        public string RejectReason { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public BusinessType BusinessType { get; set; }
        public string OtherBusinessType { get; set; }


        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string BusinessCID { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public Address BusinessAddress { get; set; }
        public string BusinessAddressSummaryEnglish { get; set; }
        public string BusinessAddressSummaryArabic { get; set; }

        public string BusinessAddressSummary => Thread.CurrentThread.CurrentCulture.IsArabic()
            ? BusinessAddressSummaryArabic
            : BusinessAddressSummaryEnglish;

        public string OwnerName { get; set; }
        public string OwnerCID { get; set; }
        public string OwnerPhoneNumber { get; set; }

        public Address OwnerAddress { get; set; }
        public string OwnerAddressSummaryEnglish { get; set; }
        public string OwnerAddressSummaryArabic { get; set; }
        public string OwnerAddressSummary => Thread.CurrentThread.CurrentCulture.IsArabic()
            ? OwnerAddressSummaryArabic
            : OwnerAddressSummaryEnglish;

        public bool HasOwnDrivers { get; set; }
        public bool HasMultiBranches { get; set; }
        public int BranchCount { get; set; }

        public bool IsDeliverToAllZones { get; set; }
        public double? ServingRadiusInKM { get; set; }
        public int NumberOfOrdersByDriverPerOrder { get; set; }
        public string Comment { get; set; }

        public ApproveSettings ApproveSettings { get; set; }

        public List<TenantSocialMediaAccount> SocialMediaAccounts { get; set; }
        public List<TenantRegistrationAttachment> Attachments { get; set; }
    }

}
