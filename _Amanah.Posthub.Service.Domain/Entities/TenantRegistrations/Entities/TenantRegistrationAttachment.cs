﻿namespace Amanah.Posthub.DATA.TenantRegistrations
{
    public class TenantRegistrationAttachment
    {
        public int Id { get; set; }
        public int TenantRegistrationId { get; set; }
        public string File { get; set; }
    }

}
