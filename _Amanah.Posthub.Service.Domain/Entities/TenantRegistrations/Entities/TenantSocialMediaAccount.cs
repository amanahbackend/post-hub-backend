﻿namespace Amanah.Posthub.DATA.TenantRegistrations
{
    public class TenantSocialMediaAccount
    {
        public int Id { get; set; } 
        public int TenantRegistrationId { get; set; }
        public string AccountLink { get; set; }
    }

}
