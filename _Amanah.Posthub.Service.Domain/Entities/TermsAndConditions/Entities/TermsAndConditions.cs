﻿using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.Entities;

namespace Amanah.Posthub.DATA
{
    public class TermsAndConditions : BaseGlobalEntity
    {
        public string TermsAndConditionsInEnglish { get; set; }
        public string TermsAndConditionsInArabic { get; set; }
        public TermsAndConditionsType Type { get; set; }
    }
}
