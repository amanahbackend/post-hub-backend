﻿using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.Domain.Users.Repositories;
using Amanah.Posthub.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Utilities.Utilites.Exceptions;
using Utilities.Utilites.Localization;

namespace Amanah.Posthub.Domain.Users.DomainServices
{
    public interface IRoleDomainService
    {
        public Task<ApplicationRole> CreateAsync(ApplicationRole role, List<string> rolePermissions = null);
        public Task<bool> DeleteAsync(string roleName);
        public Task<ApplicationRole> UpdateAsync(ApplicationRole role, List<string> rolePermissions = null);

    }

    public class RoleDomainService : IRoleDomainService
    {
        private readonly IRepository<AccountLogs> accountLogRepository;
        private readonly IRoleRepository roleRepository;
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly IUnitOfWork unitOfWork;

        public RoleDomainService(
            IRepository<AccountLogs> accountLogRepository,
            IRoleRepository roleRepository,
            RoleManager<ApplicationRole> roleManager,
            IUnitOfWork unitOfWork)
        {
            this.accountLogRepository = accountLogRepository;
            this.roleRepository = roleRepository;
            this.roleManager = roleManager;
            this.unitOfWork = unitOfWork;
        }

        public async Task<ApplicationRole> CreateAsync(ApplicationRole role, List<string> rolePermissions = null)
        {
            if (await roleRepository.IsRoleExistAsync(role.Name))
            {
                throw new DomainException(Keys.Validation.RoleAlreadyExists);
            }
            else
            {
                await unitOfWork.RunTransaction(async () =>
                 {
                     var addRoleResult = await roleManager.CreateAsync(role);
                     if (!addRoleResult.Succeeded)
                     {
                         throw new DomainException(addRoleResult.Errors.FirstOrDefault().Description);
                     }
                     foreach (string permission in rolePermissions)
                     {
                         await roleManager.AddClaimAsync(role, new Claim(CustomClaimTypes.Permission, permission));
                     }

                     accountLogRepository.Add(new AccountLogs()
                     {
                         TableName = "ManagerAccessControl",
                         ActivityType = "Create",
                         Description = $"{role.Name} has been created with Id {role.Id}"
                     });

                     await unitOfWork.SaveChangesAsync();
                 });

                return role;

            }
        }

        public async Task<bool> DeleteAsync(string roleName)
        {
            var role = await roleRepository.GetAsync(x => x.NormalizedName.ToLower() == roleName);
            if (role == null)
            {
                throw new DomainException(Keys.Validation.RoleNotFound);
            }
            bool isRoleAssignedToUser = await roleRepository.IsRoleAssignedToUsersAsync(role);
            if (isRoleAssignedToUser)
            {
                throw new DomainException(Keys.Validation.CanNotDeleteRoleAssignedToManagers);
            }
            await unitOfWork.RunTransaction(async () =>
            {
                var deleteResult = await roleManager.DeleteAsync(role);
                if (!deleteResult.Succeeded)
                {
                    throw new DomainException(deleteResult.Errors.FirstOrDefault().Description);
                }
                accountLogRepository.Add(new AccountLogs()
                {
                    TableName = "AccessControl",
                    ActivityType = "Delete",
                    Description = $"{roleName} has been deleted"
                });

                await unitOfWork.SaveChangesAsync();
            });

            return true;
        }

        public async Task<ApplicationRole> UpdateAsync(ApplicationRole role, List<string> rolePermissions = null)
        {
            var existedRole = await roleRepository.GetAsync(x => x.Name.ToLower() == role.Name.ToLower());
            if (existedRole == null)
            {
                throw new DomainException(Keys.Validation.RoleNotFound);
            }
            else
            {
                await unitOfWork.RunTransaction(async () =>
                {
                    await roleRepository.RemoveRoleClaimsAsync(existedRole);
                    foreach (var permission in rolePermissions)
                    {
                        await roleManager.AddClaimAsync(existedRole, new Claim(CustomClaimTypes.Permission, permission));
                    }

                    accountLogRepository.Add(new AccountLogs()
                    {
                        TableName = "AccessControl",
                        ActivityType = "Update",
                        Description = $"Role {existedRole.Name} has been updated with Id {existedRole.Id}",
                    });

                    await unitOfWork.SaveChangesAsync();
                });
                return existedRole;
            }
        }
    }
}
