﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Amanah.Posthub.BASE.Domain.Entities;
using Amanah.Posthub.DATA.Entities;
using Amanah.Posthub.Models.Entities;
using Microsoft.AspNetCore.Identity;

namespace Amanah.Posthub.Entities
{
    public class ApplicationUser : IdentityUser, IBaseEntity<string>
    {
        public ApplicationUser()
        {
        }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public bool IsAvailable { get; set; }

        public int CountryId { set; get; }

        public bool IsLocked { get; set; }

        public string CreatedBy_Id { get; private set; }

        public string UpdatedBy_Id { get; private set; }

        public string DeletedBy_Id { get; private set; }

        public bool IsDeleted { get; private set; }
        public string Tenant_Id { get; private set; }

        public DateTime CreatedDate { get; private set; }

        public DateTime UpdatedDate { get; private set; }

        public DateTime DeletedDate { get; private set; }

        public List<UserDevice> UserDvices { get; set; }

        public Country Country { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public void SetIsDeleted(bool isDeleted)
        {
            IsDeleted = isDeleted;
            if (isDeleted)
            {
                DeletedDate = DateTime.UtcNow;
            }
            else
            {
                DeletedDate = DateTime.MinValue;
            }
        }

        public void SetTenant(string tenantId)
        {
            if (!string.IsNullOrWhiteSpace(Tenant_Id))
            {
                throw new InvalidOperationException("Tenant already set to a value.");
            }
            Tenant_Id = tenantId;
        }

        [NotMapped]
        public List<string> RoleNames { get; set; }

        [NotMapped]
        public List<string> Permissions { get; set; }

        [NotMapped]
        public bool IsTenantUser { get; set; }

        [NotMapped]
        public int? UserType { get; set; }

        public virtual IReadOnlyCollection<IdentityUserRole<string>> UserRoles { get; set; }
        public virtual IReadOnlyCollection<IdentityUserClaim<string>> UserClaims { get; set; }

    }
}
