﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Amanah.Posthub.BASE.Authentication;
using Amanah.Posthub.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.Users.Repositories
{
    public interface IRoleRepository : IRepository<ApplicationRole, string>
    {
        Task<ApplicationRole> GetAsync(Expression<Func<ApplicationRole, bool>> roleExpression);
        Task<IQueryable<ApplicationRole>> GetAllRolesWithClaimsAsQueryableAsync(RoleType type = RoleType.Manager);
        Task<bool> IsRoleExistAsync(string roleName, RoleType type = RoleType.Manager);
        Task<bool> IsRoleAssignedToUsersAsync(ApplicationRole role);
        Task<bool> RemoveClaimAsync(ApplicationRole role, string claimName);
        Task<bool> RemoveRoleClaimsAsync(ApplicationRole role);

    }
}
