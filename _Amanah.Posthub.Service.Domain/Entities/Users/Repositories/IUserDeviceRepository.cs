﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Amanah.Posthub.Models.Entities;
using Amanah.Posthub.SharedKernel.Domain.Repositories;

namespace Amanah.Posthub.Domain.Users.Repositories
{
    public interface IUserDeviceRepository  : IRepository<UserDevice>
    {
        Task AddOrUpdateAsync(UserDevice userDevice);

        Task<List<UserDevice>> SoftDeleteAsync(Expression<Func<UserDevice, bool>> expression);
        Task<List<UserDevice>> HardDeleteAsync(Expression<Func<UserDevice, bool>> expression);
    }
}
